<?php
include(__DIR__."../../../../pages/conexion.php");

function generarFlecha($paso_id){
    global $mysqli;

    //Seleccione todas las opciondes de bot que sean 1 o 2
    $sql = "SELECT * FROM dyalogo_canales_electronicos.dy_base_autorespuestas_contenidos A INNER JOIN dyalogo_canales_electronicos.dy_base_autorespuestas B ON A.id_base_autorespuestas = B.id WHERE B.id_estpas = {$paso_id} AND (A.accion = 1 OR A.accion = 2)";
    $res = $mysqli->query($sql);

    if($res && $res->num_rows > 0){

        while($row = $res->fetch_object()){

            $idPasoEnd = 0;
            $idEstrategia = 0;

            // Busco el id del paso de la bola destino
            if($row->accion == 1){
                $sqlPasoEnd = "SELECT ESTPAS_ConsInte__b AS id, ESTPAS_ConsInte__ESTRAT_b AS estrategia FROM DYALOGOCRM_SISTEMA.ESTPAS INNER JOIN DYALOGOCRM_SISTEMA.CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b WHERE CAMPAN_IdCamCbx__b = ".$row->id_campana;
                $resPasoEnd = $mysqli->query($sqlPasoEnd);
                
                if($resPasoEnd && $resPasoEnd->num_rows > 0){
                    $pasoEnd = $resPasoEnd->fetch_object();
                    $idPasoEnd = $pasoEnd->id;
                    $idEstrategia = $pasoEnd->estrategia;
                }
            }else if($row->accion == 2){
                $sqlPasoEnd = "SELECT ESTPAS_ConsInte__b AS id, ESTPAS_ConsInte__ESTRAT_b AS estrategia FROM DYALOGOCRM_SISTEMA.ESTPAS INNER JOIN dyalogo_canales_electronicos.dy_base_autorespuestas ON ESTPAS_ConsInte__b = id_estpas WHERE id = ".$row->id_base_transferencia;
                $resPasoEnd = $mysqli->query($sqlPasoEnd);
                
                if($resPasoEnd && $resPasoEnd->num_rows > 0){
                    $pasoEnd = $resPasoEnd->fetch_object();
                    $idPasoEnd = $pasoEnd->id;
                    $idEstrategia = $pasoEnd->estrategia;
                }
            }


            // Si hay un paso final validar si existe en estcon
            if($idPasoEnd != 0){
                $sqlFlechas = "SELECT * FROM DYALOGOCRM_SISTEMA.ESTCON WHERE ESTCON_ConsInte__ESTPAS_Des_b = {$paso_id} AND ESTCON_ConsInte__ESTPAS_Has_b = $idPasoEnd";
                $resFlechas = $mysqli->query($sqlFlechas);
                if($resFlechas && $resFlechas->num_rows > 0){
                    //
                }else{
                    $insertFlecha = "INSERT INTO DYALOGOCRM_SISTEMA.ESTCON (ESTCON_Nombre____b, ESTCON_ConsInte__ESTPAS_Des_b, ESTCON_ConsInte__ESTPAS_Has_b, ESTCON_FromPort_b, ESTCON_ToPort_b, ESTCON_ConsInte__ESTRAT_b, ESTCON_Tipo_Consulta_b, ESTCON_Tipo_Insercion_b,ESTCON_ConsInte_PREGUN_Fecha_b,ESTCON_ConsInte_PREGUN_Hora_b,ESTCON_Operacion_Fecha_b,ESTCON_Operacion_Hora_b,ESTCON_Cantidad_Fecha_b,ESTCON_Cantidad_Hora_b,ESTCON_Estado_cambio_b,ESTCON_Sacar_paso_anterior_b,ESTCON_resucitar_registro) VALUES ('conector', {$paso_id}, {$idPasoEnd}, 'R', 'L', {$idEstrategia} , 1,0,-1,-1,1,1,0,0,0,0,0)";
                    $insert = $mysqli->query($insertFlecha);
                }
            }
        }

    }
    // Si es 1 necesitaria el campana
    // Si es 2 necesitaria el paso

    // Recorro las opciones
        // Validar si hay un estcon que va de $paso_id a campana o bot
        // Si no lo hay creo un estcon
}

if(isset($_GET['getdatos'])){
    $id=$mysqli->query("SELECT id,nombre FROM dyalogo_canales_electronicos.dy_base_autorespuestas WHERE id_estpas={$_GET['id_paso']}");
    if($id && $id->num_rows == 1){
        $id=$id->fetch_object();
        $nombre=$id->nombre;
        $id=$id->id;
        if($id != null && $id != '' && $id != 0){
            $datos=$mysqli->query("SELECT * FROM dyalogo_canales_electronicos.dy_base_autorespuestas_contenidos WHERE id_base_autorespuestas={$id}");
            if($datos && $datos->num_rows > '0'){
                for($i=0; $i <$datos->num_rows; $i++){
                    $data[$i]=$datos->fetch_object();
                }
                $estpas=$mysqli->query("SELECT ESTPAS_activo FROM {$BaseDatos_systema}.ESTPAS WHERE ESTPAS_ConsInte__b={$_GET['id_paso']}");
                if($estpas && $estpas->num_rows > '0'){
                    $estpas=$estpas->fetch_object();
                    $estpas=$estpas->ESTPAS_activo;
                    $data['ivrActivo']=$estpas;
                }
            }
        }
        $data['nombre']=$nombre;
        echo json_encode($data);
    }
}

if(isset($_GET['guardar'])){
    $intIdBaseRpta_t='0';
    
    if(isset($_POST['nombre_bot']) && $_POST['nombre_bot'] !=''){
        //Actualizamos estpas y dy_base_autorespuestas
        $estpas_activo=isset($_POST['pasoActivo']) ? $_POST['pasoActivo'] : '0';

        $sql=$mysqli->query("UPDATE {$BaseDatos_systema}.ESTPAS SET ESTPAS_Comentari_b='{$_POST['nombre_bot']}',ESTPAS_activo={$estpas_activo} WHERE ESTPAS_ConsInte__b={$_GET['id_paso']}");
        
        //Valido si existe dy_base_autorespuestas
        $Ssql = $mysqli->query("SELECT * FROM dyalogo_canales_electronicos.dy_base_autorespuestas WHERE id_estpas={$_GET['id_paso']} LIMIT 1");
        if($Ssql && $Ssql->num_rows > 0){
            $sql=$mysqli->query("UPDATE dyalogo_canales_electronicos.dy_base_autorespuestas SET nombre='{$_POST['nombre_bot']}', id_huesped={$_GET['huesped']} WHERE id_estpas={$_GET['id_paso']}");
        }else{
            $sql = $mysqli->query("INSERT INTO dyalogo_canales_electronicos.dy_base_autorespuestas (nombre, id_huesped, id_estpas, tipo_base) VALUES ('{$_POST['nombre_bot']}', {$_GET['huesped']}, {$_GET['id_paso']}, 1)");
        }

        $valido=true;
    }

    $intIdBaseRpta_t=$mysqli->query("SELECT id FROM dyalogo_canales_electronicos.dy_base_autorespuestas WHERE id_estpas={$_GET['id_paso']} LIMIT 1");

    if($intIdBaseRpta_t && $intIdBaseRpta_t->num_rows == '1'){
        
        $intIdBaseRpta_t=$intIdBaseRpta_t->fetch_object();
        $intIdBaseRpta_t=$intIdBaseRpta_t->id;
        $valido=false;

        if($intIdBaseRpta_t !=null && $intIdBaseRpta_t != '' && $intIdBaseRpta_t != '0'){
            if($_POST['totalFilas']=='0'){
                $valido=true;
            }else{

                for($i=0; $i<$_POST['totalFilas']; $i++){
                    
                    $oper= isset($_POST['campo_edit_'.$i]) ? 'edit': 'add';
                    $oper= isset($_POST['campo_del_'.$i]) ? 'del': $oper;
                    $strPgta_t=isset($_POST['pregunta_'.$oper.'_'.$i]) ? $_POST['pregunta_'.$oper.'_'.$i] :false; 
                    $strRpta_t=isset($_POST['rpta_'.$oper.'_'.$i]) ? $_POST['rpta_'.$oper.'_'.$i] :false;
                    $strTags_t=isset($_POST['tags_'.$oper.'_'.$i]) ? $_POST['tags_'.$oper.'_'.$i] :false;
                    $strAccion_t=isset($_POST['accion_'.$oper.'_'.$i]) ? $_POST['accion_'.$oper.'_'.$i] :false;
                    $strCampan_t=(isset($_POST['campan_'.$oper.'_'.$i]) && $_POST['campan_'.$oper.'_'.$i] != '') ? $_POST['campan_'.$oper.'_'.$i] :'null';
                    $strBot_t=(isset($_POST['bot_'.$oper.'_'.$i]) && $_POST['bot_'.$oper.'_'.$i] != '') ? $_POST['bot_'.$oper.'_'.$i] :'null';
                    $strCapturarDato_t=(isset($_POST['captura_dato_'.$oper.'_'.$i]) && $_POST['captura_dato_'.$oper.'_'.$i] != '') ? $_POST['captura_dato_'.$oper.'_'.$i] :'null';
                    $strConsultarDato_t=(isset($_POST['consulta_dato_'.$oper.'_'.$i]) && $_POST['consulta_dato_'.$oper.'_'.$i] != '') ? $_POST['consulta_dato_'.$oper.'_'.$i] :'null';

                    if($strAccion_t == 3){
                        $strPregun = $strCapturarDato_t;
                    }else if($strAccion_t == 4){
                        $strPregun = $strConsultarDato_t;
                    }else{
                        $strPregun = 'null';
                    }
                    
                    if($strPgta_t && $strRpta_t && $strTags_t){
                        if($oper=='add'){
                            //insertar las nuevas preguntas
                            $sqlInsert = "INSERT INTO dyalogo_canales_electronicos.dy_base_autorespuestas_contenidos (id_base_autorespuestas, pregunta, tags, respuesta, accion, id_campana, id_base_transferencia, id_pregun) VALUES ({$intIdBaseRpta_t},'{$strPgta_t}','{$strTags_t}','{$strRpta_t}','{$strAccion_t}',{$strCampan_t},{$strBot_t}, {$strPregun})";
                            $insert=$mysqli->query($sqlInsert);
                        }elseif($oper == 'edit'){
                            //actualizar las preguntas
                            $insert=$mysqli->query("UPDATE dyalogo_canales_electronicos.dy_base_autorespuestas_contenidos SET pregunta='{$strPgta_t}',tags='{$strTags_t}',respuesta='{$strRpta_t}',accion='{$strAccion_t}',id_campana={$strCampan_t}, id_base_transferencia = {$strBot_t}, id_pregun = {$strPregun} WHERE id={$_POST['campo_edit_'.$i]}");
                        }else{
                            //Eliminar preguntas
                            $insert=$mysqli->query("DELETE FROM dyalogo_canales_electronicos.dy_base_autorespuestas_contenidos WHERE id={$_POST['campo_del_'.$i]}");
                        }
                        if($insert){
                            $valido=true;
                        }else{
                            $valido=false;
                        }
                    }
                }
            }
        }

        generarFlecha($_GET['id_paso']);
        
        if($valido){
            echo '1';
        }else{
            echo '0';
        }            
    }
}
?>