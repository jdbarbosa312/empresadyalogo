<?php

// Vista de configuracion del la bola tipo bot

function sanear_strings($string) { 

    // $string = utf8_decode($string);

    $string = str_replace( array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'), array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'), $string );
    $string = str_replace( array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'), array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'), $string ); 
    $string = str_replace( array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'), array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'), $string ); 
    $string = str_replace( array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'), array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'), $string ); 
    $string = str_replace( array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'), array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'), $string ); 
    $string = str_replace( array('ñ', 'Ñ', 'ç', 'Ç'), array('n', 'N', 'c', 'C',), $string ); 
    //Esta parte se encarga de eliminar cualquier caracter extraño 
    $string = str_replace( array("\\", "¨", "º", "-", "~", "#", "@", "|", "!", "\"", "·", "$", "%", "&", "/", "(", ")", "?", "'", "¡", "¿", "[", "^", "`", "]", "+", "}", "{", "¨", "´", ">“, “< ", ";", ",", ":", "."), '', $string ); 
    return $string; 
}

function reformatearVariable($nombre){

    $nombre = sanear_strings($nombre);
    $nombre = str_replace(' ', '_', $nombre);
    $nombre = strtolower($nombre);
    $nombre = substr($nombre, 0, 20);

    return $nombre;
}

$url_crud = base_url."cruds/DYALOGOCRM_SISTEMA/G26/G26_CRUD.php";
$sql = $mysqli->query("select ESTPAS_ConsInte__ESTRAT_b AS estrat FROM {$BaseDatos_systema}.ESTPAS WHERE ESTPAS_ConsInte__b ={$_GET['id_paso']}");

if($sql && $sql->num_rows == '1'){

    $sql=$sql->fetch_object();
    $estrat=$sql->estrat;

    $huesped=$mysqli->query("SELECT ESTRAT_ConsInte__PROYEC_b AS huesped FROM {$BaseDatos_systema}.ESTRAT WHERE ESTRAT_ConsInte__b = {$estrat}");

    if($huesped && $huesped->num_rows == '1'){
        $huesped=$huesped->fetch_object();
        $huesped=$huesped->huesped;
        
        // Llenamos las opciones de campaña
        $opcionesCampana = '<option value="">Seleccione</option>';
        $campanas = $mysqli->query("SELECT ESTPAS_ConsInte__CAMPAN_b AS id_campan ,B.CAMPAN_IdCamCbx__b AS dy_campan, ESTPAS_Comentari_b AS nombre_campan FROM DYALOGOCRM_SISTEMA.ESTPAS LEFT JOIN DYALOGOCRM_SISTEMA.CAMPAN B ON ESTPAS_ConsInte__CAMPAN_b=CAMPAN_ConsInte__b WHERE ESTPAS_ConsInte__ESTRAT_b={$estrat} AND ESTPAS_Tipo______b =1 AND ESTPAS_ConsInte__CAMPAN_b IS NOT NULL");
        
        while($campan = $campanas->fetch_object()){
            $opcionesCampana .= '<option value="'.$campan->dy_campan.'">'.sanear_strings($campan->nombre_campan).'</option>';
        }

        // Llenamos las opciones de secciones bot
        $opcionesSecBot = '<option value="">Seleccione</option>';
        $seccBots = $mysqli->query("SELECT id, nombre FROM dyalogo_canales_electronicos.dy_base_autorespuestas WHERE id_estpas = {$_GET['id_paso']}");
        
        while($sbot =$seccBots->fetch_object()){
            $opcionesSecBot .= '<option value="'.$sbot->id.'">'.sanear_strings($sbot->nombre).'</option>';
        }
                    
        // Llenamos las opciones de bots
        $opcionesBot = '<option value="">Seleccione</option>';
        $bots = $mysqli->query("SELECT e.ESTPAS_Comentari_b AS nombre, a.id AS id FROM DYALOGOCRM_SISTEMA.ESTPAS e 
                JOIN dyalogo_canales_electronicos.secciones_bot b ON b.id_estpas = e.ESTPAS_ConsInte__b
                JOIN dyalogo_canales_electronicos.dy_base_autorespuestas a ON a.id_seccion = b.id
            WHERE e.ESTPAS_Tipo______b = 12 AND e.ESTPAS_ConsInte__ESTRAT_b = {$estrat} AND e.ESTPAS_Comentari_b IS NOT NULL
            AND b.orden = 1 AND e.ESTPAS_ConsInte__b != {$_GET['id_paso']}");
        
        while($bot =$bots->fetch_object()){
            $opcionesBot .= '<option value="'.$bot->id.'">'.sanear_strings($bot->nombre).'</option>';
        }

        // Opciones para los campos
        $camposParaVariables = [];
        $opcionesCampos = '<option value="">Seleccione</option>';
        $campos = $mysqli->query("SELECT PREGUN_ConsInte__b AS id, PREGUN_Texto_____b AS nombre FROM DYALOGOCRM_SISTEMA.PREGUN INNER JOIN DYALOGOCRM_SISTEMA.ESTRAT ON PREGUN_ConsInte__GUION__b = ESTRAT_ConsInte_GUION_Pob WHERE ESTRAT_ConsInte__b = {$estrat} AND PREGUN_Tipo______b NOT IN (6, 11, 13) AND PREGUN_Texto_____b NOT LIKE '%_DY%'");
        
        while($campo =$campos->fetch_object()){
            $camposParaVariables[$campo->nombre] = '${'.reformatearVariable($campo->nombre).'}';
            $opcionesCampos .= '<option value="'.$campo->id.'">'.sanear_strings($campo->nombre).'</option>';
        }

        $campos2 = $mysqli->query("SELECT PREGUN_ConsInte__b AS id, PREGUN_Texto_____b AS nombre FROM DYALOGOCRM_SISTEMA.PREGUN INNER JOIN DYALOGOCRM_SISTEMA.ESTRAT ON PREGUN_ConsInte__GUION__b = ESTRAT_ConsInte_GUION_Pob WHERE ESTRAT_ConsInte__b = {$estrat} AND PREGUN_Tipo______b = 1 AND PREGUN_Texto_____b NOT LIKE '%_DY%'");
    }    
}

?>

<link rel="stylesheet" href="<?=base_url?>assets/plugins/selectize.js-master/dist/css/selectize.css">
<link rel="stylesheet" href="<?=base_url?>assets/plugins/selectize.js-master/dist/css/selectize.bootstrap3.css">

<style type="text/css">

    .embed-container {
        position: relative;
        padding-bottom: 56.25%;
        height: 0;
        overflow: hidden;
    }

    .embed-container iframe {
        position: absolute;
        top:0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    
    /* .modal { overflow: auto !important; } */

    .selectize-control.multi .selectize-input>div{
		background: #337ab7;
        color: #f6f6f6;
	}

    .selectize-control.multi .selectize-input>div.green{
		background: #00a65a;
        color: #f6f6f6;
	}

    .titulo-tabla-condiciones{
        padding: 8px;
        font-weight: bold
    }

    .text-color-white{
        color: whitesmoke;
    }

</style>

<div class="">
    <div class="box box-primary">
        <div class="box-header">
            <div class="box-tools">
                
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12" id="div_formularios">
                    <div>
                        <button class="btn btn-default" id="Save">
                            <i class="fa fa-save"></i>
                        </button>
                    </div>
                    <!-- FIN BOTONES -->
                    <!-- CUERPO DEL FORMULARIO CAMPOS-->
                    <br/>
                    <div>
                        <form id="FormularioDatos" data-toggle="validator" enctype="multipart/form-data" action="#" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="18">

                                        <input type="hidden" name="pasoId" id="pasoId" value="<?php if(isset($_GET['id_paso'])){ echo  $_GET['id_paso']; }else{ echo "0"; } ?>">
                                        <input type="hidden" name="oper" id="oper" value='add'>
                                        <input type="hidden" name="txtOrdenData" id="txtOrdenData">
                                        <input type="hidden" name="seccionPrincipal" id="seccionPrincipal">
                                        <input type="hidden" name="autorespuestaPrincipal" id="autorespuestaPrincipal">
                                        <input type="hidden" name="filtrado" id="filtrado" value="0">

                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <!-- CAMPO TIPO TEXTO -->
                                                <div class="form-group">
                                                    <label for="G14_C137" id="LblG14_C137"><?php echo $str_nombre_mail_ms; ?></label>
                                                    <input type="text" class="form-control input-sm" id="nombre_bot" value="bot_<?=$_GET['id_paso']?>"  name="nombre_bot"  placeholder="<?php echo $str_nombre_mail_ms; ?>" required>
                                                </div>
                                                
                                                <div class="col-md-1 col-xs-1" style="display:none">
                                                    <div class="form-group">
                                                        <label for="pasoActivo" id="LblpasoActivo">ACTIVO</label>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="-1" name="pasoActivo" id="pasoActivo" data-error="Before you wreck yourself" checked> 
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- FIN DEL CAMPO TIPO TEXTO -->
                                            </div>

                                            <div class="col-md-12 col-xs-12">
                                                <div class="form-group">
                                                    <label for="">Texto cuando no encuentra respuesta</label>
                                                    <input type="text" name="textoNoRespuesta" id="textoNoRespuesta" class="form-control input-sm" value="No tengo esa respuesta, por favor hazme otra pregunta">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row" style="margin-top: 15px;">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-info" onclick="abrirTagsGlobales()">
                                                    Tags globales
                                                </button>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top:15px">
                                            <div class="col-md-12">
                                                <div class="panel box box-primary box-solid">
                                                    <div class="box-header with-border">
                                                        <h4 class="box-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#seccionesBot">
                                                                Secciones del bot
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="seccionesBot" class="panel-collapse collapse in">

                                                        <div class="box-body">
                                                            <!-- <p>La sección que esté de primeras es la que se va a ejecutar cuando el bot sea invocado</p> -->
                                                            <div class="seccionBienvenida" style="margin: 30px 0;">
                                                                <div class="panel box box-primary row-seccion" id="botSeccionBienvenida" style="position: relative; top: 0px; left: 0px;">
                                                                    <div class="box-header with-border">
                                                                        <h4 class="box-title" id="titulo_bienvenida">
                                                                            <div class="nombre-seccion">
                                                                                Sección de Bienvenida
                                                                                <i class="fa fa-comments-o"></i>
                                                                            </div>
                                                                        </h4>
                                                                        
                                                                        <div class="box-tools pull-right">
                                                                            <button type="button" id="editarSeccionBienvenida" class="btn btn-primary btn-sm" onclick="editarSeccion(0,0)"><i class="fa fa-edit"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="row" style="margin: 15px 30px 20px 0px;">
                                                                <h4>Buscador de secciones</h4>
                                                                <div class="col-md-10">
                                                                    <input type="text" class="form-control" id="buscandoSeccion" placeholder="Buscar por Nombre, Pregunta, Tag, Respuesta">
                                                                    <span id="mensajeBusquedaFiltrada" style="display: none;">Esta seccion esta filtrada</span>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <button type="button" class="btn btn-info btn-block" onclick="buscarSeccion()">
                                                                        Buscar
                                                                    </button>
                                                                </div>
                                                            </div>

                                                            <div id="misSeccionesBot">
                                                                
                                                            </div>

                                                            <div>
                                                                <button class="btn btn-primary pull-right" type="button" id="agregarSeccion"><i class="fa fa-plus"></i> Agregar seccion</button>
                                                            </div>

                                                            <div class="seccionDespedida" style="margin: 80px 0px;">
                                                                <div class="panel box box-primary row-seccion" id="botSeccionDespedida">
                                                                    <div class="box-header with-border">
                                                                        <h4 class="box-title" id="titulo_despedida">
                                                                            <div class="nombre-seccion">
                                                                                Sección de Despedida
                                                                                <i class="fa fa-comments-o"></i>
                                                                            </div>
                                                                        </h4>
                                                                        
                                                                        <div class="box-tools pull-right">
                                                                            <button type="button" id="editarSeccionDespedida" class="btn btn-primary btn-sm" onclick="editarSeccion(0,0)"><i class="fa fa-edit"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div> 

                                            </div>
                                
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="ruta_Adjuntos" id="ruta_Adjuntos" value='0'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="totalFilas" id="totalFilas" value='0'>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal de seccion -->
<div class="modal fade" id="modalConfiguracionSeccion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg" style="width:95%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close cerrar-modal-config">&times;</button>
                <h4 class="modal-title">Configuración de la sección del bot</h4>
            </div>
            
            <div class="modal-body">
                <div>
                    <button class="btn btn-default" id="saveSeccion">
                        <i class="fa fa-save"></i>
                    </button>

                    <button type="button" class="btn btn-info" style="margin-left: 10px;" id="preguntasBotNoEntregabtn">Respuestas que el bot no pudo entregar</button>
                </div>

                <br/>

                <form id="formularioSeccion" method="POST" enctype="multipart/form-data" action="#">

                    <input type="hidden" name="txtOrdenAccionesBot" id="txtOrdenAccionesBot">
                    <input type="hidden" name="operSeccion" id="operSeccion" value='add'>
                    <input type="hidden" name="seccionBotId" id="seccionBotId" value="0">
                    <input type="hidden" name="autorespuestaId" id="autorespuestaId" value="0">
                    <input type="hidden" name="totalFilasSeccion" id="totalFilasSeccion" value='0'>
                    <input type="hidden" name="totalAccionesFinales" id="totalAccionesFinales" value="0">
                    <input type="hidden" name="contCondicionesAccionesFinales" id="contCondicionesAccionesFinales" value="0">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Nombre de la sección del bot</label>
                                <input type="text" name="seccionNombre" id="seccionNombre" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Tipo de la sección del bot</label>
                                <select name="tipoSeccion" id="tipoSeccion" class="form-control input-sm" disabled>
                                    <option value="1">Conversacional</option>
                                    <option value="23">Transaccional</option>
                                    <option value="2">Captura de datos</option>
                                    <option value="3">Consulta de datos</option>
                                </select>
                                <p id="info_modal" class="help-block"></p>
                            </div>
                        </div>

                        <div class="col-md-6" id="groupSeccionBd">
                            <div class="form-group">
                                <label for="">Destino almacenamiento del dato</label>
                                <select name="seccionBd" id="seccionBd" class="form-control input-sm" style="width: 100%;">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row" class="margin-top:15px;">
                        <div class="col-md-12">
                            <div class="panel box box-primary" style="border: 1px solid #e6e6e6">
                                <div class="box-header with-border" style="background-color: #009fe3">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#disparadoresAcciones" class="text-color-white">
                                            DISPARADORES Y ACCIONES DE ESTA SECCIÓN
                                        </a>
                                    </h4>
                                </div>
                                <div id="disparadoresAcciones" class="panel-collapse collapse in">

                                    <div class="box-body">

                                        <button type="button" class="btn btn-primary btn-sm" style="margin-bottom:15px" id="toggleAcordeon">Abrir/cerrar acordeones</button>

                                        <!-- Acciones que se dispara al llegar a la seccion -->
                                        <div class="panel box box-success" id="row_accion_inicial" style="display:none">
                                            <div class="box-header with-border">
                                                <h4 class="box-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#cuerpoAccionInicial">
                                                        <i class="fas fa-thumbtack"></i>&nbsp;Acción que dispara al llegar a la seccion
                                                    </a>
                                                </h4>
                                            </div>

                                            <div id="cuerpoAccionInicial" class="panel-collapse collapse in">
                                                <div class="box-body">
                                                <div class="row">
                                                    <input type="hidden" id="campo_accion_inicial" name="campo_accion_inicial" value="">
                                                    <div class="col-md-12">
                                                    
                                                        <div class="form-group">
                                                            <label for="" class="labelDisparador">Disparador local (Si el usuario escribe esto y está en esta sección se va a disparar la acción ...no es obligatorio)</label>
                                                            <input type="text" id="tag_local" name="tag_local" class="form-control input-sm">
                                                            <span class="info_disparador" class="help-block">Separe los tags con comas.</span>
                                                            <a id="myLink_local" href="#" class="pull-right modalDisparador" onclick="abrirDisparadorModal(<?=$huesped?>);return false;">Disparadores de uso frecuente</a>
                                                        </div>
                                                    
                                                        <div class="form-group">
                                                            <label for="">Respuestas (Texto que escribe el bot cuando el usuario escribe los disparadores globales, locales o en general cuando llega a esta seccion desde cualquier parte ...no es obligatorio)</label>
                                                            <textarea id="rpta_accion_inicial" name="rpta_accion_inicial" cols="60" rows="1"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Accion que ejecuta el webservice -->
                                        <div class="panel box box-success" id="row_accion_webservice" style="display:none">
                                            <div class="box-header with-border">
                                                <h4 class="box-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#cuerpoAccionWebservice">
                                                        <i class="fa fa-comments-o"></i>&nbsp;Acción que dispara la ejecucion del webservice
                                                    </a>
                                                </h4>
                                            </div>

                                            <div id="cuerpoAccionWebservice" class="panel-collapse collapse">
                                                <div class="box-body">
                                                <div class="row">
                                                    <input type="hidden" id="campo_accion_webservice" name="campo_accion_webservice" value="">
                                                    <div class="col-md-9">
                                                    
                                                        <div class="form-group">
                                                            <label for="" class="labelDisparador">Disparador (Texto que escribe el usuario)</label>
                                                            <input type="text" id="tag_webservice" name="tag_webservice" class="form-control input-sm">
                                                            <span class="info_disparador" class="help-block">Separe los tags con comas.</span>
                                                            <a id="myLink_${noCampo}" href="#" class="pull-right modalDisparador" onclick="abrirDisparadorModal(<?=$huesped?>);return false;">Disparadores de uso frecuente</a>
                                                        </div>
                                                    
                                                        <div class="form-group">
                                                            <label for="">Respuestas (Texto que escribe el bot cuando termine de ejecutar los webservices de esta seccion)</label>
                                                            <textarea id="rpta_accion_webservice" name="rpta_accion_webservice" cols="60" rows="1"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        
                                                        <div class="form-group">
                                                            <label for="">Acción</label>
                                                            <select id="accion_accion_webservice" name="accion_accion_webservice" class="form-control" onchange="cambiarAccionws()">
                                                                <option value="2_1">Transferir a otra sección del bot</option>
                                                                <option value="2_2">Transferir a otro bot</option>
                                                                <option value="1">Transferir a agentes</option>
                                                            </select>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                            <label for="">Detalle de la Acción</label>
                                                            
                                                            <select class="form-control" style="display: none;" id="campan_accion_webservice" name="campan_accion_webservice">
                                                                <?php echo $opcionesCampana ?>
                                                            </select>
                                                            <select class="form-control" id="bot_seccion_accion_webservice" name="bot_seccion_accion_webservice">
                                                                <?php echo $opcionesSecBot ?>
                                                            </select>
                                                            <select class="form-control" style="display: none;" id="bot_accion_webservice" name="bot_accion_webservice">
                                                                <?php echo $opcionesBot ?>
                                                            </select>
                                                        </div>

                                                    </div>

                                                </div>
                                                </div>
                                            </div>
                                        </div>                                        

                                        <div id="accionesBotConversacional">
                                        </div>

                                        <div id="accionesBotTransaccional">
                                        </div>

                                        <div id="accionesBotCapturaDatos">
                                        </div>

                                        <div id="accionesBotConsultaDatos">
                                        </div>

                                        <div>
                                            <button class="btn btn-primary pull-right" type="button" id="agregarFilaCaptura" onclick="agregarFilaTransaccional('captura')" style="display: none; margin:5px;"><i class="fa fa-plus"></i> Agregar Captura de datos</button>
                                        </div>

                                        <div>
                                            <button class="btn btn-primary pull-right" type="button" id="agregarFilaConsulta" onclick="agregarFilaTransaccional('consulta')" style="display: none; margin:5px;"><i class="fa fa-plus"></i> Agregar Consulta de datos</button>
                                        </div>

                                        <div>
                                            <button class="btn btn-primary pull-right" type="button" id="agregarFila"><i class="fa fa-plus"></i> Agregar</button>
                                        </div>

                                    </div>
                                </div>
                            </div> 

                        </div>
                    </div>

                    <!-- Accion bot final -->
                    <div class="panel box box-primary background-blue" id="row_accion_final" style="display:none;margin-top:15px;border: 1px solid #e6e6e6;">
                        <div class="box-header with-border" style="background-color: #009fe3">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#cuerpoAccionFinal" class="text-color-white">
                                    ACCIÓN FINAL – (SOLO SE VA A EJECUTA LA PRIMERA ACCIÓN QUE CUMPLA LA CONDICIÓN)
                                </a>
                            </h4>
                        </div>

                        <div id="cuerpoAccionFinal" class="panel-collapse collapse">
                            <div class="box-body" style="padding: 25px 20px;">

                                <div id="accionesFinales">                                
                                </div>

                                <div id="accionFinalDefecto">
                                </div>

                                <div class="row" style="margin-top: 30px;">
                                    <button type="button" class="btn btn-primary pull-right" onclick="agregarAccionFinal(false, 'add')"><i class="fa fa-plus"></i> Agregar Accion</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-default pull-right cerrar-modal-config" type="button">
                    <?php echo $str_cancela;?>
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para crear solo la seccion -->
<div class="modal fade" id="crearSeccionModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close cerrar-modal-creacion-seccion">&times;</button>
                <h4 class="modal-title">Crear sección bot</h4>
            </div>

            <div class="modal-body">
                <form action="#" method="POST" id="nuevaSeccion">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Nombre de la sección</label>
                                <input type="text" name="newSeccionNombre" id="newSeccionNombre" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Tipo de la sección</label>
                                <select name="newTipoSeccion" id="newTipoSeccion" class="form-control input-sm tipo-seccion">
                                    <option value="1">Conversacional</option>
                                    <option value="23">Transaccional</option>
                                    <option value="2">Captura de datos</option>
                                    <option value="3">Consulta de datos</option>
                                </select>
                                <p class="help-block info_modal"></p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left cerrar-modal-creacion-seccion">
                    <?php echo $str_cancela;?>
                </button>
                <button type="button" class="btn btn-primary pull-right" id="crearSeccion">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="consultaPreguntasModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close cerrar-modal-consulta-preguntas">&times;</button>
                <h4 class="modal-title">Respuestas que el bot no pudo entregar</h4>
            </div>

            <div class="modal-body">
                
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" id="preguntasBotNoEntregaTable">
                            <thead>
                                <tr>
                                    <th>Texto que escribió el cliente</th>
                                    <th>Ultima fecha en que escribieron esto</th>
                                    <th>Cantidad de veces que lo han escrito</th>
                                    <th>Agregar a esta opción</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
                
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left cerrar-modal-consulta-preguntas">
                    <?php echo $str_cancela;?>
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tag globales-->
<div class="modal fade" id="tagsGlobales" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 90%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close cerrar-modal-tags-globales">&times;</button>
                <h4 class="modal-title">Lista de secciones con sus disparadores globales</h4>
            </div>

            <div class="modal-body">
                <form action="#" method="POST" id="tagsGlobalesForm">
                    <!-- <input type="hidden" name="totalCamposComunes" id="totalCamposComunes" value="0"> -->
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table" id="tagsGlobalesTable">
                                <thead>
                                    <tr>
                                        <th>Sección</th>
                                        <th>Disparadores globales (El usuario puede escribir esto en cualquier parte del bot y va a llegar a esta sección)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left cerrar-modal-tags-globales">
                    <?php echo $str_cancela;?>
                </button>
                <button type="button" class="btn btn-primary pull-right" onclick="guardarTagsGlobales()">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tags -->
<div class="modal fade" id="tagsComunesModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close cerrar-modal-tags-comunes">&times;</button>
                <h4 class="modal-title">Disparadores de uso frecuente</h4>
            </div>

            <div class="modal-body">
                <form action="#" method="POST" id="tagsComunesForm">
                    <input type="hidden" name="totalCamposComunes" id="totalCamposComunes" value="0">
                    <div class="row">
                        <div class="col-md-12"   >
                            <table class="table" id="tagsComunesTable">
                                <thead>
                                    <tr>
                                        <th>Tag del disparador</th>
                                        <th>Expresiones que contiene</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <button type="button" class="btn btn-primary btn-sm pull-right" id="addCampotagsComunesbtn">
                                    <i class="fa fa-plus"></i> Agregar campo
                                </button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left cerrar-modal-tags-comunes">
                    <?php echo $str_cancela;?>
                </button>
                <button type="button" class="btn btn-primary pull-right" onclick="guardarTagsComunes()">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Avanzado -->
<div class="modal fade" id="seccionAvanzado" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 90%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close cerrar-modal-seccion-avanzado">&times;</button>
                <h4 class="modal-title">Configuracion avanzada de la accion actual</h4>
            </div>

            <div class="modal-body">
                <form action="#" method="POST" id="formConfiguracionAvanzada">
                    <!-- <input type="hidden" name="totalCamposComunes" id="totalCamposComunes" value="0"> -->
                    <input type="hidden" name="autorespuestaContenidoId" id="autorespuestaContenidoId" value="0">
                    <input type="hidden" name="idVariableDeAccionFinal" id="idVariableDeAccionFinal" value="0">
                    <div class="row">
                        <div class="col-md-12" id="condicionesAcciones">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select name="tipoCondicion" id="tipoCondicion" class="form-control input-sm" onchange="activarCondiciones()">
                                            <option value="0">Sin condiciones</option>
                                            <option value="1">Con condiciones</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button type="button" id="btnAgregarCondicion" onclick="agregarCondicion('add')" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>Agregar Condicion</button>
                                    </div>
                                </div>
                            </div>

                            <div id="grupo_codiciones" style="display: none;">

                                <div class="row titulo-tabla-condiciones">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-3">Campo</div>
                                    <div class="col-md-2">Condicion</div>
                                    <div class="col-md-2">Tipo dato con el que se validara</div>
                                    <div class="col-md-3">Campo con el que se validara</div>
                                    <div class="col-md-1"></div>
                                </div>

                                <div id="cuerpo_condiciones"></div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left cerrar-modal-seccion-avanzado">
                    <?php echo $str_cancela;?>
                </button>
                <button type="button" class="btn btn-primary pull-right" onclick="guardarCondiciones()">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<script src="<?=base_url?>assets/plugins/selectize.js-master/dist/js/standalone/selectize.js"></script>
<script src="<?=base_url?>assets/plugins/tiny/tinymce.min.js"></script>
<script src="<?=base_url?>assets/plugins/tiny/tinymce_languages/langs/es.js"></script>
<script>

//Eventos
$(function(){

    var collapseActive = true;

    var opcionesSecBotGlobales = '';

    var opcionesWebservicesGlobales = '';

    var opcionesCamposGlobales = '<?php echo $opcionesCampos ?>';

    var camposBd = []; // Aqui va los campos de la base datos
    var variablesDinamicasBot = []; // Aqui almaceno las variables dinamicas de la bd
    var variablesDinamicasWs = []; // Aqui almaceno las variables dinamicas de las respuesta del websvice
    var variablesDinamicas = ''; // Aqui ya almaceno formanteado en html las variables dinamicas
    var variablesDinamicasObj = []; // Aqui va el objeto de las variables dinamicas

    var tagsComunes = []; // En esta variable guardo la lista de los tags comunes del huesped
    
    cargarDatos();

    // Agrega las propiedades para poder mover las secciones
    $("#misSeccionesBot").sortable({
        axis: 'y',
        update: function (event, ui) {
            var data = $(this).sortable('serialize');
            $("#txtOrdenData").val(data);
        }
    });

    $("#accionesBotCapturaDatos").sortable({
        axis: 'y',
        start: function(e, ui){
            $(ui.item).find('textarea.myTextarea').each(function(){
                tinymce.execCommand('mceRemoveEditor', false, $(this).attr('id'));
            });
        },
        stop: function(e, ui) {
            $(ui.item).find('textarea.myTextarea').each(function(){
                tinymce.execCommand('mceAddEditor', false, $(this).attr('id'));
            });
        },
        update: function (event, ui) {
            var data = $(this).sortable('serialize');
            $("#txtOrdenAccionesBot").val(data);
        }
    });

    $("#accionesBotTransaccional").sortable({
        axis: 'y',
        start: function(e, ui){
            $(ui.item).find('textarea.myTextarea').each(function(){
                tinymce.execCommand('mceRemoveEditor', false, $(this).attr('id'));
            });
        },
        stop: function(e, ui) {
            $(ui.item).find('textarea.myTextarea').each(function(){
                tinymce.execCommand('mceAddEditor', false, $(this).attr('id'));
            });
        },
        update: function (event, ui) {
            var data = $(this).sortable('serialize');
            $("#txtOrdenAccionesBot").val(data);
        }
    });

    // Eventos jquery
    $(document).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });
    
    // Esto solo cambia el label
    $("#newTipoSeccion").change(function(){
        cambiarLabelInformativo($(this).val());
    });

    // Abre y cierra el todas las modales al tiempo
    $("#toggleAcordeon").click(function(){
        if($('#accionesBotConversacional .panel-collapse.in, #accionesBotCapturaDatos .panel-collapse.in, #accionesBotConsultaDatos .panel-collapse.in').length > 0){
            collapseActive = false;
            $("#accionesBotConversacional .collapse, #accionesBotCapturaDatos .collapse, #accionesBotConsultaDatos .collapse").collapse('hide');
        }else{
            if(collapseActive){
                collapseActive = false;
                $("#accionesBotConversacional .collapse, #accionesBotCapturaDatos .collapse, #accionesBotConsultaDatos .collapse").collapse('hide');
            }else{
                collapseActive = true;
                $("#accionesBotConversacional .collapse, #accionesBotCapturaDatos .collapse, #accionesBotConsultaDatos .collapse").collapse('show');
            }
        }
    });

    // abre la modal para crear una nueva seccion
    $("#agregarSeccion").click(function(){
        
        $("#nuevaSeccion")[0].reset();
        $("#crearSeccionModal").modal();
        $("#newTipoSeccion").change();

    });

    // Crea la seccion de la modal
    $("#crearSeccion").click(function(){
        let valido = true;

        quitarCampoError();

        if($("#newSeccionNombre").val() == ''){
            campoError("newSeccionNombre");
            valido = false;
        }

        if(valido){
            let formData = new FormData($("#nuevaSeccion")[0]);
            formData.append('fraseNoRespuesta', $("#textoNoRespuesta").val());
            $.ajax({
                url: '<?=$url_crud;?>?guardarNuevaSeccion=si&id_paso=<?=$_GET['id_paso']?>&huesped=<?=$huesped?>',  
                type: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend : function(){
                    $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    console.log(data);
                    if(data.res1 && data.res2){
                        $("#crearSeccionModal").modal("hide");
                        agregarFilaSeccion(data.nombre, data.seccionId, data.tipoSeccion, data.baseAutorespuestaId);
                        editarSeccion(data.seccionId, data.baseAutorespuestaId);
                        alertify.success("Seccion creada");
                    }else{
                        alertify.error("Hubo un error al guardar la información de la seccion");
                    }
                },
                error: function(data){
                    if(data.responseText){
                        alertify.error("Hubo un error al guardar la información" + data.responseText);
                    }else{
                        alertify.error("Hubo un error al guardar la información");
                    }
                }
            })
        }
    });

    // Guarda la seccion principal
    $('#Save').click(function(){
    
        let valido = true;

        quitarCampoError();

        if($("#nombre_bot").val() == ''){
            campoError("nombre_bot");
            valido = false;
        }

        if($("#textoNoRespuesta").val() == ''){
            campoError("textoNoRespuesta");
            valido = false;
        }        
        
        if(valido){
            let myOrden = $("#misSeccionesBot").sortable('toArray');

            //Se crean un array con los datos a enviar, apartir del formulario 
            var formData = new FormData($("#FormularioDatos")[0]);
            formData.append('ordenSecciones', myOrden.toString());
            
            $.ajax({
                url: '<?=$url_crud;?>?guardar=si&id_paso=<?=$_GET['id_paso']?>&huesped=<?=$huesped?>',  
                type: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend : function(){
                    $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    alertify.success("Información guardada con exito");
                    location.reload();
                },
                error: function(data){
                    if(data.responseText){
                        alertify.error("Hubo un error al guardar la información" + data.responseText);
                    }else{
                        alertify.error("Hubo un error al guardar la información");
                    }
                }
            })
        }
    });

    // Agrego una nueva fila de campos
    $("#agregarFila").click(function(){
        agregarFilaBot("add");
    });

    // Cierra la modal de configuracion
    $(".cerrar-modal-config").click(function(){
        $("#modalConfiguracionSeccion").modal("hide");
    });

    // Cierra la modal de creacion de seccion
    $(".cerrar-modal-creacion-seccion").click(function(){
        $("#crearSeccionModal").modal("hide");
    });

    $(".cerrar-modal-tags-comunes").click(function(){
        $("#tagsComunesModal").modal("hide");
    });

    $(".cerrar-modal-tags-globales").on('click', function(){
        $('#tagsGlobales').modal('hide');
    });

    $(".cerrar-modal-seccion-avanzado").on('click', function(){
        $('#seccionAvanzado').modal('hide');
    });

    // Guarda las seccion actual
    $("#saveSeccion").click(function(){
        let valido = true;

        if($("#seccionNombre").val() == ''){
            alertify.error('El campo nombre de la seccion no puede estar vacio');
            $("#seccionNombre").focus();
            valido = false;
        }

        if($("#tipoSeccion").val() == 1){
            $('.esteTag').each(function(){
                let esteId = $(this).attr('id');
                if($("#"+esteId).val() == ''){
                    alertify.error('El campo tag no puede estar vacio');
                    valido=false;
                }
            });
        }

        if($("#tipoSeccion").val() == 2){
            $('.pgta').each(function(){
                if($(this).val() == ''){
                    alertify.error('El campo pregunta no puede estar vacio');
                    valido=false;
                }
            });  

            if($("#pregunta_accion_final").val() == ''){
                alertify.error('El campo pregunta de la accion final no puede estar vacio');
                valido=false;
            }

            if(tinymce.get("rpta_accion_final").getContent() == ''){
                alertify.error('El campo respuesta de la accion final no puede estar vacio');
                valido=false;
            }
        }

        if($("#tipoSeccion").val() == 1){
            $('.respuesta').each(function(){
                if(tinymce.get($(this).attr('id')).getContent() == ''){
                    alertify.error('El campo respuesta no puede estar vacio');
                    valido=false;
                }
            });
        }

        if(valido){

            tinymce.triggerSave();

            let myOrden = [];

            if($("#tipoSeccion").val() == 2){
                console.log("captura");
                myOrden = $("#accionesBotCapturaDatos").sortable('toArray');
            }else{
                console.log("transaccional");
                myOrden = $("#accionesBotTransaccional").sortable('toArray');
            }

            var formData = new FormData($("#formularioSeccion")[0]);
            formData.append('ordenSecciones', myOrden.toString());
            formData.append('tipoSeccion1', $("#tipoSeccion").val());

            $.ajax({
                url: '<?=$url_crud;?>?guardar_seccion=si&id_paso=<?=$_GET['id_paso']?>&huesped=<?=$huesped?>',  
                type: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend : function(){
                    $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    // $("#formularioSeccion")[0].reset();
                    alertify.success("Información guardada con exito");
                    // $("#modalConfiguracionSeccion").modal("hide");
                },
                error: function(data){
                    if(data.responseText){
                        alertify.error("Hubo un error al guardar la información" + data.responseText);
                    }else{
                        alertify.error("Hubo un error al guardar la información");
                    }
                }
            })
        }
    });

    $('#accion_accion_final').change(function(){
        
        var campo = $("#accion_accion_final").val();

        $('#campan_accion_final').hide();
        $('#bot_seccion_accion_final').hide();
        $('#bot_accion_final').hide();

        switch ($(this).val()) {
            case "1":
                $('#campan_accion_final').show();
                break;
            case "2_1":
                $('#bot_seccion_accion_final').show();
                break;
            case "2_2":
                $('#bot_accion_final').show();
                break;
            default:
                break;
        }
    });

    $("#addCampotagsComunesbtn").click(function(){
        addCampotagsComunes("add");
    });

    $(".cerrar-modal-consulta-preguntas").click(function(){
        $("#consultaPreguntasModal").modal("hide");
    });

    $("#preguntasBotNoEntregabtn").click(function(){
        $("#consultaPreguntasModal").modal();
        $("#preguntasBotNoEntregaTable tbody").html('');

        let seccion = $("#autorespuestaId").val();

        $.ajax({
            url: '<?=$url_crud;?>?traerPreguntasBotNoResponde=si&id_paso=<?=$_GET['id_paso']?>&huesped=<?=$huesped?>',  
            type: 'POST',
            dataType: 'json',
            data: {
                seccion:seccion
            },
            beforeSend : function(){
                $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
            },
            complete : function(){
                $.unblockUI();
            },
            //una vez finalizado correctamente
            success: function(data){
                if(data){

                    let listaItems = '<option value="">Seleccione</option>';
                    if(data.items.length > 0){
                        $.each(data.items, function(i, item){
                            listaItems += `<option value="${item.id}">${item.pregunta}</option>`;
                        });
                    }
                    
                    if(data.campos.length > 0){
                        $.each(data.campos, function(i, item){
                            let row = `
                                <tr id="tag_row_${item.id}">
                                    <td>${item.pregunta}</td>
                                    <td>${item.fecha_hora}</td>
                                    <td>${item.contador}</td>
                                    <td>
                                        <select id="campo_item_${item.id}" class="form-control input-sm" style="margin-bottom:5px;">
                                            ${listaItems}
                                        </select>
                                        <button type="button" class="btn btn-primary btn-sm" onclick="agregarTagAlItem(${item.id}, '${item.pregunta}')">Agregar</button>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm" onclick="eliminarTagNoDeseado(${item.id})"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            `;
                            $("#preguntasBotNoEntregaTable tbody").append(row);
                        });
                    }
                    
                }
            }
        });
    });

    $('#seccionBd').on('select2:select', function (e) {
        cambiarBd($(this).val());
    });
});

//Funciones

function inicializarTinymce(){
    tinymce.init({
        selector: 'textarea',
        height : 100,
        skin: 'small',
        // icons: 'small',
        menubar: false,
        plugins: 'emoticons, paste',
        toolbar: 'bold italic strikethrough | emoticons | mybutton',
        language: 'es',
        branding: false,
        paste_as_text: true,
        init_instance_callback: function (editor) {
            editor.on('KeyUp', function (e) {
                if($('#tipoSeccion').val() == 2){
                    let identificador = e.target.dataset.id;
                    if(identificador.startsWith('pregunta_bot_')){
                        let texto = e.target.textContent;

                        let num = $("#"+identificador).attr('num');

                        let tituloInsertar = obtenerIconoTitulo()+' '+texto;
                        $('#tituloBot_'+num).html(tituloInsertar.substr(0, 100));
                    }
                }
            });
        },
        setup: function(editor){
            var items = [];

            for (const key in variablesDinamicasObj) {

                let element = {
                    type: 'menuitem',
                    text: variablesDinamicasObj[key].nombre,
                    onAction: function () {
                        editor.insertContent(' '+variablesDinamicasObj[key].valor+' ');
                    }
                };

                items.push(element);
            }           
            
            editor.ui.registry.addMenuButton('mybutton', {
                text: 'Variables dinamicas',
                fetch: function (callback) {
                    callback(items);
                }
            });
        }
    });
}

function inicializarSelectizeTags(id){
    $('#tag_'+id).selectize({
        plugins: ['remove_button'],
        persist: false,
        createOnBlur: true,
        create: true,
        render:{
            item: function(data) {
                if(data.value.startsWith('#')){

                    if(tagsComunes.length > 0){
                        if(tagsComunes.includes(data.value)){
                            return `<div class="item c-tag-${id} green" data-value="${data.value}" onclick="abrirDisparadorModal(<?=$huesped?>, '${data.value}')">${data.value}</div>`;
                        }
                    }
                    
                }
                
                return `<div class="item c-tag-${id}" data-value="${data.value}">${data.value}</div>`;
                
            }
        },
        onItemAdd: function (value) {

            // Validar si el item ya existe en una de las listas
            if(validarTagsUnicosEnSeccion(value)){

                // Agregar el item a la lista
                let newTitulo = obtenerIconoTitulo()+' '+$('#tag_'+id).val()+ ','+value;
                if($('#tag_'+id).val() == ''){
                    newTitulo = obtenerIconoTitulo()+' '+value;
                }
                
                $('#tituloBot_'+id).html(newTitulo.substr(0, 150));

            }else{
                // Me toca darle un delay de 1seg para que le tome tiempo y ejecute el evento addItem
                setTimeout(function () {
                    limpiarTagsRepetidos(id, value);
                }, 1000);
                alertify.error("Este tag ya se encuentra agregado en otro campo de disparador");
            }
            
            
        },
        onItemRemove: function(){
            let newTitulo = obtenerIconoTitulo()+' '+$('#tag_'+id).val();
            $('#tituloBot_'+id).html(newTitulo.substr(0, 150));
        }
    });
}

function obtenerIconoTitulo(){

    let tipoSeccion = $("#tipoSeccion").val();

    let icono = '<i class="fa fa-comments-o"></i>';

    switch (tipoSeccion) {
        case '1':
            icono = '<i class="fa fa-comments-o"></i>';
            break;

        case '2':
            icono = '<i class="fa fa-arrows-v"></i>';
            break;

        case '3':
            icono = '<i class="fa fa-plug"></i>';
            break;
    
        default:
            break;
    }

    return icono;
}

function campoError(input, mensajeError = null){
    $("#"+input).addClass("error-input");
    $("#"+input).focus();

    let mensaje = "Este campo es obligatorio";

    if(mensajeError){
        mensaje = mensajeError;
    }
    alertify.error(mensaje);
}

function quitarCampoError(){
    $(".error-input").removeClass("error-input");
}

function obtenerNombreTipoSeccionEIcono(tipoSeccion){
    
    let nombre = '';

    switch (tipoSeccion) {
        case "1":
            nombre = 'Conversacional <i class="fa fa-comments-o"></i>';
            break;
        case "2":
            nombre = 'Captura de datos <i class="fa fa-list-ul"></i>';
            break;
        case "3":
            nombre = 'Consulta de datos <i class="fa fa-clone"></i>';
            break;
        
        case "23":
            nombre = 'Transaccional <i class="fa fa-list-ol"></i>';
            break;
        default:
            nombre = "Sin seccion";
            break;
    }

    return nombre;
}

// Carga los datos del crud de la bola
function cargarDatos(){
    
    $.ajax({
        url: '<?=$url_crud;?>?getdatos=si&id_paso=<?=$_GET['id_paso']?>&huesped=<?=$huesped?>',  
        type: 'POST',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend : function(){
            $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
        },
        complete : function(){
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function(data){
            if(data){
                $("#totalFilas").val(0);
                // Traigo los datos del paso
                if(data.dataPaso){
                    if(data.dataPaso.nombre != ''){
                        $("#nombre_bot").val(data.dataPaso.nombre);
                    }else{
                        $("#nombre_bot").val('BOT_'+data.dataPaso.id);
                    }
                    
                    if(data.dataPaso.activo != '-1'){
                        $('#pasoActivo').prop('checked',false);
                    }
                    
                    $("#pasoId").val(data.dataPaso.id); 
                }

                // Aqui traigo la lista de secciones
                if(data.secciones.length > 0){

                    data.secciones.forEach( item => {

                        if(item.orden == 1){
                            $("#seccionPrincipal").val(item.id);
                            $("#autorespuestaPrincipal").val(item.autorespuestaId);

                            document.getElementById('editarSeccionBienvenida').setAttribute('onclick',`editarSeccion(${item.id},${item.autorespuestaId})`);
                            
                        }else if(item.orden == data.secciones.length){
                            document.getElementById('editarSeccionDespedida').setAttribute('onclick',`editarSeccion(${item.id},${item.autorespuestaId})`);
                        }else{
                            agregarFilaSeccion(item.nombre, item.id, item.tipo_seccion, item.autorespuestaId);
                        }
                        $("#textoNoRespuesta").val(item.frase_no_encuentra_respuesta);
                    });

                }

                // Traigo la lista de bases de datos
                let listaBd = '<option value="">Seleccione</option>';

                if(data.listaBd){
                    $.each(data.listaBd, function(i, item){
                        listaBd += `<option value="${item.id}">${item.nombre}</option>`;
                    });

                    $("#seccionBd").html(listaBd);
                    $("#seccionBd").select2({
                        templateResult: function(data) {
                            var r = data.text.split('|');
                            var $result = $(
                                '<div class="row">' +
                                    
                                    '<div class="col-md-12">' + r[0] + '</div>' +
                                '</div>'
                            );
                            return $result;
                        },
                        templateSelection : function(data){
                            var r = data.text.split('|');
                            return r[0];
                        }
                    });
                }
            }
        }
    });
}

function agregarFilaSeccion(nombre, seccionId, tipoSeccion, baseAutoresId){
    let noFilas = $("#totalFilas").val();

    let nombreTipoSeccion = obtenerNombreTipoSeccionEIcono(tipoSeccion);

    let row = `
    <div class="panel box box-primary row-seccion" id="botSeccion_${seccionId}" style="position: relative; top: 0px; left: 0px;">
        <div class="box-header with-border">
            <h4 class="box-title" id="titulo_${noFilas}">
                <i class="fa fa-arrows-v"></i>&nbsp; <div class="nombre-seccion" style="display: inline;">${nombre} - Tipo ${nombreTipoSeccion}</div>
            </h4>
            
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" onclick="editarSeccion(${seccionId}, ${baseAutoresId})"><i class="fa fa-edit"></i></button>
                <button type="button" class="btn btn-danger btn-sm" onclick="eliminarSeccion(${seccionId}, ${baseAutoresId}, ${tipoSeccion})"><i class="fa fa-trash-o"></i></button>
            </div>
        </div>
    </div>
    `;

    $("#misSeccionesBot").append(row);

    noFilas++;
    $("#totalFilas").val(noFilas);
}

function eliminarSeccion(seccionId, baseAutorespuestaId, tipoSeccion){

    let mensaje = "¿Estás seguro de borrar esta sección?, Si la eliminas ten en cuenta que se perderán las configuraciones y las conexiones asociadas a esta sección.";

    alertify.confirm(mensaje, function (e) {
        if(e){
            $.ajax({
                url: '<?=$url_crud;?>?borrar_seccion=si',  
                type: 'POST',
                dataType: 'json',
                data: { seccionId:seccionId, autorespuestaId:baseAutorespuestaId, tipoSeccion:tipoSeccion },
                beforeSend : function(){
                    $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    $("#botSeccion_"+seccionId).remove();
                    alertify.success("Seccion eliminada");
                }
            });     
        }
    });
}

function editarSeccion(seccionId, autorespuestaId){
    
    $("#formularioSeccion")[0].reset();
    
    $("#accionesBotConversacional").html('');
    $("#accionesBotTransaccional").html('');

    $("#accionesBotCapturaDatos").html('');
    $("#accionesBotConsultaDatos").html('');

    $("#accionesFinales").html('');
    $("#accionFinalDefecto").html('');

    $("#modalConfiguracionSeccion").modal();

    $("#tipoSeccion").change();

    $("#totalFilasSeccion").val(0);
    $("#totalAccionesFinales").val(0);

    obtenerTagsComunes();

    // traigo los datos de la seccion
    $.ajax({
        url: '<?=$url_crud;?>?getdatosSeccion=si&id_paso=<?=$_GET['id_paso']?>&huesped=<?=$huesped?>&seccionId='+seccionId+'&autorespuestaId='+autorespuestaId,  
        type: 'POST',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend : function(){
            $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
        },
        complete : function(){
            $.unblockUI();
        },
        success: function(data){
            if(data){
                
                if(data.seccion){
                    $("#seccionNombre").val(data.seccion.nombre);
                    $("#tipoSeccion").val(data.seccion.tipo_seccion);
                    $("#seccionBotId").val(seccionId);
                    $("#autorespuestaId").val(autorespuestaId);

                    cambiarLabelInformativo(data.seccion.tipo_seccion);   
                }

                if(data.camposBd){
                    camposBd = data.camposBd;
                }

                // Aqui traigo las variables dinamicas bot             
                if(data.variablesBot){
                    variablesDinamicasBot = data.variablesBot;
                }

                // Aqui traigo las variables dinamicas ws
                if(data.variablesWs){
                    variablesDinamicasWs = data.variablesWs;
                }

                opcionesSecBotGlobales = '<option value="">Seleccione</option>';

                if(data.listaSeccionesBot){
                    $.each(data.listaSeccionesBot, function(i, item){
                        opcionesSecBotGlobales += `<option value="${item.base_autorespuesta_id}">${item.nombre}</option>`;
                    });
                }

                formatearVariablesDinamicas(variablesDinamicasBot, variablesDinamicasWs, data.listaSeccionesBot);

                inicializarTinymce();

                opcionesCamposGlobales = '<?php echo $opcionesCampos ?>';

                $("#row_accion_webservice").hide();
                inicializarSelectizeTags('webservice');
                $("#tag_webservice")[0].selectize.destroy();

                inicializarSelectizeTags('local');
                $("#tag_local")[0].selectize.destroy();
                $("#tag_local").val('');

                $("#agregarFilaCaptura").hide();
                $("#agregarFilaConsulta").hide();
                $("#agregarFila").hide();

                switch (data.seccion.tipo_seccion) {
                    case '1': // Conversacional

                        $("#agregarFila").show();

                        // Inicialize local tags
                        inicializarSelectizeTags('local');
                        tinymce.get("rpta_accion_inicial").setContent('');

                        // hide final action
                        $("#row_accion_final").hide();
                        $("#groupSeccionBd").hide();
                        $("#row_accion_inicial").show();

                        if(data.campos.length > 0){
                            data.campos.forEach( (item, i) => {

                                if(item.orden == 1){ // if action is initial
                                    
                                    $("#tag_local")[0].selectize.destroy();
                                    $("#tag_local").val(item.pregunta);
                                    inicializarSelectizeTags('local');

                                    
                                    $("#rpta_accion_inicial").val(item.respuesta); // Lo inicializo por aca
                                    tinymce.get("rpta_accion_inicial").setContent(item.respuesta);

                                }else{
                                    agregarFilaBot('edit');
                                    
                                    $("#tag_"+i)[0].selectize.destroy();
                                    $("#tag_"+i).val(item.pregunta);
                                    inicializarSelectizeTags(i);
    
                                    $("#campo_"+i).val(item.id);
    
                                    let tituloInsertar = obtenerIconoTitulo()+' '+$('#tag_'+i).val();
    
                                    $('#tituloBot_'+i).html(tituloInsertar.substr(0, 150));
                                    
                                    $("#rpta_"+i).val(item.respuesta); // Lo inicializo por aca
                                    tinymce.get("rpta_"+i).setContent(item.respuesta);
    
                                    if(item.accion == 2){
                                        if ( $("#bot_"+i+" option[value="+item.id_base_transferencia+"]").length > 0 ) {
                                            $("#accion_"+i).val("2_2").change();
                                            $("#bot_"+i).val(item.id_base_transferencia).trigger('change');
                                        }else{
                                            $("#accion_"+i).val("2_1").change();
                                            $("#bot_seccion_"+i).val(item.id_base_transferencia).trigger('change');
                                        }
                                    }else{
                                        $("#accion_"+i).val(item.accion).change();
                                    }
                                    
                                    $("#campan_"+i).val(item.id_campana).trigger('change');
                                    $("#pregun_"+i).val(item.id_pregun).trigger('change');
                                }

                            });
                        }

                        break;
                    
                    case '2':

                        $("#agregarFila").show();

                        // show final action
                        $("#row_accion_final").show();
                        $("#groupSeccionBd").show();
                        $("#row_accion_inicial").hide();

                        if(data.campos.length > 0){
                            data.campos.forEach( (item, i) => {
                                
                                agregarFilaBot('edit');

                                $("#pregunta_bot_"+i).val(item.pregunta);
                                if(tinymce.get("pregunta_bot_"+i) !== null){
                                    tinymce.get("pregunta_bot_"+i).setContent(item.pregunta);
                                }

                                let textoPregunta = $('#pregunta_bot_'+i).val();
                                textoPregunta = textoPregunta.replace(/(<([^>]+)>)/ig, '');

                                let tituloInsertar = obtenerIconoTitulo()+' '+textoPregunta;
                            
                                $('#tituloBot_'+i).html(tituloInsertar.substr(0, 150));

                                $("#campo_"+i).val(item.id);

                                $("#rpta_"+i).val(item.respuesta); // Lo inicializo por aca
                                if(tinymce.get("rpta_"+i) !== null){
                                    tinymce.get("rpta_"+i).setContent(item.respuesta);
                                }

                                if(item.accion == 2){
                                    if ( $("#bot_"+i+" option[value="+item.id_base_transferencia+"]").length > 0 ) {
                                        $("#accion_"+i).val("2_2").change();
                                        $("#bot_"+i).val(item.id_base_transferencia).trigger('change');
                                    }else{
                                        $("#accion_"+i).val("2_1").change();
                                        $("#bot_seccion_"+i).val(item.id_base_transferencia).trigger('change');
                                    }
                                }

                                $("#campan_"+i).val(item.id_campana).trigger('change');
                                
                                $("#pregun_"+i).val(item.id_pregun).trigger('change');
                                
                            });
                        }

                        if(data.camposAccionesFinales.length > 0){

                            tamano = data.camposAccionesFinales.length;
                            
                            data.camposAccionesFinales.forEach( (item, i) => {

                                let contador = $("#totalAccionesFinales").val();

                                if(i == tamano - 1){
                                    agregarAccionFinal(true, item.condicion, 'edit');
                                }else{
                                    agregarAccionFinal(false, item.condicion, 'edit');
                                }

                                $("#campo_accion_final_"+contador).val(item.id);

                                $("#rpta_accion_final_"+contador).val(item.respuesta);
                                if(tinymce.get("rpta_accion_final_"+contador) !== null){
                                    tinymce.get("rpta_accion_final_"+contador).setContent(item.respuesta);
                                }

                                if(item.accion == 2){
                                    if ( $("#bot_accion_final_"+contador+" option[value="+item.id_base_transferencia+"]").length > 0 ) {
                                        $("#accion_accion_final_"+contador).val("2_2").change();
                                        $("#bot_accion_final_"+contador).val(item.id_base_transferencia).trigger('change');
                                    }else{
                                        $("#accion_accion_final_"+contador).val("2_1").change();
                                        $("#bot_seccion_accion_final_"+contador).val(item.id_base_transferencia).trigger('change');
                                    }
                                }
                                $("#campan_accion_final_"+contador).val(item.id_campana).trigger('change');
                            });
                        }

                        if(data.camposAccionesFinales.length > 1){
                            let id = data.camposAccionesFinales.length - 1;
                            let nombreAccionFinal = `<i class="fa fa-comments-o"></i>&nbsp;Acción cuando ninguna de las condiciones de las acciones anteriores se cumplió`;
                            $("#tituloAccionFinal"+id).html(nombreAccionFinal);
                        }


                        break;  
                    
                    case '23': // Transaccional
                        
                        $("#row_accion_final").show();
                        $("#groupSeccionBd").show();
                        $("#row_accion_inicial").hide();

                        $("#agregarFilaCaptura").show();
                        $("#agregarFilaConsulta").show();

                        // LLeno en una variable la lista de webservices
                        opcionesWebservicesGlobales = '<option value="">Seleccione</option>';
                        if(data.webservices){
                            $.each(data.webservices, function(i, item){
                                opcionesWebservicesGlobales += `<option value="${item.id}">${item.nombre}</option>`;
                            });
                        }

                        break;
                    
                    default:
                        break;
                }

                if(data.seccion.tipo_seccion == 3){

                    $("#agregarFila").show();

                    // $("#tag_webservice")[0].selectize.destroy();
                    // $("#tag_"+i).val(item.pregunta);
                    inicializarSelectizeTags('webservice');

                    // Muestro la fila de la accion que ejecuta el webservice
                    $("#row_accion_webservice").show();

                    // LLeno en una variable la lista de webservices
                    opcionesWebservicesGlobales = '<option value="">Seleccione</option>';
                    if(data.webservices){
                        $.each(data.webservices, function(i, item){
                            opcionesWebservicesGlobales += `<option value="${item.id}">${item.nombre}</option>`;
                        });
                    }

                    // Obtengo las filas de las secciones, los webservices
                    if(data.wsBotSecciones){

                        $.each(data.wsBotSecciones, function(i, item){
                            agregarFilaBot('edit');

                            $("#campo_"+i).val(item.id);
                            $("#webservice_"+i).val(item.id_ws_general);
                            $("#webservice_"+i).data("oldvalue", item.id_ws_general);

                            let texto = $("#webservice_"+i+" option:selected").text();
                            $("#tituloService_"+i).html(obtenerIconoTitulo()+'&nbsp;'+texto);

                            // Ahora traigo los parametros de cada webservice

                            let htmlParamsBody = '';

                            // Armo la estructura 
                            $.each(data.wsParametrosSeccion[item.id], function(j, itemP){
                                htmlParamsBody += `
                                <tr>
                                    <input type="hidden" name="parametro_${i}_${itemP.id_parametro}" id="parametro_${i}_${itemP.id_parametro}">
                                    <td>${itemP.parametro}</td>
                                    <td>
                                        <select name="tipoParametro_${i}_${itemP.id_parametro}" id="tipoParametro_${i}_${itemP.id_parametro}" class="form-control input-sm" onchange="cambioTipoValor(${itemP.id_parametro}, ${i})">
                                            <option value="1">Estatico</option>
                                            <option value="2">Dinamico</option>
                                            <option value="3">Combinado</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="valorEstatico_${i}_${itemP.id_parametro}" id="valorEstatico_${i}_${itemP.id_parametro}" class="form-control input-sm">

                                        <select name="valorDinamico_${i}_${itemP.id_parametro}" id="valorDinamico_${i}_${itemP.id_parametro}" class="form-control input-sm" style="display:none">
                                            ${variablesDinamicas}
                                        </select> 
                                    </td>
                                </tr>
                                `;
                            });

                            // Armo la estructura de los parametros
                            let htmlParamsEstructura = `
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Nombre del parametro</th>
                                            <th>Tipo de valor</th>
                                            <th>Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ${htmlParamsBody}
                                    </tbody>
                                </table>
                            `;

                            $('#webserviceParametros_'+i).html(htmlParamsEstructura);

                            // Lleno los datos
                            $.each(data.wsParametrosSeccion[item.id], function(k, itemK){
                                $("#parametro_"+i+"_"+itemK.id_parametro).val(itemK.id);
                                $("#tipoParametro_"+i+"_"+itemK.id_parametro).val(itemK.tipo_valor);

                                if(itemK.tipo_valor == 1){
                                    $("#valorEstatico_"+i+"_"+itemK.id_parametro).val(itemK.valor);
                                }else{
                                    $("#valorDinamico_"+i+"_"+itemK.id_parametro).val(itemK.valor);
                                }

                                $("#tipoParametro_"+i+"_"+itemK.id_parametro).change();
                            });

                        });
                        
                    }

                    // Aqui obtengo los datos de la accion de la seccion
                    if(data.wsAccion){

                        $("#tag_webservice")[0].selectize.destroy();
                        $("#tag_webservice").val(data.wsAccion.pregunta);
                        inicializarSelectizeTags('webservice');

                        $("#campo_accion_webservice").val(data.wsAccion.id);
                        
                        $("#rpta_accion_webservice").val(data.wsAccion.respuesta); // Lo inicializo por aca
                        tinymce.get("rpta_accion_webservice").setContent(data.wsAccion.respuesta);                        
                    
                        if(data.wsAccion.accion == 2){
                            if ( $("#bot_accion_webservice option[value="+data.wsAccion.id_base_transferencia+"]").length > 0 ) {
                                $("#accion_accion_webservice").val("2_2").change();
                                $("#bot_accion_webservice").val(data.wsAccion.id_base_transferencia).trigger('change');
                            }else{
                                $("#accion_accion_webservice").val("2_1").change();
                                $("#bot_seccion_accion_webservice").val(data.wsAccion.id_base_transferencia).trigger('change');
                            }
                        }else{
                            $("#accion_"+i).val(data.wsAccion.accion).change();
                        }
                        
                        $("#campan_accion_webservice").val(data.wsAccion.id_campana).trigger('change');
                    }
                }

            }
        }
    });
}

function formatearVariablesDinamicas(variablesDinamicasBot, variablesDinamicasWs, listaSecciones){
    let opciones = '<option value="">Seleccione</option>';
    let varDinamicasObj = [];

    if(listaSecciones.length > 0){

        listaSecciones.forEach(element => {

            let nombre = '';
            let valor = '';

            if(element.tipo_base == 2){
                opciones += `<optgroup label="${element.nombre}">`;

                opciones += variablesDinamicasBot.map(variable => {
                    if(variable.autoresId == element.base_autorespuesta_id){

                        nombre = '${'+variable.variable+'}';
                        valor = '${'+variable.variable+'}';

                        varDinamicasObj.push({"nombre": nombre, "valor": valor, "grupo":element.nombre, "valor2": variable.id_pregun});
                        return `<option value="${valor}">${nombre}</option>`;
                    }
                });

                opciones += '</optgroup>';
            }
            
            if(element.tipo_base == 3){
                opciones += `<optgroup label="${element.nombre}">`;

                opciones += variablesDinamicasWs.map(variable => {

                    if(variable.autoresId == element.base_autorespuesta_id){
                        
                        nombre = '${'+variable.nombreService.toUpperCase()+'.'+variable.variable+'}';
                        valor = '${'+variable.seccion.toUpperCase()+'.'+variable.variable+'}';

                        varDinamicasObj.push({"nombre": nombre, "valor": valor, "grupo":element.nombre, "valor2": valor});
                        return `<option value="${valor}">${nombre}</option>`;
                    }
                });

                opciones += '</optgroup>';
            }

        });

    }

    variablesDinamicasObj = varDinamicasObj;
    variablesDinamicas = opciones;
}

function agregarFilaBot(oper, tipo = null){

    let noCampo = $("#totalFilasSeccion").val();

    let opcionesCampana = '<?php echo $opcionesCampana ?>';
    let opcionesSecBot = '<?php echo $opcionesSecBot ?>';
    let opcionesBot = '<?php echo $opcionesBot ?>';
    let opcionesCampos = opcionesCamposGlobales;

    let tipoSeccion = $("#tipoSeccion").val();

    let labelDetalleAccion = 'Detalle de la Acción';
    let labelRespuesta = 'Respuestas (Texto que escribe el bot cuando se ejecuta la accion)';
    if(tipoSeccion == 2){
        labelDetalleAccion = 'Campo de destino.';
        labelRespuesta = 'Respuestas (Texto opcional que puede escribir el bot despues de que el usuario registra el dato)';
    }

    let row = '';

    let num = noCampo;

    if(tipo != null){
        tipoSeccion = tipo;
    }

    switch (tipoSeccion) {
        case "1":
            // Conversacional

            row = agregarPlantillaConversacional(oper, noCampo, labelDetalleAccion, labelRespuesta, opcionesCampana, opcionesSecBot, opcionesBot, opcionesCampos);

            if(tipo == null){
                $("#accionesBotConversacional").append(row);
            }else{
                $("#accionesBotTransaccional").append(row);
            }

            inicializarSelectizeTags(num);

            // $("#accion_"+noCampo+" option[value=3]").hide();

            // Inicializo los textarea respuesta
            tinymce.execCommand('mceRemoveEditor', false, 'rpta_'+noCampo);
            tinymce.execCommand('mceAddEditor', false, 'rpta_'+noCampo);

            break;

        case "2":
            // Captura de datos

            row = agregarPlantillaConversacionalCaptura(oper, noCampo, labelDetalleAccion, labelRespuesta, opcionesCampana, opcionesSecBot, opcionesBot, opcionesCampos);

            if(tipo == null){
                $("#accionesBotCapturaDatos").append(row);
            }else{
                $("#accionesBotTransaccional").append(row);
            }

            $('#tag_'+noCampo).hide();
            $(".info_disparador").hide();
            $(".modalDisparador").hide();


            $(".labelDisparador").html("Pregunta que el bot le hace al usuario");
            $("#pregunta_bot_"+noCampo).show();

            $("#accion_"+noCampo+" option[value=0]").hide();
            $("#accion_"+noCampo+" option[value=1]").hide();
            $("#accion_"+noCampo+" option[value=2_1]").hide();
            $("#accion_"+noCampo+" option[value=2_2]").hide();
            $("#accion_"+noCampo+" option[value=3]").show();

            $("#accion_"+noCampo).val(3);
            $('#dar_respuesta_'+noCampo).hide();
            $('#pregun_'+noCampo).show();

            $("#pregunta_bot_"+num).keyup(function(){
                let tituloInsertar = obtenerIconoTitulo()+' '+$(this).val();
                $('#tituloBot_'+num).html(tituloInsertar.substr(0, 150));
            });

            // Oculto el campo de accion dado que por defecto es solo dar respuesta
            $("#div_accion_"+noCampo).hide();
            // Muestro el campo de la base de datos
            $("#div_base_"+noCampo).show();

            // Inicializo los textarea de pregunta
            tinymce.execCommand('mceRemoveEditor', false, 'pregunta_bot_'+noCampo);
            tinymce.execCommand('mceAddEditor', false, 'pregunta_bot_'+noCampo);

            // Inicializo los textarea respuesta
            tinymce.execCommand('mceRemoveEditor', false, 'rpta_'+noCampo);
            tinymce.execCommand('mceAddEditor', false, 'rpta_'+noCampo);

            break;

        case "3":
            // Consulta de datos
            
            row = agregarPlantillaConsultaDatos(oper, noCampo);

            if(tipo == null){
                $("#accionesBotConsultaDatos").append(row);
            }else{
                $("#accionesBotTransaccional").append(row);
            }

            break;
    
        default:
            break;
    }

    noCampo++;
    $("#totalFilasSeccion").val(noCampo);
}

function agregarPlantillaConversacional(oper, noCampo, labelDetalleAccion, labelRespuesta, opcionesCampana, opcionesSecBot, opcionesBot, opcionesCampos){

    let row = `
        <div class="panel box box-success" id="row_${oper}_${noCampo}">
            <div class="box-header with-border">
                <h4 class="box-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#cuerpoBot_${noCampo}" id="tituloBot_${noCampo}">
                        ${obtenerIconoTitulo()}&nbsp;Titulo
                    </a>
                </h4>

                <div class="box-tools pull-right" data-toggle="tooltip" title="Avanzado">
                    <button type="button" id="btnAdvancedEditField_${noCampo}" class="btn btn-warning btn-sm" onclick="seccionAvanzado(${noCampo}, 'conversacional')"><i class="fa fa-cog"></i></button>
                    <button type="button" id="btnEliminarField_${noCampo}" onclick="eliminarRowBot('${oper}', ${noCampo})" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                </div>
            </div>

            <div id="cuerpoBot_${noCampo}" class="panel-collapse collapse in">
                <div class="box-body">
                <div class="row">
                    <input type="hidden" id="campo_${noCampo}" name="campo_${oper}_${noCampo}" value="" num="${noCampo}">

                    <div class="col-md-10">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="labelDisparador">Disparador (Texto que escribe el usuario)</label>
                                <input type="text" id="tag_${noCampo}" name="tag_${oper}_${noCampo}" class="form-control input-sm tags-input esteTag" num="${noCampo}">
                                <span class="info_disparador" class="help-block">Separe los tags con comas.</span>
                                <a id="myLink_${noCampo}" href="#" class="pull-right modalDisparador" onclick="abrirDisparadorModal(<?=$huesped?>);return false;">Disparadores de uso frecuente</a>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">${labelRespuesta}</label>
                                <textarea class="myTextarea tinyMCE respuesta" id="rpta_${noCampo}" name="rpta_${oper}_${noCampo}" cols="60" rows="1" num="${noCampo}"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2" style="padding-left: 0px">
                        <div class="col-md-12" style="padding-left: 0px">
                            <div class="form-group" id="div_accion_${noCampo}">
                                <label for="">Acción</label>
                                <select id="accion_${noCampo}" name="accion_${oper}_${noCampo}" class="form-control accion" onchange="cambiarAccion(${noCampo})">
                                    <option value="0">Dar respuesta</option>
                                    <option value="2_1">Transferir a otra sección del bot</option>
                                    <option value="2_2">Transferir a otro bot</option>
                                    <option value="1">Transferir a agentes</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-12" style="padding-left: 0px">
                            <div class="form-group">
                                <label for="">${labelDetalleAccion}</label>
                                <select class="form-control" id="dar_respuesta_${noCampo}" name="dar_respuesta_${oper}_${noCampo}" disabled num="${noCampo}">
                                    <option value="0">Seleccione</option>
                                </select>
                                <select class="form-control" style="display: none;" id="campan_${noCampo}" name="campan_${oper}_${noCampo}" num="${noCampo}">
                                    ${opcionesCampana}
                                </select>
                                <select class="form-control" style="display: none;" id="bot_seccion_${noCampo}" name="bot_seccion_${oper}_${noCampo}" num="${noCampo}">
                                    ${opcionesSecBotGlobales}
                                </select>
                                <select class="form-control" style="display: none;" id="bot_${noCampo}" name="bot_${oper}_${noCampo}" num="${noCampo}">
                                    ${opcionesBot}
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12" style="padding-left: 0px">
                            <button type="button" class="btn btn-primary" onclick="cambiarSeccion(${noCampo})">Ir a la seccion</button>
                        </div>
                    </div>

                </div>
                </div>
            </div>
        </div>
    `;

    return row;
}

function agregarPlantillaConversacionalCaptura(oper, noCampo, labelDetalleAccion, labelRespuesta, opcionesCampana, opcionesSecBot, opcionesBot, opcionesCampos){
    
    let row = `
        <div class="panel box box-success" id="row_${oper}_${noCampo}">
            <div class="box-header with-border">
                <h4 class="box-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#cuerpoBot_${noCampo}" id="tituloBot_${noCampo}">
                        ${obtenerIconoTitulo()}&nbsp;Titulo
                    </a>
                </h4>

                <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                    <button type="button" id="btnAdvancedEditField_${noCampo}" class="btn btn-warning btn-sm"><i class="fa fa-cog"></i></button>
                    <button type="button" id="btnEliminarField_${noCampo}" onclick="eliminarRowBot('${oper}', ${noCampo})" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                </div>
            </div>

            <div id="cuerpoBot_${noCampo}" class="panel-collapse collapse in">
                <div class="box-body">
                <div class="row">
                    <input type="hidden" id="campo_${noCampo}" name="campo_${oper}_${noCampo}" value="" num="${noCampo}">

                    <div class="col-md-10">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="labelDisparador">Disparador (Texto que escribe el usuario)</label>
                                <input type="text" id="tag_${noCampo}" name="tag_${oper}_${noCampo}" class="form-control input-sm tags-input esteTag" num="${noCampo}">
                                <textarea id="pregunta_bot_${noCampo}" name="pregunta_bot_${oper}_${noCampo}" class="form-control input-sm preg myTextarea tinyMCE" num="${noCampo}" style="display: none;"></textarea>
                                <span class="info_disparador" class="help-block">Separe los tags con comas.</span>
                                <a id="myLink_${noCampo}" href="#" class="pull-right modalDisparador" onclick="abrirDisparadorModal(<?=$huesped?>);return false;">Disparadores de uso frecuente</a>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">${labelRespuesta}</label>
                                <textarea class="myTextarea tinyMCE respuesta" id="rpta_${noCampo}" name="rpta_${oper}_${noCampo}" cols="60" rows="1" num="${noCampo}"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2" style="padding-left: 0px">
                        <div class="col-md-12" style="padding-left: 0px">
                            <div class="form-group" id="div_accion_${noCampo}">
                                <label for="">Acción</label>
                                <select id="accion_${noCampo}" name="accion_${oper}_${noCampo}" class="form-control accion" num="${noCampo}">
                                    <option value="0">Dar respuesta</option>
                                    <option value="2_1">Transferir a otra sección del bot</option>
                                    <option value="2_2">Transferir a otro bot</option>
                                    <option value="1">Transferir a agentes</option>
                                    <option value="3">Pedirle dato al cliente</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-12" style="padding-left: 0px">
                            <div class="form-group">
                                <label for="">${labelDetalleAccion}</label>
                                <select class="form-control" id="dar_respuesta_${noCampo}" name="dar_respuesta_${oper}_${noCampo}" disabled num="${noCampo}">
                                    <option value="0">Seleccione</option>
                                </select>
                                <select class="form-control" style="display: none;" id="campan_${noCampo}" name="campan_${oper}_${noCampo}" num="${noCampo}">
                                    ${opcionesCampana}
                                </select>
                                <select class="form-control" style="display: none;" id="bot_seccion_${noCampo}" name="bot_seccion_${oper}_${noCampo}" num="${noCampo}">
                                    ${opcionesSecBotGlobales}
                                </select>
                                <select class="form-control" style="display: none;" id="bot_${noCampo}" name="bot_${oper}_${noCampo}" num="${noCampo}">
                                    ${opcionesBot}
                                </select>
                                <select class="form-control campos-pregunta" style="display: none;" id="pregun_${noCampo}" name="pregun_${oper}_${noCampo}" num="${noCampo}">
                                    ${opcionesCampos}
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12" style="padding-left: 0px">
                            <button type="button" class="btn btn-primary" style="display:none" onclick="cambiarSeccion(${noCampo})">Ir a la seccion</button>
                        </div>
                    </div>

                </div>
                </div>
            </div>
        </div>
    `;

    return row;
}

function agregarPlantillaConsultaDatos(oper, noCampo){

    let row = `
        <div class="panel box box-success" id="row_${oper}_${noCampo}">
            <div class="box-header with-border">
                <h4 class="box-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#cuerpoService_${noCampo}" id="tituloService_${noCampo}">
                        ${obtenerIconoTitulo()}&nbsp;Titulo
                    </a>
                </h4>

                <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                    <button type="button" id="btnAdvancedEditField_${noCampo}" class="btn btn-warning btn-sm"><i class="fa fa-cog"></i></button>
                    <button type="button" id="btnEliminarFieldService_${noCampo}" onclick="eliminarRowWS('${oper}', ${noCampo})" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                </div>
            </div>

            <div id="cuerpoService_${noCampo}" class="panel-collapse collapse in">
                <div class="box-body">
                    <div class="row">
                        <input type="hidden" id="campo_${noCampo}" name="campo_${oper}_${noCampo}" value="" num="${noCampo}">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="">WebService</label>
                                <select name="webservice_${oper}_${noCampo}" id="webservice_${noCampo}" class="form-control input-sm" onchange="cambiows(${noCampo})" data-oldvalue="0">
                                    ${opcionesWebservicesGlobales}
                                </select>
                            </div>
                        </div>

                        <div id="webserviceParametros_${noCampo}"></div>

                    </div>
                </div>
            </div>

        </div>
    `;

    return row;
}

function cambiarAccion(campo){

    let valorActual = $("#accion_"+campo).val();
    
    $('#dar_respuesta_'+campo).hide();
    $('#campan_'+campo).hide();
    $('#bot_seccion_'+campo).hide();
    $('#bot_'+campo).hide();
    $('#pregun_'+campo).hide();

    switch (valorActual) {
        case "1":
            $('#campan_'+campo).show();
            break;
        case "2_1":
            $('#bot_seccion_'+campo).show();
            break;
        case "2_2":
            $('#bot_'+campo).show();
            break;
        case "3":
            $('#pregun_'+campo).show();
            break;
        default:
            $('#dar_respuesta_'+campo).show();
            break;
    }
}

function cambiows(id){

    // Traigo el texto del select
    let texto = $("#webservice_"+id+" option:selected").text();
    let webservice = $("#webservice_"+id).val();
    let oldwebservice = $("#webservice_"+id).data("oldvalue");

    $("#tituloService_"+id).html(obtenerIconoTitulo()+'&nbsp;'+texto);

    // Hago la consulta para trae los parametros del webservice
    $.ajax({
        url: '<?=$url_crud;?>?getParametrosWebservice=si&webserviceId='+webservice,  
        type: 'POST',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend : function(){
            $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
        },
        complete : function(){
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function(data){
            if(data){
                console.log(data);

                // Primero hago el cambio de las variables dinamicas

                // Si se cambia el ws debo quitar los parametros viejos
                // if(oldwebservice != 0){
                //     variablesDinamicasWs = variablesDinamicasWs.filter((i) => i.id_ws !== oldwebservice);
                // }

                // if(data.parametrosRetorno){
                //     let nombreSeccion = $("#seccionNombre").val();
                //     nombreSeccion = nombreSeccion.slice(0, 10);

                //     let nombreWs = texto.slice(0, 10);

                //     let nombreSeccion = 

                //     $.each(data.parametrosRetorno, function(j, item2)){
                //         variablesDinamicasWs.push({"id_ws": webservice, "seccion": nombreSeccion});
                //     });
                // }

                if(data.parametros && data.parametros.length > 0){

                    let htmlParamsBody = '';

                    $.each(data.parametros, function(i, item){
                        htmlParamsBody += `
                        <tr>
                            <input type="hidden" name="parametro_${id}_${item.id}" id="parametro_${id}_${item.id}">
                            <td>${item.parametro}</td>
                            <td>
                                <select name="tipoParametro_${id}_${item.id}" id="tipoParametro_${id}_${item.id}" class="form-control input-sm" onchange="cambioTipoValor(${item.id}, ${id})">
                                    <option value="1">Estatico</option>
                                    <option value="2">Dinamico</option>
                                </select>
                            </td>
                            <td>
                                <input type="text" name="valorEstatico_${id}_${item.id}" id="valorEstatico_${id}_${item.id}" class="form-control input-sm">

                                <select name="valorDinamico_${id}_${item.id}" id="valorDinamico_${id}_${item.id}" class="form-control input-sm" style="display:none">
                                    ${variablesDinamicas}
                                </select> 
                            </td>
                        </tr>
                        `;
                    });
                    
                    // Armo la estructura de los parametros
                    let htmlParamsEstructura = `
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nombre del parametro</th>
                                    <th>Tipo de valor</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                ${htmlParamsBody}
                            </tbody>
                        </table>
                    `;

                    $('#webserviceParametros_'+id).html(htmlParamsEstructura);

                }else{
                    $('#webserviceParametros_'+id).html('No hay parametros para este webservice');
                }
                
            }
        },
        error: function(data){
            if(data.responseText){
                alertify.error("Hubo un error al traer la información" + data.responseText);
            }else{
                alertify.error("Hubo un error al traer la información");
            }
        }
    });

}

function cambioTipoValor(id, rowId){
    let tipoValor = $("#tipoParametro_"+rowId+"_"+id).val();
    
    $("#valorEstatico_"+rowId+"_"+id).hide();
    $("#valorDinamico_"+rowId+"_"+id).hide();

    if(tipoValor == 1){
        $("#valorEstatico_"+rowId+"_"+id).show();
    }else{
        $("#valorDinamico_"+rowId+"_"+id).show();
    }
}

function cambiarLabelInformativo(tipoSeccion){
    let texto = '';

    switch (tipoSeccion) {
        case "1":
            texto = "En estas secciones el bot va saltando a donde corresponda, según lo que escribe el cliente";
            break;
        case "2":
            texto = "En estas secciones el bot presenta cada una de las preguntas de forma secuencial en el orden que se configure acá";
            break;
        default:
            break;
    }

    $(".info_modal").html(texto);
}

function eliminarRowBot(oper, id){
    if(oper == 'add'){
        $("#row_add_"+id).remove();
    }else{
        let rowId = $("#campo_"+id).val();
    
        // Aqui elimina el filtro en la bd
        alertify.confirm("<?php echo $str_message_generico_D;?>?", function (e) {
            if(e){
                $.ajax({
                    url: '<?=$url_crud;?>?borrar_campo_bot=si',  
                    type: 'POST',
                    dataType: 'json',
                    data: { campoBotId:rowId },
                    beforeSend : function(){
                        $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                    },
                    complete : function(){
                        $.unblockUI();
                    },
                    //una vez finalizado correctamente
                    success: function(data){
                        $("#row_edit_"+id).remove();
                        alertify.success("campo eliminado");
                    }
                });     
            }
        });
    }
}

function eliminarRowWS(oper, id){
    if(oper == 'add'){
        $("#row_add_"+id).remove();
    }else{
        let rowId = $("#campo_"+id).val();
    
        // Aqui elimina el filtro en la bd
        alertify.confirm("<?php echo $str_message_generico_D;?>?", function (e) {
            if(e){
                $.ajax({
                    url: '<?=$url_crud;?>?borrar_campos_ws=si',  
                    type: 'POST',
                    dataType: 'json',
                    data: { rowId:rowId },
                    beforeSend : function(){
                        $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                    },
                    complete : function(){
                        $.unblockUI();
                    },
                    //una vez finalizado correctamente
                    success: function(data){
                        $("#row_edit_"+id).remove();
                        alertify.success("campo eliminado");
                    }
                });     
            }
        });
    }
}

function eliminarTagComun(oper, id){
    if(oper == 'add'){
        $("#tags_comun_"+id).remove();
    }else{
        let rowId = $("#cTag_"+id).val();
    
        // Aqui elimina el filtro en la bd
        alertify.confirm("<?php echo $str_message_generico_D;?>?", function (e) {
            if(e){
                $.ajax({
                    url: '<?=$url_crud;?>?borrar_campo_tag_comun=si',  
                    type: 'POST',
                    dataType: 'json',
                    data: { rowId:rowId },
                    beforeSend : function(){
                        $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                    },
                    complete : function(){
                        $.unblockUI();
                    },
                    //una vez finalizado correctamente
                    success: function(data){
                        $("#tags_comun_"+id).remove();
                        alertify.success("campo eliminado");
                    }
                });     
            }
        });
    }
}

function abrirDisparadorModal(huesped, tag = null){
    $("#tagsComunesTable tbody").html('');
    $("#tagsComunesModal").modal();
    $("#totalCamposComunes").val(0);

    $.ajax({
        url: '<?=$url_crud;?>?getTagsComunes=si&id_paso=<?=$_GET['id_paso']?>&huesped=<?=$huesped?>',  
        type: 'POST',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend : function(){
            $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
        },
        complete : function(){
            $.unblockUI();
        },
        success: function(data){
            if(data){

                if(data.campos){
                    $.each(data.campos, function(i, item){

                        addCampotagsComunes('edit');

                        $("#cTag_"+i).val(item.id);
                        $("#mTag_"+i).val(item.tag_disparador);
                        $("#tTag_"+i).val(item.expresiones);

                        $('#tTag_'+i).selectize({
                            plugins: ['remove_button'],
                            persist: false,
                            createOnBlur: true,
                            create: true,                           
                        });
                        
                    });

                }
            }
        }
    });
}

function addCampotagsComunes(oper){
    let num = $("#totalCamposComunes").val();

    let row = `
        <tr id="tags_comun_${num}">
            <input type="hidden" name="cTag_${oper}_${num}" id="cTag_${num}">
            <td>
                <input type="text" name="mTag_${oper}_${num}" id="mTag_${num}" class="form-control input-sm tag-comun-disp">
            </td>
            <td>
                <input type="text" name="tTag_${oper}_${num}" id="tTag_${num}" class="form-control input-sm tag-expresion-comun">
            </td>
            <td>
                <button type="button" id="btnEliminarEsteTag_${num}" onclick="eliminarTagComun('${oper}', ${num})" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
            </td>
        </tr>
    `;

    $("#tagsComunesTable tbody").append(row);

    if(oper == 'add'){
        $('#tTag_'+num).selectize({
            plugins: ['remove_button'],
            persist: false,
            createOnBlur: true,
            create: true,                           
        });
    }

    num++;
    $("#totalCamposComunes").val(num);
}

function guardarTagsComunes(){
    let valido = true;

    quitarCampoError();

    $('.tag-comun-disp').each(function(){
        if($(this).val() == ''){
            campoError($(this).attr('id'));
            valido=false;
        }
    });    

    // $('.tag-expresion-comun').each(function(){
    //     if($(this).val() == ''){
    //         campoError($(this).attr('id'));
    //         valido=false;
    //     }
    // });  

    if(valido){

        var formData = new FormData($("#tagsComunesForm")[0]);

        $.ajax({
            url: '<?=$url_crud;?>?guardar_tags_comunes=si&id_paso=<?=$_GET['id_paso']?>&huesped=<?=$huesped?>',  
            type: 'POST',
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend : function(){
                $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
            },
            complete : function(){
                $.unblockUI();
            },
            //una vez finalizado correctamente
            success: function(data){
                $("#tagsComunesForm")[0].reset();
                alertify.success("Información guardada con exito");
                $("#tagsComunesModal").modal("hide");
            },
            error: function(data){
                if(data.responseText){
                    alertify.error("Hubo un error al guardar la información" + data.responseText);
                }else{
                    alertify.error("Hubo un error al guardar la información");
                }
            }
        })
    }
}

// Esta es una function que cambia como las opciones de la base de datos PUEDE QUE LA BORRE
function cambiarBd(baseId){
    
    $.ajax({
        url: '<?=$url_crud;?>?get_campos_bd_especifica=si&baseId='+baseId,  
        type: 'POST',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        beforeSend : function(){
            $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
        },
        complete : function(){
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function(data){
            if(data){

                let listaCampos = '<option value="">Seleccione</option>';

                if(data.campos){
                    $.each(data.campos, function(i, item){
                        listaCampos += `<option value="${item.id}">${item.nombre}</option>`;
                    });
                }
                
                $(".campos-pregunta").html(listaCampos);
                opcionesCamposGlobales = listaCampos;
                
            }
        },
        error: function(data){
            if(data.responseText){
                alertify.error("Hubo un error al traer la información" + data.responseText);
            }else{
                alertify.error("Hubo un error al traer la información");
            }
        }
    });
}

function obtenerTagsComunes(){

    let huesped = <?=$huesped?>;

    $.ajax({
        url: '<?=$url_crud;?>?tagsComunes=true',
        type: 'POST',
        dataType: 'json',
        data: { huesped:huesped },
        // beforeSend : function(){
        //   // $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
        // },
        // complete : function(){
        //     $.unblockUI();
        // },
        //una vez finalizado correctamente
        success: function(data){
            if(data.tagsComunes && data.tagsComunes.length > 0){
                tagsComunes = data.tagsComunes;
            }
        },
        error: function(data){
            console.log(data);
            alertify.error("Se presento un error al traer los tags comunes");
        }
    });
}

function validarTagsUnicosEnSeccion(tag){

    let valido = true;
    let contador = 0;

    // Recorro todos los elementos que tengan la class item
    $(".item").each(function(){
        if($(this).attr("data-value") === tag){
            contador+= 1;
        }
    });
    
    if(contador > 1){
        valido = false;
    }

    return valido;
}

function limpiarTagsRepetidos(inputId, tag){
    // Primero limpio el tag de la vista
    $('.item.c-tag-'+inputId+'[data-value="'+tag+'"]').remove();

    // Luego lo limpio del value del archivo
    let tags = $("#tag_"+inputId).val();

    // lo elimino del value
    let arrTags = tags.split(',');
    let i = arrTags.indexOf(tag);

    if (i !== -1) {
        arrTags.splice(i,1);
    }

    tags = arrTags.toString();
    $("#tag_"+inputId).val(tags);

}

// Esta permite cambiar entre las acciones del disparador del webservice
function cambiarAccionws(){
    let accion = $("#accion_accion_webservice").val();

    $('#campan_accion_webservice').hide();
    $('#bot_seccion_accion_webservice').hide();
    $('#bot_accion_webservice').hide();

    switch (accion) {
        case "1":
            $('#campan_accion_webservice').show();
            break;
        case "2_1":
            $('#bot_seccion_accion_webservice').show();
            break;
        case "2_2":
            $('#bot_accion_webservice').show();
            break;
        default:
            break;
    }
}

function agregarTagAlItem(tagId, nuevoTag){
    
    let campoItem = $("#campo_item_"+tagId).val();
    let seccionAutores = $("#autorespuestaId").val();
    let seccion = $("#seccionBotId").val();
    let valido = true;

    if(campoItem == ''){
        $("#campo_item_"+tagId).focus();
        valido = false;

        alertify.error("El campo \"Agregar a esta opción\" debe estar seleccionado con una opcion");
    }

    if(valido){
        $.ajax({
            url: '<?=$url_crud;?>?agregarTagAlItem=true',
            type: 'POST',
            dataType: 'json',
            data: { campoItem:campoItem, tagId:tagId, nuevoTag:nuevoTag, seccion:seccionAutores },
            beforeSend : function(){
                $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
            },
            complete : function(){
                $.unblockUI();
            },
            //una vez finalizado correctamente
            success: function(data){

                if(data.valido){
                    $("#tag_row_"+tagId).remove();
                    alertify.success("tag agregado");
                    editarSeccion(seccion, seccionAutores);
                }else{
                    alertify.error("No se ha podido agregar este tag, valide si no esta ya agregado a la seccion");
                }
            }
        });
    }
}

function eliminarTagNoDeseado(tagId){

    // Aqui elimina el filtro en la bd
    alertify.confirm("<?php echo $str_message_generico_D;?>?", function (e) {
        if(e){
            $.ajax({
                url: '<?=$url_crud;?>?eliminarTagNoDeseado=true',  
                type: 'POST',
                dataType: 'json',
                data: { tagId:tagId },
                beforeSend : function(){
                    $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    $("#tag_row_"+tagId).remove();
                    alertify.success("tag eliminado");
                }
            });     
        }
    });
    
}

function abrirTagsGlobales(){

    obtenerTagsComunes();

    let pasoId = $("#pasoId").val();

    $("#tagsGlobalesTable tbody").html('');

    $.ajax({
        url: '<?=$url_crud;?>?obtenerTagsGlobales=true',
        type: 'POST',
        dataType: 'json',
        data: { pasoId:pasoId },
        beforeSend : function(){
            $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
        },
        complete : function(){
            $.unblockUI();
            $("#tagsGlobales").modal();
        },
        //una vez finalizado correctamente
        success: function(data){

            if(data.secciones && data.secciones.length > 0){
                
                data.secciones.forEach(element => {

                    let tipo = obtenerNombreTipoSeccionEIcono(element.tipo_seccion);

                    let tag = '';
                    if(element.tag_id !== null){
                        console.log("esta pasando por aca");
                        tag = element.tag;
                    }

                    let row = `
                        <tr>
                            <td style="max-width: 70%;"> ${element.nombre} - ${tipo}</td>
                            <td>
                                <div class="form-group">
                                    <input type="text" id="tag_global_${element.id}" name="tag_global_${element.id}" value="${tag}">
                                    <span>Separe los tags con comas.</span>
                                    <a href="#" class="pull-right modalDisparador" onclick="abrirDisparadorModal(<?=$huesped?>);return false;">Disparadores de uso frecuente</a>
                                </div>
                            </td>
                        </tr>
                    `;

                    $("#tagsGlobalesTable tbody").append(row);
                    inicializarSelectizeTags('global_'+element.id);
                });
            }
        },
        error: function(data){
            console.log(data);
            alertify.error("Se presento un error al cargar los datos");
        }
    });

}

function cambiarSeccion(rowActual){

    let accion = $("#accion_"+rowActual).val();

    if(accion == '2_1'){

        let seccionAutores = $("#bot_seccion_"+rowActual).val();

        $.ajax({
            url: '<?=$url_crud;?>?obtenerIdSeccion=true',
            type: 'POST',
            dataType: 'json',
            data: { seccionAutores:seccionAutores },
            beforeSend : function(){
                $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
            },
            complete : function(){
                $.unblockUI();
            },
            success: function(data){

                if(data.valido){
                    $("#modalConfiguracionSeccion").modal("hide");
                    setTimeout(() => {
                        editarSeccion(data.seccionId, seccionAutores);
                    }, 900);
                }else{
                    alertify.error("Se presento un error al traer el identificador de la seccion");   
                }
            },
            error: function(data){
                console.log(data);
                alertify.error("Se presento un error al tratar de cambiar la seccion");
            }
        });
    }else{
        alertify.error("La accion debe ser \"transferir a otra sección del bot\" para cambiar de sección");
    }

}

function guardarTagsGlobales(){

    var formData = new FormData($("#tagsGlobalesForm")[0]);

    $.ajax({
        url: '<?=$url_crud;?>?guardar_tags_globales=si&id_paso=<?=$_GET['id_paso']?>&huesped=<?=$huesped?>',  
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend : function(){
            $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
        },
        complete : function(){
            $.unblockUI();
        },
        //una vez finalizado correctamente
        success: function(data){
            $("#tagsGlobalesForm")[0].reset();
            alertify.success("Información guardada con exito");
            $("#tagsGlobales").modal("hide");
        },
        error: function(data){
            console.log(data);
            alertify.error("Hubo un error al guardar la información");
            
        }
    })
    
}

function buscarSeccion(){
    let buscando = $("#buscandoSeccion").val();


    $.ajax({
        url: '<?=$url_crud;?>?filtrarSecciones=true&id_paso=<?=$_GET['id_paso']?>',
        type: 'POST',
        dataType: 'json',
        data: { buscando:buscando },
        beforeSend : function(){
            $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
        },
        complete : function(){
            $.unblockUI();
        },
        success: function(data){
            console.log(data);
            if(data.estado){
                $("#totalFilas").val(0);
                $("#filtrado").val(1);

                $("#misSeccionesBot").html('');
                $("#mensajeBusquedaFiltrada").show();
                
                if(data.secciones.length > 0){

                    data.secciones.forEach(element => {
                        agregarFilaSeccion(element.nombre, element.id_seccion, element.tipo_base, element.id);
                    });

                }
            }else{
                alertify.error("Se presento un error al traer las secciones intenta nuevamente");   
            }
        },
        error: function(data){
            console.log(data);
            alertify.error("Se presento un error al traer la informacion intenta nuevamente");
        }
    });

    // let secciones = $("#seccionesBot .box-body .nombre-seccion");

    // let item = '';
    
    // for (let i = 0; i < secciones.length; i++) {
    //     item = $(secciones[i]).html().toLowerCase();
        
    //     for (let x = 0; x < item.length; x++) {
    //         if(buscando.length == 0 || item.indexOf(buscando) > -1 ){
    //             $(secciones[i]).closest('.row-seccion').show();
    //         }else{
    //             $(secciones[i]).closest('.row-seccion').hide();
    //         }
    //     }
    // }

    // $("#mensajeBusquedaFiltrada").hide();

    // if(buscando.length > 0){
    //     $("#mensajeBusquedaFiltrada").show();
    // }
}

function activarCondiciones(id){

    let condicion = $("#tipoCondicion").val();

    if(condicion == '0'){
        $("#grupo_codiciones").hide();
        $("#btnAgregarCondicion").hide();
    }else{
        $("#grupo_codiciones").show();
        $("#btnAgregarCondicion").show();
    }
}

function agregarAccionFinal(esAccionFinalDefecto, condicion, oper){

    let contador = $("#totalAccionesFinales").val();

    let num = contador;

    let nombre = esAccionFinalDefecto ? 'Accion por defecto' : `accion condicionada ${++num} ${condicion}`;

    let row = `
        <div class="panel box box-success" id="accionFinal${contador}">
            <div class="box-header with-border">
                <h4 class="box-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#cuerpoAccionFinal${contador}" id="tituloAccionFinal${contador}">
                        <i class="fa fa-comments-o"></i>&nbsp;${nombre}
                    </a>
                </h4>

                <div class="box-tools pull-right" data-toggle="tooltip" title="Avanzado">
                    <button type="button" id="btnAdvancedEditField_${contador}" class="btn btn-warning btn-sm" onclick="seccionAvanzado(${contador}, 'accionFinal')"><i class="fa fa-cog"></i></button>
                    <button type="button" id="btnEliminarAccionFinal_${contador}" onclick="eliminarAccionFinal(${contador})" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                </div>
            </div>

            <div id="cuerpoAccionFinal${contador}" class="panel-collapse collapse in">
                <div class="box-body">
                    <div class="row">
                        
                        <input type="hidden" id="campo_accion_final_${contador}" name="campo_accion_final_${contador}" value="">

                        <div class="col-md-9">
                            <div class="form-group">
                                <label for="">Respuestas (Texto que escribe el bot cuando se ejecuta la accion)</label>
                                <textarea id="rpta_accion_final_${contador}" name="rpta_accion_final_${contador}" cols="60" rows="1"></textarea>
                            </div>
                        </div>
                        
                        <div class="col-md-3">                                        
                            <div class="form-group">
                                <label for="">Acción</label>
                                <select id="accion_accion_final_${contador}" name="accion_accion_final_${contador}" class="form-control" onchange="cambioAccionFinal(${contador})">
                                    <option value="2_1">Transferir a otra sección del bot</option>
                                    <option value="2_2">Transferir a otro bot</option>
                                    <option value="1">Transferir a agentes</option>
                                </select>
                            </div>                                            
                            <div class="form-group">
                                <label for="">Detalle de la Acción</label>
                                <select class="form-control" style="display: none;" id="campan_accion_final_${contador}" name="campan_accion_final_${contador}">
                                    <?php echo $opcionesCampana ?>
                                </select>
                                <select class="form-control" id="bot_seccion_accion_final_${contador}" name="bot_seccion_accion_final_${contador}">
                                    <?php echo $opcionesSecBot ?>
                                </select>
                                <select class="form-control" style="display: none;" id="bot_accion_final_${contador}" name="bot_accion_final_${contador}">
                                    <?php echo $opcionesBot ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    `;

    if(esAccionFinalDefecto){
        $("#accionFinalDefecto").append(row);
        $("#btnEliminarAccionFinal_"+contador).hide();
        $("#btnAdvancedEditField_"+contador).hide();
    }else{
        $("#accionesFinales").append(row);
    }

    // Inicializo el campo
    tinymce.execCommand('mceRemoveEditor', false, 'rpta_accion_final_'+contador);
    tinymce.execCommand('mceAddEditor', false, 'rpta_accion_final_'+contador);

    contador++;
    $("#totalAccionesFinales").val(contador);

    if(oper == 'add'){
        crearAccionFinal(contador);
    }
}

function cambioAccionFinal(id){
    var campo = $("#accion_accion_final_"+id).val();

    $('#campan_accion_final_'+id).hide();
    $('#bot_seccion_accion_final_'+id).hide();
    $('#bot_accion_final_'+id).hide();

    switch (campo) {
        case "1":
            $('#campan_accion_final_'+id).show();
            break;
        case "2_1":
            $('#bot_seccion_accion_final_'+id).show();
            break;
        case "2_2":
            $('#bot_accion_final_'+id).show();
            break;
        default:
            break;
    }
}

function eliminarAccionFinal(id){
    let oper = 'add';
    if(oper == 'add'){
        $("#accionFinal"+id).remove();
    }else{
        // let rowId = $("#campo_"+id).val();
    
        // Aqui elimina el filtro en la bd
        alertify.confirm("<?php echo $str_message_generico_D;?>?", function (e) {
            if(e){
                // $.ajax({
                //     url: '<?=$url_crud;?>?borrar_campo_bot=si',  
                //     type: 'POST',
                //     dataType: 'json',
                //     data: { campoBotId:rowId },
                //     beforeSend : function(){
                //         $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                //     },
                //     complete : function(){
                //         $.unblockUI();
                //     },
                //     //una vez finalizado correctamente
                //     success: function(data){
                //         $("#row_edit_"+id).remove();
                //         alertify.success("campo eliminado");
                //     }
                // });     
                $("#accionFinal"+id).remove();
            }
        });
    }
}

function agregarCondicion(oper){

    let contador = $("#contCondicionesAccionesFinales").val();

    let row = `
        <div class="row" id="grupo_condiciones_${contador}">

            <input type="hidden" name="condiciones_accion_${contador}" id="condiciones_accion_${contador}" value="${oper}">
            <input type="hidden" name="condiciones_identificador_${contador}" id="condiciones_identificador_${contador}" value="0">

            <div class="col-md-1">
                <div class="form-group">
                    <select name="condiciones_operador_${contador}" id="condiciones_operador_${contador}" class="form-control input-sm">
                        <option value="AND">&</option>
                        <option value="OR">O</option>
                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <select name="condiciones_campo_${contador}" id="condiciones_campo_${contador}" class="form-control input-sm">
                        <option value="0">Seleccione un campo</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select name="condiciones_condicion_${contador}" id="condiciones_condicion_${contador}" class="form-control input-sm">
                        <option value="=">IGUAL A</option>
                        <option value="<>">DIFERENTE A</option>
                        <option value="<">MENOR QUE</option>
                        <option value=">">MAYOR QUE</option>
                        <option value="<=">MENOR O IGUAL QUE</option>
                        <option value=">=">MAYOR O IGUAL QUE</option>
                        <option value="EXISTE">EXISTE</option>
                        <option value="NO_EXISTE">NO EXISTE</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select name="tipo_valor_${contador}" id="tipo_valor_${contador}" class="form-control input-sm" onchange="cambiarTipoCampoValidacion(${contador})">
                        <option value="estatico">ESTATICO</option>
                        <option value="dinamico">DINAMICO</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group" id="divCondicionesValor_${contador}">
                    <input type="text" id="condiciones_valor_${contador}" name="condiciones_valor_${contador}" placeholder="Valor a buscar" class="form-control input-sm">
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <button type="button" id="btnEliminarCondicion_${contador}" onclick="eliminarCondicion(${contador},'${oper}')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                </div>
            </div>
        </div>
    `;

    $("#cuerpo_condiciones").append(row);

    if(contador == 0){
        $("#condiciones_operador_"+contador).hide();
    }

    agregarCamposaCondiciones("condiciones_campo_"+contador);

    contador++;
    $("#contCondicionesAccionesFinales").val(contador);
}

function cambiarTipoCampoValidacion(id){

    let tipoDatoAComparar = $("#tipo_valor_"+id).val();

    let campo = '';

    if(tipoDatoAComparar == 'estatico'){

        campo = `
            <input type="text" id="condiciones_valor_${id}" name="condiciones_valor_${id}" placeholder="Valor a buscar" class="form-control input-sm">
        `;

        $("#divCondicionesValor_"+id).html(campo);

    }else if(tipoDatoAComparar == 'dinamico'){

        campo = `
            <select name="condiciones_valor_${id}" id="condiciones_valor_${id}" class="form-control input-sm">
                <option value="0">Seleccione un campo</option>
            </select>
        `;

        $("#divCondicionesValor_"+id).html(campo);

        agregarCamposaCondiciones("condiciones_valor_"+id);
    }

}

function agregarCamposaCondiciones(nombreCampo){
    // Agrego los campos
    $("#"+nombreCampo).html('');

    opciones = '<option value="">Seleccione</option>';

    // Campos de la base
    opciones += `<optgroup label="Campos de la base de datos">`;
    
    opciones += `<option value='_CoInMiPo__b' tipo='3'>BD.ID_BD</option>`;
    opciones += `option value='_FechaInsercion' tipo='5'>BD.FECHA CREACION</option>`;

    opciones += camposBd.map(element => {
        return `<option value="${element.id}">BD.${element.nombre}</option>`;        
    });

    opciones += `<option value='_Estado____b' tipo='_Estado____b'>BD.ESTADO_DY (tipo reintento)</option>`;
    opciones += `<option value='_ConIntUsu_b' tipo='_ConIntUsu_b'>BD.Usuario</option>`;
    opciones += `<option value='_NumeInte__b' tipo='3'>BD.Numero de intentos</option>`;
    opciones += `<option value='_UltiGest__b' tipo='MONOEF'>BD.Ultima gesti&oacute;n</option>`;
    opciones += `<option value='_GesMasImp_b' tipo='MONOEF'>BD.Gesti&oacute;n mas importante</option>`;
    opciones += `<option value='_FecUltGes_b' tipo='5'>BD.Fecha ultima gesti&oacute;n</option>`;
    opciones += `<option value='_FeGeMaIm__b' tipo='5'>BD.Fecha gesti&oacute;n mas importante</option>`;
    opciones += `<option value='_ConUltGes_b' tipo='ListaCla'>BD.Clasificaci&oacute;n ultima gesti&oacute;n</option>`;
    opciones += `<option value='_CoGesMaIm_b' tipo='ListaCla'>BD.Clasificaci&oacute;n mas importante</option>`;
    opciones += `<option value='_Activo____b' tipo='_Activo____b'>BD.Registro activo</option>`;

    opciones += '</optgroup>';

    // Campos capturados del bot 
    opciones += `<optgroup label="Campos capturados del bot">`;
    opciones += variablesDinamicasBot.map(element => {

        let nombre = '${'+element.seccion+'.'+element.variable+'}';
        let valor = '${'+element.variable+'}';

        return `<option value="${valor}">${nombre}</option>`;        
    });
    opciones += '</optgroup>';
    
    // Campos del webservice
    opciones += `<optgroup label="Campos del webservice">`;
    opciones += variablesDinamicasWs.map(element => {

        let nombre = '${'+element.nombreService.toUpperCase()+'.'+element.variable+'}';
        let valor = '${'+element.seccion.toUpperCase()+'.'+element.variable+'}';

        return `<option value="${valor}">${nombre}</option>`;        
    });
    opciones += '</optgroup>';

    $("#"+nombreCampo).html(opciones);
}

function eliminarCondicion(id, oper){

    if(oper == 'add'){
        $("#grupo_condiciones_"+id).remove();
    }else{
        // Ejecutar servicio que elimine las condiciones
        let rowId = $("#condiciones_identificador_"+id).val();
    
        // Aqui elimina el filtro en la bd
        alertify.confirm("<?php echo $str_message_generico_D;?>?", function (e) {
            if(e){
                $.ajax({
                    url: '<?=$url_crud;?>?borrar_condicion=si',
                    type: 'POST',
                    dataType: 'json',
                    data: { condicionId:rowId },
                    beforeSend : function(){
                        $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                    },
                    complete : function(){
                        $.unblockUI();
                    },
                    //una vez finalizado correctamente
                    success: function(data){
                        $("#grupo_condiciones_"+id).remove();
                        alertify.success("Condicion eliminada");
                    }
                });     
            }
        });
    }
}

function agregarFilaTransaccional(tipo){

    let newTipo = null;

    if(tipo == 'captura'){
        newTipo = "2";
    }

    if(tipo == 'consulta'){
        newTipo = "3";
    }

    agregarFilaBot("add", newTipo);
}

function seccionAvanzado(contador, ejecucion){

    $("#contCondicionesAccionesFinales").val(0);
    $("#cuerpo_condiciones").html('');

    let campoId = $("#campo_accion_final_"+contador).val();

    $("#autorespuestaContenidoId").val(campoId);
    $("#idVariableDeAccionFinal").val(contador);

    if(ejecucion == "accionFinal"){ // Si es accion final el boton permanece desabilitado
        $("#tipoCondicion").val(1).change();
        $("#tipoCondicion").prop('disabled', 'disabled');
    }else if(ejecucion == "conversacional"){
        $("#tipoCondicion").val(0).change();
        $("#tipoCondicion").prop('disabled', false);
    }

    // Traer las condiciones
    $.ajax({
        url: '<?=$url_crud;?>?obtenerCondiciones=true',
        type: 'POST',
        dataType: 'json',
        data: { campoId:campoId },
        beforeSend : function(){
            $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
        },
        complete : function(){
            $.unblockUI();
        },
        success: function(data){
            
            if(data.condiciones.length > 0){

                $("#tipoCondicion").val(1).change();

                data.condiciones.forEach( (item, i) => {

                    // agrgeo la condicion
                    agregarCondicion('edit');
                    $("#condiciones_identificador_"+i).val(item.id);
                    $("#condiciones_operador_"+i).val(item.operador);
                    $("#condiciones_campo_"+i).val(item.campo);
                    $("#condiciones_condicion_"+i).val(item.condicion);
                    if(item.tipo_valor_comparar == 'estatico'){
                        $("#tipo_valor_"+i).val('estatico');
                    }else{
                        $("#tipo_valor_"+i).val('dinamico');
                        $("#tipo_valor_"+i).change();
                    }
                    $("#condiciones_valor_"+i).val(item.valor_a_comparar);
                });
            }else{
                if(ejecucion == "accionFinal"){ // Ejecute eso si solo es para accion final
                    $("#btnAgregarCondicion").click();
                }
            }

        },
        error: function(data){
            console.log(data);
            alertify.error("Se presento un error al traer la informacion intenta nuevamente");
        }
    });

    $("#seccionAvanzado").modal("show");

    // plantillaCondiciones(contador);
    
}

function plantillaCondiciones(contador){
    let contadorCondiciones = $("#contCondicionesAccionesFinales").val();

    let row = `
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <select name="af_tipo_condicion_${contador}" id="af_tipo_condicion_${contador}" class="form-control input-sm" onchange="activarCondiciones('${contador}')">
                    <option value="0">Sin condiciones</option>
                    <option value="1">Con condiciones</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <button type="button" id="btnAgregarCondicion_${contador}" onclick="agregarCondicion('${contador}')" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>Agregar Condicion</button>
            </div>
        </div>
    </div>

    <div id="grupo_codiciones_${contador}" style="display: none;">
        <div class="row" id="grupo_condiciones_${contador}_${contadorCondiciones}">
            <div class="col-md-5">
                <div class="form-group">
                    <select name="af_campo_${contador}_${contadorCondiciones}" id="af_campo_${contador}_${contadorCondiciones}" class="form-control input-sm">
                        <option value="0">Seleccione un campo</option>
                        <option value="0">Con condiciones</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select name="af_condicion_${contador}_${contadorCondiciones}" id="af_condicion_${contador}_${contadorCondiciones}" class="form-control input-sm">
                        <option value="=">IGUAL A</option>
                        <option value="!=">DIFERENTE A</option>
                        <option value="LIKE_1">INICIE POR</option>
                        <option value="LIKE_2">CONTIENE</option>
                        <option value="LIKE_3">TERMINE EN</option>
                    </select>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <input type="text" id="af_valor_${contador}_${contadorCondiciones}" name="af_valor_${contador}_${contadorCondiciones}" placeholder="Valor a buscar" class="form-control input-sm">
                </div>
            </div>                                                
        </div>
    </div>
    `;

    $("#condicionesAcciones").html(row);

    agregarCamposaCondiciones(contador+"_"+contadorCondiciones);

    if(contador == 'defecto'){
        $("#af_tipo_condicion_"+contador).val(0).change();
    }else{
        $("#af_tipo_condicion_"+contador).val(1).change();
    }

    contadorCondiciones++;
    $("#contCondicionesAccionesFinales").val(contadorCondiciones);
}

function crearAccionFinal(contador){
    let autorespuesta = $("#autorespuestaId").val();
    $.ajax({
        url: '<?=$url_crud;?>?crearAccionFinal=true',
        type: 'POST',
        dataType: 'json',
        data: { autorespuesta:autorespuesta },
        beforeSend : function(){
            $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
        },
        complete : function(){
            $.unblockUI();
        },
        success: function(data){
            $("#campo_accion_final_"+contador).val(data.id);
        },
        error: function(data){
            console.log(data);
            alertify.error("Se presento un error al traer la informacion intenta nuevamente");
        }
    });
}

function guardarCondiciones(){

    let formData = new FormData($("#formConfiguracionAvanzada")[0]);
    formData.append('contCondiciones', $("#contCondicionesAccionesFinales").val());
    
    $.ajax({
        url: '<?=$url_crud;?>?guardarCondiciones=true',
        type: 'POST',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend : function(){
            $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
        },
        complete : function(){
            $.unblockUI();
        },
        success: function(data){
            alertify.success("Se ha guardado la informacion correctamente");

            if(data.nombreCondicion){
                let nombreCondicion = `<i class="fa fa-comments-o"></i>&nbsp; accion condicionada ${data.nombreCondicion}`;

                let idAccion = $("#idVariableDeAccionFinal").val();
                $("#tituloAccionFinal"+idAccion).html(nombreCondicion);
            }

            $("#seccionAvanzado").modal('hide');
        },
        error: function(data){
            console.log(data);
            alertify.error("Se presento un error al traer la informacion intenta nuevamente");
        }
    });
}
</script>