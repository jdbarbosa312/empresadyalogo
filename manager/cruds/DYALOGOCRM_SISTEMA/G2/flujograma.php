<?php
    $Lsql = "SELECT G2_C69 FROM ".$BaseDatos_systema.".G2 WHERE md5(concat('".clave_get."', G2_ConsInte__b)) = '".$_GET['estrategia']."'";
    $res  = $mysqli->query($Lsql);
    if($res && $res->num_rows==1){
        $Poblacion = $res->fetch_array();
    }else{ ?>
            <script>
                window.location.href="<?=base_url?>modulo/error";
            </script>
<?php } ?>
<style type="text/css">
    .clock      {
        position:relative;left:50%;top:50%;width:36px;height:36px;padding:20px;}
     .loader {
        position: absolute;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        opacity: .9;
        background: 50% 50% no-repeat rgb(249,249,249);
     }
    .modal.in .modal-dialog {
        width: 80%;
    }
    .lista table{
        margin: 0;
    }
    .titulo-dragdrop{
        background: #f1f1f1;
        color: #858585;
        border: 1px solid #eaeaea;
        font-weight: bold;
        padding: 6px;
        margin-bottom: 0;
    }

    .box.box-warning.yellow {
        border-top-color: #f3ec12;
    }
    .error-input {
        border: 1px solid #cc0033 !important;
    }

</style>
<script type="text/javascript">
    function autofitIframe(id){
        if (!window.opera && document.all && document.getElementById){
            id.style.height=id.contentWindow.document.body.scrollHeight;
            document.getElementById('imageCarge').style.display = "none"; 
        } else if(document.getElementById) {
            id.style.height=id.contentDocument.body.scrollHeight+"px";
            document.getElementById('imageCarge').style.display = "none"; 
        }
    }
</script>

<div id="mostrar_loading" class=""  >
    <div class="container-fluid" id="barra" style="margin-top: 31% ;display: none;"  >
        <div class="row" >
            <div class="col-md-12">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" id="barraProgreso" role="progressbar" style="width: 1%; max-width: 100%;" >
                        1%
                    </div>
                    
                </div>
            </div>       
        </div>
    </div>
</div>
<div class="modal fade-in" id="editarDatos" tabindex="-1" aria-labelledby="editarDatos" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog">
   <input type="hidden" name="fallasValidacion" id="fallasValidacion" value="0">
   <input type="hidden" name="strFechaInicial_t" id="strFechaInicial_t" value="">
    <input type="hidden" name="intIdCampana_t" id="intIdCampana_t" value="">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close BorrarTabla" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title" id="title_cargue"><?php echo $str_strategia_edicion.' '; ?></h4>
            </div>
            <div class="modal-body" style="height: 100%;">
                <!--<img src="<?=base_url?>assets/img/clock.gif" style="text-align : center;" id="imageCarge">-->
                <div id="divIframe" style="overflow-x: scroll;">
                    <!--<iframe id="frameContenedor" style="width: 100%; height: 1000px;" src="" scrolling="si"  marginheight="0" marginwidth="0" noresize  frameborder="0" onload="autofitIframe(this);">

                    </iframe>-->
                </div>
            </div>
        </div>
        <input type="hidden" name="TablaTemporal" id="TablaTemporal" value="">
    </div>
</div>


<div class="modal fade-in" id="pasoscortos" tabindex="-1" aria-labelledby="editarDatos" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title" id="title_estratPasosC"><?php echo $str_strategia_edicion.' '; ?></h4>
            </div>
            <div class="modal-body">
                <!--<img src="<?=base_url?>assets/img/clock.gif" style="text-align : center;" id="imageCarge">-->
                <div id="divIframePasosCortos">
                    <!--<iframe id="frameContenedor" style="width: 100%; height: 1000px;" src="" scrolling="si"  marginheight="0" marginwidth="0" noresize  frameborder="0" onload="autofitIframe(this);">

                    </iframe>-->
                </div>
            </div>
        </div>
    </div>
</div>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php 
                $Lsql = "SELECT G2_C7, G2_C69 FROM ".$BaseDatos_systema.".G2 WHERE md5(concat('".clave_get."', G2_ConsInte__b)) = '".$_GET['estrategia']."'";
                //echo $Lsql;
                $res_Lsql = $mysqli->query($Lsql);
                $dato = $res_Lsql->fetch_array();
                $str_nombre_estrat = $dato['G2_C7'];
                $str_poblacion = $dato['G2_C69'];
            ?>
            <?php echo $str_strategias_flujograma.$str_nombre_estrat.$str_strategias_flujograma_2;?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=base_url?>index.php"><i class="fa fa-dashboard"></i> <?php echo $home;?></a></li>
            <?php if(!isset($_GET['ruta'])){ ?>
            <li><a href="<?=base_url?>modulo/estrategias"><i class="fa fa-adjust"></i> <?php echo $str_estrategia;?></a></li>
            <?php }else{ ?>
            <li><a href="index.php?page=dashEstrat&estrategia=<?php echo $_GET['estrategia'];?>&huesped=<?php echo $_SESSION['HUESPED'];?>"><i class="fa fa-adjust"></i> <?php echo $str_estrategia;?></a></li>
            <?php } ?>
            <li class="active"><?php echo $str_flujograma;?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box box-default">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-10">
                        &nbsp;
                    </div>
                    <div class="col-md-2" style="text-align: right;">
                        <button class="btn btn-default" id="Save_modal" >
                            <i class="fa fa-save"></i>
                        </button>
                        <button class="btn btn-default" id="cancel" >
                            <i class="fa fa-close"></i>
                        </button>
                    </div>
                </div>
                <div class="row" id="sample">
                    <div class="col-md-12">
                        <div class="row">
                            <!-- Menu de pasos -->
                            <div class="col-md-2">
                                <h4><?php echo $str_type_pass;?></h4>
                                <span style="display: inline-block; vertical-align: top; width:100%;">
                                    <div id="myPaletteDiv" style="height: 600px;"></div>
                                </span>
                            </div>
                            <!-- Flujograma -->
                            <div class="col-md-10">
                                <h4><?php echo $str_flujograma; ?></h4>
                                <span style="display: inline-block; vertical-align: top; width:100%">
                                    <div id="myDiagramDiv" style="border: solid 1px black; height: 600px;"></div>
                                </span>
                            </div>                                                                                                                       
                        </div>
                        <div class="row" style="display: none;">
                            <!-- FIN DEL CAMPO TIPO MEMO -->
                            <div class="form-group" style="display: none;">
                <textarea id="mySavedModel" class="form-control">
                {
                    "class": "go.GraphLinksModel",
                    "linkFromPortIdProperty": "fromPort",
                    "linkToPortIdProperty": "toPort",
                    "nodeDataArray": [
                        <?php
                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".ESTPAS WHERE md5(concat('".clave_get."', ESTPAS_ConsInte__ESTRAT_b)) = '".$_GET['estrategia']."'";
                            $res_Pasos = $mysqli->query($Lsql);
                            $i = 0;
                            $separador = '';
                            while ($keu = $res_Pasos->fetch_object()) {
                                if($i != 0){
                                    $separador = ',';
                                }
                                echo $separador."
                                {\"category\":\"".$keu->ESTPAS_Nombre__b."\", \"nombrePaso\":\"".$keu->ESTPAS_Comentari_b."\", \"active\": ".$keu->ESTPAS_activo.", \"tipoPaso\": ".$keu->ESTPAS_Tipo______b.", \"figure\":\"Circle\", \"key\": ".$keu->ESTPAS_ConsInte__b.", \"loc\":\"".$keu->ESTPAS_Loc______b."\"}"."\n";
                                $i++;
                            }
                        ?>
                    ],
                    "linkDataArray": [
                        <?php
                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".ESTCON WHERE md5(concat('".clave_get."', ESTCON_ConsInte__ESTRAT_b)) = '".$_GET['estrategia']."'";
                            $res_Pasos = $mysqli->query($Lsql);
                            $i = 0;
                            $separador = '';
                            while ($keu = $res_Pasos->fetch_object()) {
                                if($i != 0){
                                    $separador = ',';
                                }
                                if(!is_null($keu->ESTCON_Coordenadas_b)){
                                    echo $separador."
                                    {\"from\":".$keu->ESTCON_ConsInte__ESTPAS_Des_b.", \"to\":".$keu->ESTCON_ConsInte__ESTPAS_Has_b.", \"fromPort\":\"".$keu->ESTCON_FromPort_b."\", \"toPort\":\"".$keu->ESTCON_ToPort_b."\", \"visible\":true, \"points\":".$keu->ESTCON_Coordenadas_b.", \"text\":\"".str_replace("\n", '', $keu->ESTCON_Comentari_b)."\", \"active\":".$keu->ESTCON_Activo_b." }"."\n";
                                }else{
                                    echo $separador."
                                    {\"from\":".$keu->ESTCON_ConsInte__ESTPAS_Des_b.", \"to\":".$keu->ESTCON_ConsInte__ESTPAS_Has_b.", \"fromPort\":\"".$keu->ESTCON_FromPort_b."\", \"toPort\":\"".$keu->ESTCON_ToPort_b."\", \"visible\":true, \"active\":".$keu->ESTCON_Activo_b." }"."\n";
                                }
                                
                                $i++;
                            }
                        ?>
                    ]
                }
                </textarea>
                            </div>
                        </div>
                    </div>
            
                </div>
            </div>
        </div>
    </section>


<div class="modal fade-in" id="crearCampanhasNueva" tabindex="-1" aria-labelledby="editarDatos" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title" id="title_estratCampan"><?php echo $campan_title___;?></h4>
            </div>
            <div class="modal-body">
                <form id="formuarioCargarEstoEstrart">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <!-- CAMPO TIPO TEXTO -->
                            <div class="form-group">
                                <label for="G10_C71" id="LblG10_C71"><?php echo $campan_nombre__;?></label>
                                <input type="text" class="form-control input-sm" id="G10_C71" value=""  name="G10_C71"  placeholder="<?php echo $campan_nombre__;?>">
                            </div>
                        <!-- FIN DEL CAMPO TIPO TEXTO -->
                        </div>
                        <input type="hidden" name="G2_C5" id="G2_C5" value="<?php echo $_SESSION['HUESPED'];?>">
                        <input type="hidden"  class="form-control input-sm Numerico" value="<?php if(isset($_GET['id_paso'])){ echo $_GET['id_paso']; }else{ echo 0; } ;?>"  name="id_paso" id="id_estpas_mio">
                        <input type="hidden" name="G10_C72" value="-1">
                        <input type="hidden" name="G10_C74" value="<?php echo $str_poblacion; ?>">
                    </div>
                    <div class="row" id="campanasNormales">
                        <div class="col-md-12 col-xs-12">
                            <?php 
                                $str_Lsql = "SELECT  G5_ConsInte__b as id , G5_C28 FROM ".$BaseDatos_systema.".G5 WHERE G5_C29 = 1 AND G5_C316 = ".$_SESSION['HUESPED']." ORDER BY G5_C28 ASC";
                            ?>
                            <!-- CAMPO DE TIPO GUION -->
                            <div class="form-group">
                                <label for="G10_C73" id="LblG10_C73"><?php echo $campan_form_age;?></label>
                                <select class="form-control input-sm str_Select2" style="width: 100%;"  name="G10_C73" id="G10_C73">
                                    <option  value="0">NOMBRE</option>
                                    <?php
                                        /*
                                            SE RECORRE LA CONSULTA QUE TRAE LOS CAMPOS DEL GUIÓN
                                        */
                                        $combo = $mysqli->query($str_Lsql);
                                        while($obj = $combo->fetch_object()){
                                            echo "<option value='".$obj->id."' dinammicos='0'>".utf8_encode($obj->G5_C28)."</option>";

                                        }    
                                    ?>
                                </select>
                            </div>
                            <!-- FIN DEL CAMPO TIPO LISTA -->
                        </div>
                    </div>
                    <div class="row" id="backoffice" style="display: none;">
                        <div class="col-md-12 col-xs-12">
                            <?php 
                                $str_Lsql = "SELECT  G5_ConsInte__b as id , G5_C28 FROM ".$BaseDatos_systema.".G5 WHERE G5_C29 = 4 AND G5_C316 = ".$_SESSION['HUESPED']." ORDER BY G5_C28 ASC";
                            ?>
                            <!-- CAMPO DE TIPO GUION -->
                            <div class="form-group">
                                <label for="G10_C73" id="LblG10_C73"><?php echo $campan_form_age;?></label>
                                <select class="form-control input-sm str_Select2" style="width: 100%;"  name="G10_C73B" id="G10_C73B">
                                    <option  value="0">NOMBRE</option>
                                    <?php
                                        /*
                                            SE RECORRE LA CONSULTA QUE TRAE LOS CAMPOS DEL GUIÓN
                                        */
                                        $combo = $mysqli->query($str_Lsql);
                                        while($obj = $combo->fetch_object()){
                                            echo "<option value='".$obj->id."' dinammicos='0'>".utf8_encode($obj->G5_C28)."</option>";

                                        }    
                                    ?>
                                </select>
                            </div>
                            <!-- FIN DEL CAMPO TIPO LISTA -->
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>
                                <input type="checkbox" class="fromBAse" name="generarFromDB" id="generarFromDB" value="1">&nbsp;<?php echo $campan_from_db_;?>
                            </label>
                        </div>
                    </div>

                </form>
            </div>
            <div class="box-footer">
                <input type="hidden" id="AddTipoCampan">
                <button class="btn btn-default regresoCampains" type="button"  data-dismiss="modal" >
                    <?php echo $str_cancela;?>
                </button>
                <button class="btn-primary btn pull-right" type="button" id="btnSaveCampan">
                    <?php echo $str_guardar;?>
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Esta modal se encargara de la asignacion de casos de backoffice -->
<div class="modal fade" id="casoBackofficeModal" tabindex="-1" aria-labelledby="Backoffice" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Casos de BackOffice</h4>
            </div>
            <div class="modal-body">
                <form action="" id="casoBackofficeForm">
                    <!--esta accion es si para crear o editar -->
                    <input type="hidden" name="tipoAccion" id="tipoAccion">
                    <input type="hidden" name="idEstpas" id="idEstpas">
                    <input type="hidden" name="idGuion" id="idGuion">
                    <input type="hidden" name="idTareaBack" id="idTareaBack" value="0">

                    <div class="panel box box-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#sec_1">
                                    Tarea de backoffice
                                </a>
                            </h4>
                        </div>
                        <div id="sec_1" class="panel-collapse collapse in">
                            <div class="form-group row">
                                <div class="col-md-11">
                                    <label for="nombreCaso">Nombre</label>
                                    <input type="text" class="form-control" name="nombreCaso" id="nombreCaso">
                                </div>
                                <div class="col-md-1 col-xs-1">
                                    <div class="form-group">
                                        <label for="pasoActivo" id="LblpasoActivo">ACTIVO</label>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="1" name="pasoActivo" id="pasoActivo" data-error="Before you wreck yourself"> 
                                            </label>
                                        </div>
                                    </div>
                                </div>                             
                            </div>

                        <div class="form-group row camposDeEdicion">
                            <div class="col-md-12">
                                <label for="formulario">Tipo de distribución</label>
                                <select name="tipoDistribucionTrabajo" id="tipoDistribucionTrabajo" class="form-control">
                                    <option value="" disabled>Seleccionar</option>
                                    <option value="1">Todos ven todo</option>
                                    <option value="2">Asignar al que menos registros tenga en esta condición</option>
                                    <option value="3">Asignar al que menos registros tenga</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row camposDeEdicion">
                            <div class="col-md-6">
                                <label for="pregun">Campo de la condición</label>
                                <select name="pregun" id="pregun" class="form-control">
                                    <option value="">Seleccionar</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="lisopc">Valor de la condición</label>
                                <select name="lisopc" id="lisopc" class="form-control">
                                    <option value="">Seleccionar</option>
                                </select>
                            </div>
                        </div>
                        </div>
                    </div>

                    <div class="panel box box-primary camposDeEdicion">
                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#sec_2">
                                    Asignacion de usuarios
                                </a>
                            </h4>
                        </div>
                        <div id="sec_2" class="panel-collapse collapse in">
                            <!-- En esta seccion se encuentra el dragAndDrop -->
                            <div class="form-group row" id="dragAndDrop">
                                <div class="col-md-5">
                                    <div class="input-group">
                                        <input type="text" name="buscadorDisponible" id="buscadorDisponible" class="form-control">
                                        <span class="input-group-addon">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                    <p class="text-center titulo-dragdrop">Disponibles</p>
                                    <ul id="disponible" class="nav nav-pills nav-stacked lista" style="height: 400px;max-height: 400px;
                                                margin-bottom: 10px;
                                                overflow: auto;   
                                                -webkit-overflow-scrolling: touch;border: 1px solid #eaeaea;">
                                        
                                    </ul>
                                </div>
                                <div class="col-md-2 text-center" style="padding-top:100px">
                                    <button type="button" id="derecha" style="width:90px; height:30px; margin-bottom:5px" class="btn btn-info btn-sm">></button> <br>
                                    <button type="button" id="todoDerecha" style="width:90px; height:30px; margin-bottom:5px" class="btn btn-info btn-sm">>></button> <br>
                                    
                                    <button type="button" id="izquierda" style="width:90px; height:30px; margin-bottom:5px" class="btn btn-info btn-sm"><</button> <br>
                                    <button type="button" id="todoIzquierda" style="width:90px; height:30px; margin-bottom:5px" class="btn btn-info btn-sm"><<</button>
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group">
                                        <input type="text" name="buscadorSeleccionado" id="buscadorSeleccionado" class="form-control">
                                        <span class="input-group-addon">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                    <p class="text-center titulo-dragdrop">Seleccionados</p>
                                    <ul id="seleccionado" class="nav nav-pills nav-stacked lista" style="height: 400px;max-height: 400px;
                                                margin-bottom: 10px;
                                                overflow: auto;   
                                                -webkit-overflow-scrolling: touch;border: 1px solid #eaeaea;">
                                    
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="guardarCasoBackofficeButton">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para el paso LEAD -->
<div class="modal fade" id="modalLead" tabindex="-1" aria-labelledby="LEAD" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Insertar registro desde correo</h4>
            </div>
            <div class="modal-body">
                <div class="box box-primary">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <form id="form-lead">
                                <input type="hidden" name="id_estpas" id="id_estpas">
                                <div class="col-md-12">
                                    <div class="form-group col-md-11">
                                        <label for="nombreLead">Nombre</label>
                                        <input type="text" name="nombreLead" id="nombreLead" class="form-control">
                                    </div>
                                    <div class="col-md-1 col-xs-1">
                                        <div class="form-group">
                                            <label for="leadActivo" id="LblleadActivo">ACTIVO</label>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="1" name="leadActivo" id="leadActivo" data-error="Before you wreck yourself"> 
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12 table-responsive">
                                    <table class="table" id="LeadTable">
                                        <thead>
                                            <tr>
                                                <th><?php echo $title_mail_cuenta;?></th>
                                                <th><?php echo $title_mail_tipo_condicion;?></th>
                                                <th><?php echo $title_mail_condicion;?></th>
                                            </tr>    
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <select name="leadQuienRecibe" id="leadQuienRecibe" class="form-control">
                                                        <option value="0">Seleccione</option>
                                                        <?php
                                                            $str_Lsql = 'SELECT * FROM '.$dyalogo_canales_electronicos.'.dy_ce_configuracion WHERE id_huesped = '.$_SESSION['HUESPED'];
                                                            $combo = $mysqli->query($str_Lsql);
                                                            while($obj = $combo->fetch_object()){
                                                                echo "<option value='".$obj->id."' dinammicos='0'>".($obj->direccion_correo_electronico)."</option>";

                                                            }    
                                                            
                                                        ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="leadTipoCondicion" id="leadTipoCondicion" class="form-control" onchange="cambioTipoCondicionLead()">
                                                        <option value="100">Sin Condición</option>
                                                        <option value="1">Proviene del correo</option>
                                                        <option value="2">Proviene del dominio</option>
                                                        <option value="3">El asunto contiene</option>
                                                        <option value="4">El cuerpo contiene</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" id="leadCondicion" name="leadCondicion" class="form-control" disabled>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4>Campos a emparejar</h4>
                                        </div>
                                        <div class="col-md-12">
                                            <table class="table table-bordered" id="LeadCampos">
                                                <thead>
                                                    <tr>
                                                        <th>Tag inicial</th>
                                                        <th>Tag final</th>
                                                        <th>Campo de la base de datos</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="pull-right">
                                                <button type="button" id="nuevoCampoEmparejar" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Agregar campo</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="guardarPasoLead" onclick="guardarLead()">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="webserviceModal" tabindex="-1" aria-labelledby="WebService" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Web Service</h4>
            </div>
            <div class="modal-body">
                <form action="post" id="webServiceForm">
                    <!--esta accion es si para crear o editar -->
                    <input type="hidden" name="pasowsId" id="pasowsId">
                    <input type="hidden" name="valoresws" id="valoresws">
                    <input type="hidden" name="campoOrigen" id="campoOrigen">

                    <div class="panel box box-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#sec_ws1">
                                    Web Service
                                </a>
                            </h4>
                        </div>
                        <div id="sec_ws1" class="panel-collapse collapse in">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="wsNombre">Nombre</label>
                                    <input type="text" class="form-control" name="wsNombre" id="wsNombre">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Parametros</label>
                                    <table class="table table-condensed table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Tipo</th>
                                                <th>Descripcion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>strUsuario_t</td>
                                                <td>String</td>
                                                <td>El usuario autorizado para el API</td>
                                            </tr>
                                            <tr>
                                                <td>strToken_t</td>
                                                <td>String</td>
                                                <td>Token de autorizacion para el consumo</td>
                                            </tr>
                                            <tr>
                                                <td>intCodigoAccion_t</td>
                                                <td>Integer</td>
                                                <td>La accion a ejecutar con el dato</td>
                                            </tr>
                                            <tr>
                                                <td>intIdEstpas_t</td>
                                                <td>Integer</td>
                                                <td>El paso con el cual se trabaja</td>
                                            </tr>
                                            <tr>
                                                <td>booValidaConRestriccion_t</td>
                                                <td>Boolean</td>
                                                <td>Si la validación arroja un error, el proceso sigue pero se inserta como un NO Contactable</td>
                                            </tr>
                                            <tr>
                                                <td>strCamposLlave_t</td>
                                                <td>String</td>
                                                <td>Los campos que no se pueden repetir en cuanto a su valor</td>
                                            </tr>
                                            <tr>
                                                <td>mapValoresCampos_t</td>
                                                <td>Mapa</td>
                                                <td>El mapa de los valores CAMPO, VALOR que se van a usar en el request</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <label>Codigos de accion (intCodigoAccion_t)</label>
                                    <table class="table table-condensed table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Codigo</th>
                                                <th>Descripcion de la accion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Insertar en la bd y en la campaña</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Insertar solo en la base de datos</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Actualizar</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Insertar sino existe y si existe, actualizar</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>Sacar de la campaña (Elimina de muestra y de bd)</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>Actualiza la base de datos y desactiva el regisro (pone en estado 3)</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Campo llave</label>
                                        <select name="cllave" id="cllave" class="form-control" onchange="generarEjemplo()">        
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="validaConRestriccion">
                                            Restringir registros con número no valido
                                        </label>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="validaConRestriccion" id="validaConRestriccion" value="1" onchange="generarEjemplo()">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="infows">Ejemplo de consumo</label>
                                    <textarea class="form-control" name="infows" id="infows" cols="30" rows="15" style="width: 80%" readonly>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="guardarWS" onclick="saveWebservice()">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para bot inicial -->
<div class="modal fade" id="configuracionInicialBot">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Configuración inicial del bot</h4>
            </div>

            <div class="modal-body">
                <form action="#" method="POST" id="formConfiguracionIncialBot">
                    <input type="hidden" name="configuracionInicialPaso" id="configuracionInicialPaso" value="0">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Nombre de la sección</label>
                                <input type="text" name="configuracionInicialBotNombre" id="configuracionInicialBotNombre" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Tipo se seccion de Bienvenida</label>
                                <select name="configuracionInicialBotTipoSeccion" id="configuracionInicialBotTipoSeccion" class="form-control input-sm">
                                    <option value="1">Conversacional</option>
                                    <option value="23">Transaccional</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                    <?php echo $str_cancela;?>
                </button>
                <button type="button" class="btn btn-primary pull-right" id="configuracionInicialBotGuardar" onclick="guardarConfiguracionInicialBot()">
                    Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade-in" id="filtrosCampanha" tabindex="-1" aria-labelledby="editarDatos" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title" id="title_estrat_filtros"><?php echo $filtros_campanha;?></h4>
            </div>
            <div class="modal-body">
                <form id="consultaCamposWhere">
                    <input type="hidden" name="esSmsEntrante" id="esSmsEntrante" value="0">
                    <input type="hidden" name="valorPregunSms" id="valorPregunSms" value="0">

                    <div class="row" style="margin-bottom: 12px;">
                        <div class="col-md-11">
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="estconActivo">ACTIVO</label>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="-1" name="estconActivo" id="estconActivo"> 
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel box box-primary secTraspasoRegistros">
                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#s_2">
                                    <?php echo $str_acordeon_ti_1_;?>
                                </a>
                            </h4>
                        </div>
                        <div id="s_2" class="panel-collapse collapse in">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="radiocondiciones" id="radiocondiciones1" class="Radiocondiciones" value="1">
                                                <?php echo $filtros_tipo_1_c; ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12 condicionesTop" hidden>
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="radiocondiciones" id="radiocondiciones2" class="Radiocondiciones" value="2">
                                                <?php echo $filtros_tipo_2_c; ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="radiocondiciones" id="radiocondiciones3" class="Radiocondiciones" value="3">
                                                <?php echo $filtros_tipo_3_c; ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xs-12 condicionesTop" hidden>
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="radiocondiciones" id="radiocondiciones4" class="Radiocondiciones" value="4">
                                                <?php echo $filtros_tipo_4_c; ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="display: none;" id="divCantidadCampan">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group">
                                        <input type="text" name="txtCantidadRegistrps" id="txtCantidadRegistrps" class="form-control" placeholder="<?php echo $filtros_tipo_5_c; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" id="filtros">
                                    
                                </div>
                            </div>
                            <div class="row" style="display: none;" id="divFiltrosCampan">
                                <div class="col-md-9">
                                    &nbsp;
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="button" id="newFiltro" style="float:right">
                                            <?php echo $filtros_name_cam;?>
                                        </button>    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <input type="hidden" name="tipoLlamado" id="tipoLlamado">
                                <input type="hidden" name="from" id="id_paso_from" >
                                <input type="hidden" name="to" id="id_paso_to" >
                                <input type="hidden" name="dataBase" id="dataBase" value="<?php echo $str_poblacion;?>">
                                <input type="hidden" name="id_campanaFiltros" id="id_campanaFiltros">
                            </div>
                        </div>
                    </div>

                    <div class="panel box box-primary secTraspasoRegistros">
                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#s_1">
                                    <?php echo $str_acordeon_ti_2_;?>
                                </a>
                            </h4>
                        </div>
                        <div id="s_1" class="panel-collapse collapse in">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><?php echo $str_tipo_insert___; ?></label>
                                        <select class="form-control" name="cmbTipoInsercion" id="cmbTipoInsercion">
                                            <option value="0"><?php echo $str_tipo_insert_1_; ?></option>
                                            <option value="-1"><?php echo $str_tipo_insert_2_; ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label><?php echo $str_fecha_ocamp___; ?></label>
                                        <select class="form-control" id="cmbCampoFecha" name="cmbCampoFecha">
                                            <option value="-1"><?php echo $str_fecha_now_____; ?></option>
                                            <?php 
                                                $Lsql = "SELECT PREGUN_ConsInte__b , PREGUN_Texto_____b FROM ".$BaseDatos_systema.".PREGUN  WHERE PREGUN_ConsInte__GUION__b = ".$str_poblacion." AND PREGUN_Tipo______b = 5";
                                                $res = $mysqli->query($Lsql);
                                                while ($key = $res->fetch_object()) {
                                                    echo "<option value='".$key->PREGUN_ConsInte__b."'>".$key->PREGUN_Texto_____b."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label><?php echo $str_fecha_ocamp_1_; ?></label>
                                        <select class="form-control" id="masMenosFecha" name="masMenosFecha">
                                            <option value="1"><?php echo $str_fecha_ocamp_3_; ?></option>
                                            <option value="0"><?php echo $str_fecha_ocamp_4_; ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label><?php echo $str_fecha_ocamp_2_;?></label>
                                        <input type="text" class="form-control" id="txtRestaSumaFecha" name="txtRestaSumaFecha" placeholder="<?php echo $str_fecha_ocamp_2_;?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label><?php echo $str_hora_ocamp____;?></label>
                                        <select class="form-control" id="cmbCampoHora" name="cmbCampoHora">
                                            <option value="-1"><?php echo $str_hora_now______; ?></option>
                                            <?php 
                                                $Lsql = "SELECT PREGUN_ConsInte__b , PREGUN_Texto_____b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$str_poblacion." AND PREGUN_Tipo______b = 10";
                                                $res = $mysqli->query($Lsql);
                                                while ($key = $res->fetch_object()) {
                                                    echo "<option value='".$key->PREGUN_ConsInte__b."'>".$key->PREGUN_Texto_____b."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label><?php echo $str_fecha_ocamp_1_; ?></label>
                                        <select class="form-control" id="masMenosHora" name="masMenosHora">
                                            <option value="1"><?php echo $str_fecha_ocamp_3_; ?></option>
                                            <option value="0"><?php echo $str_fecha_ocamp_4_; ?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label><?php echo $str_hora_ocamp__1_;?></label>
                                        <input type="text" class="form-control" id="txtRestaSumaHora" name="txtRestaSumaHora" placeholder="<?php echo $str_hora_ocamp__1_;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel box box-primary secTraspasoRegistros">
                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#s_3">
                                    AVANZADO
                                </a>
                            </h4>
                        </div>
                        <div id="s_3" class="panel-collapse collapse in">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="checkbox">
                                        <label for="sacarPasoAnterior">
                                            <input type="checkbox" name="sacarPasoAnterior" id="sacarPasoAnterior" value="1"> SACAR DEL PASO ANTERIOR
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5">
                                    <div class="checkbox">
                                        <label for="resucitarRegistros">
                                            <input type="checkbox" name="resucitarRegistros" id="resucitarRegistros" value="1"> VOLVER A EJECUTAR LA ACCION EN REGISTROS EXISTENTES
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel box box-primary secHorarios" style="display: none;">
                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#sec_horario">
                                    Horarios
                                </a>
                            </h4>
                        </div>
                        <div id="sec_horario" class="panel-collapse collapse in">
                            <!-- Horarios -->
                            <div class="row">
                                <div class="col-md-12">
                                <div class="col-md-12">
                                    <h4 id="tituloHorario">Horario comprendido entre</h4>
                                    <h5 id="subHorario"></h5>
                                </div>
                                    <!-- Lunes -->
                                    <div class="col-md-12">
                                    
                                        <div class="col-md-4 col-xs-4">
                                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                                            <div class="form-group">
                                                <label for="G10_C108" id="LblG10_C108">Lunes</label>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" readonly="readonly" value="-1" name="G10_C108" id="G10_C108" data-error="Before you wreck yourself" checked> 
                                                    </label>
                                                </div>
                                            </div>
                                            <!-- FIN DEL CAMPO SI/NO -->
                                        </div>

                                        <div class="col-md-4 col-xs-4">
                                            <!-- CAMPO TIMEPICKER -->
                                            <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                            <div class="form-group">
                                                <label for="G10_C109" id="LblG10_C109">Hora Inicial Lunes</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control input-sm Hora"  name="G10_C109" id="G10_C109" placeholder="HH:MM:SS" >
                                                    <div class="input-group-addon" id="TMP_G10_C109">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                        </div>

                                        <div class="col-md-4 col-xs-4">
                                            <!-- CAMPO TIMEPICKER -->
                                            <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                            <div class="form-group">
                                                <label for="G10_C110" id="LblG10_C110">Hora Final Lunes</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control input-sm Hora"  name="G10_C110" id="G10_C110" placeholder="HH:MM:SS" >
                                                    <div class="input-group-addon" id="TMP_G10_C110">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                        </div>
                                    </div>

                                    <!-- Martes -->
                                    <div class="col-md-12">
                                        <div class="col-md-4 col-xs-4">
                                            <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                                            <div class="form-group">
                                                <label for="G10_C111" id="LblG10_C111">Martes</label>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" readonly="readonly" value="-1" name="G10_C111" id="G10_C111" data-error="Before you wreck yourself" checked> 
                                                    </label>
                                                </div>
                                            </div>
                                            <!-- FIN DEL CAMPO SI/NO -->
                                        </div>

                                        <div class="col-md-4 col-xs-4">
                                            <!-- CAMPO TIMEPICKER -->
                                            <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                            <div class="form-group">
                                                <label for="G10_C112" id="LblG10_C112">Hora Inicial Martes</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control input-sm Hora"  name="G10_C112" id="G10_C112" placeholder="HH:MM:SS" >
                                                    <div class="input-group-addon" id="TMP_G10_C112">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                        </div>

                                        <div class="col-md-4 col-xs-4">
                                            <!-- CAMPO TIMEPICKER -->
                                            <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                            <div class="form-group">
                                                <label for="G10_C113" id="LblG10_C113">Hora Final Martes</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control input-sm Hora"  name="G10_C113" id="G10_C113" placeholder="HH:MM:SS" >
                                                    <div class="input-group-addon" id="TMP_G10_C113">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <!-- /.form group -->
                                        </div>
                                    </div>

                                    <!-- Miercoles -->
                                    <div class="col-md-12">
                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                                                <div class="form-group">
                                                    <label for="G10_C114" id="LblG10_C114">Miercoles</label>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" readonly="readonly" value="-1" name="G10_C114" id="G10_C114" data-error="Before you wreck yourself" checked> 
                                                        </label>
                                                    </div>
                                                </div>
                                                <!-- FIN DEL CAMPO SI/NO -->
                                        </div>

                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO TIMEPICKER -->
                                                <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                                <div class="form-group">
                                                    <label for="G10_C115" id="LblG10_C115">Hora Inicial Miercoles</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm Hora"  name="G10_C115" id="G10_C115" placeholder="HH:MM:SS" >
                                                        <div class="input-group-addon" id="TMP_G10_C115">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>

                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO TIMEPICKER -->
                                                <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                                <div class="form-group">
                                                    <label for="G10_C116" id="LblG10_C116">Hora Final Miercoles</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm Hora"  name="G10_C116" id="G10_C116" placeholder="HH:MM:SS" >
                                                        <div class="input-group-addon" id="TMP_G10_C116">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>

                                    </div>

                                    <!-- Jueves -->
                                    <div class="col-md-12">
                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                                                <div class="form-group">
                                                    <label for="G10_C117" id="LblG10_C117">Jueves</label>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" readonly="readonly" value="-1" name="G10_C117" id="G10_C117" data-error="Before you wreck yourself" checked> 
                                                        </label>
                                                    </div>
                                                </div>
                                                <!-- FIN DEL CAMPO SI/NO -->
                                        </div>

                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO TIMEPICKER -->
                                                <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                                <div class="form-group">
                                                    <label for="G10_C118" id="LblG10_C118">Hora Inicial Jueves</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm Hora"  name="G10_C118" id="G10_C118" placeholder="HH:MM:SS" >
                                                        <div class="input-group-addon" id="TMP_G10_C118">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>

                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO TIPO TEXTO -->
                                                <div class="form-group">
                                                    <label for="G10_C119" id="LblG10_C119">Hora Final Jueves</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm Hora"  name="G10_C119" id="G10_C119" placeholder="HH:MM:SS" >
                                                        <div class="input-group-addon" id="TMP_G10_C118">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- FIN DEL CAMPO TIPO TEXTO -->
                                        </div>
                                    </div>

                                    <!-- Viernes -->
                                    <div class="col-md-12">
                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                                                <div class="form-group">
                                                    <label for="G10_C120" id="LblG10_C120">Viernes</label>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" readonly="readonly" value="-1" name="G10_C120" id="G10_C120" data-error="Before you wreck yourself" checked> 
                                                        </label>
                                                    </div>
                                                </div>
                                                <!-- FIN DEL CAMPO SI/NO -->
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO TIMEPICKER -->
                                                <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                                <div class="form-group">
                                                    <label for="G10_C121" id="LblG10_C121">Hora Inicial Viernes</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm Hora"  name="G10_C121" id="G10_C121" placeholder="HH:MM:SS" >
                                                        <div class="input-group-addon" id="TMP_G10_C121">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO TIMEPICKER -->
                                                <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                                <div class="form-group">
                                                    <label for="G10_C122" id="LblG10_C122">Hora Final Viernes</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm Hora"  name="G10_C122" id="G10_C122" placeholder="HH:MM:SS" >
                                                        <div class="input-group-addon" id="TMP_G10_C122">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>
                                    </div>

                                    <!-- Sabado -->
                                    <div class="col-md-12">
                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                                                <div class="form-group">
                                                    <label for="G10_C123" id="LblG10_C123">Sabado</label>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" readonly="readonly" value="-1" name="G10_C123" id="G10_C123" data-error="Before you wreck yourself"  > 
                                                        </label>
                                                    </div>
                                                </div>
                                                <!-- FIN DEL CAMPO SI/NO -->
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO TIMEPICKER -->
                                                <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                                <div class="form-group">
                                                    <label for="G10_C124" id="LblG10_C124">Hora Inicial Sabado</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm Hora"  name="G10_C124" id="G10_C124" placeholder="HH:MM:SS" >
                                                        <div class="input-group-addon" id="TMP_G10_C124">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO TIMEPICKER -->
                                                <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                                <div class="form-group">
                                                    <label for="G10_C125" id="LblG10_C125">Hora Final Sabado</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm Hora"  name="G10_C125" id="G10_C125" placeholder="HH:MM:SS" >
                                                        <div class="input-group-addon" id="TMP_G10_C125">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>
                                    </div>

                                    <!-- Domingo -->
                                    <div class="col-md-12">
                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                                                <div class="form-group">
                                                    <label for="G10_C126" id="LblG10_C126">Domingo</label>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" readonly="readonly" value="-1" name="G10_C126" id="G10_C126" data-error="Before you wreck yourself"  > 
                                                        </label>
                                                    </div>
                                                </div>
                                                <!-- FIN DEL CAMPO SI/NO -->
                                        </div>


                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO TIMEPICKER -->
                                                <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                                <div class="form-group">
                                                    <label for="G10_C127" id="LblG10_C127">Hora Inicial Domingo</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm Hora"  name="G10_C127" id="G10_C127" placeholder="HH:MM:SS" >
                                                        <div class="input-group-addon" id="TMP_G10_C127">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO TIMEPICKER -->
                                                <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                                <div class="form-group">
                                                    <label for="G10_C128" id="LblG10_C128">Hora Final Domingo</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm Hora"  name="G10_C128" id="G10_C128" placeholder="HH:MM:SS" >
                                                        <div class="input-group-addon" id="TMP_G10_C128">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>
                                    </div>

                                    <!-- Festivos -->
                                    <div class="col-md-12">
                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
                                                <div class="form-group">
                                                    <label for="G10_C129" id="LblG10_C129">Festivos</label>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" readonly="readonly" value="-1" name="G10_C129" id="G10_C129" data-error="Before you wreck yourself"  > 
                                                        </label>
                                                    </div>
                                                </div>
                                                <!-- FIN DEL CAMPO SI/NO -->
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO TIMEPICKER -->
                                                <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                                <div class="form-group">
                                                    <label for="G10_C130" id="LblG10_C130">Hora Inicial festivos</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm Hora"  name="G10_C130" id="G10_C130" placeholder="HH:MM:SS" >
                                                        <div class="input-group-addon" id="TMP_G10_C130">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                                <!-- CAMPO TIMEPICKER -->
                                                <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                                                <div class="form-group">
                                                    <label for="G10_C131" id="LblG10_C131">Hora Final Festivos</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control input-sm Hora"  name="G10_C131" id="G10_C131" placeholder="HH:MM:SS" >
                                                        <div class="input-group-addon" id="TMP_G10_C131">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <!-- /.form group -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel box box-primary secEmailFiltros" style="display: none;">

                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#sec_emailFiltros">
                                    Filtros cuenta de correo entrante
                                </a>
                            </h4>
                        </div>
                        <div id="sec_emailFiltros" class="panel-collapse collapse in">
                            <!-- Horarios -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <h4 id="tituloHorarioFiltros">Filtros del paso INGRESE_PASO_AQUI configurado con el canal CANAL HERE</h4>
                                        <h5 id="subHorario"></h5>
                                    </div>
                                    
                                    <div class="col-md-12 col-xs-12 table-responsive">
                                        <table class="table" id="correoCondiciones">
                                            <thead>
                                                <tr>
                                                    <th><?php echo $title_mail_tipo_condicion;?></th>
                                                    <th><?php echo $title_mail_condicion;?></th>
                                                    <th></th>
                                                </tr>    
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                        <div class="pull-right" style="margin-top:5px; margin-bottom:10px">
                                            <button type="button" id="nuevaCondicionCorreo" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Agregar condición</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel box box-primary secConexionPasosSms" style="display: none;">

                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#sec_conexionesSms">
                                    Configuración
                                </a>
                            </h4>
                        </div>
                        <div id="sec_conexionesSms" class="panel-collapse collapse in">
                            <!-- Horarios -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <h4 id="tituloConexionPasosSms">
                                            En esta flecha no es necesario realizar una configuración, si deseas cambiar algún valor debes hacerlo en los pasos que están conectado a esta flecha.
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- secConexionBot -->
                    <div class="panel box box-primary secConexionBot" style="display: none;">

                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#sec_conexionesBot">
                                    Configuración
                                </a>
                            </h4>
                        </div>
                        <div id="sec_conexionesBot" class="panel-collapse collapse in">
                            <!-- Horarios -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <h4 id="tituloConexionBot">
                                            Interacciones que generan la comunicación entre bots, si deseas modificarlos debes hacerlo desde el bot de donde sale esta flecha
                                        </h4>
                                    </div>

                                    <div class="col-md-12">
                                        <table class="table" id="accionesDisparanBot">
                                            <thead>
                                                <tr>
                                                    <th>Acción que dispara el evento de pasar a otro bot</th>
                                                    <th>Respuesta generada después de la acción</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<!--
                    <div class="panel box box-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#s_3">
                                    <?php echo $str_acordeon_ti_3_;?>
                                </a>
                            </h4>
                        </div>
                        <div id="s_3" class="panel-collapse collapse in">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><?php echo $str_acordeon_ti_4_; ?></label>
                                        <select class="form-control" name="cmbCambioEstado" id="cmbCambioEstado">
                                            <option value="0"><?php echo $str_seleccione; ?></option>
                                            <?php 
                                                if($Poblacion['G2_C69'] != null){
                                                    $LsqlEstados = "SELECT PREGUN_ConsInte__OPCION_B as OPCION_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$Poblacion['G2_C69']." AND PREGUN_Texto_____b = 'ESTADO_DY';";
                                                    $resEstados = $mysqli->query($LsqlEstados);
                                                    $datosListas = $resEstados->fetch_array();

                                                    if($datosListas['OPCION_ConsInte__b'] != null){
                                                        $Lsql = "SELECT LISOPC_ConsInte__b ,LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$datosListas['OPCION_ConsInte__b'];
                                                        $resXultado = $mysqli->query($Lsql);
                                                        while($key = $resXultado->fetch_object()){
                                                            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
                                                        }    
                                                    }   
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
-->
                    
                </form> 
            </div>
            <div class="box-footer">
                <button class="btn btn-default" type="button"  data-dismiss="modal" >
                    <?php echo $str_cancela;?>
                </button>
                <button class="btn-primary btn pull-right" type="button" id="btnSaveFiltros">
                    <?php echo $str_guardar;?>
                </button>

                <!-- Este boton guarda los filtros de email Entrante -->
                <button class="btn-primary btn pull-right" style="display:none" type="button" id="btnSaveFiltrosEmailEntrante">
                    <?php echo $str_guardar;?>
                </button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $(".BorrarTabla").click(function(){
        $.ajax({
            url    : '<?=base_url?>carga/carga_CRUD.php?EliminarTablaTemporal=true',
            type   : 'POST',
            data   : { strNombreTablaTemporal_t : $("#TablaTemporal").val() }
        });
    });

    var datosGuion = [];
    var Aplica = 0;
    function before_save(){
        return true;
    }

    $(function(){
        $("#cancel").click(function(){
            <?php if(isset($_GET['ruta'])){ ?>
                window.location.href  = "<?=base_url?>index.php?page=dashEstrat&estrategia=<?php echo $_GET['estrategia'];?>&huesped=<?php echo $_SESSION['HUESPED'];?>";
            <?php } else { ?>
                window.location.href  = "<?=base_url?>modulo/estrategias"; 
            <?php } ?>
        });

        $("#txtRestaSumaFecha").numeric();
        $("#txtRestaSumaHora").numeric();


        $("#Save_modal").click(function(){
           salvarEsteFlujograma();
        });
    });

    function salvarEsteFlujograma(){
        save();
        if(before_save()){
            // alert("Hola");
            $.ajax({
               url: '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_CRUD.php',  
                type: 'POST',
                data: { mySavedModel : $("#mySavedModel").val() , id_estrategia : '<?php echo $_GET['estrategia'];?>' , guardar_flugrama : 'SI' , poblacion : '<?php echo $str_poblacion; ?>'},
                //una vez finalizado correctamente
                success: function(data){
                    if(data == '1'){
                        alertify.success('<?php echo $str_Exito; ?>');
                        window.location.reload(true);      
                    }else{
                        //Algo paso, hay un error
                        alertify.error('<?php echo $error_de_proceso; ?>');
                    }                
                },
                //si ha ocurrido un error
                error: function(){
                    after_save_error();
                    alertify.error('<?php echo $error_de_red; ?>');
                },
                beforeSend : function(){
                    $.blockUI({ 
                        message: '<h3><?php echo $str_message_wait;?></h3>' ,
                        css: { 
                            border: 'none', 
                            padding: '1px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5, 
                            color: '#fff' 
                        }
                    });
                },
                complete : function(){
                    $.unblockUI();
                }
            });
        }
    }
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="<?=base_url?>assets/plugins/Flowchart/flowchart.js"></script>
<script src="<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/configFlujograma.js"></script>
<script type="text/javascript" id="code">

    $('.close').click(function(){
       borrarLogCargue();
       $("#export-errores").css('display','none');
       $("#title_cargue").html('<?php echo $str_carga;?>');
    });
    var JoseExist = false;
    
    var colors = {
        blue:   "#00B5CB",
        orange: "#F47321",
        green:  "#C8DA2B",
        gray:   "#888",
        white:  "#F5F5F5"
    }
    var newCuantosvan = 1;
    var contador2 = 1;
     /**
    * function que elimina el log de los registros recien cargados
    */
    function borrarLogCargue(){
         $.ajax({
                url: '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_CRUD.php',
                type  : 'post',
                data: {'opcion':'borrarLogCargue'},
                dataType : 'json',
                success:function(data){
                    
                }
         }); 
    }
    function init() {
        if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
        var $ = go.GraphObject.make;  // for conciseness in defining templates
    myDiagram =
        $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
            {
                initialContentAlignment: go.Spot.Center,
                allowDrop: true,  // must be true to accept drops from the Palette
                "LinkDrawn": showLinkLabel,  // this DiagramEvent listener is defined below
                "LinkRelinked": showLinkLabel,
                "animationManager.duration": 800, // slightly longer than default (600ms) animation
                "undoManager.isEnabled": true  // enable undo & redo
            }
        );

    // when the document is modified, add a "*" to the title and enable the "Save" button
    myDiagram.addDiagramListener("Modified", function(e) {
        var button = document.getElementById("SaveButton");
            if (button) button.disabled = !myDiagram.isModified;
                var idx = document.title.indexOf("*");

        if (myDiagram.isModified) {
            if (idx < 0) document.title += "*";
        } else {
            if (idx >= 0) document.title = document.title.substr(0, idx);
        }

        //console.log(e);
        if (e.change === go.ChangedEvent.Remove) {
            alert(evt.propertyName + " removed node with key: " + e.oldValue.key);
        }
    });

    myDiagram.addModelChangedListener(function(evt) {
        // ignore unimportant Transaction events
        if (!evt.isTransactionFinished) return;
        var txn = evt.object;  // a Transaction
        if (txn === null) return;
        // iterate over all of the actual ChangedEvents of the Transaction
        txn.changes.each(function(e) {
            // ignore any kind of change other than adding/removing a node
            if (e.modelChange !== "nodeDataArray") return;
            // record node insertions and removals
            if (e.change === go.ChangedEvent.Insert) {
                console.log(evt.propertyName + " added node with key: " + e.newValue.key);
                salvarEsteFlujograma();
            } else if (e.change === go.ChangedEvent.Remove) {
                //console.log(evt.propertyName + " removed node with key: " + e.oldValue.key);
               borrarNode(e.oldValue.key);
            }
        });
    });

    myDiagram.addModelChangedListener(function(evt) {
        // ignore unimportant Transaction events
        if (!evt.isTransactionFinished) return;
        var txn = evt.object;  // a Transaction
        if (txn === null) return;
        // iterate over all of the actual ChangedEvents of the Transaction
        txn.changes.each(function(e) {
            // record node insertions and removals
            if (e.change === go.ChangedEvent.Property) {
                if (e.modelChange === "linkFromKey") {
                    //console.log(evt.propertyName + " changed From key of link: " +
                    //  e.object + " from: " + e.oldValue + " to: " + e.newValue);
                } else if (e.modelChange === "linkToKey") {
                    //console.log(evt.propertyName + " changed To key of link: " +
                    // e.object + " from: " + e.oldValue + " to: " + e.newValue);
                }
            } else if (e.change === go.ChangedEvent.Insert && e.modelChange === "linkDataArray") {
                // console.log(e.newValue);
                // console.log(e.newValue.from + " added link: " + e.newValue.to);
                
                let to = e.newValue.to;
                let from = e.newValue.from;
                let nodeDataArray = e.model.nodeDataArray;
                let linkDataArray = e.model.linkDataArray;

                let foundFrom = nodeDataArray.find(element => element.key == from);
                let found = nodeDataArray.find(element => element.key == to);

                // Valido si el paso es 17 Correo entrante se realiza la siguiente validacion
                if(foundFrom && foundFrom.tipoPaso == 17 && found && found.tipoPaso != 1){
                    load();
                    alertify.warning("Este paso solo puede ser asociado con campañas entrantes.");
                }

                // valido que solo pueden entrar flechas de sms saliente a sms entrante
                if(found && found.tipoPaso == 18){

                    if(foundFrom && foundFrom.tipoPaso == 8){

                        // Valido que exista un solo link
                        let links = linkDataArray.find(element => element.to == to && element.from != from);
                        if(!links){
                            crearFlecha(from, to);
                        }else{
                            load();
                            alertify.error("El paso de sms entrante al cual se esta intentando de conectar solo puede tener una conexion");    
                        }

                    }else{
                        load();
                        alertify.warning("Este paso solo puede ser asociado con campañas entrantes de sms.");
                    }
                }
            
            } else if (e.change === go.ChangedEvent.Remove && e.modelChange === "linkDataArray") {
                // console.log(evt.propertyName + " removed link: " + e.oldValue.from);
                // esta parte lo que hace es ejecutar la accion despues de ejecutar el link, debe ser otro evento 

                let nodeDataArray = e.model.nodeDataArray;

                let found = nodeDataArray.find(element => element.key == e.oldValue.from);

                if(found && (found.tipoPaso == 12 || found.tipoPaso == 14 || found.tipoPaso == 15 || found.tipoPaso == 16)){
                    load();
                    alertify.warning("Este accion no se puede eliminar desde aqui, se debe cambiar la accion desde el paso origen.");
                }else{
                    borrarLink(e.oldValue.from , e.oldValue.to);
                }

                if(found && found.tipoPaso == 8){
                    borrarLink(e.oldValue.from , e.oldValue.to);
                    // Cambio el estado para que no reciba sms
                    quitarConexionSmsEntrante(e.oldValue.from);
                }

                if(found && found.tipoPaso == 17){
                    eliminarFiltrosFlecha(e.oldValue.from);
                }
                
            }
        });
    });

    myDiagram.addDiagramListener("ObjectDoubleClicked", function (e) {
        //console.log(e.subject.part.data);
        //console.log(e.subject.part.actualBounds.toString());
        if (e.subject.part instanceof go.Link) {
            let link = e.subject.part;
            verInformacion(link.data.from , link.data.to);
        }
    });



    // helper definitions for node templates
    function nodeStyle() {
        return [
            // The Node.location comes from the "loc" property of the node data,
            // converted by the Point.parse static method.
            // If the Node.location is changed, it updates the "loc" property of the node data,
            // converting back using the Point.stringify static method.
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
            {
                selectionObjectName: "BODY",
                // the Node.location is at the center of each node
                locationSpot: go.Spot.Center,
                locationObjectName: "BODY",
                //isShadowed: true,
                //shadowColor: "#888",
                // handle mouse enter/leave events to show/hide the ports
                mouseEnter: function (e, obj) { showPorts(obj.part, true);  },
                mouseLeave: function (e, obj) { showPorts(obj.part, false);},
                doubleClick:function(e, obj){
                    if(obj.je){
                        LlamarModal(obj.je.tipoPaso, obj.je.key, obj.je.category );                    
                    }else{
                        LlamarModal(obj.mb.tipoPaso, obj.mb.key, obj.mb.category );
                    }
                }
            }
        ];
    }
    function textStyle() {
        return { font: "9pt  Segoe UI,sans-serif", stroke: "white" };
    }
    // Define a function for creating a "port" that is normally transparent.
    // The "name" is used as the GraphObject.portId, the "spot" is used to control how links connect
    // and where the port is positioned on the node, and the boolean "output" and "input" arguments
    // control whether the user can draw links from or to the port.
    function makePort(name, spot, output, input) {
        // the port is basically just a small circle that has a white stroke when it is made visible
        return $(go.Shape, "Rectangle",
            {
                fill: "transparent",
                stroke: null,  // this is changed to "white" in the showPorts function
                desiredSize: new go.Size(8, 8),
                alignment: spot,
                alignmentFocus: spot,  // align the port on the main Shape
                portId: name,  // declare this object to be a "port"
                fromSpot: spot,
                toSpot: spot,  // declare where links may connect at this port
                fromLinkable: output,
                toLinkable: input,  // declare whether the user may draw links to/from here
                cursor: "pointer" // show a different cursor to indicate potential link point
            });
    }
    // Esta configuracion muestra el nombre del paso
    function nombrePaso(nombre){
        // Este bloque muestra el nombre del paso
        return $(go.TextBlock,
            {
                text: nombre,
                alignment: new go.Spot(0.5, 1, 0, 15), alignmentFocus: go.Spot.Center,
                stroke: "black", font: "9pt Segoe UI, sans-serif",
                overflow: go.TextBlock.OverflowEllipsis,
                maxLines: 1,
                
            },
            new go.Binding("text", "nombrePaso")
        );
    }

    function estadoPaso(){
        return new go.Binding("stroke", "active", function(v) { return null });
        // return new go.Binding("stroke", "active", function(v) { return v === -1 ? config.colores.active : config.colores.disabled; });
    }

    function estadoFlecha(tipo){
        if(tipo === "link"){
            return new go.Binding("stroke", "active", function(v) { return v === -1 ? "#009fe3" : config.colores.disabled; });
        }

        if(tipo === "arrowhead"){
            return new go.Binding("fill", "active", function(v) { return v === -1 ? "#009fe3" : config.colores.disabled; });
        }
    }
        // define the Node templates for regular nodes
        var lightText = 'whitesmoke';

        myDiagram.nodeTemplateMap.add("",  // the default category
            $(go.Node, "Spot", nodeStyle(),
            // the main object is a Panel that surrounds a TextBlock with a rectangular Shape
                $(go.Panel, "Auto",
                    $(go.Shape, "Circle",
                        {
                            fill: "#C8DA2B",
                            stroke: null
                        },
                        new go.Binding("figure", "figure")
                    ),
                    $(go.TextBlock,
                        {
                            font: "18px FontAwesome",
                            stroke: lightText,
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            wrap: go.TextBlock.WrapFit,
                            editable: false
                        },
                        new go.Binding("text").makeTwoWay()
                    )
                ),
                // four named ports, one on each side:
                makePort("T", go.Spot.Top, false, true),
                makePort("L", go.Spot.Left, true, true),
                makePort("R", go.Spot.Right, true, true),
                makePort("B", go.Spot.Bottom, true, false)
            )
        );

        myDiagram.nodeTemplateMap.add("EnPhone",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50, 
                            fill: config.colores.orange, 
                            strokeWidth: 4,
                            stroke: null
                        },
                        estadoPaso()
                    ),

                    $(go.TextBlock,
                        {
                            margin: 8, 
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.campanaEntrante,
                            editable: false
                        },
                    )
                ),

                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.campanaEntrante),
                // three named ports, one on each side except the top, all output only:
                makePort("T", go.Spot.Top, true, true),
                makePort("L", go.Spot.Left, true, true),
                makePort("R", go.Spot.Right, true, true),
                makePort("B", go.Spot.Bottom, true, true)
            )
        );

        myDiagram.nodeTemplateMap.add("CargueDatos",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50,
                            fill: config.colores.green,
                            strokeWidth: 4,
                            stroke: null
                        },
                        new go.Binding("figure", "figure"),
                        estadoPaso()
                    ),
                    $(go.TextBlock,
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.cargador,
                            wrap: go.TextBlock.WrapFit,
                            editable: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.cargador),
                // three named ports, one on each side except the top, all output only:
                makePort("T", go.Spot.Top, true, false),
                makePort("L", go.Spot.Left, true, false),
                makePort("R", go.Spot.Right, true, false),
                makePort("B", go.Spot.Bottom, true, false)
            )
        );

        // myDiagram.nodeTemplateMap.add("EnChat",
        //     $(go.Node, "Spot", nodeStyle(),
        //         $(go.Panel, "Auto",
        //             $(go.Shape, "Circle",
        //                 {
        //                     fill: "#BDBDBD",
        //                     stroke: null
        //                 },
        //                 new go.Binding("figure", "figure")
        //             ),
        //             $(go.TextBlock,
        //                 {
        //                     font: "16px FontAwesome",
        //                     stroke: lightText,
        //                     text: "\uf0e5",
        //                     margin: 8,
        //                     maxSize: new go.Size(160, NaN),
        //                     wrap: go.TextBlock.WrapFit,
        //                     editable: true
        //                 }
        //             )
        //         ),
        //         // three named ports, one on each side except the top, all output only:
        //         makePort("L", go.Spot.Left, true, false),
        //         makePort("R", go.Spot.Right, true, false),
        //         makePort("B", go.Spot.Bottom, true, false)
        //     )
        // );

        myDiagram.nodeTemplateMap.add("EnMail",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50,
                            fill: config.colores.green,
                            strokeWidth: 4,
                            stroke: null
                        },
                        new go.Binding("figure", "figure"),
                        estadoPaso()
                    ),
                    $(go.TextBlock,
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.leadsCorreo,
                            wrap: go.TextBlock.WrapFit,
                            editable: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.leadsCorreo),
                // three named ports, one on each side except the top, all output only:
                makePort("T", go.Spot.Top, true, false),
                makePort("L", go.Spot.Left, true, false),
                makePort("R", go.Spot.Right, true, false),
                makePort("B", go.Spot.Bottom, true, false)
            )
        );

        myDiagram.nodeTemplateMap.add("Formul",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50, 
                            fill: config.colores.green,
                            strokeWidth: 4,
                            stroke: null
                        },
                        new go.Binding("figure", "figure"),
                        estadoPaso()
                    ),
                    $(go.TextBlock,
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.webform,
                            wrap: go.TextBlock.WrapFit,
                            editable: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.webform),
                // three named ports, one on each side except the top, all output only:
                makePort("T", go.Spot.Top, true, false),
                makePort("L", go.Spot.Left, true, false),
                makePort("R", go.Spot.Right, true, false),
                makePort("B", go.Spot.Bottom, true, false)
            )
        );

        myDiagram.nodeTemplateMap.add("webservice",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50, 
                            minSize: new go.Size(40, 40),
                            fill: config.colores.green,
                            strokeWidth: 4,
                            stroke: null
                        },
                        estadoPaso()
                    ),
                  $(go.TextBlock, 
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.webservice,
                            wrap: go.TextBlock.WrapFit,
                            editable: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.webservice),
                // Solo pueden salir las flechas de este paso
                makePort("T", go.Spot.Top, true, false),
                makePort("L", go.Spot.Left, true, false),
                makePort("R", go.Spot.Right, true, false),
                makePort("B", go.Spot.Bottom, true, false)
            )
        );

        myDiagram.nodeTemplateMap.add("ivrTexto",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50, 
                            minSize: new go.Size(40, 40),
                            fill: config.colores.blue,
                            strokeWidth: 4,
                            stroke: null
                        },
                        estadoPaso()
                    ),
                    $(go.TextBlock, 
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.bot,
                            wrap: go.TextBlock.WrapFit,
                            editable: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.bot),
                // Solo pueden salir las flechas de este paso
                // makePort("T", go.Spot.Top, true, true),
                // makePort("L", go.Spot.Left, true, true),
                // makePort("R", go.Spot.Right, true, true),
                // makePort("B", go.Spot.Bottom, true, true)
            )
        );

        myDiagram.nodeTemplateMap.add("salPhone",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50, 
                            fill: config.colores.orange,
                            strokeWidth: 4,
                            stroke: null
                        },
                        new go.Binding("figure", "figure"),
                        estadoPaso()
                    ),
                    $(go.TextBlock,
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.campanaSaliente,
                            wrap: go.TextBlock.WrapFit,
                            editable: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.campanaSaliente),
                // three named ports, one on each side except the bottom, all input only:
                makePort("T", go.Spot.Top, true, true),
                makePort("L", go.Spot.Left, true, true),
                makePort("R", go.Spot.Right, true, true),
                makePort("B", go.Spot.Bottom, true, true)
            )
        );

        myDiagram.nodeTemplateMap.add("salMail",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50, 
                            fill: config.colores.red, 
                            strokeWidth: 4,
                            stroke: null
                        },
                        new go.Binding("figure", "figure"),
                        estadoPaso()
                    ),
                    $(go.TextBlock,
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.correoSaliente,
                            wrap: go.TextBlock.WrapFit,
                            editable: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.correoSaliente),
                // three named ports, one on each side except the bottom, all input only:
                makePort("T", go.Spot.Top, true, true),
                makePort("L", go.Spot.Left, true, true),
                makePort("R", go.Spot.Right, true, true),
                makePort("B", go.Spot.Bottom, true, true)
            )
        );

        myDiagram.nodeTemplateMap.add("salSms",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50, 
                            fill: config.colores.red, 
                            strokeWidth: 4,
                            stroke: null
                        },
                        new go.Binding("figure", "figure"),
                        estadoPaso()
                    ),
                    $(go.TextBlock,
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.smsSaliete,
                            wrap: go.TextBlock.WrapFit,
                            editable: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.smsSaliete),
                // three named ports, one on each side except the bottom, all input only:
                makePort("T", go.Spot.Top, true, true),
                makePort("L", go.Spot.Left, true, true),
                makePort("R", go.Spot.Right, true, true),
                makePort("B", go.Spot.Bottom, true, true)
            )
        );

        myDiagram.nodeTemplateMap.add("salWhatsapp",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50, 
                            fill: config.colores.red, 
                            strokeWidth: 4,
                            stroke: null
                        },
                        new go.Binding("figure", "figure"),
                        estadoPaso()
                    ),
                    $(go.TextBlock,
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.whatsapp,
                            wrap: go.TextBlock.WrapFit,
                            editable: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.salWhatsapp),
                // three named ports, one on each side except the bottom, all input only:
                makePort("T", go.Spot.Top, true, true),
                makePort("L", go.Spot.Left, true, true),
                makePort("R", go.Spot.Right, true, true),
                makePort("B", go.Spot.Bottom, true, true)
            )
        );

        myDiagram.nodeTemplateMap.add("salCheck",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50, 
                            minSize: new go.Size(40, 40),
                            fill: config.colores.orange,
                            strokeWidth: 4,
                            stroke: null
                        },
                        estadoPaso()
                    ),
                    $(go.TextBlock, 
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.backoffice,
                            editable: false,
                            isMultiline: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.backoffice),
                // three named ports, one on each side except the bottom, all input only:
                makePort("T", go.Spot.Top, true, true),
                makePort("L", go.Spot.Left, true, true),
                makePort("R", go.Spot.Right, true, true),
                makePort("B", go.Spot.Bottom, true, true)
            )
        );

        myDiagram.nodeTemplateMap.add("comChat",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50, 
                            minSize: new go.Size(40, 40),
                            fill: config.colores.yellow,
                            strokeWidth: 4,
                            stroke: null
                        },
                        estadoPaso()
                    ),
                    $(go.TextBlock, 
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.chatWeb,
                            editable: false,
                            isMultiline: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.chatWeb),
                // three named ports, one on each side except the bottom, all input only:
                
            )
        );

        myDiagram.nodeTemplateMap.add("comWhatsapp",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50,
                            minSize: new go.Size(40, 40),
                            fill: config.colores.yellow, 
                            strokeWidth: 4,
                            stroke: null
                        },
                        estadoPaso()
                    ),
                    $(go.TextBlock, 
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.whatsapp,
                            editable: false,
                            isMultiline: false
                        }
                    ),
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.whatsapp),
                // three named ports, one on each side except the bottom, all input only:
            )
        );

        myDiagram.nodeTemplateMap.add("comFacebook",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"}, 
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50, 
                            minSize: new go.Size(40, 40),
                            fill: config.colores.yellow, 
                            strokeWidth: 4,
                            stroke: null
                        },
                        estadoPaso()
                    ),
                    $(go.TextBlock, 
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.facebook,
                            editable: false,
                            isMultiline: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.facebook),
                // three named ports, one on each side except the bottom, all input only:
            )
        );

        myDiagram.nodeTemplateMap.add("comEmail",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50, 
                            minSize: new go.Size(40, 40),
                            fill: config.colores.yellow, 
                            strokeWidth: 4,
                            stroke: null
                        },
                        estadoPaso()
                    ),
                    $(go.TextBlock, 
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.correoEntrante,
                            editable: false,
                            isMultiline: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.correoEntrante),
                // three named ports, one on each side except the bottom, all input only:
                makePort("T", go.Spot.Top, true, false),
                makePort("L", go.Spot.Left, true, false),
                makePort("R", go.Spot.Right, true, false),
                makePort("B", go.Spot.Bottom, true, false)
            )
        );

        myDiagram.nodeTemplateMap.add("comSms",
            $(go.Node, "Spot", nodeStyle(),
                $(go.Panel, "Auto",
                    {name: "BODY"},
                    $(go.Shape, "Circle",
                        {
                            width: 50, height: 50, 
                            fill: config.colores.yellow,
                            strokeWidth: 4,
                            stroke: null
                        },
                        new go.Binding("figure", "figure"),
                        estadoPaso()
                    ),
                    $(go.TextBlock,
                        {
                            margin: 8,
                            maxSize: new go.Size(160, NaN),
                            stroke: config.colorIcono.white,
                            font: config.font,
                            text: config.iconos.smsEntrante,
                            wrap: go.TextBlock.WrapFit,
                            editable: false
                        }
                    )
                ),
                // Esto es para mostrar el nombre del paso
                nombrePaso(config.nombrePasos.smsEntrante),
                // three named ports, one on each side except the bottom, all input only:
                makePort("T", go.Spot.Top, true, true),
                makePort("L", go.Spot.Left, true, true),
                makePort("R", go.Spot.Right, true, true),
                makePort("B", go.Spot.Bottom, true, true)
            )
        );

        // replace the default Link template in the linkTemplateMap
        myDiagram.linkTemplate =
            $(go.Link,  // the whole link panel
            {
                routing: go.Link.AvoidsNodes,
                curve: go.Link.JumpOver,
                corner: 5,
                toShortLength: 4,
                relinkableFrom: true,
                relinkableTo: true,
                reshapable: true,
                resegmentable: true,
                // mouse-overs subtly highlight links:
                mouseEnter: function(e, link) {
                    link.findObject("HIGHLIGHT").stroke = "rgba(30,144,255,0.2)";
                },
                mouseLeave: function(e, link) {
                    link.findObject("HIGHLIGHT").stroke = "transparent";
                }
            },
            new go.Binding("points").makeTwoWay(),
            $(go.Shape,  // the highlight shape, normally transparent
                {
                    isPanelMain: true,
                    strokeWidth: 8,
                    stroke: "transparent",
                    name: "HIGHLIGHT"
                }
            ),
            $(go.Shape,  // the link path shape
                {
                    isPanelMain: true,
                    stroke: config.colores.disabled,
                    strokeWidth: 2
                },
                estadoFlecha("link")
            ),
            $(go.Shape,  // the arrowhead
                {
                    toArrow: "standard",
                    stroke: null,
                    fill: config.colores.disabled
                },
                estadoFlecha("arrowhead")
            ),
            $(go.Panel, "Auto",  // the link label, normally not visible
                {
                    visible: false,
                    name: "LABEL",
                    segmentIndex: 2,
                    segmentFraction: 0.5
                },
                new go.Binding("visible", "visible").makeTwoWay(),
                $(go.Shape, "Rectangle",  // the label shape
                {
                    fill: "#F8F8F8",
                    stroke: null
                }),
                $(go.TextBlock, "??",  // the label
                {
                    textAlign: "center",
                    font: "8pt helvetica, arial, sans-serif",
                    stroke: "#333333",
                    editable: true
                },
                new go.Binding("text").makeTwoWay())
            )
        );
        // Make link labels visible if coming out of a "conditional" node.
        // This listener is called by the "LinkDrawn" and "LinkRelinked" DiagramEvents.
        function showLinkLabel(e) {
            var label = e.subject.findObject("LABEL");
            if (label !== null) label.visible = (e.subject.fromNode.data.figure === "Circle");
        }
        // temporary links used by LinkingTool and RelinkingTool are also orthogonal:
        myDiagram.toolManager.linkingTool.temporaryLink.routing = go.Link.Orthogonal;
        myDiagram.toolManager.relinkingTool.temporaryLink.routing = go.Link.Orthogonal;
        load();  // load an initial diagram from some JSON text
        // initialize the Palette that is on the left side of the page
        myPalette =
            $(go.Palette, "myPaletteDiv",  // must name or refer to the DIV HTML element
            {
                "animationManager.duration": 800, // slightly longer than default (600ms) animation
                nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
                model: new go.GraphLinksModel([  // specify the contents of the Palette
                    { category: "CargueDatos", text: "Cargue de Datos",  tipoPaso : 5, figure : "Circle"},
                    { category: "Formul", text: "Formulario" ,  tipoPaso : 4, figure : "Circle"},
                    { category: "EnMail", text: "Lead",  tipoPaso : 10, figure : "Circle" },
                    { category: "webservice", text: "webService" ,  tipoPaso : 11, figure : "Circle"},
                    { category: "comChat", text: "Comunicacion chat web", tipoPaso : 14, figure : "Circle"},
                    { category: "comWhatsapp", text: "Comunicacion chat whatsapp", tipoPaso : 15, figure : "Circle"},
                    { category: "comFacebook", text: "Comunicacion chat facebook", tipoPaso : 16, figure : "Circle"},
                    { category: "comEmail", text: "Comunicacion email", tipoPaso : 17, figure : "Circle"},
                    { category: "comSms", text: "Comunicacion sms", tipoPaso : 18, figure : "Circle"},
                    { category: "ivrTexto", text: "ivrtexto" ,  tipoPaso : 12, figure : "Circle"},
                    { category: "EnPhone", text: "LLamandas entrantes", tipoPaso : 1, figure : "Circle"},
                    { category: "salPhone" , text: "Llamadas salientes",  tipoPaso : 6, figure : "Circle" },
                    { category: "salCheck" , text: "Tareas terminadas" , tipoPaso : 9 },
                    { category: "salMail" , text: "Correo saliente",  tipoPaso : 7, figure : "Circle" },
                    { category: "salSms" , text: "Mensajes de texto salientes",  tipoPaso : 8, figure : "Circle" },
                    { category: "salWhatsapp" , text: "Plantillas wa salientes",  tipoPaso : 13, figure : "Circle" }
                ])
            });
        // The following code overrides GoJS focus to stop the browser from scrolling
        // the page when either the Diagram or Palette are clicked or dragged onto.
        function customFocus() {
            var x = window.scrollX || window.pageXOffset;
            var y = window.scrollY || window.pageYOffset;
            go.Diagram.prototype.doFocus.call(this);
            window.scrollTo(x, y);
        }
        myDiagram.doFocus = customFocus;
        myPalette.doFocus = customFocus;
    } // end init
    
    // Make all ports on a node visible when the mouse is over the node
    function showPorts(node, show) {
        var diagram = node.diagram;
        if (!diagram || diagram.isReadOnly || !diagram.allowLink) return;
        node.ports.each(function(port) {
            port.stroke = (show ? "white" : null);
        });
    }
    // Show the diagram's model in JSON format that the user may edit
    function save() {
        document.getElementById("mySavedModel").value = myDiagram.model.toJson();
        myDiagram.isModified = false;
    }
    function load() {
        myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
    }

    $(document).ready(function(){
        init();
    });
</script>

<!-- funcion para llamar desde aqui esta parte -->
<script type="text/javascript">
    $(function(){
        cargarDatos(<?php echo $str_poblacion; ?>);
    });

    function cargarDatos(idGuion){
        $.ajax({
            url      : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G10/G10_extender_funcionalidad.php?llenarDatosPregun=true',
            type     : 'post',
            data     : { guion  :  idGuion },
            dataType : 'json',
            success  : function(data){
                datosGuion = data;
            }
        })
    }
    
    function LlamarModal(tipoPaso, key, category){
        var invocador = tipoPaso;
        var llaveInvocar = key;
        var llevaCampan = '';
        $.ajax({
            url  : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?hayCampana=true',
            type : 'post',
            data : { pasoId :  llaveInvocar },
            success : function(data){
                llevaCampan=data;
            }
        });
        if(invocador == 1){
            //window.location.href = 'index.php?page=llam_saliente&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>'
            $.ajax({
                url  : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?tieneCampana=true',
                type : 'post',
                data : { pasoId :  llaveInvocar },
                success : function(data){
                    if(data != '0'){
                        //window.location.href = 'index.php?page=entrantes&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>&ruta=1';
                        window.location.href = '<?=base_url?>modulo/ruta/entrantes/'+llevaCampan+'/<?php echo $str_poblacion; ?>/1';
                    }else{
                        /* no tiene campaña asociada */
                        $("#title_estratCampan").html('<?php echo $campan_title___;?>');
                        $("#id_estpas_mio").val(llaveInvocar);
                        $("#backoffice").hide();
                        $("#campanasNormales").show();
                        $("#AddTipoCampan").val(1);
                        $("#crearCampanhasNueva").modal();
                    }
                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                }
            });

        }else if(invocador == 2){

            $("#title_estrat").html('<?php echo $str_strategia_edicion.' '; ?>'+category);
            $("#frameContenedor").attr('src', '');

        }else if(invocador == 3){
            $.ajax({
                url  : '<?=base_url?>mostrar_popups.php?view=mail_entrante&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>',
                type : 'get',
                success : function(data){
                    $("#title_estrat").html('<?php echo $str_strategia_edicion.' '; ?>'+category);
                    $("#divIframe").html(data);
                    $("#editarDatos").modal();
                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                }
            });

        }else if(invocador == 4){   
            $.ajax({
                url  : '<?=base_url?>mostrar_popups.php?view=web_form&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>',
                type : 'get',
                success : function(data){
                    $("#title_estratPasosC").html('<?php echo $str_strategia_edicion.' '; ?>'+'formulario web');
                    $("#divIframePasosCortos").html(data);
                    $("#pasoscortos").modal();
                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                }
            });
            
        }else if(invocador == 5){
            $.ajax({
                url  : '<?=base_url?>mostrar_popups.php?view=cargueDatos&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>',
                type : 'get',
                success : function(data){
                    $("#title_estrat").html('<?php echo $str_strategia_edicion.' '; ?>'+category);
                    $("#divIframe").html(data);
                    $("#editarDatos").modal();
                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                }
            });

        }else if(invocador == 6){
            /*$.ajax({
                url  : '<?=base_url?>mostrar_popups.php?view=campan&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>',
                type : 'get',
                success : function(data){
                    $("#title_estrat").html('<?php //echo $str_strategia_edicion.' '; ?>'+category);
                    $("#divIframe").html(data);
                    $("#editarDatos").modal();
                },
                beforeSend : function(){
                   
                },
                complete : function(){
                    $.unblockUI();
                }
            });*/
            /* Toca primero preguntar si tiene o no tiene una campaña asociada */
            $.ajax({
                url  : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?tieneCampana=true',
                type : 'post',
                data : { pasoId :  llaveInvocar },
                success : function(data){
                    if(data != '0'){
                        //window.location.href = 'index.php?page=campan&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>&ruta=1';
                        window.location.href = '<?=base_url?>/modulo/ruta/campan/'+llevaCampan+'/<?php echo $str_poblacion; ?>/1';
                    }else{
                        /* no tiene campaña asociada */
                        $("#title_estratCampan").html('<?php echo $campan_title___;?>');
                        $("#backoffice").hide();
                        $("#campanasNormales").show();
                        $("#id_estpas_mio").val(llaveInvocar);
                        $("#AddTipoCampan").val(2);
                        $("#crearCampanhasNueva").modal();
                    }
                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                }
            });
           
           

        }else if(invocador == 7){
            $.ajax({
                url  : '<?=base_url?>mostrar_popups.php?view=mail&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>',
                type : 'get',
                success : function(data){
                    $("#title_estrat").html('<?php echo $str_strategia_edicion.' '; ?>'+category);
                    $("#divIframe").html(data);
                    $("#editarDatos").modal();
                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                }
            });
            
        }else if(invocador == 8){
            $.ajax({
                url  : '<?=base_url?>mostrar_popups.php?view=sms_S&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>',
                type : 'get',
                success : function(data){
                    $("#title_estrat").html('<?php echo $str_strategia_edicion.' '; ?>'+category);
                    $("#divIframe").html(data);
                    $("#editarDatos").modal();
                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                }
            });
               
        }else if(invocador == 9){

            getDatosBackoffice(llaveInvocar);
            

            // $.ajax({
            //     url  : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?tieneCampana=true',
            //     type : 'post',
            //     data : { pasoId :  llaveInvocar },
            //     success : function(data){
            //         if(data != '0'){
            //             window.location.href = 'index.php?page=backoffice&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>&ruta=1';
            //         }else{
            //             /* no tiene campaña asociada */
            //             $("#id_estpas_mio").val(llaveInvocar);
            //             $("#AddTipoCampan").val(3);
            //             $("#title_estratCampan").html('<?php echo $campan_titleB__;?>');
            //             $("#backoffice").show();
            //             $("#campanasNormales").hide();
            //             $("#crearCampanhasNueva").modal();
            //         }
            //     },
            //     beforeSend : function(){
            //         $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
            //     },
            //     complete : function(){
            //         $.unblockUI();
            //     }
            // });
        }else if(invocador == 10){
            $("#form-lead")[0].reset();

            $('#id_estpas').val(llaveInvocar);

            $.ajax({
                url  : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?getLead=true',
                type : 'post',
                dataType : 'json',
                data : { pasoId :  llaveInvocar },
                success : function(data){

                    if(data.dataLead.nombre != null){
                        $('#nombreLead').val(data.dataLead.nombre);
                    }else{
                        $('#nombreLead').val("MAIL_ENTRANTE_"+llaveInvocar);    
                    }
                    if(data.dataLead.quienRecibe != null){
                        if(data.dataLead.leadActivo==1){
                            $('#leadActivo').prop('checked','checked');
                        }else{
                            $('#leadActivo').prop('checked',false);
                        }
                        $('#leadQuienRecibe').val(data.dataLead.quienRecibe).change();
                        $('#leadTipoCondicion').val(data.dataLead.tipoCondicion).change();
                        $('#leadCondicion').val(data.dataLead.condicion);
                    }else{
                        $('#leadActivo').prop('checked','checked'); 
                    }
                    
                    $("#LeadCampos tbody").html('');
                    $.each(data.camposEmparejarLead, function(i, item){
                        var campos = '';

                        campos += '<tr>';
                        campos += '<input type="hidden" name="listCamposLead[]" id="listCamposLead_'+item.id+'" value="'+item.id+'">'
                        campos += '<td>';
                        campos += '<input type="text" required class="form-control input-sm" name="tagInicial_'+item.id+'" id="tagInicial_'+item.id+'" placeholder="<ejemplo>">';
                        campos += '</td>';
                        campos += '<td>';
                        campos += '<input type="text" class="form-control input-sm" name="tagFinal_'+item.id+'" id="tagFinal_'+item.id+'" placeholder="</ejemplo>">';
                        campos += '</td>';
                        campos += '<td>';
                        campos += '<select name="campoBD_'+item.id+'" id="campoBD_'+item.id+'" class="form-control input-sm">';
                        campos += '<option value="0">Seleccione</option>';
                        campos += opcionesCampoPregun;         
                        campos += '</select>';
                        campos += '</td>';
                        campos += '<td>';
                        campos += '<button type="button" id="'+item.id+'" class="btn btn-danger btn-sm borrarCampoEmparejarLead"><i class="fa fa-trash"></i></button>';
                        campos += '</td>';
                        campos += '</tr>';

                        $("#LeadCampos tbody").append(campos);

                        $('#tagInicial_'+item.id).val(item.tagInicial);
                        $('#tagFinal_'+item.id).val(item.tagFinal);
                        $('#campoBD_'+item.id).val(item.campoBD).change();
                    });

                    $(".borrarCampoEmparejarLead").click(function(){
                        var id = $(this).attr('id');
                        var self = $(this);
                        alertify.confirm("<?php echo $str_message_generico_D;?>?", function (e) {
                            if (e) {
                                $.ajax({
                                    url     : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php',
                                    type    : 'POST',
                                    dataType: 'json',
                                    data    : { borrarCampoEmparejarLead : true, idCampo : id},
                                    beforeSend : function(){
                                        $.blockUI({ 
                                            baseZ: 2000,
                                            message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait;?>' });
                                    },
                                    complete : function(){
                                        $.unblockUI();
                                    },
                                    success : function(data){
                                        alertify.success(data.message);
                                        self.closest('tr').remove();
                                    }
                                });
                            }
                        }); 
                        
                    });

                    $("#modalLead").modal();

                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                }
            });
        }else if(invocador == 11){
            //Esto se ejecuta cuando se hace click en bola de webservice
            $("#webServiceForm")[0].reset();
            $('#pasowsId').val(llaveInvocar);

            $.ajax({
                url  : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?getwebservice=true',
                type : 'post',
                dataType : 'json',
                data : { pasoId :  llaveInvocar, bd : '<?php echo $str_poblacion; ?>'},
                success : function(data){
                    console.log(data);
                    if(data.paso.nombre != null && data.paso.nombre != ''){
                        $('#wsNombre').val(data.paso.nombre);
                    }else{
                        $('#wsNombre').val("WEB_SERVICE_"+llaveInvocar);
                    }

                    $("#campoOrigen").val(data.origen);

                    // Aqui armo el ejemplo con los datos personalizados
                    if(data.campos){
                        let campos = '';
                        let camposOption = '';
                        let cam = data.campos;

                        cam.forEach((element, index) => {
                            if(index == cam.length - 1)
                                campos += `\t\t "${element.id}": "Incluya aqui el ${element.texto}" \n`;
                            else
                                campos += `\t\t "${element.id}": "Incluya aqui el ${element.texto}", \n`;

                            camposOption += `<option value="${element.id}">${element.texto}</option>`;
                        });
                        
                        $("#cllave").html(camposOption);
                        $("#valoresws").val(campos);

                        generarEjemplo();
                    }

                    $("#webserviceModal").modal();
                },
                beforeSend: function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete: function(){
                    $.unblockUI();
                }
                
            });
        }else if(invocador == 12){

            obtenerBot(llaveInvocar, category);

        }else if(invocador == 13){
            $.ajax({
                url: '<?=base_url?>mostrar_popups.php?view=sal_whatsapp&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>',
                type: 'get',
                success : function(data){
                    $("#title_cargue").html("Paso Saliente Whatsapp");
                    $("#divIframe").html(data);
                    $("#editarDatos").modal();
                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                }
            });
        }else if(invocador == 14){
            $.ajax({
                url: '<?=base_url?>mostrar_popups.php?view=com_chat_web&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>',
                type: 'get',
                success : function(data){
                    $("#title_cargue").html("Paso comunicación chat web");
                    $("#divIframe").html(data);
                    $("#editarDatos").modal();
                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    // $.unblockUI();
                }
            });
        }else if(invocador == 15){
            $.ajax({
                url: '<?=base_url?>mostrar_popups.php?view=com_chat_whatsapp&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>',
                type: 'get',
                success : function(data){
                    $("#title_cargue").html("Paso comunicación chat whatsapp");
                    $("#divIframe").html(data);
                    $("#editarDatos").modal();
                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    // $.unblockUI();
                }
            });
        }else if(invocador == 16){
            $.ajax({
                url: '<?=base_url?>mostrar_popups.php?view=com_chat_facebook&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>',
                type: 'get',
                success : function(data){
                    $("#title_cargue").html("Paso comunicación chat facebook messenger");
                    $("#divIframe").html(data);
                    $("#editarDatos").modal();
                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    // $.unblockUI();
                }
            });
        }else if(invocador == 17){
            $.ajax({
                url: '<?=base_url?>mostrar_popups.php?view=com_email_entrante&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>',
                type: 'get',
                success : function(data){
                    $("#title_cargue").html("Paso comunicación correo entrante");
                    $("#divIframe").html(data);
                    $("#editarDatos").modal();
                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    // $.unblockUI();
                }
            });
        }else if(invocador == 18){
            $.ajax({
                url: '<?=base_url?>mostrar_popups.php?view=com_sms_entrante&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>',
                type: 'get',
                success : function(data){
                    $("#title_cargue").html("Paso comunicación sms entrante");
                    $("#divIframe").html(data);
                    $("#editarDatos").modal();
                },
                beforeSend : function(){
                    $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    // $.unblockUI();
                }
            });
        }
        
    }

    function borrarNode(id){
        alertify.confirm("<?php echo $str_segurida__Ale_; ?>", function (e) {
            //Si la persona acepta
            if (e) {
                alertify.confirm("<?php echo $str_segurida__Al2_; ?>", function (e) {
                    //Si la persona acepta
                    if (e) {
                        $.ajax({
                            url   : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_CRUD.php',
                            type  : 'post',
                            data  : { borrarFlujograma : true, id_paso :  id },
                            success : function(data){
                                console.log(data);
                            }
                        });
                    } else {
                        
                    }
                });                 
            } else {
                
            }
        });    
    }

    function borrarLink(from , to){
        $.ajax({
            url   : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_CRUD.php',
            type  : 'post',
            data  : { borrarFlujogramaLink : true, from :  from , to : to },
            success : function(data){
                console.log(data);
            }
        });
    }

    function verInformacion(from , to){
        /* validamos que el paso principal sea de los que pueden ser saliente o entrante */
        $("#txtCantidadRegistrps").val('');
        $("#divCantidadCampan").hide();
        $("#divFiltrosCampan").hide();
        $("#filtros").html("");
        $("#consultaCamposWhere")[0].reset();
        $("#filtrosCampanha").modal('hide');
        $.ajax({
            url   : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?validaPasosPrincipales=true',
            type  : 'post',
            data  : { paso :  from },
            success : function(data){
                if(data != '0'){
                    Aplica = data;
                    campanhasHave(from);
                    /* es una campaña */
                    /* toca validar que las que lleguen si sean las que son */
                    $.ajax({
                        url   : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?validaPasosSecundarios=true',
                        type  : 'post',
                        data  : { paso :  to },
                        success : function(data){
                            if(data == '1'){
                                $("#id_paso_from").val(from);
                                $("#id_paso_to").val(to);
                                   
                                
                                // Aqui valido para ver si muestro la lista normal o la de horarios
                                $.ajax({
                                    url   : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?validarPasoChat=true',
                                    type  : 'post',
                                    data  : { pasoFrom : from , pasoTo : to },
                                    dataType : 'json',
                                    success : function(data){                                  
                                        console.log(data);

                                        if(data.esPasoChat){
                                            $(".secHorarios").show();
                                            $(".secTraspasoRegistros").hide();
                                            $(".secEmailFiltros").hide();
                                            $(".secConexionBot").hide();

                                            if(data.esDentroHorario && data.esFueraHorario){
                                                $("#tituloHorario").text("Acción de horario completo. ");
                                            }else{
                                                if(data.esDentroHorario){
                                                    $("#tituloHorario").text("Acción dentro de horario. ");
                                                }else if(data.esFueraHorario){
                                                    $("#tituloHorario").text("Acción fuera de horario. ");
                                                }
                                            }

                                            $("#subHorario").html("Si desea modificar estos horarios debe hacerlo desde el paso de chat entrante <strong>"+data.nombrePaso+"</strong>");

                                            //Horarios
                                            if(data.horario.length > 0){

                                                if(data.esDentroHorario && data.esFueraHorario){
                                                    setHorario24();
                                                }else{

                                                    $("#G10_C108").prop('checked', false);  // Check lunes
                                                    $("#G10_C111").prop('checked', false);  // Check martes
                                                    $("#G10_C114").prop('checked', false);  // Check miercoles
                                                    $("#G10_C117").prop('checked', false);  // Check jueves
                                                    $("#G10_C120").prop('checked', false);  // Check viernes
                                                    $("#G10_C123").prop('checked', false);  // Check sabado
                                                    $("#G10_C126").prop('checked', false);  // Check domingo
                                                    $("#G10_C129").prop('checked', false);  // Check festivos

                                                    if(data.esFueraHorario){
                                                        setHorario24();
                                                    }

                                                    data.horario.forEach(element => {
                                                        
                                                        if(element.dia_inicial == 1 && element.dia_final == 1){
    
                                                            $("#G10_C108").prop('checked', true);                                                        

                                                            if(data.esDentroHorario){
                                                                $("#G10_C109").val(element.momento_inicial);
                                                                $("#G10_C110").val(element.momento_final); 
                                                            }else if(data.esFueraHorario){
                                                                formaterFueraHorario(element.momento_inicial, element.momento_final, "G10_C109", "G10_C110");
                                                            }
                                                            
                                                        }
                                                        
                                                        if(element.dia_inicial == 2 && element.dia_final == 2){

                                                            $("#G10_C111").prop('checked', true);
    
                                                            if(data.esDentroHorario){
                                                                $("#G10_C112").val(element.momento_inicial);
                                                                $("#G10_C113").val(element.momento_final); 
                                                            }else if(data.esFueraHorario){
                                                                formaterFueraHorario(element.momento_inicial, element.momento_final, "G10_C112", "G10_C113");
                                                            }
        
                                                        }

                                                        if(element.dia_inicial == 3 && element.dia_final == 3){
                                                            if(!$("#G10_C114").is(':checked')){
                                                                $("#G10_C114").prop('checked', true);  
                                                            }
    
                                                            if(data.esDentroHorario){
                                                                $("#G10_C115").val(element.momento_inicial);
                                                                $("#G10_C116").val(element.momento_final); 
                                                            }else if(data.esFueraHorario){
                                                                formaterFueraHorario(element.momento_inicial,element.momento_final,"G10_C115","G10_C116");
                                                            }
                                                            
                                                        }

                                                        if(element.dia_inicial == 4 && element.dia_final == 4){
                                                            
                                                            $("#G10_C117").prop('checked', true);  
                                                            
                                                            if(data.esDentroHorario){
                                                                $("#G10_C118").val(element.momento_inicial);
                                                                $("#G10_C119").val(element.momento_final); 
                                                            }else if(data.esFueraHorario){
                                                                formaterFueraHorario(element.momento_inicial,element.momento_final,"G10_C118","G10_C119");
                                                            }
                                                            
                                                        }

                                                        if(element.dia_inicial == 5 && element.dia_final == 5){
                                                            
                                                            $("#G10_C120").prop('checked', true);  
                                                            
                                                            if(data.esDentroHorario){
                                                                $("#G10_C121").val(element.momento_inicial);
                                                                $("#G10_C122").val(element.momento_final); 
                                                            }else if(data.esFueraHorario){
                                                                formaterFueraHorario(element.momento_inicial,element.momento_final,"G10_C121","G10_C122");
                                                            }
                                                            
                                                        }
                                                        
                                                        if(element.dia_inicial == 6 && element.dia_final == 6){
                                                            
                                                            $("#G10_C123").prop('checked', true);  
                                                            
                                                            if(data.esDentroHorario){
                                                                $("#G10_C124").val(element.momento_inicial);
                                                                $("#G10_C125").val(element.momento_final); 
                                                            }else if(data.esFueraHorario){
                                                                formaterFueraHorario(element.momento_inicial,element.momento_final,"G10_C124","G10_C125");
                                                            }
                                                            
                                                        }
                                                        
                                                        if(element.dia_inicial == 7 && element.dia_final == 7){
                                                            
                                                            $("#G10_C126").prop('checked', true);  
                                                            
                                                            if(data.esDentroHorario){
                                                                $("#G10_C127").val(element.momento_inicial);
                                                                $("#G10_C128").val(element.momento_final); 
                                                            }else if(data.esFueraHorario){
                                                                formaterFueraHorario(element.momento_inicial,element.momento_final,"G10_C127","G10_C128");
                                                            }
                                                            
                                                        }
                                                        
                                                        if(element.dia_inicial == 8 && element.dia_final == 8){
                                                            
                                                            $("#G10_C129").prop('checked', true);  
                                                            
                                                            if(data.esDentroHorario){
                                                                $("#G10_C130").val(element.momento_inicial);
                                                                $("#G10_C131").val(element.momento_final); 
                                                            }else if(data.esFueraHorario){
                                                                formaterFueraHorario(element.momento_inicial,element.momento_final,"G10_C130","G10_C131");
                                                            }
                                                            
                                                        }

                                                    });
                                                    
                                                }

                                            }

                                            inicializarTimePicker();
                                            soloLectura();
                                        }else{

                                            // Oculto las secciones
                                            $(".secHorarios").hide();
                                            $(".secTraspasoRegistros").hide();
                                            $(".secEmailFiltros").hide();
                                            $(".secConexionPasosSms").hide();
                                            $("#btnSaveFiltrosEmailEntrante").hide();
                                            $(".secConexionBot").hide();

                                            $("#btnSaveFiltrosEmailEntrante").prop('disabled', false);

                                            // Traigo el tipo_paso para validar si el paso from en 17 o correo entrante
                                            if(data.tipoPasoFrom == 17){

                                                $(".secEmailFiltros").show();
                                                $("#correoCondiciones tbody").html('');
                                                $("#btnSaveFiltrosEmailEntrante").show();
                                                $("#btnSaveFiltros").hide();
                                                $("#btnSaveFiltrosEmailEntrante").prop('disabled', false);
                                                cantFiltrosEmail = 0;

                                                // Traigo los datos
                                                $.ajax({
                                                    url   : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G31/G31_CRUD.php?getFiltros=true',
                                                    type  : 'post',
                                                    data  : { pasoFrom : from , pasoTo : to },
                                                    dataType : 'json',
                                                    success: function(data){
                                                        if(data.estado && data.estado == 'ok'){

                                                            let titulo = "Filtros del paso <strong>"+data.configCorreo.nombre+"</strong> configurado con la cuenta de correo <strong>"+data.canalCorreo.direccion_correo_electronico+"<strong>";
                                                            $("#tituloHorarioFiltros").html(titulo);

                                                            // Los filtros los muestro aqui
                                                            $.each(data.filtros, function(index, value){
                                                                agregarFila('edit');
                                                                $("#campo_"+index).val(value.id);
                                                                $("#mailTipoCondicion_"+index).val(value.filtro);
                                                                
                                                                if(value.filtro != 100){
                                                                    if(value.condicion == ''){
                                                                        $("#mailCondicion_"+index).val('null');
                                                                    }else{
                                                                        $("#mailCondicion_"+index).val(value.condicion);
                                                                    }
                                                                }

                                                                $("#mailTipoCondicion_"+index).change();
                                                            });
                                                        }else if(data.estado && data.estado == 'fallo'){
                                                            $("#tituloHorarioFiltros").html(data.mensaje);
                                                            $("#btnSaveFiltrosEmailEntrante").prop('disabled', true);
                                                        }
                                                    },
                                                    error: function(data){
                                                        if(data.responseText){
                                                            alertify.error("Hubo un error al guardar la información" + data.responseText);
                                                        }else{
                                                            alertify.error("Hubo un error al guardar la información");
                                                        }
                                                    }
                                                });

                                            }else if(data.tipoPasoFrom == 8){
                                                $("#btnSaveFiltros").hide();
                                                $(".secConexionPasosSms").show();
                                            // Esto lo comente porque puede ser que desde un bot puedan salir flechas a ciertos lugares    
                                            // }else if(data.tipoPasoFrom == 12){
                                            //     $("#btnSaveFiltros").hide();
                                            //     $(".secConexionBot").show();
                                            //     $("#accionesDisparanBot tbody").html('');
                                            //     // Traigo los datos
                                            //     $.ajax({
                                            //         url   : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G26/G26_CRUD.php?getBotFiltros=true',
                                            //         type  : 'post',
                                            //         data  : { pasoFrom : from , pasoTo : to },
                                            //         dataType : 'json',
                                            //         success: function(data){
                                            //             if(data.estado && data.estado == 'ok'){

                                            //                 //Los filtros los muestro aqui
                                            //                 $.each(data.filtrosBot, function(index, value){
                                                                
                                            //                     let row = `
                                            //                         <tr>
                                            //                             <td>${value.tags}</td>
                                            //                             <td>${value.respuesta}</td>
                                            //                         </tr>
                                            //                     `;

                                            //                     $("#accionesDisparanBot tbody").append(row);
                                                                
                                            //                 });
                                            //             }else if(data.estado && data.estado == 'fallo'){
                                            //                 $("#tituloHorarioFiltros").html(data.mensaje);
                                            //                 $("#btnSaveFiltrosEmailEntrante").prop('disabled', true);
                                            //             }
                                            //         },
                                            //         error: function(data){
                                            //             if(data.responseText){
                                            //                 alertify.error("Hubo un error al mostrar la información" + data.responseText);
                                            //             }else{
                                            //                 alertify.error("Hubo un error al mostrar la información");
                                            //             }
                                            //         }
                                            //     });

                                            }else{
                                                // Configuracion por defecto de un paso    
                                                $("#btnSaveFiltros").show();
                                                $(".secTraspasoRegistros").show();
                                                $("#btnSaveFiltrosEmailEntrante").prop('disabled', true);

                                                // Reseteo estos valores aca(son para los sms, NO BORRAR)
                                                $("#esSmsEntrante").val(0);
                                                $("#valorPregunSms").val(0);
    
                                                // Esto obtine los campos de la seccion de cuando realizar la actividad
                                                $.ajax({
                                                    url   : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?getInserciones=true',
                                                    type  : 'post',
                                                    data  : { pasoFrom : from , pasoTo : to },
                                                    dataType : 'json',
                                                    success : function(data){
                                                        if(data != '0'){

                                                            // Cargo la radiocondicion por defecto
                                                            $('#radiocondiciones3').click();

                                                            $.each(data , function(i , item){
                                                                $("#cmbTipoInsercion").val(item.ESTCON_Tipo_Insercion_b);
                                                                $("#cmbTipoInsercion").val(item.ESTCON_Tipo_Insercion_b).change();
    
                                                                $("#cmbCampoFecha").val(item.ESTCON_ConsInte_PREGUN_Fecha_b);
                                                                $("#cmbCampoFecha").val(item.ESTCON_ConsInte_PREGUN_Fecha_b).change();
    
                                                                $("#cmbCampoHora").val(item.ESTCON_ConsInte_PREGUN_Hora_b);
                                                                $("#cmbCampoHora").val(item.ESTCON_ConsInte_PREGUN_Hora_b).change();
    
    
                                                                $("#masMenosFecha").val(item.ESTCON_Operacion_Fecha_b);
                                                                $("#masMenosFecha").val(item.ESTCON_Operacion_Fecha_b).change();
    
                                                                $("#masMenosHora").val(item.ESTCON_Operacion_Hora_b);
                                                                $("#masMenosHora").val(item.ESTCON_Operacion_Hora_b).change();
    
    
                                                                $("#txtRestaSumaFecha").val(item.ESTCON_Cantidad_Fecha_b);
                                                                $("#txtRestaSumaHora").val(item.ESTCON_Cantidad_Hora_b);
    
                                                                $("#cmbCambioEstado").val(item.ESTCON_Estado_cambio_b);
                                                                $("#cmbCambioEstado").val(item.ESTCON_Estado_cambio_b).change();
    
                                                                // Campos de avanzado
                                                                if(item.ESTCON_Sacar_paso_anterior_b != null && item.ESTCON_Sacar_paso_anterior_b != 0){
                                                                    $("#sacarPasoAnterior").prop("checked", true);
                                                                }else{
                                                                    $("#sacarPasoAnterior").prop("checked", false);
                                                                }
    
                                                                if(item.ESTCON_resucitar_registro != null && item.ESTCON_resucitar_registro != 0){
                                                                    $("#resucitarRegistros").prop("checked", true);
                                                                }else{
                                                                    $("#resucitarRegistros").prop("checked", false);
                                                                }

                                                                if(item.ESTCON_Activo_b == '-1'){
                                                                    $("#estconActivo").prop("checked", true);
                                                                }else{
                                                                    $("#estconActivo").prop("checked", false);
                                                                }

                                                                // Registros a los que aplica
                                                                if(item.ESTCON_Tipo_Consulta_b == '1'){
                                                                    $('#radiocondiciones1').click();
                                                                }else if(item.ESTCON_Tipo_Consulta_b == '2'){
                                                                    $('#radiocondiciones2').click();
                                                                }else if(item.ESTCON_Tipo_Consulta_b == '3'){
                                                                    $('#radiocondiciones3').click();
                                                                }else if(item.ESTCON_Tipo_Consulta_b == '4'){
                                                                    $('#radiocondiciones4').click();
                                                                }else{
                                                                    $('#radiocondiciones3').click();
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
    
                                                /* aqui toca validar q tenga pasos en el otro lado */
                                                $.ajax({
                                                    url   : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?validarPasosCondiciones=true',
                                                    type  : 'post',
                                                    data  : { pasoFrom : from , pasoTo : to },
                                                    dataType : 'json',
                                
                                                    success : function(data){
                                                        if(data != '0'){
                                                            $.each(data , function(i , item){
    
                                                                if(item.ESTCON_Tipo_Consulta_b == '1'){
                                                                    $('#radiocondiciones1').click();
                                                                }else if(item.ESTCON_Tipo_Consulta_b == '2'){
                                                                    $('#radiocondiciones2').click();
                                                                }else if(item.ESTCON_Tipo_Consulta_b == '3'){
                                                                    $('#radiocondiciones3').click();
                                                                }else if(item.ESTCON_Tipo_Consulta_b == '4'){
                                                                    $('#radiocondiciones4').click();
                                                                }
                                                            
    
                                                                $("#newFiltro").click();
    
                                                                var newnew = newCuantosvan -1;
    
    
                                                                $("#pregun_"+newnew).attr('EstaEs',  item.valor);
                                                                $("#pregun_"+newnew).val(item.campo);
                                                                $("#pregun_"+newnew).val(item.campo).change();
    
                                                                $("#condicion_"+newnew).val(item.condicion);
                                                                $("#condicion_"+newnew).val(item.condicion).change();
    
                                                                $("#valor_"+newnew).val(item.valor);
                                                                $("#valor_"+newnew).val(item.valor).change();
    
                                                                if(item.separador != null && item.separador != ''){
                                                                    $("#condiciones_"+newnew).val(item.separador);
                                                                    $("#condiciones_"+newnew).val(item.separador).change();
                                                                }
                                                                
                                                                
                                                            });
                                                        }
                                                        $("#filtrosCampanha").modal();                                          
                                                        
                                                    },
                                                    complete : function(){
                                                        $.unblockUI();
                                                    }
                                                });

                                                if(data.tipoPasoFrom == 18){
                                                    // Busco el campo de smsEntrante configurado previamente
                                                    $.ajax({
                                                    url   : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G32/G32_CRUD.php?traerConfiguracion=true',
                                                    type  : 'post',
                                                    data  : { pasoFrom : from},
                                                    dataType : 'json',
                                
                                                    success : function(data){
                                                        // Creo una variable que me permita decir si viene desde smsEntrante

                                                        if(data.smsSaliente){

                                                            if(data.smsSaliente.esperarRespuesta && data.smsSaliente.esperarRespuesta == -1){
                                                                $("#esSmsEntrante").val(1);
                                                                $("#valorPregunSms").val(data.smsSaliente.pregun);
                                                            }
                                                        }
                                                    },
                                                    error: function(data){
                                                        console.log(data);
                                                    },
                                                    complete : function(){
                                                        $.unblockUI();
                                                    }
                                                });
                                                }
                                            }

                                        }
                                        $("#filtrosCampanha").modal();
                                    },
                                    complete : function(){
                                        $.unblockUI();
                                    }
                                });
                            }
                        },
                        complete : function(){

                        }
                    });                                     
                }else{
                    $.unblockUI();
                }
            },
            beforeSend : function(){
                $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_mesa_adyacente;?>' });
            }
        });
    }

    function campanhasHave(from){
        $.ajax({
                url  : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?tieneCampana=true',
            type : 'post',
            data : { pasoId :  from },
            success : function(data){
                if(data != '0'){
                    $("#id_campanaFiltros").val(data);
                }
            }
        });
    }

    function getDatosBackoffice(llaveInvocar){
        $.ajax({
            url  : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?verificarTareasBackoffice=true',
            type : 'POST',
            data : {key : llaveInvocar, guion: <?php echo $str_poblacion; ?>},
            dataType : 'json',
            success: function(response){

                $("#casoBackofficeForm")[0].reset();
                $('#casoBackofficeForm #tipoAccion').val(response.accion);
                $('#casoBackofficeForm #idEstpas').val(llaveInvocar);
                $('#casoBackofficeForm #idGuion').val(<?php echo $str_poblacion; ?>);

                if(response.accion == "nuevo"){
                    $('#nombreCaso').val(response.nombrepaso);
                    $('#pasoActivo').prop('checked','checked');
                    $('#casoBackofficeModal .modal-title').text('Crear tarea de BackOffice');
                    $('#casoBackofficeForm .camposDeEdicion').hide();
                    $('#casoBackofficeForm #idTareaBack').val("0");

                }else if(response.accion == 'editar'){

                    $('#casoBackofficeModal .modal-title').text('Editar tarea de BackOffice');
                    $('#casoBackofficeForm #idTareaBack').val(response.respuesta[0].TAREAS_BACKOFFICE_ConsInte__b);
                    $('#nombreCaso').val(response.respuesta[0].TAREAS_BACKOFFICE_Nombre_b);
                    $('#tipoDistribucionTrabajo').val(response.respuesta[0].TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b).change();

                    $('#casoBackofficeForm .camposDeEdicion').show();
                    $('#pregun').html(response.pregunOption);
                    $('#lisopc').html(response.lisopcOption);
                    
                    if(response.respuesta[0].ESTPAS_activo == 1){
                        $('#pasoActivo').prop('checked','checked');
                    }else{
                        $('#pasoActivo').prop('checked',false);
                    }

                    if(response.respuesta[0].TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b == 2){
                        $('#pregun').val(response.respuesta[0].TAREAS_BACKOFFICE_ConsInte__PREGUN_estado_b);
                        $('#lisopc').val(response.respuesta[0].TAREAS_BACKOFFICE_ConsInte__LISOPC_estado_b);
                    }

                    $('#disponible').html(response.listaUsu);
                    $('#seleccionado').html(response.listaUsuA);
                    
                    

                }
                $('input[type="checkbox"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-blue'
                });

                var esVisible = $("#casoBackofficeModal").is(":visible");

                if(!esVisible){
                    $("#casoBackofficeModal").modal('show');
                }
                
            },
            error: function(response){
                console.log(response);
            },
            beforeSend : function(){
                $.blockUI({baseZ : 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
            },
            complete : function(){
                $.unblockUI();
            }

        });
    }

    function setHorario24(){
        $("#G10_C109").val("00:00:01");
        $("#G10_C112").val("00:00:01");
        $("#G10_C115").val("00:00:01");
        $("#G10_C118").val("00:00:01");
        $("#G10_C121").val("00:00:01");
        $("#G10_C124").val("00:00:01");
        $("#G10_C127").val("00:00:01");
        $("#G10_C130").val("00:00:01");

        $("#G10_C110").val("23:59:59");
        $("#G10_C113").val("23:59:59");
        $("#G10_C116").val("23:59:59");
        $("#G10_C119").val("23:59:59");
        $("#G10_C122").val("23:59:59");
        $("#G10_C125").val("23:59:59");
        $("#G10_C128").val("23:59:59");
        $("#G10_C131").val("23:59:59");

        $('#sec_horario :checkbox[readonly=readonly]').prop('checked','checked');
    }

    function inicializarTimePicker(){
        //Timepickers
        $("#G10_C109").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:00:00',
            'maxTime': '23:59:59',
            'setTime': '08:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C110").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:10:00',
            'maxTime': '23:59:59',
            'setTime': '17:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C112").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:00:00',
            'maxTime': '23:59:59',
            'setTime': '08:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C113").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:10:00',
            'maxTime': '23:59:59',
            'setTime': '17:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C115").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:00:00',
            'maxTime': '23:59:59',
            'setTime': '08:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C116").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:10:00',
            'maxTime': '23:59:59',
            'setTime': '17:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C118").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:00:00',
            'maxTime': '23:59:59',
            'setTime': '08:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C119").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:10:00',
            'maxTime': '23:59:59',
            'setTime': '17:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C121").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:00:00',
            'maxTime': '23:59:59',
            'setTime': '08:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C122").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:10:00',
            'maxTime': '23:59:59',
            'setTime': '17:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C124").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:00:00',
            'maxTime': '23:59:59',
            'setTime': '08:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C125").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:10:00',
            'maxTime': '23:59:59',
            'setTime': '17:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C127").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:00:00',
            'maxTime': '23:59:59',
            'setTime': '08:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C128").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:10:00',
            'maxTime': '23:59:59',
            'setTime': '17:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C130").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:00:00',
            'maxTime': '23:59:59',
            'setTime': '08:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $("#G10_C131").timepicker({
            'timeFormat': 'H:i:s',
            'minTime': '00:10:00',
            'maxTime': '23:59:59',
            'setTime': '17:00:00',
            'step'  : '5',
            'showDuration': true
        });

        $(':checkbox[readonly=readonly]').click(function(){
            return false;         
        }); 
    }

    function soloLectura(){
        $("#G10_C109").prop("disabled", true);
        $("#G10_C112").prop("disabled", true);
        $("#G10_C115").prop("disabled", true);
        $("#G10_C118").prop("disabled", true);
        $("#G10_C121").prop("disabled", true);
        $("#G10_C124").prop("disabled", true);
        $("#G10_C127").prop("disabled", true);
        $("#G10_C130").prop("disabled", true);

        $("#G10_C110").prop("disabled", true);
        $("#G10_C113").prop("disabled", true);
        $("#G10_C116").prop("disabled", true);
        $("#G10_C119").prop("disabled", true);
        $("#G10_C122").prop("disabled", true);
        $("#G10_C125").prop("disabled", true);
        $("#G10_C128").prop("disabled", true);
        $("#G10_C131").prop("disabled", true);
    }

    function formaterFueraHorario(horaInicial, horaFinal, campoInicial, campoFinal){
        let inicial = moment(horaFinal, 'hh:mm:ss');
        let final = moment(horaInicial, 'hh:mm:ss');

        inicial.add(1, 'seconds');
        final.subtract(1, 'seconds');
        
        $("#"+campoInicial).val(inicial.format('HH:mm:ss'));
        $("#"+campoFinal).val(final.format('HH:mm:ss'));
    }
</script>


<!-- funciones para manipular la creacion de campañas -->
<script type="text/javascript">
    $(function(){
        var ch = 0;
        $('.fromBAse').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $("#G10_C73").select2({
            dropdownParent: $("#crearCampanhasNueva")
        });

        $('#generarFromDB').on('ifChecked', function () { 
            ch = 1;
            $("#G10_C73").attr('disabled', true);
            $("#G10_C73B").attr('disabled', true);
        });


        $('#generarFromDB').on('ifUnchecked', function () { 
            ch = 0;
            $("#G10_C73").attr('disabled', false);
            $("#G10_C73B").attr('disabled', false);
        });





        $("#btnSaveCampan").click(function(){
            var valido = 0;
            if( $("#G10_C72").val() < 1){
                valido = 1;
                alertify.error("<?php echo $campan_error_s_;?>");
            }
            
            if( $("#G10_C71").val() == ''){
                valido = 1;
                alertify.error("El campo nombre es obligatorio");
            }
            
            if(ch == 0){
                if($("#campanasNormales").is(":visible")){
                    if( $("#G10_C73").val() == 0){
                        valido = 1;
                        alertify.error("<?php echo $campan_error_s_;?>");
                    }
                }
                

                if($("#backoffice").is(":visible")){
                    if( $("#G10_C73B").val() == 0){
                        valido = 1;
                        alertify.error("<?php echo $campan_error_s_;?>");
                    }
                }
            }

            if(valido == 0){
                /* ahora toca meter la campaña de una */
                var formData = new FormData($("#formuarioCargarEstoEstrart")[0]);
                formData.append('oper', 'add');
                var url_campan = '';
                var url_destino = '';        
                if($('#AddTipoCampan').val() == 2){     
                    url_campan = 'G10_CRUD.php';
                    url_destino = 'campan';   
                    
                }else if($('#AddTipoCampan').val() == 1){     
                    url_campan = 'G10_CRUD_v2.php';
                    url_destino = 'entrantes';

                }else if($('#AddTipoCampan').val() == 3){     
                    url_campan = 'G10_CRUD_v3.php';
                    url_destino = 'backoffice';
                
                }

                $.ajax({
                   url: '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G10/'+url_campan+'?insertarDatosGrilla=si&usuario=<?php echo $_SESSION['IDENTIFICACION'];?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    //una vez finalizado correctamente
                    success: function(datass){
                        if(datass){
                            $.ajax({
                                url  : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?hayCampana=true',
                                type : 'post',
                                data : { pasoId :  $("#id_estpas_mio").val() },
                                success : function(data){
                                    window.location.href = '<?=base_url?>modulo/ruta/'+url_destino+'/'+data+'/<?php echo $str_poblacion; ?>/1';
                                }
                            });
                        }else{
                            $.unblockUI();
                            alertify.error('<?php echo $error_de_proceso; ?>');
                        }
                    },
                    beforeSend : function(){
                        $.blockUI({ 
                            baseZ: 2000,
                            message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait;?>' });
                    }
                });
            }
        });
    });
</script>


<!-- Funcionalidad del validador de condiciones Datos-->
<script type="text/javascript">
    var totalFiltros = 0;
    $(function(){



        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
            daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: "Today",
            clear: "Clear",
            format: "yyyy-mm-dd",
            titleFormat: "yyyy-mm-dd", 
            weekStart: 0
        };

       
        $("#newFiltro").click(function(){
            var colunna1 = 2;
            var colunna2 = 4;
            var cuerpo = "<div class='row' id='id_"+newCuantosvan+"'>";
            let idActualRegistro = newCuantosvan; // Este valor lo necesito para el filtro de sms
        
            if(totalFiltros == 0){
                cuerpo += "<div class='col-md-5'>";
                colunna1 = 2;
                colunna2 = 4;
            }else{
                cuerpo += "<div class='col-md-1'>";
                cuerpo += "<div class='form-group'>";
                cuerpo += "<select class='form-control condiciones' name='condiciones_"+newCuantosvan+"'  id='condiciones_"+newCuantosvan+"' numero='"+newCuantosvan+"'>";
                cuerpo += "<option value=' AND '><?php echo $str_Filtro_AND__________;?></option>";
                cuerpo += "<option value=' OR '><?php echo $str_Filtro_OR___________;?></option>";
                cuerpo += "</select>";
                cuerpo += "</div>";
                cuerpo += "</div>";
                cuerpo += "<div class='col-md-4'>";
                colunna1 = 2;
                colunna2 = 4;
            }
            

            cuerpo += "<div class='form-group'>";
            cuerpo += "<select class='form-control miSelectPregun' name='pregun_"+newCuantosvan+"'  id='pregun_"+newCuantosvan+"' numero='"+newCuantosvan+"'>";
            cuerpo += "<option value='0'><?php echo $filtros_campos_c; ?></option>";
            $.each(datosGuion, function(i, item) {
                 cuerpo += "<option value='"+item.PREGUN_ConsInte__b+"' tipo='"+item.PREGUN_Tipo______b+"' lista='"+item.PREGUN_ConsInte__OPCION_B+"' >"+ item.PREGUN_Texto_____b+"</option>";
            }); 
            //console.log(Aplica);
            if(Aplica == '1' || Aplica == '6'){
                cuerpo += "<option value='_Estado____b' tipo='_Estado____b'><?php echo $muestra_traduc_1; ?></option>";
                cuerpo += "<option value='_ConIntUsu_b' tipo='_ConIntUsu_b'><?php echo $muestra_traduc_2; ?></option>";
                cuerpo += "<option value='_NumeInte__b' tipo='_NumeInte__b'><?php echo $muestra_traduc_3; ?></option>";
                cuerpo += "<option value='_UltiGest__b' tipo='MONOEF'><?php echo $muestra_traduc_4; ?></option>";
                cuerpo += "<option value='_GesMasImp_b' tipo='MONOEF'><?php echo $muestra_traduc_5; ?></option>";
                cuerpo += "<option value='_FecUltGes_b' tipo='_FecUltGes_b'><?php echo $muestra_traduc_6; ?></option>";
                cuerpo += "<option value='_FeGeMaIm__b' tipo='_FeGeMaIm__b'><?php echo $muestra_traduc_7; ?></option>";
                cuerpo += "<option value='_ConUltGes_b' tipo='ListaCla'><?php echo $muestra_traduc_8; ?></option>";
                cuerpo += "<option value='_CoGesMaIm_b' tipo='ListaCla'><?php echo $muestra_traduc_9; ?></option>";
                cuerpo += "<option value='_Activo____b' tipo='_Activo____b'><?php echo $muestra_traduc_10; ?></option>";
            }
            

            cuerpo += "</select>";
            cuerpo += "</div>";
            cuerpo += "</div>";
            cuerpo += "<div class='col-md-"+colunna1+"'>";
            cuerpo += "<div class='form-group'>";;
            cuerpo += "<select name='condicion_"+newCuantosvan+"' id='condicion_"+newCuantosvan+"' class='form-control'>";
            cuerpo += "<option value='0'><?php echo $filtros_condic_c; ?></option>";
            cuerpo += "</select>";
            cuerpo += "</div>";
            cuerpo += "</div>";
            cuerpo += "<div class='col-md-"+colunna2+"' id='valoresRestableses_"+ newCuantosvan +"' >";
            cuerpo += "<div class='form-group'>";
            cuerpo += "<input type='text'  name='valor_"+newCuantosvan+"' id='valor_"+newCuantosvan+"' class='form-control' placeholder='<?php echo $filtros_valor__c; ?>'>";
            cuerpo += "</div>";
            cuerpo += "</div>";
            cuerpo += "<div class='col-md-1'>";
            cuerpo += "<div class='form-group'>";
            cuerpo += "<button class='btn btn-danger btn-sm deleteFiltro' id='"+newCuantosvan+"' numeroFila='"+newCuantosvan+"' title='<?php echo $str_opcion_elimina;?>' type='button'><i class='fa fa-trash-o'></i></button>";
            cuerpo += "</div>";
            cuerpo += "</div>";
            cuerpo += "</div>";


            newCuantosvan++;
            totalFiltros++;
            contador2++;

            $("#filtros").append(cuerpo);

            $(".miSelectPregun").change(function(){
                var id = $(this).attr('numero');
                var tipo = $("#pregun_"+id+" option:selected").attr('tipo');
                var estaEs = null;
                estaEs = $(this).attr('EstaEs');
                
                var options = "";
                cuerpo = "<div class='form-group'>";
                cuerpo += "<input type='text'  name='valor_"+id+"' id='valor_"+id+"' class='form-control' placeholder='<?php echo $filtros_valor__c; ?>'>";
                cuerpo += "</div>";
                $("#valoresRestableses_"+id).html(cuerpo);

                if(tipo == '1' || tipo == '2'){
                    options += "<option value='='><?php echo $filtros_title_1c;?></option>";
                    options += "<option value='!='><?php echo $filtros_title_2c;?></option>";
                    options += "<option value='LIKE_1'><?php echo $filtros_title_3c;?></option>";
                    options += "<option value='LIKE_2'><?php echo $filtros_title_4c;?></option>";
                    options += "<option value='LIKE_3'><?php echo $filtros_title_5c;?></option>";
                }

                if(tipo == '4' || tipo == '3' || tipo == '_NumeInte__b'){
                    options += "<option value='='><?php echo $filtros_numbe_1c;?></option>";
                    options += "<option value='!='><?php echo $filtros_numbe_2c;?></option>";
                    options += "<option value='>'><?php echo $filtros_numbe_3c;?></option>";
                    options += "<option value='<'><?php echo $filtros_numbe_4c;?></option>";

                    $("#valor_"+id).numeric();
                }


                if(tipo == '_NumeInte__b'){

                    options += "<option value='='><?php echo $filtros_numbe_1c;?></option>";
                    options += "<option value='!='><?php echo $filtros_numbe_2c;?></option>";
                    options += "<option value='>'><?php echo $filtros_numbe_3c;?></option>";
                    options += "<option value='<'><?php echo $filtros_numbe_4c;?></option>";

                    $("#valor_"+id).numeric();

                    cuerpo = "<input type='hidden' name='esMuestra_"+id+"' value='SI' >";
                    $("#valoresRestableses_"+id).append(cuerpo);

                }


                if(tipo == '_FecUltGes_b' || tipo == '_FeGeMaIm__b' ){
                    options += "<option value='='><?php echo $filtros_numbe_1c;?></option>";
                    options += "<option value='!='><?php echo $filtros_numbe_2c;?></option>";
                    options += "<option value='>'><?php echo $filtros_numbe_3c;?></option>";
                    options += "<option value='<'><?php echo $filtros_numbe_4c;?></option>";
                    
                    $("#valor_"+id).datepicker({
                        language: "es",
                        autoclose: true,
                        todayHighlight: true
                    });
                    cuerpo = "<input type='hidden' name='esMuestra_"+id+"' value='SI' >";
                    $("#valoresRestableses_"+id).append(cuerpo);
                }

                if(tipo == '5'){
                    options += "<option value='='><?php echo $filtros_numbe_1c;?></option>";
                    options += "<option value='!='><?php echo $filtros_numbe_2c;?></option>";
                    options += "<option value='>'><?php echo $filtros_numbe_3c;?></option>";
                    options += "<option value='<'><?php echo $filtros_numbe_4c;?></option>";

                    $("#valor_"+id).datepicker({
                        language: "es",
                        autoclose: true,
                        todayHighlight: true
                    });
                }


                if( tipo == '_Estado____b' || tipo == '_ConIntUsu_b' || tipo == 'MONOEF' || tipo == '8' || tipo == '6' || tipo == '11'){
                    options += "<option value='='><?php echo $filtros_numbe_1c;?></option>";
                    options += "<option value='!='><?php echo $filtros_numbe_2c;?></option>";
                } 


                if(tipo == 'ListaCla'){

                    options += "<option value='='><?php echo $filtros_numbe_1c;?></option>";
                    options += "<option value='!='><?php echo $filtros_numbe_2c;?></option>";

                    cuerpo  = "<div class='form-group'>";
                    cuerpo += "<select name='valor_"+id+"'  id='valor_"+id+"' class='form-control'>";
                    cuerpo += "<option value='1'><?php echo $str_opcion_number1; ?></option>";
                    cuerpo += "<option value='2'><?php echo $str_opcion_number2; ?></option>";
                    cuerpo += "<option value='3'><?php echo $str_opcion_number3; ?></option>";
                    cuerpo += "<option value='4'><?php echo $str_opcion_number4; ?></option>";
                    cuerpo += "<option value='5'><?php echo $str_opcion_number5; ?></option>";
                    cuerpo += "<option value='6'><?php echo $str_opcion_number6; ?></option>";
                    cuerpo += "<option value='7'><?php echo $str_opcion_number7; ?></option>";
                    cuerpo += "</select>";
                    cuerpo += "<input type='hidden' name='esMuestra_"+id+"' value='SI' >";
                    cuerpo += "</div>";  

                    $("#valoresRestableses_"+id).html(cuerpo);

                    $("#valor_"+id).val(estaEs);
                    $("#valor_"+id).val(estaEs).change();
                }


                if(tipo == '8'){
                    cuerpo  = "<div class='form-group'>";
                    cuerpo += "<select name='valor_"+id+"'  id='valor_"+id+"' class='form-control'>";
                    cuerpo += "<option value='0'><?php echo $muesra_activo_2; ?></option>";
                    cuerpo += "<option value='-1'><?php echo $muesra_activo_1; ?></option>";
                    cuerpo += "</select>";
                    cuerpo += "</div>";  

                    $("#valoresRestableses_"+id).html(cuerpo);

                }


                if(tipo == '_Activo____b'){

                    options += "<option value='='><?php echo $filtros_numbe_1c;?></option>";
                    options += "<option value='!='><?php echo $filtros_numbe_2c;?></option>";

                    cuerpo  = "<div class='form-group'>";
                    cuerpo += "<select name='valor_"+id+"'  id='valor_"+id+"' class='form-control'>";
                    cuerpo += "<option value='0'><?php echo $muesra_activo_2; ?></option>";
                    cuerpo += "<option value='-1'><?php echo $muesra_activo_1; ?></option>";
                    cuerpo += "</select>";
                    cuerpo += "<input type='hidden' name='esMuestra_"+id+"' value='SI' >";
                    cuerpo += "</div>";  

                    $("#valoresRestableses_"+id).html(cuerpo);

                }

                if(tipo == '_Estado____b'){
                    cuerpo  = "<div class='form-group'>";
                    cuerpo += "<select name='valor_"+id+"'  id='valor_"+id+"' class='form-control'>";
                    cuerpo += "<option value='0'><?php echo $muestra_estado_1; ?></option>";
                    cuerpo += "<option value='1'><?php echo $muestra_estado_2; ?></option>";
                    cuerpo += "<option value='2'><?php echo $muestra_estado_3; ?></option>";
                    cuerpo += "<option value='3'><?php echo $muestra_estado_4; ?></option>";
                    cuerpo += "</select>";
                    cuerpo += "<input type='hidden' name='esMuestra_"+id+"' value='SI' >";
                    cuerpo += "</div>";  

                    $("#valoresRestableses_"+id).html(cuerpo);

                }

                if(tipo == '6'){

                    $.ajax({
                        url    : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?Lisopc=true',
                        type   : 'post',
                        data   : { lisopc : $("#pregun_"+id+" option:selected").attr('lista') },
                        async : true,
                        success : function(data){
                            cuerpo  = "<div class='form-group'>";
                            cuerpo += "<select name='valor_"+id+"' class='form-control'  id='valor_"+id+"'>";
                            cuerpo += data;
                            cuerpo += "</select>";
                            cuerpo += "</div>";  
                            $("#valoresRestableses_"+id).html(cuerpo);
                            if(estaEs != null){
                                $("#valor_"+id).val(estaEs);
                                $("#valor_"+id).val(estaEs).change();
                            }
                            
                        },
                        beforeSend : function(){
                            $.blockUI({ baseZ : 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                        },
                        complete : function(){
                            $.unblockUI();
                        }
                    });
                }

                if(tipo == '11'){

                    $.ajax({
                        url    : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?Litacompuesta=true',
                        type   : 'post',
                        data   : { idPregun : $("#pregun_"+id+" option:selected").val() },
                        async : true,
                        success : function(data){
                            cuerpo  = "<div class='form-group'>";
                            cuerpo += "<select name='valor_"+id+"' class='form-control'  id='valor_"+id+"'>";
                            cuerpo += data;
                            cuerpo += "</select>";
                            cuerpo += "</div>";  
                            $("#valoresRestableses_"+id).html(cuerpo);
                            if(estaEs != null){
                                $("#valor_"+id).val(estaEs);
                                $("#valor_"+id).val(estaEs).change();
                            }
                            
                        },
                        beforeSend : function(){
                            $.blockUI({ baseZ : 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                        },
                        complete : function(){
                            $.unblockUI();
                        }
                    });
                }

                if(tipo == '_ConIntUsu_b'){
                    $.ajax({
                        url    : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G10/G10_extender_funcionalidad.php?getUsuariosCampanha=true',
                        type   : 'post',
                        data   : { campan : $("#id_campanaFiltros").val() },
                        success : function(data){
                            cuerpo  = "<div class='form-group'>";
                            cuerpo += "<select name='valor_"+id+"' class='form-control'  id='valor_"+id+"'>";
                            $.each(JSON.parse(data), function(i, item) {
                                 cuerpo += "<option value='"+item.USUARI_ConsInte__b+"'>"+ item.USUARI_Nombre____b+"</option>";
                            }); 
                            cuerpo += "</select>";
                            cuerpo += "<input type='hidden' name='esMuestra_"+id+"' value='SI' >";
                            cuerpo += "</div>";  

                            $("#valoresRestableses_"+id).html(cuerpo);
                        },
                        beforeSend : function(){
                            $.blockUI({ baseZ : 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                        },
                        complete : function(){
                            $.unblockUI();
                        }
                    });
                }

                if(tipo == 'MONOEF'){
                    $.ajax({
                        url    : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G10/G10_extender_funcionalidad.php?getMonoefCampanha=true',
                        type   : 'post',
                        data   : { campan : $("#id_campanaFiltros").val() },
                        success : function(data){
                            cuerpo  = "<div class='form-group'>";
                            cuerpo += "<select name='valor_"+id+"' class='form-control'  id='valor_"+id+"'>";
                            $.each(JSON.parse(data), function(i, item) {
                                 cuerpo += "<option value='"+item.MONOEF_ConsInte__b+"'>"+ item.MONOEF_Texto_____b+"</option>";
                            }); 

                            //JDBD - Tipificaciones PDS
                            cuerpo += "<option value='-8'>Marcando (PDS)</option>";
                            cuerpo += "<option value='0'>Lllamada contestada (PDS)</option>";
                            cuerpo += "<option value='-3'>No contesta (PDS)</option>";
                            cuerpo += "<option value='-4'>Ocupada (PDS)</option>";
                            cuerpo += "<option value='-5'>Fallida (PDS)</option>";
                            cuerpo += "<option value='-7'>Maquina detectada (PDS)</option>";
                            cuerpo += "<option value='-6'>No se encontro ruta de salida (PDS)</option>";
                            
                            cuerpo += "</select>";
                            cuerpo += "<input type='hidden' name='esMuestra_"+id+"' value='SI' >";
                            cuerpo += "</div>";  

                            $("#valoresRestableses_"+id).html(cuerpo);

                            if(estaEs != null){
                                console.log(estaEs);
                                $("#valor_"+id).val(estaEs);
                                $("#valor_"+id).val(estaEs).change();
                            }
                        },
                        beforeSend : function(){
                            $.blockUI({ baseZ : 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                        },
                        complete : function(){
                            $.unblockUI();
                        }
                    });
                }


                $("#condicion_"+id).html(options);
            });

            $(".deleteFiltro").click(function(){
                var id = $(this).attr('id');
                console.log("si llega "+ id);
                $("#id_"+id).remove();
                totalFiltros = totalFiltros -1;
            });
            
            // Si esto se cumple todas las condiciones se van a validar por un solo campo
            if($("#esSmsEntrante").val() == 1){
                let pregunSms = $("#valorPregunSms").val();
                $("#pregun_"+idActualRegistro).val(pregunSms);
                $("#pregun_"+idActualRegistro).change();
                $("#pregun_"+idActualRegistro+" option").attr("disabled", true);
                $("#pregun_"+idActualRegistro+" option[value='"+pregunSms+"']").attr("disabled", false);
            }
        });

        

        $(".Radiocondiciones").on('change', function () { 
            if($(this).val() == 1){
                /* Es todos */  
                $("#txtCantidadRegistrps").val('');
                $("#divCantidadCampan").hide();
                $("#divFiltrosCampan").hide();
                $("#filtros").html("");
            }else if($(this).val() == 2){
                /* MontoEspecifico */
                $("#txtCantidadRegistrps").val('');
                $("#divCantidadCampan").show();
                $("#divFiltrosCampan").hide();
                $("#filtros").html("");
                
            }else if($(this).val() == 3){
                 /* Condiciones */
                $("#txtCantidadRegistrps").val('');
                $("#divCantidadCampan").hide();
                $("#divFiltrosCampan").show();
                $("#filtros").html("");

            }else if($(this).val() == 4){
                /* Registros especificos con condiciones */
                $("#txtCantidadRegistrps").val('');
                $("#divCantidadCampan").show();
                $("#divFiltrosCampan").show();
                $("#filtros").html("");
            }
        });

        $("#btnSaveFiltros").click(function(){
            valor = 1;
            $(".Radiocondiciones").each(function(){
                if($(this).is(":checked")){
                    valor = $(this).val();
            console.log("Preparando filtros " + $(this).val());
                }
            });
            valido = 1;
            if(valor == '2'){

                if($("#txtCantidadRegistrps").val() < 1){
                    alertify.error("<?php echo $error_top_regist;?>");
                    valido = 0;
                    $("#txtCantidadRegistrps").focus();
                }
            }

            if( $('#radiocondiciones3').is(':checked') ) {
                
                var cantCondiciones = $("#filtros > div").size();

                if(cantCondiciones == 0){
                    valido = 0;
                    alertify.error("Debes agregar condiciones para guardar esta configuracion");
                }
            }

            if(valido == 1){
                save();
                // alert("Hola");
                $.ajax({
                   url: '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_CRUD.php',  
                    type: 'POST',
                    data: { mySavedModel : $("#mySavedModel").val() , id_estrategia : '<?php echo $_GET['estrategia'];?>' , guardar_flugrama : 'SI' , poblacion : '<?php echo $str_poblacion; ?>'},
                    //una vez finalizado correctamente
                    success: function(data){
                        if(data == '1'){
                           var dtao = new FormData($("#consultaCamposWhere")[0]);
                            dtao.append('contador' , contador2);
                            $.ajax({
                                url  : "<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?gestionar_base_datos=true",
                                type : "post",
                                data : dtao,
                                cache: false,
                                contentType: false,
                                processData: false,
                                success : function(data){
                                    if(data == '1'){
                                        alertify.success("<?php echo $exito_top_regist; ?>");
                                        $("#txtCantidadRegistrps").val('');
                                        $("#divCantidadCampan").hide();
                                        $("#divFiltrosCampan").hide();
                                        $("#filtros").html("");
                                        $("#consultaCamposWhere")[0].reset();
                                        $("#filtrosCampanha").modal('hide');
                                        totalFiltros = 0;
                                        location.reload();
                                    }else{
                                        alertify.error(data);
                                    }
                                },
                                complete : function(){
                                    $.unblockUI();
                                }
                            }); 
                                
                        }else{
                            //Algo paso, hay un error
                            alertify.error('<?php echo $error_de_proceso; ?>');
                        }                
                    },
                    //si ha ocurrido un error
                    error: function(){
                        after_save_error();
                        alertify.error('<?php echo $error_de_red; ?>');
                    },
                    beforeSend : function(){
                        $.blockUI({ baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait;?>' });
                    }
                });
            }
        });
    });

    function cambiarTipoValor(tipollamada){
        $("#consultaCamposWhere")[0].reset();
        if(tipollamada != '1'){
            $(".condicionesTop").hide();
        }else{
            $(".condicionesTop").show();
        }
        $("#tipoLlamado").val(tipollamada);
        $("#txtCantidadRegistrps").val('');
        $("#divCantidadCampan").hide();
        $("#divFiltrosCampan").hide();
        $("#filtros").html("");
    }
</script>

<!-- Esto permite guardar los datos del formulario que tenemos para los casos de backoffice -->
<script>
    $(function(){

        // En esta funcion se encontrara el buscador que filtrara por el nombre 
        $('#buscadorDisponible, #buscadorSeleccionado').keyup(function(){
            var tipoBuscador = $(this).attr('id');
            var nombres = '';

            if(tipoBuscador == 'buscadorDisponible'){
                nombres = $('ul#disponible .nombre');
            }else if(tipoBuscador == 'buscadorSeleccionado'){
                nombres = $('ul#seleccionado .nombre');
            }

            var buscando = $(this).val();
            var item='';

            for (let i = 0; i < nombres.length; i++) {
                item = $(nombres[i]).html().toLowerCase();
                
                for (let x = 0; x < item.length; x++) {
                    if(buscando.length == 0 || item.indexOf(buscando) > -1 ){
                        $(nombres[i]).closest('li').show();
                    }else{
                        $(nombres[i]).closest('li').hide();
                        
                    }
                }
                
            }
        });

        /** Estas funciones se encargan del funcionamiento del drag & drop */
        $("#disponible").sortable({connectWith:"#seleccionado"});
        $("#seleccionado").sortable({connectWith:"#disponible"});

        // Capturo el li cuando es puesto en la lista de usuarios disponible            
        $( "#disponible" ).on( "sortreceive", function( event, ui ) {
            let arrDisponible = [];
            let arrDisponible2 = [];
            arrDisponible[0] = ui.item.data("id");
            arrDisponible2[0] = ui.item.data("camp3");

            moverUsuarios(arrDisponible, arrDisponible2, "izquierda");
        } );

        // Capturo el li cuando es puesto en la lista de usuarios seleccionados         
        $( "#seleccionado" ).on( "sortreceive", function( event, ui ) {
            let arrSeleccionado = [];
            let arrSeleccionado2 = [];
            arrSeleccionado[0] = ui.item.data("id");
            arrSeleccionado2[0] = ui.item.data("camp3");

            moverUsuarios(arrSeleccionado, arrSeleccionado2, "derecha");
        } );

        // Solo se selecciona el check cuando se clickea el li
        $("#disponible, #seleccionado").on('click', 'li', function(){
            $(this).find(".mi-check").iCheck('toggle');

            if($(this).find(".mi-check").is(':checked') ){
                $(this).addClass('seleccionado');
            }else{
                $(this).removeClass('seleccionado');
            }
            
        });

        $("#disponible, #seleccionado").on('ifToggled', 'input', function(event){
            if($(this).is(':checked') ){
                $(this).closest('li').addClass('seleccionado');
            }else{
                $(this).closest('li').removeClass('seleccionado');
            }
        });

        // Envia los elementos seleccionados a la lista de la derecha
        $('#derecha').click(function(){
            var obj = $("#disponible .seleccionado");
            $('#seleccionado').append(obj);

            let arrSeleccionado = [];
            let arrSeleccionado2 = [];
            obj.each(function (key, value){
                arrSeleccionado[key] = $(value).data("id");
                arrSeleccionado2[key] = $(value).data("camp3");
            });

            obj.removeClass('seleccionado');
            obj.find(".mi-check").iCheck('toggle');

            if(arrSeleccionado.length > 0 && arrSeleccionado2.length > 0){
                moverUsuarios(arrSeleccionado, arrSeleccionado2, "derecha");
            }
            
        });

        // Envia los elementos seleccionados a la lista de la izquerda
        $('#izquierda').click(function(){
            var obj = $("#seleccionado .seleccionado");
            $('#disponible').append(obj);

            let arrDisponible = [];
            let arrDisponible2 = [];
            obj.each(function (key, value){
                arrDisponible[key] = $(value).data("id");
                arrDisponible2[key] = $(value).data("camp3");
            });

            obj.removeClass('seleccionado');
            obj.find(".mi-check").iCheck('toggle');

            if(arrDisponible.length > 0 && arrDisponible2.length > 0){
                moverUsuarios(arrDisponible, arrDisponible2, "izquierda");
            }
        });

        // Envia todos los elementos a la derecha
        $('#todoDerecha').click(function(){
            var obj = $("#disponible li");
            $('#seleccionado').append(obj);

            let arrSeleccionado = [];
            let arrSeleccionado2 = [];
            obj.each(function (key, value){
                arrSeleccionado[key] = $(value).data("id");
                arrSeleccionado2[key] = $(value).data("camp3");
            });
            
            moverUsuarios(arrSeleccionado, arrSeleccionado2, "derecha");
        });

        // Envia todos los elementos a la izquerda
        $('#todoIzquierda').click(function(){
            var obj = $("#seleccionado li");
            $('#disponible').append(obj);

            let arrDisponible = [];
            let arrDisponible2 = [];
            obj.each(function (key, value){
                arrDisponible[key] = $(value).data("id");
                arrDisponible2[key] = $(value).data("camp3");
            });
            
            moverUsuarios(arrDisponible, arrDisponible2, "izquierda");
        });

        // ejecuto el ajax para insertar los usuarios a la lista de la izquierda o derecha
        function moverUsuarios(arrUsuarios, arrUsuarios2, accion){
            
            var tareaBack = $('#casoBackofficeForm #idTareaBack').val();

            if(accion == 'derecha'){
                ruta = "agregarUsuarioTareaBackoffice=true";
            }else if(accion = 'izquerda'){
                ruta = "quitarUsuarioTareaBackoffice=true";
            }

            $.ajax({
                    url: '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?'+ruta,  
                    type: 'POST',
                    dataType : 'json',
                    data: {arrUsuarios : arrUsuarios, arrUsuarios2 : arrUsuarios2, tareaBack : tareaBack},
                    success: function(response){
                        alertify.success("Mensaje: "+response.estado);
                    },
                    error: function(response){
                        console.log(response);
                    },
                    beforeSend : function(){
                        $.blockUI({baseZ : 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait;?>' });
                    },
                    complete : function(){
                        $.unblockUI();
                    }
                });
        }

        /**---- Esta seccion es del formulario de backoffice ----*/
        $('#casoBackofficeForm #tipoDistribucionTrabajo').change(function(){

            if($('#casoBackofficeForm #tipoDistribucionTrabajo').val() != 2){
                $('#casoBackofficeForm #pregun').attr('disabled', 'disabled');
                $('#casoBackofficeForm #lisopc').attr('disabled', 'disabled');
                $('#casoBackofficeForm #pregun').val('');
                $('#casoBackofficeForm #lisopc').val('');
                
            }else{
                $('#casoBackofficeForm #pregun').removeAttr('disabled');
                $('#casoBackofficeForm #lisopc').removeAttr('disabled');
                
            }

        });

        // Se ejecuta cuando hay un cambio en el select de prengunt
        $('#casoBackofficeForm #pregun').change(function(){

            var id = $('#casoBackofficeForm #pregun').val();

            $.ajax({
                url: '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?getLisopc=true',  
                type: 'POST',
                data: {id : id},
                dataType : 'json',
                success: function(response){
                    $('#lisopc').html(response.lisopcOption);
                },
                error: function(response){
                    console.log(response);
                },
                beforeSend : function(){
                    $.blockUI({baseZ : 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait;?>' });
                },
                complete : function(){
                    $.unblockUI();
                }
            });
        });

        // Este boton guarda el formulario de tareas de backoffice
        $('#guardarCasoBackofficeButton').click(function(){
            
            var valido = 0;

            if($("#casoBackofficeForm #nombreCaso").val() < 1){
                valido = 1;
                alertify.error("El campo nombre no puede estar vacío");
                $("#casoBackofficeForm #nombreCaso").focus();
            }

            if(valido == 0){
                var formData = new FormData($("#casoBackofficeForm")[0]);
                var estadoModal = $("#casoBackofficeForm #tipoAccion").val();
                var mikey = $('#casoBackofficeForm #idEstpas').val();

                // Dependiendo de tipoAccion va a crear un registro nuevo o actualizarlo
                if(estadoModal == 'nuevo'){
                    formData.append('nuevoCasoBackoffice', true);
                }else if(estadoModal == 'editar'){
                    formData.append('editarCasoBackoffice', true);
                }
                
                $.ajax({
                   url: '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?guardarCasoBackoffice=true',  
                    type: 'POST',
                    data: formData,
                    dataType : 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        if(response.con1 && response.con2){
                            alertify.success("Guardado exitoso");
                        }else{
                            alertify.error("Error al guardar");
                        }

                        if(estadoModal == 'nuevo'){
                            getDatosBackoffice(mikey);
                            
                        }else{
                            $("#casoBackofficeModal").modal('hide');
                            $.unblockUI();
                        }
                    },
                    error: function(response){
                        console.log(response);
                        $.unblockUI();
                    },
                    beforeSend : function(){
                        $.blockUI({baseZ : 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait;?>' });
                    },
                    complete : function(){
                        //$.unblockUI();
                    }
                });
            }

        });
    });
</script>

<!-- Funciones para LEAD -->
<script>
    /** Variables para lead */
    var opcionesCampoPregun = [];
    var contCamposEmparejar = 0;

    function cambioTipoCondicionLead(){
        var tipo_condicion = $('#leadTipoCondicion').val();
        if(tipo_condicion != 100){
            $('#leadCondicion').attr('disabled', false);
        }else{
            $('#leadCondicion').attr('disabled', true);
        }
    }

    function guardarLead(){
        var valido = 0;

        if($("#nombreLead").val() < 1){
            valido = 1;
            alertify.error("El campo nombre no puede estar vacío");
            $("#nombreLead").focus();
        }

        if($("#leadQuienRecibe").val() == 0){
            valido = 1;
            alertify.error("El campo debe ser seleccionado");
            $("#leadQuienRecibe").focus();
        }

        if(valido == 0){
            var formData = new FormData($("#form-lead")[0]);
            formData.append('contCamposEmparejar', contCamposEmparejar);
            $.ajax({
                url: '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?guardarLead=true',  
                type: 'POST',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(response){
                    if(response.estado = 1){
                        alertify.success("Guardado exitoso");
                        $("#modalLead").modal('hide');
                    }else{
                        alertify.error("Error al guardar");
                    }
                },
                error: function(response){
                    console.log(response);
                    //$.unblockUI();
                },
                beforeSend : function(){
                    $.blockUI({baseZ : 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait;?>' });
                },
                complete : function(){
                    $.unblockUI();
                }
            });
        }
    }

    $(function(){

        /**Traer los campos de PREGUN */
        $.ajax({
            url    : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php',
            type   : 'POST',
            data   : { getCamposPregun : true, idGuion: <?php echo $str_poblacion; ?> },
            dataType: 'json',
            success : function(data){
                opcionesCampoPregun = data.opciones;
            }
        });

        $('#nuevoCampoEmparejar').click(function(){
            var nuevoCampo = '';

            nuevoCampo += '<tr>';
            nuevoCampo += '<td>';
            nuevoCampo += '<input type="text" required class="form-control input-sm" name="tagInicialNuevo_'+contCamposEmparejar+'" id="tagInicialNuevo_'+contCamposEmparejar+'" placeholder="<ejemplo>">';
            nuevoCampo += '</td>';
            nuevoCampo += '<td>';
            nuevoCampo += '<input type="text" class="form-control input-sm" name="tagFinalNuevo_'+contCamposEmparejar+'" id="tagFinalNuevo_'+contCamposEmparejar+'" placeholder="</ejemplo>">';
            nuevoCampo += '</td>';
            nuevoCampo += '<td>';
            nuevoCampo += '<select name="campoBDNuevo_'+contCamposEmparejar+'" id="campoBDNuevo_'+contCamposEmparejar+'" class="form-control input-sm">';
            nuevoCampo += '<option value="0">Seleccione</option>';
            nuevoCampo += opcionesCampoPregun;         
            nuevoCampo += '</select>';
            nuevoCampo += '</td>';
            nuevoCampo += '<td>';
            nuevoCampo += '<button type="button" class="btn btn-danger btn-sm eliminarCampoEmparejar"><i class="fa fa-trash"></i></button>';
            nuevoCampo += '</td>';
            nuevoCampo += '</tr>';

            $("#LeadCampos tbody").append(nuevoCampo);

            contCamposEmparejar += 1;

            $(".eliminarCampoEmparejar").click(function(){
                $(this).closest('tr').remove();
            });
        
        });
    });
</script>

<!-- Funciones para webservice -->
<script>
    $("#wsNombre").on('keyup paste', function(){
        generarEjemplo();
    });

    function cambiarLlave(){
        alert();
    }

    function generarEjemplo(){
        
        let paso = $('#pasowsId').val();
        let llave = $("#cllave").val();
        let valores = $("#valoresws").val(); 
        let wsNombre = $("#wsNombre").val();

        let restriccionRegistro = false;

        // Valida el check de restriccion de registros
        if( $('#validaConRestriccion').prop('checked') ) {
            restriccionRegistro = true;
        }
        
        var campos = 'curl --location --request POST \'http://addons.<?php echo $URL_SERVER ?>:8080/dy_servicios_adicionales/svrs/crm/procesarLead\' \\\n';
        campos += '--header \'Accept: application/json\' \\\n';
        campos += '--header \'Content-Type: application/json\' \\\n';
        campos += '--data \'{\n';
        campos += '\t "strUsuario_t": "USUARIO",\n';
        campos += '\t "strToken_t": "TOKEN",\n';
        campos += '\t "intCodigoAccion_t": "4",\n';
        campos += '\t "booValidaConRestriccion_t": "'+restriccionRegistro+'",\n';
        campos += '\t "intIdEstpas_t": "'+paso+'",\n';
        campos += '\t "strCamposLlave_t": "'+llave+'",\n';
        campos += '\t "strOrigenLead_t": "WS_'+wsNombre+'",\n',
        campos += '\t "mapValoresCampos_t": {\n';
        campos += valores;
        campos += '\t }\n';
        campos += '}\'';
        
        $("#infows").html(campos);
    }

    function saveWebservice(){
        var valido = 0;

        if($("#wsNombre").val() < 1){
            valido = 1;
            alertify.error("El campo nombre no puede estar vacío");
            $("#wsNombre").focus();
        }

        if(valido == 0){
            var formData = new FormData($("#webServiceForm")[0]);
            $.ajax({
                url: '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G2/G2_extender_funcionalidad.php?guardarwebservice=true',  
                type: 'POST',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function(response){
                    if(response.estado == 1){
                        alertify.success("Guardado exitoso");
                        $("#webserviceModal").modal('hide');
                    }else{
                        alertify.error("Error al guardar");
                    }
                },
                error: function(response){
                    console.log(response);
                    //$.unblockUI();
                },
                beforeSend : function(){
                    $.blockUI({baseZ : 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait;?>' });
                },
                complete : function(){
                    $.unblockUI();
                }
            });
        }
    }
</script>

<!-- Funciones crud flecha cuando es filtros de correo entrante -->
<script>

    var cantFiltrosEmail = 0;


    $("#nuevaCondicionCorreo").click(function(){
        agregarFila('add');
    });

    // Guarda los filtros de email entrante
    $("#btnSaveFiltrosEmailEntrante").click(function(){

        let valido = true;

        $(".validar-condicion").removeClass("error-input");

        // Primero valido dependiendo tipo de filtro valido si es requerido
        $(".validar-condicion").each(function(){

            let id = $(this).data('id');
            
            if($("#mailTipoCondicion_"+id).val() != 100){

                if($(this).val().length == 0){
                    $(this).addClass("error-input");
                    $(this).focus();

                    alertify.error("Este campo es requerido");
                    valido = false;
                }
            }
        });

        if(valido){
            let formData = new FormData($("#consultaCamposWhere")[0]);
            formData.append("cantFiltrosEmail", cantFiltrosEmail);

            $.ajax({
                url: '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G31/G31_CRUD.php?guardarFiltros=true',  
                type: 'POST',
                data: formData,
                dataType : 'json',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend : function(){
                    $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                success: function(data){
                    console.log(data);
                    $("#consultaCamposWhere")[0].reset();
                    $("#filtrosCampanha").modal('hide');
                    alertify.success('Datos guardados');
                },
                complete : function(){
                    $.unblockUI();
                },
                error: function(data){
                    if(data.responseText){
                        alertify.error("Hubo un error al guardar la información" + data.responseText);
                    }else{
                        alertify.error("Hubo un error al guardar la información");
                    }
                }
            });
        }
    });

    function agregarFila(oper) {

        let cant = cantFiltrosEmail;

        let row = `
            <tr id="fila_${cant}">
                <input type="hidden" id="campo_${cant}" name="campo_${oper}_${cant}" value="">
                <td style="display:none;">
                    <select class="form-control input-sm" id="operador_${cant}" name="operador_${oper}_${cant}">            
                        <option value="AND">&</option>
                        <option value="OR">O</option>
                    </select>
                </td>

                <td>
                    <select class="form-control input-sm" id="mailTipoCondicion_${cant}" name="mailTipoCondicion_${oper}_${cant}" onchange="cambioTipoCondicion(${cant})">
                        <option value="100">Sin Condición</option>
                        <option value="1">Proviene del correo</option>
                        <option value="2">Proviene del dominio</option>
                        <option value="3">El asunto contiene</option>
                        <option value="4">El cuerpo contiene</option>
                        <option value="5">El asunto no contiene</option>
                        <option value="6">El cuerpo no contiene</option>
                    </select>
                </td>

                <td>
                    <input type="text" data-id="${cant}" id="mailCondicion_${cant}" name="mailCondicion_${oper}_${cant}" class="form-control input-sm validar-condicion" placeholder="Ingrese la condicion" disabled>
                </td>

                <td>
                    <button type="button" class="btn btn-danger btn-sm" onclick="eliminarFilaFiltro('${oper}', ${cant})"><i class="fa fa-trash"></i></button>
                </td>

            </tr>
        `;

        $("#correoCondiciones tbody").append(row);

        cantFiltrosEmail +=1;
    }

    function eliminarFilaFiltro(oper, id){
        if(oper == 'add'){
            $("#fila_"+id).remove();
        }else{
            let filtroId = $("#campo_"+id).val();

            // Aqui elimina el filtro en la bd
            $.ajax({
                url     : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G31/G31_CRUD.php?deleteFiltro=true',
                type    : 'POST',
                dataType: 'json',
                data    : { filtroId : filtroId},
                beforeSend : function(){
                    $.blockUI({ 
                        baseZ: 2000,
                        message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait;?>' });
                },
                complete : function(){
                    $.unblockUI();
                },
                success : function(data){
                    if(data.eliminado){
                        alertify.success(data.message);
                        $("#fila_"+id).remove();
                    }else{
                        alertify.error(data.message);
                    }
                },
                error: function(data){
                    if(data.responseText){
                        alertify.error("Hubo un error al borrar la información" + data.responseText);
                    }else{
                        alertify.error("Hubo un error al borrar la información");
                    }
                }
            });
        }
    }

    function cambioTipoCondicion(id){
        var tipo_condicion = $('#mailTipoCondicion_'+id).val();
        if(tipo_condicion != 100){
            $('#mailCondicion_'+id).attr('disabled', false);
        }else{
            $('#mailCondicion_'+id).attr('disabled', true);
        }
    }

    function eliminarFiltrosFlecha(id){
        $.ajax({
            url     : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G31/G31_CRUD.php?deleteTodosFiltros=true',
            type    : 'POST',
            dataType: 'json',
            data    : { pasoId : id},
            beforeSend : function(){
                $.blockUI({ 
                    baseZ: 2000,
                    message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait;?>' });
            },
            complete : function(){
                $.unblockUI();
            },
            success : function(data){
                if(data.eliminado){
                    alertify.success(data.message);
                }else{
                    alertify.error(data.message);
                }
            },
            error: function(data){
                if(data.responseText){
                    alertify.error("Hubo un error al borrar la información" + data.responseText);
                }else{
                    alertify.error("Hubo un error al borrar la información");
                }
            }
        });
    }

</script>

<!-- Funciones para los sms entrante -->
<script>
    function quitarConexionSmsEntrante(pasoId){
        $.ajax({
            url: '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G32/G32_CRUD.php?quitarConexiones=true',  
            type    : 'POST',
            dataType: 'json',
            data    : { pasoId : pasoId},
            beforeSend : function(){
                $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
            },
            success: function(data){
                console.log(data);
                alertify.success("Link eliminado");
            },
            complete : function(){
                $.unblockUI();
            },
            error: function(data){
                if(data.responseText){
                    alertify.error("Hubo un error al eliminar la flecha" + data.responseText);
                }else{
                    alertify.error("Hubo un error al eliminar la flecha");
                }
            }
        });
    }

    function crearFlecha(pasoFrom, pasoTo){
        $.ajax({
            url: '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G32/G32_CRUD.php?crearFlecha=true',  
            type    : 'POST',
            dataType: 'json',
            data    : { pasoFrom : pasoFrom, pasoTo : pasoTo},
            beforeSend : function(){
                $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
            },
            success: function(data){
                console.log(data);
                if(data.estado == 'ok'){
                    alertify.success("Link creado");
                    location.reload();
                }else if(data.estado == 'fallo'){
                    alertify.error(data.mensaje);
                    load();
                }else{
                    alertify.error(data.mensaje);
                    load();
                }
            },
            complete : function(){
                $.unblockUI();
            },
            error: function(data){
                if(data.responseText){
                    alertify.error("Hubo un error al crear la flecha" + data.responseText);
                }else{
                    alertify.error("Hubo un error al crear la flecha");
                }
            }
        });
    }
</script>

<!-- Funciones para el bot -->
<script>
    function obtenerBot(llaveInvocar, category){

        // Valido si el paso es nuevo
        $.ajax({
            url  : '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G26/G26_CRUD.php?getPasoEsNuevo=true&id_paso='+llaveInvocar,
            type : 'get',
            dataType: 'json',
            success : function(data){
                if(data){
                    if(data.esNuevo === true){

                        $("#configuracionInicialBotNombre").val("bot_"+llaveInvocar);
                        $("#configuracionInicialPaso").val(llaveInvocar);

                        $("#configuracionInicialBot").modal('show');
                        $.unblockUI();
                    }else{
                        // Si ya existe solo traigo la informacion
                        
                        traerInformacionBot(llaveInvocar, category);
                    }
                }

            },
            beforeSend : function(){
                $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
            },
            complete : function(){
                //$.unblockUI();
            }
        });
    }

    function traerInformacionBot(llaveInvocar, category){
        $.ajax({
            url  : '<?=base_url?>mostrar_popups.php?view=chatbot&id_paso='+llaveInvocar+'&poblacion=<?php echo $str_poblacion; ?>',
            type : 'get',
            success : function(data){
                $("#title_estrat").html('<?php echo $str_strategia_edicion.' '; ?>'+category);
                $("#divIframe").html(data);
                $("#editarDatos").modal();
            },
            beforeSend : function(){
                $.blockUI({ message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
            },
            complete : function(){
                //$.unblockUI();
            }
        });
    }

    function guardarConfiguracionInicialBot(){
        let valido = true;

        let nombre = $("#configuracionInicialBotNombre").val();
        let paso = $("#configuracionInicialPaso").val();

        if(nombre == ''){
            $("#configuracionInicialBotNombre").focus();

            alertify.error("El campo nombre no puede estar vacio");
            valido = false;
        }

        if(valido){
            
            var formData = new FormData($("#formConfiguracionIncialBot")[0]);

            $.ajax({
                url: '<?=base_url?>cruds/DYALOGOCRM_SISTEMA/G26/G26_CRUD.php?guardarConfiguracionInicial=true&id_paso='+paso+'&huesped=<?php echo $_SESSION['HUESPED'];?>',
                type: 'POST',
                dataType: 'json',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend : function(){
                    $.blockUI({  baseZ: 2000, message: '<img src="<?=base_url?>assets/img/clock.gif" /> <?php echo $str_message_wait_c;?>' });
                },
                complete : function(){
                    $.unblockUI();
                },
                //una vez finalizado correctamente
                success: function(data){
                    $("#formConfiguracionIncialBot")[0].reset();
                    $("#configuracionInicialBot").modal('hide');
                    alertify.success("Información guardada con exito");
                    traerInformacionBot(paso, 'ivrTexto');
                },
                error: function(data){
                    console.log(data);
                    if(data.responseText){
                        alertify.error("Hubo un error al guardar la información" + data.responseText);
                    }else{
                        alertify.error("Hubo un error al guardar la información");
                    }
                }
            });
        }
    }
</script>