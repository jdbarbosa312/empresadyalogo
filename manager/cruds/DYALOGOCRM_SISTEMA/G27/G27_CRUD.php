<?php 
    session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include_once(__DIR__."../../../../pages/conexion.php");

    function guardar_auditoria($accion, $superAccion){
        global $mysqli;
        global $BaseDatos_systema;
        $str_Lsql = "INSERT INTO ".$BaseDatos_systema."AUACAD (AUACAD_Fecha_____b , AUACAD_Hora______b, AUACAD_Ejecutor__b, AUACAD_TipoAcci__b , AUACAD_SubTipAcc_b, AUACAD_Accion____b , AUACAD_Huesped___b ) VALUES ('".date('Y-m-d H:s:i')."', '".date('Y-m-d H:s:i')."', ".$_SESSION['IDENTIFICACION'].", 'G14', '".$accion."', '".$superAccion."', ".$_SESSION['HUESPED']." );";
        $mysqli->query($str_Lsql);
    }   

    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

        // Trae toda la info del paso
        if(isset($_GET['CallDatos'])){

            $esql = "SELECT ESTPAS_ConsInte__b AS id, ESTPAS_Nombre__b AS nombre, ESTPAS_Tipo______b AS tipo, ESTPAS_activo AS activo FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$_POST['id_paso']." LIMIT 1";
            $rese = $mysqli->query($esql);
            $paso = $rese->fetch_array();

            $canalesWa = array();
            $i = 0;
            
            // Traigo los canales de whatsapp
            $sqlC = "SELECT * FROM dy_whatsapp.canales WHERE id_huesped = ".$_POST['huesped'];
            $resC = $mysqli->query($sqlC);
            if($resC){
                while ($item = $resC->fetch_object()) {
                    $canalesWa[$i]['id'] = $item->id;
                    $canalesWa[$i]['nombre'] = $item->nombre;
                    $canalesWa[$i]['cuenta'] = $item->cuenta;
                    $i++;
                }
            }

            // Valido si el paso ya esta configurado
            $existe = false;
            $envio_plantilla = array();
            $campos = array();
            $plantillas = array();
            $i = 0;

            $sql = "SELECT * FROM ".$dyalogo_canales_electronicos.".dy_wa_envio_plantilla WHERE id_estpas = ".$_POST['id_paso']." LIMIT 1";
            $res = $mysqli->query($sql);
            if($res->num_rows > 0){
                $envio_plantilla = $res->fetch_array();
                $existe = true;

                // Traigo los campos de la plantilla
                $csql = "SELECT e.*, p.nombre_variable FROM ".$dyalogo_canales_electronicos.".dy_wa_variables_envio e JOIN ".$dyalogo_canales_electronicos.".dy_wa_variables_plantilla p ON e.id_variable_plantilla = p.id WHERE id_envio_plantilla = ".$envio_plantilla['id'];
                $resC = $mysqli->query($csql);
                if($resC){
                    while ($item = $resC->fetch_object()) {
                        $campos[$i]['id'] = $item->id;
                        $campos[$i]['nombre_variable'] = $item->nombre_variable;
                        $campos[$i]['id_envio_plantilla'] = $item->id_envio_plantilla;
                        $campos[$i]['id_variable_plantilla'] = $item->id_variable_plantilla;
                        $campos[$i]['id_pregun'] = $item->id_pregun;
                        $campos[$i]['accion'] = $item->accion;
                        $campos[$i]['valor_estatico'] = $item->valor_estatico;
                        $i++;
                    }
                }

                // Traigo las plantillas del huesped
                $plantillas = getPlantillas($envio_plantilla['id_cuenta_whatsapp']);
            }

            echo json_encode([
                "paso" => $paso,
                "canales" => $canalesWa, 
                "existe" => $existe,
                "envio_plantilla" => $envio_plantilla,
                "campos" => $campos, 
                "plantillas" => $plantillas
            ]);
        }

        // Traigo las plantilla de una cuenta de whatsapp especifica
        if(isset($_GET['getPlantillas'])){
            if(isset($_POST['cuentawa']) && $_POST['cuentawa'] != ''){

                $plantillas = getPlantillas($_POST['cuentawa']);

                echo json_encode(["plantillas" => $plantillas]);
            }
        }

        // Traigo los datos de una plantilla
        if(isset($_GET['getCamposPlantilla'])){
            $idPlantilla = $_POST['idPlantilla'];
            
            if($idPlantilla != ''){
                $sqlCP = "SELECT * FROM ".$dyalogo_canales_electronicos.".dy_wa_variables_plantilla WHERE id_plantilla = ".$idPlantilla;
                $resCP = $mysqli->query($sqlCP);

                $campos = array();
                $i = 0;

                if($sqlCP){
                    while ($item = $resCP->fetch_object()) {
                        $campos[$i]['id'] = $item->id;
                        $campos[$i]['nombre'] = $item->nombre_variable;
                        $campos[$i]['id_plantilla'] = $item->id_plantilla;
                        $i++;
                    }
                }

                echo json_encode(["campos" => $campos]);
            }else{
                echo json_encode([]);
            }
            
        }

        if(isset($_GET['insertarDatos']) && isset($_POST["oper"])){
            
            if($_POST["oper"] == "add"){
                
                // Actualizo el paso
                $pasoSql = "UPDATE ".$BaseDatos_systema.".ESTPAS SET ESTPAS_Comentari_b = '".$_POST['nombre']."', ESTPAS_activo = '".$_POST['pasoActivo']."' WHERE ESTPAS_ConsInte__b = ".$_POST['id_paso'];
                $mysqli->query($pasoSql);
                
                $Isql = "INSERT INTO ".$dyalogo_canales_electronicos.".dy_wa_envio_plantilla (id_estpas, nombre, id_cuenta_whatsapp, id_plantilla, id_pregun_telefono) VALUES (".$_POST['id_paso'].", '".$_POST['nombre']."', ".$_POST['cuentawa'].", ".$_POST['plantilla'].", ".$_POST['to'].")";
                $mysqli->query($Isql);
                $id_envio_plantilla = $mysqli->insert_id;

                // Inserto en variables plantillas
                $arr_campos = $_POST['campov'];
                foreach ($arr_campos as $key => $value) {

                    $pregun_id = 0;
                    $valor_estatico = '';

                    if($value['accion'] == 1 && isset($value['pregun']) ){
                        $pregun_id = $value['pregun'];
                    }
                    
                    if($value['accion'] == 2 && isset($value['estatico']) ){
                        $valor_estatico = $value['estatico'];
                    }

                    $Isql = "INSERT INTO ".$dyalogo_canales_electronicos.".dy_wa_variables_envio (id_envio_plantilla, id_variable_plantilla, accion, id_pregun, valor_estatico) VALUES ('".$id_envio_plantilla."', '".$value['id']."', ".$value['accion'].", ".$pregun_id.", '".$valor_estatico."')";
                    
                    $mysqli->query($Isql);
                }

                echo json_encode(["id" => $id_envio_plantilla]);
            }

            if($_POST["oper"] == "edit"){
                // Actualizo el paso
                $activo = isset($_POST['pasoActivo']) ? $_POST['pasoActivo'] : 0;

                $pasoSql = "UPDATE ".$BaseDatos_systema.".ESTPAS SET ESTPAS_Comentari_b = '".$_POST['nombre']."', ESTPAS_activo = '".$activo."' WHERE ESTPAS_ConsInte__b = ".$_POST['id_paso'];
                $mysqli->query($pasoSql);

                $Usql = "UPDATE ".$dyalogo_canales_electronicos.".dy_wa_envio_plantilla SET nombre = '".$_POST['nombre']."', id_cuenta_whatsapp = '".$_POST['cuentawa']."', id_plantilla = '".$_POST['plantilla']."', id_pregun_telefono = '".$_POST['to']."' WHERE id_estpas = ".$_POST['id_paso']; 
                $mysqli->query($Usql);
                
                // actualizo variables plantillas
                $idEnvioPlantilla = $_POST['envio_plantilla'];
                $arr_campos = $_POST['campov'];
                foreach ($arr_campos as $key => $value) {

                    $pregun_id = 0;
                    $valor_estatico = '';

                    if($value['accion'] == 1 && isset($value['pregun']) ){
                        $pregun_id = $value['pregun'];
                    }
                    
                    if($value['accion'] == 2 && isset($value['estatico']) ){
                        $valor_estatico = $value['estatico'];
                    }

                    $Usql = "UPDATE ".$dyalogo_canales_electronicos.".dy_wa_variables_envio SET id_pregun = ".$pregun_id.", accion= '".$value['accion']."', valor_estatico = '".$valor_estatico."' WHERE id_envio_plantilla = '".$idEnvioPlantilla."' AND id_variable_plantilla = '".$value['id']."'";                    
                    $mysqli->query($Usql);
                }

                echo json_encode(["id" => $idEnvioPlantilla]);
            }
        }
    }

    function getPlantillas($cuentaWa){
        global $dyalogo_canales_electronicos;
        global $mysqli;

        $sqlP = "SELECT * FROM ".$dyalogo_canales_electronicos.".dy_wa_plantillas WHERE id_cuenta_whatsapp = ".$cuentaWa;
        $resP = $mysqli->query($sqlP);

        $plantillas = array();
        $i = 0;

        if($resP){
            while ($item = $resP->fetch_object()) {
                $plantillas[$i]['id'] = $item->id;
                $plantillas[$i]['nombre'] = $item->nombre_plantilla;
                $plantillas[$i]['contenido'] = $item->contenido_plantilla;
                $i++;
            }
        }

        return $plantillas;
    }

?>