<?php
// Este archivo se usa para descargar por ahora los archivos adjutos que se definen 
// desde la bola de salida de email

if(!empty($_GET['file'])){

    $pathAdjuntos = "/home/dyalogo/adjuntos/";
    
    $fileName = basename($_GET['file']);
    $filePath = $pathAdjuntos.$fileName;

    if(!empty($fileName) && file_exists($filePath)){

        // Defino las cabeceras
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$fileName");
        header("Content-Type: application/zip");
        header("Content-Transfer-Encoding: binary");

        // Read the file
        readfile($filePath);
        exit;
    }else{
        echo 'El archivo no existe.';
    }
}


?>