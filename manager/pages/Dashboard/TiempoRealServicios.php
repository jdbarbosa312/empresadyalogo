<?php
  session_start();

  if (isset($_GET["TiempoReal"])) {
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once('../conexion.php');
    include("../../global/WSCoreClient.php");
    include("../../global/funcionesGenerales.php");
    date_default_timezone_set('America/Bogota');

    $intIdUsuario_t = $_SESSION['IDENTIFICACION'];

    $arrJsonAgentes_t = json_decode(listaAgentesTiempoReal($intIdUsuario_t));

    $arrJsonMetas_t   = json_decode(metricasTiempoReal($intIdUsuario_t));

    $arrTiempoReal_t = [];

    $intAvtividad_t = count($arrJsonMetas_t->objSerializar_t);

    if ($intAvtividad_t > 0) {

      foreach ($arrJsonMetas_t->objSerializar_t as $keyEstrat => $valueEstrat) {

        $intIdEstrat_t = $valueEstrat->objEstrat_t->ESTRATConsInteb;
        $strNombreEstrat_t = $valueEstrat->objEstrat_t->ESTRATNombreb;
        $strColorEstrat_t = $valueEstrat->objEstrat_t->ESTRATColorb;

        $arrTiempoReal_t[$keyEstrat] = [
          "intIdEstrat_t"     => $intIdEstrat_t,
          "strNombreEstrat_t" => $strNombreEstrat_t,
          "strColorEstrat_t"  => $strColorEstrat_t,
          "arrEstpas_t"       => null
        ];

        $arrPasos_t = [];

        foreach ($valueEstrat->lstPasos_t as $keyPaso => $valuePaso) {

          $intNoDisponibles_t = 0;
          $intPausados_t = 0;
          $intOcupados_t = 0;
          $intOcupados2_t = 0;
          $intDisponibles_t = 0;

          $intIdEstpas_t = $valuePaso->objEstpas_t->ESTPASConsInteb;
          $intIdCampan_t = $valuePaso->objEstpas_t->ESTPASConsInteCAMPANb;
          $strNombreCampan_t = $valuePaso->objEstpas_t->ESTPASComentarib;
          $intTipoEstpas_t = $valuePaso->objEstpas_t->ESTPASTipob;

          $arrPasos_t[$keyPaso] = [
            "intIdEstpas_t"     => $intIdEstpas_t,
            "intIdCampan_t"     => $intIdCampan_t,
            "strNombreCampan_t" => $strNombreCampan_t,
            "strSentido_t"      => sentido($intTipoEstpas_t),
            "arrMetas_t"        => null,
            "arrAgentes_t"      => null
          ];

          $arrOrdenMetas_t = [];
          $arrMetas_t = [];

          if ($intTipoEstpas_t == 6) {

            $arrOrdenMetas_t = [0=>["metas"=>["#Sin gestion","%Sin gestion"],
                                    "color"=>"#F37D00","icono"=>"fa fa-users"],
                                1=>["metas"=>["#Gestiones","TMO"],
                                    "color"=>"#F3BC00","icono"=>"fa fa-battery-three-quarters"],
                                2=>["metas"=>["#Contactados","%Contactados"],
                                    "color"=>"#299E00","icono"=>"fa fa-hourglass-2"],
                                3=>["metas"=>["#Efectivos","%Efectivos"],
                                    "color"=>"#009FE3","icono"=>"fa fa-thumbs-o-up"]];

            foreach ($arrOrdenMetas_t as $keyGrup0 => $valueGrup0) {

              foreach ($valueGrup0["metas"] as $keyM0 => $valueM0) {

                $intIter0_t = 0;

                foreach ($valuePaso->lstDefinicionMetricas_t as $keyMeta0 => $valueMeta0) {

                  if ($valueM0 == $valueMeta0->METDEFNombreb) {
                    $intIter0_t ++;
                  }


                }

                if ($intIter0_t == 0) {

                  array_push($valuePaso->lstDefinicionMetricas_t, (object)["METDEFNombreb"=>$valueM0,"objMedicion_t"=>(object)["METMEDValorb"=>"N/A"]]);

                }

              }

            }

            foreach ($arrOrdenMetas_t as $keyGrup => $valueGrup) {
              
                $arrMetas_t[$keyGrup]["strColor_t"] = $valueGrup["color"];
                $arrMetas_t[$keyGrup]["strIcono_t"] = $valueGrup["icono"];

              foreach ($valueGrup["metas"] as $keyM => $valueM) {

                foreach ($valuePaso->lstDefinicionMetricas_t as $keyMeta => $valueMeta) {

                  if ($valueM == $valueMeta->METDEFNombreb) {

                    $intValor_t = "N/A";

                    if ($valueMeta->objMedicion_t->METMEDValorb != "N/A") {
                      $intValor_t = ceil($valueMeta->objMedicion_t->METMEDValorb);
                    }

                    $arrMetas_t[$keyGrup]["arrMetas_t"][$keyM] = ["strMeta_t"=>$valueM,"strValor_t"=>$intValor_t];

                  }


                }
                
                
              }


            }


          }elseif ($intTipoEstpas_t == 1) {

            $arrOrdenMetas_t = [0=>["metas"=>["#En cola","colaChat","colaWhatsapp","colaFacebook","colaEmail","colaWeb","%TSF","TSFchat","TSFwhatsapp","TSFfacebook","TSFemail","TSFweb"],
                                    "color"=>"#F37D00","icono"=>"fa fa-users"],
                                1=>["metas"=>["#Recibidas","recibChat","recibWhatsapp","recibFacebook","recibEmail","recibWeb","TMO","TMOchat","TMOwhatsapp","TMOfacebook","TMOemail","TMOweb"],
                                    "color"=>"#F3BC00","icono"=>"fa fa-battery-three-quarters"],
                                2=>["metas"=>["#Contestadas","conChat","conWhatsapp","conFacebook","conEmail","conWeb","%Contestadas","PORCchat","PORCwhatsapp","PORCfacebook","PORCemail","PORCweb"],
                                    "color"=>"#299E00","icono"=>"fa fa-hourglass-2"],
                                3=>["metas"=>["#Efectivos","efecChat","efecWhatsapp","efecFacebook","efecEmail","efecWeb","%Efectivos","POREchat","POREwhatsapp","POREfacebook","POREemail","POREweb"],
                                    "color"=>"#009FE3","icono"=>"fa fa-thumbs-o-up"]];

            foreach($valuePaso->lstDefinicionMetricas_t as $variantKey => $variantValue){

              if (isset($variantValue->objMedicion_t->METMEDCanalb) && $variantValue->objMedicion_t->METMEDCanalb != "telefonia") {

                  $strNombreVariante_t = nombreMetaVariante($variantValue->METDEFNombreb);


                  switch($variantValue->objMedicion_t->METMEDCanalb){

                    case "chat":

                      array_push($valuePaso->lstDefinicionMetricas_t, (object)["METDEFNombreb"=>$strNombreVariante_t."Chat","objMedicion_t"=>(object)["METMEDValorb"=>$variantValue->objMedicion_t->METMEDValorb,"METMEDCanalb"=>"chat"]]);

                      break;
                    case "whatsapp":
                      array_push($valuePaso->lstDefinicionMetricas_t, (object)["METDEFNombreb"=>$strNombreVariante_t."Whatsapp","objMedicion_t"=>(object)["METMEDValorb"=>$variantValue->objMedicion_t->METMEDValorb,"METMEDCanalb"=>"whatsapp"]]);
                      break;
                    case "facebook":
                      array_push($valuePaso->lstDefinicionMetricas_t, (object)["METDEFNombreb"=>$strNombreVariante_t."Facebook","objMedicion_t"=>(object)["METMEDValorb"=>$variantValue->objMedicion_t->METMEDValorb,"METMEDCanalb"=>"facebook"]]);
                      break;
                    case "email":
                      array_push($valuePaso->lstDefinicionMetricas_t, (object)["METDEFNombreb"=>$strNombreVariante_t."Email","objMedicion_t"=>(object)["METMEDValorb"=>$variantValue->objMedicion_t->METMEDValorb,"METMEDCanalb"=>"email"]]);
                      break;
                    case "webform":
                      array_push($valuePaso->lstDefinicionMetricas_t, (object)["METDEFNombreb"=>$strNombreVariante_t."Web","objMedicion_t"=>(object)["METMEDValorb"=>$variantValue->objMedicion_t->METMEDValorb,"METMEDCanalb"=>"webform"]]);
                      break;

                  }

                  unset($arrJsonMetas_t->objSerializar_t[$keyEstrat]->lstPasos_t[$keyPaso]->lstDefinicionMetricas_t[$variantKey]);

              }


            }

            foreach ($arrOrdenMetas_t as $keyGrup0 => $valueGrup0) {

              foreach ($valueGrup0["metas"] as $keyM0 => $valueM0) {

                $intIter0_t = 0;

                foreach ($valuePaso->lstDefinicionMetricas_t as $keyMeta0 => $valueMeta0) {

                  if ($valueM0 == $valueMeta0->METDEFNombreb && isset($valueMeta0->objMedicion_t)) {

                      $intIter0_t ++;

                  }


                }

                if ($intIter0_t == 0) {

                  array_push($valuePaso->lstDefinicionMetricas_t, (object)["METDEFNombreb"=>$valueM0,"objMedicion_t"=>(object)["METMEDValorb"=>"off","METMEDCanalb"=>"telefonia"]]);

                }

              }

            }

            foreach ($arrOrdenMetas_t as $keyGrup => $valueGrup) {
              
                $arrMetas_t[$keyGrup]["strColor_t"] = $valueGrup["color"];
                $arrMetas_t[$keyGrup]["strIcono_t"] = $valueGrup["icono"];

              foreach ($valueGrup["metas"] as $keyM => $valueM) {

                foreach ($valuePaso->lstDefinicionMetricas_t as $keyMeta => $valueMeta) {

                  if ($valueM == $valueMeta->METDEFNombreb) {

                    $intValor_t = "off";

                    if ($valueMeta->objMedicion_t->METMEDValorb != "off") {
                      $intValor_t = ceil($valueMeta->objMedicion_t->METMEDValorb);
                    }

                    $arrMetas_t[$keyGrup]["arrMetas_t"][$keyM] = ["strMeta_t"=>$valueM,"strValor_t"=>$intValor_t];

                  }


                }
                
                
              }


            }

          }

          $arrAgentes_t = [];

          foreach ($arrJsonAgentes_t->objSerializar_t as $keyAgente => $valueAgente) {

            if (property_exists($valueAgente,"intIdCampanaActual_t") == false) {

              $valueAgente->intIdCampanaActual_t = null;

            }

            if (in_array($intIdCampan_t, $valueAgente->lstCampanasAsignadas_t) || $intIdCampan_t == $valueAgente->intIdCampanaActual_t) {
              
              $strNombreEstpasActaual_t = $valueAgente->campanaActual;
              $strCanalActual_t = $valueAgente->canalActual;
              $strDuracionEstadoTiempo_t = $valueAgente->duracionEstadoTiempo;

              $booEnConversacion = isset($valueAgente->enConversacion) ? $valueAgente->enConversacion : false;
              $strEstado_t = $valueAgente->estado;

              $strFecha1_t = new DateTime($valueAgente->strFechaHoraCambioEstado_t);
              $strFecha2_t = new DateTime("now");
              $strDiffFecha_t = $strFecha1_t->diff($strFecha2_t);
              $strFecha_t = $strDiffFecha_t->format("%H:%I:%S");
              $strFechaHoraCambioEstadoFormat_t = $strFecha_t;
              $strFechaHoraCambioEstado_t = $valueAgente->strFechaHoraCambioEstado_t;

              $strFoto_t = $valueAgente->fotoActual;
              $intIdAgente_t = $valueAgente->idUsuario;
              $strIdentificacionAgente_t = $valueAgente->identificacionUsuario;

              $intIdCampanActual_t = null;
              if (isset($valueAgente->intIdCampanaActual_t)) {
                $intIdCampanActual_t = $valueAgente->intIdCampanaActual_t;
              }

              $strNombreAgente_t = $valueAgente->nombreUsuario;
              $strPausa_t = $valueAgente->pausa;

              if ($strEstado_t == "Disponible") {
                $strColorEstado_t = "#009603";
                $intDisponibles_t ++;
              }elseif ($strEstado_t == "Pausado") {
                $strColorEstado_t = "#f39d00";
                $intPausados_t ++;
              }elseif (stristr($strEstado_t, 'Ocupado')) {
                if ($intIdCampanActual_t == $intIdCampan_t) {
                  $strColorEstado_t = "#ff1c00";
                  $intOcupados_t ++;
                }else{
                  $strColorEstado_t = "#941200";
                  $intOcupados2_t ++;
                }
              }else{
                $strColorEstado_t = "#9c9c9c";
                $intNoDisponibles_t ++;
              }

              $strFoto_t = "assets/img/Kakashi.fw.png";
              if(file_exists("/var/../Dyalogo/img_usuarios/usr".$intIdAgente_t.".jpg")){
                  $strFoto_t = "/DyalogoImagenes/usr".$intIdAgente_t.".jpg";
              }
              
              $arrAgentes_t[$keyAgente] = [
                "strNombreEstpasActaual_t"  => $strNombreEstpasActaual_t,
                "strCanalActual_t"  => $strCanalActual_t,
                "strDuracionEstadoTiempo_t" => $strDuracionEstadoTiempo_t,
                "strEstado_t"               => $strEstado_t,
                "booEnConversacion"         => $booEnConversacion,
                "strColorEstado_t"          => $strColorEstado_t,
                "strFechaHoraCambioEstadoFormat_t"  => $strFechaHoraCambioEstadoFormat_t,
                "strFechaHoraCambioEstado_t"  => $strFechaHoraCambioEstado_t,
                "strFoto_t"                 => $strFoto_t,
                "intIdAgente_t"             => $intIdAgente_t,
                "strIdentificacionAgente_t" => $strIdentificacionAgente_t,
                "intIdCampanActual_t"       => $intIdCampanActual_t,
                "strNombreAgente_t"         => $strNombreAgente_t,
                "strPausa_t"                => $strPausa_t,
                "strFoto_t"                 => $strFoto_t
              ];

            }

          }

          $arrAgentes_t = unique_multidim_array($arrAgentes_t,"strIdentificacionAgente_t");

          $arrOrdenAgentes_t = ["#9c9c9c","#009603","#f39d00","#941200","#ff1c00"];

          $arrAgentesOrdenados_t = [];

          $intIter_t = 0;


          //JDBD - Organizamos los agentes por estado.
          foreach ($arrOrdenAgentes_t as $color) {

            foreach ($arrAgentes_t as $array) {

              if ($color == $array["strColorEstado_t"]) {

                $arrAgentesOrdenados_t[$intIter_t] = $array;

                $intIter_t ++;

              }

            }

          }

          $arrPasos_t[$keyPaso]["arrMetas_t"] = $arrMetas_t;
          $arrPasos_t[$keyPaso]["arrAgentes_t"] = $arrAgentesOrdenados_t;
          $arrPasos_t[$keyPaso]["arrEstados_t"] = [0=>["strEstado_t"=>"<i class=\"fa fa-warning\"></i> Sin conexion",
                                                       "strColor_t" => "#9c9c9c",
                                                       "intEstado_t"=>$intNoDisponibles_t],
                                                   1=>["strEstado_t"=>"Pausados",
                                                       "strColor_t" => "#f39d00",
                                                       "intEstado_t"=>$intPausados_t],
                                                   2=>["strEstado_t"=>"Ocupados",
                                                       "strColor_t" => "#ff1c00",
                                                       "intEstado_t"=>$intOcupados_t],
                                                   3=>["strEstado_t"=>"No disponible",
                                                       "strColor_t" => "#941200",
                                                       "intEstado_t"=>$intOcupados2_t],
                                                   4=>["strEstado_t"=>"Disponibles",
                                                       "strColor_t" => "#009603",
                                                       "intEstado_t"=>$intDisponibles_t]];

        }

        $arrTiempoReal_t[$keyEstrat]["arrEstpas_t"] = $arrPasos_t;

      }

    }

  
    echo json_encode($arrTiempoReal_t);
  
  }

  if (isset($_GET["ConsAcordeon"])) {

    if (isset($_SESSION["ACORDEONES"])) {

      $arrAcordeones_t = json_decode($_SESSION["ACORDEONES"]);

      $strIdAcordeon_t = $_POST["strIdAcordeon_t"];

      $itEA = 0;

      foreach ($arrAcordeones_t as $EAkey => $EAvalue) {

        if ($EAvalue->strIdAcordeon_t == $strIdAcordeon_t) {

          $itEA ++;

          if ($EAvalue->strEstado_t == "none") {

            echo json_encode(["box box collapsed-box","plus","none"]);

          }else{

            echo json_encode(["box box","minus","block"]);

          }

        }

      }

      if ($itEA == 0) {

          echo json_encode(["box box","minus","block"]);
          
      }



    }else{

      echo json_encode(["box box","minus","block"]);

    }

  }

  if (isset($_GET["session2"])) {

    echo $_SESSION["ACORDEONES"];

  }

function sentido($intTipoCampan_p){

  if ($intTipoCampan_p == 1) {
    return "Entrante";
  }elseif($intTipoCampan_p == 6){
    return "Saliente";
  }

}



function unique_multidim_array($array, $key) { 
   $temp_array = array(); 
   $i = 0; 
   $key_array = array(); 

   foreach($array as $val) { 
       if (!in_array($val[$key], $key_array)) { 
           $key_array[$i] = $val[$key]; 
           $temp_array[$i] = $val; 
       } 
       $i++; 
   } 
   return $temp_array; 
} 

function nombreMetaVariante($nombreMeta_p){

  switch($nombreMeta_p){

    case "#En cola":
        return "cola";
      break;
    case "#Recibidas":
        return "recib";
      break;
    case "#Contestadas":
        return "con";
      break;
    case "#Efectivos":
        return "efec";
      break;

  }

}

?>