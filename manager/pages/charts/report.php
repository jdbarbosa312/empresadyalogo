<?php

require_once ("../conexion.php");
require_once ("../../carga/Excel.php");
require_once ("../../xlsxwriter/xlsxwriter.class.php");
include "languages/spanish.php";
require_once('../../../helpers/parameters.php');

$arrDataFiltros_t = [];


if (isset($_POST["jsonTotalFil"]) && $_POST["jsonTotalFil"] != "") {

    $jsonTotalFil = str_replace("\\", "", $_POST["jsonTotalFil"]);
    $arrTotalFil = (array)json_decode($jsonTotalFil);

    $jsonCondiciones = str_replace("\\", "", $_POST["jsonCondiciones"]);
    $arrCondiciones = (array)json_decode($jsonCondiciones);

    $arrDataFiltros_t["totalFiltros"]=$arrTotalFil;
    $arrDataFiltros_t["dataFiltros"]=[];

    foreach ($arrTotalFil as $key => $value) {

            array_push($arrDataFiltros_t["dataFiltros"], ["selCampo_".$value=>$arrCondiciones["selCampo_".$value],"selOperador_".$value=>$arrCondiciones["selOperador_".$value],"valor_".$value=>$arrCondiciones["valor_".$value],"tipo_".$value=>$arrCondiciones["tipo_".$value],"selCondicion_".$value=>(isset($arrCondiciones["selCondicion_".$value]) ? $arrCondiciones["selCondicion_".$value] : null)]);  

    }

}else{

    if (isset($_POST["totalFiltros"]) && $_POST["totalFiltros"] > 0) {

        $arrTotalFiltros_t = $_POST["totalFiltros"];

        $arrDataFiltros_t["totalFiltros"]=$_POST["totalFiltros"];
        $arrDataFiltros_t["dataFiltros"]=[];

        foreach ($arrTotalFiltros_t as $value) {

            array_push($arrDataFiltros_t["dataFiltros"], ["selCampo_".$value=>$_POST["selCampo_".$value],"selOperador_".$value=>$_POST["selOperador_".$value],"valor_".$value=>$_POST["valor_".$value],"tipo_".$value=>$_POST["tipo_".$value],"selCondicion_".$value=>(isset($_POST["selCondicion_".$value]) ? $_POST["selCondicion_".$value] : null)]);    

        }


    }
    
}


$tipoReport_t = (isset($_POST["tipoReport_t"]) ? $_POST["tipoReport_t"] : 0);
$strFechaIn_t = (isset($_POST["strFechaIn_t"]) ? $_POST["strFechaIn_t"] : date('Y-m-d'));
$strFechaFn_t = (isset($_POST["strFechaFn_t"]) ? $_POST["strFechaFn_t"] : date('Y-m-d'));
$intIdPeriodo_t = (isset($_POST["intIdPeriodo_t"]) ? $_POST["intIdPeriodo_t"] : 0);
$strLimit_t = (isset($_POST["strLimit_t"]) ? $_POST["strLimit_t"] : "no");
$intIdHuesped_t = (isset($_POST["intIdHuesped_t"]) ? $_POST["intIdHuesped_t"] : 0);
$intIdEstrat_t = isset($_POST["intIdEstrat_t"]) ? $_POST["intIdEstrat_t"] : 0;
if(!is_numeric($intIdEstrat_t)){
    $sqlIdEstrategia=$mysqli->query("select ESTRAT_ConsInte__b as id from {$BaseDatos_systema}.ESTRAT where md5(concat('".clave_get."', ESTRAT_ConsInte__b)) = '".$_POST["intIdEstrat_t"]."'");
    if($sqlIdEstrategia && $sqlIdEstrategia->num_rows==1){
        $sqlIdEstrategia=$sqlIdEstrategia->fetch_object();
        $intIdEstrat_t = $sqlIdEstrategia->id;
    }
}
$intIdCBX_t = (isset($_POST["intIdCBX_t"]) ? $_POST["intIdCBX_t"] : 0);
$intIdBd_t = (isset($_POST["intIdBd_t"]) ? $_POST["intIdBd_t"] : 0);
$intIdPaso_t = (isset($_POST["intIdPaso_t"]) ? $_POST["intIdPaso_t"] : 0);
$intIdTipo_t = (isset($_POST["intIdTipo_t"]) ? $_POST["intIdTipo_t"] : 0);
$intIdMuestra_t = (isset($_POST["intIdMuestra_t"]) ? $_POST["intIdMuestra_t"] : -1);
$intIdGuion_t = (isset($_POST["intIdGuion_t"]) ? $_POST["intIdGuion_t"] : 0);
$intFilas_t = (isset($_POST["intFilas_t"]) ? $_POST["intFilas_t"] : 0);
$intLimite_t = (isset($_POST["intLimite_t"]) ? $_POST["intLimite_t"] : 10);
$intPaginaActual_t = (isset($_POST["intPaginaActual_t"]) ? $_POST["intPaginaActual_t"] : 1);
$strTablaTemp_t = (isset($_POST["strTablaTemp_t"]) ? $_POST["strTablaTemp_t"] : null);
$strCampoUnion_t = (isset($_POST["strCampoUnion_t"]) ? $_POST["strCampoUnion_t"] : null);
$strNombreExcell = (isset($_POST["NombreExcell"]) ? $_POST["NombreExcell"] : "");

if (isset($_POST["Exportar"])) {

    $arrDimensiones_t = dimensiones($tipoReport_t,$strFechaIn_t,$strFechaFn_t,$intIdPeriodo_t,0,$intIdHuesped_t,$intIdEstrat_t,$intIdCBX_t,$intIdBd_t,$intIdPaso_t,$intIdTipo_t,$intIdMuestra_t,$intIdGuion_t,0,0,"exportar",$arrDataFiltros_t);

    $resSQLReporte = $mysqli->query($arrDimensiones_t[3]." LIMIT 45000");
    
    $strNombreHoja_t = $arrDimensiones_t[4];

    $arrColumnas_t = $resSQLReporte->fetch_fields();

    $arrStyles = ['font'=>'Arial','font-size'=>10,'fill'=>'#20BEE8','color' => '#FFFF','halign'=>'center'];

    foreach ($arrColumnas_t as $name) {
        $arrHeader_t[$name->name] = "string"; 
    }

    $writer = new XLSXWriter();
    $writer->writeSheetHeader($strNombreHoja_t, $arrHeader_t, $arrStyles);

    while ($dato = $resSQLReporte->fetch_object()) {
        $writer->writeSheetRow($strNombreHoja_t,(array)$dato);
    }

    $strSQLNombreEstrat_t = "SELECT ESTRAT_Nombre____b AS nombre FROM ".$BaseDatos_systema.".ESTRAT WHERE ESTRAT_ConsInte__b = ".$intIdEstrat_t;
    $resSQLNombreEstrat_t = $mysqli->query($strSQLNombreEstrat_t);
    $strFechaActual_t = date("Y-m-d H:i:s");

    if ($resSQLNombreEstrat_t->num_rows > 0) {
        $objSQLNombreEstrat_t = $resSQLNombreEstrat_t->fetch_object();
        $strFileName_t = sanear_strings($strNombreHoja_t."_".$objSQLNombreEstrat_t->nombre).$strFechaActual_t;
    }else{
        $strFileName_t = "RESUMEN_CARGUE_DE_BASE_".$strFechaActual_t;
    }

    //JDBD generamos el escell
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$strFileName_t.'.xlsx"');
    header('Cache-Control: max-age=0');

    $writer->writeToStdOut();
    exit(0);


}else if (isset($_GET["Pivot"])) {

  $arrDimensiones_t = dimensiones($tipoReport_t,$strFechaIn_t,$strFechaFn_t,$intIdPeriodo_t,0,$intIdHuesped_t,$intIdEstrat_t,$intIdCBX_t,$intIdBd_t,$intIdPaso_t,$intIdTipo_t,$intIdMuestra_t,$intIdGuion_t,0,0,"pivot",$arrDataFiltros_t);

  $arrDatos_t = [];

  $resSQLReporte = $mysqli->query($arrDimensiones_t[3]." LIMIT 45000");

  if ($resSQLReporte) {
      if ($resSQLReporte->num_rows > 0) {
          while ($row = $resSQLReporte->fetch_assoc()) {
              $arrDatos_t[] = $row; 
          }
      }
  }

  echo json_encode($arrDatos_t);

}else if (isset($_GET["Paginado"])) {

    $intFilas_t = $_POST["intFilas_t"];
    $intLimit_t = 15;
    $intRegistrosTotal_t = $_POST["intRegistrosTotal_t"];
    $intCantidadPaginas_t = $_POST["intCantidadPaginas_t"];
    $intLimite_t = $_POST["intLimite_t"];

    $arrDimensiones_t = [$intRegistrosTotal_t,$intCantidadPaginas_t,$_POST["consulta"]." LIMIT ".$_POST["intFilas_t"].",".$intLimite_t,$_POST["consulta"]];

    $arrDimensiones_t[2] = str_replace("\\", "", $arrDimensiones_t[2]);
    $arrDimensiones_t[3] = str_replace("\\", "", $arrDimensiones_t[3]);

    $resSQLReporte_t = $mysqli->query($arrDimensiones_t[2]);

    $arrDatos_t = [];

    if ($resSQLReporte_t) {
        if ($resSQLReporte_t->num_rows > 0) {
            while ($row = $resSQLReporte_t->fetch_assoc()) {
                $arrDatos_t[] = $row; 
            }
        }
    }

    $strJsonDatos_t = json_encode($arrDatos_t);

    if (count($arrDatos_t) > 0) {

        $arrDatosHead_t = array_keys($arrDatos_t[0]);

        include "tables/grid.php";

    }else{
        echo 'No hay datos para mostrar!';
    }

}else if (isset($_GET["Reporte"])){
    
    $arrDimensiones_t = dimensiones($tipoReport_t,$strFechaIn_t,$strFechaFn_t,$intIdPeriodo_t,$strLimit_t,$intIdHuesped_t,$intIdEstrat_t,$intIdCBX_t,$intIdBd_t,$intIdPaso_t,$intIdTipo_t,$intIdMuestra_t,$intIdGuion_t,$intFilas_t,$intLimite_t,null,$arrDataFiltros_t);
    $arrDatos_t = [];

    $resSQLReporte = $mysqli->query($arrDimensiones_t[2]);

    if ($resSQLReporte) {
        if ($resSQLReporte->num_rows > 0) {
            while ($row = $resSQLReporte->fetch_assoc()) {
                $arrDatos_t[] = $row; 
            }
        }
    }

    $strJsonDatos_t = json_encode($arrDatos_t);

    if (count($arrDatos_t) > 0) {

        $arrDatosHead_t = array_keys($arrDatos_t[0]);

        include "tables/grid.php";
            


    }else{
        echo 'No hay datos para mostrar!';
    }

}else if (isset($_POST["ExportarCargue"])) {

    $arrDimensiones_t = dimensiones($tipoReport_t,$strFechaIn_t,$strFechaFn_t,$intIdPeriodo_t,0,$intIdHuesped_t,$intIdEstrat_t,$intIdCBX_t,$intIdBd_t,$intIdPaso_t,$intIdTipo_t,$intIdMuestra_t,$intIdGuion_t,0,0,"cargue",$arrDataFiltros_t);

    $strSQLReporte_t = $arrDimensiones_t[0];

    $strNombreHoja_t = $arrDimensiones_t[1];
    $arrDatos_t = [];
    $resSQLReporte = $mysqli->query($strSQLReporte_t);

    $arrColumnas_t = $resSQLReporte->fetch_fields();

    $arrStyles = ['font'=>'Arial','font-size'=>10,'fill'=>'#20BEE8','color' => '#FFFF','halign'=>'center'];

    foreach ($arrColumnas_t as $name) {
        $arrHeader_t[$name->name] = "string"; 
    }

    $writer = new XLSXWriter();
    $writer->writeSheetHeader($strNombreHoja_t, $arrHeader_t, $arrStyles);

    while ($dato = $resSQLReporte->fetch_object()) {
        $writer->writeSheetRow($strNombreHoja_t,(array)$dato);
    }

    $strSQLNombreEstrat_t = "SELECT ESTRAT_Nombre____b AS nombre FROM ".$BaseDatos_systema.".ESTRAT WHERE ESTRAT_ConsInte__b = ".$intIdEstrat_t;
    $resSQLNombreEstrat_t = $mysqli->query($strSQLNombreEstrat_t);
    $strFechaActual_t = date("Y-m-d H:i:s");

    if ($resSQLNombreEstrat_t->num_rows > 0) {
        $objSQLNombreEstrat_t = $resSQLNombreEstrat_t->fetch_object();
        $strFileName_t = sanear_strings($objSQLNombreEstrat_t->nombre).$strFechaActual_t;
    }else{
        $strFileName_t = "RESUMEN_CARGUE_DE_BASE_".$strFechaActual_t;
    }

    //JDBD generamos el escell
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$strNombreExcell.'_'.$strFileName_t.'.xlsx"');
    header('Cache-Control: max-age=0');

    $writer->writeToStdOut();
    exit(0);

}else{

    $arrDimensiones_t = dimensiones($tipoReport_t,$strFechaIn_t,$strFechaFn_t,$intIdPeriodo_t,0,$intIdHuesped_t,$intIdEstrat_t,$intIdCBX_t,$intIdBd_t,$intIdPaso_t,$intIdTipo_t,$intIdMuestra_t,$intIdGuion_t,0,0,"cargue",$arrDataFiltros_t);

    $strSQLReporte_t = $arrDimensiones_t[0];

    $strNombreHoja_t = $arrDimensiones_t[1];

    $arrDatos_t = [];
    $resSQLReporte = $mysqli->query($strSQLReporte_t);

    $arrDatos_t = [];

    if ($resSQLReporte) {
        if ($resSQLReporte->num_rows > 0) {
            while ($row = $resSQLReporte->fetch_assoc()) {
                $arrDatos_t[] = $row; 
            }
        }
    }

    $strJsonDatos_t = json_encode($arrDatos_t);

    if (count($arrDatos_t) > 0) {

        $arrDatosHead_t = array_keys($arrDatos_t[0]);

        if ($tipoReport_t == "cargue") {

            $resSQLErroresCargue_t = $mysqli->query("SELECT RESULTADO AS Errores, ".$strCampoUnion_t." AS CampoUnion FROM dy_cargue_datos.".$strTablaTemp_t." WHERE (ESTADO = 2 OR ESTADO = 3)");

            $arrDatosErrores_t = [];
            $arrHeadErrores_t = [];

            if ($resSQLErroresCargue_t && $resSQLErroresCargue_t->num_rows > 0) {

                while ($row2 = $resSQLErroresCargue_t->fetch_assoc()) {
                    $arrDatosErrores_t[] = $row2;
                }

                $arrHeadErrores_t = array_keys($arrDatosErrores_t[0]);
                

            }

            include "../../carga/tablaCargue.php";

        }else{

            include "tables/grid.php";
            
        }


    }else{
        echo 'No hay datos para mostrar!';
    }
}


/**
 *JDBD - En esta funcion retornaremos el nombre quitando todos los caracteres especiales y los acentos.
 *@param string.
 *@return string. 
 */

function sanear_strings($string)
{

    // $string = utf8_decode($string);

    $string = str_replace(array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'), array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'), $string);
    $string = str_replace(array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'), array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'), $string);
    $string = str_replace(array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'), array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'), $string);
    $string = str_replace(array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'), array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'), $string);
    $string = str_replace(array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'), array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'), $string);
    $string = str_replace(array('ñ', 'Ñ', 'ç', 'Ç'), array('n', 'N', 'c', 'C'), $string);
    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(array("\\", "¨", "º", "-", "~", "#", "@", "|", "!", "\"", "·", "$", "%", "&", "/", "(", ")", "?", "'", "¡", "¿", "[", "^", "`", "]", "+", "}", "{", "¨", "´", ">“, “< ", ";", ",", ":", "."), '', $string);
    return $string;

}



/**
 *JDBD - En esta funcion retornaremos el campo con el formato segun el tipode numero.
 *@param string.
 *@return string. 
 */

function depurarACD($strNombre_p){

    $strNombre_t = $strNombre_p;

    if ($strNombre_p == "Cont_porcentaje" || $strNombre_p == "TSF" || $strNombre_p == "TSF_Cont_antes_tsf" || $strNombre_p == "TSF_Cont_despues_tsf" || $strNombre_p == "Aban_porcentaje" || $strNombre_p == "Aban_umbral_tsf") {

        $strNombre_t = "ROUND(".$strNombre_p.",2) AS ".$strNombre_p;

    }else if ($strNombre_p == "AHT" || $strNombre_p == "THT" || $strNombre_p == "Aban_espera" || $strNombre_p == "Aban_espera_total" || $strNombre_p == "Aban_espera_min" || $strNombre_p == "Aban_espera_max") {

        $strNombre_t = "SUBSTR(".$strNombre_p.",1,8) AS ".$strNombre_p;

    }

    return $strNombre_t;

}

/**
 *JDBD - En esta funcion retornaremos el nombre de la vista segun su ID de base.
 *@param int.
 *@return string. 
 */

function nombreVistas($intIdBd_p){

    global $mysqli;
    global $BaseDatos;
    global $BaseDatos_systema;
    global $BaseDatos_telefonia;
    global $dyalogo_canales_electronicos;
    global $BaseDatos_general;


    //JDBD - Buscamos el nombre de la vista MYSQL.
    $strSQLVista_t = "SELECT nombre FROM ".$BaseDatos_general.".vistas_generadas WHERE id_guion = ".$intIdBd_p." ORDER BY id DESC LIMIT 1"; 

    $resSQLVista_t = $mysqli->query($strSQLVista_t);

    return $resSQLVista_t->fetch_object()->nombre;


}

/**
 *JDBD - Esta funcion calcula la cantidad de registros y paginas del reporte solicitado.
 *@param .....
 *@return array. 
 */

function filasPaginas($intIdBd_p,$strCondicion_p,$intLimite_p,$tipoReport_p="default",$intIdMuestra_p=null,$intIdGuion_p=null){

    global $BaseDatos;
    global $mysqli;

    //JDBD - Consultamos las cantidades de registros y las paginas que resultarian para mostrar como reporte.

    switch ($tipoReport_p) {

      case '2':

        $strSQL_t = $intIdBd_p;

        break;

      case '1':

        $strSQL_t = $intIdBd_p;

        break;

      case 'bdpaso':

        $strSQL_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos.".G".$intIdBd_p." JOIN ".$BaseDatos.".G".$intIdBd_p."_M".$intIdMuestra_p." ON  G".$intIdBd_p."_ConsInte__b = G".$intIdBd_p."_M".$intIdMuestra_p."_CoInMiPo__b WHERE ".$strCondicion_p;

        break;

      case 'gspaso':

        $strSQL_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos.".G".$intIdGuion_p." LEFT JOIN ".$BaseDatos.".G".$intIdBd_p." ON G".$intIdGuion_p."_CodigoMiembro = G".$intIdBd_p."_ConsInte__b WHERE ".$strCondicion_p;

        break;

      case 'acd':

        $strSQL_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos.".".$intIdBd_p." WHERE ".$strCondicion_p;

        break;
      
      default:
      
        $strSQL_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos.".G".$intIdBd_p." WHERE ".$strCondicion_p;
        break;
    }
    $intCantidadFilas_t = $mysqli->query($strSQL_t)->fetch_object()->cantidad;

    $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

    return [$intCantidadFilas_t,$intCantidadPaginas];

}

/**
 *JDBD - En esta funcion armamos las condiciones de los filtros que recibimos.
 *@param .....
 *@return string. 
 */

function aliasColumna($strNombre_p){

  $strNombreCorregido_p = preg_replace("/\s+/", "_", $strNombre_p);

  $strNombreCorregido_p = preg_replace('([^0-9a-zA-Z_])', "", $strNombreCorregido_p);

  $strNombreCorregido_p = strtoupper($strNombreCorregido_p);

  $strNombreCorregido_p = rtrim($strNombreCorregido_p,"_");

  $strNombreCorregido_p = substr($strNombreCorregido_p,0,40);

  if (is_numeric($strNombreCorregido_p)) {

    $strNombreCorregido_p = "CAMPO_".$strNombreCorregido_p;

  }

  return $strNombreCorregido_p;

}

/**
 *JDBD - En esta funcion armamos las condiciones de los filtros que recibimos
 *@param .....
 *@return array. 
 */

function columnasDinamicas($intIdBd_p,$strCampoEspecifico_p=null){

    global $mysqli;
    global $BaseDatos_systema;
    global $BaseDatos_general;

    $strSQLCamposDinamicos_t = "SELECT PREGUN_ConsInte__b AS ID, CONCAT('G',PREGUN_ConsInte__GUION__b,'_C',PREGUN_ConsInte__b) AS campoId , PREGUN_Texto_____b AS nombre, PREGUN_Tipo______b AS tipo, SECCIO_TipoSecc__b AS seccion FROM ".$BaseDatos_systema.".PREGUN LEFT JOIN ".$BaseDatos_systema.".SECCIO ON PREGUN_ConsInte__SECCIO_b = SECCIO_ConsInte__b WHERE PREGUN_ConsInte__GUION__b = ".$intIdBd_p." AND PREGUN_Tipo______b NOT IN (9,12,16) ORDER BY PREGUN_ConsInte__b ASC";

    $strCamposFinal_t = "";

    $resSQLCamposDinamicos_t = $mysqli->query($strSQLCamposDinamicos_t);

    $objCamposDinamicos_t = [];

    while ($obj = $resSQLCamposDinamicos_t->fetch_object()) {

      $objCamposDinamicos_t[] = $obj;

    }

    return $objCamposDinamicos_t;

}

/**
 *JDBD - En esta funcion armamos el campo que quedara como reporte.
 *@param int,int,string
 *@return string. 
 */

function campoEnReporte($intTipoCampo_p,$intIdCampo_p,$strNombreCampo_p){
    
    global $BaseDatos_general;

    switch ($intTipoCampo_p) {

      case '15':

        return "SUBSTRING_INDEX(".$intIdCampo_p.",'/',-1) AS ".aliasColumna($strNombreCampo_p).", ";

        break;

      case '13':

        return $BaseDatos_general.".fn_item_lisopc(".$intIdCampo_p.") AS ".aliasColumna($strNombreCampo_p).", ";

        break;

      case '10':

        return "DATE_FORMAT(".$intIdCampo_p.",'%H:%i:%s') AS ".aliasColumna($strNombreCampo_p).", ";

        break;

      case '6':

        return $BaseDatos_general.".fn_item_lisopc(".$intIdCampo_p.") AS ".aliasColumna($strNombreCampo_p).", ";

        break;
      
      default:

        return $intIdCampo_p." AS ".aliasColumna($strNombreCampo_p).", ";

        break;

    }


}

/**
 *JDBD - En esta funcion armamos el campo de tipo lista auxiliar.
 *@param int
 *@return array. 
 */

function campoListaAuxiliar($intIdCampo_p,$strNombre_p){


    global $mysqli;
    global $BaseDatos_systema;
    global $BaseDatos;
    global $BaseDatos_general;

    $arrData_t = [];

    $intIdCampo_t = explode("_C", $intIdCampo_p)[1];

    $strSQL_t = "SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(CAMPO__Nombre____b,'G',-1),'_',1)  AS id, CAMPO__Nombre____b AS nombre FROM ".$BaseDatos_systema.".PREGUI JOIN ".$BaseDatos_systema.".CAMPO_ ON PREGUI_ConsInte__CAMPO__b = CAMPO__ConsInte__b WHERE PREGUI_ConsInte__PREGUN_b = ".$intIdCampo_t." ORDER BY PREGUI_ConsInte__b ASC LIMIT 1";   

    if ($resSQL_t = $mysqli->query($strSQL_t)) {

        if ($resSQL_t->num_rows > 0) {

            $objSQL_t = $resSQL_t->fetch_object(); 

            $arrData_t[0] =  "L_".$intIdCampo_t.".".$objSQL_t->nombre." AS ".$strNombre_p.", ";
            $arrData_t[1] =  "LEFT JOIN ".$BaseDatos.".G".$objSQL_t->id." L_".$intIdCampo_t." ON ".$intIdCampo_p." = L_".$intIdCampo_t.".G".$objSQL_t->id."_ConsInte__b ";

        }


    }

    return $arrData_t;

}

/**
 *JDBD - En esta funcion armamos las condiciones de los filtros que recibimosm .....
 *@return array. 
 */

function columnasReporte($tipoReport_p,$intIdBd_p,$intIdMuestra_p,$intIdTipo_p=null,$intIdGuion_p=null){

    global $mysqli;
    global $BaseDatos_systema;
    global $BaseDatos_general;

    $strJOIN_t = "";

    switch ($tipoReport_p) {

      case 'bd':

        $arrColumnasDinamicas_t = columnasDinamicas($intIdBd_p);

        $strColumnasFinal_t = "G".$intIdBd_p."_ConsInte__b AS ID_DY, G".$intIdBd_p."_FechaInsercion AS FECHA_CREACION_DY, DATE_FORMAT(G".$intIdBd_p."_FechaInsercion,'%Y') AS ANHO_CREACION_DY, DATE_FORMAT(G".$intIdBd_p."_FechaInsercion,'%m') AS MES_CREACION_DY, DATE_FORMAT(G".$intIdBd_p."_FechaInsercion,'%d') AS DIA_CREACION_DY, DATE_FORMAT(G".$intIdBd_p."_FechaInsercion,'%H') AS HORA_CREACION_DY, ";

        $strColumnasDinamicas_t = "";

        foreach ($arrColumnasDinamicas_t as $value) {

          if ($value->tipo == "1" && $value->nombre == "ORIGEN_DY_WF") {

            $strColumnasFinal_t .= $value->campoId." AS ORIGEN_DY_WF, ";

            $strColumnasFinal_t .= "G".$intIdBd_p."_PasoGMI_b AS PASO_GMI_DY, ".$BaseDatos_general.".fn_item_lisopc(G".$intIdBd_p."_EstadoGMI_b) AS ESTADO_GMI_DY, ".$BaseDatos_general.".fn_item_monoef(G".$intIdBd_p."_GesMasImp_b) AS GESTION_MAS_IMPORTANTE_DY, ".$BaseDatos_general.".fn_clasificacion_traduccion(G".$intIdBd_p."_ClasificacionGMI_b) AS CLASIFICACION_GMI_DY, ".$BaseDatos_general.".fn_clasificacion_traduccion(G".$intIdBd_p."_TipoReintentoGMI_b) AS REINTENTO_GMI_DY, G".$intIdBd_p."_FecHorAgeGMI_b AS FECHA_AGENDA_GMI_DY, DATE_FORMAT(G".$intIdBd_p."_FecHorAgeGMI_b,'%Y') AS ANHO_AGENDA_GMI_DY, DATE_FORMAT(G".$intIdBd_p."_FecHorAgeGMI_b,'%m') AS MES_AGENDA_GMI_DY, DATE_FORMAT(G".$intIdBd_p."_FecHorAgeGMI_b,'%d') AS DIA_AGENDA_GMI_DY, DATE_FORMAT(G".$intIdBd_p."_FecHorAgeGMI_b,'%H') AS HORA_AGENDA_GMI_DY, G".$intIdBd_p."_ComentarioGMI_b AS COMENTARIO_GMI_DY, ".$BaseDatos_general.".fn_nombre_USUARI(G".$intIdBd_p."_UsuarioGMI_b) AS AGENTE_GMI_DY, G".$intIdBd_p."_CanalGMI_b AS CANAL_GMI_DY, G".$intIdBd_p."_SentidoGMI_b AS SENTIDO_GMI_DY, G".$intIdBd_p."_FeGeMaIm__b AS FECHA_GMI_DY, DATE_FORMAT(G".$intIdBd_p."_FeGeMaIm__b,'%Y') AS ANHO_GMI_DY, DATE_FORMAT(G".$intIdBd_p."_FeGeMaIm__b,'%m') AS MES_GMI_DY, DATE_FORMAT(G".$intIdBd_p."_FeGeMaIm__b,'%d') AS DIA_GMI_DY, DATE_FORMAT(G".$intIdBd_p."_FeGeMaIm__b,'%H') AS HORA_GMI_DY, (G".$intIdBd_p."_FeGeMaIm__b-G".$intIdBd_p."_FechaInsercion) AS DIAS_MADURACION_GMI_DY, ".$BaseDatos_general.".fn_nombre_paso(G".$intIdBd_p."_PasoUG_b) AS PASO_UG_DY, ".$BaseDatos_general.".fn_item_lisopc(G".$intIdBd_p."_EstadoUG_b) AS ESTADO_UG_DY, ".$BaseDatos_general.".fn_item_monoef(G".$intIdBd_p."_UltiGest__b) AS ULTIMA_GESTION_DY, ".$BaseDatos_general.".fn_clasificacion_traduccion(G".$intIdBd_p."_ClasificacionUG_b) AS CLASIFICACION_UG_DY, ".$BaseDatos_general.".fn_clasificacion_traduccion(G".$intIdBd_p."_TipoReintentoUG_b) AS REINTENTO_UG_DY, G".$intIdBd_p."_FecHorAgeUG_b AS FECHA_AGENDA_UG_DY, DATE_FORMAT(G".$intIdBd_p."_FecHorAgeUG_b,'%Y') AS ANHO_AGENDA_UG_DY, DATE_FORMAT(G".$intIdBd_p."_FecHorAgeUG_b,'%m') AS MES_AGENDA_UG_DY, DATE_FORMAT(G".$intIdBd_p."_FecHorAgeUG_b,'%d') AS DIA_AGENDA_UG_DY, DATE_FORMAT(G".$intIdBd_p."_FecHorAgeUG_b,'%H') AS HORA_AGENDA_UG_DY, G".$intIdBd_p."_ComentarioUG_b AS COMENTARIO_UG_DY, ".$BaseDatos_general.".fn_nombre_USUARI(G".$intIdBd_p."_UsuarioUG_b) AS AGENTE_UG_DY, G".$intIdBd_p."_Canal_____b AS CANAL_UG_DY, G".$intIdBd_p."_Sentido___b AS SENTIDO_UG_DY, G".$intIdBd_p."_FecUltGes_b AS FECHA_UG_DY, DATE_FORMAT(G".$intIdBd_p."_FecUltGes_b,'%Y') AS ANHO_UG_DY, DATE_FORMAT(G".$intIdBd_p."_FecUltGes_b,'%m') AS MES_UG_DY, DATE_FORMAT(G".$intIdBd_p."_FecUltGes_b,'%d') AS DIA_UG_DY, DATE_FORMAT(G".$intIdBd_p."_FecUltGes_b,'%H') AS HORA_UG_DY, G".$intIdBd_p."_LinkContenidoUG_b AS CONTENIDO_UG_DY, G".$intIdBd_p."_EstadoDiligenciamiento AS LLAVE_CARGUE_DY, (DAYOFMONTH((G".$intIdBd_p."_FechaInsercion - G".$intIdBd_p."_FecUltGes_b)) - DAYOFMONTH((G".$intIdBd_p."_FechaInsercion - G".$intIdBd_p."_FeGeMaIm__b))) AS DIAS_SIN_CONTACTO_DY, G".$intIdBd_p."_CantidadIntentos AS CANTIDAD_INTENTOS_DY, G".$intIdBd_p."_LinkContenidoGMI_b AS CONTENIDO_GMI_DY, ";

          }else{

            if ($value->tipo != "11") {

                $strColumnasDinamicas_t .= campoEnReporte($value->tipo,$value->campoId,aliasColumna($value->nombre));

            }else{

                $arrData_t = campoListaAuxiliar($value->campoId,aliasColumna($value->nombre));

                if (count($arrData_t) > 0) {

                    $strColumnasDinamicas_t .= $arrData_t[0];
                    $strJOIN_t .= $arrData_t[1];

                }


            }


          }

        }

        $strColumnasDinamicas_t = substr($strColumnasDinamicas_t, 0, -2);

        $strColumnasFinal_t .= $strColumnasDinamicas_t;

        return [$strColumnasFinal_t,$strJOIN_t];

        break;

      case 'bdpaso':

        $arrColumnasDinamicas_t = columnasDinamicas($intIdBd_p);

        $strColumnasFinal_t = "G".$intIdBd_p."_ConsInte__b AS ID_DY, G".$intIdBd_p."_FechaInsercion AS FECHA_CREACION_DY, DATE_FORMAT(G".$intIdBd_p."_FechaInsercion,'%Y') AS ANHO_CREACION_DY, DATE_FORMAT(G".$intIdBd_p."_FechaInsercion,'%m') AS MES_CREACION_DY, DATE_FORMAT(G".$intIdBd_p."_FechaInsercion,'%d') AS DIA_CREACION_DY, DATE_FORMAT(G".$intIdBd_p."_FechaInsercion,'%H') AS HORA_CREACION_DY, ";

        $strColumnasDinamicas_t = "";

        if ($intIdTipo_p == 7 || $intIdTipo_p == 8 || $intIdTipo_p == 10) {

            $strColumnasAdicionales_t = "(CASE G".$intIdBd_p."_M".$intIdMuestra_p."_Activo____b WHEN 0 THEN 'Inactivo' WHEN -1 THEN 'Activo' END) AS ACTIVO_DY, dyalogo_general.fn_tipo_reintento_traduccion(G".$intIdBd_p."_M".$intIdMuestra_p."_Estado____b) AS ESTADO_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_NumeInte__b AS NUMERO_INTENTOS_DY, ".$BaseDatos_general.".fn_item_monoef(G".$intIdBd_p."_M".$intIdMuestra_p."_UltiGest__b) AS ULTIMA_GESTION_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b AS FECHA_UG_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b,'%Y') AS ANHO_UG_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b,'%m') AS MES_UG_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b,'%d') AS DIA_UG_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b,'%H') AS HORA_UG_DY, dyalogo_general.fn_clasificacion_traduccion(G".$intIdBd_p."_M".$intIdMuestra_p."_ConUltGes_b) AS CLASIFICACION_UG_DY, dyalogo_general.fn_nombre_USUARI(G".$intIdBd_p."_M".$intIdMuestra_p."_UsuarioUG_b) AS USUARIO_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_CanalUG_b AS CANAL_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_SentidoUG_b AS SENTIDO_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_LinkContenidoUG_b AS LINK_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_Comentari_b AS COMENTARIO_DY, ";

        }else{

            $strColumnasAdicionales_t = "(CASE G".$intIdBd_p."_M".$intIdMuestra_p."_Activo____b WHEN 0 THEN 'Inactivo' WHEN -1 THEN 'Activo' END) AS ACTIVO_DY, dyalogo_general.fn_tipo_reintento_traduccion(G".$intIdBd_p."_M".$intIdMuestra_p."_Estado____b) AS ESTADO_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_NumeInte__b AS NUMERO_INTENTOS_DY, ".$BaseDatos_general.".fn_item_monoef(G".$intIdBd_p."_M".$intIdMuestra_p."_GesMasImp_b) AS GESTION_MAS_IMPORTANTE_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_FeGeMaIm__b AS FECHA_GMI_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FeGeMaIm__b,'%Y') AS ANHO_GMI_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FeGeMaIm__b,'%m') AS MES_GMI_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FeGeMaIm__b,'%d') AS DIA_GMI_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FeGeMaIm__b,'%H') AS HORA_GMI_DY, dyalogo_general.fn_clasificacion_traduccion(G".$intIdBd_p."_M".$intIdMuestra_p."_CoGesMaIm_b) AS CLASIFICACION_GMI_DY, dyalogo_general.fn_nombre_USUARI(G".$intIdBd_p."_M".$intIdMuestra_p."_UsuarioGMI_b) AS USUARIO_GMI_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_CanalGMI_b AS CANAL_GMI_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_SentidoGMI_b AS SENTIDO_GMI_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_LinkContenidoGMI_b AS LINK_GMI_DY, dyalogo_general.fn_tipo_reintento_traduccion(G".$intIdBd_p."_M".$intIdMuestra_p."_TipoReintentoGMI_b) AS TIPO_REINTENTO_GMI_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_CantidadIntentosGMI_b AS NUMERO_INTENTOS_GMI_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_ComentarioGMI_b AS COMENTARIO_GMI_DY, ".$BaseDatos_general.".fn_item_monoef(G".$intIdBd_p."_M".$intIdMuestra_p."_UltiGest__b) AS ULTIMA_GESTION_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b AS FECHA_UG_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b,'%Y') AS ANHO_UG_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b,'%m') AS MES_UG_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b,'%d') AS DIA_UG_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b,'%H') AS HORA_UG_DY, dyalogo_general.fn_clasificacion_traduccion(G".$intIdBd_p."_M".$intIdMuestra_p."_ConUltGes_b) AS CLASIFICACION_UG_DY, dyalogo_general.fn_nombre_USUARI(G".$intIdBd_p."_M".$intIdMuestra_p."_UsuarioUG_b) AS USUARIO_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_CanalUG_b AS CANAL_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_SentidoUG_b AS SENTIDO_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_LinkContenidoUG_b AS LINK_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_FecHorMinProGes__b AS FECHA_MINIMA_PROXIMO_INTENTO_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_Comentari_b AS COMENTARIO_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_FecHorAge_b AS FECHA_AGENDA_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FecHorAge_b,'%Y') AS ANHO_AGENDA_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FecHorAge_b,'%m') AS MES_AGENDA_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FecHorAge_b,'%d') AS DIA_AGENDA_DY, DATE_FORMAT(G".$intIdBd_p."_M".$intIdMuestra_p."_FecHorAge_b,'%H') AS HORA_AGENDA_DY, ";

        }

        foreach ($arrColumnasDinamicas_t as $value) {

          if ($value->tipo == "1" && $value->nombre == "ORIGEN_DY_WF") {

            $strColumnasFinal_t .= $value->campoId." AS ORIGEN_DY_WF, ";

          }else{

                if ($value->tipo != "11") {

                    $strColumnasDinamicas_t .= campoEnReporte($value->tipo,$value->campoId,aliasColumna($value->nombre));

                }else{

                    $arrData_t = campoListaAuxiliar($value->campoId,aliasColumna($value->nombre));

                    if (count($arrData_t) > 0) {
                        
                        $strColumnasDinamicas_t .= $arrData_t[0];
                        $strJOIN_t .= $arrData_t[1];

                    }


                }

          }

        }


        $strColumnasDinamicas_t = substr($strColumnasDinamicas_t, 0, -2);

        $strColumnasFinal_t .= $strColumnasAdicionales_t.$strColumnasDinamicas_t;

        return [$strColumnasFinal_t,$strJOIN_t];

        break;

      case 'bkpaso':

        $arrColumnasDinamicas_t = columnasDinamicas($intIdBd_p);

        $strColumnasFinal_t = "G".$intIdBd_p."_ConsInte__b AS ID_DY, G".$intIdBd_p."_FechaInsercion AS FECHA_CREACION_DY, DATE_FORMAT(G".$intIdBd_p."_FechaInsercion,'%Y') AS ANHO_CREACION_DY, DATE_FORMAT(G".$intIdBd_p."_FechaInsercion,'%m') AS MES_CREACION_DY, DATE_FORMAT(G".$intIdBd_p."_FechaInsercion,'%d') AS DIA_CREACION_DY, DATE_FORMAT(G".$intIdBd_p."_FechaInsercion,'%H') AS HORA_CREACION_DY, ";

        $strColumnasDinamicas_t = "";

        foreach ($arrColumnasDinamicas_t as $value) {

            if ($value->tipo != "11") {

                $strColumnasDinamicas_t .= campoEnReporte($value->tipo,$value->campoId,aliasColumna($value->nombre));

            }else{

                $arrData_t = campoListaAuxiliar($value->campoId,aliasColumna($value->nombre));

                if (count($arrData_t) > 0) {
                    
                    $strColumnasDinamicas_t .= $arrData_t[0];
                    $strJOIN_t .= $arrData_t[1];

                }


            }

        }

        $strColumnasDinamicas_t = substr($strColumnasDinamicas_t, 0, -2);

        $strColumnasFinal_t .= $strColumnasDinamicas_t;

        return [$strColumnasFinal_t,$strJOIN_t];

        break;

      case 'gspaso':

        $arrColumnasDinamicas_t = columnasDinamicas($intIdGuion_p);

        $strColumnasFinal_t = "G".$intIdGuion_p."_ConsInte__b AS ID_DY, G".$intIdBd_p."_ConsInte__b AS ID_BD_DY, G".$intIdBd_p."_FechaInsercion AS FECHA_CREACION_DY, ";

        $arrGestionControl_t = ["tipificacion"=>"","reintento"=>"","feAgenda"=>"","anAgenda"=>"","meAgenda"=>"","diAgenda"=>"","hoAgenda"=>"","observacion"=>"","fecha"=>"","anho"=>"","mes"=>"","dia"=>"","hora"=>""];

        $strColumnasDinamicas_t = "";

        foreach ($arrColumnasDinamicas_t as $value) {

            if ($value->seccion == "4" || $value->seccion == "3") {

              switch ($value->nombre) {

                case 'Tipificación':

                  $arrGestionControl_t["tipificacion"]=$BaseDatos_general.".fn_item_lisopc(".$value->campoId.") AS TIPIFICACIÓN_DY, ";

                  break;

                case 'Reintento':

                  $arrGestionControl_t["reintento"]=$BaseDatos_general.".fn_tipo_reintento_traduccion(".$value->campoId.") AS REINTENTO_DY, ";

                  break;

                case 'Hora Agenda':

                  $arrGestionControl_t["feAgenda"]=$value->campoId." AS FECHA_AGENDA_DY, ";
                  $arrGestionControl_t["anAgenda"]="DATE_FORMAT($value->campoId,'%Y') AS ANHO_AGENDA_DY, ";
                  $arrGestionControl_t["meAgenda"]="DATE_FORMAT($value->campoId,'%m') AS MES_AGENDA_DY, ";
                  $arrGestionControl_t["diAgenda"]="DATE_FORMAT($value->campoId,'%d') AS DIA_AGENDA_DY, ";
                  $arrGestionControl_t["hoAgenda"]="DATE_FORMAT($value->campoId,'%H') AS HORA_AGENDA_DY, ";

                  break;

                case 'Observacion':

                  $arrGestionControl_t["observacion"]=$value->campoId." AS OBSERVACION_DY, ";

                  break;

                case 'Fecha':

                  $arrGestionControl_t["fecha"]=$value->campoId." AS FECHA_GESTION_DY, ";
                  $arrGestionControl_t["anho"]="DATE_FORMAT(".$value->campoId.",'%Y') AS ANHO_GESTION_DY, ";
                  $arrGestionControl_t["mes"]="DATE_FORMAT(".$value->campoId.",'%m') AS MES_GESTION_DY, ";
                  $arrGestionControl_t["dia"]="DATE_FORMAT(".$value->campoId.",'%d') AS DIA_GESTION_DY, ";
                  $arrGestionControl_t["hora"]="DATE_FORMAT(".$value->campoId.",'%H') AS HORA_GESTION_DY, ";

                  break;

              }

            }else{

                if ($value->tipo != "11") {

                    $strColumnasDinamicas_t .= campoEnReporte($value->tipo,$value->campoId,aliasColumna($value->nombre));

                }else{

                    $arrData_t = campoListaAuxiliar($value->campoId,aliasColumna($value->nombre));

                    if (count($arrData_t) > 0) {
                        
                        $strColumnasDinamicas_t .= $arrData_t[0];
                        $strJOIN_t .= $arrData_t[1];

                    }


                }

            }



        }

        $strColumnasFinal_t .= $arrGestionControl_t["fecha"].$arrGestionControl_t["anho"].$arrGestionControl_t["mes"].$arrGestionControl_t["dia"].$arrGestionControl_t["hora"]."DATE_FORMAT(G".$intIdGuion_p."_Duracion___b,'%H:%i:%S') AS DURACION_GESTION, TIME_TO_SEC(G".$intIdGuion_p."_Duracion___b) AS DURACION_GESTION_SEG, ".$BaseDatos_general.".fn_nombre_USUARI(G".$intIdGuion_p."_Usuario) AS AGENTE_DY, G".$intIdGuion_p."_Sentido___b AS SENTIDO, G".$intIdGuion_p."_Canal_____b AS CANAL, ".$arrGestionControl_t["tipificacion"].$BaseDatos_general.".fn_clasificacion_traduccion(G".$intIdGuion_p."_Clasificacion) AS CLASIFICACION_DY, ".$arrGestionControl_t["reintento"].$arrGestionControl_t["feAgenda"].$arrGestionControl_t["anAgenda"].$arrGestionControl_t["meAgenda"].$arrGestionControl_t["diAgenda"].$arrGestionControl_t["hoAgenda"].$arrGestionControl_t["observacion"]."G".$intIdGuion_p."_LinkContenido AS DESCARGAGRABACION_DY, G".$intIdGuion_p."_Paso AS PASO_DY, G".$intIdGuion_p."_Origen_b AS ORIGEN_DY, ";

        $strColumnasDinamicas_t = substr($strColumnasDinamicas_t, 0, -2);

        $strColumnasFinal_t .= $strColumnasDinamicas_t;

        return [$strColumnasFinal_t,$strJOIN_t];

        break;

    }


}

/**
 *JDBD - En esta funcion se armara las condiciones dinamicamente.
 *@param .....
 *@return string. 
 */

function condicionesDinamicas($strJsonCondicion_p){

  $arrJsonCondicion_t = json_decode($strJsonCondicion_p);

  $strCondicionFinal_t = "";

  foreach ($arrJsonCondicion_t as $value) {

    $strCondicionFinal_t .= "(".$value.") AND ";

  }

  $strCondicionFinal_t = substr($strCondicionFinal_t, 0, -4);

  return $strCondicionFinal_t;

}

/**
 *JDBD - En esta funcion conseguiremos toda la informacion que se necesita para el reporte y su paginacion como lo es, la cantidad de registros, campos de la consulta ect.
 *@param .....
 *@return array. 
 */



function dimensiones($tipoReport_p,$strFechaIn_p,$strFechaFn_p,$intIdPeriodo_p,$strLimit_p,$intIdHuesped_p,$intIdEstrat_p,$intIdCBX_p,$intIdBd_p,$intIdPaso_p,$intIdTipo_p,$intIdMuestra_p,$intIdGuion_p,$intFilas_p,$intLimite_p,$strSolicitud_p=null,$arrDataFiltros_p){

    global $mysqli;
    global $BaseDatos;
    global $BaseDatos_systema;
    global $BaseDatos_telefonia;
    global $dyalogo_canales_electronicos;
    global $BaseDatos_general;
    global $arrCamposMuestra_t;
    global $arrCamposMuestra2_t;

    switch ($tipoReport_p) {

        case 'cargue':

            $strNombreHoja_t = "RESUMEN CARGUE";

            $strSQLVista_t = "SELECT nombre FROM ".$BaseDatos_general.".vistas_generadas WHERE id_guion = ".$intIdGuion_p." ORDER BY id DESC LIMIT 1"; 


            $resSQLVista_t = $mysqli->query($strSQLVista_t);
            $objSQLVista_t = $resSQLVista_t->fetch_object();

            if ($intIdMuestra_p == -1) {

                $strSQLReporte_t = "SELECT * FROM ".$BaseDatos.".".$objSQLVista_t->nombre." WHERE LLAVE_CARGUE = ".$strFechaIn_p." ORDER BY ID DESC LIMIT 500";

            }else{
                
                $strSQLReporte_t = "SELECT ".$BaseDatos_general.".fn_tipo_reintento_traduccion(B.G".$intIdGuion_p."_M".$intIdMuestra_p."_Estado____b) AS ESTADO, ".$BaseDatos_general.".fn_nombre_USUARI(B.G".$intIdGuion_p."_M".$intIdMuestra_p."_ConIntUsu_b) AS AGENTE_ASIGNADO, A.* FROM ".$BaseDatos.".".$objSQLVista_t->nombre." A JOIN ".$BaseDatos.".G".$intIdGuion_p."_M".$intIdMuestra_p." B ON A.ID = B.G".$intIdGuion_p."_M".$intIdMuestra_p."_CoInMiPo__b WHERE A.LLAVE_CARGUE = ".$strFechaIn_p." ORDER BY A.ID DESC LIMIT 500";


            }

            $arrReporte_t[0] = $strSQLReporte_t;
            $arrReporte_t[1] = $strNombreHoja_t;

            return $arrReporte_t;

            break;

        case 'acd':

            //JDBD - traemos las vitas ACD de la campaña.

            $strCondicion_t = "DATE(Fecha) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }

            
            $strIntervalo_t = "%ACD_DIA%";
            $strCampoIntervalo_t = "";
            $strCampoTransfer_t = ",transfer";

            if ($intIdPeriodo_p == "2") {

                    $strCampoIntervalo_t = ", Intervalo";
                    $strIntervalo_t = "%ACD_HORA%";
                    $strCampoTransfer_t = "";

            }

            $strSQLACD_t = "SELECT nombre FROM ".$BaseDatos_general.".vistas_generadas JOIN ".$BaseDatos_systema.".CAMPAN ON id_campan = CAMPAN_ConsInte__b WHERE CAMPAN_IdCamCbx__b = ".$intIdCBX_p." AND nombre LIKE '".$strIntervalo_t."'";

            $resSQLACD_t = $mysqli->query($strSQLACD_t);


            $strSQLReporte_t = "SELECT 'NO HAY REPORTE ACD DISPONIBLE' AS ERROR";
            $strSQLReporteNoLimit_t = "SELECT 'NO HAY REPORTE ACD DISPONIBLE' AS ERROR";

            if ($resSQLACD_t->num_rows > 0) {

                $strNombreACD_t = $resSQLACD_t->fetch_object()->nombre;

                $strSQLCampos_t = "DESC ".$BaseDatos.".".$strNombreACD_t;

                $resSQLCampos_t = $mysqli->query($strSQLCampos_t);

                $strColumnas_t = "";

                while ($obj = $resSQLCampos_t->fetch_object()) {

                    $strColumnas_t .= depurarACD($obj->Field).", ";

                }

                $strColumnas_t = substr($strColumnas_t, 0, -2);

                $arrFilasPaginas_t = filasPaginas($strNombreACD_t,$strCondicion_t,$intLimite_p,'acd');

                $strSQLReporte_t = "SELECT ".$strColumnas_t." FROM ".$BaseDatos.".".$strNombreACD_t." WHERE ".$strCondicion_t." ORDER BY Fecha DESC LIMIT 0,".$intLimite_p;

                $strSQLReporteNoLimit_t = "SELECT ".$strColumnas_t." FROM ".$BaseDatos.".".$strNombreACD_t." WHERE ".$strCondicion_t." ORDER BY Fecha DESC ";

            }

            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"ACD"];

            break;

        case 'bkpaso':

            $strCondicion_t = "DATE(G".$intIdBd_p."_FechaInsercion) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }

            $arrFilasPaginas_t = filasPaginas($intIdBd_p,$strCondicion_t,$intLimite_p,'bkpaso');

            $arrData_t = columnasReporte("bkpaso",$intIdBd_p,$intIdMuestra_p);

            $strColumnasDinamicas_t = $arrData_t[0];

            $strSQLReporte_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdBd_p." ".$arrData_t[1]." WHERE ".$strCondicion_t." ORDER BY G".$intIdBd_p."_ConsInte__b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdBd_p." ".$arrData_t[1]." WHERE ".$strCondicion_t." ORDER BY G".$intIdBd_p."_ConsInte__b DESC ";

            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"BACKOFFICE"];

            break;

        case 'bdpaso':

            $strCondicion_t = "DATE(G".$intIdBd_p."_FechaInsercion) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($intIdTipo_p == 7 || $intIdTipo_p == 8 || $intIdTipo_p == 10) {

                $strCondicion_t = "DATE(G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            }

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }

            $arrFilasPaginas_t = filasPaginas($intIdBd_p,$strCondicion_t,$intLimite_p,'bdpaso',$intIdMuestra_p);

            $arrData_t = columnasReporte("bdpaso",$intIdBd_p,$intIdMuestra_p,$intIdTipo_p);

            $strColumnasDinamicas_t = $arrData_t[0];

            $strSQLReporte_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdBd_p." JOIN ".$BaseDatos.".G".$intIdBd_p."_M".$intIdMuestra_p." ON G".$intIdBd_p."_ConsInte__b = G".$intIdBd_p."_M".$intIdMuestra_p."_CoInMiPo__b ".$arrData_t[1]." WHERE ".$strCondicion_t." ORDER BY G".$intIdBd_p."_ConsInte__b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdBd_p." JOIN ".$BaseDatos.".G".$intIdBd_p."_M".$intIdMuestra_p." ON G".$intIdBd_p."_ConsInte__b = G".$intIdBd_p."_M".$intIdMuestra_p."_CoInMiPo__b ".$arrData_t[1]." WHERE ".$strCondicion_t." ORDER BY G".$intIdBd_p."_ConsInte__b DESC ";

            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"RESUMEN BASE"];

            break;

        case 'gspaso':

            $strCondicion_t = "DATE(G".$intIdGuion_p."_FechaInsercion) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }

            $arrFilasPaginas_t = filasPaginas($intIdBd_p,"(G".$intIdGuion_p."_Paso = ".$intIdPaso_p." OR G".$intIdGuion_p."_Paso = '' OR G".$intIdGuion_p."_Paso IS NULL) AND ".$strCondicion_t,$intLimite_p,'gspaso',null,$intIdGuion_p);

            $arrData_t = columnasReporte("gspaso",$intIdBd_p,$intIdMuestra_p,null,$intIdGuion_p);

            $strColumnasDinamicas_t = $arrData_t[0];

            $strSQLReporte_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdGuion_p." LEFT JOIN ".$BaseDatos.".G".$intIdBd_p." ON G".$intIdGuion_p."_CodigoMiembro = G".$intIdBd_p."_ConsInte__b ".$arrData_t[1]." WHERE (G".$intIdGuion_p."_Paso = ".$intIdPaso_p." OR G".$intIdGuion_p."_Paso = '' OR G".$intIdGuion_p."_Paso IS NULL) AND ".$strCondicion_t." ORDER BY G".$intIdGuion_p."_ConsInte__b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdGuion_p." LEFT JOIN ".$BaseDatos.".G".$intIdBd_p." ON G".$intIdGuion_p."_CodigoMiembro = G".$intIdBd_p."_ConsInte__b ".$arrData_t[1]." WHERE (G".$intIdGuion_p."_Paso = ".$intIdPaso_p." OR G".$intIdGuion_p."_Paso = '' OR G".$intIdGuion_p."_Paso IS NULL) AND ".$strCondicion_t." ORDER BY G".$intIdGuion_p."_ConsInte__b DESC ";

            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"GESTIONES"];

            break;

        case '1':

            $strCondicion_t = "DATE(fecha_hora_inicio) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }

            $strSQLReporte_t = "SELECT agente_nombre AS Agente, DATE(fecha_hora_inicio) AS Inicio, DATE_FORMAT(fecha_hora_inicio,'%H')  AS Inicio_hora, DATE_FORMAT(fecha_hora_inicio,'%i')  AS Inicio_minuto, DATE(fecha_hora_fin) AS Fin, DATE_FORMAT(fecha_hora_fin,'%H')  AS Fin_hora, DATE_FORMAT(fecha_hora_fin,'%i')  AS Fin_minuto, DATE_FORMAT(SEC_TO_TIME(duracion),'%H:%i:%s') as Duracion_Horas FROM ".$BaseDatos_telefonia.".dy_v_historico_sesiones_por_campana WHERE campana_id IN (SELECT CAMPAN_IdCamCbx__b FROM ".$BaseDatos_systema.".ESTPAS JOIN ".$BaseDatos_systema.".CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b WHERE ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_p.") AND ".$strCondicion_t." ORDER BY fecha_hora_inicio DESC LIMIT 0, ".$intLimite_p;


            $strSQLReporteNoLimit_t = "SELECT agente_nombre AS Agente, DATE(fecha_hora_inicio) AS Inicio, DATE_FORMAT(fecha_hora_inicio,'%H')  AS Inicio_hora, DATE_FORMAT(fecha_hora_inicio,'%i')  AS Inicio_minuto, DATE(fecha_hora_fin) AS Fin, DATE_FORMAT(fecha_hora_fin,'%H')  AS Fin_hora, DATE_FORMAT(fecha_hora_fin,'%i')  AS Fin_minuto, DATE_FORMAT(SEC_TO_TIME(duracion),'%H:%i:%s') as Duracion_Horas FROM ".$BaseDatos_telefonia.".dy_v_historico_sesiones_por_campana WHERE campana_id IN (SELECT CAMPAN_IdCamCbx__b FROM ".$BaseDatos_systema.".ESTPAS JOIN ".$BaseDatos_systema.".CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b WHERE ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_p.") AND ".$strCondicion_t." ORDER BY fecha_hora_inicio DESC ";

            $strSQLReportePaginas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_telefonia.".dy_v_historico_sesiones_por_campana WHERE campana_id IN (SELECT CAMPAN_IdCamCbx__b FROM ".$BaseDatos_systema.".ESTPAS JOIN ".$BaseDatos_systema.".CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b WHERE ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_p.") AND ".$strCondicion_t." ORDER BY fecha_hora_inicio DESC ";

            $arrFilasPaginas_t = filasPaginas($strSQLReportePaginas_t,$strCondicion_t,$intLimite_p,'1');
            
            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"SESIONES"];

            break;

        case 'pausas':

            $strCondicion_t = "DATE(fecha_hora_inicio) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }

            $strSQLFechas_t = "SELECT DATE(fecha_hora_inicio) AS fecha FROM ".$BaseDatos_telefonia.".dy_v_historico_descansos_por_campana JOIN ".$BaseDatos_systema.".ESTPAS ON campana_id = ESTPAS_ConsInte__CAMPAN_b WHERE ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_p." AND ".$strCondicion_t." GROUP BY DATE(fecha_hora_inicio) ORDER BY DATE(fecha_hora_inicio) DESC";


            $resSQLFechas_t = $mysqli->query($strSQLFechas_t);


            $strSQLReporte_t = "SELECT 'NO HAY REPORTE DISPONIBLE' AS ERROR";

            $strSQLReporte_t = "SELECT agente_nombre AS AGENTE, tipo_descanso_nombre AS PAUSA";

            while ($obj = $resSQLFechas_t->fetch_object()) {

                $strSQLReporte_t .= ", CAST(SEC_TO_TIME(SUM((CASE WHEN DATE(fecha_hora_inicio) = '".$obj->fecha."' THEN duracion ELSE 0 END))) AS TIME(0)) AS '".$obj->fecha."'";

            }

            $strSQLReporte_t .= ", CAST(SEC_TO_TIME(SUM(duracion)) AS TIME(0)) AS TOTAL FROM ".$BaseDatos_telefonia.".dy_v_historico_descansos_por_campana JOIN ".$BaseDatos_systema.".ESTPAS ON campana_id = ESTPAS_ConsInte__CAMPAN_b WHERE ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_p." AND ".$strCondicion_t." GROUP BY agente_nombre, tipo_descanso_nombre";


            return [0,0,$strSQLReporte_t,$strSQLReporte_t,"PAUSAS - DURACION POR AGENTE"];

            break;

        case '2':

            $strCondicion_t = "DATE(fecha_hora_inicio) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            } 

            $strSQLReporte_t = "SELECT agente_nombre AS Agente, DATE(fecha_hora_inicio) AS Inicio, DATE_FORMAT(fecha_hora_inicio,'%H') AS Inicio_hora, DATE_FORMAT(fecha_hora_inicio,'%i') AS Inicio_minuto, DATE(fecha_hora_fin) AS Fin, DATE_FORMAT(fecha_hora_fin,'%H') AS Fin_hora, DATE_FORMAT(fecha_hora_fin,'%i') AS Fin_minuto, DATE_FORMAT(SEC_TO_TIME(duracion),'%H:%i:%s') as Duracion_Horas, tipo_descanso_nombre AS TipoPausa, comentario FROM ".$BaseDatos_telefonia.".dy_v_historico_descansos_por_campana WHERE campana_id in (SELECT CAMPAN_IdCamCbx__b FROM ".$BaseDatos_systema.".ESTPAS JOIN ".$BaseDatos_systema.".CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b WHERE ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_p.") AND ".$strCondicion_t." ORDER BY fecha_hora_inicio DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "SELECT agente_nombre AS Agente, DATE(fecha_hora_inicio) AS Inicio, DATE_FORMAT(fecha_hora_inicio,'%H') AS Inicio_hora, DATE_FORMAT(fecha_hora_inicio,'%i') AS Inicio_minuto, DATE(fecha_hora_fin) AS Fin, DATE_FORMAT(fecha_hora_fin,'%H') AS Fin_hora, DATE_FORMAT(fecha_hora_fin,'%i') AS Fin_minuto, DATE_FORMAT(SEC_TO_TIME(duracion),'%H:%i:%s') as Duracion_Horas, tipo_descanso_nombre AS TipoPausa, comentario FROM ".$BaseDatos_telefonia.".dy_v_historico_descansos_por_campana WHERE campana_id in (SELECT CAMPAN_IdCamCbx__b FROM ".$BaseDatos_systema.".ESTPAS JOIN ".$BaseDatos_systema.".CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b WHERE ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_p.") AND ".$strCondicion_t." ORDER BY fecha_hora_inicio DESC ";

            $strSQLReportePaginas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_telefonia.".dy_v_historico_descansos_por_campana WHERE campana_id in (SELECT CAMPAN_IdCamCbx__b FROM ".$BaseDatos_systema.".ESTPAS JOIN ".$BaseDatos_systema.".CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b WHERE ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_p.") AND ".$strCondicion_t." ORDER BY fecha_hora_inicio DESC ";

            $arrFilasPaginas_t = filasPaginas($strSQLReportePaginas_t,$strCondicion_t,$intLimite_p,'2');

            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"PAUSAS"];

            break;

        case '3':

            //JDBD - Consultamos la cantidad de registros a retornar.
            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes LEFT JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE (NOT (HoraInicialDefinida is null)) AND HoraInicialDefinida < date_format(current_time, '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." AND qrySesionesDelDia.agente_id IS NULL Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, HoraInicialDefinida FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes LEFT JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE (NOT (HoraInicialDefinida is null)) AND HoraInicialDefinida < date_format(current_time, '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." AND qrySesionesDelDia.agente_id IS NULL Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, HoraInicialDefinida FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes LEFT JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE (NOT (HoraInicialDefinida is null)) AND HoraInicialDefinida < date_format(current_time, '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." AND qrySesionesDelDia.agente_id IS NULL Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"NO REGISTRARON"];

            break;
        case '4':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraInicio > HoraInicialDefinida AND USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(HoraInicio, HoraInicialDefinida), '%H:%i:%S') as Retraso, HoraInicialDefinida, HoraInicio as HoraInicialReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraInicio > HoraInicialDefinida AND USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(HoraInicio, HoraInicialDefinida), '%H:%i:%S') as Retraso, HoraInicialDefinida, HoraInicio as HoraInicialReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraInicio > HoraInicialDefinida AND USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"LLEGARON TARDE"];

            break;
        case '5':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraInicio <= HoraInicialDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, HoraInicialDefinida, HoraInicio as HoraInicialReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraInicio <= HoraInicialDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, HoraInicialDefinida, HoraInicio as HoraInicialReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraInicio <= HoraInicialDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"LLEGARON A TIEMPO"];

            break;
        case '6':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraFin < HoraFinalDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(HoraFinalDefinida, HoraFin), '%H:%i:%S') as TiempoFaltante, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraFin < HoraFinalDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(HoraFinalDefinida, HoraFin), '%H:%i:%S') as TiempoFaltante, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraFin < HoraFinalDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"SE FUERON ANTES"];

            break;
        case '7':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraFin >= HoraFinalDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraFin >= HoraFinalDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraFin >= HoraFinalDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"SE FUERON A TIEMPO"];

            break;
        case '8':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  timediff(HoraFinalDefinida, HoraInicialDefinida) > date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p."  Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(timediff(HoraFinalDefinida, HoraInicialDefinida), timediff(HoraFin, HoraInicio)), '%H:%i:%S') as TiempoFaltante, date_format(timediff(HoraFinalDefinida, HoraInicialDefinida), '%H:%i:%S') as DuracionDefinida, date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') as DuracionReal, HoraInicialDefinida, HoraInicio as HoraInicialReal, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  timediff(HoraFinalDefinida, HoraInicialDefinida) > date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p."  Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(timediff(HoraFinalDefinida, HoraInicialDefinida), timediff(HoraFin, HoraInicio)), '%H:%i:%S') as TiempoFaltante, date_format(timediff(HoraFinalDefinida, HoraInicialDefinida), '%H:%i:%S') as DuracionDefinida, date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') as DuracionReal, HoraInicialDefinida, HoraInicio as HoraInicialReal, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  timediff(HoraFinalDefinida, HoraInicialDefinida) > date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p."  Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"SESIONES CORTAS"];

            break;
        case '9':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  timediff(HoraFinalDefinida, HoraInicialDefinida) <= date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(HoraFinalDefinida, HoraInicialDefinida), '%H:%i:%S') as DuracionDefinida, date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') as DuracionReal, HoraInicialDefinida, HoraInicio as HoraInicialReal, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  timediff(HoraFinalDefinida, HoraInicialDefinida) <= date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(HoraFinalDefinida, HoraInicialDefinida), '%H:%i:%S') as DuracionDefinida, date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') as DuracionReal, HoraInicialDefinida, HoraInicio as HoraInicialReal, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  timediff(HoraFinalDefinida, HoraInicialDefinida) <= date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"SESIONES DURACION OK"];

            break;
        case '10':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and timediff(HoraFinalProgramada, HoraInicialProgramada) < timediff(fecha_hora_fin,fecha_hora_inicio) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, timediff(timediff(fecha_hora_fin,fecha_hora_inicio), timediff(HoraFinalProgramada, HoraInicialProgramada)) as Exceso, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and timediff(HoraFinalProgramada, HoraInicialProgramada) < timediff(fecha_hora_fin,fecha_hora_inicio) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, timediff(timediff(fecha_hora_fin,fecha_hora_inicio), timediff(HoraFinalProgramada, HoraInicialProgramada)) as Exceso, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and timediff(HoraFinalProgramada, HoraInicialProgramada) < timediff(fecha_hora_fin,fecha_hora_inicio) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasConHorarioMuyLargas"];

            break;
        case '11':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and timediff(HoraFinalProgramada, HoraInicialProgramada) >= timediff(fecha_hora_fin,fecha_hora_inicio) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, timediff(timediff(HoraFinalProgramada, HoraInicialProgramada), timediff(fecha_hora_fin,fecha_hora_inicio)) as TiempoAFavor, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and timediff(HoraFinalProgramada, HoraInicialProgramada) >= timediff(fecha_hora_fin,fecha_hora_inicio) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, timediff(timediff(HoraFinalProgramada, HoraInicialProgramada), timediff(fecha_hora_fin,fecha_hora_inicio)) as TiempoAFavor, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and timediff(HoraFinalProgramada, HoraInicialProgramada) >= timediff(fecha_hora_fin,fecha_hora_inicio) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasConHorarioDuracionOk"];

            break;
        case '12':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and (HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S') or date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, IF(HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S'), timediff(HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S')),null) as SalioAntesPor, IF(date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada, timediff(date_format(fecha_hora_fin , '%H:%i:%S'), HoraFinalProgramada),null) as LlegoTardePor, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal, timediff(timediff(fecha_hora_fin,fecha_hora_inicio), timediff(HoraFinalProgramada, HoraInicialProgramada)) as TiempoDiferencia, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and (HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S') or date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, IF(HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S'), timediff(HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S')),null) as SalioAntesPor, IF(date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada, timediff(date_format(fecha_hora_fin , '%H:%i:%S'), HoraFinalProgramada),null) as LlegoTardePor, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal, timediff(timediff(fecha_hora_fin,fecha_hora_inicio), timediff(HoraFinalProgramada, HoraInicialProgramada)) as TiempoDiferencia, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and (HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S') or date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasConHorarioIncumplidas"];

            break;
        case '13':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and (HoraInicialProgramada <= date_format(fecha_hora_inicio, '%H:%i:%S') and date_format(fecha_hora_fin , '%H:%i:%S') <= HoraFinalProgramada) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, IF(HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S'),timediff(HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S')),null) as SalioAntesPor, IF(date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada, timediff(date_format(fecha_hora_fin, '%H:%i:%S'), HoraFinalProgramada),null) as LlegoTardePor, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal, timediff(timediff(fecha_hora_fin,fecha_hora_inicio), timediff(HoraFinalProgramada, HoraInicialProgramada)) as TiempoDiferencia, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and (HoraInicialProgramada <= date_format(fecha_hora_inicio, '%H:%i:%S') and date_format(fecha_hora_fin , '%H:%i:%S') <= HoraFinalProgramada) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, IF(HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S'),timediff(HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S')),null) as SalioAntesPor, IF(date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada, timediff(date_format(fecha_hora_fin, '%H:%i:%S'), HoraFinalProgramada),null) as LlegoTardePor, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal, timediff(timediff(fecha_hora_fin,fecha_hora_inicio), timediff(HoraFinalProgramada, HoraInicialProgramada)) as TiempoDiferencia, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and (HoraInicialProgramada <= date_format(fecha_hora_inicio, '%H:%i:%S') and date_format(fecha_hora_fin , '%H:%i:%S') <= HoraFinalProgramada) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasConHorarioCumplidas"];

            break;
        case '14':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and USUPAU_Tipo_b = 0 group by USUARI_ConsInte__b having time_to_sec(DuracionMaxima) < sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, timediff(sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))), DuracionMaxima) as Exceso, DuracionMaxima, sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))) as DuracionReal, count(USUARI_ConsInte__b) as CantidadReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and USUPAU_Tipo_b = 0 group by USUARI_ConsInte__b having time_to_sec(DuracionMaxima) < sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, timediff(sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))), DuracionMaxima) as Exceso, DuracionMaxima, sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))) as DuracionReal, count(USUARI_ConsInte__b) as CantidadReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and USUPAU_Tipo_b = 0 group by USUARI_ConsInte__b having time_to_sec(DuracionMaxima) < sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasSinHorarioMuyLargas"];

            break;
        case '15':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 0 and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." group by USUARI_ConsInte__b having CantidadMaxima < count(USUARI_ConsInte__b) Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, count(USUARI_ConsInte__b) - CantidadMaxima as VecesDeMas, count(USUARI_ConsInte__b) as CantidadReal, CantidadMaxima, sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))) as DuracionReal, DuracionMaxima FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 0 and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." group by USUARI_ConsInte__b having CantidadMaxima < count(USUARI_ConsInte__b) Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, count(USUARI_ConsInte__b) - CantidadMaxima as VecesDeMas, count(USUARI_ConsInte__b) as CantidadReal, CantidadMaxima, sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))) as DuracionReal, DuracionMaxima FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 0 and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." group by USUARI_ConsInte__b having CantidadMaxima < count(USUARI_ConsInte__b) Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasSinHorarioMuchasVeces"];

            break;
        case '16':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and USUPAU_Tipo_b = 0 group by USUARI_ConsInte__b having time_to_sec(DuracionMaxima) >= sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio))) and CantidadMaxima >= count(USUARI_ConsInte__b) Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))) as DuracionReal, DuracionMaxima, count(USUARI_ConsInte__b) as CantidadReal, CantidadMaxima FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and USUPAU_Tipo_b = 0 group by USUARI_ConsInte__b having time_to_sec(DuracionMaxima) >= sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio))) and CantidadMaxima >= count(USUARI_ConsInte__b) Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))) as DuracionReal, DuracionMaxima, count(USUARI_ConsInte__b) as CantidadReal, CantidadMaxima FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and USUPAU_Tipo_b = 0 group by USUARI_ConsInte__b having time_to_sec(DuracionMaxima) >= sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio))) and CantidadMaxima >= count(USUARI_ConsInte__b) Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasSinHorarioOK"];

            break;
        case '17':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes WHERE USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and HoraInicialDefinida IS NULL Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes WHERE USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and HoraInicialDefinida IS NULL Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes WHERE USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and HoraInicialDefinida IS NULL Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"AGENTES SIN MALLA DEFINIDA"];

            break;

        default:  

            $strCondicion_t = "DATE(G".$intIdBd_p."_FechaInsercion) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }
            $arrFilasPaginas_t = filasPaginas($intIdBd_p,$strCondicion_t,$intLimite_p);

            $arrData_t = columnasReporte("bd",$intIdBd_p,$intIdMuestra_p);

            $strColumnasDinamicas_t = $arrData_t[0];

            $strSQLReporte_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdBd_p." ".$arrData_t[1]." WHERE ".$strCondicion_t." ORDER BY G".$intIdBd_p."_ConsInte__b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdBd_p." ".$arrData_t[1]." WHERE ".$strCondicion_t." ORDER BY G".$intIdBd_p."_ConsInte__b DESC ";

            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"RESUMEN BASE GENERAL"];

            break;


    }

}

/**
 *JDBD - En esta funcion armamos las condiciones de los filtros que recibimos.
 *@param .....
 *@return string. 
 */

function armarCondicion($strCampo_p,$strOperador_p,$strValor_p,$strTipo_p,$strCondicion_p){

    $strCampo_p = str_replace("\\", "", $strCampo_p);


    switch ($strTipo_p) {
        case '5':

            return $strCondicion_p." DATE(".$strCampo_p.") ".$strOperador_p." '".$strValor_p."' ";

            break;
                    
        default:

            $strOperadorFinal_t  = "LIKE";

            switch ($strOperador_p) {

                case 'LIKE_1':

                    $strValorFinal_t = "'".$strValor_p."%'";

                    break;
                case 'LIKE_2':

                    $strValorFinal_t = "'%".$strValor_p."%'";

                    break;

                case 'LIKE_3':

                    $strValorFinal_t = "'%".$strValor_p."'";

                    break;
                default:

                    $strValorFinal_t = "'".$strValor_p."'";
                    $strOperadorFinal_t = $strOperador_p;

                    break;            

                }

            return $strCondicion_p." ".$strCampo_p." ".$strOperadorFinal_t." ".$strValorFinal_t." ";

            break;
    }

}