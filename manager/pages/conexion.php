<?php
    include(__DIR__."/../configuracion.php");
    require_once(__DIR__."/../../helpers/ClearData.php");
    $mysqli = new  mysqli($DB_Name, $DB_User, $DB_Pass);

    /* comprobar la conexiÃ³n */
    if ($mysqli->connect_errno) {
        printf("FallÃ³ la conexiÃ³n: %s\n", $mysqli->connect_error);
        exit();
    }

    if (!$mysqli->set_charset("utf8")) {
        printf("Error cargando el conjunto de caracteres utf8: %s\n", $mysqli->error);
        exit();
    } else {

    }
 
    $limpiar = new ClearData($mysqli);

    if($_POST){
        $_POST = $limpiar->sanearInput($_POST);         
    }
    if($_GET){
        $_GET = $limpiar->sanearInput($_GET);
    }
?>
