<?php

/**
 * JDBD - Se obtienen la lista de agentes en linea.
 * Este metodo llama al web service : 
 * dy_servicios_adicionales/svrs/actividadActual/lista que sirve para realizar las vistas de metas.
 * @param Integer id del Usuario que esta log e la pagina.
 * @return array - con toda la informacion de cada paso y sus metas.
 */
function listaAgentesTiempoReal($intIdUsuario_p){
    include(__DIR__."/../configuracion.php");
    $data =[
        "strUsuario_t"   => 'crm',
        "strToken_t"     => 'D43dasd321',
        "intIdHuesped_t" => null,
        "intUsuariId_t"  => (int)$intIdUsuario_p
    ];

    return consumirWSJSON($API_WS_TIEMPO_REAL."/dy_servicios_adicionales/svrs/actividadActual/lista",$data );
}
    /**
     * JDBD - Se obtienen la cantidad de estados por tipo en lista agentes tiempó real.
     * Este metodo llama al web service : 
     * dy_servicios_adicionales/svrs/actividadActual/agrupacionEstados que sirve para realizar las vistas de metas.
     * @param Integer id del Usuario que esta log e la pagina.
     * @return array - con toda la informacion de cada paso y sus metas.
     */
    function agrupacionEstadosTiempoReal($intIdUsuario_p){
        include(__DIR__."/../configuracion.php");
        $data =[
            "strUsuario_t"   =>  'crm',
            "strToken_t"     =>  'D43dasd321',
            "intIdHuesped_t" =>  null,
            "intUsuariId_t"  => (int)$intIdUsuario_p
        ];

        return consumirWSJSON($API_WS_TIEMPO_REAL."/dy_servicios_adicionales/svrs/actividadActual/agrupacionEstados",$data );
    }


    /**
     * Refresca el cache del distribuidor
     * DALB20200901 - Creacion de la funcion que limpia el cache 
     */
    function refrescarCacheDistribuidor(){
        include(__DIR__."/../configuracion.php");
        global $IP_SERVICIO_DISTRIBUCION;

        $data =[
            "strUsuario_t"   =>  'local',
            "strToken_t"     =>  'local'
        ];

        return consumirWSJSON("http://".$IP_SERVICIO_DISTRIBUCION.":8080/dy_distribuidor_trabajo/api/cache/refresca",$data );
    }

/**
 * JDBD - Se obtienen las metricas de cada paso que tiene la estrategia.
 * Este metodo llama al web service : 
 * dy_servicios_adicionales/svrs/actividadActual/infoEstrategiaMetas que sirve para realizar las vistas de metas.
 * @param Integer id del Usuario que esta log e la pagina.
 * @return array - con toda la informacion de cada paso y sus metas.
 */
function metricasTiempoReal($intIdUsuario_p){
    include(__DIR__."/../configuracion.php");
    $data =[
        "strUsuario_t" => "crm",
        "strToken_t" => "D43dasd321",
        "intIdUsuario_t" => (int)$intIdUsuario_p,
        "intIdEstrat_t" => NULL,
        "intIdEstpas_t" => NULL
    ];

    return consumirWSJSON($API_WS_TIEMPO_REAL."/dy_servicios_adicionales/svrs/actividadActual/infoEstrategiaMetas",$data );
}

/**
 *JDBD - Esta funcion crea las vistas para los reportes automaticos en la BD
 *por medio de la api "generateByTennant" con el id del huesped.
 *@param int - Id del huesped actual.
 *@return json - respuesda del api si fue exitoso o fallo. 
 */
function generarVistasPorHuesped($intIdGuion_p = null,$intIdHuesped_p = null){
   include(__DIR__."/../configuracion.php");
   $data = [
        "strUsuario_t"          =>  "crm",
        "strToken_t"            =>  "D43dasd321",
        "intIdGeneralTennant_t" =>  $intIdHuesped_p,
        "intIdEstrategia_t"     =>  null,
        "intIdGuion_t"           => $intIdGuion_p
   ];
                   
   return consumirWSJSON($Api_Gestion."dyalogocore/api/bi/generator/views/generateByTennant",$data );
}

/**
 *NBG - Esta funcion llama al api para regenerar contraseñas
 *por medio de la api "recordarclave" con el correo del usuario.
 *@param str - correo del agente.
 *@return json - respuesda del api si fue exitoso o fallo. 
 */

function generarpassword($strCorreo_t){
    include(__DIR__."/../configuracion.php");
    $data = [
        "strCorreo_t" => $strCorreo_t,
        "strUsuario_t" => 'adminApi',
        "strToken_t" => 'PGbtywunzaCwCLGSo7zj9CGLV9QxiVgJ'
    ];
    
    return consumirWSJSON("127.0.0.1/admin/public/api/usuarios/recordarclave",$data );
}

/**
 *NBG - Esta funcion llama al api para capturar la foto actual del agente
 *por medio de la api "tomarFoto" con el token del usuario.
 *@param str - token del agente.
 *@return json - respuesda del api si fue exitoso o fallo. 
 */
function capturarFoto($token){
    include(__DIR__."/../configuracion.php");
    $data = [
        "strToken_t" => $token
    ];
    
    return consumirWSJSON("http://127.0.0.1:8080/dyalogocbx/api/controlAgente/tomarFoto",$data );
}

/**
 * DLAB - 20190815 - Creacion metodo
 * Este etodo recibe los parametros del consumo de un web service y lo ejecuta
 * @param String strURL_p String con la informacion de la URL
 * @param Array arrayDatos_p Arreglo con los datos del consumo
 * @return String retorna la informacion y respuesta del consumo
 */
function consumirWSJSON($strURL_p, $arrayDatos_p){

    //Codificamos el arreglo en formato JSON
    $strDatosJSON_t = json_encode($arrayDatos_p);
    
    //Inicializamos la conexion CURL al web service local para ser consumido
    $objCURL_t = curl_init($strURL_p);
    
    //Asignamos todos los parametros del consumo
    curl_setopt($objCURL_t, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
    curl_setopt($objCURL_t, CURLOPT_POSTFIELDS, $strDatosJSON_t); 
    curl_setopt($objCURL_t,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($objCURL_t, CURLOPT_HTTPHEADER, array(
        'Accept: application/json',
        'Content-Type: application/json',
        'Content-Length: ' . strlen($strDatosJSON_t))                                                                      
    );

    //Obtenemos la respuesta
    $objRespuestaCURL_t = curl_exec($objCURL_t);

    //Obtenemos el error 
    $objRespuestaError_t = curl_error($objCURL_t);

    //Cerramos la conexion
    curl_close ($objCURL_t);

    //Validamos la respuesta y generamos el rerno
    if (isset($objRespuestaCURL_t)) {
        //Decodificamos la respuesta en JSON y la retornamos
        return $objRespuestaCURL_t;
    }else {
        return $objRespuestaError_t;
    }
}
?>