<?php
	function generar_Busqueda_Manual($id_busqueda){

		global $mysqli;
		global $BaseDatos;
		global $BaseDatos_systema;
		global $BaseDatos_telefonia;
		global $dyalogo_canales_electronicos;
		global $BaseDatos_general;
		
		if(!is_null($id_busqueda)){
			/* si no viene vacio el id procedemos */
			$str_guion   = 'G'.$id_busqueda;
			$str_guion_c = $str_guion."_C";

			/* buscamos los campos que se deben colocar para el buscador los que tengan PREGUN_IndiBusc__b != 0 */
			
			$str_Lsql = "SELECT PREGUN_Texto_____b as titulo_pregunta , PREGUN_Tipo______b as tipo_Pregunta, PREGUN_ConsInte__b as id , PREGUN_ConsInte__GUION__PRE_B as guion, PREGUN_ConsInte__OPCION_B as lista, PREGUN_OrdePreg__b , PREGUN_NumeMini__b as minimoNumero, PREGUN_NumeMaxi__b as maximoNumero, PREGUN_FechMini__b as fechaMinimo, PREGUN_FechMaxi__b as fechaMaximo , PREGUN_HoraMini__b as horaMini , PREGUN_HoraMaxi__b as horaMaximo, PREGUN_TexErrVal_b as error FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$id_busqueda." AND PREGUN_IndiBusc__b != 0 ORDER BY PREGUN_OrdePreg__b";

			$strLsqlCaminc = "SELECT PREGUN_OrdePreg__b, PREGUN_ConsInte__b as id, C.CAMINC_ConsInte__GUION__Gui_b as script, C.CAMINC_ConsInte__CAMPO_Gui_b as colScript FROM ".$BaseDatos_systema.".PREGUN JOIN DYALOGOCRM_SISTEMA.CAMINC as C ON C.CAMINC_ConsInte__CAMPO_Pob_b = PREGUN_ConsInte__b WHERE PREGUN_ConsInte__GUION__b = ".$id_busqueda." AND PREGUN_IndiBusc__b != 0 GROUP BY CAMINC_ConsInte__CAMPO_Gui_b ORDER BY PREGUN_OrdePreg__b;";
            
            /*NBG*2020-05* Buscamos el tipo de busqueda a generar*/
            $intTipoBusqueda_t=1;
            $strSQLTipoBusqueda="SELECT GUION__TipoBusqu__Manual_b FROM {$BaseDatos_systema}.GUION_ WHERE GUION__ConsInte__b={$id_busqueda}";
            $strSQLTipoBusqueda=$mysqli->query($strSQLTipoBusqueda);
            if($strSQLTipoBusqueda->num_rows>0){
                $strSQLTipoBusqueda=$strSQLTipoBusqueda->fetch_object();
                $intTipoBusqueda_t=$strSQLTipoBusqueda->GUION__TipoBusqu__Manual_b;
            }            
			/* La carpeta donde van a quedar alojados estos archivos  */
			
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			    $carpeta = "C:/xampp/htdocs/crm_php/formularios/".$str_guion;
			} else {
			    $carpeta = "/var/www/html/crm_php/formularios/".$str_guion;
			}
			
		    if (!file_exists($carpeta)) {
		        mkdir($carpeta, 0777);
		    }

		    /* abrimos el archivo que vamos a crear */
		    $fp = fopen($carpeta."/".$str_guion."_Busqueda_Manual.php" , "w");
		    //chmod($carpeta."/".$str_guion."_Busqueda_Manual.php" , 0777); 

		    /* iniciamos la codificacion de la pagina */
		    
		    $campo ='
<?php
	$http = "http://".$_SERVER["HTTP_HOST"];
	if (isset($_SERVER[\'HTTPS\'])) {
	    $http = "https://".$_SERVER["HTTP_HOST"];
	}
	if(isset($_GET["accion_manual"])){
		$idMonoef=-19;
		$texto = "Busqueda No Aplica";
	}else{
		$idMonoef =-18;
		$texto = "No Suministra Información";
	}
?>

<script type="text/javascript">
	function autofitIframe(id){
		if (!window.opera && document.all && document.getElementById){
			id.style.height=id.contentWindow.document.body.scrollHeight;
		} else if(document.getElementById) {
			id.style.height=id.contentDocument.body.scrollHeight+"px";
		}
	}
</script>

<link rel="stylesheet" href="assets/plugins/WinPicker/dist/wickedpicker.min.css">
<script type="text/javascript" src="assets/plugins/WinPicker/dist/wickedpicker.min.js"></script>

<div class="row" id="buscador">
	<div class="col-md-1">&nbsp;</div>
	<div class="col-md-10">
		<div class="row">
			<form id="formId">';

			$str_tablaCampos = '';
			$str_tablaCuerpo = '';
			$where = '';
			$arrCamposBusMan_t = [];
            $strTimepicker='';
            $strDatepicker='';
            $strValidaCorreo_t='';
			/* escribimos la variable que creamos arriba campo */
			
		    fputs($fp , $campo);
			fputs($fp , chr(13).chr(10)); // Genera saldo de linea 

		    $campos = $mysqli->query($str_Lsql);
		    $relacionesCaminc = $mysqli->query($strLsqlCaminc);
		    $i9 = 0;
		    $final = $campos->num_rows;
		    while ($objC = $relacionesCaminc->fetch_object()) {
		    	//JDBD - Aqui llenamos el array con  id de los campos elegidos para la busqueda manual.
	      		$arrCamposBusMan_t[]=[
	      			"script" => $objC->script,
	      			"colScript" => $objC->colScript,
	      			"camp" => $str_guion_c.$objC->id
	      		];
		    }
	      	while($obj = $campos->fetch_object()){
	      		/* esta parte es la tabla que se muestra cuando hay mas de un dato */
	      		
	      		$str_tablaCampos .= '<th>'.($obj->titulo_pregunta).'</th>';
	      		$str_tablaCuerpo .= '<td>\'+ item.'.$str_guion_c.$obj->id.' +\'</td>';

	      		/* aqui ya entramos a la busqueda en el Sql de PHP */
	      		
	      		$where .= '
			if(isset($_POST[\''.$str_guion_c.$obj->id.'\']) && !is_null($_POST[\''.$str_guion_c.$obj->id.'\']) && $_POST[\''.$str_guion_c.$obj->id.'\'] != \'\'){
				$and = null;
				if($usados == 1){
					$and = " AND ";
				}
				$Where .= $and." '.$str_guion_c.$obj->id.' LIKE \'%". $_POST[\''.$str_guion_c.$obj->id.'\'] ."%\' ";
				$usados = 1;
			}';
	       		switch ($obj->tipo_Pregunta) {
	       			case '1':
						$campo = ' 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="'.$str_guion_c.$obj->id.'" id="Lbl'.$str_guion_c.$obj->id.'">'.($obj->titulo_pregunta).'</label>
					    <input type="text" class="form-control input-sm" id="'.$str_guion_c.$obj->id.'" name="'.$str_guion_c.$obj->id.'"  placeholder="'.($obj->titulo_pregunta).'">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->';
                   
							fputs($fp , $campo);
							fputs($fp , chr(13).chr(10)); // Genera saldo de linea 
	       				break;

	       			case '2':
						$campo = ' 	
				<!-- CAMPO TIPO MEMO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="'.$str_guion_c.$obj->id.'" id="Lbl'.$str_guion_c.$obj->id.'">'.($obj->titulo_pregunta).'</label>
					    <textarea class="form-control input-sm" name="'.$str_guion_c.$obj->id.'" id="'.$str_guion_c.$obj->id.'"   placeholder="'.($obj->titulo_pregunta).'"></textarea>
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO MEMO -->';
		                    fputs($fp , $campo);
		  					fputs($fp , chr(13).chr(10)); // Genera saldo de linea 
	       				break;

	       			case '3':
						$campo = ' 
				<!-- CAMPO TIPO ENTERO -->
				<!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="'.$str_guion_c.$obj->id.'" id="Lbl'.$str_guion_c.$obj->id.'">'.($obj->titulo_pregunta).'</label>
					    <input type="text" class="form-control input-sm Numerico"  name="'.$str_guion_c.$obj->id.'" id="'.$str_guion_c.$obj->id.'" placeholder="'.($obj->titulo_pregunta).'">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO ENTERO -->';
	                     
		                    fputs($fp , $campo);
		  					fputs($fp , chr(13).chr(10)); // Genera saldo de linea 
	       				break;

	   				case '4':
						$campo = ' 	
				<!-- CAMPO TIPO DECIMAL -->
				<!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="'.$str_guion_c.$obj->id.'" id="Lbl'.$str_guion_c.$obj->id.'">'.($obj->titulo_pregunta).'</label>
					    <input type="text" class="form-control input-sm Decimal"  name="'.$str_guion_c.$obj->id.'" id="'.$str_guion_c.$obj->id.'" placeholder="'.($obj->titulo_pregunta).'">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO DECIMAL -->';
		                    
							fputs($fp , $campo);
							fputs($fp , chr(13).chr(10)); // Genera saldo de linea 
	       				break;

	       			case '5':
						$campo = ' 	
				<!-- CAMPO TIPO FECHA -->
				<!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="'.$str_guion_c.$obj->id.'" id="Lbl'.$str_guion_c.$obj->id.'">'.($obj->titulo_pregunta).'</label>
					    <input type="text" class="form-control input-sm Fecha"  name="'.$str_guion_c.$obj->id.'" id="'.$str_guion_c.$obj->id.'" placeholder="YYYY-MM-DD">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO FECHA-->';
                        
                //Armamos los Datepicker        
                $strDatepicker .='
                $("#'.$str_guion_c.$obj->id.'").datepicker({
                    language: "es",
                    autoclose: true,
                    todayHighlight: true,
                    format:"yyyy-mm-dd"
                });
                ';                        
		                   
		                    fputs($fp , $campo);
		  					fputs($fp , chr(13).chr(10)); // Genera saldo de linea 
	       				break;
	       			
	       			case '10':
						$campo = ' 	
				<!-- CAMPO TIMEPICKER -->
				<!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
				<div class="col-xs-4">
					<div class="bootstrap-timepicker">
					    <div class="form-group">
					        <label for="'.$str_guion_c.$obj->id.'" id="Lbl'.$str_guion_c.$obj->id.'">'.($obj->titulo_pregunta).'</label>
					        <div class="input-group">
					            <input type="text" class="form-control input-sm Hora"  name="'.$str_guion_c.$obj->id.'" id="'.$str_guion_c.$obj->id.'" placeholder="HH:MM:SS" >
					            <div class="input-group-addon">
					                <i class="fa fa-clock-o"></i>
					            </div>
					        </div>
					        <!-- /.input group -->
					    </div>
					    <!-- /.form group -->
					</div>
				</div>
				<!-- FIN DEL CAMPO TIMEPICKER -->';
                
                //Armamos los Timepicker
                        
                $strTimepicker .='
                var options = { //hh:mm 24 hour format only, defaults to current time
                    twentyFour: true, //Display 24 hour format, defaults to false
                    title: \''.$obj->titulo_pregunta.'\', //The Wickedpicker\'s title,
                    showSeconds: true, //Whether or not to show seconds,
                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                    show: null, //A function to be called when the Wickedpicker is shown
                    clearable: false, //Make the picker\'s input clearable (has clickable "x")
                }; 
                $("#'.$str_guion_c.$obj->id.'").wickedpicker(options);
                $("#'.$str_guion_c.$obj->id.'").val("");
                ';                        

		                    fputs($fp , $campo);
		  					fputs($fp , chr(13).chr(10)); // Genera saldo de linea 
	       				break;

	   				case '6':

						$campo = '
				<!-- CAMPO DE TIPO LISTA -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="'.$str_guion_c.$obj->id.'" id="Lbl'.$str_guion_c.$obj->id.'">'.($obj->titulo_pregunta).'</label>
					    <select class="form-control input-sm"  name="'.$str_guion_c.$obj->id.'" id="'.$str_guion_c.$obj->id.'">
					        <option value="0">Seleccione</option>
					        <?php
					            /*
					                SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
					            */
					            $str_Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = '.$obj->lista.'";

					            $obj = $mysqli->query($str_Lsql);
					            while($obje = $obj->fetch_object()){
					                echo "<option value=\'".$obje->OPCION_ConsInte__b."\'>".($obje->OPCION_Nombre____b)."</option>";

					            }    
					            
					        ?>
					    </select>
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO LISTA -->';
	                    
	              			fputs($fp , $campo);
	    					fputs($fp , chr(13).chr(10)); // Genera saldo de linea 
	       				break;

	       			case '11':

		                //Primero necesitamos obener el campo que vamos a usar
		                $campoGuion = $obj->id;
		                $guionSelect2 = $obj->guion;
		                //Luego buscamos los campos en la tabla Pregui
		                $CampoSql = "SELECT * FROM ".$BaseDatos_systema.".PREGUI WHERE PREGUI_ConsInte__PREGUN_b = ".$campoGuion;
		                //recorremos esos campos para ir a buscarlos en la tabla campo_
		                $CampoSqlR = $mysqli->query($CampoSql);
		                $camposconsultaGuion = ' G'.$obj->guion.'_ConsInte__b as id ';


		                $camposAmostrar = '';
		                $valordelArray = 0;
		                $nombresDeCampos = '';
		                $camposAcolocarDinamicamente = '0';

		                while($objet = $CampoSqlR->fetch_object()){
		                    //aqui obtenemos los campos que se colocara el valor dinamicamente al seleccionar una opcion del guion, ejemplo ciudad - departamento- pais..
		                    if($objet->PREGUI_Consinte__CAMPO__GUI_B != 0){
		                        //Consulto por el id del campo que necesitamos y se repite las veces que sean necesarias
		                        $campoamostrarSql = 'SELECT * FROM '.$BaseDatos_systema.'.CAMPO_ WHERE CAMPO__ConsInte__b = '.$objet->PREGUI_Consinte__CAMPO__GUI_B;
		                        $campoamostrarSqlE = $mysqli->query($campoamostrarSql);
		                        while($campoNombres = $campoamostrarSqlE->fetch_object()){
		                            $camposAcolocarDinamicamente .= '|'.$campoNombres->CAMPO__Nombre____b;
		                        }
		                    }

		                    //Consulto por el id del campo que necesitamos y se repite las veces que sean necesarias
		                    $campoObtenidoSql = 'SELECT * FROM '.$BaseDatos_systema.'.CAMPO_ WHERE CAMPO__ConsInte__b = '.$objet->PREGUI_ConsInte__CAMPO__b;
		                    //echo $campoObtenidoSql;
		                    $resultCamposObtenidos = $mysqli->query($campoObtenidoSql);
		                    while ($objCampo = $resultCamposObtenidos->fetch_object()) {

		                        //Busco el nombre del campo para el nombre de las columnas
		                        $selectGuion = "SELECT PREGUN_Texto_____b as titulo_pregunta FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b =".$objCampo->CAMPO__ConsInte__PREGUN_b;
		                        $selectGuionE = $mysqli->query($selectGuion);
		                        while($o = $selectGuionE->fetch_object()){
		                            if($valordelArray == 0){
		                                $nombresDeCampos .= ($o->titulo_pregunta);
		                            }else{
		                                $nombresDeCampos .= ' | '.($o->titulo_pregunta).'';
		                            }
		                        }
		                        //añadimos los campos a la consulta que se necesita para cargar el guion
		                        $camposconsultaGuion .=', '.$objCampo->CAMPO__Nombre____b;
		                        if($valordelArray == 0){
		                            $camposAmostrar .= '".($obj->'.$objCampo->CAMPO__Nombre____b.')."';
		                        }else{
		                            $camposAmostrar .= ' | ".($obj->'.$objCampo->CAMPO__Nombre____b.')."';
		                        }
		                        
		                        

		                        $valordelArray++;
		                    }
		                }

						$campo = ' 
				<?php	
				$str_Lsql = "SELECT '.$camposconsultaGuion.' FROM ".$BaseDatos.".G'.$obj->guion.'";
				?>
				<!-- CAMPO DE TIPO LISTA -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="'.$str_guion_c.$obj->id.'" id="Lbl'.$str_guion_c.$obj->id.'">'.($obj->titulo_pregunta).'</label>
					    <select class="form-control input-sm"  name="'.$str_guion_c.$obj->id.'" id="'.$str_guion_c.$obj->id.'">
					        <option>'.$nombresDeCampos.'</option>
					        <?php
					            /*
					                SE RECORRE LA CONSULTA QUE TRAE LOS CAMPOS DEL GUIÓN
					            */
					            $combo = $mysqli->query($str_Lsql);
					            while($obj = $combo->fetch_object()){
					                echo "<option value=\'".$obj->id."\' dinammicos=\''.$camposAcolocarDinamicamente.'\'>'.$camposAmostrar.'</option>";

					            }    
					            
					        ?>
					    </select>
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO LISTA -->';

	             
		                    fputs($fp , $campo);
		                    fputs($fp , chr(13).chr(10)); // Genera saldo de linea */
	       				break;

	       			case '8':
						$campo = ' 	
				<!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="'.$str_guion_c.$obj->id.'" id="Lbl'.$str_guion_c.$obj->id.'">'.($obj->titulo_pregunta).'</label>
					    <div class="checkbox">
					        <label>
					            <input type="checkbox" value="1" name="'.$str_guion_c.$obj->id.'" id="'.$str_guion_c.$obj->id.'" data-error="Before you wreck yourself"  > 
					        </label>
					    </div>
					</div>
				</div>
				<!-- FIN DEL CAMPO SI/NO -->';
	                   
							fputs($fp , $campo);
							fputs($fp , chr(13).chr(10)); // Genera saldo de linea 
	       				break;
	       			
	       			

	       			case '9':
						$campo = ' 	
				<!-- lIBRETO O LABEL -->
				<div class="col-xs-4">
					<h3>'.($obj->titulo_pregunta).'</h3>
				</div>
				<!-- FIN LIBRETO -->';
	                    fputs($fp , $campo);
	  					fputs($fp , chr(13).chr(10)); // Genera saldo de linea 

	       				break;
                        
             		case '14':
						$campo = ' 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="'.$str_guion_c.$obj->id.'" id="Lbl'.$str_guion_c.$obj->id.'">'.($obj->titulo_pregunta).'</label>
					    <input type="email" class="form-control input-sm" id="'.$str_guion_c.$obj->id.'" name="'.$str_guion_c.$obj->id.'"  placeholder="'.($obj->titulo_pregunta).'">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->';
                        
                //Armamos la validación de tipo email
                $strValidaCorreo_t .="
                if($('#".$str_guion_c.$obj->id."').val() != ''){                
                    var escorreo=/^[^@\s]+@[^@\.\s]+(\.[^@\.\s]+)+$/;         
                    if (!(escorreo.test($('#".$str_guion_c.$obj->id."').val()))) { 
                        alertify.error('Digite un correo valido');
                        $('#".$str_guion_c.$obj->id."').focus();
                        valido=1;
                    }
                }
                    ";
                   
							fputs($fp , $campo);
							fputs($fp , chr(13).chr(10)); // Genera saldo de linea 
	       				break;                      
	       			default:
	       				
	       				break;
	       		}
	       	}
   	$strCamps_t = '';
   	$strSendCamps_t = '';
   	foreach ($arrCamposBusMan_t as $camp) {
		$strCamps_t .= 'var G'.$camp["script"].'_C'.$camp["colScript"].' = $("#'.$camp["camp"].'").val();'."\n\t\t\t";
   		$strSendCamps_t .= '&G'.$camp["script"].'_C'.$camp["colScript"].'=\'+G'.$camp["script"].'_C'.$camp["colScript"].'+\'';
   	}

   	$strCamps_t = substr($strCamps_t, 0, -4);
   	$strSendCamps_t = substr($strSendCamps_t, 0, -2);

   	if ($strSendCamps_t == '') {
	   	$strCamps_t = 'var SinEnlace = "No";';
	   	$strSendCamps_t = '&SinEnlace=\'+SinEnlace';
	}

	$campo = ' 	
			</form>
		</div>
	</div>
	<div class="col-md-1">&nbsp;</div>
</div>
<div class="row" id="botones">
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-danger" id="btnCancelar" type="button"><?php echo $texto; ?></button>
	</div>
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-success" id="btnBuscar" type="button"><i class="fa fa-search"></i>';
    switch($intTipoBusqueda_t){
        case '1':
            $campo .='Buscar en toda la base de datos';
            break;
        
        case '2':
            $campo .='Buscar sobre los registros incluidos en la campaña';
            break;
        
        case '3':
            $campo .='Buscar en los registros incluidos en la campaña y asignados a mi';
            break;    
    }        
    $campo .='</button>
	</div>
</div>
<br/>
<div class="row" id="resulados">
	<div class="col-md-12 col-xs-12" id="resultadosBusqueda">

	</div>
</div>
<div class="row">
	<div class="col-md-12" id="gestiones">
		<iframe id="frameContenedor" src="" style="width: 100%; height: auto;"  marginheight="0" marginwidth="0" noresize  frameborder="0" onload="autofitIframe(this);">
              
        </iframe>
	</div>
</div>
<input type="hidden" id="id_gestion_cbx" value="<?php echo $_GET["id_gestion_cbx"];?>">
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
guardarStorage(\'<?php echo $_GET["id_gestion_cbx"]?>\');
<?php 
    $id_campan=\'\';
    if(isset($_GET[\'id_campana_crm\']) && $_GET[\'id_campana_crm\'] != \'0\'){
        $id_campan=\'&id_campana_crm=\'.$_GET[\'id_campana_crm\'];
    } 
?>

    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent(\'on\' + eventName, eventHandler);
        }
    }


    // Listen to message from child window
    bindEvent(window, \'message\', function (e) {
        if(Array.isArray(e.data)){
            console.log(e.data);
            var keys=Object.keys(e.data[0].camposivr);
            var key=0;
            $.each(e.data[0].camposivr, function(i, valor){
                if($("#"+keys[key]).length > 0){
                    $("#"+keys[key]).val(valor); 
                }
                key++;
            });
            buscarRegistros(e.data);
//          $("#btnBuscar").click();
        }
        
        if(typeof(e.data)== \'object\'){
            switch (e.data["accion"]){
                case "llamadaDesdeG" :
                    parent.postMessage(e.data, \'*\');
                    break;
                case "cerrargestion":
                    var origen="formulario"
                    finalizarGestion(e.data[datos],origen);
                    break;
            }
        }
    });
    
	$(function(){
    ';
    
        $campo .="
        //DATEPICKER
        {$strDatepicker}
        "; 
            
        $campo .="
        //TIMEPICKER
        {$strTimepicker}
        ";            
    
		$campo .='var ObjDataGestion= new Object();
        ObjDataGestion.server         = \'<?php echo $http; ?>\';
        ObjDataGestion.canal          = \'<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "Sin canal"; }?>\';
        ObjDataGestion.token          = \'<?php echo $_GET["token"];?>\';
        ObjDataGestion.predictiva     = \'<?php if(isset($_GET[\'predictiva\'])) { echo $_GET[\'predictiva\']; } else{ echo \'null\'; }?>\';
        ObjDataGestion.consinte       = \'<?php if(isset($_GET[\'consinte\'])) { echo $_GET[\'consinte\']; } else{ echo \'null\'; }?>\';
        ObjDataGestion.id_campana_crm = \'<?php if(isset($_GET[\'id_campana_crm\'])) { echo $_GET[\'id_campana_crm\']; } else{ echo \'null\'; }?>\';
        ObjDataGestion.sentido        = \'<?php if(isset($_GET[\'sentido\'])) { echo $_GET[\'sentido\']; } else{ echo \'null\'; }?>\';
        ObjDataGestion.usuario        = \'<?php if(isset($_GET[\'usuario\'])) { echo $_GET[\'usuario\']; } else{ echo \'0\'; }?>\';
        ObjDataGestion.origen         = \'<?php if(isset($_GET[\'origen\'])) { echo $_GET[\'origen\']; } else{ echo \'Sin origen\'; }?>\';
        ObjDataGestion.ani            = \'<?php if(isset($_GET[\'ani\'])) { echo $_GET[\'ani\']; } else{ echo \'0\'; }?>\';
            $("#btnBuscar").click(function(){
            var valido=0;
            '.$strValidaCorreo_t.'
            
            if(valido==0){
                var datos = $("#formId").serialize();
                $.ajax({
                    url     	: \'formularios/'.$str_guion.'/'.$str_guion.'_Funciones_Busqueda_Manual.php?action=GET_DATOS<?=$id_campan?>&agente=<?=$_GET["usuario"]?>\',
                    type		: \'post\',
                    dataType	: \'json\',
                    data		: datos,
                    success 	: function(datosq){
                        if(datosq[0].cantidad_registros > 1){
                            buscarRegistros(datosq);
                        }else if(datosq[0].cantidad_registros == 1){
                            $("#buscador").hide();
                            $("#botones").hide();
                            $("#resulados").hide();
                            var id = datosq[0].registros[0].'.$str_guion.'_ConsInte__b;
                            if(datosq[0].registros[0].id_muestra == null){
                                $.ajax({
                                    url     	: \'formularios/generados/PHP_Ejecutar.php?action=ADD_MUESTRA&campana_crm=<?php echo $_GET[\'id_campana_crm\'];?>\',
                                    type		: \'post\',
                                    data        : {id:id},
                                    success 	: function(data){
                                        <?php if(isset($_GET[\'token\'])) { ?>
                                            guardarStorage(\'<?php echo $_GET["id_gestion_cbx"]?>\',id,ObjDataGestion);
                                            $("#frameContenedor").attr(\'src\', \'<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user=\'+ id +\'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "Sin canal"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET[\'predictiva\'])) { echo "&predictiva=".$_GET[\'predictiva\']; }?><?php if(isset($_GET[\'consinte\'])) { echo "&consinte=".$_GET[\'consinte\']; }?><?php if(isset($_GET[\'id_campana_crm\'])) { echo "&campana_crm=".$_GET[\'id_campana_crm\']; }?><?php if(isset($_GET[\'sentido\'])) { echo "&sentido=".$_GET[\'sentido\']; }?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?><?php if(isset($_GET[\'origen\'])){echo \'&origen=\'.$_GET[\'origen\'];}else{echo \'&origen=BusquedaManual\';} ?>\');
                                        <?php } ?>
                                    }
                                });
                            }else{
                                <?php if(isset($_GET[\'token\'])) { ?>
                                    guardarStorage(\'<?php echo $_GET["id_gestion_cbx"]?>\',id,ObjDataGestion);
                                    $("#frameContenedor").attr(\'src\', \'<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user=\'+ id +\'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "Sin canal"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET[\'predictiva\'])) { echo "&predictiva=".$_GET[\'predictiva\']; }?><?php if(isset($_GET[\'consinte\'])) { echo "&consinte=".$_GET[\'consinte\']; }?><?php if(isset($_GET[\'id_campana_crm\'])) { echo "&campana_crm=".$_GET[\'id_campana_crm\']; }?><?php if(isset($_GET[\'sentido\'])) { echo "&sentido=".$_GET[\'sentido\']; }?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?><?php if(isset($_GET[\'origen\'])){echo \'&origen=\'.$_GET[\'origen\'];}else{echo \'&origen=BusquedaManual\';} ?>\');
                                <?php } ?>                        
                            }
                        }else{
                            adicionarRegistro(2,datosq[0].mensaje);
                        }
                    }
                });
            }
		});

		$("#btnCancelar").click(function(){
            $("#btnCancelar").attr("disabled",true);
            borrarStorage(\'<?php echo $_GET["id_gestion_cbx"]?>\');
            $.ajax({
                url     	: \'formularios/generados/PHP_Cerrar_Cancelar.php?canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "Sin canal"; } ?>&token=<?php echo $_GET[\'token\'];?>&id_gestion_cbx=<?php echo $_GET[\'id_gestion_cbx\'];?><?php if(isset($_GET[\'id_campana_crm\'])) { echo "&campana_crm=".$_GET[\'id_campana_crm\']; }?><?php if(isset($_GET[\'sentido\'])) { echo "&sentido=".$_GET[\'sentido\']; }?><?php if(isset($_GET[\'origen\'])){echo \'&origen=\'.$_GET[\'origen\'];}else{echo \'&origen=BusquedaManual\';} ?><?php echo "&idMonoef=".$idMonoef;?><?php echo "&tiempoInicio=".date("Y-m-d H:i:s");?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?>\',
                type		: \'post\',
                dataType	: \'json\',
                success 	: function(data){
                    var origen="bqmanual";
                    finalizarGestion(data,origen);    
                }
            });    
		});
	});

    function buscarRegistros(datosq){
        var ObjDataGestion= new Object();
        ObjDataGestion.server         = \'<?php echo $http; ?>\';
        ObjDataGestion.canal          = \'<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal"; }?>\';
        ObjDataGestion.token          = \'<?php echo $_GET["token"];?>\';
        ObjDataGestion.predictiva     = \'<?php if(isset($_GET[\'predictiva\'])) { echo $_GET[\'predictiva\']; } else{ echo \'null\'; }?>\';
        ObjDataGestion.consinte       = \'<?php if(isset($_GET[\'consinte\'])) { echo $_GET[\'consinte\']; } else{ echo \'null\'; }?>\';
        ObjDataGestion.id_campana_crm = \'<?php if(isset($_GET[\'id_campana_crm\'])) { echo $_GET[\'id_campana_crm\']; } else{ echo \'null\'; }?>\';
        ObjDataGestion.sentido        = \'<?php if(isset($_GET[\'sentido\'])) { echo $_GET[\'sentido\']; } else{ echo \'null\'; }?>\';
        ObjDataGestion.usuario        = \'<?php if(isset($_GET[\'usuario\'])) { echo $_GET[\'usuario\']; } else{ echo \'0\'; }?>\';
        ObjDataGestion.origen         = \'<?php if(isset($_GET[\'origen\'])) { echo $_GET[\'origen\']; } else{ echo \'Sin origen\'; }?>\';
        ObjDataGestion.ani            = \'<?php if(isset($_GET[\'ani\'])) { echo $_GET[\'ani\']; } else{ echo \'0\'; }?>\';        
        var valores = null;
        var tabla_a_mostrar = \'<div class="box box-default">\'+
        \'<div class="box-header">\'+
            \'<h3 class="box-title">RESULTADOS DE LA BUSQUEDA</h3>\'+
        \'</div>\'+
        \'<div class="box-body">\'+
            \'<table class="table table-hover table-bordered" style="width:100%;">\';
        tabla_a_mostrar += \'<thead>\';
        tabla_a_mostrar += \'<tr>\';
        tabla_a_mostrar += \' '.$str_tablaCampos.' \';
        tabla_a_mostrar += \'</tr>\';
        tabla_a_mostrar += \'</thead>\';
        tabla_a_mostrar += \'<tbody>\';
        $.each(datosq[0].registros, function(i, item) {
            tabla_a_mostrar += \'<tr idMuestra="\'+ item.id_muestra +\'" ConsInte="\'+ item.'.$str_guion.'_ConsInte__b +\'" class="EditRegistro">\';
            tabla_a_mostrar += \''.$str_tablaCuerpo.'\';
            tabla_a_mostrar += \'</tr>\';
        });
        tabla_a_mostrar += \'</tbody>\';
        tabla_a_mostrar += \'</table></div></div>\';

        $("#resultadosBusqueda").html(tabla_a_mostrar);

        $(".EditRegistro").dblclick(function(){
            var id = $(this).attr("ConsInte");
            var muestra=$(this).attr("idMuestra");
            swal({
                html : true,
                title: "Información - Dyalogo CRM",
                text: \'Esta seguro de editar este registro?\',
                type: "warning",
                confirmButtonText: "Editar registro",
                cancelButtonText : "No Editar registro",
                showCancelButton : true,
                closeOnConfirm : true
            },
                function(isconfirm){
                    if(isconfirm){
                        $("#buscador").hide();
                        $("#botones").hide();
                        $("#resulados").hide();
                        if(muestra == \'null\'){
                            $.ajax({
                                url     	: \'formularios/generados/PHP_Ejecutar.php?action=ADD_MUESTRA&campana_crm=<?php echo $_GET[\'id_campana_crm\'];?>\',
                                type		: \'post\',
                                data        : {id:id},
                                success 	: function(data){
                                    <?php if(isset($_GET[\'token\'])) { ?>
                                        guardarStorage(\'<?php echo $_GET["id_gestion_cbx"]?>\',id,ObjDataGestion);
                                        $("#frameContenedor").attr(\'src\', \'<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user=\'+ id +\'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "Sin canal"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET[\'predictiva\'])) { echo "&predictiva=".$_GET[\'predictiva\']; }?><?php if(isset($_GET[\'consinte\'])) { echo "&consinte=".$_GET[\'consinte\']; }?><?php if(isset($_GET[\'id_campana_crm\'])) { echo "&campana_crm=".$_GET[\'id_campana_crm\']; }?><?php if(isset($_GET[\'sentido\'])) { echo "&sentido=".$_GET[\'sentido\']; }?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?><?php if(isset($_GET[\'origen\'])){echo \'&origen=\'.$_GET[\'origen\'];}else{echo \'&origen=BusquedaManual\';} ?>\');
                                    <?php } ?>
                                }
                            });
                        }else{
                            <?php if(isset($_GET[\'token\'])) { ?>
                                guardarStorage(\'<?php echo $_GET["id_gestion_cbx"]?>\',id,ObjDataGestion);
                                $("#frameContenedor").attr(\'src\', \'<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user=\'+ id +\'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "Sin canal"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET[\'predictiva\'])) { echo "&predictiva=".$_GET[\'predictiva\']; }?><?php if(isset($_GET[\'consinte\'])) { echo "&consinte=".$_GET[\'consinte\']; }?><?php if(isset($_GET[\'id_campana_crm\'])) { echo "&campana_crm=".$_GET[\'id_campana_crm\']; }?><?php if(isset($_GET[\'sentido\'])) { echo "&sentido=".$_GET[\'sentido\']; }?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?><?php if(isset($_GET[\'origen\'])){echo \'&origen=\'.$_GET[\'origen\'];}else{echo \'&origen=BusquedaManual\';} ?>\');
                            <?php } ?>                        
                        }
                    }else{
                        $("#buscador").show();
                        $("#botones").show();
                        $("#resulados").show();
                    }
                });
            });    
    }
    
	function adicionarRegistro(tipo,mensaje){
        var ObjDataGestion= new Object();
        ObjDataGestion.server         = \'<?php echo $http; ?>\';
        ObjDataGestion.canal          = \'<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "Sin canal"; }?>\';
        ObjDataGestion.token          = \'<?php echo $_GET["token"];?>\';
        ObjDataGestion.predictiva     = \'<?php if(isset($_GET[\'predictiva\'])) { echo $_GET[\'predictiva\']; } else{ echo \'null\'; }?>\';
        ObjDataGestion.consinte       = \'<?php if(isset($_GET[\'consinte\'])) { echo $_GET[\'consinte\']; } else{ echo \'null\'; }?>\';
        ObjDataGestion.id_campana_crm = \'<?php if(isset($_GET[\'id_campana_crm\'])) { echo $_GET[\'id_campana_crm\']; } else{ echo \'null\'; }?>\';
        ObjDataGestion.sentido        = \'<?php if(isset($_GET[\'sentido\'])) { echo $_GET[\'sentido\']; } else{ echo \'null\'; }?>\';
        ObjDataGestion.usuario        = \'<?php if(isset($_GET[\'sentido\'])) { echo $_GET[\'usuario\']; } else{ echo \'0\'; }?>\';
        ObjDataGestion.origen         = \'<?php if(isset($_GET[\'origen\'])) { echo $_GET[\'origen\']; } else{ echo \'Sin origen\'; }?>\';
        ObjDataGestion.ani            = \'<?php if(isset($_GET[\'ani\'])) { echo $_GET[\'ani\']; } else{ echo \'0\'; }?>\';        
    
        if(mensaje == null ){
            if(tipo == "1"){
                mensaje = "Desea adicionar un registro";
            }
            if(tipo == "2"){
                mensaje = "No se encontraron datos, desea adicionar un registro";
            }
            swal({
                html : true,
                title: "Información - Dyalogo CRM",
                text: mensaje,
                type: "warning",
                 confirmButtonText: "Adicionar registro",
                cancelButtonText : "Hacer otra busqueda",
                showCancelButton : true,
                closeOnConfirm : true
            },
            function(isconfirm){
                $("#buscador").hide();
                $("#botones").hide();
                $("#resulados").hide();
                if(isconfirm){
                    //JDBD - obtenemos los valores de la busqueda manual para enviarselos al formulario
                    '.$strCamps_t.'
                    $.ajax({
                    url     	: \'formularios/generados/PHP_Ejecutar.php?action=ADD<?php if(isset($_GET[\'canal\'])){ echo "&canal={$_GET[\'canal\']}";}?>&campana_crm=<?php echo $_GET[\'id_campana_crm\'];?>\',
                    type		: \'post\',
                    dataType	: \'json\',
                    success 	: function(numeroIdnuevo){
                        <?php if(isset($_GET[\'token\'])){ ?>
                        guardarStorage(\'<?php echo $_GET["id_gestion_cbx"]?>\',numeroIdnuevo,ObjDataGestion);
                        $("#frameContenedor").attr(\'src\', \'<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user=\'+ numeroIdnuevo +\'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "Sin canal"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET[\'predictiva\'])) { echo "&predictiva=".$_GET[\'predictiva\']; }?><?php if(isset($_GET[\'consinte\'])) { echo "&consinte=".$_GET[\'consinte\']; }?><?php if(isset($_GET[\'id_campana_crm\'])) { echo "&campana_crm=".$_GET[\'id_campana_crm\']; }?><?php if(isset($_GET[\'sentido\'])) { echo "&sentido=".$_GET[\'sentido\']; }?><?php if(isset($_GET[\'origen\'])){echo \'&origen=\'.$_GET[\'origen\'];}else{echo \'&origen=BusquedaManual\';} ?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?>&nuevoregistro=true'.$strSendCamps_t.');
                        <?php } ?>
                    }
                });
                }else{
                    limpiar();
                }
            });
        }else{
            swal("", mensaje, "warning");
        }
	}
	function limpiar(){
		$("#buscador").show();
		$("#buscador :input").each(function(){
			$(this).val("");
		});
		$("#resultadosBusqueda").html("");
		$("#botones").show();
		$("#resulados").show();
	}
</script>';
			
			/* Escribimos lo que hemos generado arriba el Html , PHP , JAVASCRIPT */
			
			fputs($fp , $campo);
			fputs($fp , chr(13).chr(10)); // Genera saldo de linea 
			fclose($fp); //cerramos el editor de ese archivo

		
			
			/* Abrimos un editor para el archivo que hara las busquedas */

			$fp2 = fopen($carpeta."/".$str_guion."_Funciones_Busqueda_Manual.php" , "w");
			//chmod($carpeta."/".$str_guion."_Funciones_Busqueda_Manual.php" , 0777); 
			$campo = '<?php
	ini_set(\'display_errors\', \'On\');
	ini_set(\'display_errors\', 1);
	include(__DIR__."/../../conexion.php");
	date_default_timezone_set(\'America/Bogota\');
    if (!empty($_SERVER[\'HTTP_X_REQUESTED_WITH\']) && strtolower($_SERVER[\'HTTP_X_REQUESTED_WITH\']) == \'xmlhttprequest\') {
		if($_GET[\'action\'] == "GET_DATOS"){

			//JDBD - Busqueda manual.          
            if(isset($_GET["id_campana_crm"]) && $_GET["id_campana_crm"] != 0){

                $querymuestra="SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b FROM ".$BaseDatos_systema.".CAMPAN where CAMPAN_ConsInte__b=".$_GET["id_campana_crm"];

                $querymuestra=$mysqli->query($querymuestra);

                if($querymuestra && $querymuestra->num_rows > 0){
                    $datoCampan=$querymuestra->fetch_array();

                    $str_Pobla_Campan = "G".$datoCampan["CAMPAN_ConsInte__GUION__Pob_b"];
                    $intMuestra= $str_Pobla_Campan."_M".$datoCampan["CAMPAN_ConsInte__MUESTR_b"];
                    
                }        
            }
            ';

        $intAgente_t=0;
        switch($intTipoBusqueda_t){
            case '1':
                $campo .='$str_Lsql = " SELECT *, ".$intMuestra."_CoInMiPo__b AS id_muestra FROM ".$BaseDatos.".".$str_Pobla_Campan." LEFT JOIN ".$BaseDatos.".".$intMuestra." ON ".$str_Pobla_Campan."_ConsInte__b = ".$intMuestra."_CoInMiPo__b";
                ';
                break;

            case '2':
                $campo .='$str_Lsql = " SELECT *, ".$intMuestra."_CoInMiPo__b AS id_muestra FROM ".$BaseDatos.".".$str_Pobla_Campan." JOIN ".$BaseDatos.".".$intMuestra." ON ".$str_Pobla_Campan."_ConsInte__b = ".$intMuestra."_CoInMiPo__b";
                ';                
                break;

            case '3':
                $campo .='$str_Lsql = " SELECT *, ".$intMuestra."_CoInMiPo__b AS id_muestra FROM ".$BaseDatos.".".$str_Pobla_Campan." JOIN ".$BaseDatos.".".$intMuestra." ON ".$str_Pobla_Campan."_ConsInte__b = ".$intMuestra."_CoInMiPo__b";
                ';
                $intAgente_t=1;
                break;
            default:
                $campo .='$str_Lsql = " SELECT *, ".$intMuestra."_CoInMiPo__b AS id_muestra FROM ".$BaseDatos.".".$str_Pobla_Campan." LEFT JOIN ".$BaseDatos.".".$intMuestra." ON ".$str_Pobla_Campan."_ConsInte__b=".$intMuestra."_CoInMiPo__b";
                ';
                break;
                
        }
			
            $campo .='$Where = " WHERE ".$str_Pobla_Campan."_ConsInte__b > 0 AND ";
			$usados = 0;'.$where.'
            ';
            
            $strWhere=0;
            if($intAgente_t==1){
                
                $campo .='$wheres="";
            if($usados==0){
                $Where = " WHERE ".$intMuestra."_ConIntUsu_b = ".$_GET["agente"];
            }else{
                $wheres=$Where;
                $Where .= " AND ".$intMuestra."_ConIntUsu_b = ".$_GET["agente"];
            }
            $str_Lsql .= $Where;                
                ';
            $strWhere=1;    
            }else{
                $campo .='if($usados==0){
                    $Where = "";
                }
                $str_Lsql .= $Where;
                ';
            }
            
			$campo .='
            //echo $str_Lsql;
			$resultado = $mysqli->query($str_Lsql);
			$arrayDatos = array();
			while ($key = $resultado->fetch_assoc()) {
				$arrayDatos[] = $key;
			}

			$newJson = array();
			$newJson[0][\'cantidad_registros\'] = $resultado->num_rows;
			$newJson[0][\'registros\'] = $arrayDatos;
            if($usados == 0 && $resultado->num_rows == 0){
                $newJson[0][\'mensaje\'] = "No se encontraron registros para este tipo de busqueda";
            }else{
                if($usados == 1 && $resultado->num_rows == 0){
                    $strSQLValidaReg="SELECT * FROM {$BaseDatos}.{$str_Pobla_Campan}'; 
                    if($strWhere==0){
                       $campo .='{$Where}";
                       ';
                    }else{
                        $campo .='{$wheres}";
                        ';    
                    }
                    
                    $campo .=
                    '$strSQLValidaReg=$mysqli->query($strSQLValidaReg);
                    if($strSQLValidaReg ->num_rows > 0){
                        $newJson[0][\'mensaje\'] = "No tienes permiso para acceder a este registro";
                    }
                }
            }

			echo json_encode($newJson);
		}
	}
?>';		/* Escribimos lo que hemos generado arriba */

			fputs($fp2 , $campo);
			fputs($fp2 , chr(13).chr(10)); // Genera saldo de linea 
			fclose($fp2); 

		}else{
			echo "no se puede generar si no me envias nada";
		}
	}