<?php
    function generar_tablas_bd($idFormulario_Crear, $generar_tabla, $generar_formulario,  $generar_busqueda, $generar_web_form,$tipoGuion=0){
        
        global $mysqli;
        global $BaseDatos;
        global $BaseDatos_systema;
        global $BaseDatos_telefonia;
        global $dyalogo_canales_electronicos;
        global $BaseDatos_general;

        $Lsql = "SELECT * FROM ".$BaseDatos.".G".$idFormulario_Crear;
        $generarA = "G".$idFormulario_Crear;
        $res_Lsql = $mysqli->query($Lsql);
        if($res_Lsql){

            if($generar_tabla != 0){
                //echo "aqui";
                /* La tabla ya ha sido generada por lo menos una vez, toca editarla  */
                /* preguntamos si algun campo fue creado o editado */
                $pregun_Lsql = "SELECT PREGUN_ConsInte__b, PREGUN_Tipo______b, PREGUN_FueGener_b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$idFormulario_Crear." AND PREGUN_FueGener_b != 0 AND (PREGUN_Tipo______b != 9  AND PREGUN_Tipo______b != 12  AND PREGUN_Tipo______b != 16 )";
                $res_PregunLsql = $mysqli->query($pregun_Lsql);
                $valido = 0;


                /*VALIDAMOS QUE LOS CMAPOS EXISTAN */
                $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_ConsInte__b'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_ConsInte__b bigint(20) NOT NULL AUTO_INCREMENT";
                    $mysqli->query($edit_Lsql);
                }

                  $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_FechaInsercion'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_FechaInsercion datetime DEFAULT NOW()";
                    $mysqli->query($edit_Lsql);
                }

                $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_Usuario'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_Usuario bigint(20) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }

                $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_CodigoMiembro'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_CodigoMiembro bigint(20) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }

                $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_CodigoMiembro'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_CodigoMiembro bigint(20) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }

                 $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_PoblacionOrigen'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_PoblacionOrigen bigint(20) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }

                 $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_IdLlamada'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_IdLlamada varchar(50) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }

                $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_Sentido___b'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_Sentido___b varchar(10) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }

                $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_Canal_____b'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_Canal_____b varchar(20) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }

                 $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_CantidadIntentos'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_CantidadIntentos bigint(20) DEFAULT 0";
                    $mysqli->query($edit_Lsql);
                }

                $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_UltiGest__b'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_UltiGest__b bigint(20) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }

                $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_FecUltGes_b'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_FecUltGes_b datetime DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }

                $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_GesMasImp_b'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_GesMasImp_b bigint(20) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }

                $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_FeGeMaIm__b'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_FeGeMaIm__b datetime DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }

                 $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_EstadoDiligenciamiento'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_EstadoDiligenciamiento bigint(20) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }

                

               


                if($tipoGuion == 1){

                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_LinkContenido'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_LinkContenido varchar(500) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }
                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_Origen_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_Origen_b varchar(50) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }
                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_DetalleCanal'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_DetalleCanal varchar(255) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }
                     $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_DatoContacto'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_DatoContacto varchar(255) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }
                      $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_Paso'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_Paso bigint(20) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }
                      $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_Clasificacion'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_Clasificacion smallint(5) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }
                }

               



                if($tipoGuion == 2){
                      $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_TipoReintentoUG_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_TipoReintentoUG_b  smallint(5) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }

                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_TipoReintentoGMI_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_TipoReintentoGMI_b smallint(5) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }

                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_FecHorAgeUG_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_FecHorAgeUG_b datetime DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }


                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_FecHorAgeGMI_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_FecHorAgeGMI_b datetime DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }

                   $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_ClasificacionUG_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_ClasificacionUG_b  smallint(5) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }

                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_ClasificacionGMI_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_ClasificacionGMI_b smallint(5) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }

                     $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_EstadoUG_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_EstadoUG_b  bigint(20) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }

                     $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_EstadoGMI_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_EstadoGMI_b bigint(20) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }
                    

                     $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_UsuarioUG_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_UsuarioUG_b  bigint(20) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }

                     $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_UsuarioGMI_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_UsuarioGMI_b bigint(20) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }

                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_CanalGMI_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_CanalGMI_b varchar(20) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }


                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_SentidoGMI_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_SentidoGMI_b varchar(10) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }

                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_CantidadIntentosGMI_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_CantidadIntentosGMI_b  bigint(20) DEFAULT 0";
                        $mysqli->query($edit_Lsql);
                    }


                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_ComentarioUG_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_ComentarioUG_b  longtext DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }


                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_ComentarioGMI_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_ComentarioGMI_b longtext DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }

                     $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_LinkContenidoUG_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_LinkContenidoUG_b varchar(500) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }

                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_LinkContenidoGMI_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_LinkContenidoGMI_b varchar(500) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }

                      $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_DetalleCanalUG_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_DetalleCanalUG_b varchar(255) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }
                      $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_DetalleCanalGMI_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_DetalleCanalGMI_b varchar(255) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }
                      $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_DatoContactoUG_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_DatoContactoUG_b varchar(255) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }
                      $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_DatoContactoGMI_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_DatoContactoGMI_b varchar(255) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }

                      $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_PasoUG_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_PasoUG_b bigint(20) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }
                      $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_PasoGMI_b'";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_PasoGMI_b bigint(20) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }




                }


                                  
                                  

                      
          


                /* Luego si se proecede con el resto de las cosas a generar */

                while ($res = $res_PregunLsql->fetch_object()) {
                    if($res->PREGUN_FueGener_b == 1){
                        /* Campo Nuevo */
            
                        $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_C".$res->PREGUN_ConsInte__b."'";
                        $result = $mysqli->query($Lsql);
                        //echo $res->PREGUN_Tipo______b;
                        if($result->num_rows === 0){
                            /* recorremos todos los campos del guion y lo creamos */
                            if($res->PREGUN_Tipo______b == '5'){
                                /* es de tipo Fecha u Hora y toca ponerle dateTime */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." datetime DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '10'){
                                /* es de tipo Fecha u Hora y toca ponerle dateTime */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." datetime DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '3'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '6'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                            } 

                            if($res->PREGUN_Tipo______b == '13'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                            } 

                            if($res->PREGUN_Tipo______b == '14'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." varchar(253) CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                            } 

                            if($res->PREGUN_Tipo______b == '11'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '4'){
                                /* la pregunta es Decimal */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b."  double DEFAULT NULL";
                            } 

                            if($res->PREGUN_Tipo______b == '1' || $res->PREGUN_Tipo______b == '15'){
                                /* es de tipo Varchar */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." varchar(253) CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '2'){
                                /* es de tipo Memo */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." longtext CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '8'){
                                /* es de tipo CheckBox */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." smallint(5) DEFAULT NULL";
                            }

                            if($mysqli->query($edit_Lsql) === true){

                                $valido = 1;
                                $str_Lsql = "UPDATE ".$BaseDatos_systema.".PREGUN SET PREGUN_FueGener_b = 0 WHERE PREGUN_ConsInte__b = ".$res->PREGUN_ConsInte__b;


                                if($mysqli->query($str_Lsql) == TRUE){

                                }else{
                                    echo "Error Borrando el campo".$mysqli->error;
                                }
                            }else{
                                echo "Error Agregando Columna => ".$mysqli->error;
                            }
                        }

                    }

                    if($res->PREGUN_FueGener_b == 2){

                        $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_C".$res->PREGUN_ConsInte__b."'";
                        $result = $mysqli->query($Lsql);
                        if($result->num_rows === 0){
                            /* recorremos todos los campos del guion y lo creamos */
                            if($res->PREGUN_Tipo______b == '5'){
                                /* es de tipo Fecha u Hora y toca ponerle dateTime */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." datetime DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '10'){
                                /* es de tipo Fecha u Hora y toca ponerle dateTime */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." datetime DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '3'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '6'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                            } 

                            if($res->PREGUN_Tipo______b == '13'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                            } 

                            if($res->PREGUN_Tipo______b == '14'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." varchar(253) CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                            } 

                            if($res->PREGUN_Tipo______b == '11'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '4'){
                                /* la pregunta es Decimal */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b."  double DEFAULT NULL";
                            } 

                            if($res->PREGUN_Tipo______b == '1' || $res->PREGUN_Tipo______b == '15'){
                                /* es de tipo Varchar */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." varchar(253) CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '2'){
                                /* es de tipo Memo */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." longtext CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '8'){
                                /* es de tipo CheckBox */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." ADD ".$generarA."_C".$res->PREGUN_ConsInte__b." smallint(5) DEFAULT NULL";
                            }

                            if($mysqli->query($edit_Lsql) === true){
                                $valido = 1;
                                $str_Lsql = "UPDATE ".$BaseDatos_systema.".PREGUN SET PREGUN_FueGener_b = 0 WHERE PREGUN_ConsInte__b = ".$res->PREGUN_ConsInte__b;


                                if($mysqli->query($str_Lsql) == TRUE){

                                }else{
                                    echo "Error Borrando el campo".$mysqli->error;
                                }
                            }else{
                                echo "Error Editando Columna => ".$mysqli->error;
                            }

                        }else{
                            /* campo editado ya estaba creado */
                            /* recorremos todos los campos del guion */
                            if($res->PREGUN_Tipo______b == '5'){
                                /* es de tipo Fecha u Hora y toca ponerle dateTime */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." datetime DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '10'){
                                /* es de tipo Fecha u Hora y toca ponerle dateTime */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." datetime DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '3'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '6'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '13'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '14'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." varchar(253) CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                            }


                            if($res->PREGUN_Tipo______b == '11'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                            } 

                            if($res->PREGUN_Tipo______b == '4'){
                                /* la pregunta es Decimal */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b."  double DEFAULT NULL";
                            } 

                            if($res->PREGUN_Tipo______b == '1' || $res->PREGUN_Tipo______b == '15'){
                                /* es de tipo Varchar */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." varchar(253) CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '2'){
                                /* es de tipo Memo */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." longtext CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                            }

                            if($res->PREGUN_Tipo______b == '8'){
                                /* es de tipo CheckBox */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." smallint(5) DEFAULT NULL";
                            }

                            if($mysqli->query($edit_Lsql) === true){    
                                $valido = 1;
                                $str_Lsql = "UPDATE ".$BaseDatos_systema.".PREGUN SET PREGUN_FueGener_b = 0 WHERE PREGUN_ConsInte__b = ".$res->PREGUN_ConsInte__b;


                                if($mysqli->query($str_Lsql) == TRUE){

                                }else{
                                    echo "Error Borrando el campo".$mysqli->error;
                                }

                            }else{
                                $edit_Lsql = "UPDATE ".$BaseDatos.".".$generarA." SET ".$generarA."_C".$res->PREGUN_ConsInte__b." = NULL; ";
                                if($mysqli->query($edit_Lsql) === true){    

                                    if($res->PREGUN_Tipo______b == '5'){
                                        /* es de tipo Fecha u Hora y toca ponerle dateTime */
                                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." datetime DEFAULT NULL";
                                    }

                                    if($res->PREGUN_Tipo______b == '10'){
                                        /* es de tipo Fecha u Hora y toca ponerle dateTime */
                                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." datetime DEFAULT NULL";
                                    }

                                    if($res->PREGUN_Tipo______b == '3'){
                                        /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                                    }

                                    if($res->PREGUN_Tipo______b == '6'){
                                        /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                                    } 

                                    if($res->PREGUN_Tipo______b == '13'){
                                        /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                                    } 

                                    if($res->PREGUN_Tipo______b == '14'){
                                        /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." varchar(253) CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                                    } 

                                    if($res->PREGUN_Tipo______b == '11'){
                                        /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                                    }

                                    if($res->PREGUN_Tipo______b == '4'){
                                        /* la pregunta es Decimal */
                                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b."  double DEFAULT NULL";
                                    } 

                                    if($res->PREGUN_Tipo______b == '1' || $res->PREGUN_Tipo______b == '15'){
                                        /* es de tipo Varchar */
                                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." varchar(253) CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                                    }

                                    if($res->PREGUN_Tipo______b == '2'){
                                        /* es de tipo Memo */
                                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." longtext CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                                    }

                                    if($res->PREGUN_Tipo______b == '8'){
                                        /* es de tipo CheckBox */
                                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." CHANGE ".$generarA."_C".$res->PREGUN_ConsInte__b." ".$generarA."_C".$res->PREGUN_ConsInte__b." smallint(5) DEFAULT NULL";
                                    }

                                   // echo $edit_Lsql;
                                    //echo "Luego";
                                    if($mysqli->query($edit_Lsql) === true){   

                                    }else{
                                        echo "Error Modificando Columna => ".$mysqli->error;
                                    }
                                }else{
                                    echo "Error Modificando Columna  paso 1 => ".$mysqli->error;
                                }

                                //echo "Error Modificando Columna => ".$mysqli->error;
                            }
                        }
                    }

                    if($res->PREGUN_FueGener_b == 3){
                        $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$generarA." WHERE Field = '".$generarA."_C".$res->PREGUN_ConsInte__b."'";
                        $result = $mysqli->query($Lsql);
                        if($result->num_rows === 0){

                        }else{
                            /* campo eliminado */
                            /* recorremos todos los campos del guion */
                            if($res->PREGUN_Tipo______b == '5'){
                                /* es de tipo Fecha u Hora y toca ponerle dateTime */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." DROP COLUMN ".$generarA."_C".$res->PREGUN_ConsInte__b." ";
                            }

                            if($res->PREGUN_Tipo______b == '10'){
                                /* es de tipo Fecha u Hora y toca ponerle dateTime */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." DROP COLUMN ".$generarA."_C".$res->PREGUN_ConsInte__b." ";
                            }

                            if($res->PREGUN_Tipo______b == '3'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." DROP COLUMN ".$generarA."_C".$res->PREGUN_ConsInte__b." ";
                            } 


                            if($res->PREGUN_Tipo______b == '6'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." DROP COLUMN ".$generarA."_C".$res->PREGUN_ConsInte__b." ";
                            } 


                            if($res->PREGUN_Tipo______b == '13'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." DROP COLUMN ".$generarA."_C".$res->PREGUN_ConsInte__b." ";
                            } 

                            if($res->PREGUN_Tipo______b == '14'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." DROP COLUMN ".$generarA."_C".$res->PREGUN_ConsInte__b." ";
                            } 

                            if($res->PREGUN_Tipo______b == '11'){
                                /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." DROP COLUMN ".$generarA."_C".$res->PREGUN_ConsInte__b." ";
                            } 

                            if($res->PREGUN_Tipo______b == '4'){
                                /* la pregunta es Decimal */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." DROP COLUMN ".$generarA."_C".$res->PREGUN_ConsInte__b." ";
                            } 

                            if($res->PREGUN_Tipo______b == '1' || $res->PREGUN_Tipo______b == '15'){
                                /* es de tipo Varchar */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." DROP COLUMN ".$generarA."_C".$res->PREGUN_ConsInte__b." ";
                            }

                            if($res->PREGUN_Tipo______b == '2'){
                                /* es de tipo Memo */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." DROP COLUMN ".$generarA."_C".$res->PREGUN_ConsInte__b." ";
                            }

                            if($res->PREGUN_Tipo______b == '8'){
                                /* es de tipo CheckBox */
                                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".".$generarA." DROP COLUMN ".$generarA."_C".$res->PREGUN_ConsInte__b." ";
                            }


                            if($mysqli->query($edit_Lsql) === true){
                                $valido = 1;
                            }else{
                                echo "Error Eliminando Columna => ".$mysqli->error;
                            }


                            //Borramos campo_
                            $Lsql_Del_Campo = "DELETE FROM ".$BaseDatos_systema.".CAMPO_ WHERE CAMPO__ConsInte__PREGUN_b = ".$res->PREGUN_ConsInte__b;

                            if($mysqli->query($Lsql_Del_Campo) == TRUE){

                            }else{
                                echo "Error Borrando el Lsql_Del_Campo".$mysqli->error;
                            }

                            /* Borramos camcom */
                            
                            $Lsql_Del_Camco = "DELETE FROM ".$BaseDatos_systema.".CAMCON WHERE CAMCON_ConsInte__PREGUN_b = ".$res->PREGUN_ConsInte__b;

                            if($mysqli->query($Lsql_Del_Camco) == TRUE){

                            }else{
                                echo "Error Borrando el Lsql_Del_Camco".$mysqli->error;
                            }

                            /* Borramos caminc */
                            
                            $Lsql_Del_camin = "DELETE FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPO_Pob_b  = ".$res->PREGUN_ConsInte__b;

                            if($mysqli->query($Lsql_Del_camin) == TRUE){

                            }else{
                                echo "Error Borrando el Lsql_Del_camin".$mysqli->error;
                            }

                            /* Borramos Condiciones */
                            
                            /*$Lsql_Del_condi = "DELETE FROM ".$BaseDatos_systema.".ESTCON_CONDICIONES WHERE campo = ".$res->PREGUN_ConsInte__b;

                            if($mysqli->query($Lsql_Del_condi) == TRUE){

                            }else{
                                echo "Error Borrando el Lsql_Del_condi".$mysqli->error;
                            }*/


                            
                            /* Borramos Condiciones */
                            
                            $Lsql_Del_camor = "DELETE FROM ".$BaseDatos_systema.".CAMORD WHERE CAMORD_POBLCAMP__B LIKE '%".$res->PREGUN_ConsInte__b."'";

                            if($mysqli->query($Lsql_Del_camor) == TRUE){

                            }else{
                                echo "Error Borrando el Lsql_Del_camor".$mysqli->error;
                            }

                            /* Borramos Pregun */
                            
                            $str_Lsql = "DELETE FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b = ".$res->PREGUN_ConsInte__b;


                            if($mysqli->query($str_Lsql) == TRUE){

                            }else{
                                echo "Error Borrando el pregun  ".$mysqli->error;
                            }
                        }
                    }

                    
                }
            }
            generar_formularios_busquedas($idFormulario_Crear, $generar_formulario, $generar_busqueda, $generar_web_form);
            
        }else{
            if($generar_tabla != 0){
                /* la tabla No fue generada */

                
                //aqui entran los guiones tipo 1 que son script
                if($tipoGuion == 1){

                     $create_Lsql = "CREATE TABLE ".$BaseDatos.".".$generarA." (
                                ".$generarA."_ConsInte__b bigint(20) NOT NULL AUTO_INCREMENT,
                                ".$generarA."_FechaInsercion datetime DEFAULT NOW(),
                                ".$generarA."_Usuario bigint(20) DEFAULT NULL,
                                ".$generarA."_CodigoMiembro bigint(20) DEFAULT NULL,
                                ".$generarA."_PoblacionOrigen bigint(20) DEFAULT NULL,                                
                                ".$generarA."_Origen_b varchar(50) DEFAULT NULL,                                
                                ".$generarA."_IdLlamada varchar(50) DEFAULT NULL,                               
                                ".$generarA."_Sentido___b varchar(10) DEFAULT NULL,
                                ".$generarA."_Canal_____b varchar(20) DEFAULT NULL,
                                ".$generarA."_CantidadIntentos bigint(20) DEFAULT 0,

                                ".$generarA."_LinkContenido varchar(500) DEFAULT NULL,
                                ".$generarA."_DetalleCanal varchar(255) DEFAULT NULL,
                                ".$generarA."_DatoContacto varchar(255) DEFAULT NULL,
                                ".$generarA."_Paso bigint(20) DEFAULT NULL,
                                ".$generarA."_Clasificacion smallint(5) DEFAULT NULL,
                                ".$generarA."_Duracion___b time DEFAULT NULL,

                                ".$generarA."_EstadoDiligenciamiento bigint(20) DEFAULT NULL,
                                ".$generarA."_UltiGest__b bigint(20) DEFAULT NULL , 
                                ".$generarA."_FecUltGes_b datetime DEFAULT NULL,
                                ".$generarA."_GesMasImp_b bigint(20) DEFAULT NULL,
                                ".$generarA."_FeGeMaIm__b datetime DEFAULT NULL";

                //aqui entran los guiones tipo 2 qeu son BD        
                }else  if($tipoGuion == 2){

                    $create_Lsql = "CREATE TABLE ".$BaseDatos.".".$generarA." (
                                    ".$generarA."_ConsInte__b bigint(20) NOT NULL AUTO_INCREMENT,
                                    ".$generarA."_FechaInsercion datetime DEFAULT NOW(),
                                    ".$generarA."_Usuario bigint(20) DEFAULT NULL,                               
                                    ".$generarA."_IdLlamada varchar(50) DEFAULT NULL,

                                    ".$generarA."_UltiGest__b bigint(20) DEFAULT NULL , 
                                    ".$generarA."_GesMasImp_b bigint(20) DEFAULT NULL,
                                    ".$generarA."_FecUltGes_b datetime DEFAULT NULL,
                                    ".$generarA."_FeGeMaIm__b datetime DEFAULT NULL,
                                    ".$generarA."_TipoReintentoUG_b smallint(5) DEFAULT NULL, 
                                    ".$generarA."_TipoReintentoGMI_b smallint(5) DEFAULT NULL,
                                    ".$generarA."_FecHorAgeUG_b datetime DEFAULT NULL,
                                    ".$generarA."_FecHorAgeGMI_b datetime DEFAULT NULL,
                                    ".$generarA."_ClasificacionUG_b smallint(5) DEFAULT NULL,
                                    ".$generarA."_ClasificacionGMI_b smallint(5) DEFAULT NULL,
                                    ".$generarA."_EstadoUG_b  bigint(20) DEFAULT NULL, 
                                    ".$generarA."_EstadoGMI_b bigint(20) DEFAULT NULL,
                                    ".$generarA."_UsuarioUG_b  bigint(20) DEFAULT NULL,
                                    ".$generarA."_UsuarioGMI_b bigint(20) DEFAULT NULL,
                                    ".$generarA."_Canal_____b varchar(20) DEFAULT NULL,
                                    ".$generarA."_CanalGMI_b varchar(20) DEFAULT NULL,
                                    ".$generarA."_Sentido___b varchar(10) DEFAULT NULL,
                                    ".$generarA."_SentidoGMI_b varchar(10) DEFAULT NULL,
                                    ".$generarA."_CantidadIntentos bigint(20) DEFAULT 0,
                                    ".$generarA."_CantidadIntentosGMI_b bigint(20) DEFAULT 0,
                                    ".$generarA."_ComentarioUG_b longtext DEFAULT NULL,
                                    ".$generarA."_ComentarioGMI_b longtext DEFAULT NULL,
                                    ".$generarA."_LinkContenidoUG_b varchar(500) DEFAULT NULL,
                                    ".$generarA."_LinkContenidoGMI_b varchar(500) DEFAULT NULL,
                                    ".$generarA."_DetalleCanalUG_b varchar(255) DEFAULT NULL,
                                    ".$generarA."_DetalleCanalGMI_b varchar(255) DEFAULT NULL,
                                    ".$generarA."_DatoContactoUG_b varchar(255) DEFAULT NULL,
                                    ".$generarA."_DatoContactoGMI_b varchar(255) DEFAULT NULL,
                                    ".$generarA."_PasoUG_b bigint(20) DEFAULT NULL,
                                    ".$generarA."_PasoGMI_b bigint(20) DEFAULT NULL,

                                    ".$generarA."_CodigoMiembro bigint(20) DEFAULT NULL,
                                    ".$generarA."_PoblacionOrigen bigint(20) DEFAULT NULL, 
                                    ".$generarA."_EstadoDiligenciamiento bigint(20) DEFAULT NULL";
       

                }else{
                      $create_Lsql = "CREATE TABLE ".$BaseDatos.".".$generarA." (
                                ".$generarA."_ConsInte__b bigint(20) NOT NULL AUTO_INCREMENT,
                                ".$generarA."_FechaInsercion datetime DEFAULT NOW(),
                                ".$generarA."_Usuario bigint(20) DEFAULT NULL,
                                ".$generarA."_CodigoMiembro bigint(20) DEFAULT NULL,
                                ".$generarA."_PoblacionOrigen bigint(20) DEFAULT NULL,                                
                                ".$generarA."_IdLlamada varchar(50) DEFAULT NULL,                               
                                ".$generarA."_Sentido___b varchar(10) DEFAULT NULL,
                                ".$generarA."_Canal_____b varchar(20) DEFAULT NULL,
                                ".$generarA."_CantidadIntentos bigint(20) DEFAULT 0,

                                
                                ".$generarA."_LinkContenido varchar(500) DEFAULT NULL,
                                ".$generarA."_DetalleCanal varchar(255) DEFAULT NULL,
                                ".$generarA."_DatoContacto varchar(255) DEFAULT NULL,
                                ".$generarA."_Paso bigint(20) DEFAULT NULL,
                                ".$generarA."_Clasificacion smallint(5) DEFAULT NULL,

                                ".$generarA."_EstadoDiligenciamiento bigint(20) DEFAULT NULL,
                                ".$generarA."_UltiGest__b bigint(20) DEFAULT NULL , 
                                ".$generarA."_FecUltGes_b datetime DEFAULT NULL,
                                ".$generarA."_GesMasImp_b bigint(20) DEFAULT NULL,
                                ".$generarA."_FeGeMaIm__b datetime DEFAULT NULL";
                }

               

                
                               

                $pregun_Lsql = "SELECT PREGUN_ConsInte__b, PREGUN_Tipo______b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$idFormulario_Crear." AND (PREGUN_Tipo______b != 9  AND PREGUN_Tipo______b != 12  AND PREGUN_Tipo______b != 16 )";

                $res_PregunLsql = $mysqli->query($pregun_Lsql);
                
                while ($res = $res_PregunLsql->fetch_object()) {
                    /* recorremos todos los campos del guion */
                    if($res->PREGUN_Tipo______b == '5'){
                        /* es de tipo Fecha u Hora y toca ponerle dateTime */
                        $create_Lsql .= ",".$generarA."_C".$res->PREGUN_ConsInte__b." datetime DEFAULT NULL";
                    }

                    if($res->PREGUN_Tipo______b == '10'){
                        /* es de tipo Fecha u Hora y toca ponerle dateTime */
                        $create_Lsql .= ",".$generarA."_C".$res->PREGUN_ConsInte__b." datetime DEFAULT NULL";
                    }

                    if($res->PREGUN_Tipo______b == '3'){
                        /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                        $create_Lsql .= ",".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                    } 

                    if($res->PREGUN_Tipo______b == '6'){
                        /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                        $create_Lsql .= ",".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                    } 

                    if($res->PREGUN_Tipo______b == '13'){
                        /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                        $create_Lsql .= ",".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                    } 

                    if($res->PREGUN_Tipo______b == '14'){
                        /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                        $create_Lsql .= ",".$generarA."_C".$res->PREGUN_ConsInte__b." varchar(253) CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                    } 

                    if($res->PREGUN_Tipo______b == '11'){
                        /* la pregunta es Entero, Lista o Guion que guardan los Ids */
                        $create_Lsql .= ",".$generarA."_C".$res->PREGUN_ConsInte__b." bigint(20) DEFAULT NULL";
                    } 

                    if($res->PREGUN_Tipo______b == '4'){
                        /* la pregunta es Decimal */
                        $create_Lsql .= ",".$generarA."_C".$res->PREGUN_ConsInte__b."  double DEFAULT NULL";
                    } 

                    if($res->PREGUN_Tipo______b == '1' || $res->PREGUN_Tipo______b == '15'){
                        /* es de tipo Varchar */
                        $create_Lsql .= ",".$generarA."_C".$res->PREGUN_ConsInte__b." varchar(253) CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                    }

                    if($res->PREGUN_Tipo______b == '2'){
                        /* es de tipo Memo */
                        $create_Lsql .= ",".$generarA."_C".$res->PREGUN_ConsInte__b." longtext CHARSET utf8 COLLATE utf8_general_ci DEFAULT NULL";
                    }

                    if($res->PREGUN_Tipo______b == '8'){
                        /* es de tipo CheckBox */
                        $create_Lsql .= ",".$generarA."_C".$res->PREGUN_ConsInte__b." smallint(5) DEFAULT NULL";
                    }
                }

                $create_Lsql .= ",PRIMARY KEY (".$generarA."_ConsInte__b)) ENGINE=InnoDB AUTO_INCREMENT=0;";


                if($mysqli->query($create_Lsql) === true){
                    $Lsql_Editar_Guion = "UPDATE ".$BaseDatos_systema.".PREGUN SET PREGUN_FueGener_b = 0 WHERE PREGUN_ConsInte__GUION__b = ".$idFormulario_Crear;
                    $mysqli->query($Lsql_Editar_Guion);

                    generar_formularios_busquedas($idFormulario_Crear , $generar_formulario, $generar_busqueda, $generar_web_form);
                    //echo '1';
                }else{
                    echo $mysqli->error;
                }
            }else{
                generar_formularios_busquedas($idFormulario_Crear , $generar_formulario, $generar_busqueda, $generar_web_form);
            }
        }

    
    }


    include "generador.php";
?>