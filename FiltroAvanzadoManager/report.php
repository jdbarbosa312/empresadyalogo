<?php

require_once ("../conexion.php");
require_once ("../../carga/Excel.php");
require_once ("../../xlsxwriter/xlsxwriter.class.php");
include "languages/spanish.php";

$arrDataFiltros_t = [];


if (isset($_POST["jsonTotalFil"]) && $_POST["jsonTotalFil"] != "") {

    $jsonTotalFil = str_replace("\\", "", $_POST["jsonTotalFil"]);
    $arrTotalFil = (array)json_decode($jsonTotalFil);

    $jsonCondiciones = str_replace("\\", "", $_POST["jsonCondiciones"]);
    $arrCondiciones = (array)json_decode($jsonCondiciones);

    $arrDataFiltros_t["totalFiltros"]=$arrTotalFil;
    $arrDataFiltros_t["dataFiltros"]=[];

    foreach ($arrTotalFil as $key => $value) {

            array_push($arrDataFiltros_t["dataFiltros"], ["selCampo_".$value=>$arrCondiciones["selCampo_".$value],"selOperador_".$value=>$arrCondiciones["selOperador_".$value],"valor_".$value=>$arrCondiciones["valor_".$value],"tipo_".$value=>$arrCondiciones["tipo_".$value],"selCondicion_".$value=>(isset($arrCondiciones["selCondicion_".$value]) ? $arrCondiciones["selCondicion_".$value] : null)]);  

    }

}else{

    if (isset($_POST["totalFiltros"]) && $_POST["totalFiltros"] > 0) {

        $arrTotalFiltros_t = $_POST["totalFiltros"];

        $arrDataFiltros_t["totalFiltros"]=$_POST["totalFiltros"];
        $arrDataFiltros_t["dataFiltros"]=[];

        foreach ($arrTotalFiltros_t as $value) {

            array_push($arrDataFiltros_t["dataFiltros"], ["selCampo_".$value=>$_POST["selCampo_".$value],"selOperador_".$value=>$_POST["selOperador_".$value],"valor_".$value=>$_POST["valor_".$value],"tipo_".$value=>$_POST["tipo_".$value],"selCondicion_".$value=>(isset($_POST["selCondicion_".$value]) ? $_POST["selCondicion_".$value] : null)]);    

        }


    }
    
}


$tipoReport_t = (isset($_POST["tipoReport_t"]) ? $_POST["tipoReport_t"] : 0);
$strFechaIn_t = (isset($_POST["strFechaIn_t"]) ? $_POST["strFechaIn_t"] : date('Y-m-d'));
$strFechaFn_t = (isset($_POST["strFechaFn_t"]) ? $_POST["strFechaFn_t"] : date('Y-m-d'));
$intIdPeriodo_t = (isset($_POST["intIdPeriodo_t"]) ? $_POST["intIdPeriodo_t"] : 0);
$strLimit_t = (isset($_POST["strLimit_t"]) ? $_POST["strLimit_t"] : "no");
$intIdHuesped_t = (isset($_POST["intIdHuesped_t"]) ? $_POST["intIdHuesped_t"] : 0);
$intIdEstrat_t = (isset($_POST["intIdEstrat_t"]) ? $_POST["intIdEstrat_t"] : 0);
$intIdCBX_t = (isset($_POST["intIdCBX_t"]) ? $_POST["intIdCBX_t"] : 0);
$intIdBd_t = (isset($_POST["intIdBd_t"]) ? $_POST["intIdBd_t"] : 0);
$intIdPaso_t = (isset($_POST["intIdPaso_t"]) ? $_POST["intIdPaso_t"] : 0);
$intIdTipo_t = (isset($_POST["intIdTipo_t"]) ? $_POST["intIdTipo_t"] : 0);
$intIdMuestra_t = (isset($_POST["intIdMuestra_t"]) ? $_POST["intIdMuestra_t"] : -1);
$intIdGuion_t = (isset($_POST["intIdGuion_t"]) ? $_POST["intIdGuion_t"] : 0);
$intFilas_t = (isset($_POST["intFilas_t"]) ? $_POST["intFilas_t"] : 0);
$intLimite_t = (isset($_POST["intLimite_t"]) ? $_POST["intLimite_t"] : 10);
$intPaginaActual_t = (isset($_POST["intPaginaActual_t"]) ? $_POST["intPaginaActual_t"] : 1);
$strTablaTemp_t = (isset($_POST["strTablaTemp_t"]) ? $_POST["strTablaTemp_t"] : null);
$strCampoUnion_t = (isset($_POST["strCampoUnion_t"]) ? $_POST["strCampoUnion_t"] : null);
$strNombreExcell = (isset($_POST["NombreExcell"]) ? $_POST["NombreExcell"] : "");

if (isset($_POST["Exportar"])) {

    $arrDimensiones_t = dimensiones($tipoReport_t,$strFechaIn_t,$strFechaFn_t,$intIdPeriodo_t,0,$intIdHuesped_t,$intIdEstrat_t,$intIdCBX_t,$intIdBd_t,$intIdPaso_t,$intIdTipo_t,$intIdMuestra_t,$intIdGuion_t,0,0,"exportar",$arrDataFiltros_t);

    $resSQLReporte = $mysqli->query($arrDimensiones_t[3]." LIMIT 45000");
    
    $strNombreHoja_t = $arrDimensiones_t[4];

    $arrColumnas_t = $resSQLReporte->fetch_fields();

    $arrStyles = ['font'=>'Arial','font-size'=>10,'fill'=>'#20BEE8','color' => '#FFFF','halign'=>'center'];

    foreach ($arrColumnas_t as $name) {
        $arrHeader_t[$name->name] = "string"; 
    }

    $writer = new XLSXWriter();
    $writer->writeSheetHeader($strNombreHoja_t, $arrHeader_t, $arrStyles);

    while ($dato = $resSQLReporte->fetch_object()) {
        $writer->writeSheetRow($strNombreHoja_t,(array)$dato);
    }

    $strSQLNombreEstrat_t = "SELECT ESTRAT_Nombre____b AS nombre FROM ".$BaseDatos_systema.".ESTRAT WHERE ESTRAT_ConsInte__b = ".$intIdEstrat_t;
    $resSQLNombreEstrat_t = $mysqli->query($strSQLNombreEstrat_t);
    $strFechaActual_t = date("Y-m-d H:i:s");

    if ($resSQLNombreEstrat_t->num_rows > 0) {
        $objSQLNombreEstrat_t = $resSQLNombreEstrat_t->fetch_object();
        $strFileName_t = sanear_strings($strNombreHoja_t."_".$objSQLNombreEstrat_t->nombre).$strFechaActual_t;
    }else{
        $strFileName_t = "RESUMEN_CARGUE_DE_BASE_".$strFechaActual_t;
    }

    //JDBD generamos el escell
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$strFileName_t.'.xlsx"');
    header('Cache-Control: max-age=0');

    $writer->writeToStdOut();
    exit(0);


}else if (isset($_GET["Pivot"])) {

  $arrDimensiones_t = dimensiones($tipoReport_t,$strFechaIn_t,$strFechaFn_t,$intIdPeriodo_t,0,$intIdHuesped_t,$intIdEstrat_t,$intIdCBX_t,$intIdBd_t,$intIdPaso_t,$intIdTipo_t,$intIdMuestra_t,$intIdGuion_t,0,0,"pivot",$arrDataFiltros_t);

  $arrDatos_t = [];

  $resSQLReporte = $mysqli->query($arrDimensiones_t[3]." LIMIT 45000");

  if ($resSQLReporte) {
      if ($resSQLReporte->num_rows > 0) {
          while ($row = $resSQLReporte->fetch_assoc()) {
              $arrDatos_t[] = $row; 
          }
      }
  }

  echo json_encode($arrDatos_t);

}else if (isset($_GET["Paginado"])) {

    $intFilas_t = $_POST["intFilas_t"];
    $intLimit_t = 15;
    $intRegistrosTotal_t = $_POST["intRegistrosTotal_t"];
    $intCantidadPaginas_t = $_POST["intCantidadPaginas_t"];
    $intLimite_t = $_POST["intLimite_t"];

    $arrDimensiones_t = [$intRegistrosTotal_t,$intCantidadPaginas_t,$_POST["consulta"]." LIMIT ".$_POST["intFilas_t"].",".$intLimite_t,$_POST["consulta"]];

    $arrDimensiones_t[2] = str_replace("\\", "", $arrDimensiones_t[2]);
    $arrDimensiones_t[3] = str_replace("\\", "", $arrDimensiones_t[3]);

    $resSQLReporte_t = $mysqli->query($arrDimensiones_t[2]);

    $arrDatos_t = [];

    if ($resSQLReporte_t) {
        if ($resSQLReporte_t->num_rows > 0) {
            while ($row = $resSQLReporte_t->fetch_assoc()) {
                $arrDatos_t[] = $row; 
            }
        }
    }

    $strJsonDatos_t = json_encode($arrDatos_t);

    if (count($arrDatos_t) > 0) {

        $arrDatosHead_t = array_keys($arrDatos_t[0]);

        include "tables/grid.php";

    }else{
        echo 'No hay datos para mostrar!';
    }

}else if (isset($_GET["Reporte"])){

    $arrDimensiones_t = dimensiones($tipoReport_t,$strFechaIn_t,$strFechaFn_t,$intIdPeriodo_t,$strLimit_t,$intIdHuesped_t,$intIdEstrat_t,$intIdCBX_t,$intIdBd_t,$intIdPaso_t,$intIdTipo_t,$intIdMuestra_t,$intIdGuion_t,$intFilas_t,$intLimite_t,null,$arrDataFiltros_t);

    $arrDatos_t = [];

    $resSQLReporte = $mysqli->query($arrDimensiones_t[2]);

    if ($resSQLReporte) {
        if ($resSQLReporte->num_rows > 0) {
            while ($row = $resSQLReporte->fetch_assoc()) {
                $arrDatos_t[] = $row; 
            }
        }
    }

    $strJsonDatos_t = json_encode($arrDatos_t);

    if (count($arrDatos_t) > 0) {

        $arrDatosHead_t = array_keys($arrDatos_t[0]);

        include "tables/grid.php";
            


    }else{
        echo 'No hay datos para mostrar!';
    }

}else if (isset($_POST["ExportarCargue"])) {

    $arrDimensiones_t = dimensiones($tipoReport_t,$strFechaIn_t,$strFechaFn_t,$intIdPeriodo_t,0,$intIdHuesped_t,$intIdEstrat_t,$intIdCBX_t,$intIdBd_t,$intIdPaso_t,$intIdTipo_t,$intIdMuestra_t,$intIdGuion_t,0,0,"cargue",$arrDataFiltros_t);

    $strSQLReporte_t = $arrDimensiones_t[0];

    $strNombreHoja_t = $arrDimensiones_t[1];
    $arrDatos_t = [];
    $resSQLReporte = $mysqli->query($strSQLReporte_t);

    $arrColumnas_t = $resSQLReporte->fetch_fields();

    $arrStyles = ['font'=>'Arial','font-size'=>10,'fill'=>'#20BEE8','color' => '#FFFF','halign'=>'center'];

    foreach ($arrColumnas_t as $name) {
        $arrHeader_t[$name->name] = "string"; 
    }

    $writer = new XLSXWriter();
    $writer->writeSheetHeader($strNombreHoja_t, $arrHeader_t, $arrStyles);

    while ($dato = $resSQLReporte->fetch_object()) {
        $writer->writeSheetRow($strNombreHoja_t,(array)$dato);
    }

    $strSQLNombreEstrat_t = "SELECT ESTRAT_Nombre____b AS nombre FROM ".$BaseDatos_systema.".ESTRAT WHERE ESTRAT_ConsInte__b = ".$intIdEstrat_t;
    $resSQLNombreEstrat_t = $mysqli->query($strSQLNombreEstrat_t);
    $strFechaActual_t = date("Y-m-d H:i:s");

    if ($resSQLNombreEstrat_t->num_rows > 0) {
        $objSQLNombreEstrat_t = $resSQLNombreEstrat_t->fetch_object();
        $strFileName_t = sanear_strings($objSQLNombreEstrat_t->nombre).$strFechaActual_t;
    }else{
        $strFileName_t = "RESUMEN_CARGUE_DE_BASE_".$strFechaActual_t;
    }

    //JDBD generamos el escell
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$strNombreExcell.'_'.$strFileName_t.'.xlsx"');
    header('Cache-Control: max-age=0');

    $writer->writeToStdOut();
    exit(0);

}else{

    $arrDimensiones_t = dimensiones($tipoReport_t,$strFechaIn_t,$strFechaFn_t,$intIdPeriodo_t,0,$intIdHuesped_t,$intIdEstrat_t,$intIdCBX_t,$intIdBd_t,$intIdPaso_t,$intIdTipo_t,$intIdMuestra_t,$intIdGuion_t,0,0,"cargue",$arrDataFiltros_t);

    $strSQLReporte_t = $arrDimensiones_t[0];

    $strNombreHoja_t = $arrDimensiones_t[1];

    $arrDatos_t = [];
    $resSQLReporte = $mysqli->query($strSQLReporte_t);

    $arrDatos_t = [];

    if ($resSQLReporte) {
        if ($resSQLReporte->num_rows > 0) {
            while ($row = $resSQLReporte->fetch_assoc()) {
                $arrDatos_t[] = $row; 
            }
        }
    }

    $strJsonDatos_t = json_encode($arrDatos_t);

    if (count($arrDatos_t) > 0) {

        $arrDatosHead_t = array_keys($arrDatos_t[0]);

        if ($tipoReport_t == "cargue") {

            $resSQLErroresCargue_t = $mysqli->query("SELECT RESULTADO AS Errores, ".$strCampoUnion_t." AS CampoUnion FROM dy_cargue_datos.".$strTablaTemp_t." WHERE (ESTADO = 2 OR ESTADO = 3)");

            $arrDatosErrores_t = [];
            $arrHeadErrores_t = [];

            if ($resSQLErroresCargue_t && $resSQLErroresCargue_t->num_rows > 0) {

                while ($row2 = $resSQLErroresCargue_t->fetch_assoc()) {
                    $arrDatosErrores_t[] = $row2;
                }

                $arrHeadErrores_t = array_keys($arrDatosErrores_t[0]);
                

            }

            include "../../carga/tablaCargue.php";

        }else{

            include "tables/grid.php";
            
        }


    }else{
        echo 'No hay datos para mostrar!';
    }
}

function sanear_strings($string)
{

    // $string = utf8_decode($string);

    $string = str_replace(array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'), array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'), $string);
    $string = str_replace(array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'), array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'), $string);
    $string = str_replace(array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'), array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'), $string);
    $string = str_replace(array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'), array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'), $string);
    $string = str_replace(array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'), array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'), $string);
    $string = str_replace(array('ñ', 'Ñ', 'ç', 'Ç'), array('n', 'N', 'c', 'C'), $string);
    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(array("\\", "¨", "º", "-", "~", "#", "@", "|", "!", "\"", "·", "$", "%", "&", "/", "(", ")", "?", "'", "¡", "¿", "[", "^", "`", "]", "+", "}", "{", "¨", "´", ">“, “< ", ";", ",", ":", "."), '', $string);
    return $string;

}

/**
 *JDBD - En esta funcion retornaremos el nombre de la vista segun su ID de base.
 *@param int.
 *@return string. 
 */

function nombreVistas($intIdBd_p){

    global $mysqli;
    global $BaseDatos;
    global $BaseDatos_systema;
    global $BaseDatos_telefonia;
    global $dyalogo_canales_electronicos;
    global $BaseDatos_general;


    //JDBD - Buscamos el nombre de la vista MYSQL.
    $strSQLVista_t = "SELECT nombre FROM ".$BaseDatos_general.".vistas_generadas WHERE id_guion = ".$intIdBd_p." ORDER BY id DESC LIMIT 1"; 

    $resSQLVista_t = $mysqli->query($strSQLVista_t);

    return $resSQLVista_t->fetch_object()->nombre;


}

/**
 *JDBD - Esta funcion calcula la cantidad de registros y paginas del reporte solicitado.
 *@param .....
 *@return array. 
 */

function filasPaginas($intIdBd_p,$strCondicion_p,$intLimite_p,$tipoReport_p="default",$intIdMuestra_p=null,$intIdGuion_p=null){

    global $BaseDatos;
    global $mysqli;

    //JDBD - Consultamos las cantidades de registros y las paginas que resultarian para mostrar como reporte.

    switch ($tipoReport_p) {

      case '2':

        $strSQL_t = $intIdBd_p;

        break;

      case '1':

        $strSQL_t = $intIdBd_p;

        break;

      case 'bdpaso':

        $strSQL_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos.".G".$intIdBd_p." JOIN ".$BaseDatos.".G".$intIdBd_p."_M".$intIdMuestra_p." ON  G".$intIdBd_p."_ConsInte__b = G".$intIdBd_p."_M".$intIdMuestra_p."_CoInMiPo__b WHERE ".$strCondicion_p;

        break;

      case 'gspaso':

        $strSQL_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos.".G".$intIdGuion_p." JOIN ".$BaseDatos.".G".$intIdBd_p." ON G".$intIdGuion_p."_CodigoMiembro = G".$intIdBd_p."_ConsInte__b WHERE ".$strCondicion_p;

        break;

      case 'acd':

        $strSQL_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos.".".$intIdBd_p." WHERE ".$strCondicion_p;

        break;
      
      default:
      
        $strSQL_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos.".G".$intIdBd_p." WHERE ".$strCondicion_p;

        break;
    }

    $intCantidadFilas_t = $mysqli->query($strSQL_t)->fetch_object()->cantidad;

    $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

    return [$intCantidadFilas_t,$intCantidadPaginas];

}

/**
 *JDBD - En esta funcion armamos las condiciones de los filtros que recibimos.
 *@param .....
 *@return string. 
 */

function aliasColumna($strNombre_p){

  $strNombreCorregido_p = preg_replace("/\s+/", "_", $strNombre_p);

  $strNombreCorregido_p = preg_replace('([^0-9a-zA-Z_-])', "", $strNombreCorregido_p);

  $strNombreCorregido_p = strtoupper($strNombreCorregido_p);

  $strNombreCorregido_p = rtrim($strNombreCorregido_p,"_");

  $strNombreCorregido_p = substr($strNombreCorregido_p,0,40);

  return $strNombreCorregido_p;

}

/**
 *JDBD - En esta funcion armamos las condiciones de los filtros que recibimos
 *@param .....
 *@return array. 
 */

function columnasDinamicas($intIdBd_p,$strCampoEspecifico_p=null){

    global $mysqli;
    global $BaseDatos_systema;
    global $BaseDatos_general;

    $strSQLCamposDinamicos_t = "SELECT PREGUN_ConsInte__b AS ID, CONCAT('G',PREGUN_ConsInte__GUION__b,'_C',PREGUN_ConsInte__b) AS campoId , PREGUN_Texto_____b AS nombre, PREGUN_Tipo______b AS tipo, SECCIO_TipoSecc__b AS seccion FROM ".$BaseDatos_systema.".PREGUN LEFT JOIN ".$BaseDatos_systema.".SECCIO ON PREGUN_ConsInte__SECCIO_b = SECCIO_ConsInte__b WHERE PREGUN_ConsInte__GUION__b = ".$intIdBd_p." AND PREGUN_Tipo______b NOT IN (11,13,8,9,12,15,16) ORDER BY PREGUN_ConsInte__b DESC";

    $strCamposFinal_t = "";

    $resSQLCamposDinamicos_t = $mysqli->query($strSQLCamposDinamicos_t);

    $objCamposDinamicos_t = [];

    while ($obj = $resSQLCamposDinamicos_t->fetch_object()) {

      $objCamposDinamicos_t[] = $obj;

    }

    return $objCamposDinamicos_t;

}
/**
 *JDBD - En esta funcion armamos las condiciones de los filtros que recibimosm .....
 *@return array. 
 */

function columnasReporte($tipoReport_p,$intIdBd_p,$intIdMuestra_p,$intIdTipo_p=null,$intIdGuion_p=null){

    global $mysqli;
    global $BaseDatos_systema;
    global $BaseDatos_general;


    switch ($tipoReport_p) {

      case 'bd':

        $arrColumnasDinamicas_t = columnasDinamicas($intIdBd_p);

        $strColumnasFinal_t = "G".$intIdBd_p."_ConsInte__b AS ID_DY, G".$intIdBd_p."_FechaInsercion AS FECHA_CREACION_DY, ";

        $strColumnasDinamicas_t = "";

        foreach ($arrColumnasDinamicas_t as $value) {

          if ($value->tipo == "1" && $value->nombre == "ORIGEN_DY_WF") {

            $strColumnasFinal_t .= $value->campoId." AS ORIGEN_DY_WF, ";

            $strColumnasFinal_t .= "G".$intIdBd_p."_PasoGMI_b AS PASO_GMI, ".$BaseDatos_general.".fn_item_lisopc(G".$intIdBd_p."_EstadoGMI_b) AS ESTADO_GMI, ".$BaseDatos_general.".fn_item_monoef(G".$intIdBd_p."_GesMasImp_b) AS GESTION_MAS_IMPORTANTE, ".$BaseDatos_general.".fn_clasificacion_traduccion(G".$intIdBd_p."_ClasificacionGMI_b) AS CLASIFICACION_GMI, ".$BaseDatos_general.".fn_clasificacion_traduccion(G".$intIdBd_p."_TipoReintentoGMI_b) AS REINTENTO_GMI, G".$intIdBd_p."_FecHorAgeGMI_b AS FECHA_AGENDA_GMI, G".$intIdBd_p."_ComentarioGMI_b AS COMENTARIO_GMI, ".$BaseDatos_general.".fn_nombre_USUARI(G".$intIdBd_p."_UsuarioGMI_b) AS AGENTE_GMI, G".$intIdBd_p."_CanalGMI_b AS CANAL_GMI, G".$intIdBd_p."_SentidoGMI_b AS SENTIDO_GMI, G".$intIdBd_p."_FeGeMaIm__b AS FECHA_GMI, (G".$intIdBd_p."_FeGeMaIm__b-G".$intIdBd_p."_FechaInsercion) AS DIAS_MADURACION_GMI, ".$BaseDatos_general.".fn_nombre_paso(G".$intIdBd_p."_PasoUG_b) AS PASO_UG, ".$BaseDatos_general.".fn_item_lisopc(G".$intIdBd_p."_EstadoUG_b) AS ESTADO_UG, ".$BaseDatos_general.".fn_item_monoef(G".$intIdBd_p."_UltiGest__b) AS ULTIMA_GESTION, ".$BaseDatos_general.".fn_clasificacion_traduccion(G".$intIdBd_p."_ClasificacionUG_b) AS CLASIFICACION_UG, ".$BaseDatos_general.".fn_clasificacion_traduccion(G".$intIdBd_p."_TipoReintentoUG_b) AS REINTENTO_UG, G".$intIdBd_p."_FecHorAgeUG_b AS FECHA_AGENDA_UG, G".$intIdBd_p."_ComentarioUG_b AS COMENTARIO_UG, ".$BaseDatos_general.".fn_nombre_USUARI(G".$intIdBd_p."_UsuarioUG_b) AS AGENTE_UG, G".$intIdBd_p."_Canal_____b AS CANAL_UG, G".$intIdBd_p."_Sentido___b AS SENTIDO_UG, G".$intIdBd_p."_FecUltGes_b AS FECHA_UG, G".$intIdBd_p."_LinkContenidoUG_b AS CONTENIDO_UG, G".$intIdBd_p."_EstadoDiligenciamiento AS LLAVE_CARGUE, (DAYOFMONTH((G".$intIdBd_p."_FechaInsercion - G".$intIdBd_p."_FecUltGes_b)) - DAYOFMONTH((G".$intIdBd_p."_FechaInsercion - G".$intIdBd_p."_FeGeMaIm__b))) AS DIAS_SIN_CONTACTO, G".$intIdBd_p."_CantidadIntentos AS CANTIDAD_INTENTOS, G".$intIdBd_p."_LinkContenidoGMI_b AS CONTENIDO_GMI, ";

          }else{

            switch ($value->tipo) {

              case '6':

                $strColumnasDinamicas_t .= $BaseDatos_general.".fn_item_lisopc(".$value->campoId.") AS ".aliasColumna($value->nombre).", ";

                break;
              
              default:

                $strColumnasDinamicas_t .= $value->campoId." AS ".aliasColumna($value->nombre).", ";

                break;

            }

          }

        }

        $strColumnasDinamicas_t = substr($strColumnasDinamicas_t, 0, -2);

        $strColumnasFinal_t .= $strColumnasDinamicas_t;

        return $strColumnasFinal_t;

        break;

      case 'bdpaso':

        $arrColumnasDinamicas_t = columnasDinamicas($intIdBd_p);

        $strColumnasFinal_t = "G".$intIdBd_p."_ConsInte__b AS ID_DY, G".$intIdBd_p."_FechaInsercion AS FECHA_CREACION_DY, ";

        $strColumnasDinamicas_t = "";

        if ($intIdTipo_p == 7 || $intIdTipo_p == 8 || $intIdTipo_p == 10) {

            $strColumnasAdicionales_t = "(CASE G".$intIdBd_p."_M".$intIdMuestra_p."_Activo____b WHEN 0 THEN 'Inactivo' WHEN -1 THEN 'Activo' END) AS ACTIVO_DY, dyalogo_general.fn_tipo_reintento_traduccion(G".$intIdBd_p."_M".$intIdMuestra_p."_Estado____b) AS ESTADO_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_NumeInte__b AS NUMERO_INTENTOS_DY, ".$BaseDatos_general.".fn_item_monoef(G".$intIdBd_p."_M".$intIdMuestra_p."_UltiGest__b) AS ULTIMA_GESTION_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b AS FECHA_UG_DY, dyalogo_general.fn_clasificacion_traduccion(G".$intIdBd_p."_M".$intIdMuestra_p."_ConUltGes_b) AS CLASIFICACION_UG_DY, dyalogo_general.fn_nombre_USUARI(G".$intIdBd_p."_M".$intIdMuestra_p."_UsuarioUG_b) AS USUARIO_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_CanalUG_b AS CANAL_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_SentidoUG_b AS SENTIDO_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_LinkContenidoUG_b AS LINK_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_Comentari_b AS COMENTARIO_DY, ";

        }else{

            $strColumnasAdicionales_t = "(CASE G".$intIdBd_p."_M".$intIdMuestra_p."_Activo____b WHEN 0 THEN 'Inactivo' WHEN -1 THEN 'Activo' END) AS ACTIVO_DY, dyalogo_general.fn_tipo_reintento_traduccion(G".$intIdBd_p."_M".$intIdMuestra_p."_Estado____b) AS ESTADO_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_NumeInte__b AS NUMERO_INTENTOS_DY, ".$BaseDatos_general.".fn_item_monoef(G".$intIdBd_p."_M".$intIdMuestra_p."_GesMasImp_b) AS GESTION_MAS_IMPORTANTE_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_FeGeMaIm__b AS FECHA_GMI_DY, dyalogo_general.fn_clasificacion_traduccion(G".$intIdBd_p."_M".$intIdMuestra_p."_CoGesMaIm_b) AS CLASIFICACION_GMI_DY, dyalogo_general.fn_nombre_USUARI(G".$intIdBd_p."_M".$intIdMuestra_p."_UsuarioGMI_b) AS USUARIO_GMI_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_CanalGMI_b AS CANAL_GMI_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_SentidoGMI_b AS SENTIDO_GMI_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_LinkContenidoGMI_b AS LINK_GMI_DY, dyalogo_general.fn_tipo_reintento_traduccion(G".$intIdBd_p."_M".$intIdMuestra_p."_TipoReintentoGMI_b) AS TIPO_REINTENTO_GMI_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_CantidadIntentosGMI_b AS NUMERO_INTENTOS_GMI_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_ComentarioGMI_b AS COMENTARIO_GMI_DY, ".$BaseDatos_general.".fn_item_monoef(G".$intIdBd_p."_M".$intIdMuestra_p."_UltiGest__b) AS ULTIMA_GESTION_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b AS FECHA_UG_DY, dyalogo_general.fn_clasificacion_traduccion(G".$intIdBd_p."_M".$intIdMuestra_p."_ConUltGes_b) AS CLASIFICACION_UG_DY, dyalogo_general.fn_nombre_USUARI(G".$intIdBd_p."_M".$intIdMuestra_p."_UsuarioUG_b) AS USUARIO_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_CanalUG_b AS CANAL_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_SentidoUG_b AS SENTIDO_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_LinkContenidoUG_b AS LINK_UG_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_FecHorMinProGes__b AS FECHA_MINIMA_PROXIMO_INTENTO_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_Comentari_b AS COMENTARIO_DY, G".$intIdBd_p."_M".$intIdMuestra_p."_FecHorAge_b AS FECHA_AGENDA_DY, ";

        }

        foreach ($arrColumnasDinamicas_t as $value) {

          if ($value->tipo == "1" && $value->nombre == "ORIGEN_DY_WF") {

            $strColumnasFinal_t .= $value->campoId." AS ORIGEN_DY_WF, ";

          }else{

            switch ($value->tipo) {

              case '6':

                $strColumnasDinamicas_t .= $BaseDatos_general.".fn_item_lisopc(".$value->campoId.") AS ".aliasColumna($value->nombre).", ";

                break;
              
              default:

                $strColumnasDinamicas_t .= $value->campoId." AS ".aliasColumna($value->nombre).", ";

                break;

            }

          }

        }

        $strColumnasDinamicas_t = substr($strColumnasDinamicas_t, 0, -2);

        $strColumnasFinal_t .= $strColumnasAdicionales_t.$strColumnasDinamicas_t;

        return $strColumnasFinal_t;

        break;

      case 'bkpaso':

        $arrColumnasDinamicas_t = columnasDinamicas($intIdBd_p);

        $strColumnasFinal_t = "G".$intIdBd_p."_ConsInte__b AS ID_DY, G".$intIdBd_p."_FechaInsercion AS FECHA_CREACION_DY, ";

        $strColumnasDinamicas_t = "";

        foreach ($arrColumnasDinamicas_t as $value) {

            switch ($value->tipo) {

              case '6':

                $strColumnasDinamicas_t .= $BaseDatos_general.".fn_item_lisopc(".$value->campoId.") AS ".aliasColumna($value->nombre).", ";

                break;
              
              default:

                $strColumnasDinamicas_t .= $value->campoId." AS ".aliasColumna($value->nombre).", ";

                break;

            }


        }

        $strColumnasDinamicas_t = substr($strColumnasDinamicas_t, 0, -2);

        $strColumnasFinal_t .= $strColumnasDinamicas_t;

        return $strColumnasFinal_t;

        break;

      case 'gspaso':

        $arrColumnasDinamicas_t = columnasDinamicas($intIdGuion_p);

        $strColumnasFinal_t = "G".$intIdGuion_p."_ConsInte__b AS ID_DY, G".$intIdBd_p."_FechaInsercion AS FECHA_CREACION_DY, G".$intIdGuion_p."_CodigoMiembro AS ID_BD_DY, ";

        $arrGestionControl_t = ["tipificacion"=>"","reintento"=>"","feAgenda"=>"","observacion"=>"","fecha"=>""];

        $strColumnasDinamicas_t = "";

        foreach ($arrColumnasDinamicas_t as $value) {

            if ($value->seccion == "4" || $value->seccion == "3") {

              switch ($value->nombre) {

                case 'Tipificación':

                  $arrGestionControl_t["tipificacion"]=$BaseDatos_general.".fn_item_lisopc(".$value->campoId.") AS TIPIFICACIÓN_DY, ";

                  break;

                case 'Reintento':

                  $arrGestionControl_t["reintento"]=$BaseDatos_general.".fn_tipo_reintento_traduccion(".$value->campoId.") AS REINTENTO_DY, ";

                  break;

                case 'Hora Agenda':

                  $arrGestionControl_t["feAgenda"]=$value->campoId." AS FECHA_AGENDA_DY, ";

                  break;

                case 'Observacion':

                  $arrGestionControl_t["observacion"]=$value->campoId." AS OBSERVACION_DY, ";

                  break;

                case 'Fecha':

                  $arrGestionControl_t["fecha"]=$value->campoId." AS FECHA_GESTION_DY, ";

                  break;

              }

            }else{

              switch ($value->tipo) {

                case '6':

                  $strColumnasDinamicas_t .= $BaseDatos_general.".fn_item_lisopc(".$value->campoId.") AS ".aliasColumna($value->nombre).", ";

                  break;
                
                default:

                    $strColumnasDinamicas_t .= $value->campoId." AS ".aliasColumna($value->nombre).", ";                  

                  break;

              }

            }



        }

        $strColumnasFinal_t .= $arrGestionControl_t["fecha"]."DATE_FORMAT(G".$intIdGuion_p."_Duracion___b,'%H:%i:%S') AS DURACION_GESTION, TIME_TO_SEC(G".$intIdGuion_p."_Duracion___b) AS DURACION_GESTION_SEG, ".$BaseDatos_general.".fn_nombre_USUARI(G".$intIdGuion_p."_Usuario) AS AGENTE_DY, G".$intIdGuion_p."_Sentido___b AS SENTIDO, G".$intIdGuion_p."_Canal_____b AS CANAL, ".$arrGestionControl_t["tipificacion"].$BaseDatos_general.".fn_clasificacion_traduccion(G".$intIdGuion_p."_Clasificacion) AS CLASIFICACION_DY, ".$arrGestionControl_t["reintento"].$arrGestionControl_t["feAgenda"].$arrGestionControl_t["observacion"]."G".$intIdGuion_p."_LinkContenido AS DESCARGAGRABACION_DY, G".$intIdGuion_p."_Paso AS PASO_DY, G".$intIdGuion_p."_Origen_b AS ORIGEN_DY, ";

        $strColumnasDinamicas_t = substr($strColumnasDinamicas_t, 0, -2);

        $strColumnasFinal_t .= $strColumnasDinamicas_t;

        return $strColumnasFinal_t;

        break;

    }


}

/**
 *JDBD - En esta funcion se armara las condiciones dinamicamente.
 *@param .....
 *@return string. 
 */

function condicionesDinamicas($strJsonCondicion_p){

  $arrJsonCondicion_t = json_decode($strJsonCondicion_p);

  $strCondicionFinal_t = "";

  foreach ($arrJsonCondicion_t as $value) {

    $strCondicionFinal_t .= "(".$value.") AND ";

  }

  $strCondicionFinal_t = substr($strCondicionFinal_t, 0, -4);

  return $strCondicionFinal_t;

}

/**
 *JDBD - En esta funcion conseguiremos toda la informacion que se necesita para el reporte y su paginacion como lo es, la cantidad de registros, campos de la consulta ect.
 *@param .....
 *@return array. 
 */



function dimensiones($tipoReport_p,$strFechaIn_p,$strFechaFn_p,$intIdPeriodo_p,$strLimit_p,$intIdHuesped_p,$intIdEstrat_p,$intIdCBX_p,$intIdBd_p,$intIdPaso_p,$intIdTipo_p,$intIdMuestra_p,$intIdGuion_p,$intFilas_p,$intLimite_p,$strSolicitud_p=null,$arrDataFiltros_p){

    global $mysqli;
    global $BaseDatos;
    global $BaseDatos_systema;
    global $BaseDatos_telefonia;
    global $dyalogo_canales_electronicos;
    global $BaseDatos_general;
    global $arrCamposMuestra_t;
    global $arrCamposMuestra2_t;

    switch ($tipoReport_p) {

        case 'cargue':

            $strNombreHoja_t = "RESUMEN CARGUE";

            $strSQLVista_t = "SELECT nombre FROM ".$BaseDatos_general.".vistas_generadas WHERE id_guion = ".$intIdGuion_p." ORDER BY id DESC LIMIT 1"; 


            $resSQLVista_t = $mysqli->query($strSQLVista_t);
            $objSQLVista_t = $resSQLVista_t->fetch_object();

            if ($intIdMuestra_p == -1) {

                $strSQLReporte_t = "SELECT * FROM ".$BaseDatos.".".$objSQLVista_t->nombre." WHERE LLAVE_CARGUE = ".$strFechaIn_p." ORDER BY ID DESC LIMIT 45000";

            }else{
                
                $strSQLReporte_t = "SELECT ".$BaseDatos_general.".fn_tipo_reintento_traduccion(B.G".$intIdGuion_p."_M".$intIdMuestra_p."_Estado____b) AS ESTADO, ".$BaseDatos_general.".fn_nombre_USUARI(B.G".$intIdGuion_p."_M".$intIdMuestra_p."_ConIntUsu_b) AS AGENTE_ASIGNADO, A.* FROM ".$BaseDatos.".".$objSQLVista_t->nombre." A JOIN ".$BaseDatos.".G".$intIdGuion_p."_M".$intIdMuestra_p." B ON A.ID = B.G".$intIdGuion_p."_M".$intIdMuestra_p."_CoInMiPo__b WHERE A.LLAVE_CARGUE = ".$strFechaIn_p." ORDER BY A.ID DESC LIMIT 45000";


            }

            $arrReporte_t[0] = $strSQLReporte_t;
            $arrReporte_t[1] = $strNombreHoja_t;

            return $arrReporte_t;

            break;

        case 'acd':

            //JDBD - traemos las vitas ACD de la campaña.

            $strCondicion_t = "DATE(Fecha) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }

            
            $strIntervalo_t = "%ACD_DIA%";
            $strCampoIntervalo_t = "";

            if ($intIdPeriodo_p == "2") {

                    $strCampoIntervalo_t = ", Intervalo";
                    $strIntervalo_t = "%ACD_HORA%";

            }

            $strSQLACD_t = "SELECT nombre FROM ".$BaseDatos_general.".vistas_generadas JOIN ".$BaseDatos_systema.".CAMPAN ON id_campan = CAMPAN_ConsInte__b WHERE CAMPAN_IdCamCbx__b = ".$intIdCBX_p." AND nombre LIKE '".$strIntervalo_t."'";

            $resSQLACD_t = $mysqli->query($strSQLACD_t);


            $strSQLReporte_t = "SELECT 'NO HAY REPORTE ACD DISPONIBLE' AS ERROR";
            $strSQLReporteNoLimit_t = "SELECT 'NO HAY REPORTE ACD DISPONIBLE' AS ERROR";

            if ($resSQLACD_t->num_rows > 0) {

                $strNombreACD_t = $resSQLACD_t->fetch_object()->nombre;

                $arrFilasPaginas_t = filasPaginas($strNombreACD_t,$strCondicion_t,$intLimite_p,'acd');

                $strSQLReporte_t = "SELECT Fecha".$strCampoIntervalo_t.", Estrategia, TSF_Tiempo, TSF_porcentaje, Ofrecidas, Contestadas, Cont_antes_tsf, Cont_despues_tsf, Aban_despues_tsf, ROUND(Cont_porcentaje,2) AS Cont_porcentaje, ROUND(TSF,2) AS TSF, ROUND(TSF_Cont_antes_tsf,2) AS TSF_Cont_antes_tsf, ROUND(TSF_Cont_despues_tsf,2) AS TSF_Cont_despues_tsf, ASA, ASAMin, ASAMax, TSA, SUBSTR(AHT,1,8) AS AHT, SUBSTR(THT,1,8) AS THT, Aban, Aban_antes_tsf, ROUND(Aban_porcentaje,2) AS Aban_porcentaje, ROUND(Aban_umbral_tsf,2) AS Aban_umbral_tsf,  SUBSTR(Aban_espera,1,8) AS Aban_espera, SUBSTR(Aban_espera_total,1,8) AS Aban_espera_total, SUBSTR(Aban_espera_min,1,8) AS Aban_espera_min, SUBSTR(Aban_espera_max,1,8) AS Aban_espera_max FROM ".$BaseDatos.".".$strNombreACD_t." WHERE ".$strCondicion_t." ORDER BY Fecha DESC LIMIT 0,".$intLimite_p;

                $strSQLReporteNoLimit_t = "SELECT Fecha".$strCampoIntervalo_t.", Estrategia, TSF_Tiempo, TSF_porcentaje, Ofrecidas, Contestadas, Cont_antes_tsf, Cont_despues_tsf, Aban_despues_tsf, ROUND(Cont_porcentaje,2) AS Cont_porcentaje, ROUND(TSF,2) AS TSF, ROUND(TSF_Cont_antes_tsf,2) AS TSF_Cont_antes_tsf, ROUND(TSF_Cont_despues_tsf,2) AS TSF_Cont_despues_tsf, ASA, ASAMin, ASAMax, TSA, SUBSTR(AHT,1,8) AS AHT, SUBSTR(THT,1,8) AS THT, Aban, Aban_antes_tsf, ROUND(Aban_porcentaje,2) AS Aban_porcentaje, ROUND(Aban_umbral_tsf,2) AS Aban_umbral_tsf,  SUBSTR(Aban_espera,1,8) AS Aban_espera, SUBSTR(Aban_espera_total,1,8) AS Aban_espera_total, SUBSTR(Aban_espera_min,1,8) AS Aban_espera_min, SUBSTR(Aban_espera_max,1,8) AS Aban_espera_max FROM ".$BaseDatos.".".$strNombreACD_t." WHERE ".$strCondicion_t." ORDER BY Fecha DESC ";

            }

            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"ACD"];

            break;

        case 'bkpaso':

            $strCondicion_t = "DATE(G".$intIdBd_p."_FechaInsercion) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }

            $arrFilasPaginas_t = filasPaginas($intIdBd_p,$strCondicion_t,$intLimite_p,'bkpaso');

            $strColumnasDinamicas_t = columnasReporte("bkpaso",$intIdBd_p,$intIdMuestra_p);

            $strSQLReporte_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdBd_p." WHERE ".$strCondicion_t." ORDER BY G".$intIdBd_p."_ConsInte__b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdBd_p." WHERE ".$strCondicion_t." ORDER BY G".$intIdBd_p."_ConsInte__b DESC ";

            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"BACKOFFICE"];

            break;

        case 'bdpaso':

            $strCondicion_t = "DATE(G".$intIdBd_p."_FechaInsercion) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($intIdTipo_p == 7 || $intIdTipo_p == 8 || $intIdTipo_p == 10) {

                $strCondicion_t = "DATE(G".$intIdBd_p."_M".$intIdMuestra_p."_FecUltGes_b) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            }

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }

            $arrFilasPaginas_t = filasPaginas($intIdBd_p,$strCondicion_t,$intLimite_p,'bdpaso',$intIdMuestra_p);

            $strColumnasDinamicas_t = columnasReporte("bdpaso",$intIdBd_p,$intIdMuestra_p,$intIdTipo_p);

            $strSQLReporte_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdBd_p." JOIN ".$BaseDatos.".G".$intIdBd_p."_M".$intIdMuestra_p." ON G".$intIdBd_p."_ConsInte__b = G".$intIdBd_p."_M".$intIdMuestra_p."_CoInMiPo__b WHERE ".$strCondicion_t." ORDER BY G".$intIdBd_p."_ConsInte__b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdBd_p." JOIN ".$BaseDatos.".G".$intIdBd_p."_M".$intIdMuestra_p." ON G".$intIdBd_p."_ConsInte__b = G".$intIdBd_p."_M".$intIdMuestra_p."_CoInMiPo__b WHERE ".$strCondicion_t." ORDER BY G".$intIdBd_p."_ConsInte__b DESC ";

            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"RESUMEN BASE"];

            break;

        case 'gspaso':

            $strCondicion_t = "DATE(G".$intIdGuion_p."_FechaInsercion) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }

            $arrFilasPaginas_t = filasPaginas($intIdBd_p,$strCondicion_t,$intLimite_p,'gspaso',null,$intIdGuion_p);

            $strColumnasDinamicas_t = columnasReporte("gspaso",$intIdBd_p,$intIdMuestra_p,null,$intIdGuion_p);

            $strSQLReporte_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdGuion_p." LEFT JOIN ".$BaseDatos.".G".$intIdBd_p." ON G".$intIdGuion_p."_CodigoMiembro = G".$intIdBd_p."_ConsInte__b WHERE ".$strCondicion_t." ORDER BY G".$intIdGuion_p."_ConsInte__b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdGuion_p." LEFT JOIN ".$BaseDatos.".G".$intIdBd_p." ON G".$intIdGuion_p."_CodigoMiembro = G".$intIdBd_p."_ConsInte__b WHERE ".$strCondicion_t." ORDER BY G".$intIdGuion_p."_ConsInte__b DESC ";

            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"GESTIONES"];

            break;

        case '1':

            $strSQLPaso_t = "SELECT ESTPAS_ConsInte__CAMPAN_b AS id FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_p;
            $resSQLPaso_t = $mysqli->query($strSQLPaso_t);
            $strIdCampan_t = '';
            $i = 0;
            
            while ($row = $resSQLPaso_t->fetch_object()) {
                if (!is_null($row->id)) {
                    $strIdCampan_t .= $row->id.",";
                    $i++;
                }
            }

            $strIdCampan_t = substr($strIdCampan_t, 0, -1);

            $strCondicion_t = "DATE(fecha_hora_inicio) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }

            $strSQLReporte_t = "SELECT agente_nombre as Agente, DATE_FORMAT(fecha_hora_inicio,'%Y-%m-%d') as Inicio, DATE_FORMAT(fecha_hora_inicio,'%H') as Inicio_hora, DATE_FORMAT(fecha_hora_inicio,'%i') as Inicio_minuto, DATE_FORMAT(fecha_hora_fin,'%Y-%m-%d') as Fin, DATE_FORMAT(fecha_hora_fin,'%H') as Fin_hora, DATE_FORMAT(fecha_hora_fin,'%i') as Fin_minuto, DATE_FORMAT(SEC_TO_TIME(duracion),'%H:%i:%s') as Duracion_Horas FROM ".$BaseDatos_telefonia.".dy_v_historico_sesiones WHERE agente_id IN(SELECT id FROM ".$BaseDatos_telefonia.".dy_agentes join ".$BaseDatos_systema.".USUARI ON id_usuario_asociado = USUARI_UsuaCBX___b join ".$BaseDatos_systema.".ASITAR on USUARI_ConsInte__b = ASITAR_ConsInte__USUARI_b WHERE ASITAR_ConsInte__CAMPAN_b in(".$strIdCampan_t.")) AND ".$strCondicion_t." ORDER BY fecha_hora_inicio DESC LIMIT 0,".$intLimite_p;


            $strSQLReporteNoLimit_t = "SELECT agente_nombre as Agente, DATE_FORMAT(fecha_hora_inicio,'%Y-%m-%d') as Inicio, DATE_FORMAT(fecha_hora_inicio,'%H') as Inicio_hora, DATE_FORMAT(fecha_hora_inicio,'%i') as Inicio_minuto, DATE_FORMAT(fecha_hora_fin,'%Y-%m-%d') as Fin, DATE_FORMAT(fecha_hora_fin,'%H') as Fin_hora, DATE_FORMAT(fecha_hora_fin,'%i') as Fin_minuto, DATE_FORMAT(SEC_TO_TIME(duracion),'%H:%i:%s') as Duracion_Horas FROM ".$BaseDatos_telefonia.".dy_v_historico_sesiones WHERE agente_id IN(SELECT id FROM ".$BaseDatos_telefonia.".dy_agentes join ".$BaseDatos_systema.".USUARI ON id_usuario_asociado = USUARI_UsuaCBX___b join ".$BaseDatos_systema.".ASITAR on USUARI_ConsInte__b = ASITAR_ConsInte__USUARI_b WHERE ASITAR_ConsInte__CAMPAN_b in(".$strIdCampan_t.")) AND ".$strCondicion_t." ORDER BY fecha_hora_inicio DESC ";

            $strSQLReportePaginas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_telefonia.".dy_v_historico_sesiones WHERE agente_id IN(SELECT id FROM ".$BaseDatos_telefonia.".dy_agentes join ".$BaseDatos_systema.".USUARI ON id_usuario_asociado = USUARI_UsuaCBX___b join ".$BaseDatos_systema.".ASITAR on USUARI_ConsInte__b = ASITAR_ConsInte__USUARI_b WHERE ASITAR_ConsInte__CAMPAN_b in(".$strIdCampan_t.")) AND ".$strCondicion_t." ORDER BY fecha_hora_inicio DESC ";

            $arrFilasPaginas_t = filasPaginas($strSQLReportePaginas_t,$strCondicion_t,$intLimite_p,'1');

            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"SESIONES"];

            break;

        case '2':

            $strSQLPaso_t = "SELECT ESTPAS_ConsInte__CAMPAN_b AS id FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_p;
            $resSQLPaso_t = $mysqli->query($strSQLPaso_t);
            $strIdCampan_t = '';
            $i = 0;

            while ($row = $resSQLPaso_t->fetch_object()) {
                if (!is_null($row->id)) {
                    $strIdCampan_t .= $row->id.",";
                    $i++;
                }
            }

            $strIdCampan_t = substr($strIdCampan_t, 0, -1);

            $strCondicion_t = "DATE(fecha_hora_inicio) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }

            $strSQLReporte_t = "SELECT agente_nombre as Agente, DATE_FORMAT(fecha_hora_inicio,'%Y-%m-%d') as Inicio, DATE_FORMAT(fecha_hora_inicio,'%H') as Inicio_hora, DATE_FORMAT(fecha_hora_inicio,'%i') as Inicio_minuto, DATE_FORMAT(fecha_hora_fin,'%Y-%m-%d') as Fin, DATE_FORMAT(fecha_hora_fin,'%H') as Fin_hora, DATE_FORMAT(fecha_hora_fin,'%i') as Fin_minuto, DATE_FORMAT(SEC_TO_TIME(duracion),'%H:%i:%s') as Duracion_Horas, tipo_descanso_nombre as TipoPausa, comentario AS Comentario FROM ".$BaseDatos_telefonia.".dy_v_historico_descansos WHERE agente_id IN(SELECT id FROM ".$BaseDatos_telefonia.".dy_agentes join ".$BaseDatos_systema.".USUARI ON id_usuario_asociado = USUARI_UsuaCBX___b join ".$BaseDatos_systema.".ASITAR on USUARI_ConsInte__b = ASITAR_ConsInte__USUARI_b WHERE ASITAR_ConsInte__CAMPAN_b in(".$strIdCampan_t.")) AND ".$strCondicion_t." ORDER BY fecha_hora_inicio DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "SELECT agente_nombre as Agente, DATE_FORMAT(fecha_hora_inicio,'%Y-%m-%d') as Inicio, DATE_FORMAT(fecha_hora_inicio,'%H') as Inicio_hora, DATE_FORMAT(fecha_hora_inicio,'%i') as Inicio_minuto, DATE_FORMAT(fecha_hora_fin,'%Y-%m-%d') as Fin, DATE_FORMAT(fecha_hora_fin,'%H') as Fin_hora, DATE_FORMAT(fecha_hora_fin,'%i') as Fin_minuto, DATE_FORMAT(SEC_TO_TIME(duracion),'%H:%i:%s') as Duracion_Horas, tipo_descanso_nombre as TipoPausa, comentario AS Comentario FROM ".$BaseDatos_telefonia.".dy_v_historico_descansos WHERE agente_id IN(SELECT id FROM ".$BaseDatos_telefonia.".dy_agentes join ".$BaseDatos_systema.".USUARI ON id_usuario_asociado = USUARI_UsuaCBX___b join ".$BaseDatos_systema.".ASITAR on USUARI_ConsInte__b = ASITAR_ConsInte__USUARI_b WHERE ASITAR_ConsInte__CAMPAN_b in(".$strIdCampan_t.")) AND ".$strCondicion_t." ORDER BY fecha_hora_inicio DESC ";

            $strSQLReportePaginas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_telefonia.".dy_v_historico_descansos WHERE agente_id IN(SELECT id FROM ".$BaseDatos_telefonia.".dy_agentes join ".$BaseDatos_systema.".USUARI ON id_usuario_asociado = USUARI_UsuaCBX___b join ".$BaseDatos_systema.".ASITAR on USUARI_ConsInte__b = ASITAR_ConsInte__USUARI_b WHERE ASITAR_ConsInte__CAMPAN_b in(".$strIdCampan_t.")) AND ".$strCondicion_t." ORDER BY fecha_hora_inicio DESC ";

            $arrFilasPaginas_t = filasPaginas($strSQLReportePaginas_t,$strCondicion_t,$intLimite_p,'2');

            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"PAUSAS"];

            break;

        case '3':

            //JDBD - Consultamos la cantidad de registros a retornar.
            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes LEFT JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE (NOT (HoraInicialDefinida is null)) AND HoraInicialDefinida < date_format(current_time, '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." AND qrySesionesDelDia.agente_id IS NULL Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, HoraInicialDefinida FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes LEFT JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE (NOT (HoraInicialDefinida is null)) AND HoraInicialDefinida < date_format(current_time, '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." AND qrySesionesDelDia.agente_id IS NULL Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, HoraInicialDefinida FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes LEFT JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE (NOT (HoraInicialDefinida is null)) AND HoraInicialDefinida < date_format(current_time, '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." AND qrySesionesDelDia.agente_id IS NULL Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"NO REGISTRARON"];

            break;
        case '4':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraInicio > HoraInicialDefinida AND USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(HoraInicio, HoraInicialDefinida), '%H:%i:%S') as Retraso, HoraInicialDefinida, HoraInicio as HoraInicialReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraInicio > HoraInicialDefinida AND USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(HoraInicio, HoraInicialDefinida), '%H:%i:%S') as Retraso, HoraInicialDefinida, HoraInicio as HoraInicialReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraInicio > HoraInicialDefinida AND USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"LLEGARON TARDE"];

            break;
        case '5':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraInicio <= HoraInicialDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, HoraInicialDefinida, HoraInicio as HoraInicialReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraInicio <= HoraInicialDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, HoraInicialDefinida, HoraInicio as HoraInicialReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraInicio <= HoraInicialDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"LLEGARON A TIEMPO"];

            break;
        case '6':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraFin < HoraFinalDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(HoraFinalDefinida, HoraFin), '%H:%i:%S') as TiempoFaltante, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraFin < HoraFinalDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(HoraFinalDefinida, HoraFin), '%H:%i:%S') as TiempoFaltante, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraFin < HoraFinalDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"SE FUERON ANTES"];

            break;
        case '7':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraFin >= HoraFinalDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraFin >= HoraFinalDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  HoraFin >= HoraFinalDefinida and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"SE FUERON A TIEMPO"];

            break;
        case '8':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  timediff(HoraFinalDefinida, HoraInicialDefinida) > date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p."  Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(timediff(HoraFinalDefinida, HoraInicialDefinida), timediff(HoraFin, HoraInicio)), '%H:%i:%S') as TiempoFaltante, date_format(timediff(HoraFinalDefinida, HoraInicialDefinida), '%H:%i:%S') as DuracionDefinida, date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') as DuracionReal, HoraInicialDefinida, HoraInicio as HoraInicialReal, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  timediff(HoraFinalDefinida, HoraInicialDefinida) > date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p."  Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(timediff(HoraFinalDefinida, HoraInicialDefinida), timediff(HoraFin, HoraInicio)), '%H:%i:%S') as TiempoFaltante, date_format(timediff(HoraFinalDefinida, HoraInicialDefinida), '%H:%i:%S') as DuracionDefinida, date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') as DuracionReal, HoraInicialDefinida, HoraInicio as HoraInicialReal, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  timediff(HoraFinalDefinida, HoraInicialDefinida) > date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p."  Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"SESIONES CORTAS"];

            break;
        case '9':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  timediff(HoraFinalDefinida, HoraInicialDefinida) <= date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(HoraFinalDefinida, HoraInicialDefinida), '%H:%i:%S') as DuracionDefinida, date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') as DuracionReal, HoraInicialDefinida, HoraInicio as HoraInicialReal, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  timediff(HoraFinalDefinida, HoraInicialDefinida) <= date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, date_format(timediff(HoraFinalDefinida, HoraInicialDefinida), '%H:%i:%S') as DuracionDefinida, date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') as DuracionReal, HoraInicialDefinida, HoraInicio as HoraInicialReal, HoraFinalDefinida, HoraFin as HoraFinalReal FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes JOIN ".$BaseDatos_systema.".qrySesionesDelDia ON IdAgente = qrySesionesDelDia.agente_id WHERE  timediff(HoraFinalDefinida, HoraInicialDefinida) <= date_format(timediff(HoraFin, HoraInicio), '%H:%i:%S') and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"SESIONES DURACION OK"];

            break;
        case '10':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and timediff(HoraFinalProgramada, HoraInicialProgramada) < timediff(fecha_hora_fin,fecha_hora_inicio) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, timediff(timediff(fecha_hora_fin,fecha_hora_inicio), timediff(HoraFinalProgramada, HoraInicialProgramada)) as Exceso, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and timediff(HoraFinalProgramada, HoraInicialProgramada) < timediff(fecha_hora_fin,fecha_hora_inicio) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, timediff(timediff(fecha_hora_fin,fecha_hora_inicio), timediff(HoraFinalProgramada, HoraInicialProgramada)) as Exceso, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and timediff(HoraFinalProgramada, HoraInicialProgramada) < timediff(fecha_hora_fin,fecha_hora_inicio) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasConHorarioMuyLargas"];

            break;
        case '11':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and timediff(HoraFinalProgramada, HoraInicialProgramada) >= timediff(fecha_hora_fin,fecha_hora_inicio) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, timediff(timediff(HoraFinalProgramada, HoraInicialProgramada), timediff(fecha_hora_fin,fecha_hora_inicio)) as TiempoAFavor, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and timediff(HoraFinalProgramada, HoraInicialProgramada) >= timediff(fecha_hora_fin,fecha_hora_inicio) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, timediff(timediff(HoraFinalProgramada, HoraInicialProgramada), timediff(fecha_hora_fin,fecha_hora_inicio)) as TiempoAFavor, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and timediff(HoraFinalProgramada, HoraInicialProgramada) >= timediff(fecha_hora_fin,fecha_hora_inicio) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasConHorarioDuracionOk"];

            break;
        case '12':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and (HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S') or date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, IF(HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S'), timediff(HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S')),null) as SalioAntesPor, IF(date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada, timediff(date_format(fecha_hora_fin , '%H:%i:%S'), HoraFinalProgramada),null) as LlegoTardePor, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal, timediff(timediff(fecha_hora_fin,fecha_hora_inicio), timediff(HoraFinalProgramada, HoraInicialProgramada)) as TiempoDiferencia, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and (HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S') or date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, IF(HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S'), timediff(HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S')),null) as SalioAntesPor, IF(date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada, timediff(date_format(fecha_hora_fin , '%H:%i:%S'), HoraFinalProgramada),null) as LlegoTardePor, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal, timediff(timediff(fecha_hora_fin,fecha_hora_inicio), timediff(HoraFinalProgramada, HoraInicialProgramada)) as TiempoDiferencia, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and (HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S') or date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasConHorarioIncumplidas"];

            break;
        case '13':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and (HoraInicialProgramada <= date_format(fecha_hora_inicio, '%H:%i:%S') and date_format(fecha_hora_fin , '%H:%i:%S') <= HoraFinalProgramada) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, IF(HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S'),timediff(HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S')),null) as SalioAntesPor, IF(date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada, timediff(date_format(fecha_hora_fin, '%H:%i:%S'), HoraFinalProgramada),null) as LlegoTardePor, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal, timediff(timediff(fecha_hora_fin,fecha_hora_inicio), timediff(HoraFinalProgramada, HoraInicialProgramada)) as TiempoDiferencia, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and (HoraInicialProgramada <= date_format(fecha_hora_inicio, '%H:%i:%S') and date_format(fecha_hora_fin , '%H:%i:%S') <= HoraFinalProgramada) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, IF(HoraInicialProgramada > date_format(fecha_hora_inicio, '%H:%i:%S'),timediff(HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S')),null) as SalioAntesPor, IF(date_format(fecha_hora_fin , '%H:%i:%S') > HoraFinalProgramada, timediff(date_format(fecha_hora_fin, '%H:%i:%S'), HoraFinalProgramada),null) as LlegoTardePor, HoraInicialProgramada, date_format(fecha_hora_inicio, '%H:%i:%S') as HoraInicialReal, HoraFinalProgramada, date_format(fecha_hora_fin , '%H:%i:%S') as HoraFinalReal, timediff(timediff(fecha_hora_fin,fecha_hora_inicio), timediff(HoraFinalProgramada, HoraInicialProgramada)) as TiempoDiferencia, timediff(HoraFinalProgramada, HoraInicialProgramada) DuracionProgramada, timediff(fecha_hora_fin,fecha_hora_inicio) as DuracionReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 1 and (HoraInicialProgramada <= date_format(fecha_hora_inicio, '%H:%i:%S') and date_format(fecha_hora_fin , '%H:%i:%S') <= HoraFinalProgramada) and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasConHorarioCumplidas"];

            break;
        case '14':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and USUPAU_Tipo_b = 0 group by USUARI_ConsInte__b having time_to_sec(DuracionMaxima) < sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, timediff(sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))), DuracionMaxima) as Exceso, DuracionMaxima, sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))) as DuracionReal, count(USUARI_ConsInte__b) as CantidadReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and USUPAU_Tipo_b = 0 group by USUARI_ConsInte__b having time_to_sec(DuracionMaxima) < sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, timediff(sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))), DuracionMaxima) as Exceso, DuracionMaxima, sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))) as DuracionReal, count(USUARI_ConsInte__b) as CantidadReal FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and USUPAU_Tipo_b = 0 group by USUARI_ConsInte__b having time_to_sec(DuracionMaxima) < sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasSinHorarioMuyLargas"];

            break;
        case '15':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 0 and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." group by USUARI_ConsInte__b having CantidadMaxima < count(USUARI_ConsInte__b) Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, count(USUARI_ConsInte__b) - CantidadMaxima as VecesDeMas, count(USUARI_ConsInte__b) as CantidadReal, CantidadMaxima, sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))) as DuracionReal, DuracionMaxima FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 0 and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." group by USUARI_ConsInte__b having CantidadMaxima < count(USUARI_ConsInte__b) Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, count(USUARI_ConsInte__b) - CantidadMaxima as VecesDeMas, count(USUARI_ConsInte__b) as CantidadReal, CantidadMaxima, sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))) as DuracionReal, DuracionMaxima FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUPAU_Tipo_b = 0 and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." group by USUARI_ConsInte__b having CantidadMaxima < count(USUARI_ConsInte__b) Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasSinHorarioMuchasVeces"];

            break;
        case '16':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and USUPAU_Tipo_b = 0 group by USUARI_ConsInte__b having time_to_sec(DuracionMaxima) >= sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio))) and CantidadMaxima >= count(USUARI_ConsInte__b) Order by USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))) as DuracionReal, DuracionMaxima, count(USUARI_ConsInte__b) as CantidadReal, CantidadMaxima FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and USUPAU_Tipo_b = 0 group by USUARI_ConsInte__b having time_to_sec(DuracionMaxima) >= sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio))) and CantidadMaxima >= count(USUARI_ConsInte__b) Order by USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo, tipo_descanso_nombre as Pausa, sec_to_time(sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio)))) as DuracionReal, DuracionMaxima, count(USUARI_ConsInte__b) as CantidadReal, CantidadMaxima FROM ".$BaseDatos_systema.".qryPausasProgramadasDelDia left join dyalogo_telefonia.dy_v_historico_descansos ON IdAgente = agente_id and USUPAU_PausasId_b = tipo_descanso_id WHERE fecha_hora_inicio >= current_date() and USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and USUPAU_Tipo_b = 0 group by USUARI_ConsInte__b having time_to_sec(DuracionMaxima) >= sum(time_to_sec(timediff(fecha_hora_fin,fecha_hora_inicio))) and CantidadMaxima >= count(USUARI_ConsInte__b) Order by USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"PausasSinHorarioOK"];

            break;
        case '17':

            $strSQLCantidadFilas_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes WHERE USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and HoraInicialDefinida IS NULL Order By USUARI_Nombre____b";

            $intCantidadFilas_t = $mysqli->query($strSQLCantidadFilas_t)->fetch_object()->cantidad;

            $intCantidadPaginas = ceil($intCantidadFilas_t/$intLimite_p);

            $strSQLReporte_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes WHERE USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and HoraInicialDefinida IS NULL Order By USUARI_Nombre____b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "select USUARI_ConsInte__b as Id, USUARI_Nombre____b as Nombre, USUARI_Correo___b as Correo FROM ".$BaseDatos_systema.".qryUSUARI_usuarios_agentes WHERE USUARI_ConsInte__PROYEC_b = ".$intIdHuesped_p." and HoraInicialDefinida IS NULL Order By USUARI_Nombre____b DESC ";

            return [$intCantidadFilas_t,$intCantidadPaginas,$strSQLReporte_t,$strSQLReporteNoLimit_t,"AGENTES SIN MALLA DEFINIDA"];

            break;

        default:  

            $strCondicion_t = "DATE(G".$intIdBd_p."_FechaInsercion) BETWEEN '".$strFechaIn_p."' AND '".$strFechaFn_p."'";

            if ($arrDataFiltros_p["totalFiltros"]>0) {

                $strCondicion_t = "";

                foreach ($arrDataFiltros_p["totalFiltros"] as $ite => $value) {


                    $strCondicion_t .=armarCondicion($arrDataFiltros_p["dataFiltros"][$ite]["selCampo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selOperador_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["valor_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["tipo_".$value],$arrDataFiltros_p["dataFiltros"][$ite]["selCondicion_".$value]);

                }

            }

            $arrFilasPaginas_t = filasPaginas($intIdBd_p,$strCondicion_t,$intLimite_p);

            $strColumnasDinamicas_t = columnasReporte("bd",$intIdBd_p,$intIdMuestra_p);

            $strSQLReporte_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdBd_p." WHERE ".$strCondicion_t." ORDER BY G".$intIdBd_p."_ConsInte__b DESC LIMIT 0,".$intLimite_p;

            $strSQLReporteNoLimit_t = "SELECT ".$strColumnasDinamicas_t." FROM ".$BaseDatos.".G".$intIdBd_p." WHERE ".$strCondicion_t." ORDER BY G".$intIdBd_p."_ConsInte__b DESC ";

            return [$arrFilasPaginas_t[0],$arrFilasPaginas_t[1],$strSQLReporte_t,$strSQLReporteNoLimit_t,"RESUMEN BASE GENERAL"];

            break;


    }

}

/**
 *JDBD - En esta funcion armamos las condiciones de los filtros que recibimos.
 *@param .....
 *@return string. 
 */

function armarCondicion($strCampo_p,$strOperador_p,$strValor_p,$strTipo_p,$strCondicion_p){


    switch ($strTipo_p) {
        case '5':

            return $strCondicion_p." DATE(".$strCampo_p.") ".$strOperador_p." '".$strValor_p."' ";

            break;
                    
        default:

            $strOperadorFinal_t  = "LIKE";

            switch ($strOperador_p) {

                case 'LIKE_1':

                    $strValorFinal_t = "'".$strValor_p."%'";

                    break;
                case 'LIKE_2':

                    $strValorFinal_t = "'%".$strValor_p."%'";

                    break;

                case 'LIKE_3':

                    $strValorFinal_t = "'%".$strValor_p."'";

                    break;
                default:

                    $strValorFinal_t = "'".$strValor_p."'";
                    $strOperadorFinal_t = $strOperador_p;

                    break;            

                }

            return $strCondicion_p." ".$strCampo_p." ".$strOperadorFinal_t." ".$strValorFinal_t." ";

            break;
    }

}