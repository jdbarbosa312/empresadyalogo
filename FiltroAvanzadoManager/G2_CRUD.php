<?php
    session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."../../../../pages/conexion.php");
    include(__DIR__."../../../../global/WSCoreClient.php");



    function pregun($intSeccio_p,$intBd_p){

        global $mysqli;
        global $BaseDatos_systema;

        $strCampo_t = "Fecha";

        $strIdPregun_t = "";

        if ($intSeccio_p == "3") {

            $strCampo_t = "Tipificación";

        }

        $strSQL_t = "SELECT PREGUN_ConsInte__b AS id FROM ".$BaseDatos_systema.".PREGUN JOIN ".$BaseDatos_systema.".SECCIO ON PREGUN_ConsInte__SECCIO_b = SECCIO_ConsInte__b WHERE PREGUN_ConsInte__GUION__b = ".$intBd_p." AND SECCIO_TipoSecc__b = ".$intSeccio_p." AND PREGUN_Texto_____b = '".$strCampo_t."'";

        if ($resSQL_t = $mysqli->query($strSQL_t)) {

            if ($resSQL_t->num_rows > 0) {

                $strIdPregun_t = "G".$intBd_p."_C".$resSQL_t->fetch_object()->id;

            }

        }

        return $strIdPregun_t;

    }

    function guardar_auditoria($accion, $superAccion) {
        
        global $mysqli;
        global $BaseDatos_systema;

        $str_Lsql = "INSERT INTO " . $BaseDatos_systema . ".AUACAD (AUACAD_Fecha_____b , AUACAD_Hora______b, AUACAD_Ejecutor__b, AUACAD_TipoAcci__b , AUACAD_SubTipAcc_b, AUACAD_Accion____b , AUACAD_Huesped___b ) VALUES ('" . date('Y-m-d H:s:i') . "', '" . date('Y-m-d H:s:i') . "', " . $_SESSION['IDENTIFICACION'] . ", 'G10', '" . $accion . "', '" .$mysqli->real_escape_string($superAccion). "', " . $_SESSION['HUESPED'] . " );";
        $mysqli->query($str_Lsql);

    }

    include(__DIR__."../../../../global/funcionesGenerales.php");
    include(__DIR__."/../reporteador.php");


    /**
     *JDBD - Em esta funcion se le aplicara un formato para el nombre de la columna.
     *@param .....
     *@return string. 
     */

    function aliasColumna($strNombre_p){

      $strNombreCorregido_p = preg_replace("/\s+/", "_", $strNombre_p);

      $strNombreCorregido_p = preg_replace('([^0-9a-zA-Z_-])', "", $strNombreCorregido_p);

      $strNombreCorregido_p = strtoupper($strNombreCorregido_p);

      $strNombreCorregido_p = rtrim($strNombreCorregido_p,"_");

      $strNombreCorregido_p = substr($strNombreCorregido_p,0,40);

      return $strNombreCorregido_p;

    }


    /**
     *JDBD - En esta funcion se retornara toda la informacion en columnas dinamicas del reporte.
     *@param .....
     *@return obj. 
     */

    function columnasDinamicas($intIdBd_p){

        global $mysqli;
        global $BaseDatos_systema;
        global $BaseDatos_general;

        $strSQLCamposDinamicos_t = "SELECT PREGUN_ConsInte__b AS ID, CONCAT('G',PREGUN_ConsInte__GUION__b,'_C',PREGUN_ConsInte__b) AS campoId , PREGUN_Texto_____b AS nombre, PREGUN_Tipo______b AS tipo, SECCIO_TipoSecc__b AS seccion FROM ".$BaseDatos_systema.".PREGUN LEFT JOIN ".$BaseDatos_systema.".SECCIO ON PREGUN_ConsInte__SECCIO_b = SECCIO_ConsInte__b WHERE PREGUN_ConsInte__GUION__b = ".$intIdBd_p." AND PREGUN_Tipo______b NOT IN (11,9,12,16) ORDER BY PREGUN_ConsInte__b ASC";

        $arrCampos_t = [];

        $objSQLCamposDinamicos_t = $mysqli->query($strSQLCamposDinamicos_t);

        while ($obj = $objSQLCamposDinamicos_t->fetch_object()) {

            $arrCampos_t[] = ["ID"=>$obj->ID,"campoId"=>$obj->campoId,"nombre"=>aliasColumna($obj->nombre),"tipo"=>$obj->tipo,"seccion"=>$obj->seccion];

        }

        return $arrCampos_t;
        

    }

    if (isset($_GET["filtroAvanzadoJSON"])) {

        echo json_encode($_POST);

    }

    if (isset($_GET["traerOpcionesLista"])) {

        $intIdCampo_t = $_POST["intIdCampo_t"];
        $strEspecial_t = $_POST["strEspecial_t"];

        if ($strEspecial_t) {

            if ($strEspecial_t == "estado") {

                $strSQLLista_t = "SELECT LISOPC_ConsInte__b AS id, LISOPC_Nombre____b AS nombre FROM ".$BaseDatos_systema.".LISOPC  JOIN ".$BaseDatos_systema.".OPCION ON LISOPC_ConsInte__OPCION_b = OPCION_ConsInte__b WHERE OPCION_Nombre____b = 'ESTADO_DY_".$intIdCampo_t."' ORDER BY LISOPC_ConsInte__b ASC";

                $resSQLLista_t = $mysqli->query($strSQLLista_t);

                if ($resSQLLista_t->num_rows > 0) {

                    while ($obj = $resSQLLista_t->fetch_object()) {

                        echo '<option value="'.$obj->id.'">'.aliasColumna($obj->nombre).'</option>';

                    }

                }

            }else if ($strEspecial_t == "monoef") {

                $strSQLLista_t = "SELECT LISOPC_Clasifica_b AS id, LISOPC_Nombre____b AS nombre, OP.CAMPAN_Nombre____b AS campana FROM ".$BaseDatos_systema.".LISOPC JOIN (SELECT OPCION_ConsInte__b, CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN JOIN ".$BaseDatos_systema.".OPCION ON CAMPAN_ConsInte__GUION__Gui_b = OPCION_ConsInte__GUION__b  WHERE CAMPAN_ConsInte__GUION__Pob_b = ".$intIdCampo_t." AND OPCION_Nombre____b LIKE 'Tipificaciones - %' GROUP BY CAMPAN_ConsInte__GUION__Gui_b) AS OP ON LISOPC_ConsInte__OPCION_b = OP.OPCION_ConsInte__b";

                $resSQLLista_t = $mysqli->query($strSQLLista_t);

                if ($resSQLLista_t->num_rows > 0) {

                    while ($obj = $resSQLLista_t->fetch_object()) {

                        echo '<option value="'.$obj->id.'">'.aliasColumna($obj->nombre." - ".$obj->campana).'</option>';

                    }

                }

            }else if ($strEspecial_t == "usu") {

                $strSQLLista_t = "SELECT ASITAR_ConsInte__USUARI_b AS id, USUARI_Nombre____b AS nombre FROM ".$BaseDatos_systema.".ASITAR JOIN (SELECT CAMPAN_ConsInte__b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__GUION__Pob_b = ".$intIdCampo_t.") AS CAM ON ASITAR_ConsInte__CAMPAN_b = CAM.CAMPAN_ConsInte__b JOIN ".$BaseDatos_systema.".USUARI ON ASITAR_ConsInte__USUARI_b = USUARI_ConsInte__b GROUP BY ASITAR_ConsInte__USUARI_b";

                $resSQLLista_t = $mysqli->query($strSQLLista_t);

                if ($resSQLLista_t->num_rows > 0) {

                    while ($obj = $resSQLLista_t->fetch_object()) {

                        echo '<option value="'.$obj->id.'">'.aliasColumna($obj->nombre).'</option>';

                    }

                }

            }


        }else{

            $strSQLLista_t = "SELECT LISOPC_ConsInte__b AS id, LISOPC_Nombre____b AS nombre from ".$BaseDatos_systema.".LISOPC JOIN ".$BaseDatos_systema.".PREGUN ON LISOPC_ConsInte__OPCION_b = PREGUN_ConsInte__OPCION_B WHERE PREGUN_ConsInte__b = ".$intIdCampo_t."";

            $resSQLLista_t = $mysqli->query($strSQLLista_t);

            if ($resSQLLista_t->num_rows > 0) {

                while ($obj = $resSQLLista_t->fetch_object()) {

                    echo '<option value="'.$obj->id.'">'.aliasColumna($obj->nombre).'</option>';

                }

            }

        }

    }


    if (isset($_GET["traerCamposDelReporte"])) {

        error_reporting(0);

        $tipoReport_t = $_POST["tipoReport_t"];
        $intIdHuesped_t = $_POST["intIdHuesped_t"];
        $intIdEstrat_t = $_POST["intIdEstrat_t"];
        $intIdBd_t = $_POST["intIdBd_t"];
        $intIdPaso_t = $_POST["intIdPaso_t"];
        $intIdTipo_t = $_POST["intIdTipo_t"];
        $intIdGuion_t = $_POST["intIdGuion_t"];
        $intIdCBX_t = $_POST["intIdCBX_t"];
        $intIdPeriodo_t = $_POST["intIdPeriodo_t"];
        $intIdMuestra_t = $_POST["intIdMuestra_t"];

        $arrCampos_t = [];
        $arrCamposTemp_t = [];

        switch ($tipoReport_t) {

            case 'pausas';

                $arrCampos_t[0]=["campoId"=>"fecha_hora_inicio","nombre"=>"Inicio","tipo"=>"5"];

                echo json_encode($arrCampos_t);                 

                break;

            case '2';

                $arrCampos_t[0]=["campoId"=>"agente_nombre","nombre"=>"Agente","tipo"=>"1"];
                $arrCampos_t[1]=["campoId"=>"fecha_hora_inicio","nombre"=>"Inicio","tipo"=>"5"];
                $arrCampos_t[2]=["campoId"=>"fecha_hora_fin","nombre"=>"Fin","tipo"=>"5"];
                $arrCampos_t[3]=["campoId"=>"tipo_descanso_nombre","nombre"=>"TipoPausa","tipo"=>"1"];
                $arrCampos_t[4]=["campoId"=>"comentario","nombre"=>"Comentario","tipo"=>"1"];

                echo json_encode($arrCampos_t);                 

                break;

            case '1';

                $arrCampos_t[0]=["campoId"=>"agente_nombre","nombre"=>"Agente","tipo"=>"1"];
                $arrCampos_t[1]=["campoId"=>"fecha_hora_inicio","nombre"=>"Inicio","tipo"=>"5"];
                $arrCampos_t[2]=["campoId"=>"fecha_hora_fin","nombre"=>"Fin","tipo"=>"5"];

                echo json_encode($arrCampos_t);                 

                break;

            case 'acd';

                if ($intIdPeriodo_t == "1") {

                    $arrCampos_t[0]=["campoId"=>"Fecha","nombre"=>"Fecha","tipo"=>"5"];
                    $arrCampos_t[1]=["campoId"=>"Estrategia","nombre"=>"Estrategia","tipo"=>"1"];
                    $arrCampos_t[2]=["campoId"=>"TSF_Tiempo","nombre"=>"TSF_Tiempo","tipo"=>"3"];
                    $arrCampos_t[3]=["campoId"=>"TSF_porcentaje","nombre"=>"TSF_porcentaje","tipo"=>"3"];
                    $arrCampos_t[4]=["campoId"=>"Ofrecidas","nombre"=>"Ofrecidas","tipo"=>"3"];
                    $arrCampos_t[5]=["campoId"=>"Contestadas","nombre"=>"Contestadas","tipo"=>"3"];
                    $arrCampos_t[6]=["campoId"=>"Cont_antes_tsf","nombre"=>"Cont_antes_tsf","tipo"=>"3"];
                    $arrCampos_t[7]=["campoId"=>"Cont_despues_tsf","nombre"=>"Cont_despues_tsf","tipo"=>"3"];
                    $arrCampos_t[8]=["campoId"=>"Aban_despues_tsf","nombre"=>"Aban_despues_tsf","tipo"=>"3"];
                    $arrCampos_t[9]=["campoId"=>"Cont_porcentaje","nombre"=>"Cont_porcentaje","tipo"=>"1"];
                    $arrCampos_t[10]=["campoId"=>"TSF","nombre"=>"TSF","tipo"=>"4"];
                    $arrCampos_t[11]=["campoId"=>"TSF_Cont_antes_tsf","nombre"=>"TSF_Cont_antes_tsf","tipo"=>"4"];
                    $arrCampos_t[12]=["campoId"=>"TSF_Cont_despues_tsf","nombre"=>"TSF_Cont_despues_tsf","tipo"=>"4"];
                    $arrCampos_t[13]=["campoId"=>"ASA","nombre"=>"ASA","tipo"=>"10"];
                    $arrCampos_t[14]=["campoId"=>"ASAMin","nombre"=>"ASAMin","tipo"=>"10"];
                    $arrCampos_t[15]=["campoId"=>"ASAMax","nombre"=>"ASAMax","tipo"=>"10"];
                    $arrCampos_t[16]=["campoId"=>"TSA","nombre"=>"TSA","tipo"=>"10"];
                    $arrCampos_t[17]=["campoId"=>"AHT","nombre"=>"AHT","tipo"=>"10"];
                    $arrCampos_t[18]=["campoId"=>"THT","nombre"=>"THT","tipo"=>"10"];
                    $arrCampos_t[19]=["campoId"=>"Aban","nombre"=>"Aban","tipo"=>"3"];
                    $arrCampos_t[20]=["campoId"=>"Aban_antes_tsf","nombre"=>"Aban_antes_tsf","tipo"=>"3"];
                    $arrCampos_t[21]=["campoId"=>"Aban_porcentaje","nombre"=>"Aban_porcentaje","tipo"=>"4"];
                    $arrCampos_t[22]=["campoId"=>"Aban_umbral_tsf","nombre"=>"Aban_umbral_tsf","tipo"=>"4"];
                    $arrCampos_t[23]=["campoId"=>"Aban_espera","nombre"=>"Aban_espera","tipo"=>"10"];
                    $arrCampos_t[24]=["campoId"=>"Aban_espera_total","nombre"=>"Aban_espera_total","tipo"=>"10"];
                    $arrCampos_t[25]=["campoId"=>"Aban_espera_min","nombre"=>"Aban_espera_min","tipo"=>"10"];
                    $arrCampos_t[26]=["campoId"=>"Aban_espera_max","nombre"=>"Aban_espera_max","tipo"=>"10"];
                    $arrCampos_t[27]=["campoId"=>"transfer","nombre"=>"transfer","tipo"=>"3"];

                }else if ($intIdPeriodo_t == "2") {

                    $arrCampos_t[0]=["campoId"=>"Fecha","nombre"=>"Fecha","tipo"=>"5"];
                    $arrCampos_t[1]=["campoId"=>"Intervalo","nombre"=>"Intervalo","tipo"=>"1"];
                    $arrCampos_t[2]=["campoId"=>"Estrategia","nombre"=>"Estrategia","tipo"=>"1"];
                    $arrCampos_t[3]=["campoId"=>"TSF_Tiempo","nombre"=>"TSF_Tiempo","tipo"=>"3"];
                    $arrCampos_t[4]=["campoId"=>"TSF_porcentaje","nombre"=>"TSF_porcentaje","tipo"=>"3"];
                    $arrCampos_t[5]=["campoId"=>"Ofrecidas","nombre"=>"Ofrecidas","tipo"=>"3"];
                    $arrCampos_t[6]=["campoId"=>"Contestadas","nombre"=>"Contestadas","tipo"=>"3"];
                    $arrCampos_t[7]=["campoId"=>"Cont_antes_tsf","nombre"=>"Cont_antes_tsf","tipo"=>"3"];
                    $arrCampos_t[8]=["campoId"=>"Cont_despues_tsf","nombre"=>"Cont_despues_tsf","tipo"=>"3"];
                    $arrCampos_t[9]=["campoId"=>"Aban_despues_tsf","nombre"=>"Aban_despues_tsf","tipo"=>"3"];
                    $arrCampos_t[10]=["campoId"=>"Cont_porcentaje","nombre"=>"Cont_porcentaje","tipo"=>"1"];
                    $arrCampos_t[11]=["campoId"=>"TSF","nombre"=>"TSF","tipo"=>"4"];
                    $arrCampos_t[12]=["campoId"=>"TSF_Cont_antes_tsf","nombre"=>"TSF_Cont_antes_tsf","tipo"=>"4"];
                    $arrCampos_t[13]=["campoId"=>"TSF_Cont_despues_tsf","nombre"=>"TSF_Cont_despues_tsf","tipo"=>"4"];
                    $arrCampos_t[14]=["campoId"=>"ASA","nombre"=>"ASA","tipo"=>"10"];
                    $arrCampos_t[15]=["campoId"=>"ASAMin","nombre"=>"ASAMin","tipo"=>"10"];
                    $arrCampos_t[16]=["campoId"=>"ASAMax","nombre"=>"ASAMax","tipo"=>"10"];
                    $arrCampos_t[17]=["campoId"=>"TSA","nombre"=>"TSA","tipo"=>"10"];
                    $arrCampos_t[18]=["campoId"=>"AHT","nombre"=>"AHT","tipo"=>"10"];
                    $arrCampos_t[19]=["campoId"=>"THT","nombre"=>"THT","tipo"=>"10"];
                    $arrCampos_t[20]=["campoId"=>"Aban","nombre"=>"Aban","tipo"=>"3"];
                    $arrCampos_t[21]=["campoId"=>"Aban_antes_tsf","nombre"=>"Aban_antes_tsf","tipo"=>"3"];
                    $arrCampos_t[22]=["campoId"=>"Aban_porcentaje","nombre"=>"Aban_porcentaje","tipo"=>"4"];
                    $arrCampos_t[23]=["campoId"=>"Aban_umbral_tsf","nombre"=>"Aban_umbral_tsf","tipo"=>"4"];
                    $arrCampos_t[24]=["campoId"=>"Aban_espera","nombre"=>"Aban_espera","tipo"=>"10"];
                    $arrCampos_t[25]=["campoId"=>"Aban_espera_total","nombre"=>"Aban_espera_total","tipo"=>"10"];
                    $arrCampos_t[26]=["campoId"=>"Aban_espera_min","nombre"=>"Aban_espera_min","tipo"=>"10"];
                    $arrCampos_t[27]=["campoId"=>"Aban_espera_max","nombre"=>"Aban_espera_max","tipo"=>"10"];

                }




                echo json_encode($arrCampos_t); 
                

                break;
            case 'bd':

                $arrCamposDinamicos_t = columnasDinamicas($intIdBd_t);

                $arrCampos_t[0]=["campoId"=>"G".$intIdBd_t."_ConsInte__b","nombre"=>"ID_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                $arrCampos_t[1]=["campoId"=>"G".$intIdBd_t."_FechaInsercion","nombre"=>"FECHA_CREACION_DY","tipo"=>"5","idbg"=>$intIdBd_t];
                $arrCampos_t[2]=["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FechaInsercion,'%Y')","nombre"=>"ANHO_CREACION_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                $arrCampos_t[3]=["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FechaInsercion,'%m')","nombre"=>"MES_CREACION_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                $arrCampos_t[4]=["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FechaInsercion,'%d')","nombre"=>"DIA_CREACION_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                $arrCampos_t[5]=["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FechaInsercion,'%H')","nombre"=>"HORA_CREACION_DY","tipo"=>"3","idbg"=>$intIdBd_t];

                foreach ($arrCamposDinamicos_t as $value) {

                    if ($value["tipo"] == "1" && $value["nombre"] == "ORIGEN_DY_WF") {

                        $arrCampos_t[] = ["campoId"=>$value["campoId"],"nombre"=>"ORIGEN_DY_WF","tipo"=>"1","idbg"=>$intIdBd_t,"idpregun"=>$value["ID"]];

                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_PasoGMI_b","nombre"=>"PASO_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_EstadoGMI_b","nombre"=>"ESTADO_GMI_DY","tipo"=>"estado","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_GesMasImp_b","nombre"=>"GESTION_MAS_IMPORTANTE_DY","tipo"=>"monoef","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_ClasificacionGMI_b","nombre"=>"CLASIFICACION_GMI_DY","tipo"=>"clasi","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_TipoReintentoGMI_b","nombre"=>"REINTENTO_GMI_DY","tipo"=>"clasi","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_FecHorAgeGMI_b","nombre"=>"FECHA_AGENDA_GMI_DY","tipo"=>"5","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FecHorAgeGMI_b,'%Y')","nombre"=>"ANHO_AGENDA_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FecHorAgeGMI_b,'%m')","nombre"=>"MES_AGENDA_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FecHorAgeGMI_b,'%d')","nombre"=>"DIA_AGENDA_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FecHorAgeGMI_b,'%H')","nombre"=>"HORA_AGENDA_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_ComentarioGMI_b","nombre"=>"COMENTARIO_GMI_DY","tipo"=>"1","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_UsuarioGMI_b","nombre"=>"AGENTE_GMI_DY","tipo"=>"usu","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_CanalGMI_b","nombre"=>"CANAL_GMI_DY","tipo"=>"canal","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_SentidoGMI_b","nombre"=>"SENTIDO_GMI_DY","tipo"=>"sentido","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_FeGeMaIm__b","nombre"=>"FECHA_GMI_DY","tipo"=>"5","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FeGeMaIm__b,'%Y')","nombre"=>"ANHO_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FeGeMaIm__b,'%m')","nombre"=>"MES_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FeGeMaIm__b,'%d')","nombre"=>"DIA_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FeGeMaIm__b,'%H')","nombre"=>"HORA_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"(G".$intIdBd_t."_FeGeMaIm__b-G".$intIdBd_t."_FechaInsercion)","nombre"=>"DIAS_MADURACION_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_PasoUG_b","nombre"=>"PASO_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_EstadoUG_b","nombre"=>"ESTADO_UG_DY","tipo"=>"estado","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_UltiGest__b","nombre"=>"ULTIMA_GESTION_DY","tipo"=>"monoef","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_ClasificacionUG_b","nombre"=>"CLASIFICACION_UG_DY","tipo"=>"clasi","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_TipoReintentoUG_b","nombre"=>"REINTENTO_UG_DY","tipo"=>"clasi","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_FecHorAgeUG_b","nombre"=>"FECHA_AGENDA_UG_DY","tipo"=>"5","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FecHorAgeUG_b,'%Y')","nombre"=>"ANHO_AGENDA_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FecHorAgeUG_b,'%m')","nombre"=>"MES_AGENDA_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FecHorAgeUG_b,'%d')","nombre"=>"DIA_AGENDA_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FecHorAgeUG_b,'%H')","nombre"=>"HORA_AGENDA_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_ComentarioUG_b","nombre"=>"COMENTARIO_UG_DY","tipo"=>"1","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_UsuarioUG_b","nombre"=>"AGENTE_UG_DY","tipo"=>"usu","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_Canal_____b","nombre"=>"CANAL_UG_DY","tipo"=>"canal","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_Sentido___b","nombre"=>"SENTIDO_UG_DY","tipo"=>"sentido","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_FecUltGes_b","nombre"=>"FECHA_UG_DY","tipo"=>"5","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FecUltGes_b,'%Y')","nombre"=>"ANHO_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FecUltGes_b,'%m')","nombre"=>"MES_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FecUltGes_b,'%d')","nombre"=>"DIA_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FecUltGes_b,'%H')","nombre"=>"HORA_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_LinkContenidoUG_b","nombre"=>"CONTENIDO_UG_DY","tipo"=>"1","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_EstadoDiligenciamiento","nombre"=>"LLAVE_CARGUE_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_CantidadIntentos","nombre"=>"CANTIDAD_INTENTOS_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                        $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_LinkContenidoGMI_b","nombre"=>"CONTENIDO_GMI_DY","tipo"=>"1","idbg"=>$intIdBd_t];



                    }else{

                        $arrCamposTemp_t[] = ["campoId"=>$value["campoId"],"nombre"=>$value["nombre"],"tipo"=>$value["tipo"],"idbg"=>$intIdBd_t,"idpregun"=>$value["ID"]];


                    }

                }

                foreach ($arrCamposTemp_t as $value) {

                    array_push($arrCampos_t,$value);
                    
                }       


                echo json_encode($arrCampos_t);         

                break;

            case 'bdpaso':

                $arrCamposDinamicos_t = columnasDinamicas($intIdBd_t);

                $arrCampos_t[0]=["campoId"=>"G".$intIdBd_t."_ConsInte__b","nombre"=>"ID_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                $arrCampos_t[1]=["campoId"=>"G".$intIdBd_t."_FechaInsercion","nombre"=>"FECHA_CREACION_DY","tipo"=>"5","idbg"=>$intIdBd_t];
                $arrCampos_t[2]=["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FechaInsercion,'%Y')","nombre"=>"ANHO_CREACION_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                $arrCampos_t[3]=["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FechaInsercion,'%m')","nombre"=>"MES_CREACION_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                $arrCampos_t[4]=["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FechaInsercion,'%d')","nombre"=>"DIA_CREACION_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                $arrCampos_t[5]=["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FechaInsercion,'%H')","nombre"=>"HORA_CREACION_DY","tipo"=>"3","idbg"=>$intIdBd_t];

                foreach ($arrCamposDinamicos_t as $value) {

                    if ($value["tipo"] == "1" && $value["nombre"] == "ORIGEN_DY_WF") {

                        $arrCampos_t[] = ["campoId"=>$value["campoId"],"nombre"=>"ORIGEN_DY_WF","tipo"=>"1","idbg"=>$intIdBd_t,"idpregun"=>$value["ID"]];

                        if ($intIdTipo_t == 7 || $intIdTipo_t == 8 || $intIdTipo_t == 10) {

                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_Activo____b","nombre"=>"ACTIVO_DY","tipo"=>"activo","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_Estado____b","nombre"=>"ESTADO_DY","tipo"=>"estado","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_NumeInte__b","nombre"=>"NUMERO_INTENTOS_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_UltiGest__b","nombre"=>"ULTIMA_GESTION_DY","tipo"=>"monoef","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_FecUltGes_b","nombre"=>"FECHA_UG_DY","tipo"=>"5","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FecUltGes_b,'%Y')","nombre"=>"ANHO_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FecUltGes_b,'%m')","nombre"=>"MES_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FecUltGes_b,'%d')","nombre"=>"DIA_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FecUltGes_b,'%H')","nombre"=>"HORA_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_ConUltGes_b","nombre"=>"CLASIFICACION_UG_DY","tipo"=>"clasi","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_UsuarioUG_b","nombre"=>"USUARIO_UG_DY","tipo"=>"usu","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_CanalUG_b","nombre"=>"CANAL_UG_DY","tipo"=>"canal","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_SentidoUG_b","nombre"=>"SENTIDO_UG_DY","tipo"=>"sentido","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_LinkContenidoUG_b","nombre"=>"LINK_UG_DY","tipo"=>"1","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_Comentari_b","nombre"=>"COMENTARIO_DY","tipo"=>"1","idbg"=>$intIdBd_t];
                            
                            
                        }else{

                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_Activo____b","nombre"=>"ACTIVO_DY","tipo"=>"activo","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_Estado____b","nombre"=>"ESTADO_DY","tipo"=>"estado","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_NumeInte__b","nombre"=>"NUMERO_INTENTOS_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_GesMasImp_b","nombre"=>"GESTION_MAS_IMPORTANTE_DY","tipo"=>"monoef","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_FeGeMaIm__b","nombre"=>"FECHA_GMI_DY","tipo"=>"5","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FeGeMaIm__b,'%Y')","nombre"=>"ANHO_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FeGeMaIm__b,'%m')","nombre"=>"MES_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FeGeMaIm__b,'%d')","nombre"=>"DIA_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FeGeMaIm__b,'%H')","nombre"=>"HORA_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_CoGesMaIm_b","nombre"=>"CLASIFICACION_GMI_DY","tipo"=>"clasi","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_UsuarioGMI_b","nombre"=>"USUARIO_GMI_DY","tipo"=>"usu","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_CanalGMI_b","nombre"=>"CANAL_GMI_DY","tipo"=>"canal","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_SentidoGMI_b","nombre"=>"SENTIDO_GMI_DY","tipo"=>"sentido","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_LinkContenidoGMI_b","nombre"=>"LINK_GMI_DY","tipo"=>"1","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_TipoReintentoGMI_b","nombre"=>"TIPO_REINTENTO_GMI_DY","tipo"=>"estado","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_CantidadIntentosGMI_b","nombre"=>"NUMERO_INTENTOS_GMI_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_ComentarioGMI_b","nombre"=>"COMENTARIO_GMI_DY","tipo"=>"1","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_UltiGest__b","nombre"=>"ULTIMA_GESTION_DY","tipo"=>"monoef","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_FecUltGes_b","nombre"=>"FECHA_UG_DY","tipo"=>"5","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FecUltGes_b,'%Y')","nombre"=>"ANHO_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FecUltGes_b,'%m')","nombre"=>"MES_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FecUltGes_b,'%d')","nombre"=>"DIA_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FecUltGes_b,'%H')","nombre"=>"HORA_UG_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_ConUltGes_b","nombre"=>"CLASIFICACION_UG_DY","tipo"=>"clasi","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_CanalUG_b","nombre"=>"CANAL_UG_DY","tipo"=>"canal","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_SentidoUG_b","nombre"=>"SENTIDO_UG_DY","tipo"=>"sentido","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_LinkContenidoUG_b","nombre"=>"LINK_UG_DY","tipo"=>"1","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_FecHorMinProGes__b","nombre"=>"FECHA_MINIMA_PROXIMO_INTENTO_DY","tipo"=>"5","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_Comentari_b","nombre"=>"COMENTARIO_DY","tipo"=>"1","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdBd_t."_M".$intIdMuestra_t."_FecHorAge_b","nombre"=>"FECHA_AGENDA_DY","tipo"=>"5","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FecHorAge_b,'%Y')","nombre"=>"ANHO_AGENDA_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FecHorAge_b,'%m')","nombre"=>"MES_AGENDA_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FecHorAge_b,'%d')","nombre"=>"DIA_AGENDA_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                            $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_M".$intIdMuestra_t."_FecHorAge_b,'%H')","nombre"=>"HORA_AGENDA_DY","tipo"=>"3","idbg"=>$intIdBd_t];


                        }


                    }else{

                        $arrCamposTemp_t[] = ["campoId"=>$value["campoId"],"nombre"=>$value["nombre"],"tipo"=>$value["tipo"],"idbg"=>$intIdBd_t,"idpregun"=>$value["ID"]];


                    }

                }

                foreach ($arrCamposTemp_t as $value) {

                    array_push($arrCampos_t,$value);
                    
                }       


                echo json_encode($arrCampos_t);  

                break;

            case 'bkpaso':

                $arrCamposDinamicos_t = columnasDinamicas($intIdBd_t);

                $arrCampos_t[0]=["campoId"=>"G".$intIdBd_t."_ConsInte__b","nombre"=>"ID_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                $arrCampos_t[1]=["campoId"=>"G".$intIdBd_t."_FechaInsercion","nombre"=>"FECHA_CREACION_DY","tipo"=>"5","idbg"=>$intIdBd_t];
                $arrCampos_t[2]=["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FechaInsercion,'%Y')","nombre"=>"ANHO_CREACION_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                $arrCampos_t[3]=["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FechaInsercion,'%m')","nombre"=>"MES_CREACION_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                $arrCampos_t[4]=["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FechaInsercion,'%d')","nombre"=>"DIA_CREACION_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                $arrCampos_t[5]=["campoId"=>"DATE_FORMAT(G".$intIdBd_t."_FechaInsercion,'%H')","nombre"=>"HORA_CREACION_DY","tipo"=>"3","idbg"=>$intIdBd_t];

                foreach ($arrCamposDinamicos_t as $value) {

                        $arrCamposTemp_t[] = ["campoId"=>$value["campoId"],"nombre"=>$value["nombre"],"tipo"=>$value["tipo"],"idbg"=>$intIdBd_t,"idpregun"=>$value["ID"]];

                }

                foreach ($arrCamposTemp_t as $value) {

                    array_push($arrCampos_t,$value);
                    
                }       


                echo json_encode($arrCampos_t);         

                break;

            case 'gspaso':

                $arrCamposDinamicos_t = columnasDinamicas($intIdGuion_t);

                $arrCampos_t[0]=["campoId"=>"G".$intIdGuion_t."_ConsInte__b","nombre"=>"ID_DY","tipo"=>"3","idbg"=>$intIdGuion_t];
                $arrCampos_t[1]=["campoId"=>"G".$intIdBd_t."_ConsInte__b","nombre"=>"ID_BD_DY","tipo"=>"3","idbg"=>$intIdBd_t];
                $arrCampos_t[2]=["campoId"=>"G".$intIdBd_t."_FechaInsercion","nombre"=>"FECHA_CREACION_DY","tipo"=>"5","idbg"=>$intIdBd_t];
                $arrCampos_t[3]=["campoId"=>"G".$intIdGuion_t."_CodigoMiembro","nombre"=>"ID_BD_DY","tipo"=>"3","idbg"=>$intIdGuion_t];

                $arrGestionControl_t = ["tipificacion"=>"","reintento"=>"","feAgenda"=>"","observacion"=>"","fecha"=>""];

                foreach ($arrCamposDinamicos_t as $value) {

                    if ($value["seccion"] == "4" || $value["seccion"] == "3") {



                      switch ($value["nombre"]) {

                        case 'TIPIFICACIN':

                          $arrGestionControl_t["tipificacion"]=$value["ID"];

                          break;

                        case 'REINTENTO':

                          $arrGestionControl_t["reintento"]=$value["ID"];

                          break;

                        case 'HORA_AGENDA':

                          $arrGestionControl_t["feAgenda"]=$value["ID"];

                          break;

                        case 'OBSERVACION':

                          $arrGestionControl_t["observacion"]=$value["ID"];

                          break;

                        case 'FECHA':

                          $arrGestionControl_t["fecha"]="G".$intIdGuion_t."_FechaInsercion";

                          break;

                      }

                    }else{

                        $arrCamposTemp_t[] = ["campoId"=>$value["campoId"],"nombre"=>$value["nombre"],"tipo"=>$value["tipo"],"idbg"=>$intIdGuion_t,"idpregun"=>$value["ID"]];

                    }

                }



                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>$arrGestionControl_t["fecha"],"nombre"=>"FECHA_GESTION_DY","tipo"=>"5","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(".$arrGestionControl_t["fecha"].",'%Y')","nombre"=>"ANHO_GESTION_DY","tipo"=>"3","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(".$arrGestionControl_t["fecha"].",'%m')","nombre"=>"MES_GESTION_DY","tipo"=>"3","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(".$arrGestionControl_t["fecha"].",'%d')","nombre"=>"DIA_GESTION_DY","tipo"=>"3","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(".$arrGestionControl_t["fecha"].",'%H')","nombre"=>"HORA_GESTION_DY","tipo"=>"3","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdGuion_t."_Usuario","nombre"=>"AGENTE_DY","tipo"=>"usu","idbg"=>$intIdBd_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdGuion_t."_Sentido___b","nombre"=>"SENTIDO","tipo"=>"sentido","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdGuion_t."_Canal_____b","nombre"=>"CANAL","tipo"=>"canal","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdGuion_t."_C".$arrGestionControl_t["tipificacion"],"idpregun"=>$arrGestionControl_t["tipificacion"],"nombre"=>"TIPIFICACIÓN_DY","tipo"=>"6","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdGuion_t."_Clasificacion","nombre"=>"CLASIFICACION_DY","tipo"=>"clasi","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdGuion_t."_C".$arrGestionControl_t["reintento"],"idpregun"=>$arrGestionControl_t["reintento"],"nombre"=>"REINTENTO_DY","tipo"=>"6","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdGuion_t."_C".$arrGestionControl_t["feAgenda"],"idpregun"=>$arrGestionControl_t["feAgenda"],"nombre"=>"FECHA_AGENDA_DY","tipo"=>"5","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdGuion_t."_C".$arrGestionControl_t["feAgenda"].",'%Y')","idpregun"=>$arrGestionControl_t["feAgenda"],"nombre"=>"ANHO_AGENDA_DY","tipo"=>"3","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdGuion_t."_C".$arrGestionControl_t["feAgenda"].",'%m')","idpregun"=>$arrGestionControl_t["feAgenda"],"nombre"=>"MES_AGENDA_DY","tipo"=>"3","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdGuion_t."_C".$arrGestionControl_t["feAgenda"].",'%d')","idpregun"=>$arrGestionControl_t["feAgenda"],"nombre"=>"DIA_AGENDA_DY","tipo"=>"3","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"DATE_FORMAT(G".$intIdGuion_t."_C".$arrGestionControl_t["feAgenda"].",'%H')","idpregun"=>$arrGestionControl_t["feAgenda"],"nombre"=>"HORA_AGENDA_DY","tipo"=>"3","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdGuion_t."_C".$arrGestionControl_t["observacion"],"idpregun"=>$arrGestionControl_t["observacion"],"nombre"=>"OBSERVACION_DY","tipo"=>"1","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdGuion_t."_LinkContenido","nombre"=>"DESCARGAGRABACION_DY","tipo"=>"1","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdGuion_t."_Paso","nombre"=>"PASO_DY","tipo"=>"3","idbg"=>$intIdGuion_t];
                $arrCampos_t[count($arrCampos_t)] = ["campoId"=>"G".$intIdGuion_t."_Origen_b","nombre"=>"ORIGEN_DY","tipo"=>"1","idbg"=>$intIdGuion_t];


                foreach ($arrCamposTemp_t as $value) {

                    array_push($arrCampos_t,$value);
                    
                }       


                echo json_encode($arrCampos_t);         

                break;
            
            default:



                break;
        }


    }

    function opcionesLista($strNombre_p)
    {   
        $strNombre_t = trim($strNombre_p);

        $arrBuscar_t = ['á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä','é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë','í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î','ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô','ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü','ñ', 'Ñ', 'ç', 'Ç'];

        $arrCambiar_t = ['a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A','e', 'e', 'e', 'e', 'E', 'E', 'E', 'E','i', 'i', 'i', 'i', 'I', 'I', 'I', 'I','o', 'o', 'o', 'o', 'O', 'O', 'O', 'O','u', 'u', 'u', 'u', 'U', 'U', 'U', 'U','n', 'N', 'c', 'C'];

        $strNombre_t = str_replace($arrBuscar_t, $arrCambiar_t, $strNombre_t);  

        $strNombre_t = preg_replace("/\s+/", "_", $strNombre_t);

        $strNombre_t = preg_replace('([^0-9a-zA-Z_])', "", $strNombre_t);

        $strNombre_t = strtoupper($strNombre_t);

        $strNombre_t = rtrim($strNombre_t,"_");

        return $strNombre_t; 
    }

    if (isset($_GET['ConsumirWS'])) {
        //JDBD - se actualizan las vistas del huesped.
        echo generarVistasPorHuesped(null,$_GET['huespedId']);
        
    }

    if(isset($_POST['opcion']) && $_POST['opcion'] == "borrarLogCargue"){
         $Lsql = "DELETE FROM ".$BaseDatos_systema.".LOG_CARGUE WHERE LOG_CARGUE_Token_b =".$_SESSION['TOKEN'];
         $res = $mysqli->query($Lsql);
    }
    if (isset($_POST['ACD'])) {
        $EstpasLslq = "SELECT ESTPAS_ConsInte__CAMPAN_b FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_Tipo______b = 1 AND ESTPAS_ConsInte__ESTRAT_b = ".$_POST['ACD'];
        $resEspaslq = $mysqli->query($EstpasLslq);
        $nombresRe = [];
        if ($resEspaslq->num_rows > 0) {
            while ($key = $resEspaslq->fetch_object()) {
                if($key->ESTPAS_ConsInte__CAMPAN_b != null && !empty($key->ESTPAS_ConsInte__CAMPAN_b)){
                    $CampanLsql = "SELECT CAMPAN_Nombre____b, CAMPAN_IdCamCbx__b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$key->ESTPAS_ConsInte__CAMPAN_b;   
                    $resPanLsql = $mysqli->query($CampanLsql);
                    $dataAnLsql = $resPanLsql->fetch_array();
                    $NombreCamp = $dataAnLsql['CAMPAN_Nombre____b']; 
                    $CAMPAN_IdCamCbx__b = $dataAnLsql['CAMPAN_IdCamCbx__b'];

                    $titulodeLapregunta = $NombreCamp;
                    $titulodeLapregunta = sanear_strings($titulodeLapregunta);
                    $titulodeLapregunta = str_replace(' ', '_', $titulodeLapregunta);
                    $titulodeLapregunta = substr($titulodeLapregunta, 0 , 20);


                    $nombresRe[] = ['nombre' => 'ACD '.$titulodeLapregunta,
                                    'IdCamCbx__b' => (int)$CAMPAN_IdCamCbx__b];
                }
            }
            echo json_encode($nombresRe);
        }else{
            $nombresRe[] = ['vacio' => 0];
            echo json_encode($nombresRe);
        }


        // echo $EstpasLslq;
    }
    if (isset($_GET['limpiar_nombre'])) {

        $res_nombre = "UPDATE ".$BaseDatos_general.".reportes_automatizados 
                       SET ruta_archivo = SUBSTRING(ruta_archivo,1,length(ruta_archivo)-19)
                       WHERE id_huesped = ".$_GET['huespedId']." AND id_estrategia = ".$_GET['estrategiaId'].";";
        // if ($mysqli->query($res_nombre) === TRUE) {
        //     echo "SE ARRANCA LA FECHA";
        // }
    }


    if (isset($_GET['correosFinal'])) {
        $data = array(  
                    "strUsuario_t"              =>  'crm',
                    "strToken_t"                =>  'D43dasd321',
                    "intIdHuesped_p"      =>  $_GET['huespedId'],
                    "intIdEstrategia_p" => $_GET['estrategiaId'],
                    "strEmailSolicitante_p" => $_GET['correosFinal']
                );                
        $data_string = json_encode($data); 
        $ch = curl_init('http://127.0.0.1:8080/dyalogocore/api/reportes/solicitarReportesAutomatizados');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Accept: application/json', 
            'Content-Type: application/json',                                                                               
            'Content-Length: ' . strlen($data_string))                                                                      
        ); 
        $respuesta = curl_exec ($ch);
        $error = curl_error($ch);
        curl_close ($ch);

        echo json_encode($respuesta);
    }

    //JDBD - Esta funcion retorna el nombre de la vista para reportes.
        function nombreVistaBd($intEstratEstpasId_p,$intTipo_t){

            global $mysqli;
            global $BaseDatos_systema;
            global $BaseDatos_general;

            if ($intTipo_t == 2) {

                $strSQLNombreVista_t = "SELECT MAX(id) AS id, nombre FROM ".$BaseDatos_general.".vistas_generadas JOIN ".$BaseDatos_systema.".ESTRAT ON id_guion = ESTRAT_ConsInte_GUION_Pob WHERE ESTRAT_ConsInte__b = ".$intEstratEstpasId_p." GROUP BY id_guion";

            }

            if ($intTipo_t == 3) {

                $strSQLNombreVista_t = "SELECT nombre FROM ".$BaseDatos_general.".vistas_generadas JOIN ".$BaseDatos_systema.".CAMPAN ON id_campan = CAMPAN_ConsInte__b JOIN ".$BaseDatos_systema.".ESTPAS ON CAMPAN_ConsInte__b = ESTPAS_ConsInte__CAMPAN_b WHERE ESTPAS_ConsInte__b = ".$intEstratEstpasId_p." AND nombre LIKE '%ACD_DIA%'";

            }

            if ($intTipo_t == 4) {

                $strSQLNombreVista_t = "SELECT nombre FROM ".$BaseDatos_general.".vistas_generadas JOIN ".$BaseDatos_systema.".CAMPAN ON id_campan = CAMPAN_ConsInte__b JOIN ".$BaseDatos_systema.".ESTPAS ON CAMPAN_ConsInte__b = ESTPAS_ConsInte__CAMPAN_b WHERE ESTPAS_ConsInte__b = ".$intEstratEstpasId_p." AND nombre LIKE '%ACD_HORA%'";

            }


            $resSQLNombreVista_t = $mysqli->query($strSQLNombreVista_t);

            if ($resSQLNombreVista_t && $resSQLNombreVista_t->num_rows > 0) {

                return $resSQLNombreVista_t->fetch_object()->nombre; 

            }else{

                return "sin vista";

            }

        }    

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

        if (isset($_GET["DataGraficaBd_4"])) {

            $intEstratId_t = $_POST["intEstratId_t"];

            //JDBD - Seteamos las varibles para traer la informacion del reporte grafico.
                $strFechaInicial_t = $_POST["strFechaInicial_t"];
                $strFechaFinal_t = $_POST["strFechaFinal_t"];
                $strNombreVistaBd_t = nombreVistaBd($intEstratId_t,2);

             $strSQLData_t = "SELECT COUNT(1) AS cantidad, GESTION_MAS_IMPORTANTE FROM ".$BaseDatos.".".$strNombreVistaBd_t." WHERE DATE(FECHA_CREACION) >= '".$strFechaInicial_t."' AND DATE(FECHA_CREACION) <= '".$strFechaFinal_t."' GROUP BY GESTION_MAS_IMPORTANTE ORDER BY GESTION_MAS_IMPORTANTE ASC";

             $resSQLData_t = $mysqli->query($strSQLData_t);

             $arrDatos_t = [];

            if ($resSQLData_t && $resSQLData_t->num_rows > 0) {

                while ($obj = $resSQLData_t->fetch_object()) {
                    $arrDatos_t[] = $obj;
                }

            }

            echo json_encode($arrDatos_t);

        }

        if (isset($_GET["DataGraficaBd_3"])) {

            $intEstratId_t = $_POST["intEstratId_t"];

            //JDBD - Seteamos las varibles para traer la informacion del reporte grafico.
                $strFechaInicial_t = $_POST["strFechaInicial_t"];
                $strFechaFinal_t = $_POST["strFechaFinal_t"];
                $strNombreVistaBd_t = nombreVistaBd($intEstratId_t,2);

             $strSQLData_t = "SELECT COUNT(1) AS cantidad, REINTENTO_GMI FROM ".$BaseDatos.".".$strNombreVistaBd_t." WHERE DATE(FECHA_CREACION) >= '".$strFechaInicial_t."' AND DATE(FECHA_CREACION) <= '".$strFechaFinal_t."' GROUP BY REINTENTO_GMI ORDER BY REINTENTO_GMI DESC";

             $resSQLData_t = $mysqli->query($strSQLData_t);

             $arrDatos_t = [];

            if ($resSQLData_t && $resSQLData_t->num_rows > 0) {

                while ($obj = $resSQLData_t->fetch_object()) {
                    $arrDatos_t[] = $obj;
                }

            }

            echo json_encode($arrDatos_t);

        }

        if (isset($_GET["DataGraficaBd_2"])) {

            $intEstratId_t = $_POST["intEstratId_t"];

            //JDBD - Seteamos las varibles para traer la informacion del reporte grafico.
                $strFechaInicial_t = $_POST["strFechaInicial_t"];
                $strFechaFinal_t = $_POST["strFechaFinal_t"];
                $strNombreVistaBd_t = nombreVistaBd($intEstratId_t,2);

             $strSQLData_t = "SELECT COUNT(1) AS cantidad, CANTIDAD_INTENTOS FROM ".$BaseDatos.".".$strNombreVistaBd_t." WHERE DATE(FECHA_CREACION) >= '".$strFechaInicial_t."' AND DATE(FECHA_CREACION) <= '".$strFechaFinal_t."' GROUP BY CANTIDAD_INTENTOS ORDER BY CANTIDAD_INTENTOS ASC";

             $resSQLData_t = $mysqli->query($strSQLData_t);

             $arrDatos_t = [];

            if ($resSQLData_t && $resSQLData_t->num_rows > 0) {

                while ($obj = $resSQLData_t->fetch_object()) {
                    $arrDatos_t[] = $obj;
                }

            }

            echo json_encode($arrDatos_t);

        }

        if (isset($_GET["DataGraficaSC_4"])) {

            //JDBD - Seteamos las varibles para traer la informacion del reporte grafico.
            $strFechaInicial_t = $_POST["strFechaInicial_t"];
            $strFechaFinal_t = $_POST["strFechaFinal_t"];
            $idTipo_t = $_POST["idTipo_t"];
            $idBd_t = $_POST["idBd_t"];
            $idEstpas_t = $_POST["idEstpas_t"];

            $strHoras_t = "";
            $arrDatos_t = ["horas"=>[],"data"=>[]];

            $pregun1 = pregun(3,$idBd_t);

            $strSQLHoras_t = "SELECT DATE_FORMAT(G".$idBd_t."_FechaInsercion,'%H') AS hora FROM ".$BaseDatos.".G".$idBd_t." WHERE (G".$idBd_t."_Paso = ".$idEstpas_t." OR G".$idBd_t."_Paso IS NULL) AND DATE(G".$idBd_t."_FechaInsercion) BETWEEN '".$strFechaInicial_t."' AND '".$strFechaFinal_t."' GROUP BY DATE_FORMAT(G".$idBd_t."_FechaInsercion,'%H')";



            if ($resSQLHoras_t = $mysqli->query($strSQLHoras_t)) {

                if ($resSQLHoras_t->num_rows > 0) {

                    while ($obj = $resSQLHoras_t->fetch_object()) {

                        $hora = $obj->hora;

                        $strHoras_t .= ", SUM((CASE WHEN DATE_FORMAT(G".$idBd_t."_FechaInsercion,'%H') = '".$hora."' THEN 1 ELSE 0 END)) AS '".$hora."'";

                        array_push($arrDatos_t["horas"], $hora);

                    }

                    $strSQLData_t = "SELECT SUBSTR(".$BaseDatos_general.".fn_item_lisopc(".$pregun1."),1,20) AS gestion ".$strHoras_t." FROM ".$BaseDatos.".G".$idBd_t." WHERE (G".$idBd_t."_Paso = ".$idEstpas_t." OR G".$idBd_t."_Paso IS NULL) AND DATE(G".$idBd_t."_FechaInsercion) BETWEEN '".$strFechaInicial_t."' AND '".$strFechaFinal_t."' GROUP BY ".$pregun1." ORDER BY ".$pregun1." DESC";

                    if ($resSQLData_t = $mysqli->query($strSQLData_t)) {

                        if ($resSQLData_t->num_rows > 0) {

                            while ($obj = $resSQLData_t->fetch_object()) {

                                array_push($arrDatos_t["data"], $obj);
                            }

                        }

                    }
                }

            }

            echo json_encode($arrDatos_t);

        }

        if (isset($_GET["DataGraficaSC_3"])) {

            //JDBD - Seteamos las varibles para traer la informacion del reporte grafico.
            $strFechaInicial_t = $_POST["strFechaInicial_t"];
            $strFechaFinal_t = $_POST["strFechaFinal_t"];
            $idTipo_t = $_POST["idTipo_t"];
            $idBd_t = $_POST["idBd_t"];
            $idEstpas_t = $_POST["idEstpas_t"];

            $strHoras_t = "";
            $arrDatos_t = ["horas"=>[],"data"=>[]];

            if ($_GET["DataGraficaSC_3"] == "in") {

                $arrDatos_t = [];

                $strSQLVista_t = "SELECT nombre AS vista FROM ".$BaseDatos_systema.".ESTPAS A JOIN ".$BaseDatos_general.".vistas_generadas B ON A.ESTPAS_ConsInte__CAMPAN_b = B.id_campan WHERE A.ESTPAS_ConsInte__b = ".$idEstpas_t." AND B.nombre LIKE '%_ACD_HORA_%'";

                if ($resSQLVista_t = $mysqli->query($strSQLVista_t)) {

                    if ($resSQLVista_t->num_rows == 1) {

                        $strVista_t = $resSQLVista_t->fetch_object()->vista;

                        $strSQLData_t = "SELECT Intervalo, SUM(Cont_antes_tsf) AS Cont_antes_tsf, SUM(Cont_despues_tsf) AS Cont_despues_tsf, SUM(Aban_antes_tsf) AS Aban_antes_tsf, SUM(Aban_despues_tsf) AS Aban_despues_tsf FROM ".$BaseDatos.".".$strVista_t." WHERE DATE(Fecha) BETWEEN '".$strFechaInicial_t."' AND '".$strFechaFinal_t."' GROUP BY Intervalo ORDER BY Intervalo DESC";

                        if ($resSQLData_t = $mysqli->query($strSQLData_t)) {

                            if ($resSQLData_t->num_rows > 0) {

                                while ($obj = $resSQLData_t->fetch_object()) {

                                    $arrDatos_t[] = $obj;

                                }

                            }

                        }

                    }

                }

            }else{

                $strSQLHoras_t = "SELECT DATE_FORMAT(G".$idBd_t."_FechaInsercion,'%H') AS hora FROM ".$BaseDatos.".G".$idBd_t." WHERE (G".$idBd_t."_Paso = ".$idEstpas_t." OR G".$idBd_t."_Paso IS NULL) AND DATE(G".$idBd_t."_FechaInsercion) BETWEEN '".$strFechaInicial_t."' AND '".$strFechaFinal_t."' GROUP BY DATE_FORMAT(G".$idBd_t."_FechaInsercion,'%H')";


                if ($resSQLHoras_t = $mysqli->query($strSQLHoras_t)) {

                    if ($resSQLHoras_t->num_rows > 0) {

                        while ($obj = $resSQLHoras_t->fetch_object()) {

                            $hora = $obj->hora;

                            $strHoras_t .= ", SUM((CASE WHEN DATE_FORMAT(G".$idBd_t."_FechaInsercion,'%H') = '".$hora."' THEN 1 ELSE 0 END)) AS '".$hora."'";

                            array_push($arrDatos_t["horas"], $hora);

                        }

                    }

                }

                $strSQLData_t = "SELECT ".$BaseDatos_general.".fn_clasificacion_traduccion(G".$idBd_t."_Clasificacion) AS clasificacion ".$strHoras_t." FROM ".$BaseDatos.".G".$idBd_t." WHERE (G".$idBd_t."_Paso = ".$idEstpas_t." OR G".$idBd_t."_Paso IS NULL) AND DATE(G".$idBd_t."_FechaInsercion) BETWEEN '".$strFechaInicial_t."' AND '".$strFechaFinal_t."' GROUP BY G".$idBd_t."_Clasificacion ORDER BY ".$BaseDatos_general.".fn_clasificacion_traduccion(G".$idBd_t."_Clasificacion) ASC";

                if ($resSQLData_t = $mysqli->query($strSQLData_t)) {

                    if ($resSQLData_t->num_rows > 0) {

                        while ($obj = $resSQLData_t->fetch_object()) {

                            array_push($arrDatos_t["data"], $obj);
                        }

                    }

                }

            }


            echo json_encode($arrDatos_t);

        }

        if (isset($_GET["DataGraficaSC_2"])) {

            //JDBD - Seteamos las varibles para traer la informacion del reporte grafico.
            $strFechaInicial_t = $_POST["strFechaInicial_t"];
            $strFechaFinal_t = $_POST["strFechaFinal_t"];
            $idTipo_t = $_POST["idTipo_t"];
            $idBd_t = $_POST["idBd_t"];
            $idEstpas_t = $_POST["idEstpas_t"];

            $arrDatos_t = [];

            $pregun = pregun(3,$idBd_t);

            $strSQLData_t = "SELECT COUNT(1) AS cantidad, SUBSTR(".$BaseDatos_general.".fn_item_lisopc(".$pregun."),1,20) AS gestion FROM ".$BaseDatos.".G".$idBd_t." WHERE DATE(G".$idBd_t."_FechaInsercion) BETWEEN '".$strFechaInicial_t."' AND '".$strFechaFinal_t."' AND (G".$idBd_t."_Paso = ".$idEstpas_t." OR G".$idBd_t."_Paso IS NULL) GROUP BY ".$pregun." ORDER BY ".$pregun." DESC";

            if ($resSQLData_t = $mysqli->query($strSQLData_t)) {

                if ($resSQLData_t->num_rows > 0) {

                    while ($obj = $resSQLData_t->fetch_object()) {
                        $arrDatos_t[]=$obj;
                    }

                }

            }
            
            echo json_encode($arrDatos_t);

        }

        if (isset($_GET["DataGraficaSC_1"])) {

            //JDBD - Seteamos las varibles para traer la informacion del reporte grafico.
            $strFechaInicial_t = $_POST["strFechaInicial_t"];
            $strFechaFinal_t = $_POST["strFechaFinal_t"];
            $idTipo_t = $_POST["idTipo_t"];
            $idBd_t = $_POST["idBd_t"];
            $idEstpas_t = $_POST["idEstpas_t"];

            $strHoras_t = "";
            $arrDatos_t = ["horas"=>[],"data"=>[]];

            $strSQLHoras_t = "SELECT DATE_FORMAT(G".$idBd_t."_FechaInsercion,'%H') AS hora FROM ".$BaseDatos.".G".$idBd_t." WHERE (G".$idBd_t."_Paso = ".$idEstpas_t." OR G".$idBd_t."_Paso IS NULL) AND DATE(G".$idBd_t."_FechaInsercion) BETWEEN '".$strFechaInicial_t."' AND '".$strFechaFinal_t."' GROUP BY DATE_FORMAT(G".$idBd_t."_FechaInsercion,'%H')";

            if ($resSQLHoras_t = $mysqli->query($strSQLHoras_t)) {

                if ($resSQLHoras_t->num_rows > 0) {

                    while ($obj = $resSQLHoras_t->fetch_object()) {

                        $hora = $obj->hora;

                        $strHoras_t .= ", SUM((CASE WHEN DATE_FORMAT(G".$idBd_t."_FechaInsercion,'%H') = '".$hora."' THEN 1 ELSE 0 END)) AS '".$hora."'";

                        array_push($arrDatos_t["horas"], $hora);

                    }

                }

            }

            $strSQLData_t = "SELECT ".$BaseDatos_general.".fn_nombre_USUARI(G".$idBd_t."_Usuario) AS agente ".$strHoras_t." FROM ".$BaseDatos.".G".$idBd_t." WHERE (G".$idBd_t."_Paso = ".$idEstpas_t." OR G".$idBd_t."_Paso IS NULL) AND DATE(G".$idBd_t."_FechaInsercion) BETWEEN '".$strFechaInicial_t."' AND '".$strFechaFinal_t."' GROUP BY G".$idBd_t."_Usuario ORDER BY ".$BaseDatos_general.".fn_nombre_USUARI(G".$idBd_t."_Usuario) ASC";

            if ($resSQLData_t = $mysqli->query($strSQLData_t)) {

                if ($resSQLData_t->num_rows > 0) {

                    while ($obj = $resSQLData_t->fetch_object()) {

                        array_push($arrDatos_t["data"], $obj);
                    }

                }

            }

            echo json_encode($arrDatos_t);

        }

        if (isset($_GET["DataGraficaBd_1"])) {

            $intEstratId_t = $_POST["intEstratId_t"];

            //JDBD - Seteamos las varibles para traer la informacion del reporte grafico.
                $strFechaInicial_t = $_POST["strFechaInicial_t"];
                $strFechaFinal_t = $_POST["strFechaFinal_t"];
                $strNombreVistaBd_t = nombreVistaBd($intEstratId_t,2);

             $strSQLData_t = "SELECT COUNT(1) AS cantidad, CLASIFICACION_GMI FROM ".$BaseDatos.".".$strNombreVistaBd_t." WHERE CLASIFICACION_GMI IN ('Sin gestion','No contactable','No contactado','Contactado','No efectivo','Efectivo') AND DATE(FECHA_CREACION) >= '".$strFechaInicial_t."' AND DATE(FECHA_CREACION) <= '".$strFechaFinal_t."' GROUP BY CLASIFICACION_GMI ORDER BY FIELD(CLASIFICACION_GMI,'Sin gestion','No contactable','No contactado','Contactado','No efectivo','Efectivo')";

             $resSQLData_t = $mysqli->query($strSQLData_t);

             $arrDatos_t = [];

            if ($resSQLData_t && $resSQLData_t->num_rows > 0) {

                while ($obj = $resSQLData_t->fetch_object()) {
                    $arrDatos_t[] = $obj;
                }

            }

            echo json_encode($arrDatos_t);

        }

        if (isset($_POST["TraerGraficas"])) {

            $intIdEstrat_t = $_POST['TraerGraficas'];
            $intIdHuesped_t = $_POST['intIdHuesped'];

            $strSQLBaseEstrat_t = "SELECT ESTRAT_ConsInte_GUION_Pob AS bd, ESTRAT_Nombre____b AS nombre FROM ".$BaseDatos_systema.".ESTRAT WHERE ESTRAT_ConsInte__b = ".$intIdEstrat_t;

            $strSQLBdPasos_t = "SELECT (CASE WHEN ESTPAS_Tipo______b IN (1,6,9) THEN CAMPAN_Nombre____b ELSE ESTPAS_Comentari_b END) AS nombre, ESTPAS_ConsInte__b AS paso, CAMPAN_IdCamCbx__b AS idCBX, ESTPAS_Tipo______b AS tipo , CAMPAN_ConsInte__GUION__Gui_b AS guion, (CASE WHEN ESTPAS_Tipo______b > 6 THEN ESTPAS_ConsInte__MUESTR_b ELSE CAMPAN_ConsInte__MUESTR_b END) AS muestra FROM ".$BaseDatos_systema.".ESTPAS LEFT JOIN ".$BaseDatos_systema.".CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b WHERE ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_t." AND ESTPAS_Tipo______b IN (1,6,9) ORDER BY ESTPAS_Tipo______b ASC";
            $resSQLBdPasos_t = $mysqli->query($strSQLBdPasos_t);

            $resSQLBaseEstrat_t = $mysqli->query($strSQLBaseEstrat_t);
            $objSQLBaseEstrat_t = $resSQLBaseEstrat_t->fetch_object();

            $intIdBase_t = $objSQLBaseEstrat_t->bd;
            $strNombreEstrat_t = opcionesLista($objSQLBaseEstrat_t->nombre);

            $strHTMLReports_t = '<option value="bd" idBd="'.$intIdBase_t.'">GENERAL - '.$strNombreEstrat_t.'</option>';

            while ($objSQLBdPasos_t = $resSQLBdPasos_t->fetch_object()) {

                $strHTMLReports_t .= '<option value="sc" idTipo="'.$objSQLBdPasos_t->tipo.'" idEstpas="'.$objSQLBdPasos_t->paso.'" idBd="'.$objSQLBdPasos_t->guion.'">'.opcionesLista($objSQLBdPasos_t->nombre).'</option>';

            }

            echo $strHTMLReports_t;

        }
        
        if(isset($_POST['TraerReportes'])){


            $intIdEstrat_t = $_POST['TraerReportes'];
            $intIdHuesped_t = $_POST['intIdHuesped'];
            $strSQLBaseEstrat_t = "SELECT ESTRAT_ConsInte_GUION_Pob AS bd, ESTRAT_Nombre____b AS nombre FROM ".$BaseDatos_systema.".ESTRAT WHERE ESTRAT_ConsInte__b = ".$intIdEstrat_t;

            $strSQLBdPasos_t = "SELECT (CASE WHEN ESTPAS_Tipo______b IN (1,6,9) THEN CAMPAN_Nombre____b ELSE ESTPAS_Comentari_b END) AS nombre, ESTPAS_ConsInte__b AS paso, CAMPAN_IdCamCbx__b AS idCBX, ESTPAS_Tipo______b AS tipo , CAMPAN_ConsInte__GUION__Gui_b AS guion, (CASE WHEN ESTPAS_Tipo______b > 6 THEN ESTPAS_ConsInte__MUESTR_b ELSE CAMPAN_ConsInte__MUESTR_b END) AS muestra FROM ".$BaseDatos_systema.".ESTPAS LEFT JOIN ".$BaseDatos_systema.".CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b WHERE ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_t." AND ESTPAS_Tipo______b IN (1,6,7,8,9) ORDER BY ESTPAS_Tipo______b ASC";
            $resSQLBdPasos_t = $mysqli->query($strSQLBdPasos_t);

            $resSQLBaseEstrat_t = $mysqli->query($strSQLBaseEstrat_t);
            $objSQLBaseEstrat_t = $resSQLBaseEstrat_t->fetch_object();

            $intIdBase_t = $objSQLBaseEstrat_t->bd;
            $strNombreEstrat_t = opcionesLista($objSQLBaseEstrat_t->nombre);

            $strHTMLReports_t = '';
            $strHTMLReportsBack_t = '';
            $strHTMLReportsGestiones_t = '';
            $strHTMLReportsACD_t = '';
            $strHTMLReports_t .= '<optgroup label="RESUMEN DE BASE">';
                $strHTMLReports_t .= '<option value="bd" idTipo="" idEstrat="'.$intIdEstrat_t.'" idBd="'.$intIdBase_t.'" idGuion="" idCampanCBX="" periodo="" idPaso="" idMuestra="">GENERAL - '.$strNombreEstrat_t.'</option>';

                while ($objSQLBdPasos_t = $resSQLBdPasos_t->fetch_object()) {

                    if ($objSQLBdPasos_t->tipo == 9) {

                        $strHTMLReportsBack_t .= '<option value="bkpaso" idTipo="" idEstrat="'.$intIdEstrat_t.'" idBd="'.$intIdBase_t.'" idGuion="'.$objSQLBdPasos_t->guion.'" idCampanCBX="" periodo="" idPaso="" idMuestra="">BACK - '.opcionesLista($objSQLBdPasos_t->nombre).'</option>';
                    }else{

                        $strTipo_t = "PASO - ";

                        if ($objSQLBdPasos_t->tipo == 1) {
                            
                            $strTipo_t = "CAMPAÑA - ENTRANTE - ";

                        }elseif ($objSQLBdPasos_t->tipo == 6) {
                            
                            $strTipo_t = "CAMPAÑA - SALIENTE - ";
                            
                        }else if($objSQLBdPasos_t->tipo == 7 || $objSQLBdPasos_t->tipo == 10){

                            $strTipo_t = "CORREO - ";

                        }else if($objSQLBdPasos_t->tipo == 8){

                            $strTipo_t = "SMS - ";

                        }

                        $strHTMLReports_t .= '<option value="bdpaso" idTipo="'.$objSQLBdPasos_t->tipo.'" idEstrat="'.$intIdEstrat_t.'" idBd="'.$intIdBase_t.'" idGuion="" idCampanCBX="" periodo="" idPaso="" idMuestra="'.$objSQLBdPasos_t->muestra.'">'.$strTipo_t.opcionesLista($objSQLBdPasos_t->nombre).'</option>'; 
                    }   

                    if ($objSQLBdPasos_t->tipo < 7) {

                        $strHTMLReportsGestiones_t .= '<option value="gspaso" idTipo="" idEstrat="'.$intIdEstrat_t.'" idBd="'.$intIdBase_t.'" idGuion="'.$objSQLBdPasos_t->guion.'" idCampanCBX="" periodo="" idPaso="'.$objSQLBdPasos_t->paso.'" idMuestra="'.$objSQLBdPasos_t->muestra.'">GS - '.opcionesLista($objSQLBdPasos_t->nombre).'</option>';

                        if ($objSQLBdPasos_t->tipo == 1) {

                             $strHTMLReportsACD_t .= '<option value="acd" idTipo="" idEstrat="'.$intIdEstrat_t.'" idBd="'.$intIdBase_t.'" idGuion="'.$objSQLBdPasos_t->guion.'" idCampanCBX="'.$objSQLBdPasos_t->idCBX.'" periodo="1" idPaso="" idMuestra="'.$objSQLBdPasos_t->muestra.'">ACD - DIA - '.opcionesLista($objSQLBdPasos_t->nombre).'</option>';
                             $strHTMLReportsACD_t .= '<option value="acd" idEstrat="'.$intIdEstrat_t.'" idBd="'.$intIdBase_t.'" idGuion="'.$objSQLBdPasos_t->guion.'" idCampanCBX="'.$objSQLBdPasos_t->idCBX.'" periodo="2" idPaso="" idMuestra="'.$objSQLBdPasos_t->muestra.'">ACD - HORA - '.opcionesLista($objSQLBdPasos_t->nombre).'</option>';

                        } 

                    }   


                }

            $strHTMLReports_t .= $strHTMLReportsBack_t;
            
            $strHTMLReports_t .= '</optgroup>';
            $strHTMLReports_t .= '<optgroup label="GESTIONES">';
                $strHTMLReports_t .= $strHTMLReportsGestiones_t;
            $strHTMLReports_t .= '</optgroup>';
            $strHTMLReports_t .= '<optgroup label="ACD">';
                $strHTMLReports_t .= $strHTMLReportsACD_t;
            $strHTMLReports_t .= '</optgroup>';

            $arrAdherencias_t = ['SESIONES','PAUSAS','NO SE REGISTRARON','LLEGARON TARDE','LLEGARON A TIEMPO','SE FUERON ANTES','SE FUERON A TIEMPO','SESIONES CORTAS','SESIONES DURACION OK','PAUSAS CON HORARIO MUY LARGAS','PAUSAS CON HORARIO DURACION OK','PAUSAS CON HORARIO INCUMPLIDAS','PAUSAS CON HORARIOS CUMPLIDAS','PAUSAS SIN HORARIO MUY LARGA','PAUSAS SIN HORARIO MUCHAS VECES','PAUSAS SIN HORARIO OK','AGENTES SIN MALLA DEFINIDA'];

            $strHTMLReports_t .= '<optgroup label="ADHERENCIAS">'; 

                foreach ($arrAdherencias_t as $key => $reporte) {
                    $strHTMLReports_t .= '<option value="'.($key+1).'" idTipo="" idEstrat="'.$intIdEstrat_t.'" idBd="" idGuion="" idCampanCBX="" periodo="" idPaso="" idMuestra="" >'.$reporte.'</option>';    

                    if ($key == 1) {

                        $strHTMLReports_t .= '<option value="pausas" idTipo="" idEstrat="'.$intIdEstrat_t.'" idBd="" idGuion="" idCampanCBX="" periodo="" idPaso="" idMuestra="" >PAUSAS - DURACION POR AGENTE</option>';  

                    }
                }

            $strHTMLReports_t .= '</optgroup>';

            echo $strHTMLReports_t;

        }


        //Datos del formulario
        if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2_ConsInte__b, G2_C7 as principal ,G2_C5,G2_C6,G2_C7,G2_C8,G2_C9,G2_C10,G2_C11,G2_C12,G2_C13,G2_C14, G2_C69 FROM '.$BaseDatos_systema.'.G2 WHERE G2_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            

            while($key = $result->fetch_object()){

                $datos[$i]['G2_C5'] = utf8_encode($key->G2_C5);

                $datos[$i]['G2_C6'] = utf8_encode($key->G2_C6);

                $datos[$i]['G2_C7'] = $key->G2_C7;

                $datos[$i]['G2_C8'] = utf8_encode($key->G2_C8);

                $datos[$i]['G2_C10'] = utf8_encode($key->G2_C10);

                $datos[$i]['G2_C11'] = utf8_encode($key->G2_C11);

                $datos[$i]['G2_C12'] = utf8_encode($key->G2_C12);

                $datos[$i]['G2_C13'] = utf8_encode($key->G2_C13);

                $datos[$i]['G2_C14'] = utf8_encode($key->G2_C14);

                $datos[$i]['G2_C69'] = utf8_encode($key->G2_C69);
      
                $datos[$i]['principal'] = utf8_encode($key->principal);

                $imagenUser = "assets/img/user2-160x160.jpg?foto=".rand(10, 9999);
                if(file_exists("../../../pages/CampanhasImagenes/".$key->G2_ConsInte__b.".jpg")){
                    $imagenUser = "pages/CampanhasImagenes/".$key->G2_ConsInte__b.".jpg?foto=".rand(10, 9999);
                }

                $datos[$i]['imagenes'] = $imagenUser;   

                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['traer_Flujograma'])){
            echo "{
    \"class\": \"go.GraphLinksModel\",
    \"linkFromPortIdProperty\": \"fromPort\",
    \"linkToPortIdProperty\": \"toPort\",
    \"nodeDataArray\": [";
            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__ESTRAT_b = ".$_POST['id'];
            $res_Pasos = $mysqli->query($Lsql);
            $x = 0;
            $separador = '';
            while ($keu = $res_Pasos->fetch_object()) {
                if($x != 0){
                    $separador = ',';
                }
                echo $separador."
        {\"category\":\"".$keu->ESTPAS_Nombre__b."\",  \"tipoPaso\": ".$keu->ESTPAS_Tipo______b.", \"figure\":\"Circle\", \"key\": ".$keu->ESTPAS_ConsInte__b.", \"loc\":\"".$keu->ESTPAS_Loc______b."\"}"."\n";
                $x++;
            }

    echo "],
    \"linkDataArray\": [ ";
           $Lsql = "SELECT * FROM ".$BaseDatos_systema.".ESTCON WHERE ESTCON_ConsInte__ESTRAT_b = ".$_POST['id'];
            $res_Pasos = $mysqli->query($Lsql);
            $x = 0;
            $separador = '';
            while ($keu = $res_Pasos->fetch_object()) {
                if($x != 0){
                    $separador = ',';
                }
               echo $separador."
        {\"from\":".$keu->ESTCON_ConsInte__ESTPAS_Des_b.", \"to\":".$keu->ESTCON_ConsInte__ESTPAS_Has_b.", \"fromPort\":\"".$keu->ESTCON_FromPort_b."\", \"toPort\":\"".$keu->ESTCON_ToPort_b."\", \"visible\":true, \"points\":".$keu->ESTCON_Coordenadas_b.", \"text\":\"".$keu->ESTCON_Comentari_b."\"}"."\n";
                $x++;
            }
    echo "
    ]
}";
        }

        if(isset($_POST['traeMiPaso'])){
            $LSql = "SELECT ESTPAS_ConsInte__b FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__ESTRAT_b = ".$_POST['idEstrat'];
            $res = $mysqli->query($LSql);
            $dato = $res->fetch_array();
            echo $dato['ESTPAS_ConsInte__b'];
        }

        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = 'SELECT G2_ConsInte__b as id,  G2_C7 as camp1 , TIPO_ESTRAT_Nombre____b as camp2 ';
            $Lsql .= ' FROM '.$BaseDatos_systema.'.G2 JOIN  '.$BaseDatos_systema.'.TIPO_ESTRAT ON TIPO_ESTRAT_ConsInte__b = G2_C6 WHERE G2_C5 = '.$_SESSION['HUESPED'];
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= ' AND (G2_C7 like "%'.$_POST['Busqueda'].'%" ';
                $Lsql .= ' OR TIPO_ESTRAT_Nombre____b like "%'.$_POST['Busqueda'].'%") ';
            }

           
            $Lsql .= ' ORDER BY G2_C7 ASC LIMIT 0, 50'; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = $key->camp1;
                $datos[$i]['camp2'] = $key->camp2;
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        if(isset($_GET['CallDatosCombo_Guion_G2_C10'])){
            $Ysql = 'SELECT   G4_ConsInte__b as id  FROM ".$BaseDatos_systema.".G4';
            $guion = $mysqli->query($Ysql);
            echo '<select class="form-control input-sm"  name="G2_C10" id="G2_C10">';
            echo '<option ></option>';
            while($obj = $guion->fetch_object()){
               echo "<option value='".$obj->id."' dinammicos='0'></option>";
            } 
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G2_C11'])){
            $Ysql = 'SELECT   G4_ConsInte__b as id  FROM ".$BaseDatos_systema.".G4';
            $guion = $mysqli->query($Ysql);
            echo '<select class="form-control input-sm"  name="G2_C11" id="G2_C11">';
            echo '<option ></option>';
            while($obj = $guion->fetch_object()){
               echo "<option value='".$obj->id."' dinammicos='0'></option>";
            } 
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G2_C12'])){
            $Ysql = 'SELECT   G4_ConsInte__b as id  FROM ".$BaseDatos_systema.".G4';
            $guion = $mysqli->query($Ysql);
            echo '<select class="form-control input-sm"  name="G2_C12" id="G2_C12">';
            echo '<option ></option>';
            while($obj = $guion->fetch_object()){
               echo "<option value='".$obj->id."' dinammicos='0'></option>";
            } 
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G2_C13'])){
            $Ysql = 'SELECT   G4_ConsInte__b as id  FROM ".$BaseDatos_systema.".G4';
            $guion = $mysqli->query($Ysql);
            echo '<select class="form-control input-sm"  name="G2_C13" id="G2_C13">';
            echo '<option ></option>';
            while($obj = $guion->fetch_object()){
               echo "<option value='".$obj->id."' dinammicos='0'></option>";
            } 
            echo '</select>';
        }


        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos_systema.".G2 WHERE G2_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G2_ConsInte__b as id,  G2_C7 as camp1 , G2_C8 as camp2  FROM '.$BaseDatos_systema.'.$BaseDatos_systema.".G2   WHERE G2_C5 = '.$_SESSION['HUESPED'].'  ORDER BY G2_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".($obj->camp1)."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".($obj->camp2)."</p>
                    </td>
                </tr>";
            } 
        }
          
        //Insertar Extras en caso de haber
        


        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos_systema.".G2 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos_systema.".G2(";
            $LsqlV = " VALUES ("; 
  
            $G2_C5 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2_C5"])){
                if($_POST["G2_C5"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //$G2_C5 = $_POST["G2_C5"];
                    $G2_C5 = str_replace(".", "", $_POST["G2_C5"]);
                    $G2_C5 =  str_replace(",", ".", $G2_C5);
                    $LsqlU .= $separador." G2_C5 = '".$G2_C5."'";
                    $LsqlI .= $separador." G2_C5";
                    $LsqlV .= $separador."'".$G2_C5."'";
                    $validar = 1;
                }
            }
  
            $G2_C6 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2_C6"])){
                if($_POST["G2_C6"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //$G2_C6 = $_POST["G2_C6"];
                    $G2_C6 = str_replace(".", "", $_POST["G2_C6"]);
                    $G2_C6 =  str_replace(",", ".", $G2_C6);
                    $LsqlU .= $separador." G2_C6 = '".$G2_C6."'";
                    $LsqlI .= $separador." G2_C6";
                    $LsqlV .= $separador."'".$G2_C6."'";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2_C7"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2_C7 = '".$_POST["G2_C7"]."'";
                $LsqlI .= $separador."G2_C7";
                $LsqlV .= $separador."'".$_POST["G2_C7"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2_C8"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2_C8 = '".$_POST["G2_C8"]."'";
                $LsqlI .= $separador."G2_C8";
                $LsqlV .= $separador."'".$_POST["G2_C8"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2_C9"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2_C9 = '".$_POST["G2_C9"]."'";
                $LsqlI .= $separador."G2_C9";
                $LsqlV .= $separador."'".$_POST["G2_C9"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2_C10"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2_C10 = '".$_POST["G2_C10"]."'";
                $LsqlI .= $separador."G2_C10";
                $LsqlV .= $separador."'".$_POST["G2_C10"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2_C11"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2_C11 = '".$_POST["G2_C11"]."'";
                $LsqlI .= $separador."G2_C11";
                $LsqlV .= $separador."'".$_POST["G2_C11"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2_C12"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2_C12 = '".$_POST["G2_C12"]."'";
                $LsqlI .= $separador."G2_C12";
                $LsqlV .= $separador."'".$_POST["G2_C12"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2_C13"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2_C13 = '".$_POST["G2_C13"]."'";
                $LsqlI .= $separador."G2_C13";
                $LsqlV .= $separador."'".$_POST["G2_C13"]."'";
                $validar = 1;
            }


            if(isset($_POST["G2_C69"])){
                if($_POST["G2_C69"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2_C69 = '".$_POST["G2_C69"]."'";
                    $LsqlI .= $separador."G2_C69";
                    $LsqlV .= $separador."'".$_POST["G2_C69"]."'";
                    $validar = 1;
                }
                
            }

            if(isset($_POST['G10_C74'])){
                if($_POST["G10_C74"] != '' && $_POST["G10_C74"] != '0'){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2_C69 = '".$_POST["G10_C74"]."'";
                    $LsqlI .= $separador."G2_C69";
                    $LsqlV .= $separador."'".$_POST["G10_C74"]."'";
                    $validar = 1;
                }
                
            }
             
  
            if(isset($_POST["G2_C14"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                if(empty($_POST["G2_C14"]) || is_null($_POST["G2_C14"]) ){       
                    $LsqlU .= $separador."G2_C14 = '".$_POST["txtOcultoColor"]."'";
                    $LsqlI .= $separador."G2_C14";
                    $LsqlV .= $separador."'".$_POST["txtOcultoColor"]."'";
                }else{
                    $LsqlU .= $separador."G2_C14 = '".$_POST["G2_C14"]."'";
                    $LsqlI .= $separador."G2_C14";
                    $LsqlV .= $separador."'".$_POST["G2_C14"]."'";
                }
                
                $validar = 1;
            }else{
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $LsqlU .= $separador."G2_C14 = '".color_rand()."'";
                $LsqlI .= $separador."G2_C14";
                $LsqlV .= $separador."'".color_rand()."'";
                $validar = 1;
            }
             
 
            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }

            
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){

                    $Lsql = $LsqlU." WHERE G2_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    
                    
                }
            }

            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                     
                    if($_POST["oper"] == 'add' ){
                        
                        $id_Usuario_Nuevo = $mysqli->insert_id;
                        if(isset($_FILES['txtFileCampana']['tmp_name']) && !empty($_FILES['txtFileCampana']['tmp_name'])){
                            $ruta = "../../../pages/CampanhasImagenes";
                            if(!file_exists($ruta))
                            {
                                mkdir($ruta);
                            }

                            $extencion = explode('.', basename($_FILES['txtFileCampana']['name']));   
                            $target_path = $ruta.'/'.$id_Usuario_Nuevo.'.jpg'; 
                            $target_path = str_replace(' ', '', $target_path);
                            
                            copy($_FILES['txtFileCampana']['tmp_name'], $target_path);
                            $imagenNombre = $id_Usuario_Nuevo.'.jpg'; 
                            $imagenNombre = str_replace(' ', '', $imagenNombre);
                        }

                        /*ctrCrearReportesDiarios($id_Usuario_Nuevo);
                        ctrCrearReportesSemanales($id_Usuario_Nuevo);
                        ctrCrearReportesMensuales($id_Usuario_Nuevo);
                        */
                        guardar_auditoria("INSERTAR", "INSERTAR REGISTRO EN G2");
                        echo $id_Usuario_Nuevo;

                    }else if($_POST["oper"] == 'edit' ){

                        if(isset($_FILES['txtFileCampana']['tmp_name']) && !empty($_FILES['txtFileCampana']['tmp_name'])){
                            
                            $ruta = "../../../pages/CampanhasImagenes";
                            if(!file_exists($ruta))
                            {
                                mkdir($ruta);
                            }

                            
  
                            $extencion = explode('.', basename($_FILES['txtFileCampana']['name']));   
                            $target_path = $ruta.'/'.$_POST['id'].'.jpg'; 
                            $target_path = str_replace(' ', '', $target_path);
                            
                            if(file_exists($target_path)){
                                unlink($target_path);
                            }

                            $ver = (float)phpversion();
                            if ($ver > 7.0) {
                                $nuevoAncho = 500;
                                $nuevoAlto  = 500;
                                list($ancho, $alto) = getimagesize($_FILES['txtFileCampana']['tmp_name']);
                                $origen  = imagecreatefromjpeg($_FILES['txtFileCampana']['tmp_name']);
                                //le decimos que vamos acrear una imagen en el destino con esos ancho y alto
                                $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                                //cortamos esta imagen

                                
                                imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                                imagejpeg($destino, $target_path);
                            }else{
                                copy($_FILES['txtFileCampana']['tmp_name'], $target_path);
                            }
                            

                            $imagenNombre = $_POST["id"].'.jpg'; 
                            $imagenNombre = str_replace(' ', '', $imagenNombre);
                        }
                        //JDBD actualizamos todos los reportes y los cambios nuevos.
                        if(isset($_POST['totalGuardadosAsuntos'])){
                            //JDBD recorremos la cantidad de reportes guardados de la estrategias.
                            for ($i = 0; $i < $_POST['totalGuardadosAsuntos']; $i++) { 
                                if(isset($_POST['txtAsuntosGuardados_'.$i]) && $_POST['txtAquienVa_'.$i] != '' && $_POST['txtNombreReporte_'.$i] != '' && $_POST['txtHoraEnvio_'.$i] != '' ){

                                        $strDestinatario_t = $_POST['txtAquienVa_'.$i];
                                        $strCopia_t = $_POST['txtCopiaA_'.$i];
                                        $strHoraEnvio_t = $_POST['txtHoraEnvio_'.$i];
                                        $intPeriodicidad_t = $_POST['cmbPeriodicidad_'.$i];
                                        $strNombreReport_t = $_POST['txtNombreReporte_'.$i];
                                        $intIdEstrat_t = $_POST['id'];
                                        $intIdHuesped_t = $_SESSION['HUESPED'];
                                        $intIdReporte_t = $_POST['txtAsuntosGuardados_'.$i];

                                    //JDBD actualizamos los reportes que no sean tipo adherencia.
                                    if ($intPeriodicidad_t != '4') {

                                        $strUp_t = updateNuevoReporte($intIdEstrat_t,$strDestinatario_t,$strCopia_t,$intPeriodicidad_t,$strHoraEnvio_t,$strNombreReport_t,$intIdReporte_t);

                                    }else{
                                        //JDBD actualizamos los reportes de tipo adherencia.
                                        $updateSQL = "UPDATE ".$BaseDatos_general.".reportes_automatizados SET destinatarios = '".$strDestinatario_t."' , destinatarios_cc = '".$strCopia_t."', momento_envio = '".$strHoraEnvio_t."', asunto = '".$strNombreReport_t."' WHERE id = ".$intIdReporte_t;
                                        $res = $mysqli->query($updateSQL); 
                                        ctrCrearReportesDirarioAdherencia($intIdEstrat_t,$intIdHuesped_t,null,null,null,"::UPDATE::",null,true);
                                    }

                                }
                            }
                        }

                        //JDBD vamos a insertar los nuevos reportes.
                        if(isset($_POST['contarAsuntos']) && $_POST['contarAsuntos'] != 1){
                            //JDBD recorremos la cantidad de reportes nuevos.
                            for ($i=0; $i < $_POST['contarAsuntos']; $i++) { 
                                if(isset($_POST['GtxtNombreReporte_'.$i]) && $_POST['GtxtNombreReporte_'.$i] != ''  && $_POST['GtxtAquienVa_'.$i] != ''  && $_POST['GtxtHoraEnvio_'.$i] != '' ){

                                    $strNombreReport_t = $_POST['GtxtNombreReporte_'.$i];
                                    $strDestinatario_t = $_POST['GtxtAquienVa_'.$i];
                                    $strCopia_t = $_POST['GtxtCopiaA_'.$i];
                                    $strHoraEnvio_t = $_POST['GtxtHoraEnvio_'.$i];
                                    $intPeriodicidad_t = $_POST['GcmbPeriodicidad_'.$i];
                                    $intIdEstrat_t = $_POST['id'];
                                    $intIdHuesped_t = $_SESSION['HUESPED'];

                                    $strProyecto_t = str_replace(' ', '', $_SESSION['PROYECTO']);
                                    $strProyecto_t = substr($strProyecto_t, 0, 8);
                                    $strProyecto_t = sanear_strings($strProyecto_t);
                                    $strNomEstrat_t = datosEstrat($intIdEstrat_t);
                                    $strRutaArchivo_t = "/tmp/".$strProyecto_t."_".$strNomEstrat_t;
                                    $strRutaArchivo_t = str_replace(' ', '_', $strRutaArchivo_t);
                                    $strRutaArchivo_t = quitarTildes($strRutaArchivo_t);

                                    //JDBD validamos el tipo de reporte para el nombre del excell e insertamos.
                                    switch ($intPeriodicidad_t) {
                                        case '1':
                                            $strRutaArchivo_t .= "_DIARIO_";
                                            insertarNuevoReporte($strNombreReport_t,$strRutaArchivo_t,$strDestinatario_t,$strHoraEnvio_t,$strCopia_t,$intPeriodicidad_t,$intIdEstrat_t,$intIdHuesped_t,true);
                                            break;
                                        case '2':
                                            $strRutaArchivo_t .= "_SEMANAL_";
                                            insertarNuevoReporte($strNombreReport_t,$strRutaArchivo_t,$strDestinatario_t,$strHoraEnvio_t,$strCopia_t,$intPeriodicidad_t,$intIdEstrat_t,$intIdHuesped_t,true);
                                            break;
                                        case '3':
                                            $strRutaArchivo_t .= "_MENSUAL_";
                                            insertarNuevoReporte($strNombreReport_t,$strRutaArchivo_t,$strDestinatario_t,$strHoraEnvio_t,$strCopia_t,$intPeriodicidad_t,$intIdEstrat_t,$intIdHuesped_t,true);
                                            break;
                                        case '4':
                                            ctrCrearReportesDirarioAdherencia($intIdEstrat_t,$intIdHuesped_t, $strDestinatario_t,$strCopia_t,null,$strNombreReport_t,$strHoraEnvio_t,true);
                                            break;            
                                    }
                                }
                            }
                            
                        }

                        if(isset($_POST['contadorMetasViejas'])){

                            $intIdEstrat_t = $_POST['id'];
                            $arrPasos_t = obtenerPasos($intIdEstrat_t);

                            if (count($arrPasos_t) > 0) {
                                foreach ($arrPasos_t as $key => $bola) {

                                    switch ($bola["ESTPAS_Tipo______b"]) {
                                        case 1:
                                            //JDBD - iniciamos la insercion de las metas entrantes
                                            insertarMetas($intIdEstrat_t,$bola["ESTPAS_ConsInte__b"],1);
                                            break;
                                        case 6:
                                            //JDBD - iniciamos la insercion de las metas salientes
                                            insertarMetas($intIdEstrat_t,$bola["ESTPAS_ConsInte__b"],6);
                                            break;
                                    }

                                } 
                            }
                                
                        }
 
                        guardar_auditoria("ACTUALIZAR", "ACTUALIZO EL REGISTRO # ".$_POST["id"]." EN G2");

                        echo '0';
                    }else if($_POST["oper"] == 'del' ){
                        $Lsql_InsercionRepor = "DELETE FROM ".$BaseDatos_general.".reportes_automatizados WHERE id_estrategia =".$_POST['id'];
                        if($mysqli->query($Lsql_InsercionRepor) === true ){
                            guardar_auditoria("ELIMINAR", "ELIMINO EL REGISTRO # ".$_POST['id']." EN G2");
                            echo '1';
                        }else{
                            echo $mysqli->error;
                        }
                    }
                } else {
                    echo json_encode(array('code' => '-2' , 'messaje' => $mysqli->errno));
                }
            }
        }

        if(isset($_POST['crearRegistroNuevo'])){
            $LsqlI = "INSERT INTO ".$BaseDatos_systema.".G2(G2_C5) VALUES (".$_SESSION['HUESPED'].")";
            if ($mysqli->query($LsqlI) === TRUE) {
                echo $mysqli->insert_id;
            }
        }

        
        if(isset($_GET["insertarDatosSubgrilla_0"])){
            
            if(isset($_POST["oper"])){
                $Lsql  = '';

                $validar = 0;
                $LsqlU = "UPDATE ".$BaseDatos_systema.".G4 SET "; 
                $LsqlI = "INSERT INTO ".$BaseDatos_systema.".G4(";
                $LsqlV = " VALUES ("; 
     
                                                                             
                if(isset($_POST["G4_C21"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G4_C21 = '".$_POST["G4_C21"]."'";
                    $LsqlI .= $separador."G4_C21";
                    $LsqlV .= $separador."'".$_POST["G4_C21"]."'";
                    $validar = 1;
                }
                                                                              
                                                                               
     
                $G4_C22= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G4_C22"])){    
                    if($_POST["G4_C22"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G4_C22 = $_POST["G4_C22"];
                        $LsqlU .= $separador." G4_C22 = '".$G4_C22."'";
                        $LsqlI .= $separador." G4_C22";
                        $LsqlV .= $separador."'".$G4_C22."'";
                        $validar = 1;
                    }
                }
     
                $G4_C24= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G4_C24"])){    
                    if($_POST["G4_C24"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G4_C24 = $_POST["G4_C24"];
                        $LsqlU .= $separador." G4_C24 = '".$G4_C24."'";
                        $LsqlI .= $separador." G4_C24";
                        $LsqlV .= $separador."'".$G4_C24."'";
                        $validar = 1;
                    }
                }
     
                $G4_C25= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G4_C25"])){    
                    if($_POST["G4_C25"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G4_C25 = $_POST["G4_C25"];
                        $LsqlU .= $separador." G4_C25 = '".$G4_C25."'";
                        $LsqlI .= $separador." G4_C25";
                        $LsqlV .= $separador."'".$G4_C25."'";
                        $validar = 1;
                    }
                }
     
                $G4_C26= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G4_C26"])){    
                    if($_POST["G4_C26"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G4_C26 = $_POST["G4_C26"];
                        $LsqlU .= $separador." G4_C26 = '".$G4_C26."'";
                        $LsqlI .= $separador." G4_C26";
                        $LsqlV .= $separador."'".$G4_C26."'";
                        $validar = 1;
                    }
                }
     
                $G4_C27= NULL;
                //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
                if(isset($_POST["G4_C27"])){    
                    if($_POST["G4_C27"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G4_C27 = $_POST["G4_C27"];
                        $LsqlU .= $separador." G4_C27 = '".$G4_C27."'";
                        $LsqlI .= $separador." G4_C27";
                        $LsqlV .= $separador."'".$G4_C27."'";
                        $validar = 1;
                    }
                }

                if(isset($_POST["Padre"])){
                    if($_POST["Padre"] != ''){
                        //esto es porque el padre es el entero
                        
                        $numero = $_POST["Padre"];
             

                        $G4_C23 = $numero;
                        $LsqlU .= ", G4_C23 = ".$G4_C23."";
                        $LsqlI .= ", G4_C23";
                        $LsqlV .= ",".$_POST["Padre"];
                    }
                }  



                if(isset($_POST['oper'])){
                    if($_POST["oper"] == 'add' ){
                        $Lsql = $LsqlI.")" . $LsqlV.")";
                    }else if($_POST["oper"] == 'edit' ){
                        $Lsql = $LsqlU." WHERE G4_ConsInte__b =".$_POST["providerUserId"]; 
                    }else if($_POST['oper'] == 'del'){
                        $Lsql = "DELETE FROM  ".$BaseDatos_systema.".G4 WHERE G4_ConsInte__b = ".$_POST['id'];
                        $validar = 1;
                    }
                }

                if($validar == 1){
                    // echo $Lsql;
                        $id_Usuario_Nuevo = 1;
                    if ($mysqli->query($Lsql) === TRUE) {
                        $id_Usuario_Nuevo = $mysqli->insert_id;

                        if($_POST["oper"] == 'add' ){
                            guardar_auditoria("INSERTAR", "INSERTAR REGISTRO EN G4");
                        }else if($_POST["oper"] == 'edit' ){
                            guardar_auditoria("ACTUALIZAR", "ACTUALIZO EL REGISTRO # ".$_POST["padre"]." EN G4");
                        }else if($_POST["oper"] == 'del' ){
                           guardar_auditoria("ELIMINAR", "ELIMINO EL REGISTRO # ".$_POST['id']." EN G4");
                        }

                        echo $id_Usuario_Nuevo ;
                    } else {
                        echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                    }  
                }  
            }
        }
                                        
            

        

        if(isset($_GET["callDatosSubgrilla_0"])){

            $id = $_GET['id'];  

            
            $numero = $id;
                    

            $SQL = "SELECT G4_ConsInte__b, G4_C21, G4_C22, G4_C23, G4_C24, G4_C25, G4_C26, G4_C27 FROM ".$BaseDatos_systema.".G4  ";

            $SQL .= " WHERE G4_C23 = ".$numero; 

            $PEOBUS_VeRegPro__b = 0 ;
            $idUsuario = $_SESSION['IDENTIFICACION'];
            $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = 4";
            $query = $mysqli->query($peobus);
            

            while ($key =  $query->fetch_object()) {
                $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            }

            if($PEOBUS_VeRegPro__b != 0){
                $SQL .= " AND G4_Usuario = ".$idUsuario;
            }
        
            $SQL .= " ORDER BY G4_C21";

          //  echo $SQL;
            if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
                header("Content-type: application/xhtml+xml;charset=utf-8"); 
            } else { 
                header("Content-type: text/xml;charset=utf-8"); 
            } 

            $et = ">"; 
            echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
            echo "<rows>"; // be sure to put text data in CDATA

            $result = $mysqli->query($SQL);
            while( $fila = $result->fetch_object() ) {
                echo "<row asin='".$fila->G4_ConsInte__b."'>"; 
                echo "<cell>". ($fila->G4_ConsInte__b)."</cell>"; 
                

                echo "<cell>". ($fila->G4_C21)."</cell>";

                echo "<cell>". $fila->G4_C22."</cell>"; 



                echo "<cell>". $fila->G4_C25."</cell>"; 

                echo "<cell>". $fila->G4_C26."</cell>"; 

                echo "<cell>". $fila->G4_C27."</cell>"; 
                echo "</row>"; 
            } 
            echo "</rows>"; 
        }


        if(isset($_POST['guardar_flugrama'])){
            if(isset($_POST['mySavedModel'])){
                if(!empty($_POST['mySavedModel'])){
                    //$jsonArecorrer = json_decode($_POST['mySavedModel']);
                    $strin1 = str_replace("\\n","",$_POST['mySavedModel']);
                    $strin1 = str_replace('\\',"",$strin1);
                    $jsonArecorrer = json_decode($strin1);                    
                    $nodeArray = $jsonArecorrer->nodeDataArray;
                    $id_Usuario_Nuevo = $_POST['id_estrategia'];
                    $flujogramaAnterior = null;

                    $Xsql_json = "SELECT G2_C9  FROM ".$BaseDatos_systema.".G2 WHERE G2_ConsInte__b = ".$id_Usuario_Nuevo;
                    $cur_XsqlJson = $mysqli->query($Xsql_json);
                    $res_Cur_XsqlJson = $cur_XsqlJson->fetch_array();
                    if(!empty($res_Cur_XsqlJson['G2_C9'])){
                        $flujogramaAnterior = json_decode($res_Cur_XsqlJson['G2_C9']);
                    }
                    


                    $Lsql = "UPDATE ".$BaseDatos_systema.".G2 SET G2_C9 = '".$strin1."' WHERE G2_ConsInte__b = ".$id_Usuario_Nuevo;
                    if($mysqli->query($Lsql)===TRUE){
                        guardar_auditoria("ACTUALIZO", "ACTUALIZO EL FLUJOGRAMA DE LA ESTRATEGIA ".$id_Usuario_Nuevo);
                    }

                    $valido = 0;
                    $estPasId = 0;
                    $i = 0;
                    foreach ($nodeArray as $key) {

                        //primero validamos que ese paso exista
                        $Lsql = "SELECT * FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$key->key;
                        $queri_Lsql = $mysqli->query($Lsql);
                        if($queri_Lsql->num_rows > 0){ // Este paso existe
                            $resUlt = $queri_Lsql->fetch_array();
                            $LsqlPas = "UPDATE ".$BaseDatos_systema.".ESTPAS SET ESTPAS_ConsInte__ESTRAT_b = ".$id_Usuario_Nuevo.", ESTPAS_Nombre__b = '".$key->category."', ESTPAS_Tipo______b = ".$key->tipoPaso.", ESTPAS_Loc______b = '".$key->loc."', ESTPAS_key_____b = '".$key->key."' WHERE ESTPAS_ConsInte__b = ".$key->key;

                            if($mysqli->query($LsqlPas) === true){

                                $valido = 1;
                                if($key->tipoPaso == '7' || $key->tipoPaso == '8' || $key->tipoPaso == '9'){
                                    $id_Muestras = $resUlt['ESTPAS_ConsInte__MUESTR_b'];
                                    $id_Guion = $_POST['poblacion'];
                                    $Lsql = "SELECT * FROM ".$BaseDatos.".G".$id_Guion."_M".$id_Muestras;
                                    $resMuestra = $mysqli->query($Lsql);
                                    if($resMuestra){
                                        //Perfecto la muestra Existe y no nospreocupamos 
                                    }else{
                                        //Nooooooooooooo, It is a tramp, not found the fuck muestra creation, Why..Wilson Whyyyy
                                        $CreateMuestraLsql = "CREATE TABLE `".$BaseDatos."`.`G".$id_Guion."_M".$id_Muestras."` (
                                                  `G".$id_Guion."_M".$id_Muestras."_CoInMiPo__b` int(10) NOT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_ConIntUsu_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_Activo____b` smallint(5) DEFAULT '-1',
                                                  `G".$id_Guion."_M".$id_Muestras."_FecHorMinProGes__b` datetime DEFAULT NULL,
                                                  

                                                  `G".$id_Guion."_M".$id_Muestras."_UltiGest__b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_GesMasImp_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_FecUltGes_b` datetime DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_FeGeMaIm__b` datetime DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_Estado____b` int(10) DEFAULT '0',
                                                  `G".$id_Guion."_M".$id_Muestras."_TipoReintentoGMI_b` smallint(5) DEFAULT '0',
                                                  `G".$id_Guion."_M".$id_Muestras."_FecHorAge_b` datetime DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_FecHorAgeGMI_b` datetime DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_ConUltGes_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_CoGesMaIm_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_EstadoUG_b`  int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_EstadoGMI_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_UsuarioUG_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_UsuarioGMI_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_CanalUG_b` varchar(20) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_CanalGMI_b` varchar(20) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_SentidoUG_b` varchar(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_SentidoGMI_b` varchar(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_NumeInte__b` int(10) DEFAULT '0',
                                                  `G".$id_Guion."_M".$id_Muestras."_CantidadIntentosGMI_b` int(10) DEFAULT '0',
                                                  `G".$id_Guion."_M".$id_Muestras."_Comentari_b` longtext DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_ComentarioGMI_b` longtext DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_LinkContenidoUG_b` varchar(500) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_LinkContenidoGMI_b` varchar(500) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_DetalleCanalUG_b` varchar(255) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_DetalleCanalGMI_b` varchar(255) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_DatoContactoUG_b` varchar(255) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_DatoContactoGMI_b` varchar(255) DEFAULT NULL,

                                                  
                                                  `G".$id_Guion."_M".$id_Muestras."_TienGest__b` varchar(253) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_MailEnvi__b` smallint(5) DEFAULT NULL, 
                                                  `G".$id_Guion."_M".$id_Muestras."_GruRegRel_b` int(10) DEFAULT NULL, 
                                                  `G".$id_Guion."_M".$id_Muestras."_EfeUltGes_b` smallint(5) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_EfGeMaIm__b` smallint(5) DEFAULT NULL,                                         
                                                  

                                                  PRIMARY KEY (`G".$id_Guion."_M".$id_Muestras."_CoInMiPo__b`),
                                                  KEY `G".$id_Guion."_M".$id_Muestras."_Estado____b_Indice` (`G".$id_Guion."_M".$id_Muestras."_Estado____b`),
                                                  KEY `G".$id_Guion."_M".$id_Muestras."_ConIntUsu_b_Indice` (`G".$id_Guion."_M".$id_Muestras."_ConIntUsu_b`)
                                                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
                                        if($mysqli->query($CreateMuestraLsql) === true){
                                            //echo "Si creo la tabla";
                                               
                                        }else{
                                            echo "No pude crear la muetsra por => ".$mysqli->error;
                                        }
                                    }
                                }
                                guardar_auditoria("ACTUALIZO", "ACTUALIZO EL PASO CON EL ID ".$key->key);
                            }else{
                                $mysqli->error;
                            }

                        }else{ // lo estan creando nuevo
                            $LsqlPas = "INSERT INTO ".$BaseDatos_systema.".ESTPAS (ESTPAS_ConsInte__ESTRAT_b, ESTPAS_Nombre__b, ESTPAS_Tipo______b, ESTPAS_Loc______b, ESTPAS_key_____b) VALUES (".$id_Usuario_Nuevo.", '".$key->category."', ".$key->tipoPaso.", '".$key->loc."', '".$key->key."')";
                            if($mysqli->query($LsqlPas) === true){
                                $valido = 1;
                                $estPasId = $mysqli->insert_id;
                                if($key->tipoPaso == '7' || $key->tipoPaso == '8' || $key->tipoPaso == '9'){
                                    /* toca crear la muestra y asignarla al pinche paso */
                                    $id_Muestras = 0;
                                    $id_Guion = $_POST['poblacion'];
                                    $Lsql = "INSERT INTO ".$BaseDatos_systema.".MUESTR (MUESTR_Nombre____b, MUESTR_ConsInte__GUION__b) VALUES ('".$id_Guion."_MUESTRA_".rand()."', '".$id_Guion."')";
                                    if($mysqli->query($Lsql) === true){
                                        $id_Muestras = $mysqli->insert_id;
                                        /* toca asociarla al Paso */
                                        //echo "Entra aqui tambien y este es el id de la muestra".$id_Muestras;

                                        $CreateMuestraLsql = "CREATE TABLE `".$BaseDatos."`.`G".$id_Guion."_M".$id_Muestras."` (
                                                  `G".$id_Guion."_M".$id_Muestras."_CoInMiPo__b` int(10) NOT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_ConIntUsu_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_Activo____b` smallint(5) DEFAULT '-1',
                                                  `G".$id_Guion."_M".$id_Muestras."_FecHorMinProGes__b` datetime DEFAULT NULL,
                                                  

                                                  `G".$id_Guion."_M".$id_Muestras."_UltiGest__b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_GesMasImp_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_FecUltGes_b` datetime DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_FeGeMaIm__b` datetime DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_Estado____b` int(10) DEFAULT '0',
                                                  `G".$id_Guion."_M".$id_Muestras."_TipoReintentoGMI_b` smallint(5) DEFAULT '0',
                                                  `G".$id_Guion."_M".$id_Muestras."_FecHorAge_b` datetime DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_FecHorAgeGMI_b` datetime DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_ConUltGes_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_CoGesMaIm_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_EstadoUG_b`  int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_EstadoGMI_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_UsuarioUG_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_UsuarioGMI_b` int(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_CanalUG_b` varchar(20) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_CanalGMI_b` varchar(20) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_SentidoUG_b` varchar(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_SentidoGMI_b` varchar(10) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_NumeInte__b` int(10) DEFAULT '0',
                                                  `G".$id_Guion."_M".$id_Muestras."_CantidadIntentosGMI_b` int(10) DEFAULT '0',
                                                  `G".$id_Guion."_M".$id_Muestras."_Comentari_b` longtext DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_ComentarioGMI_b` longtext DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_LinkContenidoUG_b` varchar(500) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_LinkContenidoGMI_b` varchar(500) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_DetalleCanalUG_b` varchar(255) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_DetalleCanalGMI_b` varchar(255) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_DatoContactoUG_b` varchar(255) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_DatoContactoGMI_b` varchar(255) DEFAULT NULL,

                                                  
                                                  `G".$id_Guion."_M".$id_Muestras."_TienGest__b` varchar(253) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_MailEnvi__b` smallint(5) DEFAULT NULL, 
                                                  `G".$id_Guion."_M".$id_Muestras."_GruRegRel_b` int(10) DEFAULT NULL, 
                                                  `G".$id_Guion."_M".$id_Muestras."_EfeUltGes_b` smallint(5) DEFAULT NULL,
                                                  `G".$id_Guion."_M".$id_Muestras."_EfGeMaIm__b` smallint(5) DEFAULT NULL,                                         
                                                  

                                                  PRIMARY KEY (`G".$id_Guion."_M".$id_Muestras."_CoInMiPo__b`),
                                                  KEY `G".$id_Guion."_M".$id_Muestras."_Estado____b_Indice` (`G".$id_Guion."_M".$id_Muestras."_Estado____b`),
                                                  KEY `G".$id_Guion."_M".$id_Muestras."_ConIntUsu_b_Indice` (`G".$id_Guion."_M".$id_Muestras."_ConIntUsu_b`)
                                                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
                                        if($mysqli->query($CreateMuestraLsql) === true){
                                            //echo "Si creo la tabla";
                                            $PasoLsql = "UPDATE ".$BaseDatos_systema.".ESTPAS SET ESTPAS_ConsInte__MUESTR_b = ".$id_Muestras." WHERE ESTPAS_ConsInte__b = ".$estPasId;
                                            $mysqli->query($PasoLsql);

                                        }else{
                                            echo $mysqli->error;
                                        }
                                    }else{
                                        echo "No guardo la muestra => ".$mysqli->error;
                                    }
                                }
                                guardar_auditoria("INSERTO", "INSERTO EL PASO CON EL ID ".$key->key);
                            }else{
                                $mysqli->error;
                            }
                        }

                        //echo "  - ".$LsqlPas;
                
                        
                    }
                    


                    $datosConecciones = $jsonArecorrer->linkDataArray;

                    foreach ($datosConecciones as $conescciones) {

                        $LsqlPass = "SELECT ESTPAS_ConsInte__b FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__ESTRAT_b = ".$id_Usuario_Nuevo." AND ESTPAS_key_____b = '".$conescciones->from."' ";
                        $f = $mysqli->query($LsqlPass);
                        $from = $f->fetch_array();

                        $LsqlPassTo = "SELECT ESTPAS_ConsInte__b FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__ESTRAT_b = ".$id_Usuario_Nuevo." AND ESTPAS_key_____b = '".$conescciones->to."' ";
                        $t = $mysqli->query($LsqlPassTo);
                        $to = $t->fetch_array();
                        $texto = null;

                        if(isset($conescciones->text) && $conescciones->text != '' && !is_null($conescciones->text) && !empty($conescciones->text)){
                            $texto = $conescciones->text;
                        }

                        $Lsql = "SELECT * FROM ".$BaseDatos_systema.".ESTCON WHERE ESTCON_ConsInte__ESTPAS_Des_b = ".$from['ESTPAS_ConsInte__b']." AND ESTCON_ConsInte__ESTPAS_Has_b = ".$to['ESTPAS_ConsInte__b'];

                        $res = $mysqli->query($Lsql);
                        if($res->num_rows > 0){
                            /* existe */
                            $InsertCon = "UPDATE ".$BaseDatos_systema.".ESTCON SET ESTCON_Nombre____b = 'Conector', ESTCON_FromPort_b =  '".$conescciones->fromPort."', ESTCON_ToPort_b = '".$conescciones->toPort."', ESTCON_Coordenadas_b = '".json_encode($conescciones->points)."', ESTCON_Comentari_b = '".$texto."' WHERE ESTCON_ConsInte__ESTPAS_Des_b = ".$from['ESTPAS_ConsInte__b']." AND ESTCON_ConsInte__ESTPAS_Has_b = ".$to['ESTPAS_ConsInte__b'];
                        }else{
                            /* No existe */
                            $InsertCon = "INSERT INTO ".$BaseDatos_systema.".ESTCON(ESTCON_Nombre____b, ESTCON_ConsInte__ESTPAS_Des_b, ESTCON_ConsInte__ESTPAS_Has_b, ESTCON_FromPort_b, ESTCON_ToPort_b, ESTCON_Coordenadas_b, ESTCON_Comentari_b, ESTCON_ConsInte__ESTRAT_b) VALUES ('Conector', ".$from['ESTPAS_ConsInte__b'].", ".$to['ESTPAS_ConsInte__b'].", '".$conescciones->fromPort."', '".$conescciones->toPort."', '".json_encode($conescciones->points)."', '".$texto."', ".$id_Usuario_Nuevo.")";
                        }
                        
                        //echo $InsertCon." ";
    
                        if($mysqli->query($InsertCon) === true){
                            $valido = 1;
                        }
                    }

                    if($valido == 1){
                        echo "1";
                    }else{
                        Echo "Ocurrio un error";
                    }
                }
            }
        }

        if(isset($_POST['guardar_flugrama_simple'])){
            if(isset($_POST['mySavedModel'])){
                if(!empty($_POST['mySavedModel'])){
                    //$jsonArecorrer = json_decode($_POST['mySavedModel']);
                    $strin1 = str_replace("\\n","",$_POST['mySavedModel']);
                    $strin1 = str_replace('\\',"",$strin1);
                    $jsonArecorrer = json_decode($strin1);                    
                    $nodeArray = $jsonArecorrer->nodeDataArray;
                    $id_Usuario_Nuevo = $_POST['id_estrategia'];
                    $flujogramaAnterior = null;

                    $Xsql_json = "SELECT G2_C9 FROM ".$BaseDatos_systema.".G2 WHERE G2_ConsInte__b = ".$id_Usuario_Nuevo;
                    $cur_XsqlJson = $mysqli->query($Xsql_json);
                    $res_Cur_XsqlJson = $cur_XsqlJson->fetch_array();
                    if(!empty($res_Cur_XsqlJson['G2_C9'])){
                        $flujogramaAnterior = json_decode($res_Cur_XsqlJson['G2_C9']);
                    }
                    


                    $Lsql = "UPDATE ".$BaseDatos_systema.".G2 SET G2_C9 = '".$strin1."' WHERE G2_ConsInte__b = ".$id_Usuario_Nuevo;
                    if($mysqli->query($Lsql)===TRUE){
                        guardar_auditoria("ACTUALIZO", "ACTUALIZO EL FLUJOGRAMA DE LA ESTRATEGIA ".$id_Usuario_Nuevo);
                    }

                    $Lsql = "DELETE FROM ".$BaseDatos_systema.".ESTCON WHERE ESTCON_ConsInte__ESTRAT_b = ".$id_Usuario_Nuevo;
                    if($mysqli->query($Lsql)===TRUE){
                        guardar_auditoria("ELIMINO", "ELIMINO TODOS LOS CONECTORES DE LA ESTRATEGIA ".$id_Usuario_Nuevo);
                    }

                    $valido = 0;
                    $estPasId = 0;
      
                    foreach ($nodeArray as $key) {

                        //primero validamos que ese paso exista
                        $Lsql = "SELECT * FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$key->key;
                        $queri_Lsql = $mysqli->query($Lsql);
                        if($queri_Lsql->num_rows > 0){ // Este paso existe
                            $LsqlPas = "UPDATE ".$BaseDatos_systema.".ESTPAS SET ESTPAS_ConsInte__ESTRAT_b = ".$id_Usuario_Nuevo.", ESTPAS_Nombre__b = '".$key->category."', ESTPAS_Tipo______b = ".$key->tipoPaso.", ESTPAS_Loc______b = '".$key->loc."', ESTPAS_key_____b = '".$key->key."' WHERE ESTPAS_ConsInte__b = ".$key->key;

                            if($mysqli->query($LsqlPas) === true){
                                $valido = 1;
                                guardar_auditoria("ACTUALIZO", "ACTUALIZO EL PASO CON EL ID ".$key->key);
                            }else{
                                $mysqli->error;
                            }

                        }else{ // lo estan creando nuevo
                            $LsqlPas = "INSERT INTO ".$BaseDatos_systema.".ESTPAS (ESTPAS_ConsInte__ESTRAT_b, ESTPAS_Nombre__b, ESTPAS_Tipo______b, ESTPAS_Loc______b, ESTPAS_key_____b) VALUES (".$id_Usuario_Nuevo.", '".$key->category."', ".$key->tipoPaso.", '".$key->loc."', '".$key->key."')";
                            if($mysqli->query($LsqlPas) === true){
                                $valido = 1;
                                $estPasId = $mysqli->insert_id;
                                guardar_auditoria("INSERTO", "INSERTO EL PASO CON EL ID ".$key->key);
                            }else{
                                $mysqli->error;
                            }
                        }

                        //echo "  - ".$LsqlPas;
                    
                    }

                    
                    if($valido == 1){
                        echo $estPasId;
                    }else{
                        Echo "Ocurrio un error";
                    }
                }
            }
        }

        if(isset($_POST['borrarFlujograma'])){

            $intIdPaso_t = $_POST['id_paso'];

            EliminarCampañaEnCascada($intIdPaso_t);
            
        }

        if(isset($_POST['borrarFlujogramaLink'])){
            $from   = $_POST['from'];
            $to     = $_POST['to'];
            $Lsql = "DELETE FROM ".$BaseDatos_systema.".ESTCON WHERE ESTCON_ConsInte__ESTPAS_Des_b = ".$from." AND ESTCON_ConsInte__ESTPAS_Has_b = ".$to;

            if($mysqli->query($Lsql)===TRUE){
                guardar_auditoria("ELIMINO", "ELIMINO LA CONECCION ".$from." A ".$to);
            }else{
                echo "error => ".$mysqli->error;
            }
        }
    }

?>
