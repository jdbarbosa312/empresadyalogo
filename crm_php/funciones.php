<?php

	include('Jwt/jwt.php');


	function procesarPaso($intEstpas_p,$intIdBd_p,$intIdMuestra_p,$intConsInte_p){

		global $IP_FRONT_EJB;

	    $data =[
	        "strUsuario_t"   => 'crmphp',
	        "strToken_t"     => 'vYjQ0GOlEiD3i9ZACvS06Ro8xXkCQ7Xf',
	        "intEstpas_t" => (int)$intEstpas_p,
	        "intIdBd_t"  => (int)$intIdBd_p,
	        "intIdMuestra_t"  => (int)$intIdMuestra_p,
	        "intConsInte_t"  => (int)$intConsInte_p
	    ];

	    return consumirWSJSON($IP_FRONT_EJB."/dyjmsq/qs/pasos/procesar",$data );
	}

	function consumirWSJSON($strURL_p, $arrayDatos_p){

	    //Codificamos el arreglo en formato JSON
	    $strDatosJSON_t = json_encode($arrayDatos_p);
	    
	    //Inicializamos la conexion CURL al web service local para ser consumido
	    $objCURL_t = curl_init($strURL_p);
	    
	    //Asignamos todos los parametros del consumo
	    curl_setopt($objCURL_t, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	    curl_setopt($objCURL_t, CURLOPT_POSTFIELDS, $strDatosJSON_t); 
	    curl_setopt($objCURL_t,CURLOPT_RETURNTRANSFER,true);
	    curl_setopt($objCURL_t, CURLOPT_HTTPHEADER, array(
	        'Accept: application/json',
	        'Content-Type: application/json',
	        'Content-Length: ' . strlen($strDatosJSON_t))                                                                      
	    );

	    //Obtenemos la respuesta
	    $objRespuestaCURL_t = curl_exec($objCURL_t);

	    //Obtenemos el error 
	    $objRespuestaError_t = curl_error($objCURL_t);

	    //Cerramos la conexion
	    curl_close ($objCURL_t);

	    //Validamos la respuesta y generamos el rerno
	    if (isset($objRespuestaCURL_t)) {
	        //Decodificamos la respuesta en JSON y la retornamos
	        return $objRespuestaCURL_t;
	    }else {
	        return $objRespuestaError_t;
	    }
	}

	function getFechaAgenda($fecha, $operador, $cantidad){

		$operador = ($operador == 1) ? "+" : "-";
		
		if($fecha == -1){
			$actual = date("Y-m-d");
			$newFecha = date("Y-m-d", strtotime($actual."$operador $cantidad days"));
		}else{
			$newFecha = date("Y-m-d", strtotime($fecha."$operador $cantidad days"));
		}
		
		return $newFecha;
	}

	function getHoraAgenda($hora, $operador, $cantidad){

		$operador = ($operador == 1) ? "+" : "-";

		if($hora == -1){
			$actual = "00:00:00";
			$newHora = date("H:i:s", strtotime($actual."$operador $cantidad hour"));
		}else{
			$newHora = date("H:i:s", strtotime($hora."$operador $cantidad hour"));
		}

		return $newHora;
	}

	function getTipoDistribucion($paso, $tipoPaso){
		global $mysqli;
		global $BaseDatos;
		global $BaseDatos_systema;

		// Si el tipo de distribucion retorna 1 tratara de asignar un agente
		// Si el tipo de distribucion retorna 0 no agrega agente
		
		// Busco el tipo de distribucion en la campaña
		if($tipoPaso == 6){
			$sqlCampana = "SELECT CAMPAN_ConfDinam_b AS distribucion, CAMPAN_ConsInte__b AS id FROM ".$BaseDatos_systema.".CAMPAN JOIN ".$BaseDatos_systema.".ESTPAS ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b WHERE ESTPAS_ConsInte__b = ".$paso;
			$resSqlCampana = $mysqli->query($sqlCampana);

			if($resSqlCampana){
				if($resSqlCampana->num_rows > 0){
					$r = $resSqlCampana->fetch_array();

					//En campaña si es -1 es dinamica y no se asigna; si es 0 es predefinida y se asigna
					if($r['distribucion'] == -1){
						return array(0, $r['id'], 6);
					}else{
						return array(1, $r['id'], 6, $r['distribucion']);
					}
				}
			}
		}

		// Busco el tipo de distribucion si es backoffice
		if($tipoPaso == 9){
			$sqlBackoffice = "SELECT TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b AS distribucion, TAREAS_BACKOFFICE_ConsInte__b AS id, TAREAS_BACKOFFICE_ConsInte__PREGUN_estado_b AS condicion, TAREAS_BACKOFFICE_ConsInte__LISOPC_estado_b AS valor FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE JOIN ".$BaseDatos_systema.".ESTPAS ON ESTPAS_ConsInte__b = TAREAS_BACKOFFICE_ConsInte__ESTPAS_b WHERE ESTPAS_ConsInte__b = ".$paso;
			$resSqlBackoffice = $mysqli->query($sqlBackoffice);

			if ($resSqlBackoffice) {
				if ($resSqlBackoffice->num_rows > 0) {
					$r = $resSqlBackoffice->fetch_array();

					// En backoffice 1 no asigna agente; 2-3 asigna un agente
					if($r['distribucion'] != 1){
						return array(1, $r['id'], 9, $r['distribucion'], $r['condicion'], $r['valor']);
					}else{
						return array(0, $r['id'], 9);
					}
				}
			}
		}
		
		return array(0, 0);
	}

	function asignarAgente($condicion, $tipoPaso, $intBd_p, $intM_p, $distribucion = null, $variable = null, $valor = null){
		global $mysqli;
		global $BaseDatos;
		global $BaseDatos_systema;

		if($tipoPaso == 6){
			$sql = "SELECT ASITAR_ConsInte__USUARI_b as agente FROM ".$BaseDatos_systema.".ASITAR LEFT JOIN DYALOGOCRM_WEB.G".$intBd_p."_M".$intM_p." ON G".$intBd_p."_M".$intM_p."_ConIntUsu_b = ASITAR_ConsInte__USUARI_b AND G".$intBd_p."_M".$intM_p."_Estado____b <> 3 WHERE ASITAR_ConsInte__CAMPAN_b = ".$condicion." GROUP BY (ASITAR_ConsInte__USUARI_b) ORDER BY COUNT(G".$intBd_p."_M".$intM_p."_CoInMiPo__b) LIMIT 1";
			$resSql = $mysqli->query($sql);
			
			if($resSql){
				if($resSql->num_rows > 0){
					$r = $resSql->fetch_array();
					return $r['agente'];
				}
			}
		}

		if($tipoPaso == 9){

			if($distribucion == 2){
				$sql = "SELECT ASITAR_BACKOFFICE_ConsInte__USUARI_b as agente FROM ".$BaseDatos_systema.".ASITAR_BACKOFFICE 
					LEFT JOIN DYALOGOCRM_WEB.G".$intBd_p."_M".$intM_p." 
						ON G".$intBd_p."_M".$intM_p."_ConIntUsu_b = ASITAR_BACKOFFICE_ConsInte__USUARI_b AND G".$intBd_p."_M".$intM_p."_Estado____b <> 3 
					LEFT JOIN DYALOGOCRM_WEB.G".$intBd_p." 
						ON G".$intBd_p."_ConsInte__b = G".$intBd_p."_M".$intM_p."_CoInMiPo__b AND G".$intBd_p."_C".$variable." = ".$valor."
					WHERE ASITAR_BACKOFFICE_ConsInte__TAREAS_BACKOFFICE_b = ".$condicion." 
					GROUP BY (ASITAR_BACKOFFICE_ConsInte__USUARI_b) 
					ORDER BY COUNT(G".$intBd_p."_ConsInte__b) LIMIT 1";
			}else{
				$sql = "SELECT ASITAR_BACKOFFICE_ConsInte__USUARI_b as agente FROM ".$BaseDatos_systema.".ASITAR_BACKOFFICE LEFT JOIN DYALOGOCRM_WEB.G".$intBd_p."_M".$intM_p." ON G".$intBd_p."_M".$intM_p."_ConIntUsu_b = ASITAR_BACKOFFICE_ConsInte__USUARI_b AND G".$intBd_p."_M".$intM_p."_Estado____b <> 3 WHERE ASITAR_BACKOFFICE_ConsInte__TAREAS_BACKOFFICE_b = ".$condicion." GROUP BY (ASITAR_BACKOFFICE_ConsInte__USUARI_b) ORDER BY COUNT(G".$intBd_p."_M".$intM_p."_CoInMiPo__b) LIMIT 1";
			}
			$resSql = $mysqli->query($sql);
			
			if($resSql){
				if($resSql->num_rows > 0){
					$r = $resSql->fetch_array();
					return $r['agente'];
				}
			}
		}
		
		return 0;
	}

	// Valida si el id del paso esta relacionada a una campana
	function validarPasoSalidaEsCampana($intPasoId){

		global $mysqli;
		global $BaseDatos_systema;

		$Ssql = "SELECT ESTPAS_Tipo______b AS TipoPaso FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$intPasoId." LIMIT 1";
		$res = $mysqli->query($Ssql);

		if($res){
			if($res->num_rows > 0){
				$r = $res->fetch_array();

				if($r['TipoPaso'] == 6 || $r['TipoPaso'] == 1){
					return true;
				}
			}
		}

		return false;
	}

	// Traigo la muestra de una campaña basado en el id del paso
	function muestraCampanaPasoSalida($intPasoId){
		global $mysqli;
		global $BaseDatos_systema;

		$Ssql = "SELECT CAMPAN_ConsInte__MUESTR_b AS muestra FROM ".$BaseDatos_systema.".CAMPAN INNER JOIN ".$BaseDatos_systema.".ESTPAS ON CAMPAN_ConsInte__b = ESTPAS_ConsInte__CAMPAN_b WHERE ESTPAS_ConsInte__b = ".$intPasoId." LIMIT 1";
		$res = $mysqli->query($Ssql);

		$muestra = 0;

		if($res){
			if($res->num_rows > 0){
				$r = $res->fetch_array();
				$muestra = $r['muestra'];
			}
		}

		return $muestra;
	}

	function insertarEnMuestra($intBd_p, $intM_p, $intC_p, $intAgente_p, $resucitarRegistro = false, $intE_p = null, $intF_p = null, $intH_p = null){

		global $mysqli;
		global $BaseDatos;
		global $BaseDatos_systema;

		$intEstado_t = 0;
		$strFecha_t = ",G".$intBd_p."_M".$intM_p."_FecHorAge_b";
		$strFechaV_t = ",NULL AS Agenda";
		$strFechaVU_t = "NULL";

		$strAgente = ",G".$intBd_p."_M".$intM_p."_ConIntUsu_b";
		$strAgenteV = ",NULL AS Agente";
		$strAgenteVU = "NULL";

		// if (!is_null($intF_p) && $intF_p != -1) {

		// 	$strFechaV_t = ",G".$intBd_p."_C".$intF_p;

		// }

		if($intAgente_p > 0){
			$strAgenteV = ",".$intAgente_p;
			$strAgenteVU = $intAgente_p;
		}

		if (!is_null($intE_p) && $intE_p == -1) {
			if (!is_null($intF_p) && $intF_p != "") {
				if (!is_null($intH_p) && $intH_p != "") {

					$intEstado_t = 2;
					$strFechaV_t = ",'".trim($intF_p)." ".trim($intH_p)."'";
					$strFechaVU_t = "'".trim($intF_p)." ".trim($intH_p)."'";
				}
			}
			$intEstado_t = 2;
		}

		$strSQLInsertM_t = "INSERT INTO ".$BaseDatos.".G".$intBd_p."_M".$intM_p." (G".$intBd_p."_M".$intM_p."_CoInMiPo__b,G".$intBd_p."_M".$intM_p."_Activo____b,G".$intBd_p."_M".$intM_p."_Estado____b,G".$intBd_p."_M".$intM_p."_TipoReintentoGMI_b,G".$intBd_p."_M".$intM_p."_NumeInte__b,G".$intBd_p."_M".$intM_p."_CantidadIntentosGMI_b".$strFecha_t."".$strAgente.") SELECT G".$intBd_p."_ConsInte__b,-1 AS Activo____b,".$intEstado_t." AS Estado____b,0 AS TipoReintentoGMI_b,0 AS NumeInte__b,0 AS CantidadIntentosGMI_b".$strFechaV_t."".$strAgenteV."  FROM ".$BaseDatos.".G".$intBd_p." WHERE G".$intBd_p."_ConsInte__b = ".$intC_p;
		if($resucitarRegistro){
			$strSQLInsertM_t = "UPDATE ".$BaseDatos.".G".$intBd_p."_M".$intM_p." SET G".$intBd_p."_M".$intM_p."_Activo____b = -1, G".$intBd_p."_M".$intM_p."_Estado____b = ".$intEstado_t." ".$strFecha_t." = ".$strFechaVU_t." ".$strAgente." = ".$strAgenteVU." WHERE G".$intBd_p."_M".$intM_p."_CoInMiPo__b = ".$intC_p;
		}
		
		if ($mysqli->query($strSQLInsertM_t)) {

			return true;

		}else{

			return false;

		}
	}

	function DispararProceso($intPasoId_p, $intRegistroId_p = null, $intUsuAsigado = null){

		global $mysqli;
		global $BaseDatos;
		global $BaseDatos_systema;

		$strSQLEstcon_t = "SELECT ESTCON_Consulta_sql_b,ESTRAT_ConsInte_GUION_Pob,ESTPAS_ConsInte__MUESTR_b,ESTCON_Tipo_Insercion_b, ESTCON_ConsInte_PREGUN_Fecha_b, ESTCON_ConsInte_PREGUN_Hora_b, ESTCON_Operacion_Fecha_b, ESTCON_Operacion_Hora_b, ESTCON_Cantidad_Fecha_b, ESTCON_Cantidad_Hora_b, ESTCON_resucitar_registro, CAMPAN_ConsInte__MUESTR_b, ESTPAS_Tipo______b, ESTPAS_ConsInte__b FROM ".$BaseDatos_systema.".ESTCON JOIN ".$BaseDatos_systema.".ESTPAS ON ESTCON_ConsInte__ESTPAS_Has_b = ESTPAS_ConsInte__b  JOIN ".$BaseDatos_systema.".ESTRAT ON ESTPAS_ConsInte__ESTRAT_b = ESTRAT_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b WHERE ESTCON_ConsInte__ESTPAS_Des_b = ".$intPasoId_p." AND ESTPAS_Tipo______b IN (6,7,8,9,13)";
		$resSQLEstcon_t = $mysqli->query($strSQLEstcon_t);

		$algo = "";

		$pasoSalidaEsCampana = false;
		// Valido si el paso de donde salen es campana
		if (validarPasoSalidaEsCampana($intPasoId_p)) {
			
			$pasoSalidaEsCampana = true;

			// Traigo la muestra de la campana para armar el nuevo $strSQLInsert_t
			$muestraCampana = muestraCampanaPasoSalida($intPasoId_p);
		}

		if ($resSQLEstcon_t->num_rows > 0) {
			while ($row = $resSQLEstcon_t->fetch_object()) {

				// Extraigo solo el where de la consulta
				$strFiltros_t = $row->ESTCON_Consulta_sql_b;
				$strFiltros_t = explode("WHERE", $strFiltros_t);

				if(isset($strFiltros_t[1])){
					$strFiltros_t = " AND ( ". $strFiltros_t[1]." )";
				}else{
					$strFiltros_t = "";
				}

				$intBase = $row->ESTRAT_ConsInte_GUION_Pob;
				$strTipoPaso_t = $row->ESTPAS_Tipo______b;
				$intIdPasoCondicion_t = $row->ESTPAS_ConsInte__b;

				$resucitarRegistro = $row->ESTCON_resucitar_registro;

				$arrEstcon = array(
					"campoFecha" 	=> $row->ESTCON_ConsInte_PREGUN_Fecha_b,
					"campoHora" 	=> $row->ESTCON_ConsInte_PREGUN_Hora_b,
					"operacionFecha"=> $row->ESTCON_Operacion_Fecha_b,
					"operacionHora" => $row->ESTCON_Operacion_Hora_b,
					"cantidadFecha" => $row->ESTCON_Cantidad_Fecha_b,
					"cantidadHora" 	=> $row->ESTCON_Cantidad_Hora_b
				);

				if ($strTipoPaso_t == 6 || $strTipoPaso_t == 1) {

					$intMuestra = $row->CAMPAN_ConsInte__MUESTR_b;

				}else{

					$intMuestra = $row->ESTPAS_ConsInte__MUESTR_b;

				}

				$strTipoInsercion_t = $row->ESTCON_Tipo_Insercion_b;

				// Necesito traer las fechas para luego realizar el agendamiento si aplica
				$strFechas = '';
				if(!is_null($arrEstcon['campoFecha']) && $arrEstcon['campoFecha'] != -1 && $arrEstcon['campoFecha'] != 0){
					$strFechas .= ", DATE(G".$intBase."_C".$arrEstcon['campoFecha'].") AS fecha";
				}

				if(!is_null($arrEstcon['campoHora']) && $arrEstcon['campoHora'] != -1 && $arrEstcon['campoHora'] != 0){
					$strFechas .= ", TIME(G".$intBase."_C".$arrEstcon['campoHora'].") AS hora";
				}
				
				// Valido si el paso de salida es campana para cambiar la consulta y ponerle un join mas, esto es por que hay condiciones que
				// validan por registros de la muestra de donde sale la flecha
				if($pasoSalidaEsCampana){

					$strSQLInsert_t = "SELECT G".$intBase."_ConsInte__b AS id ".$strFechas.", B.G".$intBase."_M".$intMuestra."_CoInMiPo__b AS muestra FROM ".$BaseDatos.".G".$intBase." A LEFT JOIN ".$BaseDatos.".G".$intBase."_M".$intMuestra." B ON A.G".$intBase."_ConsInte__b = B.G".$intBase."_M".$intMuestra."_CoInMiPo__b LEFT JOIN ".$BaseDatos.".G".$intBase."_M".$muestraCampana." C ON A.G".$intBase."_ConsInte__b = C.G".$intBase."_M".$muestraCampana."_CoInMiPo__b WHERE G".$intBase."_ConsInte__b = ".$intRegistroId_p." ".$strFiltros_t;
				}else{

					// Crea la consulta normalita de un paso que no sea campana
					$strSQLInsert_t = "SELECT G".$intBase."_ConsInte__b AS id ".$strFechas.", B.G".$intBase."_M".$intMuestra."_CoInMiPo__b AS muestra FROM ".$BaseDatos.".G".$intBase." A LEFT JOIN ".$BaseDatos.".G".$intBase."_M".$intMuestra." B ON A.G".$intBase."_ConsInte__b = B.G".$intBase."_M".$intMuestra."_CoInMiPo__b WHERE G".$intBase."_ConsInte__b = ".$intRegistroId_p." ".$strFiltros_t;
				}

				// Busco los registros
				$resSQLInsert_t = $mysqli->query($strSQLInsert_t);
				
				$algo .= "|".$strSQLInsert_t;
				
				if ($resSQLInsert_t) {
					if ($resSQLInsert_t->num_rows > 0) {

						while ($fila = $resSQLInsert_t->fetch_object()) {

							//Validacion adicional solo debe ejecutar el registro enviado
							if($fila->id == $intRegistroId_p){
								// Buscar si se asigna el agente aqui, cuando el registro cumple con kas condiciones de insertar
								$arrDistribucion = getTipoDistribucion($intIdPasoCondicion_t, $strTipoPaso_t);		
								if($arrDistribucion[0] == 1){
									if(!is_null($intUsuAsigado) && $intUsuAsigado > 0){
										$agente = $intUsuAsigado;
									}else{
										// Verifico si es backoffice o campaña
										if($arrDistribucion[2] == 6){
											$agente = asignarAgente($arrDistribucion[1], $strTipoPaso_t, $intBase, $intMuestra);
										}else if($arrDistribucion[2] == 9){
											$agente = asignarAgente($arrDistribucion[1], $strTipoPaso_t, $intBase, $intMuestra, $arrDistribucion[3], $arrDistribucion[4], $arrDistribucion[5]);
										}
									}
								}else{
									$agente = 0;
								}
								
								// Aqui valido si el registro es nuevo o ya existe
								if(is_null($fila->muestra)){
									if ($strTipoInsercion_t == 0) {
									
										if (insertarEnMuestra($intBase,$intMuestra,$fila->id, $agente)) {
		
											if ($strTipoPaso_t == 7 || $strTipoPaso_t == 8) {
												
												 procesarPaso($intIdPasoCondicion_t,$intBase,$intMuestra,$fila->id);
											}
											
										}
		
									}else{
										// Si es agenda realizo el calculo de la insercion
										$campoFecha = isset($fila->fecha) ? $fila->fecha : -1;
										$campoHora = isset($fila->hora) ? $fila->hora : -1;
		
										$fecha = getFechaAgenda($campoFecha, $arrEstcon['operacionFecha'], $arrEstcon['cantidadFecha']);
			
										$hora = getHoraAgenda($campoHora, $arrEstcon['operacionHora'], $arrEstcon['cantidadHora']);
			
										insertarEnMuestra($intBase, $intMuestra, $fila->id, $agente, false, -1, $fecha, $hora);
									}
								}else{
									// Sino trato de resucitar el registro
									if($resucitarRegistro){
										if ($strTipoInsercion_t == 0) {
	
											if (insertarEnMuestra($intBase,$intMuestra,$fila->id, $agente, true)) {
			
												if ($strTipoPaso_t == 7 || $strTipoPaso_t == 8) {
													
													procesarPaso($intIdPasoCondicion_t,$intBase,$intMuestra,$fila->id);
												}
												
											}
			
										}else{
											// Si es agenda realizo el calculo de la insercion
											$campoFecha = isset($fila->fecha) ? $fila->fecha : -1;
											$campoHora = isset($fila->hora) ? $fila->hora : -1;
			
											$fecha = getFechaAgenda($campoFecha, $arrEstcon['operacionFecha'], $arrEstcon['cantidadFecha']);
				
											$hora = getHoraAgenda($campoHora, $arrEstcon['operacionHora'], $arrEstcon['cantidadHora']);
				
											insertarEnMuestra($intBase, $intMuestra, $fila->id, $agente, true, -1, $fecha, $hora);
										}
									}
								}
							}
						}

					}
				}

			}	


		}

		return $algo;
	}

	 /**
	 * JDBD-2020-05-03 : Se obtiene un fragmento cadena para MySQL con el campo mas operador mas valor Eje: (G###_C### = 'valor').
	 * @param String - Nombre del campo de la tabla Guion en la base.
	 * @param String - Operador Ejem: = , != , <, >, etc.
	 * @param Integer - Tipo de campo Ejem_: 3=Int, 1=Text, 6=Lista, etc.
	 * @param String - Valor o dato a filtar segun para el campo y tipo de campo que venga.
	 * @return String - Retorna la condicion armada para MySQL segun el campo, operador y valor a filtrar.
	 */
	function operadorYFiltro($strCampo_p,$strOperador_p,$intTipo_p,$valor_p){

		$strSQLFiltros_t = "";

		if ($intTipo_p != 5 && $intTipo_p != 10) {

			$strSQLFiltros_t .= $strCampo_p." ";

			if ($intTipo_p == 6 || $intTipo_p == 4 || $intTipo_p == 3){

				if ($intTipo_p == 6) {
					if ($valor_p == "0") {
						switch ($strOperador_p) {
							case '!=':
								$strSQLFiltros_t .= " IS NOT NULL ";
								break;
							default:
								$strSQLFiltros_t .= " IS NULL ";
								break;
						}
					}else{
						$strSQLFiltros_t .= $strOperador_p." ".$valor_p." ";
					}
				}else{
					if ($valor_p == "") {
						switch ($strOperador_p) {
							case '!=':
								$strSQLFiltros_t .= " IS NOT NULL ";
								break;
							default:
								$strSQLFiltros_t .= " IS NULL ";
								break;
						}
					}else{
						$strSQLFiltros_t .= $strOperador_p." ".$valor_p." ";
					}
				}


			}else{

				if ($valor_p == "") {
					switch ($strOperador_p) {
						case '!=':
							$strSQLFiltros_t = "(".$strCampo_p." IS NOT NULL OR ".$strCampo_p." != '') ";
							break;
						default:
							$strSQLFiltros_t = "(".$strCampo_p." IS NULL OR ".$strCampo_p." = '') ";
							break;
					}
				}else{
					switch ($strOperador_p) {
						case 'LIKE_1':
							$strSQLFiltros_t .= " LIKE '".$valor_p."%' ";
							break;
						case 'LIKE_2':
							$strSQLFiltros_t .= " LIKE '%".$valor_p."%' ";
							break;
						case 'LIKE_3':
							$strSQLFiltros_t .= " LIKE '%".$valor_p."' ";
							break;
						default:
							$strSQLFiltros_t .= $strOperador_p." '".$valor_p."' ";
							break;
					}
				}


			}			
		}elseif($intTipo_p == 5){

			if ($valor_p == "") {
				switch ($strOperador_p) {
					case '!=':
						$strSQLFiltros_t .= $strCampo_p." IS NOT NULL ";
						break;
					default:
						$strSQLFiltros_t .= $strCampo_p." IS NULL ";
						break;
				}
			}else{
				switch ($strOperador_p) {
					case 'LIKE_1':
						$strSQLFiltros_t .= "DATE_FORMAT(".$strCampo_p.",'%Y-%m-%d') LIKE '".$valor_p."%' ";
						break;
					case 'LIKE_2':
						$strSQLFiltros_t .= "DATE_FORMAT(".$strCampo_p.",'%Y-%m-%d') LIKE '%".$valor_p."%' ";
						break;
					case 'LIKE_3':
						$strSQLFiltros_t .= "DATE_FORMAT(".$strCampo_p.",'%Y-%m-%d') LIKE '%".$valor_p."' ";
						break;
					default:
						$strSQLFiltros_t .= "DATE_FORMAT(".$strCampo_p.",'%Y-%m-%d') ".$strOperador_p." '".$valor_p."' ";
						break;
				}
			}			


		}elseif($intTipo_p == 10){

			$valor_p = str_replace(" ", "", $valor_p);

			switch ($strOperador_p) {
				case 'LIKE_1':
					$strSQLFiltros_t .= "DATE_FORMAT(".$strCampo_p.",'%H:%i:%s') LIKE '".$valor_p."%' ";
					break;
				case 'LIKE_2':
					$strSQLFiltros_t .= "DATE_FORMAT(".$strCampo_p.",'%H:%i:%s') LIKE '%".$valor_p."%' ";
					break;
				case 'LIKE_3':
					$strSQLFiltros_t .= "DATE_FORMAT(".$strCampo_p.",'%H:%i:%s') LIKE '%".$valor_p."' ";
					break;
				default:
					$strSQLFiltros_t .= "DATE_FORMAT(".$strCampo_p.",'%H:%i:%s') ".$strOperador_p." '".$valor_p."' ";
					break;
			}

		}

		return $strSQLFiltros_t;

	}

	/**
	 * JDBD - Se obtienen el texto traducido de la opcion de la lista.
	 * @param Integer id de LISOPC.
	 * @param Integer tipo del campo.
	 * @return string - texto traducido de la opcion.
	 */
	function traductor($val_p,$tip_p){
		global $mysqli;
		global $BaseDatos;
		global $BaseDatos_systema;

		
		$arrTipList_t = [6,11,13];
		if ($tip_p == 6 || $tip_p == 11 || $tip_p == 13) {
				
			$opcion = "SELECT LISOPC_Nombre____b as nom 
					   FROM ".$BaseDatos_systema.".LISOPC 
					   WHERE LISOPC_ConsInte__b = ".$val_p;

		    $opcion = $mysqli->query($opcion);

		    $opcion = $opcion->fetch_array();

		    return $opcion["nom"];
		}else{
			return $val_p;
		}	
	}	
	/*YCR 2019-10-11
	*Funcion para crear usuario por defecto
	*/
	function crearMiembroDefault($idCampan){
		global $mysqli;
		global $BaseDatos;
		global $BaseDatos_systema;

		if($idCampan != 0 || $idCampan != ''){
			$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$idCampan;
	        $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	        $datoCampan = $res_Lsql_Campan->fetch_array();
	        $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	        $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	        $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	        $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];

	        $Lsql = "SELECT * FROM  ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = -1 ";
	        if( ($result = $mysqli->query($Lsql) ) == TRUE  ){
	        	if($result->num_rows ==  0){
	        		$fecha_creacion = date('Y-m-d H:i:s');
	        		$LsqlBD= "INSERT INTO ".$BaseDatos.".".$str_Pobla_Campan." (".$str_Pobla_Campan."_ConsInte__b,".$str_Pobla_Campan."_FechaInsercion)
							VALUES ('-1','".$fecha_creacion."')";						
					$mysqli->query($LsqlBD);
					$muestraCompleta = $str_Pobla_Campan."_M".$int_Muest_Campan;
					$LsqlMuestra="INSERT INTO  ".$BaseDatos.".".$muestraCompleta."  (".$muestraCompleta ."_CoInMiPo__b ,  ".$muestraCompleta."_NumeInte__b, ".$muestraCompleta."_Estado____b ) VALUES ('-1', '0','3')";
					$mysqli->query($LsqlMuestra);						

	        	}
	        }
		}
	}
	/**
	 * fn   valida que los campos no esten vacios o nulos
	 */
	function validador($strCampo){
		if($strCampo == "" || $strCampo == null ){
			$valor="NULL";
		}else{
			$valor=$strCampo;
		}
		return $valor;
	}
	//validar el acceso del usuario por token o por Session
	function detalle_canal($n,$p,$c){
		global $mysqli;
		global $BaseDatos;
		global $BaseDatos_systema;

    	$nombre_usuario = "Numero no se identifica";

	    if (is_numeric($n)) {

	        $consul = "SELECT B.id, B.patron FROM dyalogo_telefonia.pasos_troncales A JOIN dyalogo_telefonia.tipos_destino B ON A.id_tipos_destino = B.id  WHERE A.id_campana = ".$c;

	        $obj = $mysqli->query($consul);

	        $canPat = 0;
	        $canNum = 0;
	        $idDestino = 0;
            if($obj -> num_rows > 0){
                while($obje = $obj->fetch_object()){
                    $canPat = strlen($obje->patron);
                    $canNum = strlen($n);
                    if ($canPat == $canNum) {
                        $idDestino = $obje->id;
                    }

               }
            }

	        $consul = "SELECT id_troncal FROM dyalogo_telefonia.pasos_troncales WHERE id_estpas = ".$p." AND id_tipos_destino  = ".$idDestino;

	        $obj = $mysqli->query($consul);

	        $id_troncal = 0;
	        while($obje = $obj->fetch_object()){
	            $id_troncal = $obje->id_troncal;
	       }

	       $consul = "SELECT nombre_usuario FROM dyalogo_telefonia.dy_troncales WHERE id = ".$id_troncal;

	       $obj = $mysqli->query($consul);


	        while($obje = $obj->fetch_object()){
	            $nombre_usuario = $obje->nombre_usuario;
	       }
	    }    

    	return $nombre_usuario;

    }

	function getMailUser($token = null){
		if(isset($_SESSION['CODIGO'])){
			return $_SESSION['CODIGO'];
		}

		if(!is_null($token)){
			$user = JWT::decode(trim($token, '"'));
			if(!is_null($user)){
				$nombres;
				//var_dump($user);
				foreach ($user as $key) {
					$nombres = $key->NOMBRES ;
				}
				return $nombres;
			}else{
				return 0;
			}
		}

		if(!isset($_SESSION['CODIGO']) && is_null($token)){
			return 0;
		}

	}	
	
	function getAccesoUser($token = null){

		if(isset($_SESSION['ACCESO'])){
			return $_SESSION['ACCESO'];
		}

		if(!is_null($token)){
			$user = JWT::decode(trim($token, '"'));
			if(!is_null($user)){
				$acceso = null;
				foreach ($user as $key) {
					$acceso = $key->ACCESO ;
				}
				return $acceso;
			}else{
				return 0;
			}
		}

		if(!isset($_SESSION['ACCESO']) && is_null($token)){
			return 0;
		}
	}


	function getIdentificacionUser($token = null){
	
		if(isset($_SESSION['IDENTIFICACION'])){
			return $_SESSION['IDENTIFICACION'];
		}

		if(!is_null($token)){
			$user = JWT::decode(trim($token, '"'));
			if(!is_null($user)){
				$id;
				foreach ($user as $key) {
					$id = $key->IDENTIFICACION ;
				}
				return $id;
			}else{
				return 0;
			}
		}

		if(!isset($_SESSION['IDENTIFICACION']) && is_null($token)){
			return 0;
		}
	}

	function getIdToken($token){
		$user = JWT::decode(trim($token, '"'));
		if(!is_null($user)){
			//var_dump($user);
			$id;
			foreach ($user as $key) {
				$id = $key->IDENTIFICACION ;
			}
			return $id;
		}else{
			return 0;
		}
	}

	function getNombreUser($token = null){
		if(isset($_SESSION['NOMBRES'])){
			return $_SESSION['NOMBRES'];
		}

		if(!is_null($token)){
			$user = JWT::decode(trim($token, '"'));
			if(!is_null($user)){
				$nombres;
				//var_dump($user);
				foreach ($user as $key) {
					$nombres = $key->NOMBRES ;
				}
				return $nombres;
			}else{
				return 0;
			}
		}

		if(!isset($_SESSION['NOMBRES']) && is_null($token)){
			return 0;
		}

	}


	function getProyectoUser($token = null){

		
		if(isset($_SESSION['PROYECTO_CRM'])){
			return $_SESSION['PROYECTO_CRM'];
		}

		if(!is_null($token)){
			$user = JWT::decode(trim($token, '"'));
			if(!is_null($user)){
				$nombres;
				//var_dump($user);
				foreach ($user as $key) {
					$nombres = $key->PROYECTO_CRM ;
				}
				return $nombres;
			}else{
				return 0;
			}
		}

		if(!isset($_SESSION['PROYECTO_CRM']) && is_null($token)){
			return 0;
		}
	}

	function getFechaUser($token = null){
		if(isset($_SESSION['FECHA'])){
			return $_SESSION['FECHA'];
		}

		if(!is_null($token)){
			$user = JWT::decode(trim($token, '"'));
			if(!is_null($user)){
				$fecha;
				//var_dump($user);
				foreach ($user as $key) {
					$fecha = $key->FECHA ;
				}
				return $fecha;
			}else{
				return 0;
			}
		}

		if(!isset($_SESSION['FECHA']) && is_null($token)){
			return 0;
		}
	}

    
    function NombreAgente($id_usuari){
		global $mysqli;
		global $BaseDatos;
		global $BaseDatos_systema;
        
        $strNameAgente="SELECT USUARI_Nombre____b FROM DYALOGOCRM_SISTEMA.USUARI WHERE USUARI_ConsInte__b = {$id_usuari} LIMIT 1";
        $strNameAgente=$mysqli->query($strNameAgente);
        if($strNameAgente && $strNameAgente->num_rows > 0 ){
            $strNameAgente=$strNameAgente->fetch_object();
            $strNameAgente=$strNameAgente->USUARI_Nombre____b;
        }else{
            $strNameAgente='N/A';
        }
        
        return $strNameAgente;
    }
	/*NBG 2020-10-21
	*Funcion para obtener los patrones de marcación del huesped
	*/
    function ObtenerPatron($id_huesped,$id_campan=null){
		global $mysqli;       
        
        if(is_null($id_campan)){
            $sql=$mysqli->query("SELECT patron_validacion AS patron_regexp,patron, patron_ejemplo FROM dyalogo_telefonia.tipos_destino where id_huesped={$id_huesped} AND patron_validacion IS NOT NULL;");
        }elseif(is_numeric($id_campan) and $id_campan >0){
            $sql=$mysqli->query("select b.patron_validacion as patron_regexp,patron, patron_ejemplo from dyalogo_telefonia.pasos_troncales a join dyalogo_telefonia.tipos_destino b on a.id_tipos_destino=b.id where id_campana={$id_campan} and b.id_huesped={$id_huesped};");
        }
        if($sql && $sql->num_rows > 0){
            $regexp= array();
			$patron= array();
			$patronEjemplo = array();
            $i=0;
            while($row = $sql->fetch_assoc()) {
                $regexp[$i]=$row['patron_regexp'];
                $patron[$i]=$row['patron'];
                $patronEjemplo[$i] = $row['patron_ejemplo'];
                $i++;
            }
            $result=array(
                'patron_regexp' =>$regexp,
                'patron' =>$patron,
                'patron_ejemplo' => $patronEjemplo
            );
            return json_encode($result);
        }else{
            return false;
        }      
    }
?>