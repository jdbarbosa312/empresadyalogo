<?php
    session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    date_default_timezone_set('America/Bogota');
    include("conexion.php");
    include("Jwt/jwt.php");

    if(isset($_POST['id_cbx']) && !isset($_GET['close_token'])){
        $Lsql = "select USUARI_ConsInte__b, USUARI_Codigo____b, USUARI_Nombre____b , USUARI_ConsInte__b, USUARI_FechCrea__b , USUARI_InPeToGu__b, USUARI_ConsInte__PROYEC_b FROM ".$BaseDatos_systema.".USUARI WHERE USUARI_UsuaCBX___b = '".$_POST['id_cbx']."' ";


        $query = $mysqli->query($Lsql) or trigger_error($mysqli->error." [$Lsql]");
        if($query->num_rows > 0) {
            // creamos la session

            $datosSesion = array();
            $nombres = null;
            $userid = 0;
            while($key = $query->fetch_object()){
                $datosSesion[0]['CODIGO'] = $key->USUARI_Codigo____b;
                $datosSesion[0]['NOMBRES']   = $key->USUARI_Nombre____b;
                $datosSesion[0]['IDENTIFICACION'] = $key->USUARI_ConsInte__b;
                $datosSesion[0]['FECHA']  = $key->USUARI_FechCrea__b;
                $datosSesion[0]['ACCESO']  = $key->USUARI_InPeToGu__b;
                $datosSesion[0]['PROYECTO_CRM']  = $key->USUARI_ConsInte__PROYEC_b;
                $datosSesion[0]['HUESPED_CRM'] = $key->USUARI_ConsInte__PROYEC_b;
                $datosSesion[0]['LOGIN_OK']  = true;
                $datosSesion[0]['iat'] = time();
                $datosSesion[0]['exp'] = time() + 60;
                $nombres = $key->USUARI_Nombre____b;
                $userid  = $key->USUARI_ConsInte__b;
            }

            $jwt = JWT::encode($datosSesion, '');

            $InsertSQl = "INSERT INTO ".$BaseDatos_systema.".SESSIONS( SESSIONS__USUARI_ConsInte__b, SESSIONS__Token, SESSIONS__Estado__b) VALUES (".$userid." , '" . $jwt . "' , 1)";
            $query = $mysqli->query($InsertSQl) or trigger_error($mysqli->error." [$InsertSQl]");

            echo json_encode(array( 'code' => 1,
                                    'nombres' => $nombres,
                                    'token' => $jwt
                            ));

        }else{
            echo json_encode(array('code' => 0,
                                    'response' => 'No es un id valido!'
                            ));
        }
    }

    if(isset($_GET['close_token'])){
        $id_cbx  = $_POST['id_cbx'];
        $Lsql = "select USUARI_ConsInte__b, USUARI_Codigo____b, USUARI_Nombre____b , USUARI_ConsInte__b, USUARI_FechCrea__b , USUARI_InPeToGu__b, USUARI_ConsInte__PROYEC_b FROM ".$BaseDatos_systema.".USUARI WHERE USUARI_UsuaCBX___b = '".$id_cbx."' ";
        $query = $mysqli->query($Lsql) or trigger_error($mysqli->error." [$Lsql]");
        $usuario = 0;
        if($query->num_rows > 0) {
            while($key = $query->fetch_object()){
                $usuario = $key->USUARI_ConsInte__b;
            }
        }

        if($usuario != 0){
            $updateSql = "UPDATE ".$BaseDatos_systema.".SESSIONS SET  SESSIONS__Estado__b = 2, SESSIONS__Fecha_final = '".date('Y-m-d H:i:s')."' WHERE SESSIONS__USUARI_ConsInte__b = ". $usuario ." AND SESSIONS__Estado__b = 1 ;";
           //jgir echo $updateSql;
            $query = $mysqli->query($updateSql) or trigger_error($mysqli->error." [$updateSql]");
            echo json_encode(array('code' => 1,
                                    'response' => 'Se ha cerrado la session!'
                            ));
        }else{
            echo json_encode(array('code' => 0,
                                    'response' => 'No es un Id valido!'
                            ));
        }



    }

    if (isset($_POST['usuario'])){

        $contrasena = encriptaPassword($_POST['password']); //md5($_POST['password']);
        // echo $contrasena;
        if (isset($_POST['quality'])) {
            $QUALITY = $_POST['quality'];
        }else{
            $QUALITY = 0;
        }
        $stmtLsql = "SELECT USUARI_ConsInte__b, USUARI_Codigo____b, USUARI_Cargo_____b, USUARI_Nombre____b , USUARI_ConsInte__b, USUARI_FechCrea__b , USUARI_InPeToGu__b, USUARI_ConsInte__PROYEC_b ,USUARI_Clave_____b, USUARI_Correo___b , USUARI_UsuaCBX___b FROM ".$BaseDatos_systema.".USUARI WHERE USUARI_Correo___b = '".$_POST['usuario']."' AND USUARI_IndiActi__b = '-1' ";

        //$query = $mysqli->query($Lsql) or trigger_error($mysqli->error." [$Lsql]");

        $stmt = $mysqli->query($stmtLsql);


        if($stmt->num_rows > 0) {
            $datos = $stmt->fetch_array();
            if($datos['USUARI_Clave_____b'] == $contrasena ){
                //echo "paso por aqui ";
                $datosSesion = array();
               // while($key = $stmt->fetch_object()){
                    $_SESSION['CODIGO'] = $user;
                    $_SESSION['NOMBRES']   = $datos['USUARI_Nombre____b'];
                    $_SESSION['IDENTIFICACION'] = $datos['USUARI_ConsInte__b'];
                    $_SESSION['FECHA']  = $datos['USUARI_FechCrea__b'];
                    $_SESSION['CORREO']         = $datos['USUARI_Correo___b'];
                    $_SESSION['ACCESO']  = $datos['USUARI_InPeToGu__b'];
                    $_SESSION['USUARICBX']      = $datos['USUARI_UsuaCBX___b'];
                    $_SESSION['PROYECTO_CRM']  = $datos['USUARI_ConsInte__PROYEC_b'];
                    $_SESSION['HUESPED_CRM']  = $datos['USUARI_ConsInte__PROYEC_b'];
                    $_SESSION['LOGIN_OK']  = true;
                    $_SESSION['QUALITY'] = $QUALITY;
                    $_SESSION['CARGO']  = $datos['USUARI_Cargo_____b'];


                    $datosSesion[0]['CODIGO'] = $user;
                    $datosSesion[0]['NOMBRES']   = $datos['USUARI_Nombre____b'];
                    $datosSesion[0]['IDENTIFICACION'] = $datos['USUARI_ConsInte__b'];
                    $datosSesion[0]['FECHA']  = $datos['USUARI_FechCrea__b'];
                    $datosSesion[0]['ACCESO']  = $datos['USUARI_InPeToGu__b'];
                    $datosSesion[0]['PROYECTO_CRM']  = $datos['USUARI_ConsInte__PROYEC_b'];
                    $datosSesion[0]['HUESPED_CRM'] = $datos['USUARI_ConsInte__PROYEC_b'];
                    $datosSesion[0]['LOGIN_OK']  = true;
                    $datosSesion[0]['iat'] = time();
                    $datosSesion[0]['exp'] = time() + 60;
                   /*   $p = new OAuthProvider();
                    $_SESSION['TOKEN']  = $p->generateToken(8);  */

                //}

                $jwt = JWT::encode($datosSesion, '');
                $_SESSION['TOKEN']  = $jwt;
                if($QUALITY == '1'){
                    if($datos['USUARI_Cargo_____b'] == 'calidad' || $datos['USUARI_Cargo_____b'] == 'super-administrador' || $datos['USUARI_Cargo_____b'] == 'administrador'){
                        header('Location: index.php?quality='.$QUALITY);
                    }else{
                       $_SESSION['register']='denegado';
                       header('Location: login.php?quality='.$QUALITY); 
                    }
                }elseif($datos['USUARI_Cargo_____b'] == 'backoffice' || $datos['USUARI_Cargo_____b'] == 'super-administrador' || $datos['USUARI_Cargo_____b'] == 'administrador' || $datos['USUARI_Cargo_____b'] == 'agente'){
                    header('Location: index.php?quality='.$QUALITY);
                }else{
                    $_SESSION['register']='denegado';
                    header('Location: login.php?quality='.$QUALITY); 
                }
            }else{
                $_SESSION['register']='fail';
                header('Location: login.php?quality='.$QUALITY);
            }
        } else {
             // si el resultado de la query no es positivo
             // devolvemos que el usuario o contraseña son erroneos
            $_SESSION['register']='noexiste';
            header('Location: login.php?quality='.$QUALITY);
        }


    }

    if(isset($_POST['operacion'])){
        if($_POST['operacion'] == "ADD"){
            $Lsql = "INSERT INTO ".$BaseDatos_systema.".USUARI (USUARI_Codigo____b, USUARI_Nombre____b,  USUARI_Identific_b, USUARI_Cargo_____b, USUARI_Clave_____b) VALUES ('".$_POST['CodigoUsuario']."' , '".$_POST['NombreUsuario']."' , '".$_POST['IdentificacionUsuario']."' , '".$_POST['RolesUsuario']."' , '".encriptaPassword($_POST['PasswordUsuario'])."' )";

            if ($mysqli->query($Lsql) === TRUE) {
                header('Location: index.php?pagina=usuarios');
            } else {
                echo "Error Hacieno el proceso los registros : " . $mysqli->error;
            }
        }
    }

    function right($str, $length) {

         return substr($str, -$length);
    }

    function encriptaPassword($pass){

        $strCad =   "".chr(33).chr(41).chr(90).chr(94).chr(77).chr(33).chr(65).chr(183)."'".chr(83).chr(68).chr(33).chr(64).chr(41).chr(35).chr(40).chr(36).chr(36).chr(35).chr(64).chr(35).chr(41).chr(95).chr(33).chr(64).chr(68).chr(70).chr(65).chr(36)."CZ".chr(35).chr(60)."AJDA".chr(62).chr(60)."ASD".chr(33).chr(64).chr(35)."M".chr(35)."N".chr(36).chr(37)."N".chr(94)."M".chr(38)."N".chr(42)."K".chr(40)."s".chr(91) .chr(92).chr(124).chr(93).chr(91).chr(47).chr(46).chr(96).chr(45).chr(43).chr(61).chr(33)."2M2xz".chr(94)."a12_%#@&\|\/".chr(46)."`'[]=-{}-1#".chr(36)."f%A%__#A!_CA?()+_!@#".chr(36)."";

        $clave = "";
        $pass2 = "";
        $CAR = "";
        $Codigo = "";
        $strLen = "";


       // echo $strCad;

        if (is_null($pass) || $pass == ""){
            $pass = $strCad;

        }else{
            if(strlen($pass) > 126){
                echo "La palabra es muy larga para encriptar";

            }

            if(strlen($pass) < 10){
                $strLen = "00".strlen($pass);

            }

            if(strlen($pass) > 10 && strlen($pass) < 100){
                $strLen = "0".strlen($pass);
            }

            if(strlen($pass) >= 100){
                $strLen = strlen($pass);
            }


            $pass = $pass . substr($strCad, strlen($pass), strlen($strCad)) . $strLen;

        }


        $clave = chr(33).chr(123).chr(37).chr(125).chr(252).chr(40).chr(38).chr(41).chr(64).chr(47).chr(42).chr(64).chr(96).chr(94).chr(64).chr(35).chr(36).chr(33).chr(95).chr(91).chr(93);
        $pass2 = "";


        $jose = '';

        for($i = 0; $i < strlen($pass); $i++){

            $CAR =  substr($pass, $i, 1);

            $Codigo = substr($clave, (($i - 1) % strLen($clave)) + 1, 1);

            if($Codigo == ''){
                //echo "<br> ".'aja';
                $Codigo = substr($clave, 0, 1);
            }
            //echo "<br> ITERACCION ".$i." CAR => ".$CAR." , Codigo => ".$this->vaores($Codigo);
            //echo "<br> Este es el que va => ".  $this->right("0". dechex(ord($Codigo) ^ ord($CAR)), 2);
            $pass2 = $pass2 . right("0". dechex(ord($Codigo) ^ ord($CAR)), 2);

        }
        return strtoupper($pass2);

    }
