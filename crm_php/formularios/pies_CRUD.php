<?php

ini_set('display_errors', 'On');
ini_set('display_errors', 1);
include("../conexion.php");
date_default_timezone_set('America/Bogota');

function limpiarNombre($strNombre_p){

    $strPatron_t = "/[\<\>\&\'\"]/";

    return preg_replace($strPatron_t, "", $strNombre_p);

}

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    if (isset($_GET["traerOpcionesLista"])) {

        $intIdCampo_t = $_POST["intIdCampo_t"];

        //JDBD-2020-05-03 : Traemos las opciones de la lista.
        $strSQLOpcionesLista_t = "SELECT C.LISOPC_ConsInte__b AS id, C.LISOPC_Nombre____b AS opcion FROM ".$BaseDatos_systema.".PREGUN A JOIN ".$BaseDatos_systema.".OPCION B ON A.PREGUN_ConsInte__OPCION_B = B.OPCION_ConsInte__b JOIN ".$BaseDatos_systema.".LISOPC C ON B.OPCION_ConsInte__b = C.LISOPC_ConsInte__OPCION_b WHERE A.PREGUN_ConsInte__b = ".$intIdCampo_t;

        $resSQLOpcionesLista_t = $mysqli->query($strSQLOpcionesLista_t);

        echo '<option value="0">Seleccione</option>';
        while ($row = $resSQLOpcionesLista_t->fetch_object()) {
            echo '<option value="'.$row->id.'">'.$row->opcion.'</option>';
        }

    }

    if (isset($_GET["traerCamposDelGuion"])) {

        $intIdGuion_t = $_POST["intIdGuion_t"];

        //JDBD-2020-05-03 : Verificamos que el cargo que tiene el usuario en sesion tenga permisos de calidad para traer los campos de dicho seccion.
        if ($_POST["strSessionCargo_t"] == "calidad" || $_POST["strSessionCargo_t"] == "administrador" || $_POST["strSessionCargo_t"] == "super-administrador") {
            $strSQLCampos_t = "SELECT PREGUN_ConsInte__b AS id, PREGUN_Texto_____b AS nombre, PREGUN_Tipo______b AS tipo FROM ".$BaseDatos_systema.".PREGUN JOIN DYALOGOCRM_SISTEMA.SECCIO ON PREGUN_ConsInte__SECCIO_b = SECCIO_ConsInte__b WHERE SECCIO_TipoSecc__b != 4 AND PREGUN_ConsInte__GUION__b = ".$intIdGuion_t." AND PREGUN_Tipo______b NOT IN (12,9,8,13,11) AND PREGUN_Texto_____b NOT IN ('ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY') ORDER BY PREGUN_ConsInte__b ASC";
        }else{
            $strSQLCampos_t = "SELECT PREGUN_ConsInte__b AS id, PREGUN_Texto_____b AS nombre, PREGUN_Tipo______b AS tipo FROM ".$BaseDatos_systema.".PREGUN JOIN DYALOGOCRM_SISTEMA.SECCIO ON PREGUN_ConsInte__SECCIO_b = SECCIO_ConsInte__b WHERE SECCIO_TipoSecc__b NOT IN (2,4) AND PREGUN_ConsInte__GUION__b = ".$intIdGuion_t." AND PREGUN_Tipo______b NOT IN (12,9,8,13,11) AND PREGUN_Texto_____b NOT IN ('ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY') ORDER BY PREGUN_ConsInte__b ASC";
        }

            

        $resSQLCampos_t = $mysqli->query($strSQLCampos_t);

        $arrDatosCampos_t = [];

        echo '<option value="0" tipo="3">Seleccione</option>';
        echo '<option value="G'.$intIdGuion_t.'_FechaInsercion" tipo="5">Fecha creacion</option>';
        while ($row = $resSQLCampos_t->fetch_object()) {
            echo '<option value="'.$row->id.'" tipo="'.$row->tipo.'">'.limpiarNombre($row->nombre).'</option>';
        }

    }
}

?>