<?php
    session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include("conexion.php");
    require_once('../helpers/parameters.php');
    if (isset($_SESSION['QUALITY'])) {
        $quality=$_SESSION['QUALITY'];
    }else{
        if (isset($_GET["quality"])){
            $quality=$_GET["quality"];
        }else{
            $quality=0;
        }
    }
    if(!isset($_SESSION['LOGIN_OK']) && !isset($_GET['token']) && !isset($_SESSION['LOGIN_OK_MANAGER'])){
        header('Location: login.php?quality='.$quality);
    }

    $token = null;
	$GION_TITLE = "";
    $str_busqueda = "Buscar";

	include('funciones.php');

    if(isset($_GET['token'])){
        $token  = $_GET['token'];
        $userid = getIdToken($token);
        //echo "Este es el ID > ".$userid;
        
        //echo $token;
        $tokenSql = "SELECT * FROM ".$BaseDatos_systema.".SESSIONS WHERE SESSIONS__USUARI_ConsInte__b = ". $userid ." AND SESSIONS__Token = '".$token."' AND SESSIONS__Estado__b = 1 ";
        $query = $mysqli->query($tokenSql) or trigger_error($mysqli->error." [$tokenSql]"); ;
        // si el resultado de la query es positivo
        if($query->num_rows > 0) {

        }else{
            header('Location: message.php');
        }
    }
    
    if(isset($_GET['formulario'])){
        $sqlIdGuion=$mysqli->query("select GUION__ConsInte__b as id from {$BaseDatos_systema}.GUION_ where md5(concat('".clave_get."', GUION__ConsInte__b)) = '".$_GET['formulario']."'");
        if($sqlIdGuion && $sqlIdGuion->num_rows==1){
            $sqlIdGuion=$sqlIdGuion->fetch_object();
            $_GET['formulario']=$sqlIdGuion->id;
        }
		$LsqlGUION = "SELECT GUION__ConsInte__b, GUION__Nombre____b FROM ".$BaseDatos_systema.".GUION_ WHERE md5(concat('".clave_get."', GUION__ConsInte__b)) = '".$_GET['formulario']."'";
		
		$results = $mysqli->query($LsqlGUION);

		while($key = $results->fetch_object()){
			$GION_TITLE = ($key->GUION__Nombre____b);
        } 
        
        if(isset($_GET['tareabackoffice'])){
            $LsqlTarea = "SELECT TAREAS_BACKOFFICE_ConsInte__b, TAREAS_BACKOFFICE_Nombre_b FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b =".$_GET['tareabackoffice'];
            
            $results = $mysqli->query($LsqlTarea);
    
            while($key = $results->fetch_object()){
                $GION_TITLE = ($key->TAREAS_BACKOFFICE_Nombre_b);
            } 
        }
		
		include('head.php');
        if(isset($_GET['busqueda'])){
            include ('formularios/G'.$_GET['formulario'].'/G'.$_GET['formulario'].'_Busqueda.php');
        }else{
            include ('formularios/G'.$_GET['formulario'].'/G'.$_GET['formulario'].'.php');
        }
        
    }else{
        if(isset($_GET['campan'])){
            $Guion = 0;//id de la campaña
            $tabla = 0;// $_GET['user'];//ide del usuario

            $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET['campan'];

            $result = $mysqli->query($Lsql);
            while($obj = $result->fetch_object()){
                $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
                $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
            } 

            
            //SELECT de la camic
            $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_GET['campan'];
            
			$LsqlGUION = "SELECT GUION__ConsInte__b, GUION__Nombre____b FROM ".$BaseDatos_systema.".GUION_ WHERE GUION__ConsInte__b =".$Guion;
		
			$results = $mysqli->query($LsqlGUION);

			while($key = $results->fetch_object()){
				$GION_TITLE = ($key->GUION__Nombre____b);
			} 
			
			include('head.php');
		
            
            if(isset($_GET['busqueda'])){
                include ('formularios/G'.$Guion.'/G'.$Guion.'_Busqueda.php');
            }else{
                include ('formularios/G'.$Guion.'/G'.$Guion.'.php');
            }
        }

        if(isset($_GET['id_campana_cbx'])){
            $Lsql = "SELECT CAMPAN_TipoCamp__b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_IdCamCbx__b =".$_GET['id_campana_cbx'];
            $resultado = $mysqli->query($Lsql);
            $CAMPAN_TipoCamp__b = 1;
            $CAMPAN_ConsInte__GUION__Gui_b = null;
            $CAMPAN_ConsInte__GUION__Pob_b = null;
            while ($key = $resultado->fetch_object()) {
                $CAMPAN_TipoCamp__b = $key->CAMPAN_TipoCamp__b;
                $CAMPAN_ConsInte__GUION__Gui_b = $key->CAMPAN_ConsInte__GUION__Gui_b;
                $CAMPAN_ConsInte__GUION__Pob_b = $key->CAMPAN_ConsInte__GUION__Pob_b;
            }

            include('head.php');
            
            switch ($CAMPAN_TipoCamp__b) {
                case 1:
                    include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_Manual.php');
                    break;

                case 5:
                    include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_Telefono.php');
                    break;

                case 6:
                    include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_Telefono.php');
                    break;

                case 3:
                   
                    include ('formularios/G'.$CAMPAN_ConsInte__GUION__Pob_b.'/G'.$CAMPAN_ConsInte__GUION__Pob_b.'_Busqueda_ani.php');
                    break;
            }
            
        }

        if(!isset($_GET['id_campana_cbx']) && !isset($_GET['campan']) && !isset($_GET['pagina'])){
            include('head.php');
        }
    }

    if(isset($_GET['pagina'])){
        include('head.php');
        include($_GET['pagina']."/".$_GET['pagina'].".php");
    }
    include('footer.php'); 
?>
