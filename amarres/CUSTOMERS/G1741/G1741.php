
<?php date_default_timezone_set('America/Bogota'); ?>
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1741/G1741_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

	$PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;
	if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1741_ConsInte__b as id, G1741_C31240 as camp1 , G1741_C31238 as camp2 FROM ".$BaseDatos.".G1741  WHERE G1741_Usuario = ".$idUsuario." ORDER BY G1741_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1741_ConsInte__b as id, G1741_C31240 as camp1 , G1741_C31238 as camp2 FROM ".$BaseDatos.".G1741  ORDER BY G1741_ConsInte__b DESC LIMIT 0, 50";
        }
    }else{
        $Zsql = "SELECT G1741_ConsInte__b as id, G1741_C31240 as camp1 , G1741_C31238 as camp2 FROM ".$BaseDatos.".G1741  ORDER BY G1741_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4327">
                DATOS BASICOS DEL CASO
            </a>
        </h4>
    </div>
    <div id="s_4327" class="panel-collapse collapse in">
        <div class="box-body">

	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">

	 
				        <!-- CAMPO TIPO ENTERO -->
				        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
				        <div class="form-group">
				            <label for="G1741_C31240" id="LblG1741_C31240">ID CASO</label>
				            <input type="text" class="form-control input-sm Numerico" value="<?php if(!isset($_GET["registroId"]) && isset($_GET["view"])){ $Lsql = "SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 31240"; $res = $mysqli->query($Lsql); $dato = $res->fetch_array(); echo ($dato["CONTADORES_Valor_b"] + 1); $XLsql = "UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = CONTADORES_Valor_b + 1 WHERE CONTADORES_ConsInte__PREGUN_b = 31240"; $mysqli->query($XLsql);}?>" readonly name="G1741_C31240" id="G1741_C31240" placeholder="ID CASO">
				        </div>
				        <!-- FIN DEL CAMPO TIPO ENTERO -->
	  
	            </div>


	            <div class="col-md-6 col-xs-6">

	  
				        <!-- CAMPO TIPO MEMO -->
				        <div class="form-group">
				            <label for="G1741_C31244" id="LblG1741_C31244">Descripción</label>
				            <textarea class="form-control input-sm" name="G1741_C31244" id="G1741_C31244"  value="" placeholder="Descripción"></textarea>
				        </div>
				        <!-- FIN DEL CAMPO TIPO MEMO -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">


				        <!-- CAMPO DE TIPO LISTA -->
				        <div class="form-group">
				            <label for="G1741_C31241" id="LblG1741_C31241">Canal</label>
				            <select class="form-control input-sm select2"  style="width: 100%;" name="G1741_C31241" id="G1741_C31241">
				                <option value="0">Seleccione</option>
				                <?php
				                    /*
				                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
				                    */
				                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2914 ORDER BY LISOPC_Nombre____b ASC";

				                    $obj = $mysqli->query($Lsql);
				                    while($obje = $obj->fetch_object()){
				                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

				                    }    
				                    
				                ?>
				            </select>
				        </div>
				        <!-- FIN DEL CAMPO TIPO LISTA -->
	  
	            </div>


	            <div class="col-md-6 col-xs-6">


				        <!-- CAMPO DE TIPO LISTA -->
				        <div class="form-group">
				            <label for="G1741_C31242" id="LblG1741_C31242">Tipificacion</label>
				            <select class="form-control input-sm select2"  style="width: 100%;" name="G1741_C31242" id="G1741_C31242">
				                <option value="0">Seleccione</option>
				                <?php
				                    /*
				                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
				                    */
				                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2912 ORDER BY LISOPC_Nombre____b ASC";

				                    $obj = $mysqli->query($Lsql);
				                    while($obje = $obj->fetch_object()){
				                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

				                    }    
				                    
				                ?>
				            </select>
				        </div>
				        <!-- FIN DEL CAMPO TIPO LISTA -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">


				        <!-- CAMPO DE TIPO LISTA -->
				        <div class="form-group">
				            <label for="G1741_C31243" id="LblG1741_C31243">Subtipificacion</label>
				            <select class="form-control input-sm select2"  style="width: 100%;" name="G1741_C31243" id="G1741_C31243">
				                <option value="0">Seleccione</option>
				                <?php
				                    /*
				                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
				                    */
				                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2913 ORDER BY LISOPC_Nombre____b ASC";

				                    $obj = $mysqli->query($Lsql);
				                    while($obje = $obj->fetch_object()){
				                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

				                    }    
				                    
				                ?>
				            </select>
				        </div>
				        <!-- FIN DEL CAMPO TIPO LISTA -->
	  
	            </div>


	            <div class="col-md-6 col-xs-6">


				        <!-- CAMPO DE TIPO LISTA -->
				        <div class="form-group">
				            <label for="G1741_C31363" id="LblG1741_C31363">Gestion</label>
				            <select class="form-control input-sm select2"  style="width: 100%;" name="G1741_C31363" id="G1741_C31363">
				                <option value="0">Seleccione</option>
				                <?php
				                    /*
				                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
				                    */
				                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2915 ORDER BY LISOPC_Nombre____b ASC";

				                    $obj = $mysqli->query($Lsql);
				                    while($obje = $obj->fetch_object()){
				                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

				                    }    
				                    
				                ?>
				            </select>
				        </div>
				        <!-- FIN DEL CAMPO TIPO LISTA -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">


				        <!-- CAMPO DE TIPO LISTA -->
				        <div class="form-group">
				            <label for="G1741_C31388" id="LblG1741_C31388">Asignado a </label>
				            <select class="form-control input-sm select2"  style="width: 100%;" name="G1741_C31388" id="G1741_C31388">
				                <option value="0">Seleccione</option>
				                <?php
				                    /*
				                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
				                    */
				                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1683 ORDER BY LISOPC_Nombre____b ASC";

				                    $obj = $mysqli->query($Lsql);
				                    while($obje = $obj->fetch_object()){
				                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

				                    }    
				                    
				                ?>
				            </select>
				        </div>
				        <!-- FIN DEL CAMPO TIPO LISTA -->
	  
	            </div>


	            <div class="col-md-6 col-xs-6">

				        <div class="form-group">
				            <label for="G1741_C31391" id="LblG1741_C31391">Estado</label>
				            <select class="form-control input-sm select2"  style="width: 100%;" name="G1741_C31391" id="G1741_C31391">
				                <option value="22988">Abierto</option>
				                <?php
				                    /*
				                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
				                    */
				                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1684 AND LISOPC_ConsInte__b != 22988 ORDER BY LISOPC_Nombre____b ASC";

				                    $obj = $mysqli->query($Lsql);
				                    while($obje = $obj->fetch_object()){
				                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

				                    }    
				                    
				                ?>
				            </select>
				        </div>
	  
	            </div>

	  
	        </div>
	        <div class="row">


	            <div class="col-md-6 col-xs-6">

	  
				        <!-- CAMPO TIPO FECHA -->
				        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
				        <div class="form-group">
				            <label for="G1741_C31389" id="LblG1741_C31389">Fecha creación</label>
				            <input readonly type="text" class="form-control input-sm Fecha" value="<?php echo date("Y-m-d"); ?>"  name="G1741_C31389" id="G1741_C31389" placeholder="YYYY-MM-DD">
				        </div>
				        <!-- FIN DEL CAMPO TIPO FECHA-->
	  
	            </div>

	            <div class="col-md-6 col-xs-6">

	  
				        <!-- CAMPO TIPO FECHA -->
				        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
				        <div class="form-group">
				            <label for="G1741_C31390" id="LblG1741_C31390">Fecha cierre</label>
				            <input readonly type="text" class="form-control input-sm Fecha" value=""  name="G1741_C31390" id="G1741_C31390" placeholder="YYYY-MM-DD">
				        </div>
				        <!-- FIN DEL CAMPO TIPO FECHA-->
	  
	            </div>
	  
	        </div>


        </div>
    </div>
</div>

<div id="4328" style='display:none;'>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1741_C31235" id="LblG1741_C31235">ORIGEN_DY_WF</label>
			            <input type="text" class="form-control input-sm" id="G1741_C31235" value="" readonly name="G1741_C31235"  placeholder="ORIGEN_DY_WF">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1741_C31236" id="LblG1741_C31236">OPTIN_DY_WF</label>
			            <input type="text" class="form-control input-sm" id="G1741_C31236" value="" readonly name="G1741_C31236"  placeholder="OPTIN_DY_WF">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1741_C31237" id="LblG1741_C31237">ESTADO_DY</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1741_C31237" id="G1741_C31237">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1675 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4412">
                DATOS DEL CLIENTE
            </a>
        </h4>
    </div>
    <div id="s_4412" class="panel-collapse">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1741_C31238" id="LblG1741_C31238">NIT EMPRESA</label>
			            <input type="text" class="form-control input-sm" id="G1741_C31238" value=""  name="G1741_C31238"  placeholder="NIT EMPRESA">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1741_C31239" id="LblG1741_C31239">ID PERSONA</label>
			            <input readonly type="text" class="form-control input-sm" id="G1741_C31239" value=""  name="G1741_C31239"  placeholder="ID PERSONA">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			       
			        <!-- CAMPO DE TIPO GUION -->
			        <div class="form-group">
			            <label for="G1741_C31245" id="LblG1741_C31245">Solicitante</label>
			            <select class="form-control input-sm select2" style="width: 100%;"  name="G1741_C31245" id="G1741_C31245">
			                <option value="0">Seleccione</option>

			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

            <div class="col-md-6 col-xs-6">

			        <div class="form-group">
			            <label for="G1741_C61247" id="LblG1741_C61247">Correo</label>
			            <input type="text" class="form-control input-sm" value="<?php if(isset($_GET["G1741_C61247"])) { echo $_GET["G1741_C61247"];} ?>"  name="G1741_C61247" id="G1741_C61247" placeholder="Correo">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


        </div>

        <div class="row">

            <div class="col-md-6 col-xs-6">

			        <div class="form-group">
			            <label for="G1741_C64296" id="LblG1741_C64296">Celular</label>
			            <input type="text" class="form-control input-sm" value="<?php if(isset($_GET["G1741_C64296"])) { echo $_GET["G1741_C64296"];} ?>"  name="G1741_C64296" id="G1741_C64296" placeholder="Celular">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


        </div>


        </div>
    </div>
</div>

<div class="panel box box-primary" style="display: none">
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4413">
                OTROS
            </a>
        </h4>
    </div>
    <div id="s_4413" class="panel-collapse collapse in">
        <div class="box-body">



        <div class="row" style="display: none">

            <div class="col-md-6 col-xs-6">

			        <div class="form-group">
			            <label for="G1741_C63630" id="LblG1741_C63630">Campaña</label>
			            <input readonly type="number" class="form-control input-sm" value="<?php if(isset($_GET["G1741_C63630"])) { echo $_GET["G1741_C63630"];} ?>"  name="G1741_C63630" id="G1741_C63630" placeholder="Campaña">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


        </div>


        </div>
    </div>
</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4437">
                ARCHIVOS ADJUNTOS
            </a>
        </h4>
    </div>
    <div id="s_4437" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">
 					<div class="panel">
 						<label for="FG1741_C31958" id="LblFG1741_C31958">ADJUNTO 1</label>	
				        <input type="file" class="adjuntos" id="FG1741_C31958" name="FG1741_C31958">
				        <p class="help-block">
				            Peso maximo del archivo 9 MB
				        </p>
				        <input name="G1741_C31958" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G1741_C31958" name="G1741_C31958" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
				    </div> 
            </div>


            <div class="col-md-6 col-xs-6">

 					<div class="panel">
 						<label for="FG1741_C31959" id="LblFG1741_C31959">ADJUNTO 2</label>	
				        <input type="file" class="adjuntos" id="FG1741_C31959" name="FG1741_C31959">
				        <p class="help-block">
				            Peso maximo del archivo 9 MB
				        </p>
				        <input name="G1741_C31959" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G1741_C31959" name="G1741_C31959" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
				    </div>
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 					<div class="panel">
 						<label for="FG1741_C31960" id="LblFG1741_C31960">ADJUNTO 3</label>	
				        <input type="file" class="adjuntos" id="FG1741_C31960" name="FG1741_C31960">
				        <p class="help-block">
				            Peso maximo del archivo 9 MB
				        </p>
				        <input name="G1741_C31960" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G1741_C31960" name="G1741_C31960" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
				    </div>
  
            </div>


            <div class="col-md-6 col-xs-6">

 					<div class="panel">
 						<label for="FG1741_C31961" id="LblFG1741_C31961">ADJUNTO 4</label>	
				        <input type="file" class="adjuntos" id="FG1741_C31961" name="FG1741_C31961">
				        <p class="help-block">
				            Peso maximo del archivo 9 MB
				        </p>
				        <input name="G1741_C31961" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G1741_C31961" name="G1741_C31961" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
				    </div>
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 					<div class="panel">
 						<label for="FG1741_C31962" id="LblFG1741_C31962">ADJUNTO 5</label>	
				        <input type="file" class="adjuntos" id="FG1741_C31962" name="FG1741_C31962">
				        <p class="help-block">
				            Peso maximo del archivo 9 MB
				        </p>
				        <input name="G1741_C31962" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G1741_C31962" name="G1741_C31962" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
				    </div>
  
            </div>


            <div class="col-md-6 col-xs-6">

 					<div class="panel">
 						<label for="FG1741_C31963" id="LblFG1741_C31963">ADJUNTO 6</label>	
				        <input type="file" class="adjuntos" id="FG1741_C31963" name="FG1741_C31963">
				        <p class="help-block">
				            Peso maximo del archivo 9 MB
				        </p>
				        <input name="G1741_C31963" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G1741_C31963" name="G1741_C31963" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
				    </div>
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 					<div class="panel">
 						<label for="FG1741_C31964" id="LblFG1741_C31964">ADJUNTO 7</label>	
				        <input type="file" class="adjuntos" id="FG1741_C31964" name="FG1741_C31964">
				        <p class="help-block">
				            Peso maximo del archivo 9 MB
				        </p>
				        <input name="G1741_C31964" value="Sin Adjuntos" disabled readonly class="btn btn-primary btn-sm" id ="G1741_C31964" name="G1741_C31964" onClick="location.href='<?=$url_crud;?>?adjunto='+this.value" >
				    </div>
  
            </div>


        </div>


        </div>
    </div>
</div>

<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<hr/>
<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">

        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">Gestiones</a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
            <button title="Crear Gestiones" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_0"><i class="fa fa-plus"></i></button>
        </div>

    </div>

</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G1741/G1741_eventos.js"></script> 
<script type="text/javascript">
    $(function(){
        <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        	<?php if($_GET["yourfather"] != "NULL"){ ?>
        		$("#G1741_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        	<?php }else{ ?>
        		if(document.getElementById("G1741_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
        				$.ajax({
	                        url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
	                        type     : 'POST',
	                        data     : { q : <?php echo $_GET["idFather"]; ?> },
	                        type : 'post',
	                        success  : function(data){
	                            $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
	                    }});
        			}else{
        				$("#G1741_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
        			}
			<?php } ?>
        <?php } ?>
        
                        		<?php if (!isset($_GET["idFather"])) { ?>
		                        	 $("#add").click(function(){
							                $("#G1741_C31240").val("<?php if(!isset($_GET["registroId"])){ $Lsql = "SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 31240"; $res = $mysqli->query($Lsql); $dato = $res->fetch_array(); echo ($dato["CONTADORES_Valor_b"] + 1); $XLsql = "UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = CONTADORES_Valor_b + 1 WHERE CONTADORES_ConsInte__PREGUN_b = 31240"; $mysqli->query($XLsql);}?>");
							         });
					            <?php } ?>	

        $(".adjuntos").change(function(){
            var imax = $(this).attr('valor');
            var imagen = this.files[0];

            // if(imagen['type'] != 'image/jpeg' && imagen['type'] != 'image/png' && imagen['type'] != "application/pdf"){
            //     $(".NuevaFoto").val('');
            //     swal({
            //         title : "Error al subir el archivo",
            //         text  : "El archivo debe estar en formato PNG , JPG, PDF",
            //         type  : "error",
            //         confirmButtonText : "Cerrar"
            //     });
            // }else 
            if(imagen['size'] > 9000000 ) {
                $(".NuevaFoto").val('');
                swal({
                    title : "Error al subir el archivo",
                    text  : "El archivo no debe pesar mas de 9 MB",
                    type  : "error",
                    confirmButtonText : "Cerrar"
                });
            }
        });
                        	
    	var meses = new Array(12);
    	meses[0] = "01";
    	meses[1] = "02";
    	meses[2] = "03";
    	meses[3] = "04";
    	meses[4] = "05";
    	meses[5] = "06";
    	meses[6] = "07";
    	meses[7] = "08";
    	meses[8] = "09";
    	meses[9] = "10";
    	meses[10] = "11";
    	meses[11] = "12";

    	var d = new Date();
        var h = d.getHours();
        var horas = (h < 10) ? '0' + h : h;
        var dia = d.getDate();
        var dias = (dia < 10) ? '0' + dia : dia;
        var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
        $("#FechaInicio").val(fechaInicial);
            

        //Esta es por si lo llaman en modo formulario de edicion LigthBox
        <?php if(isset($_GET['registroId'])){ ?>
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
            dataType : 'json',
            success  : function(data){
                //recorrer datos y enviarlos al formulario
                $.each(data, function(i, item) {
                    

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            
 
                    $("#G1741_C31240").val(item.G1741_C31240);
 
                    $("#G1741_C31244").val(item.G1741_C31244);
 
                    $("#G1741_C31241").val(item.G1741_C31241).trigger("change"); 
 
                    $("#G1741_C31242").val(item.G1741_C31242).trigger("change"); 
 
                    $("#G1741_C31243").val(item.G1741_C31243).trigger("change"); 
 
                    $("#G1741_C31363").val(item.G1741_C31363).trigger("change"); 
 
                    $("#G1741_C31388").val(item.G1741_C31388).trigger("change"); 
 
                    $("#G1741_C31235").val(item.G1741_C31235);
 
                    $("#G1741_C31236").val(item.G1741_C31236);
 
                    $("#G1741_C31237").val(item.G1741_C31237).trigger("change"); 
 
                    $("#G1741_C31238").val(item.G1741_C31238);
 
                    $("#G1741_C31239").val(item.G1741_C31239);
 
                    $("#G1741_C31245").val(item.G1741_C31245).trigger("change"); 
 
                    $("#G1741_C31391").val(item.G1741_C31391).trigger("change"); 
 
                    $("#G1741_C31389").val(item.G1741_C31389);
 
                    $("#G1741_C31390").val(item.G1741_C31390);

                    $("#G1741_C61247").val(item.G1741_C61247);

                    $("#G1741_C64296").val(item.G1741_C64296);
 		
                    if (item.G1741_C31958) {
                    	$("#G1741_C31958").val(item.G1741_C31958);
                    }else{
                    	$("#G1741_C31958").val("Sin Adjunto");
                    }
 		
                    if (item.G1741_C31959) {
                    	$("#G1741_C31959").val(item.G1741_C31959);
                    }else{
                    	$("#G1741_C31959").val("Sin Adjunto");
                    }
 		
                    if (item.G1741_C31960) {
                    	$("#G1741_C31960").val(item.G1741_C31960);
                    }else{
                    	$("#G1741_C31960").val("Sin Adjunto");
                    }
 		
                    if (item.G1741_C31961) {
                    	$("#G1741_C31961").val(item.G1741_C31961);
                    }else{
                    	$("#G1741_C31961").val("Sin Adjunto");
                    }
 		
                    if (item.G1741_C31962) {
                    	$("#G1741_C31962").val(item.G1741_C31962);
                    }else{
                    	$("#G1741_C31962").val("Sin Adjunto");
                    }
 		
                    if (item.G1741_C31963) {
                    	$("#G1741_C31963").val(item.G1741_C31963);
                    }else{
                    	$("#G1741_C31963").val("Sin Adjunto");
                    }
 		
                    if (item.G1741_C31964) {
                    	$("#G1741_C31964").val(item.G1741_C31964);
                    }else{
                    	$("#G1741_C31964").val("Sin Adjunto");
                    }
			        cargarHijos_0($("#G1741_C31240").val());
					$("#h3mio").html(item.principal);

                });
                //Deshabilitar los campos 3

                //Habilitar todos los campos para edicion
                $('#FormularioDatos :input').each(function(){
                    $(this).attr('disabled', true);
                });

                              

                //Habilidar los botones de operacion, add, editar, eliminar
                $("#add").attr('disabled', false);
                $("#edit").attr('disabled', false);
                $("#delete").attr('disabled', false);

                //Desahabiliatra los botones de salvar y seleccionar_registro
                $("#cancel").attr('disabled', true);
                $("#Save").attr('disabled', true);
            } 
        });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

     	$("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        $("#btnLlamar_0").attr('padre', <?php echo $_GET['registroId'];?>);

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
        	/*$("#btnLlamar_0").attr('padre', <?php echo $_GET['user'];?>);
        	vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
        	idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');
            
    	$("#btnLlamar_0").attr('padre', $("#G1741_C31240").val());
    		var id_0 = $("#G1741_C31240").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
            $("#btnLlamar_0").attr('padre', $("#G1741_C31240").val());
    		var id_0 = $("#G1741_C31240").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
        });

        $("#btnLlamar_0").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G1741_C31240").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1745&view=si&formaDetalle=si&formularioPadre=1741&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=31392<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&G1745_C31394=<?php if(!isset($_GET["token"])){echo $_SESSION["NOMBRES"];}else{echo getNombreUser($_GET["token"]);}?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    
                    if (validado == '0') {
                    	var form = $("#FormularioDatos");
	                    //Se crean un array con los datos a enviar, apartir del formulario 
	                    var formData = new FormData($("#FormularioDatos")[0]);
	                    $.ajax({
	                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
	                        type: 'POST',
	                        data: formData,
	                        cache: false,
	                        contentType: false,
	                        processData: false,
	                        //una vez finalizado correctamente
	                        success: function(data){
	                            if(data){
	                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
	                                if($("#oper").val() == 'add'){
	                                    idTotal = data;
	                                }else{
	                                    idTotal= $("#hidId").val();
	                                }
	                                $("#hidId").val(idTotal);

	                                int_guardo = 1;
	                                $(".llamadores").attr('padre', idTotal);
	                                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1745&view=si&formaDetalle=si&formularioPadre=1741&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=31392&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
	                                $("#editarDatos").modal('show');
	                                $("#oper").val('edit');

	                            }else{
	                                //Algo paso, hay un error
	                                alertify.error('Un error ha ocurrido');
	                            }                
	                        },
	                        //si ha ocurrido un error
	                        error: function(){
	                            after_save_error();
	                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
	                        }
	                    });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1745&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1741&pincheCampo=31392&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>&G1745_C31394=<?php if(!isset($_GET["token"])){echo $_SESSION["NOMBRES"];}else{echo getNombreUser($_GET["token"]);}?>');
                $("#editarDatos").modal('show');
            }
        });
        //Select2 estos son los guiones
        


    $("#G1741_C31241").select2();

    $("#G1741_C31242").select2();

    $("#G1741_C31243").select2();

    $("#G1741_C31363").select2();

    $("#G1741_C31388").select2();

    $("#G1741_C31237").select2();

        $("#G1741_C31245").select2({
        	placeholder: "Buscar",
	        allowClear: false,
	        minimumInputLength: 3,
	        ajax: {
	            url: '<?=$url_crud;?>?CallDatosCombo_Guion_G1741_C31245=si',
	            dataType: 'json',
	            type : 'post',
	            delay: 250,
	            data: function (params) {
	                return {
	                    q: params.term
	                };
	            },
              	processResults: function(data) {
				  	return {
				    	results: $.map(data, function(obj) {
				      		return {
				        		id: obj.id,
				        		text: obj.text
				      		};
				    	})
				  	};
				},
	            cache: true
	        }
    	});            
        
        $("#G1741_C31245").change(function(){
            $.ajax({
        		url   : '<?php echo $url_crud;?>',
        		data  : { dameValoresCamposDinamicos_Guion_G1741_C31245 : $(this).val() },
        		type  : 'post',
        		dataType : 'json',
        		success  : function(data){

        				$("#G1741_C31239").val(data[0].nit);

        		}
        	});
        });
                                      

    $("#G1741_C31391").select2();
        //datepickers
        

        // $("#G1741_C31389").datepicker({
        //     language: "es",
        //     autoclose: true,
        //     todayHighlight: true
        // });

        $("#G1741_C31390").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Validaciones numeros Enteros
        

    	$("#G1741_C31240").numeric();
		        

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para Canal 

    $("#G1741_C31241").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Solicitud 

    $("#G1741_C31242").change(function(){  
    	//Esto es la parte de las listas dependientes    		
		// $.ajax({
		// 	url    : '<?php echo $url_crud; ?>',
		// 	type   : 'post',
		// 	data   : { getListaHija : true , opcionID : '1678' , idPadre : $(this).val() },
		// 	success : function(data){
		// 		setTimeout(function(){
	 //    			var optG1741_C31243 = $("#G1741_C31243").val();
		// 			$("#G1741_C31243").html(data);
		// 			if (optG1741_C31243 != null) {
		// 				$("#G1741_C31243").val(optG1741_C31243).trigger("change");
		// 			}
		// 		},1000);
		// 	}
		// });
        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '2913' , idPadre : $(this).val() },
            success : function(data){
                var optG1741_C31243 = $("#G1741_C31243").attr("opt");
                $("#G1741_C31243").html(data);
                if (optG1741_C31243 != null) {
                    $("#G1741_C31243").val(optG1741_C31243).trigger("change");
                }
            }
        });
		
    });

    //function para Detalle 

  //   $("#G1741_C31243").change(function(){  
  //   	//Esto es la parte de las listas dependientes   	
		// $.ajax({
		// 	url    : '<?php echo $url_crud; ?>',
		// 	type   : 'post',
		// 	data   : { getListaHija : true , opcionID : '1681' , idPadre : $(this).val() },
		// 	success : function(data){
		// 		setTimeout(function(){
		// 			var optG1741_C31363 = $("#G1741_C31363").val();
		// 			$("#G1741_C31363").html(data);
		// 			if (optG1741_C31363 != null) {
		// 				$("#G1741_C31363").val(optG1741_C31363).trigger("change");
		// 			}
		// 		},1000);
		// 	}
		// });
		
  //   });

    //function para Motivo de escalamiento 

  //   $("#G1741_C31363").change(function(){  
  //   	//Esto es la parte de las listas dependientes    	
		// $.ajax({
		// 	url    : '<?php echo $url_crud; ?>',
		// 	type   : 'post',
		// 	data   : { getListaHija : true , opcionID : '1683' , idPadre : $(this).val() },
		// 	success : function(data){
		// 		setTimeout(function(){
		// 			var optG1741_C31388 = $("#G1741_C31388").val();
		// 			$("#G1741_C31388").html(data);
		// 			if (optG1741_C31388 != null) {
		// 				$("#G1741_C31388").val(optG1741_C31388).trigger("change");
		// 			}
		// 		},1000);
		// 	}
		// });
		
  //   });

    //function para Asignado a  

    $("#G1741_C31388").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ESTADO_DY 

    $("#G1741_C31237").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Estado 

    $("#G1741_C31391").change(function(){  

        if ($(this).val() == "22989") {

         	var today = moment().format('YYYY-MM-DD');
            $("#G1741_C31390").val(today);
        	

        }

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
        	
        	var bol_respuesta = before_save();
        	var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
            	alertify.error("Es necesaria la tipificación!");
            	valido = 1;
            }

            if($(".reintento").val() == '2'){
            	if($(".TxtFechaReintento").val().length < 1){
            		alertify.error("Es necesario llenar la fecha de reintento!");
            		$(".TxtFechaReintento").focus();
            		valido = 1;
            	}

            	if($(".TxtHoraReintento").val().length < 1){
            		alertify.error("Es necesario llenar la hora de reintento!");
            		$(".TxtHoraReintento").focus();
            		valido = 1;
            	}
            }

            //JDBD ASIGNACIONES DE CASOS A BACKOFFICE.
            var intSolicitud_t = parseInt($("#G1741_C31242").val()); 
            var intDetalle_t = parseInt($("#G1741_C31243").val());
            var intEscalamiento_t = parseInt($("#G1741_C31363").val());
            var arrBack1_t = [22957,22958,22959,23272,22960];
            var arrBack2_t = [22961,22963,22962,22964,22965,22973,22974,22975,23273,23274];
            var arrBack3_t = [23275,23276,23277,23278,23279,23280,23281];
            var intAsignado_t = null;

            if (intSolicitud_t == 22927) {            	
            	if (arrBack1_t.indexOf(intDetalle_t) >= 0) {
            		intAsignado_t = 1901;
            	}
            }

            if (intDetalle_t == 22953) {
            	if (arrBack2_t.indexOf(intEscalamiento_t) >= 0) {
            		intAsignado_t = 1854;
            	}else if(arrBack3_t.indexOf(intEscalamiento_t) >= 0){
            		intAsignado_t = 1857;
            	}
            }

            if(valido == '0'){
	        	if(bol_respuesta){            
		            var form = $("#FormularioDatos");
		            //Se crean un array con los datos a enviar, apartir del formulario 

		            var formData = new FormData($("#FormularioDatos")[0]);
		            formData.append("backoffice",intAsignado_t);

		            $.ajax({
		               url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
		                type: 'POST',
		                data: formData,
		                cache: false,
		                contentType: false,
		                processData: false,
		                //una vez finalizado correctamente
		                success: function(data){
		                    if(data != '0'){
		                    	<?php if(!isset($_GET['campan'])){ ?>
			                        //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
			                        if($("#oper").val() == 'add'){
			                            idTotal = data;
			                        }else{
			                            idTotal= $("#hidId").val();
			                        }
			                       
			                        //Limpiar formulario
			                        form[0].reset();
			                        after_save();
			                        <?php if(isset($_GET['registroId'])){ ?>
			                        	var ID = <?=$_GET['registroId'];?>
			                        <?php }else{ ?>	
			                        	var ID = data
			                        <?php } ?>	
			                        $.ajax({
			                            url      : '<?=$url_crud;?>',
			                            type     : 'POST',
			                            data     : { CallDatos : 'SI', id : ID },
			                            dataType : 'json',
			                            success  : function(data){
			                                //recorrer datos y enviarlos al formulario
			                                $.each(data, function(i, item) {
		                                    
 
		                                    	$("#G1741_C31240").val(item.G1741_C31240);
 
		                                    	$("#G1741_C31244").val(item.G1741_C31244);

		                                    	$("#G1741_C31241").val(item.G1741_C31241).trigger("change"); 

		                                    	$("#G1741_C31242").val(item.G1741_C31242).trigger("change"); 

		                                    	$("#G1741_C31243").val(item.G1741_C31243).trigger("change"); 

		                                    	$("#G1741_C31363").val(item.G1741_C31363).trigger("change"); 

		                                    	$("#G1741_C31388").val(item.G1741_C31388).trigger("change"); 
 
		                                    	$("#G1741_C31235").val(item.G1741_C31235);
 
		                                    	$("#G1741_C31236").val(item.G1741_C31236);

		                                    	$("#G1741_C31237").val(item.G1741_C31237).trigger("change"); 
 
		                                    	$("#G1741_C31238").val(item.G1741_C31238);
 
		                                    	$("#G1741_C31239").val(item.G1741_C31239);

		                                    	$("#G1741_C31245").val(item.G1741_C31245).trigger("change"); 

		                                    	$("#G1741_C31391").val(item.G1741_C31391).trigger("change"); 
 
		                                    	$("#G1741_C31389").val(item.G1741_C31389);
 
		                                    	$("#G1741_C31390").val(item.G1741_C31390);

		                                    	$("#G1741_C61247").val(item.G1741_C61247);

		                                    	$("#G1741_C64296").val(item.G1741_C64296);
 		
		                                    	if (item.G1741_C31958) {
		                                    		$("#G1741_C31958").val(item.G1741_C31958);
		                                    	}else{
		                                    		$("#G1741_C31958").val("Sin Adjunto");
		                                    	}
 		
		                                    	if (item.G1741_C31959) {
		                                    		$("#G1741_C31959").val(item.G1741_C31959);
		                                    	}else{
		                                    		$("#G1741_C31959").val("Sin Adjunto");
		                                    	}
 		
		                                    	if (item.G1741_C31960) {
		                                    		$("#G1741_C31960").val(item.G1741_C31960);
		                                    	}else{
		                                    		$("#G1741_C31960").val("Sin Adjunto");
		                                    	}
 		
		                                    	if (item.G1741_C31961) {
		                                    		$("#G1741_C31961").val(item.G1741_C31961);
		                                    	}else{
		                                    		$("#G1741_C31961").val("Sin Adjunto");
		                                    	}
 		
		                                    	if (item.G1741_C31962) {
		                                    		$("#G1741_C31962").val(item.G1741_C31962);
		                                    	}else{
		                                    		$("#G1741_C31962").val("Sin Adjunto");
		                                    	}
 		
		                                    	if (item.G1741_C31963) {
		                                    		$("#G1741_C31963").val(item.G1741_C31963);
		                                    	}else{
		                                    		$("#G1741_C31963").val("Sin Adjunto");
		                                    	}
 		
		                                    	if (item.G1741_C31964) {
		                                    		$("#G1741_C31964").val(item.G1741_C31964);
		                                    	}else{
		                                    		$("#G1741_C31964").val("Sin Adjunto");
		                                    	}
		              							$("#h3mio").html(item.principal);
			                                });

			                                //Deshabilitar los campos 2

			                                //Habilitar todos los campos para edicion
			                                $('#FormularioDatos :input').each(function(){
			                                    $(this).attr('disabled', true);
			                                });

			                                //Habilidar los botones de operacion, add, editar, eliminar
			                                $("#add").attr('disabled', false);
			                                $("#edit").attr('disabled', false);
			                                $("#delete").attr('disabled', false);

			                                //Desahabiliatra los botones de salvar y seleccionar_registro
			                                $("#cancel").attr('disabled', true);
			                                $("#Save").attr('disabled', true);
			                            } 
			                        })
			                        $("#hidId").val(ID);  

		                        <?php }else{ 
		                        	if(!isset($_GET['formulario'])){
		                        ?>

		                        	$.ajax({
		                        		url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
		                        		type  : "post",
		                        		data  : formData,
		                    		 	cache: false,
					                    contentType: false,
					                    processData: false,
		                        		success : function(xt){
		                        			console.log(xt);
		                        			window.location.href = "quitar.php";
		                        		}
		                        	});
		                        	
				
		                        <?php } 
		                        	}
		                        ?>            
		                    }else{
		                        //Algo paso, hay un error
		                        $("#Save").attr('disabled', false);
		                        alertify.error('Un error ha ocurrido y no pudimos guardar la información');
		                    }                
		                },
		                //si ha ocurrido un error
		                error: function(){
		                    after_save_error();
		                    $("#Save").attr('disabled', false);
		                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
		                }
		            });
          		}
          	}
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','ID CASO','Descripción','Canal','Solicitud','Detalle','Motivo de escalamiento','Asignado a ','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','NIT EMPRESA','ID PERSONA','Solicitante','Estado','Fecha creación','Fecha cierre','ADJUNTO 1','ADJUNTO 2','ADJUNTO 3','ADJUNTO 4','ADJUNTO 5','ADJUNTO 6','ADJUNTO 7'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
	                ,
	                {  
	                    name:'G1741_C31240', 
	                    index:'G1741_C31240', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1741_C31244', 
	                    index:'G1741_C31244', 
	                    width:150, 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1741_C31241', 
	                    index:'G1741_C31241', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1676&campo=G1741_C31241'
	                    }
	                }

	                ,
	                { 
	                    name:'G1741_C31242', 
	                    index:'G1741_C31242', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1677&campo=G1741_C31242'
	                    }
	                }

	                ,
	                { 
	                    name:'G1741_C31243', 
	                    index:'G1741_C31243', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1678&campo=G1741_C31243'
	                    }
	                }

	                ,
	                { 
	                    name:'G1741_C31363', 
	                    index:'G1741_C31363', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1681&campo=G1741_C31363'
	                    }
	                }

	                ,
	                { 
	                    name:'G1741_C31388', 
	                    index:'G1741_C31388', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1683&campo=G1741_C31388'
	                    }
	                }

	                ,
	                { 
	                    name:'G1741_C31235', 
	                    index: 'G1741_C31235', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1741_C31236', 
	                    index: 'G1741_C31236', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1741_C31237', 
	                    index:'G1741_C31237', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1675&campo=G1741_C31237'
	                    }
	                }

	                ,
	                { 
	                    name:'G1741_C31238', 
	                    index: 'G1741_C31238', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1741_C31239', 
	                    index: 'G1741_C31239', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1741_C31245', 
	                    index:'G1741_C31245', 
	                    width:300 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1741_C31245=si',
	                        dataInit:function(el){
	                        	$(el).select2();
	                            /*$(el).select2({ 
	                                templateResult: function(data) {
	                                    var r = data.text.split('|');
	                                    var row = '<div class="row">';
	                                    var totalRows = 12 / r.length;
	                                    for(i= 0; i < r.length; i++){
	                                        row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
	                                    }
	                                    row += '</div>';
	                                    var $result = $(row);
	                                    return $result;
	                                },
	                                templateSelection : function(data){
	                                    var r = data.text.split('|');
	                                    return r[0];
	                                }
	                            });*/
	                            $(el).change(function(){
	                                var valores = $(el + " option:selected").attr("llenadores");
	                                var campos =  $(el + " option:selected").attr("dinammicos");
	                                var r = valores.split('|');
	                                if(r.length > 1){

	                                    var c = campos.split('|');
	                                    for(i = 1; i < r.length; i++){
	                                        $("#"+ rowid +"_"+c[i]).val(r[i]);
	                                    }
	                                }
	                            });
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1741_C31391', 
	                    index:'G1741_C31391', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1684&campo=G1741_C31391'
	                    }
	                }

	                ,
	                {  
	                    name:'G1741_C31389', 
	                    index:'G1741_C31389', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1741_C31390', 
	                    index:'G1741_C31390', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1741_C31958', 
	                    index: 'G1741_C31958', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1741_C31959', 
	                    index: 'G1741_C31959', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1741_C31960', 
	                    index: 'G1741_C31960', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1741_C31961', 
	                    index: 'G1741_C31961', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1741_C31962', 
	                    index: 'G1741_C31962', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1741_C31963', 
	                    index: 'G1741_C31963', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1741_C31964', 
	                    index: 'G1741_C31964', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    

                    $("#"+ rowid +"_G1741_C31245").change(function(){
                        var valores = $("#"+ rowid +"_G1741_C31245 option:selected").attr("llenadores");
                        var campos = $("#"+ rowid +"_G1741_C31245 option:selected").attr("dinammicos");
                        var r = valores.split('|');

                        if(r.length > 1){

                            var c = campos.split('|');
                            for(i = 1; i < r.length; i++){
                                if(!$("#"+c[i]).is("select")) {
                                // the input field is not a select
                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                }else{
                                    var change = r[i].replace(' ', ''); 
                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                }
                                
                            }
                        }
                    });
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1741_C31240',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
	            ,subGrid: true,
	            subGridRowExpanded: function(subgrid_id, row_id) { 
	                // we pass two parameters 
	                // subgrid_id is a id of the div tag created whitin a table data 
	                // the id of this elemenet is a combination of the "sg_" + id of the row 
	                // the row_id is the id of the row 
	                // If we wan to pass additinal parameters to the url we can use 
	                // a method getRowData(row_id) - which returns associative array in type name-value 
	                // here we can easy construct the flowing 
	                $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Id Caso','Fecha','Gestor','Gestión', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }
 
                        ,
                        {  
                            name:'G1745_C31392', 
                            index:'G1745_C31392', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1745_C31393', 
                            index: 'G1745_C31393', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1745_C31394', 
                            index: 'G1745_C31394', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1745_C31395', 
                            index:'G1745_C31395', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

            }, 
            subGridRowColapsed: function(subgrid_id, row_id) { 
                // this function is called before removing the data 
                //var subgrid_table_id; 
                //subgrid_table_id = subgrid_id+"_t"; 
                //jQuery("#"+subgrid_table_id).remove(); 
            }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(x){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', Busqueda : x},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');
            $("#btnLlamar_0").attr('padre', id);
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            

                        $("#G1741_C31240").val(item.G1741_C31240);

                        $("#G1741_C31244").val(item.G1741_C31244);
 
                    $("#G1741_C31241").val(item.G1741_C31241).trigger("change"); 
 
                    $("#G1741_C31242").val(item.G1741_C31242).trigger("change"); 
 
                    $("#G1741_C31243").val(item.G1741_C31243).trigger("change"); 
 
                    $("#G1741_C31363").val(item.G1741_C31363).trigger("change"); 
 
                    $("#G1741_C31388").val(item.G1741_C31388).trigger("change"); 

                        $("#G1741_C31235").val(item.G1741_C31235);

                        $("#G1741_C31236").val(item.G1741_C31236);
 
                    $("#G1741_C31237").val(item.G1741_C31237).trigger("change"); 

                        $("#G1741_C31238").val(item.G1741_C31238);

                        $("#G1741_C31239").val(item.G1741_C31239);
 
                    $("#G1741_C31245").val(item.G1741_C31245).trigger("change"); 
 
                    $("#G1741_C31391").val(item.G1741_C31391).trigger("change"); 

                        $("#G1741_C31389").val(item.G1741_C31389);

                        $("#G1741_C31390").val(item.G1741_C31390);

                        $("#G1741_C61247").val(item.G1741_C61247);

                        $("#G1741_C64296").val(item.G1741_C64296);
		
                        if (item.G1741_C31958) {
                        	$("#G1741_C31958").val(item.G1741_C31958);
                        }else{
                        	$("#G1741_C31958").val("Sin Adjunto");
                        }
		
                        if (item.G1741_C31959) {
                        	$("#G1741_C31959").val(item.G1741_C31959);
                        }else{
                        	$("#G1741_C31959").val("Sin Adjunto");
                        }
		
                        if (item.G1741_C31960) {
                        	$("#G1741_C31960").val(item.G1741_C31960);
                        }else{
                        	$("#G1741_C31960").val("Sin Adjunto");
                        }
		
                        if (item.G1741_C31961) {
                        	$("#G1741_C31961").val(item.G1741_C31961);
                        }else{
                        	$("#G1741_C31961").val("Sin Adjunto");
                        }
		
                        if (item.G1741_C31962) {
                        	$("#G1741_C31962").val(item.G1741_C31962);
                        }else{
                        	$("#G1741_C31962").val("Sin Adjunto");
                        }
		
                        if (item.G1741_C31963) {
                        	$("#G1741_C31963").val(item.G1741_C31963);
                        }else{
                        	$("#G1741_C31963").val("Sin Adjunto");
                        }
		
                        if (item.G1741_C31964) {
                        	$("#G1741_C31964").val(item.G1741_C31964);
                        }else{
                        	$("#G1741_C31964").val("Sin Adjunto");
                        }
			            cargarHijos_0($("#G1741_C31240").val());
        				$("#h3mio").html(item.principal);
        				
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 
    } 

    <?php } ?>


    

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','Id Caso','Fecha','Gestor','Gestión', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
                ,
                {  
                    name:'G1745_C31392', 
                    index:'G1745_C31392', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1745_C31393', 
                    index: 'G1745_C31393', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1745_C31394', 
                    index: 'G1745_C31394', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {
                    name:'G1745_C31395', 
                    index:'G1745_C31395', 
                    width:150, 
                    editable: true 
                }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles0",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1745_C31392',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Gestiones',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1745&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=31392&formularioPadre=1741<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){
        
    	$("#btnLlamar_0").attr('padre', $("#G1741_C31240").val());
    		var id_0 = $("#G1741_C31240").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
    }
</script>
<script type="text/javascript">
	$(document).ready(function() {
		<?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                		while($objDatos = $resultDatosSql->fetch_object()){ ?>
                        	document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
             		<?php  
             			}	
                    }
                    
                } */  
            }
        ?>
    	<?php if(isset($_GET['user'])){ ?>
        	
    	$("#btnLlamar_0").attr('padre', $("#G1741_C31240").val());
    		var id_0 = $("#G1741_C31240").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
        	idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
		
	});
</script>
