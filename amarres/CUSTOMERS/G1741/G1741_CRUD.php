<?php
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (isset($_GET['adjunto'])) {
            $archivo=$_GET['adjunto'];
            $extensiones = explode(".", $archivo);
            // $ext = ['pdf','jpeg','jpg','png'];
            // if (in_array(end($extensiones),$ext)) {
                if (is_file("/Dyalogo/tmp/G1741/".$archivo)) {
                    $size = strlen("/Dyalogo/tmp/G1741/".$archivo);

                    if ($size>0) {
                        $nombre=basename("/Dyalogo/tmp/G1741/".$archivo);
                        $tamaño = filesize("/Dyalogo/tmp/G1741/".$archivo);
                        header('Content-Description: File Transfer');
                        header("Content-type: application/force-download");
                        header("Content-disposition: attachment; filename=".$nombre);
                        header("Content-Transfer-Encoding: binary");
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header("Content-Length: " . $tamaño);
                        ob_clean();
                        flush();
                        readfile("/Dyalogo/tmp/G1741/".$archivo);
                    }            
                }else{ 
                    // header('Location:' . getenv('HTTP_REFERER'));
                    echo "<h1>EL ARCHIVO NO EXISTE O LO HAN BORRADO...<br> !!VUELVE ATRAS</h1>";
                }            
            // }else{
                // header('Location:' . getenv('HTTP_REFERER'));
                // echo "Este archivo no se puede descargar.";
            // }

    }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1741_ConsInte__b, G1741_FechaInsercion , G1741_Usuario ,  G1741_CodigoMiembro  , G1741_PoblacionOrigen , G1741_EstadoDiligenciamiento ,  G1741_IdLlamada , G1741_C31240 as principal ,G1741_C31240,G1741_C31244,G1741_C31241,G1741_C31242,G1741_C31243,G1741_C31363,G1741_C31388,G1741_C31235,G1741_C31236,G1741_C31237,G1741_C31238,G1741_C31239,G1741_C31245,G1741_C31391,G1741_C31389,G1741_C31390,G1741_C31958,G1741_C31959,G1741_C31960,G1741_C31961,G1741_C31962,G1741_C31963,G1741_C31964, G1741_C61247, G1741_C64296 FROM '.$BaseDatos.'.G1741 WHERE G1741_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1741_C31240'] = $key->G1741_C31240;

                $datos[$i]['G1741_C31244'] = $key->G1741_C31244;

                $datos[$i]['G1741_C31241'] = $key->G1741_C31241;

                $datos[$i]['G1741_C31242'] = $key->G1741_C31242;

                $datos[$i]['G1741_C31243'] = $key->G1741_C31243;

                $datos[$i]['G1741_C31363'] = $key->G1741_C31363;

                $datos[$i]['G1741_C31388'] = $key->G1741_C31388;

                $datos[$i]['G1741_C31235'] = $key->G1741_C31235;

                $datos[$i]['G1741_C31236'] = $key->G1741_C31236;

                $datos[$i]['G1741_C31237'] = $key->G1741_C31237;

                $datos[$i]['G1741_C31238'] = $key->G1741_C31238;

                $datos[$i]['G1741_C31239'] = $key->G1741_C31239;

                $datos[$i]['G1741_C31245'] = $key->G1741_C31245;

                $datos[$i]['G1741_C31391'] = $key->G1741_C31391;

                $datos[$i]['G1741_C31389'] = explode(' ', $key->G1741_C31389)[0];

                $datos[$i]['G1741_C31390'] = explode(' ', $key->G1741_C31390)[0];

                $datos[$i]['G1741_C31958'] = $key->G1741_C31958;

                $datos[$i]['G1741_C31959'] = $key->G1741_C31959;

                $datos[$i]['G1741_C31960'] = $key->G1741_C31960;

                $datos[$i]['G1741_C31961'] = $key->G1741_C31961;

                $datos[$i]['G1741_C31962'] = $key->G1741_C31962;

                $datos[$i]['G1741_C31963'] = $key->G1741_C31963;

                $datos[$i]['G1741_C31964'] = $key->G1741_C31964;

                $datos[$i]['G1741_C61247'] = $key->G1741_C61247;

                $datos[$i]['G1741_C64296'] = $key->G1741_C64296;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1741_ConsInte__b as id,  G1741_C31240 as camp1 , G1741_C31238 as camp2 ";
            $Lsql .= " FROM ".$BaseDatos.".G1741 ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " WHERE G1741_Usuario = ".$SESSION["IDENTIFICACION"]." AND G1741_C31240 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1741_C31238 like '%".$_POST['Busqueda']."%' ";
            }
            $Lsql .= " WHERE G1741_Usuario = ".$SESSION["IDENTIFICACION"]." ORDER BY G1741_ConsInte__b DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

        if(isset($_GET['MostrarCombo_Guion_G1741_C31245'])){
            echo '<select class="form-control input-sm"  name="G1741_C31245" id="G1741_C31245">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['Combo_Guion_G1741_C31245'])){
            $Ysql = "SELECT G1740_ConsInte__b as id,  G1740_C31227 as text FROM ".$BaseDatos.".G1740 WHERE G1740_ConsInte__b = ".$_POST['q'];
            $guion = $mysqli->query($Ysql);
            $obj = $guion->fetch_array();

            echo "<option value='".$obj['id']."'>".$obj['text']."</option>";
        }

        if(isset($_GET['CallDatosCombo_Guion_G1741_C31245'])){
            $Ysql = "SELECT G1740_ConsInte__b as id,  G1740_C31227 as text FROM ".$BaseDatos.".G1740 WHERE G1740_C31227 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_G1741_C31245'])){
             $Lsql = "SELECT  G1740_ConsInte__b AS id, G1740_C31225 AS nit  FROM ".$BaseDatos.".G1740 WHERE G1740_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_G1741_C31245'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	            $data[]=$key;
                $i++;
            }
            
            echo json_encode($data);
        }
        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1741");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1741_ConsInte__b, G1741_FechaInsercion , G1741_Usuario ,  G1741_CodigoMiembro  , G1741_PoblacionOrigen , G1741_EstadoDiligenciamiento ,  G1741_IdLlamada , G1741_C31240 as principal ,G1741_C31240,G1741_C31244, a.LISOPC_Nombre____b as G1741_C31241, b.LISOPC_Nombre____b as G1741_C31242, c.LISOPC_Nombre____b as G1741_C31243, d.LISOPC_Nombre____b as G1741_C31363, e.LISOPC_Nombre____b as G1741_C31388,G1741_C31235,G1741_C31236, f.LISOPC_Nombre____b as G1741_C31237,G1741_C31238,G1741_C31239, G1740_C31228, g.LISOPC_Nombre____b as G1741_C31391,G1741_C31389,G1741_C31390,G1741_C31958,G1741_C31959,G1741_C31960,G1741_C31961,G1741_C31962,G1741_C31963,G1741_C31964 FROM '.$BaseDatos.'.G1741 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1741_C31241 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1741_C31242 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1741_C31243 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1741_C31363 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1741_C31388 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1741_C31237 LEFT JOIN '.$BaseDatos.'.G1740 ON G1740_ConsInte__b  =  G1741_C31245 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1741_C31391';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G1741_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1741_ConsInte__b , ($fila->G1741_C31240) , ($fila->G1741_C31244) , ($fila->G1741_C31241) , ($fila->G1741_C31242) , ($fila->G1741_C31243) , ($fila->G1741_C31363) , ($fila->G1741_C31388) , ($fila->G1741_C31235) , ($fila->G1741_C31236) , ($fila->G1741_C31237) , ($fila->G1741_C31238) , ($fila->G1741_C31239) , ($fila->G1740_C31228) , ($fila->G1741_C31391) , explode(' ', $fila->G1741_C31389)[0] , explode(' ', $fila->G1741_C31390)[0] , ($fila->G1741_C31958) , ($fila->G1741_C31959) , ($fila->G1741_C31960) , ($fila->G1741_C31961) , ($fila->G1741_C31962) , ($fila->G1741_C31963) , ($fila->G1741_C31964) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1741 WHERE G1741_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1741_ConsInte__b as id,  G1741_C31240 as camp1 , G1741_C31238 as camp2  FROM '.$BaseDatos.'.G1741 ORDER BY G1741_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1741 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1741(";
            $LsqlV = " VALUES ("; 
  
            $G1741_C31240 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1741_C31240"])){
                if($_POST["G1741_C31240"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1741_C31240 = $_POST["G1741_C31240"];
                    $LsqlU .= $separador." G1741_C31240 = ".$G1741_C31240."";
                    $LsqlI .= $separador." G1741_C31240";
                    $LsqlV .= $separador.$G1741_C31240;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1741_C31244"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C31244 = '".$_POST["G1741_C31244"]."'";
                $LsqlI .= $separador."G1741_C31244";
                $LsqlV .= $separador."'".$_POST["G1741_C31244"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1741_C31241"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C31241 = '".$_POST["G1741_C31241"]."'";
                $LsqlI .= $separador."G1741_C31241";
                $LsqlV .= $separador."'".$_POST["G1741_C31241"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1741_C31242"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C31242 = '".$_POST["G1741_C31242"]."'";
                $LsqlI .= $separador."G1741_C31242";
                $LsqlV .= $separador."'".$_POST["G1741_C31242"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1741_C31243"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C31243 = '".$_POST["G1741_C31243"]."'";
                $LsqlI .= $separador."G1741_C31243";
                $LsqlV .= $separador."'".$_POST["G1741_C31243"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1741_C31363"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C31363 = '".$_POST["G1741_C31363"]."'";
                $LsqlI .= $separador."G1741_C31363";
                $LsqlV .= $separador."'".$_POST["G1741_C31363"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1741_C31388"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C31388 = '".$_POST["G1741_C31388"]."'";
                $LsqlI .= $separador."G1741_C31388";
                $LsqlV .= $separador."'".$_POST["G1741_C31388"]."'";
                $validar = 1;
            }

            if(isset($_POST["G1741_C61247"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C61247 = '".$_POST["G1741_C61247"]."'";
                $LsqlI .= $separador."G1741_C61247";
                $LsqlV .= $separador."'".$_POST["G1741_C61247"]."'";
                $validar = 1;
            }

            if(isset($_POST["G1741_C63630"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlI .= $separador."G1741_C63630";
                $LsqlV .= $separador."'".$_POST["G1741_C63630"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1741_C31235"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C31235 = '".$_POST["G1741_C31235"]."'";
                $LsqlI .= $separador."G1741_C31235";
                $LsqlV .= $separador."'".$_POST["G1741_C31235"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1741_C31236"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C31236 = '".$_POST["G1741_C31236"]."'";
                $LsqlI .= $separador."G1741_C31236";
                $LsqlV .= $separador."'".$_POST["G1741_C31236"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1741_C31237"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C31237 = '".$_POST["G1741_C31237"]."'";
                $LsqlI .= $separador."G1741_C31237";
                $LsqlV .= $separador."'".$_POST["G1741_C31237"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1741_C31238"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C31238 = '".$_POST["G1741_C31238"]."'";
                $LsqlI .= $separador."G1741_C31238";
                $LsqlV .= $separador."'".$_POST["G1741_C31238"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1741_C31239"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C31239 = '".$_POST["G1741_C31239"]."'";
                $LsqlI .= $separador."G1741_C31239";
                $LsqlV .= $separador."'".$_POST["G1741_C31239"]."'";
                $validar = 1;
            }

            if(isset($_POST["G1741_C64296"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C64296 = '".$_POST["G1741_C64296"]."'";
                $LsqlI .= $separador."G1741_C64296";
                $LsqlV .= $separador."'".$_POST["G1741_C64296"]."'";
                $validar = 1;
            }
  
            if(isset($_POST["G1741_C31245"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C31245 = '".$_POST["G1741_C31245"]."'";
                $LsqlI .= $separador."G1741_C31245";
                $LsqlV .= $separador."'".$_POST["G1741_C31245"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1741_C31391"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_C31391 = '".$_POST["G1741_C31391"]."'";
                $LsqlI .= $separador."G1741_C31391";
                $LsqlV .= $separador."'".$_POST["G1741_C31391"]."'";
                $validar = 1;
            }
             
 
            $G1741_C31389 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1741_C31389"])){    
                if($_POST["G1741_C31389"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1741_C31389"]);
                    if(count($tieneHora) > 1){
                    	$G1741_C31389 = "'".$_POST["G1741_C31389"]."'";
                    }else{
                    	$G1741_C31389 = "'".str_replace(' ', '',$_POST["G1741_C31389"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1741_C31389 = ".$G1741_C31389;
                    $LsqlI .= $separador." G1741_C31389";
                    $LsqlV .= $separador.$G1741_C31389;
                    $validar = 1;
                }
            }
 
            $G1741_C31390 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1741_C31390"])){    
                if($_POST["G1741_C31390"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1741_C31390"]);
                    if(count($tieneHora) > 1){
                    	$G1741_C31390 = "'".$_POST["G1741_C31390"]."'";
                    }else{
                    	$G1741_C31390 = "'".str_replace(' ', '',$_POST["G1741_C31390"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1741_C31390 = ".$G1741_C31390;
                    $LsqlI .= $separador." G1741_C31390";
                    $LsqlV .= $separador.$G1741_C31390;
                    $validar = 1;
                }
            }

            if (isset($_FILES)) {
                $destinoFile = '/Dyalogo/tmp/G1741/';
                $fechUp = date('Y-m-d_H:i:s');
                if (!file_exists("/Dyalogo/tmp/G1741")){
                    mkdir("/Dyalogo/tmp/G1741", 0777);
                }

                if (isset($_FILES["FG1741_C31958"]['tmp_name'])) {
                    if ($_FILES["FG1741_C31958"]['size'] != 0) {
                        $G1741_C31958 = $_FILES["FG1741_C31958"]['tmp_name'];
                        $nG1741_C31958 = $fechUp."_".$_FILES["FG1741_C31958"]['name'];
                        $rutaFinal = $destinoFile.$nG1741_C31958;
                        if (is_uploaded_file($G1741_C31958)) {
                            move_uploaded_file($G1741_C31958, $rutaFinal);
                        }

                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $LsqlU .= $separador."G1741_C31958 = '".$nG1741_C31958."'";
                        $LsqlI .= $separador."G1741_C31958";
                        $LsqlV .= $separador."'".$nG1741_C31958."'";
                        $validar = 1;
                    }
                }

                if (isset($_FILES["FG1741_C31959"]['tmp_name'])) {
                    if ($_FILES["FG1741_C31959"]['size'] != 0) {
                        $G1741_C31959 = $_FILES["FG1741_C31959"]['tmp_name'];
                        $nG1741_C31959 = $fechUp."_".$_FILES["FG1741_C31959"]['name'];
                        $rutaFinal = $destinoFile.$nG1741_C31959;
                        if (is_uploaded_file($G1741_C31959)) {
                            move_uploaded_file($G1741_C31959, $rutaFinal);
                        }

                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $LsqlU .= $separador."G1741_C31959 = '".$nG1741_C31959."'";
                        $LsqlI .= $separador."G1741_C31959";
                        $LsqlV .= $separador."'".$nG1741_C31959."'";
                        $validar = 1;
                    }
                }

                if (isset($_FILES["FG1741_C31960"]['tmp_name'])) {
                    if ($_FILES["FG1741_C31960"]['size'] != 0) {
                        $G1741_C31960 = $_FILES["FG1741_C31960"]['tmp_name'];
                        $nG1741_C31960 = $fechUp."_".$_FILES["FG1741_C31960"]['name'];
                        $rutaFinal = $destinoFile.$nG1741_C31960;
                        if (is_uploaded_file($G1741_C31960)) {
                            move_uploaded_file($G1741_C31960, $rutaFinal);
                        }

                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $LsqlU .= $separador."G1741_C31960 = '".$nG1741_C31960."'";
                        $LsqlI .= $separador."G1741_C31960";
                        $LsqlV .= $separador."'".$nG1741_C31960."'";
                        $validar = 1;
                    }
                }

                if (isset($_FILES["FG1741_C31961"]['tmp_name'])) {
                    if ($_FILES["FG1741_C31961"]['size'] != 0) {
                        $G1741_C31961 = $_FILES["FG1741_C31961"]['tmp_name'];
                        $nG1741_C31961 = $fechUp."_".$_FILES["FG1741_C31961"]['name'];
                        $rutaFinal = $destinoFile.$nG1741_C31961;
                        if (is_uploaded_file($G1741_C31961)) {
                            move_uploaded_file($G1741_C31961, $rutaFinal);
                        }

                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $LsqlU .= $separador."G1741_C31961 = '".$nG1741_C31961."'";
                        $LsqlI .= $separador."G1741_C31961";
                        $LsqlV .= $separador."'".$nG1741_C31961."'";
                        $validar = 1;
                    }
                }

                if (isset($_FILES["FG1741_C31962"]['tmp_name'])) {
                    if ($_FILES["FG1741_C31962"]['size'] != 0) {
                        $G1741_C31962 = $_FILES["FG1741_C31962"]['tmp_name'];
                        $nG1741_C31962 = $fechUp."_".$_FILES["FG1741_C31962"]['name'];
                        $rutaFinal = $destinoFile.$nG1741_C31962;
                        if (is_uploaded_file($G1741_C31962)) {
                            move_uploaded_file($G1741_C31962, $rutaFinal);
                        }

                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $LsqlU .= $separador."G1741_C31962 = '".$nG1741_C31962."'";
                        $LsqlI .= $separador."G1741_C31962";
                        $LsqlV .= $separador."'".$nG1741_C31962."'";
                        $validar = 1;
                    }
                }

                if (isset($_FILES["FG1741_C31963"]['tmp_name'])) {
                    if ($_FILES["FG1741_C31963"]['size'] != 0) {
                        $G1741_C31963 = $_FILES["FG1741_C31963"]['tmp_name'];
                        $nG1741_C31963 = $fechUp."_".$_FILES["FG1741_C31963"]['name'];
                        $rutaFinal = $destinoFile.$nG1741_C31963;
                        if (is_uploaded_file($G1741_C31963)) {
                            move_uploaded_file($G1741_C31963, $rutaFinal);
                        }

                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $LsqlU .= $separador."G1741_C31963 = '".$nG1741_C31963."'";
                        $LsqlI .= $separador."G1741_C31963";
                        $LsqlV .= $separador."'".$nG1741_C31963."'";
                        $validar = 1;
                    }
                }

                if (isset($_FILES["FG1741_C31964"]['tmp_name'])) {
                    if ($_FILES["FG1741_C31964"]['size'] != 0) {
                        $G1741_C31964 = $_FILES["FG1741_C31964"]['tmp_name'];
                        $nG1741_C31964 = $fechUp."_".$_FILES["FG1741_C31964"]['name'];
                        $rutaFinal = $destinoFile.$nG1741_C31964;
                        if (is_uploaded_file($G1741_C31964)) {
                            move_uploaded_file($G1741_C31964, $rutaFinal);
                        }

                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $LsqlU .= $separador."G1741_C31964 = '".$nG1741_C31964."'";
                        $LsqlI .= $separador."G1741_C31964";
                        $LsqlV .= $separador."'".$nG1741_C31964."'";
                        $validar = 1;
                    }
                }
            }
             

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1741_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1741_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}


			$padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1741_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
			if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    if (!is_null($_POST["backoffice"])) {
                        $intAsignar_t = $_POST["backoffice"];
                    }else{
                        $intAsignar_t = $_GET['usuario'];
                    }
                    $LsqlI .= ", G1741_Usuario , G1741_FechaInsercion";
                    $LsqlV .= ", ".$intAsignar_t." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1741_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1741 WHERE G1741_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta
            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                    	echo $mysqli->insert_id;
                	}else{
                		
                		echo "1";    		
                	}

                } else {
                	echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1745_ConsInte__b, G1745_C31392, G1745_C31393, G1745_C31394, G1745_C31395 FROM ".$BaseDatos.".G1745  ";

        $SQL .= " WHERE G1745_C31392 = '".$numero."'"; 

        $SQL .= " ORDER BY G1745_C31392";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1745_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1745_ConsInte__b)."</cell>"; 
            

            echo "<cell>". $fila->G1745_C31392."</cell>"; 

            echo "<cell>". ($fila->G1745_C31393)."</cell>";

            echo "<cell>". ($fila->G1745_C31394)."</cell>";

            echo "<cell><![CDATA[". ($fila->G1745_C31395)."]]></cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1745 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1745(";
            $LsqlV = " VALUES ("; 
 
                                                                         
            if(isset($_POST["G1745_C31393"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1745_C31393 = '".$_POST["G1745_C31393"]."'";
                $LsqlI .= $separador."G1745_C31393";
                $LsqlV .= $separador."'".$_POST["G1745_C31393"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1745_C31394"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1745_C31394 = '".$_POST["G1745_C31394"]."'";
                $LsqlI .= $separador."G1745_C31394";
                $LsqlV .= $separador."'".$_POST["G1745_C31394"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
  

            if(isset($_POST["G1745_C31395"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1745_C31395 = '".$_POST["G1745_C31395"]."'";
                $LsqlI .= $separador."G1745_C31395";
                $LsqlV .= $separador."'".$_POST["G1745_C31395"]."'";
                $validar = 1;
            }
                                                                           

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1745_C31392 = $numero;
                    $LsqlU .= ", G1745_C31392 = ".$G1745_C31392."";
                    $LsqlI .= ", G1745_C31392";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1745_Usuario ,  G1745_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1745_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1745 WHERE  G1745_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
