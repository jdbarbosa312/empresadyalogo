<?php
    session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
    
    if(isset($_POST['getListaHija'])){

        $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
        //echo $Lsql;
        $res = $mysqli->query($Lsql);
        echo "<option value='0'>Seleccione</option>";
        while($key = $res->fetch_object()){
            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
        }
    }   

    //Inserciones o actualizaciones
    if(isset($_POST["oper"])){
        $str_Lsql  = '';

        $validar = 0;
        $str_LsqlU = "UPDATE ".$BaseDatos.".G1446 SET "; 
        $str_LsqlI = "INSERT INTO ".$BaseDatos.".G1446( G1446_FechaInsercion ,";
        $str_LsqlV = " VALUES ('".date('Y-m-d H:s:i')."',"; 
  
        if(isset($_POST["G1446_C25390"]) && $_POST["G1446_C25390"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1446_C25390 = '".$_POST["G1446_C25390"]."'";
            $str_LsqlI .= $separador."G1446_C25390";
            $str_LsqlV .= $separador."'".$_POST["G1446_C25390"]."'";
            $validar = 1;
        }
         
 
        $G1446_C25383 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1446_C25383"])){    
            if($_POST["G1446_C25383"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1446_C25383 = "'".str_replace(' ', '',$_POST["G1446_C25383"])." 00:00:00'";
                $str_LsqlU .= $separador." G1446_C25383 = ".$G1446_C25383;
                $str_LsqlI .= $separador." G1446_C25383";
                $str_LsqlV .= $separador.$G1446_C25383;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1446_C25384"]) && $_POST["G1446_C25384"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1446_C25384 = '".$_POST["G1446_C25384"]."'";
            $str_LsqlI .= $separador."G1446_C25384";
            $str_LsqlV .= $separador."'".$_POST["G1446_C25384"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1446_C25385"]) && $_POST["G1446_C25385"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1446_C25385 = '".$_POST["G1446_C25385"]."'";
            $str_LsqlI .= $separador."G1446_C25385";
            $str_LsqlV .= $separador."'".$_POST["G1446_C25385"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1446_C25386"]) && $_POST["G1446_C25386"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1446_C25386 = '".$_POST["G1446_C25386"]."'";
            $str_LsqlI .= $separador."G1446_C25386";
            $str_LsqlV .= $separador."'".$_POST["G1446_C25386"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1446_C25387"]) && $_POST["G1446_C25387"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1446_C25387 = '".$_POST["G1446_C25387"]."'";
            $str_LsqlI .= $separador."G1446_C25387";
            $str_LsqlV .= $separador."'".$_POST["G1446_C25387"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1446_C25388"]) && $_POST["G1446_C25388"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1446_C25388 = '".$_POST["G1446_C25388"]."'";
            $str_LsqlI .= $separador."G1446_C25388";
            $str_LsqlV .= $separador."'".$_POST["G1446_C25388"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1446_C25933"]) && $_POST["G1446_C25933"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1446_C25933 = '".$_POST["G1446_C25933"]."'";
            $str_LsqlI .= $separador."G1446_C25933";
            $str_LsqlV .= $separador."'".$_POST["G1446_C25933"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1446_C25934"]) && $_POST["G1446_C25934"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1446_C25934 = '".$_POST["G1446_C25934"]."'";
            $str_LsqlI .= $separador."G1446_C25934";
            $str_LsqlV .= $separador."'".$_POST["G1446_C25934"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1446_C25382"]) && $_POST["G1446_C25382"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1446_C25382 = '".$_POST["G1446_C25382"]."'";
            $str_LsqlI .= $separador."G1446_C25382";
            $str_LsqlV .= $separador."'".$_POST["G1446_C25382"]."'";
            $validar = 1;
        }
         
 
        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $str_Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($str_Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G1446_C";
                $valorH = $valorG.$campo;
                $str_LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $str_LsqlI .= $separador." ".$valorH;
                $str_LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1446_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $str_LsqlI .= $separador."G1446_IdLlamada";
            $str_LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        if(isset($_POST['ORIGEN_DY_WF'])){
            if($_POST['ORIGEN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $Origen = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 1446 AND PREGUN_Texto_____b = 'ORIGEN_DY_WF'";
                $res_Origen = $mysqli->query($Origen);
                if($res_Origen->num_rows > 0){
                    $dataOrigen = $res_Origen->fetch_array();

                    $str_LsqlU .= $separador."G1446_C".$dataOrigen['PREGUN_ConsInte__b']." = '".$_POST['ORIGEN_DY_WF']."'";
                    $str_LsqlI .= $separador."G1446_C".$dataOrigen['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador."'".$_POST['ORIGEN_DY_WF']."'";
                    $validar = 1;
                }
                

            }
        }

        if(isset($_POST['OPTIN_DY_WF'])){
            if($_POST['OPTIN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $confirmado = null;
                if($_POST['OPTIN_DY_WF'] == 'SIMPLE'){
                    $confirmado  = "'CONFIRMADO'";
                }

                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 1446 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                if($res_OPTIN_DY_WF->num_rows > 0){
                    $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();

                    $str_LsqlU .= $separador."G1446_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = ".$confirmado;
                    $str_LsqlI .= $separador."G1446_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador." ".$confirmado;
                    $validar = 1;
                }
            }
        }


        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' ){
                
                $str_Lsql = $str_LsqlI.")" . $str_LsqlV.")";
            }
        }

        //si trae algo que insertar inserta

        //echo $str_Lsql;
        if($validar == 1){
            if ($mysqli->query($str_Lsql) === TRUE) {
                $ultimoResgistroInsertado = $mysqli->insert_id;

                


                if(isset($_POST['v'])){
                    
                
                }else{
                    header('Location:http://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=MTQ0Ng==&result=1');
                }
            } else {
                echo "Error Hacieno el proceso los registros : " . $mysqli->error;
            }
        }
    }
    


?>
