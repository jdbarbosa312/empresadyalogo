<?php
    session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
    
    if(isset($_POST['getListaHija'])){

        $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
        //echo $Lsql;
        $res = $mysqli->query($Lsql);
        echo "<option value='0'>Seleccione</option>";
        while($key = $res->fetch_object()){
            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
        }
    }   

    //Inserciones o actualizaciones
    if(isset($_POST["oper"])){
        $str_Lsql  = '';

        $validar = 0;
        $str_LsqlU = "UPDATE ".$BaseDatos.".G2264 SET "; 
        $str_LsqlI = "INSERT INTO ".$BaseDatos.".G2264( G2264_FechaInsercion ,";
        $str_LsqlV = " VALUES ('".date('Y-m-d H:s:i')."',"; 
  
        if(isset($_POST["G2264_C44231"]) && $_POST["G2264_C44231"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44231 = '".$_POST["G2264_C44231"]."'";
            $str_LsqlI .= $separador."G2264_C44231";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44231"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44232"]) && $_POST["G2264_C44232"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44232 = '".$_POST["G2264_C44232"]."'";
            $str_LsqlI .= $separador."G2264_C44232";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44232"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44233"]) && $_POST["G2264_C44233"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44233 = '".$_POST["G2264_C44233"]."'";
            $str_LsqlI .= $separador."G2264_C44233";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44233"]."'";
            $validar = 1;
        }
         
  
        $G2264_C44234 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G2264_C44234"])){
            if($_POST["G2264_C44234"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G2264_C44234 = $_POST["G2264_C44234"];
                $G2264_C44234 = str_replace(".", "", $_POST["G2264_C44234"]);
                $G2264_C44234 =  str_replace(",", ".", $G2264_C44234);
                $str_LsqlU .= $separador." G2264_C44234 = '".$G2264_C44234."'";
                $str_LsqlI .= $separador." G2264_C44234";
                $str_LsqlV .= $separador."'".$G2264_C44234."'";
                $validar = 1;
            }
        }
  
        if(isset($_POST["G2264_C44235"]) && $_POST["G2264_C44235"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44235 = '".$_POST["G2264_C44235"]."'";
            $str_LsqlI .= $separador."G2264_C44235";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44235"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44236"]) && $_POST["G2264_C44236"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44236 = '".$_POST["G2264_C44236"]."'";
            $str_LsqlI .= $separador."G2264_C44236";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44236"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44237"]) && $_POST["G2264_C44237"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44237 = '".$_POST["G2264_C44237"]."'";
            $str_LsqlI .= $separador."G2264_C44237";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44237"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44238"]) && $_POST["G2264_C44238"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44238 = '".$_POST["G2264_C44238"]."'";
            $str_LsqlI .= $separador."G2264_C44238";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44238"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44239"]) && $_POST["G2264_C44239"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44239 = '".$_POST["G2264_C44239"]."'";
            $str_LsqlI .= $separador."G2264_C44239";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44239"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44240"]) && $_POST["G2264_C44240"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44240 = '".$_POST["G2264_C44240"]."'";
            $str_LsqlI .= $separador."G2264_C44240";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44240"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44241"]) && $_POST["G2264_C44241"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44241 = '".$_POST["G2264_C44241"]."'";
            $str_LsqlI .= $separador."G2264_C44241";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44241"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44242"]) && $_POST["G2264_C44242"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44242 = '".$_POST["G2264_C44242"]."'";
            $str_LsqlI .= $separador."G2264_C44242";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44242"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44243"]) && $_POST["G2264_C44243"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44243 = '".$_POST["G2264_C44243"]."'";
            $str_LsqlI .= $separador."G2264_C44243";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44243"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44244"]) && $_POST["G2264_C44244"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44244 = '".$_POST["G2264_C44244"]."'";
            $str_LsqlI .= $separador."G2264_C44244";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44244"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44245"]) && $_POST["G2264_C44245"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44245 = '".$_POST["G2264_C44245"]."'";
            $str_LsqlI .= $separador."G2264_C44245";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44245"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44230"]) && $_POST["G2264_C44230"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44230 = '".$_POST["G2264_C44230"]."'";
            $str_LsqlI .= $separador."G2264_C44230";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44230"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44246"]) && $_POST["G2264_C44246"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44246 = '".$_POST["G2264_C44246"]."'";
            $str_LsqlI .= $separador."G2264_C44246";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44246"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44247"]) && $_POST["G2264_C44247"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44247 = '".$_POST["G2264_C44247"]."'";
            $str_LsqlI .= $separador."G2264_C44247";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44247"]."'";
            $validar = 1;
        }
         
  
        $G2264_C44248 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G2264_C44248"])){
            if($_POST["G2264_C44248"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G2264_C44248 = $_POST["G2264_C44248"];
                $G2264_C44248 = str_replace(".", "", $_POST["G2264_C44248"]);
                $G2264_C44248 =  str_replace(",", ".", $G2264_C44248);
                $str_LsqlU .= $separador." G2264_C44248 = '".$G2264_C44248."'";
                $str_LsqlI .= $separador." G2264_C44248";
                $str_LsqlV .= $separador."'".$G2264_C44248."'";
                $validar = 1;
            }
        }
 
        $G2264_C44249 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G2264_C44249"])){    
            if($_POST["G2264_C44249"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G2264_C44249 = "'".str_replace(' ', '',$_POST["G2264_C44249"])." 00:00:00'";
                $str_LsqlU .= $separador." G2264_C44249 = ".$G2264_C44249;
                $str_LsqlI .= $separador." G2264_C44249";
                $str_LsqlV .= $separador.$G2264_C44249;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G2264_C44250"]) && $_POST["G2264_C44250"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44250 = '".$_POST["G2264_C44250"]."'";
            $str_LsqlI .= $separador."G2264_C44250";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44250"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44251"]) && $_POST["G2264_C44251"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44251 = '".$_POST["G2264_C44251"]."'";
            $str_LsqlI .= $separador."G2264_C44251";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44251"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44252"]) && $_POST["G2264_C44252"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44252 = '".$_POST["G2264_C44252"]."'";
            $str_LsqlI .= $separador."G2264_C44252";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44252"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44253"]) && $_POST["G2264_C44253"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44253 = '".$_POST["G2264_C44253"]."'";
            $str_LsqlI .= $separador."G2264_C44253";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44253"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44254"]) && $_POST["G2264_C44254"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44254 = '".$_POST["G2264_C44254"]."'";
            $str_LsqlI .= $separador."G2264_C44254";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44254"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44255"]) && $_POST["G2264_C44255"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44255 = '".$_POST["G2264_C44255"]."'";
            $str_LsqlI .= $separador."G2264_C44255";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44255"]."'";
            $validar = 1;
        }
         
 
        $G2264_C44256 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G2264_C44256"])){    
            if($_POST["G2264_C44256"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G2264_C44256 = "'".str_replace(' ', '',$_POST["G2264_C44256"])." 00:00:00'";
                $str_LsqlU .= $separador." G2264_C44256 = ".$G2264_C44256;
                $str_LsqlI .= $separador." G2264_C44256";
                $str_LsqlV .= $separador.$G2264_C44256;
                $validar = 1;
            }
        }
  
        $G2264_C44257 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
        if(isset($_POST["G2264_C44257"])){   
            if($_POST["G2264_C44257"] != '' && $_POST["G2264_C44257"] != 'undefined' && $_POST["G2264_C44257"] != 'null'){
                $separador = "";
                $fecha = date('Y-m-d');
                if($validar == 1){
                    $separador = ",";
                }

                $G2264_C44257 = "'".$fecha." ".str_replace(' ', '',$_POST["G2264_C44257"])."'";
                $str_LsqlU .= $separador." G2264_C44257 = ".$G2264_C44257."";
                $str_LsqlI .= $separador." G2264_C44257";
                $str_LsqlV .= $separador.$G2264_C44257;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G2264_C44258"]) && $_POST["G2264_C44258"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44258 = '".$_POST["G2264_C44258"]."'";
            $str_LsqlI .= $separador."G2264_C44258";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44258"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G2264_C44259"]) && $_POST["G2264_C44259"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_C44259 = '".$_POST["G2264_C44259"]."'";
            $str_LsqlI .= $separador."G2264_C44259";
            $str_LsqlV .= $separador."'".$_POST["G2264_C44259"]."'";
            $validar = 1;
        }
         
 
        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $str_Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($str_Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G2264_C";
                $valorH = $valorG.$campo;
                $str_LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $str_LsqlI .= $separador." ".$valorH;
                $str_LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G2264_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $str_LsqlI .= $separador."G2264_IdLlamada";
            $str_LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        if(isset($_POST['ORIGEN_DY_WF'])){
            if($_POST['ORIGEN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $Origen = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2264 AND PREGUN_Texto_____b = 'ORIGEN_DY_WF'";
                $res_Origen = $mysqli->query($Origen);
                if($res_Origen->num_rows > 0){
                    $dataOrigen = $res_Origen->fetch_array();

                    $str_LsqlU .= $separador."G2264_C".$dataOrigen['PREGUN_ConsInte__b']." = '".$_POST['ORIGEN_DY_WF']."'";
                    $str_LsqlI .= $separador."G2264_C".$dataOrigen['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador."'".$_POST['ORIGEN_DY_WF']."'";
                    $validar = 1;
                }
                

            }
        }

        if(isset($_POST['OPTIN_DY_WF'])){
            if($_POST['OPTIN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $confirmado = null;
                if($_POST['OPTIN_DY_WF'] == 'SIMPLE'){
                    $confirmado  = "'CONFIRMADO'";
                }

                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2264 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                if($res_OPTIN_DY_WF->num_rows > 0){
                    $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();

                    $str_LsqlU .= $separador."G2264_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = ".$confirmado;
                    $str_LsqlI .= $separador."G2264_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador." ".$confirmado;
                    $validar = 1;
                }
            }
        }


        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' ){
                
                $str_Lsql = $str_LsqlI.")" . $str_LsqlV.")";
            }
        }

        //si trae algo que insertar inserta

        //echo $str_Lsql;
        if($validar == 1){
            if ($mysqli->query($str_Lsql) === TRUE) {
                $ultimoResgistroInsertado = $mysqli->insert_id;

                


                if(isset($_POST['v'])){
                    
                
                }else{
                    header('Location:http://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=MjI2NA==&result=1');
                }
            } else {
                echo "Error Hacieno el proceso los registros : " . $mysqli->error;
            }
        }
    }
    


?>
