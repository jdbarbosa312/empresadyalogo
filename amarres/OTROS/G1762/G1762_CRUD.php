<?php
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
        $id1='';
        $id2='';
        
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1762_ConsInte__b, G1762_FechaInsercion , G1762_Usuario ,  G1762_CodigoMiembro  , G1762_PoblacionOrigen , G1762_EstadoDiligenciamiento ,  G1762_IdLlamada , G1762_C34514 ,G1762_C31739 as principal ,G1762_C31723,G1762_C31724,G1762_C31725,G1762_C31726,G1762_C31727,G1762_C31728,G1762_C31729,G1762_C31730,G1762_C31731,G1762_C31966,G1762_C31736,G1762_C31737,G1762_C31738,G1762_C31739,G1762_C31740,G1762_C31741,G1762_C31742,G1762_C31743,G1762_C31744,G1762_C31745,G1762_C31746,G1762_C31747,G1762_C31748,G1762_C31750,G1762_C31766,G1762_C31767,G1762_C31814,G1762_C31815,G1762_C31816,G1762_C31817,G1762_C31968,G1762_C31799,G1762_C34556,G1762_C34557 FROM '.$BaseDatos.'.G1762 WHERE G1762_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1762_C31723'] = $key->G1762_C31723;

                $datos[$i]['G1762_C31724'] = $key->G1762_C31724;

                $datos[$i]['G1762_C31725'] = explode(' ', $key->G1762_C31725)[0];
  
                if(!is_null($key->G1762_C31726)){
                    $datos[$i]['G1762_C31726'] = explode(' ', $key->G1762_C31726)[1];
                }

                $datos[$i]['G1762_C31727'] = $key->G1762_C31727;

                $datos[$i]['G1762_C31728'] = $key->G1762_C31728;

                $datos[$i]['G1762_C31729'] = $key->G1762_C31729;

                $datos[$i]['G1762_C31730'] = $key->G1762_C31730;

                $datos[$i]['G1762_C31731'] = $key->G1762_C31731;

                $datos[$i]['G1762_C31966'] = $key->G1762_C31966;

                $datos[$i]['G1762_C31736'] = $key->G1762_C31736;

                $datos[$i]['G1762_C31737'] = $key->G1762_C31737;

                $datos[$i]['G1762_C31738'] = $key->G1762_C31738;

                $datos[$i]['G1762_C31739'] = $key->G1762_C31739;

                $datos[$i]['G1762_C31740'] = $key->G1762_C31740;

                $datos[$i]['G1762_C31741'] = $key->G1762_C31741;

                $datos[$i]['G1762_C31742'] = $key->G1762_C31742;

                $datos[$i]['G1762_C31743'] = $key->G1762_C31743;

                $datos[$i]['G1762_C31744'] = $key->G1762_C31744;

                $datos[$i]['G1762_C31745'] = $key->G1762_C31745;

                $datos[$i]['G1762_C31746'] = $key->G1762_C31746;

                $datos[$i]['G1762_C31747'] = $key->G1762_C31747;

                $datos[$i]['G1762_C31748'] = explode(' ', $key->G1762_C31748)[0];

                $datos[$i]['G1762_C31750'] = $key->G1762_C31750;

                $datos[$i]['G1762_C31766'] = $key->G1762_C31766;

                $datos[$i]['G1762_C31767'] = $key->G1762_C31767;

                $datos[$i]['G1762_C31814'] = $key->G1762_C31814;

                $datos[$i]['G1762_C31815'] = $key->G1762_C31815;

                $datos[$i]['G1762_C31816'] = $key->G1762_C31816;

                $datos[$i]['G1762_C31817'] = $key->G1762_C31817;

                $datos[$i]['G1762_C31968'] = $key->G1762_C31968;

                $datos[$i]['G1762_C31799'] = $key->G1762_C31799;
                
                $datos[$i]['G1762_C34514'] = $key->G1762_C34514;
                
                $datos[$i]['G1762_C34556'] = $key->G1762_C34556;
                
                $datos[$i]['G1762_C34557'] = $key->G1762_C34557;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1762_ConsInte__b as id,  G1762_C31738 as camp2 , G1762_C31739 as camp1 ";
            $Lsql .= " FROM ".$BaseDatos.".G1762 ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " WHERE G1762_C31738 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1762_C31739 like '%".$_POST['Busqueda']."%' ";
            }
            $Lsql .= " ORDER BY G1762_ConsInte__b DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
	    	$res = $mysqli->query($Lsql);
            if(isset($_POST['link'])){
                while($key = $res->fetch_object()){
                    echo $key->LISOPC_Nombre____b;
                }
            }else{
                echo "<option value='0'>Seleccione</option>";  
                while($key = $res->fetch_object()){
                    echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
                }                
            }
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1762");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1762_ConsInte__b, G1762_FechaInsercion , G1762_Usuario ,  G1762_CodigoMiembro  , G1762_PoblacionOrigen , G1762_EstadoDiligenciamiento ,  G1762_IdLlamada , G1762_C31739 as principal , a.LISOPC_Nombre____b as G1762_C31723, b.LISOPC_Nombre____b as G1762_C31724,G1762_C31725,G1762_C31726,G1762_C34514,G1762_C31727,G1762_C31728,G1762_C31729,G1762_C31730,G1762_C31731,G1762_C31966,G1762_C31736,G1762_C31737,G1762_C31738,G1762_C31739,G1762_C31740,G1762_C31741,G1762_C31742,G1762_C31743,G1762_C31744,G1762_C31745,G1762_C31746,G1762_C31747,G1762_C31748, c.LISOPC_Nombre____b as G1762_C31750, d.LISOPC_Nombre____b as G1762_C31766,G1762_C31767, e.LISOPC_Nombre____b as G1762_C31814, f.LISOPC_Nombre____b as G1762_C31815, g.LISOPC_Nombre____b as G1762_C31816, h.LISOPC_Nombre____b as G1762_C31817,G1762_C31968, i.LISOPC_Nombre____b as G1762_C31799, j.LISOPC_Nombre____b as G1762_C34556, k.LISOPC_Nombre____b as G1762_C34557 FROM '.$BaseDatos.'.G1762 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1762_C31723 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1762_C31724 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1762_C31750 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1762_C31766 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1762_C31814 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1762_C31815 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1762_C31816 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1762_C31817 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G1762_C31799 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G1762_C34556 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G1762_C34557 ';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1762_C31726)){
                    $hora_a = explode(' ', $fila->G1762_C31726)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1762_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1762_ConsInte__b , ($fila->G1762_C31723) , ($fila->G1762_C31724) , explode(' ', $fila->G1762_C31725)[0] , $hora_a , ($fila->G1762_C31727) , ($fila->G1762_C31728) , ($fila->G1762_C31729) , ($fila->G1762_C31730) , ($fila->G1762_C31731) , ($fila->G1762_C31966) , ($fila->G1762_C31736) , ($fila->G1762_C31737) , ($fila->G1762_C31738) , ($fila->G1762_C31739) , ($fila->G1762_C31740) , ($fila->G1762_C31741) , ($fila->G1762_C31742) , ($fila->G1762_C31743) , ($fila->G1762_C31744) , ($fila->G1762_C31745) , ($fila->G1762_C31746) , ($fila->G1762_C31747) , explode(' ', $fila->G1762_C31748)[0] , ($fila->G1762_C31750) , ($fila->G1762_C31766) , ($fila->G1762_C31767) , ($fila->G1762_C31814) , ($fila->G1762_C31815) , ($fila->G1762_C31816) , ($fila->G1762_C31817) , ($fila->G1762_C31968) ,($fila->G1762_C34556) ,($fila->G1762_C34557) ,($fila->G1762_C34514) , ($fila->G1762_C31799) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1762 WHERE G1762_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1762_ConsInte__b as id,  G1762_C31738 as camp2 , G1762_C31739 as camp1  FROM '.$BaseDatos.'.G1762 ORDER BY G1762_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1762 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1762(";
            $LsqlV = " VALUES (";
            $Lsql1767= "INSERT INTO ".$BaseDatos.".G1767(";
            $Lsql1767V= " VALUES (";
            $Lsql1771= "INSERT INTO ".$BaseDatos.".G1771(";
            $Lsql1771V= " VALUES (";
 
            $G1762_C31723 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1762_C31723 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1762_C31723 = ".$G1762_C31723;
                    $LsqlI .= $separador." G1762_C31723";
                    $LsqlV .= $separador.$G1762_C31723;
                    $validar = 1;

                    
                }
            }
 
            $G1762_C31724 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1762_C31724 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1762_C31724 = ".$G1762_C31724;
                    $LsqlI .= $separador." G1762_C31724";
                    $LsqlV .= $separador.$G1762_C31724;
                    $validar = 1;
                }
            }
 
            $G1762_C31725 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1762_C31725 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1762_C31725 = ".$G1762_C31725;
                    $LsqlI .= $separador." G1762_C31725";
                    $LsqlV .= $separador.$G1762_C31725;
                    $validar = 1;
                }
            }
 
            $G1762_C31726 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1762_C31726 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1762_C31726 = ".$G1762_C31726;
                    $LsqlI .= $separador." G1762_C31726";
                    $LsqlV .= $separador.$G1762_C31726;
                    $validar = 1;
                }
            }
 
            $G1762_C31727 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1762_C31727 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1762_C31727 = ".$G1762_C31727;
                    $LsqlI .= $separador." G1762_C31727";
                    $LsqlV .= $separador.$G1762_C31727;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1762_C31728"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31728 = '".$_POST["G1762_C31728"]."'";
                $LsqlI .= $separador."G1762_C31728";
                $LsqlV .= $separador."'".$_POST["G1762_C31728"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31729"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31729 = '".$_POST["G1762_C31729"]."'";
                $LsqlI .= $separador."G1762_C31729";
                $LsqlV .= $separador."'".$_POST["G1762_C31729"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31730"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31730 = '".$_POST["G1762_C31730"]."'";
                $LsqlI .= $separador."G1762_C31730";
                $LsqlV .= $separador."'".$_POST["G1762_C31730"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31731"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31731 = '".$_POST["G1762_C31731"]."'";
                $LsqlI .= $separador."G1762_C31731";
                $LsqlV .= $separador."'".$_POST["G1762_C31731"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31735"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31735 = '".$_POST["G1762_C31735"]."'";
                $LsqlI .= $separador."G1762_C31735";
                $LsqlV .= $separador."'".$_POST["G1762_C31735"]."'";
                $validar = 1;
            }
             
  
            $G1762_C31966 = NULL;
            //este es de tipo numero no se deja ir asi '', si esta vacio lo mejor es no mandarlo
            if(isset($_POST["G1762_C31966"])){
                if($_POST["G1762_C31966"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1762_C31966 = $_POST["G1762_C31966"];
                    $LsqlU .= $separador." G1762_C31966 = ".$G1762_C31966."";
                    $LsqlI .= $separador." G1762_C31966";
                    $LsqlV .= $separador.$G1762_C31966;
                    $validar = 1;
                    
                    $Lsql1767 .= " G1767_C31887";
                    $Lsql1767V .= $G1762_C31966;
                    
                    $Lsql1771 .= " G1771_C31975";
                    $Lsql1771V .= $G1762_C31966;                    
                }
            }
  
            if(isset($_POST["G1762_C31736"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31736 = '".$_POST["G1762_C31736"]."'";
                $LsqlI .= $separador."G1762_C31736";
                $LsqlV .= $separador."'".$_POST["G1762_C31736"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31737"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31737 = '".$_POST["G1762_C31737"]."'";
                $LsqlI .= $separador."G1762_C31737";
                $LsqlV .= $separador."'".$_POST["G1762_C31737"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31738"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31738 = '".$_POST["G1762_C31738"]."'";
                $LsqlI .= $separador."G1762_C31738";
                $LsqlV .= $separador."'".$_POST["G1762_C31738"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31739"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31739 = '".$_POST["G1762_C31739"]."'";
                $LsqlI .= $separador."G1762_C31739";
                $LsqlV .= $separador."'".$_POST["G1762_C31739"]."'";
                $validar = 1;
                
                $Lsql1767 .= $separador." G1767_C31893";
                $Lsql1767V .= $separador."'".$_POST["G1762_C31739"]."'";
                
                $Lsql1771 .= $separador." G1771_C31978";
                $Lsql1771V .= $separador."'".$_POST["G1762_C31739"]."'";                
            }
             
  
            if(isset($_POST["G1762_C31740"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31740 = '".$_POST["G1762_C31740"]."'";
                $LsqlI .= $separador."G1762_C31740";
                $LsqlV .= $separador."'".$_POST["G1762_C31740"]."'";
                $validar = 1;
                
                $Lsql1767 .= $separador." G1767_C31896";
                $Lsql1767V .= $separador."'".$_POST["G1762_C31740"]."'";
                
                $Lsql1771 .= $separador." G1771_C31979";
                $Lsql1771V .= $separador."'".$_POST["G1762_C31740"]."'";                
                
                
            }
             
  
            if(isset($_POST["G1762_C31741"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31741 = '".$_POST["G1762_C31741"]."'";
                $LsqlI .= $separador."G1762_C31741";
                $LsqlV .= $separador."'".$_POST["G1762_C31741"]."'";
                $validar = 1;
                
                $Lsql1767 .= $separador." G1767_C31900";
                $Lsql1767V .= $separador."'".$_POST["G1762_C31741"]."'";
                
                $Lsql1771 .= $separador." G1771_C31980";
                $Lsql1771V .= $separador."'".$_POST["G1762_C31741"]."'";                
                
                
            }
             
  
            if(isset($_POST["G1762_C31742"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31742 = '".$_POST["G1762_C31742"]."'";
                $LsqlI .= $separador."G1762_C31742";
                $LsqlV .= $separador."'".$_POST["G1762_C31742"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31743"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31743 = '".$_POST["G1762_C31743"]."'";
                $LsqlI .= $separador."G1762_C31743";
                $LsqlV .= $separador."'".$_POST["G1762_C31743"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31744"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31744 = '".$_POST["G1762_C31744"]."'";
                $LsqlI .= $separador."G1762_C31744";
                $LsqlV .= $separador."'".$_POST["G1762_C31744"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31745"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31745 = '".$_POST["G1762_C31745"]."'";
                $LsqlI .= $separador."G1762_C31745";
                $LsqlV .= $separador."'".$_POST["G1762_C31745"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31746"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31746 = '".$_POST["G1762_C31746"]."'";
                $LsqlI .= $separador."G1762_C31746";
                $LsqlV .= $separador."'".$_POST["G1762_C31746"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31747"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31747 = '".$_POST["G1762_C31747"]."'";
                $LsqlI .= $separador."G1762_C31747";
                $LsqlV .= $separador."'".$_POST["G1762_C31747"]."'";
                $validar = 1;
            }
             
 
            $G1762_C31748 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1762_C31748"])){    
                if($_POST["G1762_C31748"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1762_C31748"]);
                    if(count($tieneHora) > 1){
                    	$G1762_C31748 = "'".$_POST["G1762_C31748"]."'";
                    }else{
                    	$G1762_C31748 = "'".str_replace(' ', '',$_POST["G1762_C31748"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1762_C31748 = ".$G1762_C31748;
                    $LsqlI .= $separador." G1762_C31748";
                    $LsqlV .= $separador.$G1762_C31748;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1762_C31750"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31750 = '".$_POST["G1762_C31750"]."'";
                $LsqlI .= $separador."G1762_C31750";
                $LsqlV .= $separador."'".$_POST["G1762_C31750"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31766"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31766 = '".$_POST["G1762_C31766"]."'";
                $LsqlI .= $separador."G1762_C31766";
                $LsqlV .= $separador."'".$_POST["G1762_C31766"]."'";
                $validar = 1;
                
                if($_POST["G1762_C31766"] !=0){
                    $con="select LISOPC_Nombre____b as nombre from DYALOGOCRM_SISTEMA.LISOPC where LISOPC_ConsInte__b=".$_POST["G1762_C31766"];
                    $cons=$mysqli->query($con);
                    $value=$cons->fetch_object();

                    $Lsql1767 .= $separador." G1767_C31892";
                    $Lsql1767V .= $separador."'".$value->nombre."'";
                    
                    $Lsql1771 .= $separador." G1771_C31982";
                    $Lsql1771V .= $separador."'".$value->nombre."'";                    
                }
            }
            
            if(isset($_POST['orden']) && $_POST['orden'] !=0){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                
                $Lsql1767 .= $separador." G1767_C31903";
                $Lsql1767V .= $separador."'".$_POST["orden"]."'";
                
                $Lsql1771 .= $separador." G1771_C31981";
                $Lsql1771V .= $separador."'".$_POST["orden"]."'";                
            }
             
  
            if(isset($_POST["G1762_C31767"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31767 = '".$_POST["G1762_C31767"]."'";
                $LsqlI .= $separador."G1762_C31767";
                $LsqlV .= $separador."'".$_POST["G1762_C31767"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31814"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31814 = '".$_POST["G1762_C31814"]."'";
                $LsqlI .= $separador."G1762_C31814";
                $LsqlV .= $separador."'".$_POST["G1762_C31814"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31815"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31815 = '".$_POST["G1762_C31815"]."'";
                $LsqlI .= $separador."G1762_C31815";
                $LsqlV .= $separador."'".$_POST["G1762_C31815"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31816"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31816 = '".$_POST["G1762_C31816"]."'";
                $LsqlI .= $separador."G1762_C31816";
                $LsqlV .= $separador."'".$_POST["G1762_C31816"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31817"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31817 = '".$_POST["G1762_C31817"]."'";
                $LsqlI .= $separador."G1762_C31817";
                $LsqlV .= $separador."'".$_POST["G1762_C31817"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31968"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31968 = '".$_POST["G1762_C31968"]."'";
                $LsqlI .= $separador."G1762_C31968";
                $LsqlV .= $separador."'".$_POST["G1762_C31968"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31799"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31799 = '".$_POST["G1762_C31799"]."'";
                $LsqlI .= $separador."G1762_C31799";
                $LsqlV .= $separador."'".$_POST["G1762_C31799"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1762_C31800"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C31800 = '".$_POST["G1762_C31800"]."'";
                $LsqlI .= $separador."G1762_C31800";
                $LsqlV .= $separador."'".$_POST["G1762_C31800"]."'";
                $validar = 1;
            }
            
            if(isset($_POST["G1762_C34514"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C34514 = '".$_POST["G1762_C34514"]."'";
                $LsqlI .= $separador."G1762_C34514";
                $LsqlV .= $separador."'".$_POST["G1762_C34514"]."'";
                $validar = 1;
            }            
            
            if(isset($_POST["G1762_C34556"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C34556 = '".$_POST["G1762_C34556"]."'";
                $LsqlI .= $separador."G1762_C34556";
                $LsqlV .= $separador."'".$_POST["G1762_C34556"]."'";
                $validar = 1;
            }
            
            if(isset($_POST["G1762_C34557"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_C34557 = '".$_POST["G1762_C34557"]."'";
                $LsqlI .= $separador."G1762_C34557";
                $LsqlV .= $separador."'".$_POST["G1762_C34557"]."'";
                $validar = 1;
            }            
            
            $agente=0;
			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1762_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1762_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
                $agente=1;
			}


			$padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1762_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
                $Lsqls = $Lsql1767.",G1767_C31890,G1767_C31889,G1767_FechaInsercion,G1767_Usuario )" . $Lsql1767V.",date_format(curdate(), '%y-%m-%d 00:00:00'),now(), now(),".$_GET['usuario'].")";
            
                $Lsqls1771 = $Lsql1771.",G1771_C31976,G1771_C31977,G1771_FechaInsercion,G1771_Usuario )" . $Lsql1771V.",date_format(curdate(), '%y-%m-%d 00:00:00'),now(), now(),".$_GET['usuario'].")";             
            
			if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1762_Usuario , G1762_FechaInsercion, G1762_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1762_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1762 WHERE G1762_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            // echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                    	$UltimoID = $mysqli->insert_id;
                    	echo $mysqli->insert_id;
                	}else{
                		if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                		echo "1";    		
                	}
                    
                    if($agente ==1){
                        $barrio=$mysqli->query($Lsqls);
                        $id1= $mysqli->insert_id;
                        $llamadas=$mysqli->query($Lsqls1771);
                        $id2= $mysqli->insert_id;
                        $upG1767="update ".$BaseDatos.".G1767 set G1767_C36179=".$UltimoID." where G1767_ConsInte__b=".$id1;
//                        echo $upG1767;
                        $upG1767=$mysqli->query($upG1767);
                        $upG1771="update ".$BaseDatos.".G1771 set G1771_C36182=".$UltimoID." where G1771_ConsInte__b=".$id2;
                        $upG1771=$mysqli->query($upG1771);
//                        echo $upG1771;  
                    }    

                } else {
                	echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }       
    }
    
    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1767_ConsInte__b, G1767_C31887, G1767_C31890,G1767_C36179, G1767_C31889, G1767_C31893, G1767_C31896, G1767_C31900, G1767_C31903, G1767_C31892, G1767_C31883, G1767_C31884, k.LISOPC_Nombre____b as  G1767_C31885 FROM ".$BaseDatos.".G1767  LEFT JOIN ".$BaseDatos_systema.".LISOPC as k ON k.LISOPC_ConsInte__b =  G1767_C31885 ";

        $SQL .= " WHERE G1767_C31900 = '".$numero."' AND G1767_FechaInsercion >= DATE_SUB(curdate(), INTERVAL 10 DAY)"; 

        $SQL .= " ORDER BY G1767_C31887 DESC";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1767_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1767_ConsInte__b)."</cell>"; 
            

            echo "<cell>". $fila->G1767_C31887."</cell>"; 

            if($fila->G1767_C31890 != ''){
                echo "<cell>". explode(' ', $fila->G1767_C31890)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1767_C31889 != ''){
                echo "<cell>". explode(' ', $fila->G1767_C31889)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1767_C31893)."</cell>";

            echo "<cell>". ($fila->G1767_C31896)."</cell>";

            echo "<cell>". ($fila->G1767_C31900)."</cell>";

            echo "<cell>". ($fila->G1767_C31903)."</cell>";

            echo "<cell>". ($fila->G1767_C31892)."</cell>";

            echo "<cell>". ($fila->G1767_C31883)."</cell>";

            echo "<cell>". ($fila->G1767_C31884)."</cell>";

            echo "<cell>". ($fila->G1767_C31885)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_1"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1763_ConsInte__b, G1763_C31824, G1763_C31825, G1763_C31831, G1763_C31832, e.LISOPC_Nombre____b as  G1763_C31835, G1763_C31827, g.LISOPC_Nombre____b as  G1763_C31858, G1763_C31828, G1763_C31826, G1763_C31833, G1763_C31830, G1763_C31829, G1763_C31834, G1763_C31859, G1763_C31860, G1763_C31861, G1763_C31862, G1763_C31863, G1763_C31864, t.LISOPC_Nombre____b as  G1763_C31970, G1763_C31971, G1763_C31818, G1763_C31819, x.LISOPC_Nombre____b as  G1763_C31820 FROM ".$BaseDatos.".G1763  LEFT JOIN ".$BaseDatos_systema.".LISOPC as e ON e.LISOPC_ConsInte__b =  G1763_C31835 LEFT JOIN ".$BaseDatos_systema.".LISOPC as g ON g.LISOPC_ConsInte__b =  G1763_C31858 LEFT JOIN ".$BaseDatos_systema.".LISOPC as t ON t.LISOPC_ConsInte__b =  G1763_C31970 LEFT JOIN ".$BaseDatos_systema.".LISOPC as x ON x.LISOPC_ConsInte__b =  G1763_C31820 ";

        $SQL .= " WHERE G1763_C31827 = '".$numero."'"; 

        $SQL .= " ORDER BY G1763_C31824";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1763_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1763_ConsInte__b)."</cell>"; 
            

            echo "<cell>". $fila->G1763_C31824."</cell>"; 

            echo "<cell>". $fila->G1763_C31825."</cell>";
            
            if($fila->G1763_C31826 != ''){
                echo "<cell>". explode(' ', $fila->G1763_C31826)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }
            
            if($fila->G1763_C31829 != ''){
                echo "<cell>". explode(' ', $fila->G1763_C31829)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }
            
            echo "<cell>". ($fila->G1763_C31835)."</cell>";
            
            echo "<cell>". ($fila->G1763_C31858)."</cell>";
            
            echo "<cell>". ($fila->G1763_C31827)."</cell>";
            
            echo "<cell>". ($fila->G1763_C31970)."</cell>";

            if($fila->G1763_C31971 != ''){
                echo "<cell>". explode(' ', $fila->G1763_C31971)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }            

            echo "<cell>". ($fila->G1763_C31831)."</cell>";

            echo "<cell>". ($fila->G1763_C31832)."</cell>";

            echo "<cell>". ($fila->G1763_C31828)."</cell>";

            echo "<cell>". ($fila->G1763_C31833)."</cell>";

            echo "<cell>". ($fila->G1763_C31830)."</cell>";

            echo "<cell>". ($fila->G1763_C31834)."</cell>";

            echo "<cell>". ($fila->G1763_C31859)."</cell>";

            if($fila->G1763_C31860 != ''){
                echo "<cell>". explode(' ', $fila->G1763_C31860)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1763_C31861)."</cell>";

            echo "<cell>". ($fila->G1763_C31862)."</cell>";

            if($fila->G1763_C31863 != ''){
                echo "<cell>". explode(' ', $fila->G1763_C31863)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell><![CDATA[". ($fila->G1763_C31864)."]]></cell>";

            echo "<cell>". ($fila->G1763_C31818)."</cell>";

            echo "<cell>". ($fila->G1763_C31819)."</cell>";

            echo "<cell>". ($fila->G1763_C31820)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_2"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1772_ConsInte__b, G1772_C31988, G1772_C31989, G1772_C31990, G1772_C31991, G1772_C31992, G1772_C31993, G1772_C31994, G1772_C31985, G1772_C31986, k.LISOPC_Nombre____b as  G1772_C31987 FROM ".$BaseDatos.".G1772  LEFT JOIN ".$BaseDatos_systema.".LISOPC as k ON k.LISOPC_ConsInte__b =  G1772_C31987 ";

        $SQL .= " WHERE G1772_C31993 = '".$numero."'"; 

        $SQL .= " ORDER BY G1772_C31988";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1772_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1772_ConsInte__b)."</cell>"; 
            

            echo "<cell>". ($fila->G1772_C31988)."</cell>";

            echo "<cell>". ($fila->G1772_C31989)."</cell>";
            
            if($fila->G1772_C31990 != ''){
                echo "<cell>". explode(' ', $fila->G1772_C31990)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }            

            echo "<cell><![CDATA[". ($fila->G1772_C31991)."]]></cell>";

            echo "<cell>". ($fila->G1772_C31992)."</cell>";

            echo "<cell>". ($fila->G1772_C31993)."</cell>";

            if($fila->G1772_C31994 != ''){
                echo "<cell>". explode(' ', $fila->G1772_C31994)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1772_C31985)."</cell>";

            echo "<cell>". ($fila->G1772_C31986)."</cell>";

            echo "<cell>". ($fila->G1772_C31987)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_3"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1771_ConsInte__b, G1771_C31975, G1771_C31976, G1771_C31977, G1771_C31978, G1771_C31979, G1771_C31980, G1771_C31981, G1771_C31982, G1771_C31972, G1771_C31973, k.LISOPC_Nombre____b as  G1771_C31974 FROM ".$BaseDatos.".G1771  LEFT JOIN ".$BaseDatos_systema.".LISOPC as k ON k.LISOPC_ConsInte__b =  G1771_C31974 ";

        $SQL .= " WHERE G1771_C31978 = '".$numero."'"; 

        $SQL .= " ORDER BY G1771_C31975";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1771_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1771_ConsInte__b)."</cell>"; 
            

            echo "<cell>". ($fila->G1771_C31975)."</cell>";

            if($fila->G1771_C31976 != ''){
                echo "<cell>". explode(' ', $fila->G1771_C31976)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1771_C31977 != ''){
                echo "<cell>". explode(' ', $fila->G1771_C31977)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1771_C31978)."</cell>";

            echo "<cell>". ($fila->G1771_C31979)."</cell>";

            echo "<cell>". ($fila->G1771_C31980)."</cell>";

            echo "<cell>". ($fila->G1771_C31981)."</cell>";

            echo "<cell>". ($fila->G1771_C31982)."</cell>";

            echo "<cell>". ($fila->G1771_C31972)."</cell>";

            echo "<cell>". ($fila->G1771_C31973)."</cell>";

            echo "<cell>". ($fila->G1771_C31974)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1767 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1767(";
            $LsqlV = " VALUES ("; 
 
            $G1767_C31887= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1767_C31887"])){    
                if($_POST["G1767_C31887"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1767_C31887 = $_POST["G1767_C31887"];
                    $LsqlU .= $separador." G1767_C31887 = '".$G1767_C31887."'";
                    $LsqlI .= $separador." G1767_C31887";
                    $LsqlV .= $separador."'".$G1767_C31887."'";
                    $validar = 1;
                }
            }

            $G1767_C31890 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1767_C31890"])){    
                if($_POST["G1767_C31890"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1767_C31890 = "'".str_replace(' ', '',$_POST["G1767_C31890"])." 00:00:00'";
                    $LsqlU .= $separador." G1767_C31890 = ".$G1767_C31890;
                    $LsqlI .= $separador." G1767_C31890";
                    $LsqlV .= $separador.$G1767_C31890;
                    $validar = 1;
                }
            }
 
            $G1767_C31889 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1767_C31889"])){    
                if($_POST["G1767_C31889"] != '' && $_POST["G1767_C31889"] != 'undefined' && $_POST["G1767_C31889"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1767_C31889 = "'".$fecha." ".str_replace(' ', '',$_POST["G1767_C31889"])."'";
                    $LsqlU .= $separador."  G1767_C31889 = ".$G1767_C31889."";
                    $LsqlI .= $separador."  G1767_C31889";
                    $LsqlV .= $separador.$G1767_C31889;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1767_C31893"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1767_C31893 = '".$_POST["G1767_C31893"]."'";
                $LsqlI .= $separador."G1767_C31893";
                $LsqlV .= $separador."'".$_POST["G1767_C31893"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1767_C31896"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1767_C31896 = '".$_POST["G1767_C31896"]."'";
                $LsqlI .= $separador."G1767_C31896";
                $LsqlV .= $separador."'".$_POST["G1767_C31896"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1767_C31903"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1767_C31903 = '".$_POST["G1767_C31903"]."'";
                $LsqlI .= $separador."G1767_C31903";
                $LsqlV .= $separador."'".$_POST["G1767_C31903"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1767_C31892"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1767_C31892 = '".$_POST["G1767_C31892"]."'";
                $LsqlI .= $separador."G1767_C31892";
                $LsqlV .= $separador."'".$_POST["G1767_C31892"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1767_C31883"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1767_C31883 = '".$_POST["G1767_C31883"]."'";
                $LsqlI .= $separador."G1767_C31883";
                $LsqlV .= $separador."'".$_POST["G1767_C31883"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1767_C31884"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1767_C31884 = '".$_POST["G1767_C31884"]."'";
                $LsqlI .= $separador."G1767_C31884";
                $LsqlV .= $separador."'".$_POST["G1767_C31884"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1767_C31885"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1767_C31885 = '".$_POST["G1767_C31885"]."'";
                $LsqlI .= $separador."G1767_C31885";
                $LsqlV .= $separador."'".$_POST["G1767_C31885"]."'";
                $validar = 1;
            }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1767_C31900 = $numero;
                    $LsqlU .= ", G1767_C31900 = ".$G1767_C31900."";
                    $LsqlI .= ", G1767_C31900";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1767_Usuario ,  G1767_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1767_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1767 WHERE  G1767_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_1"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1763 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1763(";
            $LsqlV = " VALUES ("; 
 
            $G1763_C31824= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1763_C31824"])){    
                if($_POST["G1763_C31824"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1763_C31824 = $_POST["G1763_C31824"];
                    $LsqlU .= $separador." G1763_C31824 = '".$G1763_C31824."'";
                    $LsqlI .= $separador." G1763_C31824";
                    $LsqlV .= $separador."'".$G1763_C31824."'";
                    $validar = 1;
                }
            }
 
            $G1763_C31825= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1763_C31825"])){    
                if($_POST["G1763_C31825"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1763_C31825 = $_POST["G1763_C31825"];
                    $LsqlU .= $separador." G1763_C31825 = '".$G1763_C31825."'";
                    $LsqlI .= $separador." G1763_C31825";
                    $LsqlV .= $separador."'".$G1763_C31825."'";
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1763_C31831"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31831 = '".$_POST["G1763_C31831"]."'";
                $LsqlI .= $separador."G1763_C31831";
                $LsqlV .= $separador."'".$_POST["G1763_C31831"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1763_C31832"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31832 = '".$_POST["G1763_C31832"]."'";
                $LsqlI .= $separador."G1763_C31832";
                $LsqlV .= $separador."'".$_POST["G1763_C31832"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1763_C31835"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31835 = '".$_POST["G1763_C31835"]."'";
                $LsqlI .= $separador."G1763_C31835";
                $LsqlV .= $separador."'".$_POST["G1763_C31835"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1763_C31858"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31858 = '".$_POST["G1763_C31858"]."'";
                $LsqlI .= $separador."G1763_C31858";
                $LsqlV .= $separador."'".$_POST["G1763_C31858"]."'";
                $validar = 1;
            }
 
                                                                         
            if(isset($_POST["G1763_C31828"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31828 = '".$_POST["G1763_C31828"]."'";
                $LsqlI .= $separador."G1763_C31828";
                $LsqlV .= $separador."'".$_POST["G1763_C31828"]."'";
                $validar = 1;
            }
                                                                          
                                                                           

            $G1763_C31826 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1763_C31826"])){    
                if($_POST["G1763_C31826"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1763_C31826 = "'".str_replace(' ', '',$_POST["G1763_C31826"])." 00:00:00'";
                    $LsqlU .= $separador." G1763_C31826 = ".$G1763_C31826;
                    $LsqlI .= $separador." G1763_C31826";
                    $LsqlV .= $separador.$G1763_C31826;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1763_C31833"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31833 = '".$_POST["G1763_C31833"]."'";
                $LsqlI .= $separador."G1763_C31833";
                $LsqlV .= $separador."'".$_POST["G1763_C31833"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1763_C31830"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31830 = '".$_POST["G1763_C31830"]."'";
                $LsqlI .= $separador."G1763_C31830";
                $LsqlV .= $separador."'".$_POST["G1763_C31830"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1763_C31829 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1763_C31829"])){    
                if($_POST["G1763_C31829"] != '' && $_POST["G1763_C31829"] != 'undefined' && $_POST["G1763_C31829"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1763_C31829 = "'".$fecha." ".str_replace(' ', '',$_POST["G1763_C31829"])."'";
                    $LsqlU .= $separador."  G1763_C31829 = ".$G1763_C31829."";
                    $LsqlI .= $separador."  G1763_C31829";
                    $LsqlV .= $separador.$G1763_C31829;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1763_C31834"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31834 = '".$_POST["G1763_C31834"]."'";
                $LsqlI .= $separador."G1763_C31834";
                $LsqlV .= $separador."'".$_POST["G1763_C31834"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1763_C31859"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31859 = '".$_POST["G1763_C31859"]."'";
                $LsqlI .= $separador."G1763_C31859";
                $LsqlV .= $separador."'".$_POST["G1763_C31859"]."'";
                $validar = 1;
            }
                                                                          
                                                                           

            $G1763_C31860 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1763_C31860"])){    
                if($_POST["G1763_C31860"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1763_C31860 = "'".str_replace(' ', '',$_POST["G1763_C31860"])." 00:00:00'";
                    $LsqlU .= $separador." G1763_C31860 = ".$G1763_C31860;
                    $LsqlI .= $separador." G1763_C31860";
                    $LsqlV .= $separador.$G1763_C31860;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1763_C31861"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31861 = '".$_POST["G1763_C31861"]."'";
                $LsqlI .= $separador."G1763_C31861";
                $LsqlV .= $separador."'".$_POST["G1763_C31861"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1763_C31862"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31862 = '".$_POST["G1763_C31862"]."'";
                $LsqlI .= $separador."G1763_C31862";
                $LsqlV .= $separador."'".$_POST["G1763_C31862"]."'";
                $validar = 1;
            }
                                                                          
                                                                           

            $G1763_C31863 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1763_C31863"])){    
                if($_POST["G1763_C31863"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1763_C31863 = "'".str_replace(' ', '',$_POST["G1763_C31863"])." 00:00:00'";
                    $LsqlU .= $separador." G1763_C31863 = ".$G1763_C31863;
                    $LsqlI .= $separador." G1763_C31863";
                    $LsqlV .= $separador.$G1763_C31863;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G1763_C31864"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31864 = '".$_POST["G1763_C31864"]."'";
                $LsqlI .= $separador."G1763_C31864";
                $LsqlV .= $separador."'".$_POST["G1763_C31864"]."'";
                $validar = 1;
            }
                                                                           
 
            if(isset($_POST["G1763_C31970"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31970 = '".$_POST["G1763_C31970"]."'";
                $LsqlI .= $separador."G1763_C31970";
                $LsqlV .= $separador."'".$_POST["G1763_C31970"]."'";
                $validar = 1;
            }

            $G1763_C31971 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1763_C31971"])){    
                if($_POST["G1763_C31971"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1763_C31971 = "'".str_replace(' ', '',$_POST["G1763_C31971"])." 00:00:00'";
                    $LsqlU .= $separador." G1763_C31971 = ".$G1763_C31971;
                    $LsqlI .= $separador." G1763_C31971";
                    $LsqlV .= $separador.$G1763_C31971;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1763_C31818"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31818 = '".$_POST["G1763_C31818"]."'";
                $LsqlI .= $separador."G1763_C31818";
                $LsqlV .= $separador."'".$_POST["G1763_C31818"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1763_C31819"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31819 = '".$_POST["G1763_C31819"]."'";
                $LsqlI .= $separador."G1763_C31819";
                $LsqlV .= $separador."'".$_POST["G1763_C31819"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1763_C31820"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1763_C31820 = '".$_POST["G1763_C31820"]."'";
                $LsqlI .= $separador."G1763_C31820";
                $LsqlV .= $separador."'".$_POST["G1763_C31820"]."'";
                $validar = 1;
            }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1763_C31827 = $numero;
                    $LsqlU .= ", G1763_C31827 = ".$G1763_C31827."";
                    $LsqlI .= ", G1763_C31827";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1763_Usuario ,  G1763_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1763_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1763 WHERE  G1763_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_2"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1772 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1772(";
            $LsqlV = " VALUES ("; 
 
                                                                         
            if(isset($_POST["G1772_C31988"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1772_C31988 = '".$_POST["G1772_C31988"]."'";
                $LsqlI .= $separador."G1772_C31988";
                $LsqlV .= $separador."'".$_POST["G1772_C31988"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1772_C31989"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1772_C31989 = '".$_POST["G1772_C31989"]."'";
                $LsqlI .= $separador."G1772_C31989";
                $LsqlV .= $separador."'".$_POST["G1772_C31989"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1772_C31990"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1772_C31990 = '".$_POST["G1772_C31990"]."'";
                $LsqlI .= $separador."G1772_C31990";
                $LsqlV .= $separador."'".$_POST["G1772_C31990"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
  

            if(isset($_POST["G1772_C31991"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1772_C31991 = '".$_POST["G1772_C31991"]."'";
                $LsqlI .= $separador."G1772_C31991";
                $LsqlV .= $separador."'".$_POST["G1772_C31991"]."'";
                $validar = 1;
            }
                                                                           
 
                                                                         
            if(isset($_POST["G1772_C31992"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1772_C31992 = '".$_POST["G1772_C31992"]."'";
                $LsqlI .= $separador."G1772_C31992";
                $LsqlV .= $separador."'".$_POST["G1772_C31992"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1772_C31993"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1772_C31993 = '".$_POST["G1772_C31993"]."'";
                $LsqlI .= $separador."G1772_C31993";
                $LsqlV .= $separador."'".$_POST["G1772_C31993"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            $G1772_C31994 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1772_C31994"])){    
                if($_POST["G1772_C31994"] != '' && $_POST["G1772_C31994"] != 'undefined' && $_POST["G1772_C31994"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1772_C31994 = "'".$fecha." ".str_replace(' ', '',$_POST["G1772_C31994"])."'";
                    $LsqlU .= $separador."  G1772_C31994 = ".$G1772_C31994."";
                    $LsqlI .= $separador."  G1772_C31994";
                    $LsqlV .= $separador.$G1772_C31994;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1772_C31985"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1772_C31985 = '".$_POST["G1772_C31985"]."'";
                $LsqlI .= $separador."G1772_C31985";
                $LsqlV .= $separador."'".$_POST["G1772_C31985"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1772_C31986"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1772_C31986 = '".$_POST["G1772_C31986"]."'";
                $LsqlI .= $separador."G1772_C31986";
                $LsqlV .= $separador."'".$_POST["G1772_C31986"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1772_C31987"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1772_C31987 = '".$_POST["G1772_C31987"]."'";
                $LsqlI .= $separador."G1772_C31987";
                $LsqlV .= $separador."'".$_POST["G1772_C31987"]."'";
                $validar = 1;
            }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1772_C31995 = $numero;
                    $LsqlU .= ", G1772_C31995 = ".$G1772_C31995."";
                    $LsqlI .= ", G1772_C31995";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1772_Usuario ,  G1772_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1772_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1772 WHERE  G1772_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_3"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1771 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1771(";
            $LsqlV = " VALUES ("; 
 
                                                                         
            if(isset($_POST["G1771_C31975"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1771_C31975 = '".$_POST["G1771_C31975"]."'";
                $LsqlI .= $separador."G1771_C31975";
                $LsqlV .= $separador."'".$_POST["G1771_C31975"]."'";
                $validar = 1;
            }
                                                                          
                                                                           

            $G1771_C31976 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1771_C31976"])){    
                if($_POST["G1771_C31976"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1771_C31976 = "'".str_replace(' ', '',$_POST["G1771_C31976"])." 00:00:00'";
                    $LsqlU .= $separador." G1771_C31976 = ".$G1771_C31976;
                    $LsqlI .= $separador." G1771_C31976";
                    $LsqlV .= $separador.$G1771_C31976;
                    $validar = 1;
                }
            }
 
            $G1771_C31977 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1771_C31977"])){    
                if($_POST["G1771_C31977"] != '' && $_POST["G1771_C31977"] != 'undefined' && $_POST["G1771_C31977"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1771_C31977 = "'".$fecha." ".str_replace(' ', '',$_POST["G1771_C31977"])."'";
                    $LsqlU .= $separador."  G1771_C31977 = ".$G1771_C31977."";
                    $LsqlI .= $separador."  G1771_C31977";
                    $LsqlV .= $separador.$G1771_C31977;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1771_C31979"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1771_C31979 = '".$_POST["G1771_C31979"]."'";
                $LsqlI .= $separador."G1771_C31979";
                $LsqlV .= $separador."'".$_POST["G1771_C31979"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1771_C31980"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1771_C31980 = '".$_POST["G1771_C31980"]."'";
                $LsqlI .= $separador."G1771_C31980";
                $LsqlV .= $separador."'".$_POST["G1771_C31980"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1771_C31981"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1771_C31981 = '".$_POST["G1771_C31981"]."'";
                $LsqlI .= $separador."G1771_C31981";
                $LsqlV .= $separador."'".$_POST["G1771_C31981"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1771_C31982"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1771_C31982 = '".$_POST["G1771_C31982"]."'";
                $LsqlI .= $separador."G1771_C31982";
                $LsqlV .= $separador."'".$_POST["G1771_C31982"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1771_C31972"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1771_C31972 = '".$_POST["G1771_C31972"]."'";
                $LsqlI .= $separador."G1771_C31972";
                $LsqlV .= $separador."'".$_POST["G1771_C31972"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
                                                                         
            if(isset($_POST["G1771_C31973"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1771_C31973 = '".$_POST["G1771_C31973"]."'";
                $LsqlI .= $separador."G1771_C31973";
                $LsqlV .= $separador."'".$_POST["G1771_C31973"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
 
            if(isset($_POST["G1771_C31974"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1771_C31974 = '".$_POST["G1771_C31974"]."'";
                $LsqlI .= $separador."G1771_C31974";
                $LsqlV .= $separador."'".$_POST["G1771_C31974"]."'";
                $validar = 1;
            }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1771_C31978 = $numero;
                    $LsqlU .= ", G1771_C31978 = ".$G1771_C31978."";
                    $LsqlI .= ", G1771_C31978";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1771_Usuario ,  G1771_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1771_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1771 WHERE  G1771_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
