<?php
	ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
	include(__DIR__."/../../conexion.php");
	date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    	if($_GET['action'] == "GET_DATOS"){

    		$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_CamAd1Cbx_b , CAMPAN_CamAd2Cbx_b , CAMPAN_CamAd3Cbx_b, CAMPAN_CamAd4Cbx_b , CAMPAN_CamAd5Cbx_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
	        $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	        $datoCampan = $res_Lsql_Campan->fetch_array();
	        $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	        $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	        $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	        $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];


	        $CAMPAN_CamAd1Cbx_b = null;
			$CAMPAN_CamAd2Cbx_b = null;
			$CAMPAN_CamAd3Cbx_b = null;
			$CAMPAN_CamAd4Cbx_b = null;
			$CAMPAN_CamAd5Cbx_b = null;

	        $CAMPAN_CamAd1Cbx_b = $datoCampan['CAMPAN_CamAd1Cbx_b'];
			$CAMPAN_CamAd2Cbx_b = $datoCampan['CAMPAN_CamAd2Cbx_b'];
			$CAMPAN_CamAd3Cbx_b = $datoCampan['CAMPAN_CamAd3Cbx_b'];
			$CAMPAN_CamAd4Cbx_b = $datoCampan['CAMPAN_CamAd4Cbx_b'];
			$CAMPAN_CamAd5Cbx_b = $datoCampan['CAMPAN_CamAd5Cbx_b'];


			$Lsql = " SELECT * FROM ".$BaseDatos.".".$str_Pobla_Campan;
			$Where = " WHERE ";
			
			if(!isset($_POST['consinte'])){
				$usados = 0;
				if(isset($_POST['dato_adicional_1'])){
					if($CAMPAN_CamAd1Cbx_b != '' && !is_null($CAMPAN_CamAd1Cbx_b)){
						/* si tenia definido los campos de busqueda  */
						$and = '';
						if($usados != 0){
							$and = ' OR ';
						}

						$Where .= $and." ".$str_Pobla_Campan.'_C'.$CAMPAN_CamAd1Cbx_b." = '". $_POST['dato_adicional_1']."'";
						$usados = 1;
					}
				}

				if(isset($_POST['dato_adicional_2'])){
					if($CAMPAN_CamAd2Cbx_b != '' && !is_null($CAMPAN_CamAd2Cbx_b)){
						/* si tenia definido los campos de busqueda  */
						$and = '';
						if($usados != 0){
							$and = ' OR ';
						}

						$Where .= $and." ".$str_Pobla_Campan.'_C'.$CAMPAN_CamAd2Cbx_b." = '". $_POST['dato_adicional_2']."'";
						$usados = 1;
					}
				}

				if(isset($_POST['dato_adicional_3'])){
					if($CAMPAN_CamAd3Cbx_b != '' && !is_null($CAMPAN_CamAd3Cbx_b)){
						/* si tenia definido los campos de busqueda  */
						$and = '';
						if($usados != 0){
							$and = ' OR ';
						}

						$Where .= $and." ".$str_Pobla_Campan.'_C'.$CAMPAN_CamAd3Cbx_b." = '". $_POST['dato_adicional_3']."'";
						$usados = 1;
					}
				}

				if(isset($_POST['dato_adicional_4'])){
					if($CAMPAN_CamAd4Cbx_b != '' && !is_null($CAMPAN_CamAd4Cbx_b)){
						/* si tenia definido los campos de busqueda  */
						$and = '';
						if($usados != 0){
							$and = ' OR ';
						}

						$Where .= $and." ".$str_Pobla_Campan.'_C'.$CAMPAN_CamAd4Cbx_b." = '". $_POST['dato_adicional_4']."'";
						$usados = 1;
					}
				}

				if(isset($_POST['dato_adicional_5'])){
					if($CAMPAN_CamAd5Cbx_b != '' && !is_null($CAMPAN_CamAd5Cbx_b)){
						/* si tenia definido los campos de busqueda  */
						$and = '';
						if($usados != 0){
							$and = ' OR ';
						}

						$Where .= $and." ".$str_Pobla_Campan.'_C'.$CAMPAN_CamAd5Cbx_b." = '". $_POST['dato_adicional_5']."'";
						$usados = 1;
					}
				}
				if($usados == 0){
					$Where = " WHERE 1 ";
				}

			}else{
				if(!is_null($_POST['consinte']) && $_POST['consinte'] != ''){
					$Where .= $str_Pobla_Campan."_ConsInte__b = ". $_POST['consinte'];
				}
			}
			


			$Lsql .= $Where;
			//echo $Lsql;
			$resultado = $mysqli->query($Lsql);
			$arrayDatos = array();
			while ($key = $resultado->fetch_assoc()) {
				$arrayDatos[] = $key;
			}

			$newJson = array();
			$newJson[0]['cantidad_registros'] = $resultado->num_rows;
			$newJson[0]['registros'] = $arrayDatos;

			echo json_encode($newJson);
		}

		if($_GET['action'] == 'ADD'){
			/* toca insertar un registro vacio y editarlo desde el script */
			
			$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_CamAd1Cbx_b , CAMPAN_CamAd2Cbx_b , CAMPAN_CamAd3Cbx_b, CAMPAN_CamAd4Cbx_b , CAMPAN_CamAd5Cbx_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];

	        $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	        $datoCampan = $res_Lsql_Campan->fetch_array();

	        $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	        $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	        $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	        $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];


	        $CAMPAN_CamAd1Cbx_b = null;
			$CAMPAN_CamAd2Cbx_b = null;
			$CAMPAN_CamAd3Cbx_b = null;
			$CAMPAN_CamAd4Cbx_b = null;
			$CAMPAN_CamAd5Cbx_b = null;

	        $CAMPAN_CamAd1Cbx_b = $datoCampan['CAMPAN_CamAd1Cbx_b'];
			$CAMPAN_CamAd2Cbx_b = $datoCampan['CAMPAN_CamAd2Cbx_b'];
			$CAMPAN_CamAd3Cbx_b = $datoCampan['CAMPAN_CamAd3Cbx_b'];
			$CAMPAN_CamAd4Cbx_b = $datoCampan['CAMPAN_CamAd4Cbx_b'];
			$CAMPAN_CamAd5Cbx_b = $datoCampan['CAMPAN_CamAd5Cbx_b'];



			$Lsql = "INSERT INTO ".$BaseDatos.".".$str_Pobla_Campan." (".$str_Pobla_Campan."_FechaInsercion";
			$Vsql = "VALUES ('".date('Y-m-d H:i:s')."'";

			$usados = 0;
			if(isset($_POST['dato_adicional_1'])){
				if($CAMPAN_CamAd1Cbx_b != '' && !is_null($CAMPAN_CamAd1Cbx_b)){
					/* si tenia definido los campos de busqueda  */
					$and = '';
					if($usados != 0){
						$and = ' , ';
					}

					$Lsql .= " , ".$str_Pobla_Campan.'_C'.$CAMPAN_CamAd1Cbx_b;
					$Vsql .= " , '".$_POST['dato_adicional_1']."'";
					$usados = 1;
				}
			}

			if(isset($_POST['dato_adicional_2'])){
				if($CAMPAN_CamAd2Cbx_b != '' && !is_null($CAMPAN_CamAd2Cbx_b)){
					/* si tenia definido los campos de busqueda  */
					$and = '';
					if($usados != 0){
						$and = ' , ';
					}

					$Lsql .= " , ".$str_Pobla_Campan.'_C'.$CAMPAN_CamAd2Cbx_b;
					$Vsql .= " , '".$_POST['dato_adicional_2']."'";
					$usados = 1;
				}
			}

			if(isset($_POST['dato_adicional_3'])){
				if($CAMPAN_CamAd3Cbx_b != '' && !is_null($CAMPAN_CamAd3Cbx_b)){
					/* si tenia definido los campos de busqueda  */
					$and = '';
					if($usados != 0){
						$and = ' , ';
					}

					$Lsql .= " , ".$str_Pobla_Campan.'_C'.$CAMPAN_CamAd3Cbx_b;
					$Vsql .= " , '".$_POST['dato_adicional_3']."'";
					$usados = 1;
				}
			}

			if(isset($_POST['dato_adicional_4'])){
				if($CAMPAN_CamAd4Cbx_b != '' && !is_null($CAMPAN_CamAd4Cbx_b)){
					/* si tenia definido los campos de busqueda  */
					$and = '';
					if($usados != 0){
						$and = ' , ';
					}

					$Lsql .= " , ".$str_Pobla_Campan.'_C'.$CAMPAN_CamAd4Cbx_b;
					$Vsql .= " , '".$_POST['dato_adicional_4']."'";
					$usados = 1;
				}
			}

			if(isset($_POST['dato_adicional_5'])){
				if($CAMPAN_CamAd5Cbx_b != '' && !is_null($CAMPAN_CamAd5Cbx_b)){
					/* si tenia definido los campos de busqueda  */
					$and = '';
					if($usados != 0){
						$and = ' , ';
					}

					$Lsql .= " , ".$str_Pobla_Campan.'_C'.$CAMPAN_CamAd5Cbx_b;
					$Vsql .= " , '".$_POST['dato_adicional_5']."'";
					$usados = 1;
				}
			}

			$insertarLSql = $Lsql.") ".$Vsql.");";
			//echo $insertarLSql;
			if ($mysqli->query($insertarLSql) === TRUE) {
                $resultado = $mysqli->insert_id;
                $InsertMuestra = "INSERT INTO ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." (".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b , ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b) VALUES ($resultado, 0);";
                $mysqli->query($InsertMuestra);
                echo $resultado;
            }else{
            	echo "Error insertando el registro => ".$mysqli->error;
            }

		}	

	}
?>