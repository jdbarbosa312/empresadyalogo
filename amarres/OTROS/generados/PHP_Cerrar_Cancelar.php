<?php
	ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
	include(__DIR__."/../../conexion.php");
	include(__DIR__."/../../funciones.php");
	date_default_timezone_set('America/Bogota');

		if(isset($_GET['campana_crm'])  && isset($_GET['idMonoef']) ){
		crearMiembroDefault($_GET["campana_crm"]);
		/* primero buscamos la campaña que nos esta llegando */
		$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];

		//echo $Lsql_Campan;

        $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
        $datoCampan = $res_Lsql_Campan->fetch_array();
        $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
        $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
        $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
        $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
        $muestra="G".$int_Pobla_Camp_2."_M".$int_Muest_Campan;
        $fechaInsercion = date('Y-m-d H:i:s');
        $nombreUsuario = "NULL";
        $idUsuario = "NULL";
        $codigoMiembro = -1;
        $idLlamada = "NULL";
        $sentido = "NULL";
        $canal = "bq_manual";
        $linkContenido = "";
        $detalleCanal= "busqueda_manual";
        $datoContacto = "Numero no se identifica";
        $paso = "NULL";        
        $duracion = "NULL";
        $clasificacion = 'NULL';
        $tipificacion = $_GET['idMonoef'];
        $reintento = 'NULL';
        $fecha = date('Y-m-d');
        $hora = date('H:i:s');
        $comentario = "Registro cerrado desde la busqueda manual";
        
        if(isset($_GET['token'])){
        	$nombreUsuario = getNombreUser($_GET['token']);
        	$idUsuario  = getIdToken($_GET['token']);
        }

         if(isset($_GET['sentido']) & $_GET['sentido'] != 0){
    		if($_GET['sentido'] == '1'){
    			$sentido = 'Saliente';
    		}else{
    			$sentido = 'Entrante';
    		}
    	}else{
    		$sentido = "Entrante";
    	}

		$valorId_Gestion_Cbx_2 = '';
		if(isset($_GET['id_gestion_cbx']) && $_GET['id_gestion_cbx'] != '' && $_GET['id_gestion_cbx'] != null){

			$idLlamada = $_GET['id_gestion_cbx'];
		    $valorId_Gestion_Cbx_2 = $_GET['id_gestion_cbx'];
		    
		    /* Toca averiguar lo del Coninte de la gestio */
		    $valorSentido = explode('_', $valorId_Gestion_Cbx_2)[1];
		    $LsqlXUnique = "SELECT unique_id FROM dyalogo_telefonia.dy_llamadas_salientes where id_dy_llamada = '".$valorSentido."';";
		    $resPXunique = $mysqli->query($LsqlXUnique);
		    if($resPXunique){
		        $estoUnique = $resPXunique->fetch_array();
		        if( $estoUnique['unique_id'] != null &&  $estoUnique['unique_id'] != ''){
		            $idLlamada = $estoUnique['unique_id'];  
		        }else{
		            $idLlamada = $valorSentido;
		        }
		    }else{
		        $idLlamada = $valorSentido;
		    }
		}


    	if($tipificacion == '-18'){

	    	if( $idLlamada != '' && $idLlamada != 'NULL' ){
				$Lsql = "SELECT ip_servidor FROM dyalogo_telefonia.dy_configuracion_crm WHERE id_huesped = -1 AND sistema = 0";
	            if( ($query = $mysqli->query($Lsql)) == TRUE ){

	                $array = $query->fetch_array();    
	                $linkContenido="https://".$array["ip_servidor"].":8181/dyalogocore/api/voip/downloadrecord?tk=25L8cKxojzX5HFeXgy2L&uid=".$idLlamada."&uid2=".$valorSentido;              	  
	            }
			} 
    	}

    	 
    	$Lsql="SELECT ESTPAS_ConsInte__b FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND (ESTPAS_Tipo______b = 1 OR ESTPAS_Tipo______b = 6)";            
        if( ($query = $mysqli->query($Lsql)) == TRUE ){
            $array = $query->fetch_array();  
            if($array["ESTPAS_ConsInte__b"] != ''){
            	 $paso=$array["ESTPAS_ConsInte__b"];    
            	  
            }
                

        }

        $fechaInicial =  new DateTime($_GET['tiempoInicio']);
        $fechaFinal = new DateTime($fechaInsercion);
        $duracion = $fechaInicial->diff($fechaFinal);
        $duracion = $duracion->format("%H:%I:%S");

        $LmonoEfLSql = "SELECT * FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_GET['idMonoef'];
        $resMonoEf = $mysqli->query($LmonoEfLSql);
        $dataMonoEf = $resMonoEf->fetch_array();
        $reintento = $dataMonoEf['MONOEF_TipNo_Efe_b'];
        $clasificacion = $dataMonoEf['MONOEF_Contacto__b'];
      

    	/* Ahora procedemos a insertar las cosas que necesitamos */
        $Lsql = "SELECT * FROM ".$BaseDatos_systema.".GUION_ WHERE GUION__ConsInte__b = ".$int_Guion_Campan;
        $res = $mysqli->query($Lsql);
        $dato = $res->fetch_array();
             

        /*YCR 2019-10-16
		*Inserción de datos en el SCRIPT
        */
        $LsqlScript = "INSERT INTO ".$BaseDatos.".G".$int_Guion_Campan."(";
        $LsqlScript .= "G".$int_Guion_Campan."_FechaInsercion";
        $LsqlScript .= ",G".$int_Guion_Campan."_Usuario";
        $LsqlScript .= ",G".$int_Guion_Campan."_CodigoMiembro";
        $LsqlScript .= ",G".$int_Guion_Campan."_IdLlamada";
        $LsqlScript .= ",G".$int_Guion_Campan."_Sentido___b";
        $LsqlScript .= ",G".$int_Guion_Campan."_Canal_____b";
        $LsqlScript .= ",G".$int_Guion_Campan."_LinkContenido";
        $LsqlScript .= ",G".$int_Guion_Campan."_DetalleCanal";
        $LsqlScript .= ",G".$int_Guion_Campan."_DatoContacto";
        $LsqlScript .= ",G".$int_Guion_Campan."_Paso";
        $LsqlScript .= ",G".$int_Guion_Campan."_Clasificacion";
        $LsqlScript .= ",G".$int_Guion_Campan."_Duracion___b";
        $LsqlScript .= ",G".$int_Guion_Campan."_C".$dato['GUION__ConsInte__PREGUN_Tip_b'];
        $LsqlScript .= ",G".$int_Guion_Campan."_C".$dato['GUION__ConsInte__PREGUN_Rep_b'];
        $LsqlScript .= ",G".$int_Guion_Campan."_C".$dato['GUION__ConsInte__PREGUN_Fec_b'];
        $LsqlScript .= ",G".$int_Guion_Campan."_C".$dato['GUION__ConsInte__PREGUN_Hor_b'];
        $LsqlScript .= ",G".$int_Guion_Campan."_C".$dato['GUION__ConsInte__PREGUN_Com_b'];
        $LsqlScript .= ",G".$int_Guion_Campan."_C".$dato['GUION__ConsInte__PREGUN_Age_b']." )";
        $LsqlScript .= "VALUES (";
        $LsqlScript .= "'".$fechaInsercion."'";
        $LsqlScript .= ",'".$idUsuario."'";
        $LsqlScript .= ",'".$codigoMiembro."'";
        $LsqlScript .= ",'".$idLlamada."'";
        $LsqlScript .= ",'".$sentido."'";
        $LsqlScript .= ",'".$canal."'";
        $LsqlScript .= ",'".$linkContenido."'";
        $LsqlScript .= ",'".$detalleCanal."'";
        $LsqlScript.= ",'".$datoContacto."'";
        $LsqlScript .= ",'".$paso."'";
        $LsqlScript .= ",'".$clasificacion."'";
        $LsqlScript .= ",'".$duracion."'";
        $LsqlScript .= ",'".$tipificacion."'";
        $LsqlScript .= ",'".$reintento."'";
        $LsqlScript .= ",'".$fecha."'";
        $LsqlScript .= ",'".$hora."'";
        $LsqlScript .= ",'".$comentario."'";
        $LsqlScript .= ",'".$nombreUsuario."')";

        if($mysqli->query($LsqlScript ) === TRUE){
        	
        }else{
        	echo "Error insertando Script => ".$mysqli->error;
    		$queryScript="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
			VALUES(\"".$LsqlScript  ."\",\"".$mysqli->error."\",'Insercion Script Busqueda Manual')";
			$mysqli->query($queryScript);
        }

        /*YCR
		* Actualizacion de la BD
        */
        $LsqlBD = "UPDATE ".$BaseDatos.".".$str_Pobla_Campan." SET ";
        $LsqlBD .= $str_Pobla_Campan."_CantidadIntentos = ".$str_Pobla_Campan."_CantidadIntentos +1";
        $LsqlBD .= ",".$str_Pobla_Campan."_ComentarioUG_b = '".$comentario."'";
        $LsqlBD .= ",".$str_Pobla_Campan."_DetalleCanalUG_b = '".$detalleCanal."'";
        $LsqlBD .= "  WHERE  ".$str_Pobla_Campan."_ConsInte__b = -1 ";

        
         
		if($mysqli->query($LsqlBD ) === TRUE){
        	$idNuevo=$mysqli->insert_id;
        }else{
        	echo "Error actualizando intentos BD => ".$mysqli->error;
    		$queryBD="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
			VALUES(\"".$LsqlBD."\",\"".$mysqli->error."\",'Actualizacion BD Busqueda Manual')";
			$mysqli->query($queryBD);
        }		


        $LsqlMuestra = "UPDATE ".$BaseDatos.".".$muestra." SET ";
        $LsqlMuestra .= $muestra."_NumeInte__b = ".$muestra."_NumeInte__b + 1";
        $LsqlMuestra .= "  WHERE  ".$muestra."_CoInMiPo__b = -1 ";	

        if($mysqli->query($LsqlMuestra  ) === TRUE){
        	
        }else{
        	echo "Error actaulizando intentos Muestra  => ".$mysqli->error;
    		$queryMuestra="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
			VALUES(\"".$LsqlMuestra."\",\"".$mysqli->error."\",'Actualizacion Muestra Busqueda Manual')";
			$mysqli->query($queryMuestra);
        }		

					


        /*YCR 2019-10-16
		*Inserción de datos en el CONDIA
        */
        $LsqlCondia = "INSERT INTO ".$BaseDatos_systema.".CONDIA (
			        	CONDIA_IndiEfec__b, 
			        	CONDIA_TipNo_Efe_b, 
			        	CONDIA_ConsInte__MONOEF_b, 
			        	CONDIA_TiemDura__b, 
			        	CONDIA_Fecha_____b, 
			        	CONDIA_ConsInte__CAMPAN_b, 
			        	CONDIA_ConsInte__USUARI_b, 
			        	CONDIA_ConsInte__GUION__Gui_b, 
			        	CONDIA_ConsInte__GUION__Pob_b, 
			        	CONDIA_ConsInte__MUESTR_b, 
			        	CONDIA_CodiMiem__b, 
			        	CONDIA_Observacio_b, 
			        	CONDIA_IdenLlam___b,
			        	CONDIA_TiemPrev__b,
			        	CONDIA_Canal_b,
			        	CONDIA_Sentido___b,
                        CONDIA_UniqueId_b) 
			        	VALUES (
		        		'0', 
		        		'".$reintento."',
		        		'".$tipificacion."',
		        		'".date('Y-m-d').' '.$duracion."',
		        		'".$fechaInsercion."',
		        		'".$_GET["campana_crm"]."',
		        		'".$idUsuario."',
		        		'".$int_Guion_Campan."',
		        		'".$int_Pobla_Camp_2."',
		        		'".$int_Muest_Campan."',
		        		'".$codigoMiembro."',
		        		'".$comentario."' ,
		        		'".$idLlamada."',
		        		'0',
		        		'Bq.Manual',
		        		'".$_GET['sentido']."',
                        '".$idLlamada."'
	        		)";
	    
	    if($mysqli->query($LsqlCondia) === true){

        }else{
        	echo "Error insertando Condia => ".$mysqli->error;
    		$queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
			VALUES(\"".$LsqlCondia."\",\"".$mysqli->error."\",'Insercion Condia Busqueda Manual')";
			$mysqli->query($queryCondia);
        }
	
	}




	/* Cerrar Gestion */
	if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){
    	$data =  array();
		
		$data = array(	
				"strToken_t" => $_GET['token'], 
				"strIdGestion_t" => $_GET['id_gestion_cbx'], 
				"intTipoReintento_t" => -1,
				"strFechaHoraAgenda_t" => -1,
				"intConsInte_t"	=> -1,
				"booForzarCierre_t" => true,
				"intMonoefEfectiva_t" => -1,
				"intConsInteTipificacion_t" => -1,
				"boolFinalizacionDesdeBlend_t" => false,
				"intMonoefContacto_t" => -1
			); 
				
	        	
        $ch = curl_init($IP_CONFIGURADA .'gestion/finalizar');
				                                                                   
		$data_string = json_encode($data);    

		//echo $data_string;
		//especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 	"POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 

		//le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, 	array(                                                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data_string))                                                                      
			); 
		//recogemos la respuesta
		$respuesta = curl_exec ($ch);
		//o el error, por si falla
		$error = curl_error($ch);
		//y finalmente cerramos curl
		//echo "Respuesta =>  ". $respuesta;
		//echo "<br/>Error => ".$error;

		curl_close ($ch);
	}
