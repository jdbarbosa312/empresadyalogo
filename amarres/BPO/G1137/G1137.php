
<?php date_default_timezone_set('America/Bogota'); ?>
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edición</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1137/G1137_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

	$PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

	if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1137_ConsInte__b as id, G1137_C17541 as camp1 , G1137_C17543 as camp2, LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1137 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1137_C17554  WHERE G1137_Usuario = ".$idUsuario." ORDER BY FIELD(G1137_C17554,14288,14289,14290,14291,14292), G1137_FechaInsercion DESC  LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1137_ConsInte__b as id, G1137_C17541 as camp1 , G1137_C17543 as camp2, LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1137 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1137_C17554  ORDER BY FIELD(G1137_C17554,14288,14289,14290,14291,14292), G1137_FechaInsercion DESC LIMIT 0, 50";
        }
    }else{
        $Zsql = "SELECT G1137_ConsInte__b as id, G1137_C17541 as camp1 , G1137_C17543 as camp2,  LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1137 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1137_C17554  ORDER BY FIELD(G1137_C17554,14288,14289,14290,14291,14292), G1137_FechaInsercion DESC  LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>

<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="box box-primary box-solid">
			<div class="box-header">
                <h3 class="box-title">Historico de gestiones</h3>
            </div>
			<div class="box-body">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Gesti&oacute;n</th>
							<th>Comentarios</th>
							<th>Fecha - hora</th>
							<th>Agente</th>
						</tr>
					</thead>
					<tbody id="tablaGestiones">
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="2192" >


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C20679" id="LblG1137_C20679">Código Cliente </label>
			            <input type="text" class="form-control input-sm" id="G1137_C20679" value="" readonly name="G1137_C20679"  placeholder="Código Cliente ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_CodigoMiembro" id="LblG1137_CodigoMiembro">CODIGO MIEMBRO</label>
			            <input type="text" class="form-control input-sm" id="G1137_CodigoMiembro" value="" readonly name="G1137_CodigoMiembro"  placeholder="CODIGO MIEMBRO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">


            		<!-- CAMPO TIPO TEXTO -->
 					<div class="form-group">
			            <label for="G1137_Usuario" id="LblG1137_Usuario">AGENTE-CALL</label>
			            <input type="text" style="color:#0897B0" class="form-control input-sm" id="G1137_Usuario" value="" readonly name="G1137_Usuario"  placeholder="NOMBRE AGENTE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17541" id="LblG1137_C17541">RAZON SOCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17541" value="" readonly name="G1137_C17541"  placeholder="RAZON SOCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17542" id="LblG1137_C17542">NOMBRE COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17542" value="" readonly name="G1137_C17542"  placeholder="NOMBRE COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17543" id="LblG1137_C17543">CONTACTO COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17543" value="" readonly name="G1137_C17543"  placeholder="CONTACTO COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C20680" id="LblG1137_C20680">ID Contacto comercial</label>
			            <input type="text" class="form-control input-sm" id="G1137_C20680" value="" readonly name="G1137_C20680"  placeholder="ID Contacto comercial">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C20681" id="LblG1137_C20681">Cuidad de expedición </label>
			            <input type="text" class="form-control input-sm" id="G1137_C20681" value="" readonly name="G1137_C20681"  placeholder="Cuidad de expedición ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17544" id="LblG1137_C17544">CEDULA NIT</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17544" value="" readonly name="G1137_C17544"  placeholder="CEDULA NIT">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17545" id="LblG1137_C17545">TEL 1</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17545" value="" readonly name="G1137_C17545"  placeholder="TEL 1">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17546" id="LblG1137_C17546">TEL 2</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17546" value="" readonly name="G1137_C17546"  placeholder="TEL 2">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17547" id="LblG1137_C17547">TEL 3</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17547" value="" readonly name="G1137_C17547"  placeholder="TEL 3">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C19153" id="LblG1137_C19153">CIUDAD</label>
			            <input type="text" class="form-control input-sm" id="G1137_C19153" value="" readonly name="G1137_C19153"  placeholder="CIUDAD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17548" id="LblG1137_C17548">DEPARTAMENTO</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17548" value="" readonly name="G1137_C17548"  placeholder="DEPARTAMENTO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17549" id="LblG1137_C17549">MAIL</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17549" value="" readonly name="G1137_C17549"  placeholder="MAIL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17550" id="LblG1137_C17550">DIRECCION</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17550" value="" readonly name="G1137_C17550"  placeholder="DIRECCION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17551" id="LblG1137_C17551">FECHA DE CARGUE</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17551" value="" readonly name="G1137_C17551"  placeholder="FECHA DE CARGUE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17552" id="LblG1137_C17552">CLASIFICACION</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17552" value="" readonly name="G1137_C17552"  placeholder="CLASIFICACION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1137_C19154" id="LblG1137_C19154">CUPO CLIENTE</label>
			            <input type="text" class="form-control input-sm Numerico" value="" readonly name="G1137_C19154" id="G1137_C19154" placeholder="CUPO CLIENTE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1137_C19155" id="LblG1137_C19155">ESTADO CLIENTE</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1137_C19155" readonly id="G1137_C19155">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 865 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1137_C19156" id="LblG1137_C19156">FECHA ACTIVACION</label>
			            <input type="text" class="form-control input-sm Fecha" value="" readonly name="G1137_C19156" id="G1137_C19156" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1137_C19157" id="LblG1137_C19157">FECHA VENCIMIENTO</label>
			            <input type="text" class="form-control input-sm Fecha" value="" readonly name="G1137_C19157" id="G1137_C19157" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>

  
        </div>

 <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group" hidden="" >
			            <label for="G1137_C22660" id="LblG1137_C22660">Agente-call</label>
			            <input type="text" class="form-control input-sm" id="G1137_C22660" value="" readonly name="G1137_C22660"  placeholder="Agente-call">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


        </div>


</div>

<div id="2194" style='display:none;'>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17560" id="LblG1137_C17560">Agente</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17560" value="<?php echo getNombreUser($token);?>" readonly name="G1137_C17560"  placeholder="Agente">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17561" id="LblG1137_C17561">Fecha</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17561" value="<?php echo date('Y-m-d');?>" readonly name="G1137_C17561"  placeholder="Fecha">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17562" id="LblG1137_C17562">Hora</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17562" value="<?php echo date('H:i:s');?>" readonly name="G1137_C17562"  placeholder="Hora">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C17563" id="LblG1137_C17563">Campaña</label>
			            <input type="text" class="form-control input-sm" id="G1137_C17563" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"];}else{
                	echo "SIN CAMPAÑA";}?>" readonly name="G1137_C17563"  placeholder="Campaña">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2410">
                DATOS DE PERFIL
            </a>
        </h4>
    </div>
    <div id="s_2410" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1137_C19158" id="LblG1137_C19158">¿Es ó ha sido cliente de Fincaraiz?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1137_C19158" readonly id="G1137_C19158">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1137_C19159" id="LblG1137_C19159">¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1137_C19159" readonly id="G1137_C19159">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			       
			        <!-- CAMPO DE TIPO GUION -->
			        <div class="form-group">
			            <label for="G1137_C19160" id="LblG1137_C19160">¿Desde que ciudad realiza la administración de los inmuebles?</label>
			            <select class="form-control input-sm select2" style="width: 100%;" readonly  name="G1137_C19160" id="G1137_C19160">
			                <option value="0" id="opcionVacia">Seleccione</option>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1137_C19161" id="LblG1137_C19161">¿Cuántos inmuebles USADOS tiene en su inventario?</label>
			            <input type="text" class="form-control input-sm Numerico" value="" readonly name="G1137_C19161" id="G1137_C19161" placeholder="¿Cuántos inmuebles USADOS tiene en su inventario?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1137_C19162" id="LblG1137_C19162">Cupo autorizado</label>
			            <input type="text" class="form-control input-sm" id="G1137_C19162" value="" readonly name="G1137_C19162"  placeholder="Cupo autorizado">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1137_C19163" id="LblG1137_C19163">¿Cuántos inmuebles NUEVOS tiene en su inventario?</label>
			            <input type="text" class="form-control input-sm Numerico" value="" readonly name="G1137_C19163" id="G1137_C19163" placeholder="¿Cuántos inmuebles NUEVOS tiene en su inventario?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>

  
        </div>


        </div>
    </div>
</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2196">
                Datos tarea
            </a>
        </h4>
    </div>
    <div id="s_2196" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO MEMO -->
			        <div class="form-group">
			            <label for="G1137_C17564" id="LblG1137_C17564">Observación</label>
			            <textarea class="form-control input-sm" name="G1137_C17564" id="G1137_C17564"  value="" disabled="disabled" placeholder="Observación"></textarea>
			        </div>
			        <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div>


        </div>


        </div>
    </div>
</div>

<div id="2193" >


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
            </div>


            <div class="col-md-6 col-xs-6">

  
            </div>

  
        </div>


</div>

<input type="hidden" name="campana_crm" id="campana_crm" value="<?php if(isset($_GET["campana_crm"])){echo $_GET["campana_crm"];}else{ echo "569";}?>">
<div class="row" style="background-color: #FAFAFA; ">
	<br/>
    <div class="col-md-5 col-xs-5">
        <div class="form-group">
       		<label for="G1137_C17553">Tipificaci&oacute;n</label>
            <select class="form-control input-sm tipificacionBackOffice" name="tipificacion" id="G1137_C17553">
                <option value="0">Seleccione</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 855;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-5 col-xs-5">
        <div class="form-group">
        	<label for="G1137_C17554">Estado de la tarea</label>
            <select class="form-control input-sm reintento" name="reintento" id="G1137_C17554">
                <option value="0">Seleccione</option>
                <?php
                	$Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC 
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 856;";
	                $obj = $mysqli->query($Lsql);
	                while($obje = $obj->fetch_object()){
	                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
	                }      
                ?>
            </select>     
        </div>
    </div>
    <div class="col-md-2 col-xs-2" style="text-align: center;">
    	<label for="G1137_C17554" style="visibility:hidden;">Estado de la tarea</label>
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Guardar Gesti&oacute;n
        </button>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G1137_C17559" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G1137/G1137_eventos.js"></script> 
<script type="text/javascript">
    $(function(){

    	var meses = new Array(12);
    	meses[0] = "01";
    	meses[1] = "02";
    	meses[2] = "03";
    	meses[3] = "04";
    	meses[4] = "05";
    	meses[5] = "06";
    	meses[6] = "07";
    	meses[7] = "08";
    	meses[8] = "09";
    	meses[9] = "10";
    	meses[10] = "11";
    	meses[11] = "12";

    	var d = new Date();
        var h = d.getHours();
        var horas = (h < 10) ? '0' + h : h;
        var dia = d.getDate();
        var dias = (dia < 10) ? '0' + dia : dia;
        var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
        $("#FechaInicio").val(fechaInicial);
            

        //Esta es por si lo llaman en modo formulario de edicion LigthBox
        <?php if(isset($_GET['registroId'])){ ?>
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
            dataType : 'json',
            success  : function(data){
                //recorrer datos y enviarlos al formulario
                $.each(data, function(i, item) {
                    
 
                    $("#G1137_C20679").val(item.G1137_C20679);

                    $("#G1137_CodigoMiembro").val(item.G1137_CodigoMiembro);

                    $("#G1137_Usuario").val(item.G1137_Usuario);
 
                    $("#G1137_C17541").val(item.G1137_C17541);
 
                    $("#G1137_C17542").val(item.G1137_C17542);
 
                    $("#G1137_C17543").val(item.G1137_C17543);
 
                    $("#G1137_C20680").val(item.G1137_C20680);
 
                    $("#G1137_C20681").val(item.G1137_C20681);
 
                    $("#G1137_C17544").val(item.G1137_C17544);
 
                    $("#G1137_C17545").val(item.G1137_C17545);
 
                    $("#G1137_C17546").val(item.G1137_C17546);
 
                    $("#G1137_C17547").val(item.G1137_C17547);
 
                    $("#G1137_C19153").val(item.G1137_C19153);
 
                    $("#G1137_C17548").val(item.G1137_C17548);
 
                    $("#G1137_C17549").val(item.G1137_C17549);
 
                    $("#G1137_C17550").val(item.G1137_C17550);
 
                    $("#G1137_C17551").val(item.G1137_C17551);
 
                    $("#G1137_C17552").val(item.G1137_C17552);
 
                    $("#G1137_C19154").val(item.G1137_C19154);
 
                    $("#G1137_C19155").val(item.G1137_C19155);
 
                    $("#G1137_C19156").val(item.G1137_C19156);
 
                    $("#G1137_C19157").val(item.G1137_C19157);
 
                    $("#G1137_C22660").val(item.G1137_C22660);
 
                    $("#G1137_C17553").val(item.G1137_C17553);
 
                    $("#G1137_C17554").val(item.G1137_C17554);
 
                    $("#G1137_C17555").val(item.G1137_C17555);
 
                    $("#G1137_C17556").val(item.G1137_C17556);
 
                    $("#G1137_C17557").val(item.G1137_C17557);
 
                    $("#G1137_C17558").val(item.G1137_C17558);
 
                    $("#G1137_C17559").val(item.G1137_C17559);
 
                    $("#G1137_C17560").val(item.G1137_C17560);
 
                    $("#G1137_C17561").val(item.G1137_C17561);
 
                    $("#G1137_C17562").val(item.G1137_C17562);
 
                    $("#G1137_C17563").val(item.G1137_C17563);
 
                    $("#G1137_C17564").val(item.G1137_C17564);
 
                    $("#G1137_C19158").val(item.G1137_C19158);
 
                    $("#G1137_C19159").val(item.G1137_C19159);
 
                    $("#G1137_C19160").val(item.G1137_C19160);
 
                    $("#G1137_C19160").val(item.G1137_C19160).trigger("change"); 
 
                    $("#G1137_C19161").val(item.G1137_C19161);
 
                    $("#G1137_C19162").val(item.G1137_C19162);
 
                    $("#G1137_C19163").val(item.G1137_C19163);

					$("#h3mio").html(item.principal);

                });
                //Deshabilitar los campos

                //Habilitar todos los campos para edicion
                $('#FormularioDatos :input').each(function(){
                    $(this).attr('disabled', true);
                });

                              

                //Habilidar los botones de operacion, add, editar, eliminar
                $("#add").attr('disabled', false);
                $("#edit").attr('disabled', false);
                $("#delete").attr('disabled', false);

                //Desahabiliatra los botones de salvar y seleccionar_registro
                $("#cancel").attr('disabled', true);
                $("#Save").attr('disabled', true);
            } 
        });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

     	$("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
        	/*
        	vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
        	idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        

        //datepickers
        

        $("#G1137_C19156").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1137_C19157").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1137_C17557").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1137_C17558").wickedpicker(options);

        //Validaciones numeros Enteros
        

    	$("#G1137_C19154").numeric();
		        
    	$("#G1137_C17555").numeric();
		        
    	$("#G1137_C17556").numeric();
		        
    	$("#G1137_C19161").numeric();
		        
    	$("#G1137_C19163").numeric();
		        

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO CLIENTE 

    $("#G1137_C19155").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ¿Es ó ha sido cliente de Fincaraiz? 

    $("#G1137_C19158").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz? 

    $("#G1137_C19159").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });


        $("#G1137_C17553").change(function(){
        	var id = $(this).attr('id');
            var valor = $("#"+ id +" option:selected").attr('efecividad');
            var monoef = $("#"+ id +" option:selected").attr('monoef');
            var TipNoEF = $("#"+ id +" option:selected").attr('TipNoEF');
            var cambio = $("#"+ id +" option:selected").attr('cambio');
            var importancia = $("#"+ id + " option:selected").attr('importancia');
            var contacto = $("#"+id+" option:selected").attr('contacto');
            $(".reintento").val(TipNoEF).change();
            $("#Efectividad").val(valor);
            $("#MonoEf").val(monoef);
            $("#TipNoEF").val(TipNoEF);
            $("#MonoEfPeso").val(importancia);
            $("#ContactoMonoEf").val(contacto);
    	});
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
        	$("#Save").attr('disabled', true);
        	var bol_respuesta = before_save();
        	var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
            	alertify.error("Es necesaria la tipificación!");
            	valido = 1;
            }

            if($(".reintento").val() == '2'){
            	if($(".TxtFechaReintento").val().length < 1){
            		alertify.error("Es necesario llenar la fecha de reintento!");
            		$(".TxtFechaReintento").focus();
            		valido = 1;
            	}

            	if($(".TxtHoraReintento").val().length < 1){
            		alertify.error("Es necesario llenar la hora de reintento!");
            		$(".TxtHoraReintento").focus();
            		valido = 1;
            	}
            }

            if(valido == '0'){
	        	if(bol_respuesta){            
		            var form = $("#FormularioDatos");
		            //Se crean un array con los datos a enviar, apartir del formulario 
		            var formData = new FormData($("#FormularioDatos")[0]);
		            if($("#G1137_C17553 option:selected").text() == 'Devuelta'){
	                	formData.append("tareaDevuelta","SI");
	                }
		            $.ajax({
		               url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "569"; } ?>',  
		                type: 'POST',
		                data: formData,
		                cache: false,
		                contentType: false,
		                processData: false,
		                //una vez finalizado correctamente
		                success: function(data){
		                    if(data != '0'){
		                    	$("#Save").attr('disabled', false);
			                    alertify.success('Información guardada con exito');   
		                    }else{
		                        //Algo paso, hay un error
		                        $("#Save").attr('disabled', false);
		                        alertify.error('Un error ha ocurrido y no pudimos guardar la información');
		                    }                
		                },
		                //si ha ocurrido un error
		                error: function(){
		                    after_save_error();
		                    $("#Save").attr('disabled', false);
		                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
		                }
		            });
          		}
          	}
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto comercial','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente-call','PASO_ID','REGISTRO_ID','Agente','Fecha','Hora','Campaña','Observación','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

	                ,
	                { 
	                    name:'G1137_C20679', 
	                    index: 'G1137_C20679', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_CodigoMiembro', 
	                    index: 'G1137_CodigoMiembro', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_Usuario', 
	                    index: 'G1137_Usuario', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17541', 
	                    index: 'G1137_C17541', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17542', 
	                    index: 'G1137_C17542', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17543', 
	                    index: 'G1137_C17543', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C20680', 
	                    index: 'G1137_C20680', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C20681', 
	                    index: 'G1137_C20681', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17544', 
	                    index: 'G1137_C17544', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17545', 
	                    index: 'G1137_C17545', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17546', 
	                    index: 'G1137_C17546', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17547', 
	                    index: 'G1137_C17547', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C19153', 
	                    index: 'G1137_C19153', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17548', 
	                    index: 'G1137_C17548', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17549', 
	                    index: 'G1137_C17549', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17550', 
	                    index: 'G1137_C17550', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17551', 
	                    index: 'G1137_C17551', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17552', 
	                    index: 'G1137_C17552', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1137_C19154', 
	                    index:'G1137_C19154', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1137_C19155', 
	                    index:'G1137_C19155', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1137_C19155'
	                    }
	                }

	                ,
	                {  
	                    name:'G1137_C19156', 
	                    index:'G1137_C19156', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1137_C19157', 
	                    index:'G1137_C19157', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1137_C22660', 
	                    index: 'G1137_C22660', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1137_C17555', 
	                    index:'G1137_C17555', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }
 
	                ,
	                {  
	                    name:'G1137_C17556', 
	                    index:'G1137_C17556', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1137_C17560', 
	                    index: 'G1137_C17560', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17561', 
	                    index: 'G1137_C17561', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17562', 
	                    index: 'G1137_C17562', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17563', 
	                    index: 'G1137_C17563', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C17564', 
	                    index:'G1137_C17564', 
	                    width:150, 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1137_C19158', 
	                    index:'G1137_C19158', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1137_C19158'
	                    }
	                }

	                ,
	                { 
	                    name:'G1137_C19159', 
	                    index:'G1137_C19159', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1137_C19159'
	                    }
	                }

	                ,
	                { 
	                    name:'G1137_C19160', 
	                    index:'G1137_C19160', 
	                    width:300 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1137_C19160=si',
	                        dataInit:function(el){
	                        	$(el).select2();
	                            /*$(el).select2({ 
	                                templateResult: function(data) {
	                                    var r = data.text.split('|');
	                                    var row = '<div class="row">';
	                                    var totalRows = 12 / r.length;
	                                    for(i= 0; i < r.length; i++){
	                                        row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
	                                    }
	                                    row += '</div>';
	                                    var $result = $(row);
	                                    return $result;
	                                },
	                                templateSelection : function(data){
	                                    var r = data.text.split('|');
	                                    return r[0];
	                                }
	                            });*/
	                            $(el).change(function(){
	                                var valores = $(el + " option:selected").attr("llenadores");
	                                var campos =  $(el + " option:selected").attr("dinammicos");
	                                var r = valores.split('|');
	                                if(r.length > 1){

	                                    var c = campos.split('|');
	                                    for(i = 1; i < r.length; i++){
	                                        $("#"+ rowid +"_"+c[i]).val(r[i]);
	                                    }
	                                }
	                            });
	                        }
	                    }
	                }
 
	                ,
	                {  
	                    name:'G1137_C19161', 
	                    index:'G1137_C19161', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1137_C19162', 
	                    index: 'G1137_C19162', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1137_C19163', 
	                    index:'G1137_C19163', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    

                    $("#"+ rowid +"_G1137_C19160").change(function(){
                        var valores = $("#"+ rowid +"_G1137_C19160 option:selected").attr("llenadores");
                        var campos = $("#"+ rowid +"_G1137_C19160 option:selected").attr("dinammicos");
                        var r = valores.split('|');

                        if(r.length > 1){

                            var c = campos.split('|');
                            for(i = 1; i < r.length; i++){
                                if(!$("#"+c[i]).is("select")) {
                                // the input field is not a select
                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                }else{
                                    var change = r[i].replace(' ', ''); 
                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                }
                                
                            }
                        }
                    });
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1137_C17541',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(x){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', Busqueda : x  , G1137_C20679 : $("#busq_G1137_C20679").val() , G1137_C17541 : $("#busq_G1137_C17541").val() , G1137_C17542 : $("#busq_G1137_C17542").val() , G1137_C17544 : $("#busq_G1137_C17544").val(), CodMiembro : $("#busq_G1137_CodigoMiembro").val() },
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    var strIconoBackOffice = '';
                    if(data[i].estado == '1'){
                        strIconoBackOffice = 'En gestión';
                    }else if(data[i].estado == '2'){
                        strIconoBackOffice = 'Cerrada';
                    }else if(data[i].estado == '3'){
                        strIconoBackOffice = 'Devuelta';
                    }
                    

                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"<span style='position: relative;right: 2px;float: right;font-size:10px;"+ data[i].color+ "'>"+strIconoBackOffice+"</span></p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');
            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G1137_C20679").val(item.G1137_C20679);

                        $("#G1137_CodigoMiembro").val(item.G1137_CodigoMiembro);

                    	$("#G1137_Usuario").val(item.G1137_Usuario);

                        $("#G1137_C17541").val(item.G1137_C17541);

                        $("#G1137_C17542").val(item.G1137_C17542);

                        $("#G1137_C17543").val(item.G1137_C17543);

                        $("#G1137_C20680").val(item.G1137_C20680);

                        $("#G1137_C20681").val(item.G1137_C20681);

                        $("#G1137_C17544").val(item.G1137_C17544);

                        $("#G1137_C17545").val(item.G1137_C17545);

                        $("#G1137_C17546").val(item.G1137_C17546);

                        $("#G1137_C17547").val(item.G1137_C17547);

                        $("#G1137_C19153").val(item.G1137_C19153);

                        $("#G1137_C17548").val(item.G1137_C17548);

                        $("#G1137_C17549").val(item.G1137_C17549);

                        $("#G1137_C17550").val(item.G1137_C17550);

                        $("#G1137_C17551").val(item.G1137_C17551);

                        $("#G1137_C17552").val(item.G1137_C17552);

                        $("#G1137_C19154").val(item.G1137_C19154);

                        $("#G1137_C19155").val(item.G1137_C19155);
 
        	            $("#G1137_C19155").val(item.G1137_C19155).trigger("change"); 

                        $("#G1137_C19156").val(item.G1137_C19156);

                        $("#G1137_C19157").val(item.G1137_C19157);

                        $("#G1137_C22660").val(item.G1137_C22660);

                        $("#G1137_C17553").val(item.G1137_C17553);
 
        	            $("#G1137_C17553").val(item.G1137_C17553).trigger("change"); 

                        $("#G1137_C17554").val(item.G1137_C17554);
 
        	            $("#G1137_C17554").val(item.G1137_C17554).trigger("change"); 

                        $("#G1137_C17555").val(item.G1137_C17555);

                        $("#G1137_C17556").val(item.G1137_C17556);

                        $("#G1137_C17557").val(item.G1137_C17557);

                        $("#G1137_C17558").val(item.G1137_C17558);

                        $("#G1137_C17559").val(item.G1137_C17559);

                        $("#G1137_C17560").val(item.G1137_C17560);

                        $("#G1137_C17561").val(item.G1137_C17561);

                        $("#G1137_C17562").val(item.G1137_C17562);

                        $("#G1137_C17563").val(item.G1137_C17563);

                        $("#G1137_C17564").val(item.G1137_C17564);

                        $("#G1137_C19158").val(item.G1137_C19158);
 
        	            $("#G1137_C19158").val(item.G1137_C19158).trigger("change"); 

                        $("#G1137_C19159").val(item.G1137_C19159);
 
        	            $("#G1137_C19159").val(item.G1137_C19159).trigger("change"); 

                        $("#G1137_C19160").val(item.G1137_C19160);
 
        	            $("#G1137_C19160").val(item.G1137_C19160).trigger("change"); 

                        $("#G1137_C19161").val(item.G1137_C19161);

                        $("#G1137_C19162").val(item.G1137_C19162);

                        $("#G1137_C19163").val(item.G1137_C19163);
        				$("#h3mio").html(item.principal);
        				
                    });

               
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();

            $.ajax({
	        	url   : '<?php echo $url_crud; ?>',
	        	type  : 'post',
	        	data  : { DameHistorico : 'si', user_codigo_mien : idTotal, campana_crm : '<?php if(isset($_GET['campana_crm'])) { echo $_GET['campana_crm']; } else { echo "569"; } ?>' },
	        	dataType : 'html',
	        	success : function(data){
	        		$("#tablaGestiones").html('');
	        		$("#tablaGestiones").html(data);
	        	}

	    	});
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
	$(document).ready(function() {
		<?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                		while($objDatos = $resultDatosSql->fetch_object()){ ?>
                        	document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
             		<?php  
             			}	
                    }
                    
                } */  
            }
        ?>
       
    	<?php if(isset($_GET['user'])){ ?>
        	
        	idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
		
	});
</script>
