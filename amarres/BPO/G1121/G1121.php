
<?php date_default_timezone_set('America/Bogota'); ?>
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1121/G1121_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

	$PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

	if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1121_ConsInte__b as id, G1121_C17106 as camp1 , G1121_C17107 as camp2 FROM ".$BaseDatos.".G1121  WHERE G1121_Usuario = ".$idUsuario." ORDER BY G1121_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1121_ConsInte__b as id, G1121_C17106 as camp1 , G1121_C17107 as camp2 FROM ".$BaseDatos.".G1121  ORDER BY G1121_ConsInte__b DESC LIMIT 0, 50";
        }
    }else{
        $Zsql = "SELECT G1121_ConsInte__b as id, G1121_C17106 as camp1 , G1121_C17107 as camp2 FROM ".$BaseDatos.".G1121  ORDER BY G1121_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div id="2138" >


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1121_C20659" id="LblG1121_C20659">CODIGO CLIENTE</label>
			            <input type="text" class="form-control input-sm Numerico" value=""  name="G1121_C20659" id="G1121_C20659" placeholder="CODIGO CLIENTE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17106" id="LblG1121_C17106">RAZON SOCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17106" value=""  name="G1121_C17106"  placeholder="RAZON SOCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17107" id="LblG1121_C17107">NOMBRE COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17107" value=""  name="G1121_C17107"  placeholder="NOMBRE COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17108" id="LblG1121_C17108">CEDULA NIT</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17108" value=""  name="G1121_C17108"  placeholder="CEDULA NIT">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17643" id="LblG1121_C17643">CONTACTO COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17643" value=""  name="G1121_C17643"  placeholder="CONTACTO COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C20660" id="LblG1121_C20660">ID CONTACTO COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1121_C20660" value=""  name="G1121_C20660"  placeholder="ID CONTACTO COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C20688" id="LblG1121_C20688">Cuidad de expedición </label>
			            <input type="text" class="form-control input-sm" id="G1121_C20688" value=""  name="G1121_C20688"  placeholder="Cuidad de expedición ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17109" id="LblG1121_C17109">TEL 1</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17109" value=""  name="G1121_C17109"  placeholder="TEL 1">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17110" id="LblG1121_C17110">TEL 2</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17110" value=""  name="G1121_C17110"  placeholder="TEL 2">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17111" id="LblG1121_C17111">TEL 3</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17111" value=""  name="G1121_C17111"  placeholder="TEL 3">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17112" id="LblG1121_C17112">CIUDAD</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17112" value=""  name="G1121_C17112"  placeholder="CIUDAD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17113" id="LblG1121_C17113">DEPARTAMENTO</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17113" value=""  name="G1121_C17113"  placeholder="DEPARTAMENTO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17114" id="LblG1121_C17114">MAIL</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17114" value=""  name="G1121_C17114"  placeholder="MAIL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17115" id="LblG1121_C17115">DIRECCION</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17115" value=""  name="G1121_C17115"  placeholder="DIRECCION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17116" id="LblG1121_C17116">FECHA DE CARGUE</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17116" value=""  name="G1121_C17116"  placeholder="FECHA DE CARGUE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1121_C17117" id="LblG1121_C17117">CLASIFICACION</label>
			            <input type="text" class="form-control input-sm Numerico" value=""  name="G1121_C17117" id="G1121_C17117" placeholder="CLASIFICACION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1121_C17644" id="LblG1121_C17644">CUPO CLIENTE</label>
			            <input type="text" class="form-control input-sm Numerico" value=""  name="G1121_C17644" id="G1121_C17644" placeholder="CUPO CLIENTE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1121_C17645" id="LblG1121_C17645">ESTADO CLIENTE</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1121_C17645" id="G1121_C17645">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 865 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1121_C17647" id="LblG1121_C17647">FECHA ACTIVACION</label>
			            <input type="text" class="form-control input-sm Fecha" value=""  name="G1121_C17647" id="G1121_C17647" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1121_C17646" id="LblG1121_C17646">FECHA VENCIMIENTO</label>
			            <input type="text" class="form-control input-sm Fecha" value=""  name="G1121_C17646" id="G1121_C17646" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C20335" id="LblG1121_C20335">CALIFICACIÓN </label>
			            <input type="text" class="form-control input-sm" id="G1121_C20335" value=""  name="G1121_C20335"  placeholder="CALIFICACIÓN ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


        </div>


</div>

<div id="2139" style='display:none;'>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17118" id="LblG1121_C17118">ORIGEN_DY_WF</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17118" value="" readonly name="G1121_C17118"  placeholder="ORIGEN_DY_WF">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C17119" id="LblG1121_C17119">OPTIN_DY_WF</label>
			            <input type="text" class="form-control input-sm" id="G1121_C17119" value="" readonly name="G1121_C17119"  placeholder="OPTIN_DY_WF">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1121_C17120" id="LblG1121_C17120">ESTADO_DY</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1121_C17120" id="G1121_C17120">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 825 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2400">
                Datos perfil
            </a>
        </h4>
    </div>
    <div id="s_2400" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1121_C18885" id="LblG1121_C18885">Es o ha sido cliente de Fincaraiz ?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1121_C18885" id="G1121_C18885">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1121_C18886" id="LblG1121_C18886">Lleva algún proceso o tiene alguna propuesta con otro asesor de Fincaraiz ?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1121_C18886" id="G1121_C18886">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			       
			        <!-- CAMPO DE TIPO GUION -->
			        <div class="form-group">
			            <label for="G1121_C18887" id="LblG1121_C18887">Desde que ciudad realiza la administración de los inmuebles ?</label>
			            <select class="form-control input-sm select2" style="width: 100%;"  name="G1121_C18887" id="G1121_C18887">
			                <option value="0">Seleccione</option>

			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1121_C18888" id="LblG1121_C18888">Cuantos inmuebles USADOS tiene en su inventario ?</label>
			            <input type="text" class="form-control input-sm Numerico" value=""  name="G1121_C18888" id="G1121_C18888" placeholder="Cuantos inmuebles USADOS tiene en su inventario ?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1121_C18889" id="LblG1121_C18889">Cuantos inmuebles NUEVOS tiene en su inventario ?</label>
			            <input type="text" class="form-control input-sm Numerico" value=""  name="G1121_C18889" id="G1121_C18889" placeholder="Cuantos inmuebles NUEVOS tiene en su inventario ?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C18890" id="LblG1121_C18890">Cupo autorizado</label>
			            <input type="text" class="form-control input-sm" id="G1121_C18890" value=""  name="G1121_C18890"  placeholder="Cupo autorizado">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO MEMO -->
			        <div class="form-group">
			            <label for="G1121_C20690" id="LblG1121_C20690">COMENTARIOS BACKOFFICE </label>
			            <textarea class="form-control input-sm" name="G1121_C20690" id="G1121_C20690"  value="" placeholder="COMENTARIOS BACKOFFICE "></textarea>
			        </div>
			        <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C21549" id="LblG1121_C21549">Cupo Usado</label>
			            <input type="text" class="form-control input-sm" id="G1121_C21549" value=""  name="G1121_C21549"  placeholder="Cupo Usado">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C21550" id="LblG1121_C21550">Cupo Nuevo</label>
			            <input type="text" class="form-control input-sm" id="G1121_C21550" value=""  name="G1121_C21550"  placeholder="Cupo Nuevo">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1121_C21551" id="LblG1121_C21551">Pack</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1121_C21551" id="G1121_C21551">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 978 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1121_C21552" id="LblG1121_C21552">Cupo</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1121_C21552" id="G1121_C21552">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 977 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1121_C21553" id="LblG1121_C21553">Tiempo</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1121_C21553" id="G1121_C21553">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 979 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO DECIMAL -->
			        <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1121_C21554" id="LblG1121_C21554">Precio full sin IVA</label>
			            <input type="text" class="form-control input-sm Decimal" value=""  name="G1121_C21554" id="G1121_C21554" placeholder="Precio full sin IVA">
			        </div>
			        <!-- FIN DEL CAMPO TIPO DECIMAL -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1121_C21555" id="LblG1121_C21555">Descuento</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1121_C21555" id="G1121_C21555">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 980 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO DECIMAL -->
			        <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1121_C21556" id="LblG1121_C21556">Valor con dto sin IVA</label>
			            <input type="text" class="form-control input-sm Decimal" value=""  name="G1121_C21556" id="G1121_C21556" placeholder="Valor con dto sin IVA">
			        </div>
			        <!-- FIN DEL CAMPO TIPO DECIMAL -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1121_C21557" id="LblG1121_C21557">Vigencia</label>
			            <input type="text" class="form-control input-sm Fecha" value=""  name="G1121_C21557" id="G1121_C21557" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C21558" id="LblG1121_C21558">Obsequio</label>
			            <input type="text" class="form-control input-sm" id="G1121_C21558" value=""  name="G1121_C21558"  placeholder="Obsequio">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO MEMO -->
			        <div class="form-group">
			            <label for="G1121_C21559" id="LblG1121_C21559">Observación</label>
			            <textarea class="form-control input-sm" name="G1121_C21559" id="G1121_C21559"  value="" placeholder="Observación"></textarea>
			        </div>
			        <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1121_C21560" id="LblG1121_C21560">Fecha de lectura de contrato</label>
			            <input type="text" class="form-control input-sm Fecha" value=""  name="G1121_C21560" id="G1121_C21560" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIMEPICKER -->
			        <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
			        <div class="bootstrap-timepicker">
			            <div class="form-group">
			                <label for="G1121_C21561" id="LblG1121_C21561">Hora de lectura de contrato</label>
			                <div class="input-group">
			                    <input type="text" class="form-control input-sm Hora"  name="G1121_C21561" id="G1121_C21561" placeholder="HH:MM:SS" >
			                    <div class="input-group-addon" id="TMP_G1121_C21561">
			                        <i class="fa fa-clock-o"></i>
			                    </div>
			                </div>
			                <!-- /.input group -->
			            </div>
			            <!-- /.form group -->
			        </div>
			        <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1121_C21562" id="LblG1121_C21562">Fecha de activación</label>
			            <input type="text" class="form-control input-sm Fecha" value=""  name="G1121_C21562" id="G1121_C21562" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIMEPICKER -->
			        <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
			        <div class="bootstrap-timepicker">
			            <div class="form-group">
			                <label for="G1121_C21563" id="LblG1121_C21563">Hora de activación</label>
			                <div class="input-group">
			                    <input type="text" class="form-control input-sm Hora"  name="G1121_C21563" id="G1121_C21563" placeholder="HH:MM:SS" >
			                    <div class="input-group-addon" id="TMP_G1121_C21563">
			                        <i class="fa fa-clock-o"></i>
			                    </div>
			                </div>
			                <!-- /.input group -->
			            </div>
			            <!-- /.form group -->
			        </div>
			        <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div>

  
        </div>


        </div>
    </div>
</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2212">
                Datos de la llamada
            </a>
        </h4>
    </div>
    <div id="s_2212" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1121_C17639" id="LblG1121_C17639">ESTADO</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1121_C17639" id="G1121_C17639">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 828 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1121_C17640" id="LblG1121_C17640">TIPIFICACION 1</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1121_C17640" id="G1121_C17640">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 829 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1121_C17641" id="LblG1121_C17641">TIPIFICACION 2</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1121_C17641" id="G1121_C17641">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 830 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1121_C17642" id="LblG1121_C17642">TIPIFICACION 3</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1121_C17642" id="G1121_C17642">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 831 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1121_C22653" id="LblG1121_C22653">Agente</label>
			            <input type="text" class="form-control input-sm" id="G1121_C22653" value=""  name="G1121_C22653"  placeholder="Agente">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


        </div>


        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G1121/G1121_eventos.js"></script> 
<script type="text/javascript">
    $(function(){

    	var meses = new Array(12);
    	meses[0] = "01";
    	meses[1] = "02";
    	meses[2] = "03";
    	meses[3] = "04";
    	meses[4] = "05";
    	meses[5] = "06";
    	meses[6] = "07";
    	meses[7] = "08";
    	meses[8] = "09";
    	meses[9] = "10";
    	meses[10] = "11";
    	meses[11] = "12";

    	var d = new Date();
        var h = d.getHours();
        var horas = (h < 10) ? '0' + h : h;
        var dia = d.getDate();
        var dias = (dia < 10) ? '0' + dia : dia;
        var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
        $("#FechaInicio").val(fechaInicial);
            

        //Esta es por si lo llaman en modo formulario de edicion LigthBox
        <?php if(isset($_GET['registroId'])){ ?>
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
            dataType : 'json',
            success  : function(data){
                //recorrer datos y enviarlos al formulario
                $.each(data, function(i, item) {
                    
 
                    $("#G1121_C20659").val(item.G1121_C20659);
 
                    $("#G1121_C17106").val(item.G1121_C17106);
 
                    $("#G1121_C17107").val(item.G1121_C17107);
 
                    $("#G1121_C17108").val(item.G1121_C17108);
 
                    $("#G1121_C17643").val(item.G1121_C17643);
 
                    $("#G1121_C20660").val(item.G1121_C20660);
 
                    $("#G1121_C20688").val(item.G1121_C20688);
 
                    $("#G1121_C17109").val(item.G1121_C17109);
 
                    $("#G1121_C17110").val(item.G1121_C17110);
 
                    $("#G1121_C17111").val(item.G1121_C17111);
 
                    $("#G1121_C17112").val(item.G1121_C17112);
 
                    $("#G1121_C17113").val(item.G1121_C17113);
 
                    $("#G1121_C17114").val(item.G1121_C17114);
 
                    $("#G1121_C17115").val(item.G1121_C17115);
 
                    $("#G1121_C17116").val(item.G1121_C17116);
 
                    $("#G1121_C17117").val(item.G1121_C17117);
 
                    $("#G1121_C17644").val(item.G1121_C17644);
 
                    $("#G1121_C17645").val(item.G1121_C17645);
 
                    $("#G1121_C17647").val(item.G1121_C17647);
 
                    $("#G1121_C17646").val(item.G1121_C17646);
 
                    $("#G1121_C20335").val(item.G1121_C20335);
 
                    $("#G1121_C17118").val(item.G1121_C17118);
 
                    $("#G1121_C17119").val(item.G1121_C17119);
 
                    $("#G1121_C17120").val(item.G1121_C17120);
 
                    $("#G1121_C17639").val(item.G1121_C17639);
 
                    $("#G1121_C17640").val(item.G1121_C17640);
 
                    $("#G1121_C17641").val(item.G1121_C17641);
 
                    $("#G1121_C17642").val(item.G1121_C17642);
 
                    $("#G1121_C22653").val(item.G1121_C22653);
 
                    $("#G1121_C18885").val(item.G1121_C18885);
 
                    $("#G1121_C18886").val(item.G1121_C18886);
 
                    $("#G1121_C18887").val(item.G1121_C18887);
 
                    $("#G1121_C18887").val(item.G1121_C18887).trigger("change"); 
 
                    $("#G1121_C18888").val(item.G1121_C18888);
 
                    $("#G1121_C18889").val(item.G1121_C18889);
 
                    $("#G1121_C18890").val(item.G1121_C18890);
 
                    $("#G1121_C20690").val(item.G1121_C20690);
 
                    $("#G1121_C21549").val(item.G1121_C21549);
 
                    $("#G1121_C21550").val(item.G1121_C21550);
 
                    $("#G1121_C21551").val(item.G1121_C21551);
 
                    $("#G1121_C21552").val(item.G1121_C21552);
 
                    $("#G1121_C21553").val(item.G1121_C21553);
 
                    $("#G1121_C21554").val(item.G1121_C21554);
 
                    $("#G1121_C21555").val(item.G1121_C21555);
 
                    $("#G1121_C21556").val(item.G1121_C21556);
 
                    $("#G1121_C21557").val(item.G1121_C21557);
 
                    $("#G1121_C21558").val(item.G1121_C21558);
 
                    $("#G1121_C21559").val(item.G1121_C21559);
 
                    $("#G1121_C21560").val(item.G1121_C21560);
 
                    $("#G1121_C21561").val(item.G1121_C21561);
 
                    $("#G1121_C21562").val(item.G1121_C21562);
 
                    $("#G1121_C21563").val(item.G1121_C21563);

					$("#h3mio").html(item.principal);

                });
                //Deshabilitar los campos

                //Habilitar todos los campos para edicion
                $('#FormularioDatos :input').each(function(){
                    $(this).attr('disabled', true);
                });

                              

                //Habilidar los botones de operacion, add, editar, eliminar
                $("#add").attr('disabled', false);
                $("#edit").attr('disabled', false);
                $("#delete").attr('disabled', false);

                //Desahabiliatra los botones de salvar y seleccionar_registro
                $("#cancel").attr('disabled', true);
                $("#Save").attr('disabled', true);
            } 
        });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

     	$("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
        	/*
        	vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
        	idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G1121_C17645").select2();

    $("#G1121_C17120").select2();

    $("#G1121_C18885").select2();

    $("#G1121_C18886").select2();

        $("#G1121_C18887").select2({
        	placeholder: "Buscar",
	        allowClear: false,
	        minimumInputLength: 3,
	        ajax: {
	            url: '<?=$url_crud;?>?CallDatosCombo_Guion_G1121_C18887=si',
	            dataType: 'json',
	            type : 'post',
	            delay: 250,
	            data: function (params) {
	                return {
	                    q: params.term
	                };
	            },
              	processResults: function(data) {
				  	return {
				    	results: $.map(data, function(obj) {
				      		return {
				        		id: obj.id,
				        		text: obj.text
				      		};
				    	})
				  	};
				},
	            cache: true
	        }
    	});            
        
        $("#G1121_C18887").change(function(){
            $.ajax({
        		url   : '<?php echo $url_crud;?>',
        		data  : { dameValoresCamposDinamicos_Guion_G1121_C18887 : $(this).val() },
        		type  : 'post',
        		dataType : 'json',
        		success  : function(data){
        				
        		}
        	});
        });
                                      

    $("#G1121_C21551").select2();

    $("#G1121_C21552").select2();

    $("#G1121_C21553").select2();

    $("#G1121_C21555").select2();

    $("#G1121_C17639").select2();

    $("#G1121_C17640").select2();

    $("#G1121_C17641").select2();

    $("#G1121_C17642").select2();
        //datepickers
        

        $("#G1121_C17647").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1121_C17646").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1121_C21557").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1121_C21560").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1121_C21562").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora de lectura de contrato', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1121_C21561").wickedpicker(options);

        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora de activación', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1121_C21563").wickedpicker(options);

        //Validaciones numeros Enteros
        

    	$("#G1121_C20659").numeric();
		        
    	$("#G1121_C17117").numeric();
		        
    	$("#G1121_C17644").numeric();
		        
    	$("#G1121_C18888").numeric();
		        
    	$("#G1121_C18889").numeric();
		        

        //Validaciones numeros Decimales
        

        $("#G1121_C21554").numeric({ decimal : ".",  negative : false, scale: 4 });
		        
        $("#G1121_C21556").numeric({ decimal : ".",  negative : false, scale: 4 });
		        

        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO CLIENTE 

    $("#G1121_C17645").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ESTADO_DY 

    $("#G1121_C17120").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Es o ha sido cliente de Fincaraiz ? 

    $("#G1121_C18885").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Lleva algún proceso o tiene alguna propuesta con otro asesor de Fincaraiz ? 

    $("#G1121_C18886").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Pack 

    $("#G1121_C21551").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Cupo 

    $("#G1121_C21552").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Tiempo 

    $("#G1121_C21553").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Descuento 

    $("#G1121_C21555").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ESTADO 

    $("#G1121_C17639").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para TIPIFICACION 1 

    $("#G1121_C17640").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para TIPIFICACION 2 

    $("#G1121_C17641").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para TIPIFICACION 3 

    $("#G1121_C17642").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
        	$("#Save").attr('disabled', true);
        	var bol_respuesta = before_save();
        	var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
            	alertify.error("Es necesaria la tipificación!");
            	valido = 1;
            }

            if($(".reintento").val() == '2'){
            	if($(".TxtFechaReintento").val().length < 1){
            		alertify.error("Es necesario llenar la fecha de reintento!");
            		$(".TxtFechaReintento").focus();
            		valido = 1;
            	}

            	if($(".TxtHoraReintento").val().length < 1){
            		alertify.error("Es necesario llenar la hora de reintento!");
            		$(".TxtHoraReintento").focus();
            		valido = 1;
            	}
            }

            if(valido == '0'){
	        	if(bol_respuesta){            
		            var form = $("#FormularioDatos");
		            //Se crean un array con los datos a enviar, apartir del formulario 
		            var formData = new FormData($("#FormularioDatos")[0]);
		            $.ajax({
		               url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
		                type: 'POST',
		                data: formData,
		                cache: false,
		                contentType: false,
		                processData: false,
		                //una vez finalizado correctamente
		                success: function(data){
		                    if(data != '0'){
		                    	<?php if(!isset($_GET['campan'])){ ?>
			                        //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
			                        if($("#oper").val() == 'add'){
			                            idTotal = data;
			                        }else{
			                            idTotal= $("#hidId").val();
			                        }
			                       
			                        //Limpiar formulario
			                        form[0].reset();
			                        after_save();
			                        <?php if(isset($_GET['registroId'])){ ?>
			                        $.ajax({
			                            url      : '<?=$url_crud;?>',
			                            type     : 'POST',
			                            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
			                            dataType : 'json',
			                            success  : function(data){
			                                //recorrer datos y enviarlos al formulario
			                                $.each(data, function(i, item) {
		                                    
 
		                                    	$("#G1121_C20659").val(item.G1121_C20659);
 
		                                    	$("#G1121_C17106").val(item.G1121_C17106);
 
		                                    	$("#G1121_C17107").val(item.G1121_C17107);
 
		                                    	$("#G1121_C17108").val(item.G1121_C17108);
 
		                                    	$("#G1121_C17643").val(item.G1121_C17643);
 
		                                    	$("#G1121_C20660").val(item.G1121_C20660);
 
		                                    	$("#G1121_C20688").val(item.G1121_C20688);
 
		                                    	$("#G1121_C17109").val(item.G1121_C17109);
 
		                                    	$("#G1121_C17110").val(item.G1121_C17110);
 
		                                    	$("#G1121_C17111").val(item.G1121_C17111);
 
		                                    	$("#G1121_C17112").val(item.G1121_C17112);
 
		                                    	$("#G1121_C17113").val(item.G1121_C17113);
 
		                                    	$("#G1121_C17114").val(item.G1121_C17114);
 
		                                    	$("#G1121_C17115").val(item.G1121_C17115);
 
		                                    	$("#G1121_C17116").val(item.G1121_C17116);
 
		                                    	$("#G1121_C17117").val(item.G1121_C17117);
 
		                                    	$("#G1121_C17644").val(item.G1121_C17644);
 
		                                    	$("#G1121_C17645").val(item.G1121_C17645);
 
		                                    	$("#G1121_C17647").val(item.G1121_C17647);
 
		                                    	$("#G1121_C17646").val(item.G1121_C17646);
 
		                                    	$("#G1121_C20335").val(item.G1121_C20335);
 
		                                    	$("#G1121_C17118").val(item.G1121_C17118);
 
		                                    	$("#G1121_C17119").val(item.G1121_C17119);
 
		                                    	$("#G1121_C17120").val(item.G1121_C17120);
 
		                                    	$("#G1121_C17639").val(item.G1121_C17639);
 
		                                    	$("#G1121_C17640").val(item.G1121_C17640);
 
		                                    	$("#G1121_C17641").val(item.G1121_C17641);
 
		                                    	$("#G1121_C17642").val(item.G1121_C17642);
 
		                                    	$("#G1121_C22653").val(item.G1121_C22653);
 
		                                    	$("#G1121_C18885").val(item.G1121_C18885);
 
		                                    	$("#G1121_C18886").val(item.G1121_C18886);
 
		                                    	$("#G1121_C18887").val(item.G1121_C18887);

		                                    	$("#G1121_C18887").val(item.G1121_C18887).trigger("change"); 
 
		                                    	$("#G1121_C18888").val(item.G1121_C18888);
 
		                                    	$("#G1121_C18889").val(item.G1121_C18889);
 
		                                    	$("#G1121_C18890").val(item.G1121_C18890);
 
		                                    	$("#G1121_C20690").val(item.G1121_C20690);
 
		                                    	$("#G1121_C21549").val(item.G1121_C21549);
 
		                                    	$("#G1121_C21550").val(item.G1121_C21550);
 
		                                    	$("#G1121_C21551").val(item.G1121_C21551);
 
		                                    	$("#G1121_C21552").val(item.G1121_C21552);
 
		                                    	$("#G1121_C21553").val(item.G1121_C21553);
 
		                                    	$("#G1121_C21554").val(item.G1121_C21554);
 
		                                    	$("#G1121_C21555").val(item.G1121_C21555);
 
		                                    	$("#G1121_C21556").val(item.G1121_C21556);
 
		                                    	$("#G1121_C21557").val(item.G1121_C21557);
 
		                                    	$("#G1121_C21558").val(item.G1121_C21558);
 
		                                    	$("#G1121_C21559").val(item.G1121_C21559);
 
		                                    	$("#G1121_C21560").val(item.G1121_C21560);
 
		                                    	$("#G1121_C21561").val(item.G1121_C21561);
 
		                                    	$("#G1121_C21562").val(item.G1121_C21562);
 
		                                    	$("#G1121_C21563").val(item.G1121_C21563);
		              							$("#h3mio").html(item.principal);
			                                });

			                                //Deshabilitar los campos

			                                //Habilitar todos los campos para edicion
			                                $('#FormularioDatos :input').each(function(){
			                                    $(this).attr('disabled', true);
			                                });

			                                //Habilidar los botones de operacion, add, editar, eliminar
			                                $("#add").attr('disabled', false);
			                                $("#edit").attr('disabled', false);
			                                $("#delete").attr('disabled', false);

			                                //Desahabiliatra los botones de salvar y seleccionar_registro
			                                $("#cancel").attr('disabled', true);
			                                $("#Save").attr('disabled', true);
			                            } 
			                        })
			                        $("#hidId").val(<?php echo $_GET['registroId'];?>);
			                        <?php } else { ?>
			                            llenar_lista_navegacion('');
			                        <?php } ?>   

		                        <?php }else{ 
		                        	if(!isset($_GET['formulario'])){
		                        ?>

		                        	$.ajax({
		                        		url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
		                        		type  : "post",
		                        		data  : formData,
		                    		 	cache: false,
					                    contentType: false,
					                    processData: false,
		                        		success : function(xt){
		                        			console.log(xt);
		                        			window.location.href = "quitar.php";
		                        		}
		                        	});
		                        	
				
		                        <?php } 
		                        	}
		                        ?>            
		                    }else{
		                        //Algo paso, hay un error
		                        $("#Save").attr('disabled', false);
		                        alertify.error('Un error ha ocurrido y no pudimos guardar la información');
		                    }                
		                },
		                //si ha ocurrido un error
		                error: function(){
		                    after_save_error();
		                    $("#Save").attr('disabled', false);
		                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
		                }
		            });
          		}
          	}
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','CODIGO CLIENTE','RAZON SOCIAL','NOMBRE COMERCIAL','CEDULA NIT','CONTACTO COMERCIAL','ID CONTACTO COMERCIAL','Cuidad de expedición ','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','CALIFICACIÓN ','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','ESTADO','TIPIFICACION 1','TIPIFICACION 2','TIPIFICACION 3','Agente','Es o ha sido cliente de Fincaraiz ?','Lleva algún proceso o tiene alguna propuesta con otro asesor de Fincaraiz ?','Desde que ciudad realiza la administración de los inmuebles ?','Cuantos inmuebles USADOS tiene en su inventario ?','Cuantos inmuebles NUEVOS tiene en su inventario ?','Cupo autorizado','COMENTARIOS BACKOFFICE ','Cupo Usado','Cupo Nuevo','Pack','Cupo','Tiempo','Precio full sin IVA','Descuento','Valor con dto sin IVA','Vigencia','Obsequio','Observación','Fecha de lectura de contrato','Hora de lectura de contrato','Fecha de activación','Hora de activación'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
	                ,
	                {  
	                    name:'G1121_C20659', 
	                    index:'G1121_C20659', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1121_C17106', 
	                    index: 'G1121_C17106', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17107', 
	                    index: 'G1121_C17107', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17108', 
	                    index: 'G1121_C17108', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17643', 
	                    index: 'G1121_C17643', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C20660', 
	                    index: 'G1121_C20660', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C20688', 
	                    index: 'G1121_C20688', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17109', 
	                    index: 'G1121_C17109', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17110', 
	                    index: 'G1121_C17110', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17111', 
	                    index: 'G1121_C17111', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17112', 
	                    index: 'G1121_C17112', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17113', 
	                    index: 'G1121_C17113', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17114', 
	                    index: 'G1121_C17114', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17115', 
	                    index: 'G1121_C17115', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17116', 
	                    index: 'G1121_C17116', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1121_C17117', 
	                    index:'G1121_C17117', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }
 
	                ,
	                {  
	                    name:'G1121_C17644', 
	                    index:'G1121_C17644', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1121_C17645', 
	                    index:'G1121_C17645', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1121_C17645'
	                    }
	                }

	                ,
	                {  
	                    name:'G1121_C17647', 
	                    index:'G1121_C17647', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1121_C17646', 
	                    index:'G1121_C17646', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1121_C20335', 
	                    index: 'G1121_C20335', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17118', 
	                    index: 'G1121_C17118', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17119', 
	                    index: 'G1121_C17119', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C17120', 
	                    index:'G1121_C17120', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=825&campo=G1121_C17120'
	                    }
	                }

	                ,
	                { 
	                    name:'G1121_C17639', 
	                    index:'G1121_C17639', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=828&campo=G1121_C17639'
	                    }
	                }

	                ,
	                { 
	                    name:'G1121_C17640', 
	                    index:'G1121_C17640', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=829&campo=G1121_C17640'
	                    }
	                }

	                ,
	                { 
	                    name:'G1121_C17641', 
	                    index:'G1121_C17641', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=830&campo=G1121_C17641'
	                    }
	                }

	                ,
	                { 
	                    name:'G1121_C17642', 
	                    index:'G1121_C17642', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=831&campo=G1121_C17642'
	                    }
	                }

	                ,
	                { 
	                    name:'G1121_C22653', 
	                    index: 'G1121_C22653', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C18885', 
	                    index:'G1121_C18885', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1121_C18885'
	                    }
	                }

	                ,
	                { 
	                    name:'G1121_C18886', 
	                    index:'G1121_C18886', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1121_C18886'
	                    }
	                }

	                ,
	                { 
	                    name:'G1121_C18887', 
	                    index:'G1121_C18887', 
	                    width:300 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1121_C18887=si',
	                        dataInit:function(el){
	                        	$(el).select2();
	                            /*$(el).select2({ 
	                                templateResult: function(data) {
	                                    var r = data.text.split('|');
	                                    var row = '<div class="row">';
	                                    var totalRows = 12 / r.length;
	                                    for(i= 0; i < r.length; i++){
	                                        row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
	                                    }
	                                    row += '</div>';
	                                    var $result = $(row);
	                                    return $result;
	                                },
	                                templateSelection : function(data){
	                                    var r = data.text.split('|');
	                                    return r[0];
	                                }
	                            });*/
	                            $(el).change(function(){
	                                var valores = $(el + " option:selected").attr("llenadores");
	                                var campos =  $(el + " option:selected").attr("dinammicos");
	                                var r = valores.split('|');
	                                if(r.length > 1){

	                                    var c = campos.split('|');
	                                    for(i = 1; i < r.length; i++){
	                                        $("#"+ rowid +"_"+c[i]).val(r[i]);
	                                    }
	                                }
	                            });
	                        }
	                    }
	                }
 
	                ,
	                {  
	                    name:'G1121_C18888', 
	                    index:'G1121_C18888', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }
 
	                ,
	                {  
	                    name:'G1121_C18889', 
	                    index:'G1121_C18889', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1121_C18890', 
	                    index: 'G1121_C18890', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C20690', 
	                    index:'G1121_C20690', 
	                    width:150, 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C21549', 
	                    index: 'G1121_C21549', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C21550', 
	                    index: 'G1121_C21550', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C21551', 
	                    index:'G1121_C21551', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=978&campo=G1121_C21551'
	                    }
	                }

	                ,
	                { 
	                    name:'G1121_C21552', 
	                    index:'G1121_C21552', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=977&campo=G1121_C21552'
	                    }
	                }

	                ,
	                { 
	                    name:'G1121_C21553', 
	                    index:'G1121_C21553', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=979&campo=G1121_C21553'
	                    }
	                }

	                ,
	                {  
	                    name:'G1121_C21554', 
	                    index:'G1121_C21554', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1121_C21555', 
	                    index:'G1121_C21555', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=980&campo=G1121_C21555'
	                    }
	                }

	                ,
	                {  
	                    name:'G1121_C21556', 
	                    index:'G1121_C21556', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
	                        }
	                    } 
	                }

	                ,
	                {  
	                    name:'G1121_C21557', 
	                    index:'G1121_C21557', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1121_C21558', 
	                    index: 'G1121_C21558', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1121_C21559', 
	                    index:'G1121_C21559', 
	                    width:150, 
	                    editable: true 
	                }

	                ,
	                {  
	                    name:'G1121_C21560', 
	                    index:'G1121_C21560', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1121_C21561', 
	                    index:'G1121_C21561', 
	                    width:70 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            //Timepicker
	                            var options = { 
	                                now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
	                                twentyFour: true, //Display 24 hour format, defaults to false
	                                title: 'Hora de lectura de contrato', //The Wickedpicker's title,
	                                showSeconds: true, //Whether or not to show seconds,
	                                secondsInterval: 1, //Change interval for seconds, defaults to 1
	                                minutesInterval: 1, //Change interval for minutes, defaults to 1
	                                beforeShow: null, //A function to be called before the Wickedpicker is shown
	                                show: null, //A function to be called when the Wickedpicker is shown
	                                clearable: false, //Make the picker's input clearable (has clickable "x")
	                            }; 
	                            $(el).wickedpicker(options);
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1121_C21562', 
	                    index:'G1121_C21562', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1121_C21563', 
	                    index:'G1121_C21563', 
	                    width:70 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            //Timepicker
	                            var options = { 
	                                now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
	                                twentyFour: true, //Display 24 hour format, defaults to false
	                                title: 'Hora de activación', //The Wickedpicker's title,
	                                showSeconds: true, //Whether or not to show seconds,
	                                secondsInterval: 1, //Change interval for seconds, defaults to 1
	                                minutesInterval: 1, //Change interval for minutes, defaults to 1
	                                beforeShow: null, //A function to be called before the Wickedpicker is shown
	                                show: null, //A function to be called when the Wickedpicker is shown
	                                clearable: false, //Make the picker's input clearable (has clickable "x")
	                            }; 
	                            $(el).wickedpicker(options);
	                        }
	                    }
	                }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    

                    $("#"+ rowid +"_G1121_C18887").change(function(){
                        var valores = $("#"+ rowid +"_G1121_C18887 option:selected").attr("llenadores");
                        var campos = $("#"+ rowid +"_G1121_C18887 option:selected").attr("dinammicos");
                        var r = valores.split('|');

                        if(r.length > 1){

                            var c = campos.split('|');
                            for(i = 1; i < r.length; i++){
                                if(!$("#"+c[i]).is("select")) {
                                // the input field is not a select
                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                }else{
                                    var change = r[i].replace(' ', ''); 
                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                }
                                
                            }
                        }
                    });
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1121_C17106',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(x){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', Busqueda : x},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');
            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G1121_C20659").val(item.G1121_C20659);

                        $("#G1121_C17106").val(item.G1121_C17106);

                        $("#G1121_C17107").val(item.G1121_C17107);

                        $("#G1121_C17108").val(item.G1121_C17108);

                        $("#G1121_C17643").val(item.G1121_C17643);

                        $("#G1121_C20660").val(item.G1121_C20660);

                        $("#G1121_C20688").val(item.G1121_C20688);

                        $("#G1121_C17109").val(item.G1121_C17109);

                        $("#G1121_C17110").val(item.G1121_C17110);

                        $("#G1121_C17111").val(item.G1121_C17111);

                        $("#G1121_C17112").val(item.G1121_C17112);

                        $("#G1121_C17113").val(item.G1121_C17113);

                        $("#G1121_C17114").val(item.G1121_C17114);

                        $("#G1121_C17115").val(item.G1121_C17115);

                        $("#G1121_C17116").val(item.G1121_C17116);

                        $("#G1121_C17117").val(item.G1121_C17117);

                        $("#G1121_C17644").val(item.G1121_C17644);

                        $("#G1121_C17645").val(item.G1121_C17645);
 
        	            $("#G1121_C17645").val(item.G1121_C17645).trigger("change"); 

                        $("#G1121_C17647").val(item.G1121_C17647);

                        $("#G1121_C17646").val(item.G1121_C17646);

                        $("#G1121_C20335").val(item.G1121_C20335);

                        $("#G1121_C17118").val(item.G1121_C17118);

                        $("#G1121_C17119").val(item.G1121_C17119);

                        $("#G1121_C17120").val(item.G1121_C17120);
 
        	            $("#G1121_C17120").val(item.G1121_C17120).trigger("change"); 

                        $("#G1121_C17639").val(item.G1121_C17639);
 
        	            $("#G1121_C17639").val(item.G1121_C17639).trigger("change"); 

                        $("#G1121_C17640").val(item.G1121_C17640);
 
        	            $("#G1121_C17640").val(item.G1121_C17640).trigger("change"); 

                        $("#G1121_C17641").val(item.G1121_C17641);
 
        	            $("#G1121_C17641").val(item.G1121_C17641).trigger("change"); 

                        $("#G1121_C17642").val(item.G1121_C17642);
 
        	            $("#G1121_C17642").val(item.G1121_C17642).trigger("change"); 

                        $("#G1121_C22653").val(item.G1121_C22653);

                        $("#G1121_C18885").val(item.G1121_C18885);
 
        	            $("#G1121_C18885").val(item.G1121_C18885).trigger("change"); 

                        $("#G1121_C18886").val(item.G1121_C18886);
 
        	            $("#G1121_C18886").val(item.G1121_C18886).trigger("change"); 

                        $("#G1121_C18887").val(item.G1121_C18887);
 
        	            $("#G1121_C18887").val(item.G1121_C18887).trigger("change"); 

                        $("#G1121_C18888").val(item.G1121_C18888);

                        $("#G1121_C18889").val(item.G1121_C18889);

                        $("#G1121_C18890").val(item.G1121_C18890);

                        $("#G1121_C20690").val(item.G1121_C20690);

                        $("#G1121_C21549").val(item.G1121_C21549);

                        $("#G1121_C21550").val(item.G1121_C21550);

                        $("#G1121_C21551").val(item.G1121_C21551);
 
        	            $("#G1121_C21551").val(item.G1121_C21551).trigger("change"); 

                        $("#G1121_C21552").val(item.G1121_C21552);
 
        	            $("#G1121_C21552").val(item.G1121_C21552).trigger("change"); 

                        $("#G1121_C21553").val(item.G1121_C21553);
 
        	            $("#G1121_C21553").val(item.G1121_C21553).trigger("change"); 

                        $("#G1121_C21554").val(item.G1121_C21554);

                        $("#G1121_C21555").val(item.G1121_C21555);
 
        	            $("#G1121_C21555").val(item.G1121_C21555).trigger("change"); 

                        $("#G1121_C21556").val(item.G1121_C21556);

                        $("#G1121_C21557").val(item.G1121_C21557);

                        $("#G1121_C21558").val(item.G1121_C21558);

                        $("#G1121_C21559").val(item.G1121_C21559);

                        $("#G1121_C21560").val(item.G1121_C21560);

                        $("#G1121_C21561").val(item.G1121_C21561);

                        $("#G1121_C21562").val(item.G1121_C21562);

                        $("#G1121_C21563").val(item.G1121_C21563);
        				$("#h3mio").html(item.principal);
        				
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
	$(document).ready(function() {
		<?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                		while($objDatos = $resultDatosSql->fetch_object()){ ?>
                        	document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
             		<?php  
             			}	
                    }
                    
                } */  
            }
        ?>
    	<?php if(isset($_GET['user'])){ ?>
        	
        	idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
		
	});
</script>
