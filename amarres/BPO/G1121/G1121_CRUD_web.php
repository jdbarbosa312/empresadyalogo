<?php
    session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
    
    if(isset($_POST['getListaHija'])){

        $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
        //echo $Lsql;
        $res = $mysqli->query($Lsql);
        echo "<option value='0'>Seleccione</option>";
        while($key = $res->fetch_object()){
            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
        }
    }   

    //Inserciones o actualizaciones
    if(isset($_POST["oper"])){
        $str_Lsql  = '';

        $validar = 0;
        $str_LsqlU = "UPDATE ".$BaseDatos.".G1121 SET "; 
        $str_LsqlI = "INSERT INTO ".$BaseDatos.".G1121( G1121_FechaInsercion ,";
        $str_LsqlV = " VALUES ('".date('Y-m-d H:s:i')."',"; 
  
        $G1121_C20659 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1121_C20659"])){
            if($_POST["G1121_C20659"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G1121_C20659 = $_POST["G1121_C20659"];
                $G1121_C20659 = str_replace(".", "", $_POST["G1121_C20659"]);
                $G1121_C20659 =  str_replace(",", ".", $G1121_C20659);
                $str_LsqlU .= $separador." G1121_C20659 = '".$G1121_C20659."'";
                $str_LsqlI .= $separador." G1121_C20659";
                $str_LsqlV .= $separador."'".$G1121_C20659."'";
                $validar = 1;
            }
        }

         if(isset($_POST["G1121_UsuarioUG_b"]) && $_POST["G1121_UsuarioUG_b"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_UsuarioUG_b = '".$_POST["G1121_UsuarioUG_b"]."'";
            $str_LsqlI .= $separador."G1121_UsuarioUG_b";
            $str_LsqlV .= $separador."'".$_POST["G1121_UsuarioUG_b"]."'";
            $validar = 1;
        }
        if(isset($_POST["G1121_UsuarioUG_b"]) && $_POST["G1121_UsuarioUG_b"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_UsuarioGMI_b = '".$_POST["G1121_UsuarioUG_b"]."'";
            $str_LsqlI .= $separador."G1121_UsuarioGMI_b";
            $str_LsqlV .= $separador."'".$_POST["G1121_UsuarioUG_b"]."'";
            $validar = 1;
        }
  
        if(isset($_POST["G1121_C17106"]) && $_POST["G1121_C17106"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17106 = '".$_POST["G1121_C17106"]."'";
            $str_LsqlI .= $separador."G1121_C17106";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17106"]."'";
            $validar = 1;
        }         
  
        if(isset($_POST["G1121_C17107"]) && $_POST["G1121_C17107"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17107 = '".$_POST["G1121_C17107"]."'";
            $str_LsqlI .= $separador."G1121_C17107";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17107"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17108"]) && $_POST["G1121_C17108"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17108 = '".$_POST["G1121_C17108"]."'";
            $str_LsqlI .= $separador."G1121_C17108";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17108"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17643"]) && $_POST["G1121_C17643"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17643 = '".$_POST["G1121_C17643"]."'";
            $str_LsqlI .= $separador."G1121_C17643";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17643"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C20660"]) && $_POST["G1121_C20660"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C20660 = '".$_POST["G1121_C20660"]."'";
            $str_LsqlI .= $separador."G1121_C20660";
            $str_LsqlV .= $separador."'".$_POST["G1121_C20660"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C20688"]) && $_POST["G1121_C20688"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C20688 = '".$_POST["G1121_C20688"]."'";
            $str_LsqlI .= $separador."G1121_C20688";
            $str_LsqlV .= $separador."'".$_POST["G1121_C20688"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17109"]) && $_POST["G1121_C17109"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17109 = '".$_POST["G1121_C17109"]."'";
            $str_LsqlI .= $separador."G1121_C17109";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17109"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17110"]) && $_POST["G1121_C17110"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17110 = '".$_POST["G1121_C17110"]."'";
            $str_LsqlI .= $separador."G1121_C17110";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17110"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17111"]) && $_POST["G1121_C17111"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17111 = '".$_POST["G1121_C17111"]."'";
            $str_LsqlI .= $separador."G1121_C17111";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17111"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17112"]) && $_POST["G1121_C17112"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17112 = '".$_POST["G1121_C17112"]."'";
            $str_LsqlI .= $separador."G1121_C17112";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17112"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17113"]) && $_POST["G1121_C17113"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17113 = '".$_POST["G1121_C17113"]."'";
            $str_LsqlI .= $separador."G1121_C17113";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17113"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17114"]) && $_POST["G1121_C17114"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17114 = '".$_POST["G1121_C17114"]."'";
            $str_LsqlI .= $separador."G1121_C17114";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17114"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17115"]) && $_POST["G1121_C17115"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17115 = '".$_POST["G1121_C17115"]."'";
            $str_LsqlI .= $separador."G1121_C17115";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17115"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17116"]) && $_POST["G1121_C17116"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17116 = '".$_POST["G1121_C17116"]."'";
            $str_LsqlI .= $separador."G1121_C17116";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17116"]."'";
            $validar = 1;
        }
         
  
        $G1121_C17117 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1121_C17117"])){
            if($_POST["G1121_C17117"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G1121_C17117 = $_POST["G1121_C17117"];
                $G1121_C17117 = str_replace(".", "", $_POST["G1121_C17117"]);
                $G1121_C17117 =  str_replace(",", ".", $G1121_C17117);
                $str_LsqlU .= $separador." G1121_C17117 = '".$G1121_C17117."'";
                $str_LsqlI .= $separador." G1121_C17117";
                $str_LsqlV .= $separador."'".$G1121_C17117."'";
                $validar = 1;
            }
        }
  
        $G1121_C17644 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1121_C17644"])){
            if($_POST["G1121_C17644"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G1121_C17644 = $_POST["G1121_C17644"];
                $G1121_C17644 = str_replace(".", "", $_POST["G1121_C17644"]);
                $G1121_C17644 =  str_replace(",", ".", $G1121_C17644);
                $str_LsqlU .= $separador." G1121_C17644 = '".$G1121_C17644."'";
                $str_LsqlI .= $separador." G1121_C17644";
                $str_LsqlV .= $separador."'".$G1121_C17644."'";
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1121_C17645"]) && $_POST["G1121_C17645"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17645 = '".$_POST["G1121_C17645"]."'";
            $str_LsqlI .= $separador."G1121_C17645";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17645"]."'";
            $validar = 1;
        }
         
 
        $G1121_C17647 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1121_C17647"])){    
            if($_POST["G1121_C17647"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1121_C17647 = "'".str_replace(' ', '',$_POST["G1121_C17647"])." 00:00:00'";
                $str_LsqlU .= $separador." G1121_C17647 = ".$G1121_C17647;
                $str_LsqlI .= $separador." G1121_C17647";
                $str_LsqlV .= $separador.$G1121_C17647;
                $validar = 1;
            }
        }
 
        $G1121_C17646 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1121_C17646"])){    
            if($_POST["G1121_C17646"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1121_C17646 = "'".str_replace(' ', '',$_POST["G1121_C17646"])." 00:00:00'";
                $str_LsqlU .= $separador." G1121_C17646 = ".$G1121_C17646;
                $str_LsqlI .= $separador." G1121_C17646";
                $str_LsqlV .= $separador.$G1121_C17646;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1121_C20335"]) && $_POST["G1121_C20335"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C20335 = '".$_POST["G1121_C20335"]."'";
            $str_LsqlI .= $separador."G1121_C20335";
            $str_LsqlV .= $separador."'".$_POST["G1121_C20335"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17120"]) && $_POST["G1121_C17120"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17120 = '".$_POST["G1121_C17120"]."'";
            $str_LsqlI .= $separador."G1121_C17120";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17120"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17639"]) && $_POST["G1121_C17639"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17639 = '".$_POST["G1121_C17639"]."'";
            $str_LsqlI .= $separador."G1121_C17639";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17639"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17640"]) && $_POST["G1121_C17640"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17640 = '".$_POST["G1121_C17640"]."'";
            $str_LsqlI .= $separador."G1121_C17640";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17640"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17641"]) && $_POST["G1121_C17641"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17641 = '".$_POST["G1121_C17641"]."'";
            $str_LsqlI .= $separador."G1121_C17641";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17641"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C17642"]) && $_POST["G1121_C17642"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C17642 = '".$_POST["G1121_C17642"]."'";
            $str_LsqlI .= $separador."G1121_C17642";
            $str_LsqlV .= $separador."'".$_POST["G1121_C17642"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C22653"]) && $_POST["G1121_C22653"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C22653 = '".$_POST["G1121_C22653"]."'";
            $str_LsqlI .= $separador."G1121_C22653";
            $str_LsqlV .= $separador."'".$_POST["G1121_C22653"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C18885"]) && $_POST["G1121_C18885"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C18885 = '".$_POST["G1121_C18885"]."'";
            $str_LsqlI .= $separador."G1121_C18885";
            $str_LsqlV .= $separador."'".$_POST["G1121_C18885"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C18886"]) && $_POST["G1121_C18886"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C18886 = '".$_POST["G1121_C18886"]."'";
            $str_LsqlI .= $separador."G1121_C18886";
            $str_LsqlV .= $separador."'".$_POST["G1121_C18886"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C18887"]) && $_POST["G1121_C18887"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C18887 = '".$_POST["G1121_C18887"]."'";
            $str_LsqlI .= $separador."G1121_C18887";
            $str_LsqlV .= $separador."'".$_POST["G1121_C18887"]."'";
            $validar = 1;
        }
         
  
        $G1121_C18888 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1121_C18888"])){
            if($_POST["G1121_C18888"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G1121_C18888 = $_POST["G1121_C18888"];
                $G1121_C18888 = str_replace(".", "", $_POST["G1121_C18888"]);
                $G1121_C18888 =  str_replace(",", ".", $G1121_C18888);
                $str_LsqlU .= $separador." G1121_C18888 = '".$G1121_C18888."'";
                $str_LsqlI .= $separador." G1121_C18888";
                $str_LsqlV .= $separador."'".$G1121_C18888."'";
                $validar = 1;
            }
        }
  
        $G1121_C18889 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1121_C18889"])){
            if($_POST["G1121_C18889"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G1121_C18889 = $_POST["G1121_C18889"];
                $G1121_C18889 = str_replace(".", "", $_POST["G1121_C18889"]);
                $G1121_C18889 =  str_replace(",", ".", $G1121_C18889);
                $str_LsqlU .= $separador." G1121_C18889 = '".$G1121_C18889."'";
                $str_LsqlI .= $separador." G1121_C18889";
                $str_LsqlV .= $separador."'".$G1121_C18889."'";
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1121_C18890"]) && $_POST["G1121_C18890"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C18890 = '".$_POST["G1121_C18890"]."'";
            $str_LsqlI .= $separador."G1121_C18890";
            $str_LsqlV .= $separador."'".$_POST["G1121_C18890"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C20690"]) && $_POST["G1121_C20690"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C20690 = '".$_POST["G1121_C20690"]."'";
            $str_LsqlI .= $separador."G1121_C20690";
            $str_LsqlV .= $separador."'".$_POST["G1121_C20690"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C21549"]) && $_POST["G1121_C21549"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C21549 = '".$_POST["G1121_C21549"]."'";
            $str_LsqlI .= $separador."G1121_C21549";
            $str_LsqlV .= $separador."'".$_POST["G1121_C21549"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C21550"]) && $_POST["G1121_C21550"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C21550 = '".$_POST["G1121_C21550"]."'";
            $str_LsqlI .= $separador."G1121_C21550";
            $str_LsqlV .= $separador."'".$_POST["G1121_C21550"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C21551"]) && $_POST["G1121_C21551"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C21551 = '".$_POST["G1121_C21551"]."'";
            $str_LsqlI .= $separador."G1121_C21551";
            $str_LsqlV .= $separador."'".$_POST["G1121_C21551"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C21552"]) && $_POST["G1121_C21552"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C21552 = '".$_POST["G1121_C21552"]."'";
            $str_LsqlI .= $separador."G1121_C21552";
            $str_LsqlV .= $separador."'".$_POST["G1121_C21552"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C21553"]) && $_POST["G1121_C21553"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C21553 = '".$_POST["G1121_C21553"]."'";
            $str_LsqlI .= $separador."G1121_C21553";
            $str_LsqlV .= $separador."'".$_POST["G1121_C21553"]."'";
            $validar = 1;
        }
         
  
        $G1121_C21554 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1121_C21554"])){
            if($_POST["G1121_C21554"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                
                $G1121_C21554 = str_replace(".", "", $_POST["G1121_C21554"]);
                $G1121_C21554 =  str_replace(",", ".", $G1121_C21554);
                $str_LsqlU .= $separador." G1121_C21554 = '".$G1121_C21554."'";
                $str_LsqlI .= $separador." G1121_C21554";
                $str_LsqlV .= $separador."'".$G1121_C21554."'";
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1121_C21555"]) && $_POST["G1121_C21555"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C21555 = '".$_POST["G1121_C21555"]."'";
            $str_LsqlI .= $separador."G1121_C21555";
            $str_LsqlV .= $separador."'".$_POST["G1121_C21555"]."'";
            $validar = 1;
        }
         
  
        $G1121_C21556 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1121_C21556"])){
            if($_POST["G1121_C21556"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                
                $G1121_C21556 = str_replace(".", "", $_POST["G1121_C21556"]);
                $G1121_C21556 =  str_replace(",", ".", $G1121_C21556);
                $str_LsqlU .= $separador." G1121_C21556 = '".$G1121_C21556."'";
                $str_LsqlI .= $separador." G1121_C21556";
                $str_LsqlV .= $separador."'".$G1121_C21556."'";
                $validar = 1;
            }
        }
 
        $G1121_C21557 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1121_C21557"])){    
            if($_POST["G1121_C21557"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1121_C21557 = "'".str_replace(' ', '',$_POST["G1121_C21557"])." 00:00:00'";
                $str_LsqlU .= $separador." G1121_C21557 = ".$G1121_C21557;
                $str_LsqlI .= $separador." G1121_C21557";
                $str_LsqlV .= $separador.$G1121_C21557;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1121_C21558"]) && $_POST["G1121_C21558"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C21558 = '".$_POST["G1121_C21558"]."'";
            $str_LsqlI .= $separador."G1121_C21558";
            $str_LsqlV .= $separador."'".$_POST["G1121_C21558"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1121_C21559"]) && $_POST["G1121_C21559"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_C21559 = '".$_POST["G1121_C21559"]."'";
            $str_LsqlI .= $separador."G1121_C21559";
            $str_LsqlV .= $separador."'".$_POST["G1121_C21559"]."'";
            $validar = 1;
        }
         
 
        $G1121_C21560 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1121_C21560"])){    
            if($_POST["G1121_C21560"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1121_C21560 = "'".str_replace(' ', '',$_POST["G1121_C21560"])." 00:00:00'";
                $str_LsqlU .= $separador." G1121_C21560 = ".$G1121_C21560;
                $str_LsqlI .= $separador." G1121_C21560";
                $str_LsqlV .= $separador.$G1121_C21560;
                $validar = 1;
            }
        }
  
        $G1121_C21561 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
        if(isset($_POST["G1121_C21561"])){   
            if($_POST["G1121_C21561"] != '' && $_POST["G1121_C21561"] != 'undefined' && $_POST["G1121_C21561"] != 'null'){
                $separador = "";
                $fecha = date('Y-m-d');
                if($validar == 1){
                    $separador = ",";
                }

                $G1121_C21561 = "'".$fecha." ".str_replace(' ', '',$_POST["G1121_C21561"])."'";
                $str_LsqlU .= $separador." G1121_C21561 = ".$G1121_C21561."";
                $str_LsqlI .= $separador." G1121_C21561";
                $str_LsqlV .= $separador.$G1121_C21561;
                $validar = 1;
            }
        }
 
        $G1121_C21562 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1121_C21562"])){    
            if($_POST["G1121_C21562"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1121_C21562 = "'".str_replace(' ', '',$_POST["G1121_C21562"])." 00:00:00'";
                $str_LsqlU .= $separador." G1121_C21562 = ".$G1121_C21562;
                $str_LsqlI .= $separador." G1121_C21562";
                $str_LsqlV .= $separador.$G1121_C21562;
                $validar = 1;
            }
        }
  
        $G1121_C21563 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
        if(isset($_POST["G1121_C21563"])){   
            if($_POST["G1121_C21563"] != '' && $_POST["G1121_C21563"] != 'undefined' && $_POST["G1121_C21563"] != 'null'){
                $separador = "";
                $fecha = date('Y-m-d');
                if($validar == 1){
                    $separador = ",";
                }

                $G1121_C21563 = "'".$fecha." ".str_replace(' ', '',$_POST["G1121_C21563"])."'";
                $str_LsqlU .= $separador." G1121_C21563 = ".$G1121_C21563."";
                $str_LsqlI .= $separador." G1121_C21563";
                $str_LsqlV .= $separador.$G1121_C21563;
                $validar = 1;
            }
        }
 
        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $str_Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($str_Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G1121_C";
                $valorH = $valorG.$campo;
                $str_LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $str_LsqlI .= $separador." ".$valorH;
                $str_LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1121_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $str_LsqlI .= $separador."G1121_IdLlamada";
            $str_LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        if(isset($_POST['ORIGEN_DY_WF'])){
            $origen = $_POST['ORIGEN_DY_WF'];
            if($_POST['ORIGEN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $Origen = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 1121 AND PREGUN_Texto_____b = 'ORIGEN_DY_WF'";
                $res_Origen = $mysqli->query($Origen);
                if($res_Origen->num_rows > 0){
                    $dataOrigen = $res_Origen->fetch_array();

                    $str_LsqlU .= $separador."G1121_C".$dataOrigen['PREGUN_ConsInte__b']." = '".$_POST['ORIGEN_DY_WF']."'";
                    $str_LsqlI .= $separador."G1121_C".$dataOrigen['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador."'".$_POST['ORIGEN_DY_WF']."'";
                    $validar = 1;
                }
                

            }
        }else{
            $origen = '';
        }

        if(isset($_POST['OPTIN_DY_WF'])){
            if($_POST['OPTIN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $confirmado = null;
                if($_POST['OPTIN_DY_WF'] == 'SIMPLE'){
                    $confirmado  = "'CONFIRMADO'";
                }

                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 1121 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                if($res_OPTIN_DY_WF->num_rows > 0){
                    $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();

                    $str_LsqlU .= $separador."G1121_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = ".$confirmado;
                    $str_LsqlI .= $separador."G1121_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador." ".$confirmado;
                    $validar = 1;
                }
            }
        }


        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' ){
                
                $str_Lsql = $str_LsqlI.")" . $str_LsqlV.")";
            }
        }
        //si trae algo que insertar inserta
                       

        

        if($validar == 1){

            //JDBD - Validamos si el registro ya existe.
            $strSQLExiste_t = "SELECT COUNT(1) AS cantidad FROM ".$BaseDatos.".G1121 WHERE G1121_C17109 = '".$_POST["G1121_C17109"]."' OR G1121_C17109 LIKE '%".$_POST["G1121_C17109"]."%' OR G1121_C17114 = '".$_POST["G1121_C17114"]."' OR G1121_C17114 LIKE '%".$_POST["G1121_C17114"]."%'";

            $intVecesRegistrado_t = 0;

            if ($resSQLExiste_t = $mysqli->query($strSQLExiste_t)) {
                
                $objSQLExiste_t = $resSQLExiste_t->fetch_object();

                $intVecesRegistrado_t = $objSQLExiste_t->cantidad;

            }

            if ($intVecesRegistrado_t > 0) {

                header('Location:http://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=MTEyMQ==&result=2');

            }else{

                if ($mysqli->query($str_Lsql) === TRUE) {
                    $Agenda='2';
                    $fecha_actual = '2015-01-01 00:00:00';
                    $ultimoResgistroInsertado = $mysqli->insert_id;

                         if ($origen == "perfiles") {
                             $MuestraSql = "INSERT INTO ".$BaseDatos.".G1121_M1161 (
                                    G1121_M1161_CoInMiPo__b, 
                                    G1121_M1161_ConIntUsu_b, 
                                    G1121_M1161_FecUltGes_b,
                                    G1121_M1161_Estado____b,
                                    G1121_M1161_FecHorAge_b)                            
                                    VALUES (
                                    '".$ultimoResgistroInsertado."', 
                                    '".$_POST['G1121_UsuarioUG_b']."',
                                    '".date('Y-m-d H:s:i')."',
                                    '".$Agenda."',
                                    '".$fecha_actual."'
                                    
                                )";                         
                         }else{
                             $MuestraSql = "INSERT INTO ".$BaseDatos.".G1121_M679 (
                                    G1121_M679_CoInMiPo__b, 
                                    G1121_M679_ConIntUsu_b, 
                                    G1121_M679_FecUltGes_b,
                                    G1121_M679_Estado____b,
                                    G1121_M679_FecHorAge_b)                            
                                    VALUES (
                                    '".$ultimoResgistroInsertado."', 
                                    '".$_POST['G1121_UsuarioUG_b']."',
                                    '".date('Y-m-d H:s:i')."',
                                    '".$Agenda."',
                                    '".$fecha_actual."'
                                    
                                )";                        
                            } 
                            
                             $sql=$mysqli->query($MuestraSql);   
                                    

                        header('Location:http://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=MTEyMQ==&result=1');

                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }

            }

        }

    }
    


?>
