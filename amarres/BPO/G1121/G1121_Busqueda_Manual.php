
<?php
	//JDBD SE COMENTA LA CONEXION YA QUE INTERFIERE CON EL SISTEMA DE SEGURIDAD "/../../conexion.php");
	$http = "http://".$_SERVER["HTTP_HOST"];
	if (isset($_SERVER['HTTPS'])) {
	    $http = "https://".$_SERVER["HTTP_HOST"];
	}
?>

<script type="text/javascript">
	function autofitIframe(id){
		if (!window.opera && document.all && document.getElementById){
			id.style.height=id.contentWindow.document.body.scrollHeight;
		} else if(document.getElementById) {
			id.style.height=id.contentDocument.body.scrollHeight+"px";
		}
	}
</script>

<div class="row" id="buscador">
	<div class="col-md-1">&nbsp;</div>
	<div class="col-md-10">
		<div class="row">
			<form id="formId">
 
				<!-- CAMPO TIPO ENTERO -->
				<!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
				<!--<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1121_C20659" id="LblG1121_C20659">CODIGO CLIENTE</label>
					    <input type="text" class="form-control input-sm Numerico" onchange="clearNum(this)" name="G1121_C20659" id="G1121_C20659" placeholder="CODIGO CLIENTE" disabled>
					</div>
				</div> -->
				<!-- FIN DEL CAMPO TIPO ENTERO -->
				
				<!-- CAMPO TIPO TEXTO -->
  				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1121_ConsInte__b" id="LblG1121_ConsInte__b">Código Miembro</label>
					    <input type="text" class="form-control input-sm Numerico" onchange="clearNum(this)" name="G1121_ConsInte__b" id="G1121_ConsInte__b" placeholder="Código Miembro">
					</div>
				</div>	
				<!-- FIN DEL CAMPO TIPO ENTERO -->

				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1121_C17106" id="LblG1121_C17106">RAZON SOCIAL</label>
					    <input type="text" class="form-control input-sm" id="G1121_C17106" onchange="clearText(this)" name="G1121_C17106"  placeholder="RAZON SOCIAL">
					</div>
				</div>

				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1121_C17107" id="LblG1121_C17107">NOMBRE COMERCIAL</label>
					    <input type="text" class="form-control input-sm" id="G1121_C17107" onchange="clearText(this)" name="G1121_C17107"  placeholder="NOMBRE COMERCIAL">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1121_C17108" id="LblG1121_C17108">CEDULA NIT</label>
					    <input type="text" class="form-control input-sm" id="G1121_C17108" onchange="clearNum(this)" name="G1121_C17108"  placeholder="CEDULA NIT">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1121_C17109" id="LblG1121_C17109">TEL 1</label>
					    <input type="text" class="form-control input-sm" id="G1121_C17109" name="G1121_C17109" onchange="clearNum(this)"  placeholder="TEL 1">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4 ocultar">
					<div class="form-group">
					    <label for="G1121_C17110" id="LblG1121_C17110">TEL 2</label>
					    <input type="text" class="form-control input-sm" id="G1121_C17110" name="G1121_C17110" onchange="clearNum(this)" placeholder="TEL 2">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4 ocultar">
					<div class="form-group">
					    <label for="G1121_C17111" id="LblG1121_C17111">TEL 3</label>
					    <input type="text" class="form-control input-sm" id="G1121_C17111" name="G1121_C17111" onchange="clearNum(this)" placeholder="TEL 3">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1121_C17114" id="LblG1121_C17114">MAIL</label>
					    <input type="text" class="form-control input-sm" id="G1121_C17114" name="G1121_C17114" onchange="clearCorreo(this)" placeholder="MAIL">
					</div>

				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 	
			</form>
		</div>
	</div>
	<div class="col-md-1">&nbsp;</div>
</div>
<div class="row" id="botones">
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-danger" id="btnCancelar" type="button">Cancelar</button>
	</div>
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-success" id="btnBuscar" type="button"><i class="fa fa-search"></i> Busqueda</button>
	</div>
</div>
<br/>
<div class="row" id="resulados">
	<div class="col-md-12 col-xs-12" id="resultadosBusqueda">

	</div>
</div>
<div class="row">
	<div class="col-md-12" id="gestiones">
		<iframe id="frameContenedor" src="" style="width: 100%; height: 2500px;"  marginheight="0" marginwidth="0" noresize  frameborder="0" onload="autofitIframe(this);">
              
        </iframe>
	</div>
</div>

<script type="text/javascript">

	$('.ocultar').hide();

	function clearText(input){
        input.value = input.value.replace(/ /g,"")
        
    }
    function clearNum(input){
        input.value = input.value.replace(/[^0-9]/g,"")     

    }

    function clearCorreo(input){
        input.value = input.value.replace(/ /g,"")
	
	}
	
	$(function(){
		$("#btnBuscar").click(function(){
			var datos = $("#formId").serialize();
			$.ajax({
				url     	: 'formularios/G1121/G1121_Funciones_Busqueda_Manual.php?action=GET_DATOS',
				type		: 'post',
				dataType	: 'json',
				data		: datos,
				success 	: function(datosq){
					if(datosq[0].cantidad_registros > 1){
						var valores = null;
						var tabla_a_mostrar = '<div class="box box-default">'+
			            '<div class="box-header">'+
			                '<h3 class="box-title">RESULTADOS DE LA BUSQUEDA</h3>'+
			            '</div>'+
			            '<div class="box-body">'+
			        		'<table class="table table-hover table-bordered" style="width:100%;">';
						tabla_a_mostrar += '<thead>';
						tabla_a_mostrar += '<tr>';
						tabla_a_mostrar += ' <th>CODIGO CLIENTE</th><th>RAZON SOCIAL</th><th>NOMBRE COMERCIAL</th><th>CEDULA NIT</th><th>TEL 1</th><th>TEL 2</th><th>TEL 3</th><th>MAIL</th> ';
						tabla_a_mostrar += '</tr>';
						tabla_a_mostrar += '</thead>';
						tabla_a_mostrar += '<tbody>';
						$.each(datosq[0].registros, function(i, item) {
							tabla_a_mostrar += '<tr ConsInte="'+ item.G1121_ConsInte__b +'" class="EditRegistro">';
							tabla_a_mostrar += '<td>'+ item.G1121_C20659 +'</td><td>'+ item.G1121_C17106 +'</td><td>'+ item.G1121_C17107 +'</td><td>'+ item.G1121_C17108 +'</td><td>'+ item.G1121_C17109 +'</td><td>'+ item.G1121_C17110 +'</td><td>'+ item.G1121_C17111 +'</td><td>'+ item.G1121_C17114 +'</td>';
							tabla_a_mostrar += '</tr>';
						});
						tabla_a_mostrar += '</tbody>';
						tabla_a_mostrar += '</table></div></div>';
						
						$("#resultadosBusqueda").html(tabla_a_mostrar);
						
						$(".EditRegistro").dblclick(function(){
							var id = $(this).attr("ConsInte");
							swal({
	                            html : true,
	                            title: "Información - Dyalogo CRM",
	                            text: 'Esta seguro de editar este registro?',
	                            type: "warning",
	                            confirmButtonText: "Editar registro",
	                            cancelButtonText : "No Editar registro",
	                            showCancelButton : true,
	                            closeOnConfirm : true
	                        },
		                        function(isconfirm){
		                        	if(isconfirm){
		                        		$("#buscador").hide();
		                        		$("#botones").hide();
		                        		$("#resulados").hide();
		                        		<?php if(isset($_GET['token'])){ ?>
	                        			$("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?consinte='+id+'&campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
		                        		<?php } ?>
		                        	}else{
		                        		$("#buscador").show();
		                    			$("#botones").show();
		                    			$("#resulados").show();
		                        	}
		                        });
							});
					}else if(datosq[0].cantidad_registros == 1){
						$("#buscador").hide();
                		$("#botones").hide();
                		$("#resulados").hide();
						var id = datosq[0].registros[0].G1121_ConsInte__b;
						<?php if(isset($_GET['token'])){ ?>
            			$("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
	            		<?php } ?>
					}else{
					 	swal({
                            html : true,
                            title: "Información - Dyalogo CRM",
                            text: 'No se encontraron datos, desea adicionar un registro?',
                            type: "warning",
                            confirmButtonText: "Adicionar registro",
                            cancelButtonText : "No adicionar registro",
                            showCancelButton : true,
                            closeOnConfirm : true
                        },
                        function(isconfirm){
                        	$("#buscador").hide();
                    		$("#botones").hide();
                    		$("#resulados").hide();
                        	if(isconfirm){
								$.ajax({
								url : 'formularios/generados/PHP_Ejecutar.php?<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?>&action=ADD&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
								type		: 'post',
								dataType	: 'json',
								success 	: function(numeroIdnuevo){

									G1121_C17108 = $("#G1121_C17108").val();
									G1121_C17109 = $("#G1121_C17109").val();
									G1121_C17114 = $("#G1121_C17114").val();

									<?php if(isset($_GET['token'])){ ?>
			            			$("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ numeroIdnuevo +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>&nuevoregistro=true&G1122_C17123='+G1121_C17108+'&G1122_C17124='+G1121_C17109+'&G1122_C17129='+G1121_C17114);
				            		<?php } ?>
								}
							});
                        	}else{
                        		$("#buscador").show();
                        		$("#buscador :input").each(function(){
                        			$(this).val('');
                        		});
                    			$("#botones").show();
                    			$("#resulados").show();
                        	}
                        });
					}
				}
			});
		});

		$("#btnCancelar").click(function(){
			window.location = 'formularios/generados/PHP_Cerrar_Cancelar.php?canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; } ?>&token=<?php echo $_GET['token'];?>&id_gestion_cbx=<?php echo $_GET['id_gestion_cbx'];?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>';
		});
	});
</script>
