
<?php date_default_timezone_set('America/Bogota'); ?>
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edición</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1140/G1140_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

	$PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

	if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1140_ConsInte__b as id, G1140_C17614 as camp1 , G1140_C17616 as camp2, LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1140 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1140_C17627  WHERE G1140_Usuario = ".$idUsuario." ORDER BY field (G1140_C17627,14317,14318,14319,14320,14321), G1140_FechaInsercion DESC  LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1140_ConsInte__b as id, G1140_C17614 as camp1 , G1140_C17616 as camp2, LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1140 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1140_C17627  ORDER BY field (G1140_C17627,14317,14318,14319,14320,14321), G1140_FechaInsercion DESC  LIMIT 0, 50";
        }
    }else{
        $Zsql = "SELECT G1140_ConsInte__b as id, G1140_C17614 as camp1 , G1140_C17616 as camp2,  LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1140 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1140_C17627  ORDER BY field (G1140_C17627,14317,14318,14319,14320,14321), G1140_FechaInsercion DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>

<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="box box-primary box-solid">
			<div class="box-header">
                <h3 class="box-title">Historico de gestiones</h3>
            </div>
			<div class="box-body">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Gesti&oacute;n</th>
							<th>Comentarios</th>
							<th>Fecha - hora</th>
							<th>Agente</th>
						</tr>
					</thead>
					<tbody id="tablaGestiones">
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="2207" >


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C20685" id="LblG1140_C20685">CÓDIGO CLIENTE </label>
			            <input type="text" class="form-control input-sm" id="G1140_C20685" value="" readonly name="G1140_C20685"  placeholder="CÓDIGO CLIENTE ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO --> 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_CodigoMiembro" id="LblG1140_CodigoMiembro">CODIGO MIEMBRO</label>
			            <input type="text" class="form-control input-sm" id="G1140_CodigoMiembro" value="" readonly name="G1140_CodigoMiembro"  placeholder="CODIGO MIEMBRO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">
            		<!-- CAMPO TIPO TEXTO -->
 					<div class="form-group">
			            <label for="G1140_Usuario" id="LblG1140_Usuario">AGENTE-CALL</label>
			            <input type="text" style="color:#0897B0" class="form-control input-sm" id="G1140_Usuario" value="" readonly name="G1140_Usuario"  placeholder="NOMBRE AGENTE">            
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17614" id="LblG1140_C17614">RAZON SOCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17614" value="" readonly name="G1140_C17614"  placeholder="RAZON SOCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17615" id="LblG1140_C17615">NOMBRE COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1340_C23283" value="" readonly name="G1340_C23283"  placeholder="NOMBRE COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17616" id="LblG1140_C17616">CONTACTO COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17616" value="" readonly name="G1140_C17616"  placeholder="CONTACTO COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C20686" id="LblG1140_C20686">ID CONTACTO COMERCIAL </label>
			            <input type="text" class="form-control input-sm" id="G1140_C20686" value="" readonly name="G1140_C20686"  placeholder="ID CONTACTO COMERCIAL ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C20687" id="LblG1140_C20687">Cuidad de expedición </label>
			            <input type="text" class="form-control input-sm" id="G1140_C20687" value="" readonly name="G1140_C20687"  placeholder="Cuidad de expedición ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17617" id="LblG1140_C17617">CEDULA NIT</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17617" value="" readonly name="G1140_C17617"  placeholder="CEDULA NIT">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17618" id="LblG1140_C17618">TEL 1</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17618" value="" readonly name="G1140_C17618"  placeholder="TEL 1">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17619" id="LblG1140_C17619">TEL 2</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17619" value="" readonly name="G1140_C17619"  placeholder="TEL 2">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17620" id="LblG1140_C17620">TEL 3</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17620" value="" readonly name="G1140_C17620"  placeholder="TEL 3">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C20202" id="LblG1140_C20202">CIUDAD</label>
			            <input type="text" class="form-control input-sm" id="G1140_C20202" value="" readonly name="G1140_C20202"  placeholder="CIUDAD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17621" id="LblG1140_C17621">DEPARTAMENTO</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17621" value="" readonly name="G1140_C17621"  placeholder="DEPARTAMENTO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17622" id="LblG1140_C17622">MAIL</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17622" value="" readonly name="G1140_C17622"  placeholder="MAIL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17623" id="LblG1140_C17623">DIRECCION</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17623" value="" readonly name="G1140_C17623"  placeholder="DIRECCION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17624" id="LblG1140_C17624">FECHA DE CARGUE</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17624" value="" readonly name="G1140_C17624"  placeholder="FECHA DE CARGUE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17625" id="LblG1140_C17625">CLASIFICACION</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17625" value="" readonly name="G1140_C17625"  placeholder="CLASIFICACION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1140_C20203" id="LblG1140_C20203">CUPO CLIENTE</label>
			            <input type="text" class="form-control input-sm Numerico" value="" readonly name="G1140_C20203" id="G1140_C20203" placeholder="CUPO CLIENTE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1140_C20204" id="LblG1140_C20204">ESTADO CLIENTE</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1140_C20204" readonly id="G1140_C20204">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 865 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1140_C20205" id="LblG1140_C20205">FECHA ACTIVACION</label>
			            <input type="text" class="form-control input-sm Fecha" value="" readonly name="G1140_C20205" id="G1140_C20205" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1140_C20206" id="LblG1140_C20206">FECHA VENCIMIENTO</label>
			            <input type="text" class="form-control input-sm Fecha" value="" readonly name="G1140_C20206" id="G1140_C20206" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>

  
        </div>

           <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group" hidden="">
			            <label for="G1140_C22662" id="LblG1140_C22662">Agente-call</label>
			            <input type="text" class="form-control input-sm" id="G1140_C22662" value="" readonly name="G1140_C22662"  placeholder="Agente-call">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


        </div>


</div>

<div id="2209" style='display:none;'>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17633" id="LblG1140_C17633">Agente</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17633" value="<?php echo getNombreUser($token);?>" readonly name="G1140_C17633"  placeholder="Agente">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17634" id="LblG1140_C17634">Fecha</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17634" value="<?php echo date('Y-m-d');?>" readonly name="G1140_C17634"  placeholder="Fecha">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17635" id="LblG1140_C17635">Hora</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17635" value="<?php echo date('H:i:s');?>" readonly name="G1140_C17635"  placeholder="Hora">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C17636" id="LblG1140_C17636">Campaña</label>
			            <input type="text" class="form-control input-sm" id="G1140_C17636" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"];}else{
                	echo "SIN CAMPAÑA";}?>" readonly name="G1140_C17636"  placeholder="Campaña">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2416">
                DATOS DE PERFIL
            </a>
        </h4>
    </div>
    <div id="s_2416" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1140_C20231" id="LblG1140_C20231">¿Es ó ha sido cliente de Fincaraiz?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1140_C20231" readonly id="G1140_C20231">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1140_C20232" id="LblG1140_C20232">¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1140_C20232" readonly id="G1140_C20232">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			       
			        <!-- CAMPO DE TIPO GUION -->
			        <div class="form-group">
			            <label for="G1140_C20233" id="LblG1140_C20233">¿Desde que ciudad realiza la administración de los inmuebles?</label>
			            <select class="form-control input-sm select2" style="width: 100%;" readonly  name="G1140_C20233" id="G1140_C20233">
			                <option value="0" id="opcionVacia">Seleccione</option>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1140_C20234" id="LblG1140_C20234">¿Cuántos inmuebles USADOS tiene en su inventario?</label>
			            <input type="text" class="form-control input-sm Numerico" value="" readonly name="G1140_C20234" id="G1140_C20234" placeholder="¿Cuántos inmuebles USADOS tiene en su inventario?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1140_C20235" id="LblG1140_C20235">Cupo autorizado</label>
			            <input type="text" class="form-control input-sm" id="G1140_C20235" value="" readonly name="G1140_C20235"  placeholder="Cupo autorizado">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1140_C20236" id="LblG1140_C20236">¿Cuántos inmuebles NUEVOS tiene en su inventario?</label>
			            <input type="text" class="form-control input-sm Numerico" value="" readonly name="G1140_C20236" id="G1140_C20236" placeholder="¿Cuántos inmuebles NUEVOS tiene en su inventario?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>

  
        </div>


        </div>
    </div>
</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2211">
                Datos tarea
            </a>
        </h4>
    </div>
    <div id="s_2211" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1140_C17637" id="LblG1140_C17637">Tipo de solicitud</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1140_C17637" disabled="disabled" id="G1140_C17637">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 864 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO MEMO -->
			        <div class="form-group">
			            <label for="G1140_C17638" id="LblG1140_C17638">Observación</label>
			            <textarea class="form-control input-sm" name="G1140_C17638" id="G1140_C17638"  value="" disabled="disabled" placeholder="Observación"></textarea>
			        </div>
			        <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div>

  
        </div>


        </div>
    </div>
</div>

<div id="2208" >


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
            </div>


            <div class="col-md-6 col-xs-6">

  
            </div>

  
        </div>


</div>

<input type="hidden" name="campana_crm" id="campana_crm" value="<?php if(isset($_GET["campana_crm"])){echo $_GET["campana_crm"];}else{ echo "572";}?>">
<div class="row" style="background-color: #FAFAFA; ">
	<br/>
    <div class="col-md-5 col-xs-5">
        <div class="form-group">
       		<label for="G1140_C17626">Tipificaci&oacute;n</label>
            <select class="form-control input-sm tipificacionBackOffice" name="tipificacion" id="G1140_C17626">
                <option value="0">Seleccione</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 862;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-5 col-xs-5">
        <div class="form-group">
        	<label for="G1140_C17627">Estado de la tarea</label>
            <select class="form-control input-sm reintento" name="reintento" id="G1140_C17627">
                <option value="0">Seleccione</option>
                <?php
                	$Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC 
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 863;";
	                $obj = $mysqli->query($Lsql);
	                while($obje = $obj->fetch_object()){
	                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
	                }      
                ?>
            </select>     
        </div>
    </div>
    <div class="col-md-2 col-xs-2" style="text-align: center;">
    	<label for="G1140_C17627" style="visibility:hidden;">Estado de la tarea</label>
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Guardar Gesti&oacute;n
        </button>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G1140_C17632" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/pies.php");?>
<script type="text/javascript" src="formularios/G1140/G1140_eventos.js"></script> 
<script type="text/javascript">
    $(function(){

    	var meses = new Array(12);
    	meses[0] = "01";
    	meses[1] = "02";
    	meses[2] = "03";
    	meses[3] = "04";
    	meses[4] = "05";
    	meses[5] = "06";
    	meses[6] = "07";
    	meses[7] = "08";
    	meses[8] = "09";
    	meses[9] = "10";
    	meses[10] = "11";
    	meses[11] = "12";

    	var d = new Date();
        var h = d.getHours();
        var horas = (h < 10) ? '0' + h : h;
        var dia = d.getDate();
        var dias = (dia < 10) ? '0' + dia : dia;
        var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
        $("#FechaInicio").val(fechaInicial);
            

        //Esta es por si lo llaman en modo formulario de edicion LigthBox
        <?php if(isset($_GET['registroId'])){ ?>
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
            dataType : 'json',
            success  : function(data){
                //recorrer datos y enviarlos al formulario
                $.each(data, function(i, item) {
                    
 
                    $("#G1140_C20685").val(item.G1140_C20685);

                    $("#G1140_CodigoMiembro").val(item.G1140_CodigoMiembro);

                    $("#G1140_Usuario").val(item.G1140_Usuario);
 
                    $("#G1140_C17614").val(item.G1140_C17614);
 
                    $("#G1140_C17615").val(item.G1140_C17615);
 
                    $("#G1140_C17616").val(item.G1140_C17616);
 
                    $("#G1140_C20686").val(item.G1140_C20686);
 
                    $("#G1140_C20687").val(item.G1140_C20687);
 
                    $("#G1140_C17617").val(item.G1140_C17617);
 
                    $("#G1140_C17618").val(item.G1140_C17618);
 
                    $("#G1140_C17619").val(item.G1140_C17619);
 
                    $("#G1140_C17620").val(item.G1140_C17620);
 
                    $("#G1140_C20202").val(item.G1140_C20202);
 
                    $("#G1140_C17621").val(item.G1140_C17621);
 
                    $("#G1140_C17622").val(item.G1140_C17622);
 
                    $("#G1140_C17623").val(item.G1140_C17623);
 
                    $("#G1140_C17624").val(item.G1140_C17624);
 
                    $("#G1140_C17625").val(item.G1140_C17625);
 
                    $("#G1140_C20203").val(item.G1140_C20203);
 
                    $("#G1140_C20204").val(item.G1140_C20204);
 
                    $("#G1140_C20205").val(item.G1140_C20205);
 
                    $("#G1140_C20206").val(item.G1140_C20206);
 
                    $("#G1140_C22662").val(item.G1140_C22662);
 
                    $("#G1140_C17626").val(item.G1140_C17626);
 
                    $("#G1140_C17627").val(item.G1140_C17627);
 
                    $("#G1140_C17628").val(item.G1140_C17628);
 
                    $("#G1140_C17629").val(item.G1140_C17629);
 
                    $("#G1140_C17630").val(item.G1140_C17630);
 
                    $("#G1140_C17631").val(item.G1140_C17631);
 
                    $("#G1140_C17632").val(item.G1140_C17632);
 
                    $("#G1140_C17633").val(item.G1140_C17633);
 
                    $("#G1140_C17634").val(item.G1140_C17634);
 
                    $("#G1140_C17635").val(item.G1140_C17635);
 
                    $("#G1140_C17636").val(item.G1140_C17636);
 
                    $("#G1140_C17637").val(item.G1140_C17637);
 
                    $("#G1140_C17638").val(item.G1140_C17638);
 
                    $("#G1140_C20231").val(item.G1140_C20231);
 
                    $("#G1140_C20232").val(item.G1140_C20232);
 
                    $("#G1140_C20233").val(item.G1140_C20233);
 
                    $("#G1140_C20233").val(item.G1140_C20233).trigger("change"); 
 
                    $("#G1140_C20234").val(item.G1140_C20234);
 
                    $("#G1140_C20235").val(item.G1140_C20235);
 
                    $("#G1140_C20236").val(item.G1140_C20236);

					$("#h3mio").html(item.principal);

                });
                //Deshabilitar los campos

                //Habilitar todos los campos para edicion
                $('#FormularioDatos :input').each(function(){
                    $(this).attr('disabled', true);
                });

                              

                //Habilidar los botones de operacion, add, editar, eliminar
                $("#add").attr('disabled', false);
                $("#edit").attr('disabled', false);
                $("#delete").attr('disabled', false);

                //Desahabiliatra los botones de salvar y seleccionar_registro
                $("#cancel").attr('disabled', true);
                $("#Save").attr('disabled', true);
            } 
        });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

     	$("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
        	/*
        	vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
        	idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G1140_C17637").select2();
        //datepickers
        

        $("#G1140_C20205").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1140_C20206").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1140_C17630").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1140_C17631").wickedpicker(options);

        //Validaciones numeros Enteros
        

    	$("#G1140_C20203").numeric();
		        
    	$("#G1140_C17628").numeric();
		        
    	$("#G1140_C17629").numeric();
		        
    	$("#G1140_C20234").numeric();
		        
    	$("#G1140_C20236").numeric();
		        

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO CLIENTE 

    $("#G1140_C20204").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ¿Es ó ha sido cliente de Fincaraiz? 

    $("#G1140_C20231").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz? 

    $("#G1140_C20232").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Tipo de solicitud 

    $("#G1140_C17637").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });


        $("#G1140_C17626").change(function(){
        	var id = $(this).attr('id');
            var valor = $("#"+ id +" option:selected").attr('efecividad');
            var monoef = $("#"+ id +" option:selected").attr('monoef');
            var TipNoEF = $("#"+ id +" option:selected").attr('TipNoEF');
            var cambio = $("#"+ id +" option:selected").attr('cambio');
            var importancia = $("#"+ id + " option:selected").attr('importancia');
            var contacto = $("#"+id+" option:selected").attr('contacto');
            $(".reintento").val(TipNoEF).change();
            $("#Efectividad").val(valor);
            $("#MonoEf").val(monoef);
            $("#TipNoEF").val(TipNoEF);
            $("#MonoEfPeso").val(importancia);
            $("#ContactoMonoEf").val(contacto);
    	});
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
        	$("#Save").attr('disabled', true);
        	var bol_respuesta = before_save();
        	var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
            	alertify.error("Es necesaria la tipificación!");
            	valido = 1;
            }

            if($(".reintento").val() == '2'){
            	if($(".TxtFechaReintento").val().length < 1){
            		alertify.error("Es necesario llenar la fecha de reintento!");
            		$(".TxtFechaReintento").focus();
            		valido = 1;
            	}

            	if($(".TxtHoraReintento").val().length < 1){
            		alertify.error("Es necesario llenar la hora de reintento!");
            		$(".TxtHoraReintento").focus();
            		valido = 1;
            	}
            }

            if(valido == '0'){
	        	if(bol_respuesta){            
		            var form = $("#FormularioDatos");
		            //Se crean un array con los datos a enviar, apartir del formulario 
		            var formData = new FormData($("#FormularioDatos")[0]);
		            if($("#G1140_C17626 option:selected").text() == 'Devuelta'){
	                	formData.append("tareaDevuelta","SI");
	                }
		            $.ajax({
		               url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "572"; } ?>',  
		                type: 'POST',
		                data: formData,
		                cache: false,
		                contentType: false,
		                processData: false,
		                //una vez finalizado correctamente
		                success: function(data){
		                    if(data != '0'){
		                    	$("#Save").attr('disabled', false);
			                    alertify.success('Información guardada con exito');   
		                    }else{
		                        //Algo paso, hay un error
		                        $("#Save").attr('disabled', false);
		                        alertify.error('Un error ha ocurrido y no pudimos guardar la información');
		                    }                
		                },
		                //si ha ocurrido un error
		                error: function(){
		                    after_save_error();
		                    $("#Save").attr('disabled', false);
		                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
		                }
		            });
          		}
          	}
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','CÓDIGO CLIENTE ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID CONTACTO COMERCIAL ','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente-call','PASO_ID','REGISTRO_ID','Agente','Fecha','Hora','Campaña','Tipo de solicitud','Observación','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

	                ,
	                { 
	                    name:'G1140_C20685', 
	                    index: 'G1140_C20685', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17614', 
	                    index: 'G1140_C17614', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_CodigoMiembro', 
	                    index: 'G1140_CodigoMiembro', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_Usuario', 
	                    index: 'G1140_Usuario', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17615', 
	                    index: 'G1140_C17615', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17616', 
	                    index: 'G1140_C17616', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C20686', 
	                    index: 'G1140_C20686', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C20687', 
	                    index: 'G1140_C20687', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17617', 
	                    index: 'G1140_C17617', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17618', 
	                    index: 'G1140_C17618', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17619', 
	                    index: 'G1140_C17619', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17620', 
	                    index: 'G1140_C17620', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C20202', 
	                    index: 'G1140_C20202', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17621', 
	                    index: 'G1140_C17621', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17622', 
	                    index: 'G1140_C17622', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17623', 
	                    index: 'G1140_C17623', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17624', 
	                    index: 'G1140_C17624', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17625', 
	                    index: 'G1140_C17625', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1140_C20203', 
	                    index:'G1140_C20203', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1140_C20204', 
	                    index:'G1140_C20204', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1140_C20204'
	                    }
	                }

	                ,
	                {  
	                    name:'G1140_C20205', 
	                    index:'G1140_C20205', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1140_C20206', 
	                    index:'G1140_C20206', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1140_C22662', 
	                    index: 'G1140_C22662', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1140_C17628', 
	                    index:'G1140_C17628', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }
 
	                ,
	                {  
	                    name:'G1140_C17629', 
	                    index:'G1140_C17629', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1140_C17633', 
	                    index: 'G1140_C17633', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17634', 
	                    index: 'G1140_C17634', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17635', 
	                    index: 'G1140_C17635', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17636', 
	                    index: 'G1140_C17636', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C17637', 
	                    index:'G1140_C17637', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=864&campo=G1140_C17637'
	                    }
	                }

	                ,
	                { 
	                    name:'G1140_C17638', 
	                    index:'G1140_C17638', 
	                    width:150, 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1140_C20231', 
	                    index:'G1140_C20231', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1140_C20231'
	                    }
	                }

	                ,
	                { 
	                    name:'G1140_C20232', 
	                    index:'G1140_C20232', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1140_C20232'
	                    }
	                }

	                ,
	                { 
	                    name:'G1140_C20233', 
	                    index:'G1140_C20233', 
	                    width:300 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1140_C20233=si',
	                        dataInit:function(el){
	                        	$(el).select2();
	                            /*$(el).select2({ 
	                                templateResult: function(data) {
	                                    var r = data.text.split('|');
	                                    var row = '<div class="row">';
	                                    var totalRows = 12 / r.length;
	                                    for(i= 0; i < r.length; i++){
	                                        row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
	                                    }
	                                    row += '</div>';
	                                    var $result = $(row);
	                                    return $result;
	                                },
	                                templateSelection : function(data){
	                                    var r = data.text.split('|');
	                                    return r[0];
	                                }
	                            });*/
	                            $(el).change(function(){
	                                var valores = $(el + " option:selected").attr("llenadores");
	                                var campos =  $(el + " option:selected").attr("dinammicos");
	                                var r = valores.split('|');
	                                if(r.length > 1){

	                                    var c = campos.split('|');
	                                    for(i = 1; i < r.length; i++){
	                                        $("#"+ rowid +"_"+c[i]).val(r[i]);
	                                    }
	                                }
	                            });
	                        }
	                    }
	                }
 
	                ,
	                {  
	                    name:'G1140_C20234', 
	                    index:'G1140_C20234', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1140_C20235', 
	                    index: 'G1140_C20235', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1140_C20236', 
	                    index:'G1140_C20236', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    

                    $("#"+ rowid +"_G1140_C20233").change(function(){
                        var valores = $("#"+ rowid +"_G1140_C20233 option:selected").attr("llenadores");
                        var campos = $("#"+ rowid +"_G1140_C20233 option:selected").attr("dinammicos");
                        var r = valores.split('|');

                        if(r.length > 1){

                            var c = campos.split('|');
                            for(i = 1; i < r.length; i++){
                                if(!$("#"+c[i]).is("select")) {
                                // the input field is not a select
                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                }else{
                                    var change = r[i].replace(' ', ''); 
                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                }
                                
                            }
                        }
                    });
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1140_C17614',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(x){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', Busqueda : x  , G1140_C20685 : $("#busq_G1140_C20685").val() , G1140_C17614 : $("#busq_G1140_C17614").val() , G1140_C17615 : $("#busq_G1140_C17615").val() , G1140_C17617 : $("#busq_G1140_C17617").val(), CodMiembro : $("#busq_G1140_CodigoMiembro").val() },
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    var strIconoBackOffice = '';
                    if(data[i].estado == '1'){
                        strIconoBackOffice = 'En gestión';
                    }else if(data[i].estado == '2'){
                        strIconoBackOffice = 'Cerrada';
                    }else if(data[i].estado == '3'){
                        strIconoBackOffice = 'Devuelta';
                    }
                    

                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"<span style='position: relative;right: 2px;float: right;font-size:10px;"+ data[i].color+ "'>"+strIconoBackOffice+"</span></p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');
            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G1140_C20685").val(item.G1140_C20685);

                        $("#G1140_CodigoMiembro").val(item.G1140_CodigoMiembro);

                    	$("#G1140_Usuario").val(item.G1140_Usuario);

                        $("#G1140_C17614").val(item.G1140_C17614);

                        $("#G1140_C17615").val(item.G1140_C17615);

                        $("#G1140_C17616").val(item.G1140_C17616);

                        $("#G1140_C20686").val(item.G1140_C20686);

                        $("#G1140_C20687").val(item.G1140_C20687);

                        $("#G1140_C17617").val(item.G1140_C17617);

                        $("#G1140_C17618").val(item.G1140_C17618);

                        $("#G1140_C17619").val(item.G1140_C17619);

                        $("#G1140_C17620").val(item.G1140_C17620);

                        $("#G1140_C20202").val(item.G1140_C20202);

                        $("#G1140_C17621").val(item.G1140_C17621);

                        $("#G1140_C17622").val(item.G1140_C17622);

                        $("#G1140_C17623").val(item.G1140_C17623);

                        $("#G1140_C17624").val(item.G1140_C17624);

                        $("#G1140_C17625").val(item.G1140_C17625);

                        $("#G1140_C20203").val(item.G1140_C20203);

                        $("#G1140_C20204").val(item.G1140_C20204);
 
        	            $("#G1140_C20204").val(item.G1140_C20204).trigger("change"); 

                        $("#G1140_C20205").val(item.G1140_C20205);

                        $("#G1140_C20206").val(item.G1140_C20206);

                        $("#G1140_C22662").val(item.G1140_C22662);

                        $("#G1140_C17626").val(item.G1140_C17626);
 
        	            $("#G1140_C17626").val(item.G1140_C17626).trigger("change"); 

                        $("#G1140_C17627").val(item.G1140_C17627);
 
        	            $("#G1140_C17627").val(item.G1140_C17627).trigger("change"); 

                        $("#G1140_C17628").val(item.G1140_C17628);

                        $("#G1140_C17629").val(item.G1140_C17629);

                        $("#G1140_C17630").val(item.G1140_C17630);

                        $("#G1140_C17631").val(item.G1140_C17631);

                        $("#G1140_C17632").val(item.G1140_C17632);

                        $("#G1140_C17633").val(item.G1140_C17633);

                        $("#G1140_C17634").val(item.G1140_C17634);

                        $("#G1140_C17635").val(item.G1140_C17635);

                        $("#G1140_C17636").val(item.G1140_C17636);

                        $("#G1140_C17637").val(item.G1140_C17637);
 
        	            $("#G1140_C17637").val(item.G1140_C17637).trigger("change"); 

                        $("#G1140_C17638").val(item.G1140_C17638);

                        $("#G1140_C20231").val(item.G1140_C20231);
 
        	            $("#G1140_C20231").val(item.G1140_C20231).trigger("change"); 

                        $("#G1140_C20232").val(item.G1140_C20232);
 
        	            $("#G1140_C20232").val(item.G1140_C20232).trigger("change"); 

                        $("#G1140_C20233").val(item.G1140_C20233);
 
        	            $("#G1140_C20233").val(item.G1140_C20233).trigger("change"); 

                        $("#G1140_C20234").val(item.G1140_C20234);

                        $("#G1140_C20235").val(item.G1140_C20235);

                        $("#G1140_C20236").val(item.G1140_C20236);
        				$("#h3mio").html(item.principal);
        				
                    });

               
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();

            $.ajax({
	        	url   : '<?php echo $url_crud; ?>',
	        	type  : 'post',
	        	data  : { DameHistorico : 'si', user_codigo_mien : idTotal, campana_crm : '<?php if(isset($_GET['campana_crm'])) { echo $_GET['campana_crm']; } else { echo "572"; } ?>' },
	        	dataType : 'html',
	        	success : function(data){
	        		$("#tablaGestiones").html('');
	        		$("#tablaGestiones").html(data);
	        	}

	    	});
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
	$(document).ready(function() {
		<?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                		while($objDatos = $resultDatosSql->fetch_object()){ ?>
                        	document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
             		<?php  
             			}	
                    }
                    
                } */  
            }
        ?>
    	<?php if(isset($_GET['user'])){ ?>
        	
        	idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
		
	});
</script>
