<?php
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1007_ConsInte__b, G1007_FechaInsercion , G1007_Usuario ,  G1007_CodigoMiembro  , G1007_PoblacionOrigen , G1007_EstadoDiligenciamiento ,  G1007_IdLlamada , G1007_C14027 as principal ,G1007_C14020,G1007_C14021,G1007_C14027,G1007_C14026,G1007_C27096,G1007_C27097,G1007_C27098,G1007_C14028,G1007_C14029,G1007_C14024,G1007_C14023,G1007_C14025,G1007_C14030,G1007_C14031,G1007_C27099,G1007_C14022,G1007_C14032,G1007_C14033,G1007_C14034,G1007_C15540,G1007_C15541,G1007_C15542 FROM '.$BaseDatos.'.G1007 WHERE G1007_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1007_C14020'] = explode(' ', $key->G1007_C14020)[0];

                $datos[$i]['G1007_C14021'] = $key->G1007_C14021;

                $datos[$i]['G1007_C14027'] = $key->G1007_C14027;

                $datos[$i]['G1007_C14026'] = $key->G1007_C14026;

                $datos[$i]['G1007_C27096'] = $key->G1007_C27096;

                $datos[$i]['G1007_C27097'] = $key->G1007_C27097;

                $datos[$i]['G1007_C27098'] = $key->G1007_C27098;

                $datos[$i]['G1007_C14028'] = $key->G1007_C14028;

                $datos[$i]['G1007_C14029'] = $key->G1007_C14029;

                $datos[$i]['G1007_C14024'] = $key->G1007_C14024;

                $datos[$i]['G1007_C14023'] = $key->G1007_C14023;

                $datos[$i]['G1007_C14025'] = $key->G1007_C14025;

                $datos[$i]['G1007_C14030'] = $key->G1007_C14030;

                $datos[$i]['G1007_C14031'] = $key->G1007_C14031;

                $datos[$i]['G1007_C27099'] = $key->G1007_C27099;

                $datos[$i]['G1007_C14022'] = $key->G1007_C14022;

                $datos[$i]['G1007_C14032'] = $key->G1007_C14032;

                $datos[$i]['G1007_C14033'] = $key->G1007_C14033;

                $datos[$i]['G1007_C14034'] = $key->G1007_C14034;

                $datos[$i]['G1007_C15540'] = $key->G1007_C15540;

                $datos[$i]['G1007_C15541'] = $key->G1007_C15541;

                $datos[$i]['G1007_C15542'] = $key->G1007_C15542;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1007_ConsInte__b as id,  G1007_C14027 as camp1 , G1007_C14026 as camp2 ";
            $Lsql .= " FROM ".$BaseDatos.".G1007 ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " WHERE G1007_C14027 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1007_C14026 like '%".$_POST['Busqueda']."%' ";
            }
            $Lsql .= " ORDER BY G1007_ConsInte__b DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1007");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1007_ConsInte__b, G1007_FechaInsercion , G1007_Usuario ,  G1007_CodigoMiembro  , G1007_PoblacionOrigen , G1007_EstadoDiligenciamiento ,  G1007_IdLlamada , G1007_C14027 as principal ,G1007_C14020,G1007_C14021,G1007_C14027,G1007_C14026,G1007_C27096,G1007_C27097,G1007_C27098,G1007_C14028,G1007_C14029,G1007_C14024,G1007_C14023,G1007_C14025,G1007_C14030,G1007_C14031,G1007_C27099,G1007_C14022,G1007_C14032,G1007_C14033,G1007_C14034, a.LISOPC_Nombre____b as G1007_C15540, b.LISOPC_Nombre____b as G1007_C15541, c.LISOPC_Nombre____b as G1007_C15542 FROM '.$BaseDatos.'.G1007 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1007_C15540 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1007_C15541 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1007_C15542';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G1007_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1007_ConsInte__b , explode(' ', $fila->G1007_C14020)[0] , ($fila->G1007_C14021) , ($fila->G1007_C14027) , ($fila->G1007_C14026) , ($fila->G1007_C27096) , ($fila->G1007_C27097) , ($fila->G1007_C27098) , ($fila->G1007_C14028) , ($fila->G1007_C14029) , ($fila->G1007_C14024) , ($fila->G1007_C14023) , ($fila->G1007_C14025) , ($fila->G1007_C14030) , ($fila->G1007_C14031) , ($fila->G1007_C27099) , ($fila->G1007_C14022) , ($fila->G1007_C14032) , ($fila->G1007_C14033) , ($fila->G1007_C14034) , ($fila->G1007_C15540) , ($fila->G1007_C15541) , ($fila->G1007_C15542) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1007 WHERE G1007_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1007_ConsInte__b as id,  G1007_C14027 as camp1 , G1007_C14026 as camp2  FROM '.$BaseDatos.'.G1007 ORDER BY G1007_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1007 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1007(";
            $LsqlV = " VALUES ("; 
 
            $G1007_C14020 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1007_C14020"])){    
                if($_POST["G1007_C14020"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1007_C14020"]);
                    if(count($tieneHora) > 1){
                    	$G1007_C14020 = "'".$_POST["G1007_C14020"]."'";
                    }else{
                    	$G1007_C14020 = "'".str_replace(' ', '',$_POST["G1007_C14020"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1007_C14020 = ".$G1007_C14020;
                    $LsqlI .= $separador." G1007_C14020";
                    $LsqlV .= $separador.$G1007_C14020;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1007_C14021"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14021 = '".$_POST["G1007_C14021"]."'";
                $LsqlI .= $separador."G1007_C14021";
                $LsqlV .= $separador."'".$_POST["G1007_C14021"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C14027"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14027 = '".$_POST["G1007_C14027"]."'";
                $LsqlI .= $separador."G1007_C14027";
                $LsqlV .= $separador."'".$_POST["G1007_C14027"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C14026"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14026 = '".$_POST["G1007_C14026"]."'";
                $LsqlI .= $separador."G1007_C14026";
                $LsqlV .= $separador."'".$_POST["G1007_C14026"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C27096"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C27096 = '".$_POST["G1007_C27096"]."'";
                $LsqlI .= $separador."G1007_C27096";
                $LsqlV .= $separador."'".$_POST["G1007_C27096"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C27097"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C27097 = '".$_POST["G1007_C27097"]."'";
                $LsqlI .= $separador."G1007_C27097";
                $LsqlV .= $separador."'".$_POST["G1007_C27097"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C27098"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C27098 = '".$_POST["G1007_C27098"]."'";
                $LsqlI .= $separador."G1007_C27098";
                $LsqlV .= $separador."'".$_POST["G1007_C27098"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C14028"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14028 = '".$_POST["G1007_C14028"]."'";
                $LsqlI .= $separador."G1007_C14028";
                $LsqlV .= $separador."'".$_POST["G1007_C14028"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C14029"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14029 = '".$_POST["G1007_C14029"]."'";
                $LsqlI .= $separador."G1007_C14029";
                $LsqlV .= $separador."'".$_POST["G1007_C14029"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C14024"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14024 = '".$_POST["G1007_C14024"]."'";
                $LsqlI .= $separador."G1007_C14024";
                $LsqlV .= $separador."'".$_POST["G1007_C14024"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C14023"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14023 = '".$_POST["G1007_C14023"]."'";
                $LsqlI .= $separador."G1007_C14023";
                $LsqlV .= $separador."'".$_POST["G1007_C14023"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C14025"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14025 = '".$_POST["G1007_C14025"]."'";
                $LsqlI .= $separador."G1007_C14025";
                $LsqlV .= $separador."'".$_POST["G1007_C14025"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C14030"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14030 = '".$_POST["G1007_C14030"]."'";
                $LsqlI .= $separador."G1007_C14030";
                $LsqlV .= $separador."'".$_POST["G1007_C14030"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C14031"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14031 = '".$_POST["G1007_C14031"]."'";
                $LsqlI .= $separador."G1007_C14031";
                $LsqlV .= $separador."'".$_POST["G1007_C14031"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C27099"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C27099 = '".$_POST["G1007_C27099"]."'";
                $LsqlI .= $separador."G1007_C27099";
                $LsqlV .= $separador."'".$_POST["G1007_C27099"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C14022"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14022 = '".$_POST["G1007_C14022"]."'";
                $LsqlI .= $separador."G1007_C14022";
                $LsqlV .= $separador."'".$_POST["G1007_C14022"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C14032"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14032 = '".$_POST["G1007_C14032"]."'";
                $LsqlI .= $separador."G1007_C14032";
                $LsqlV .= $separador."'".$_POST["G1007_C14032"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C14033"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14033 = '".$_POST["G1007_C14033"]."'";
                $LsqlI .= $separador."G1007_C14033";
                $LsqlV .= $separador."'".$_POST["G1007_C14033"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C14034"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C14034 = '".$_POST["G1007_C14034"]."'";
                $LsqlI .= $separador."G1007_C14034";
                $LsqlV .= $separador."'".$_POST["G1007_C14034"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C15540"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C15540 = '".$_POST["G1007_C15540"]."'";
                $LsqlI .= $separador."G1007_C15540";
                $LsqlV .= $separador."'".$_POST["G1007_C15540"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C15541"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C15541 = '".$_POST["G1007_C15541"]."'";
                $LsqlI .= $separador."G1007_C15541";
                $LsqlV .= $separador."'".$_POST["G1007_C15541"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1007_C15542"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_C15542 = '".$_POST["G1007_C15542"]."'";
                $LsqlI .= $separador."G1007_C15542";
                $LsqlV .= $separador."'".$_POST["G1007_C15542"]."'";
                $validar = 1;
            }
             

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1007_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1007_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}


			$padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1007_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
			if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1007_Usuario , G1007_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1007_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1007 WHERE G1007_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                    	echo $mysqli->insert_id;
                	}else{
                		
                		echo "1";    		
                	}

                } else {
                	echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
