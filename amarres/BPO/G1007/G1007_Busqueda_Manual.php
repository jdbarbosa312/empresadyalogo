
<?php
	//JDBD SE COMENTA LA CONEXION YA QUE INTERFIERE CON EL SISTEMA DE SEGURIDAD "/../../conexion.php");
	$http = "http://".$_SERVER["HTTP_HOST"];
	if (isset($_SERVER['HTTPS'])) {
	    $http = "https://".$_SERVER["HTTP_HOST"];
	}
	if(isset($_GET["accion_manual"])){
		$idMonoef=-19;
		$texto = "Busqueda No Aplica";
	}else{
		$idMonoef =-18;
		$texto = "No Suministra Información";
	}
?>

<script type="text/javascript">
	function autofitIframe(id){
		if (!window.opera && document.all && document.getElementById){
			id.style.height=id.contentWindow.document.body.scrollHeight;
		} else if(document.getElementById) {
			id.style.height=id.contentDocument.body.scrollHeight+"px";
		}
	}
</script>

<div class="row" id="buscador">
	<div class="col-md-1">&nbsp;</div>
	<div class="col-md-10">
		<div class="row">
			<form id="formId">
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1007_C14027" id="LblG1007_C14027">CLIENTE</label>
					    <input type="text" class="form-control input-sm" id="G1007_C14027" name="G1007_C14027"  placeholder="CLIENTE">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1007_C14026" id="LblG1007_C14026">CODIGO CLIENTE</label>
					    <input type="text" class="form-control input-sm" id="G1007_C14026" name="G1007_C14026"  placeholder="CODIGO CLIENTE">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1007_C14028" id="LblG1007_C14028">TELEFONO 1</label>
					    <input type="text" class="form-control input-sm" id="G1007_C14028" name="G1007_C14028"  placeholder="TELEFONO 1">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1007_C14030" id="LblG1007_C14030">EMAIL</label>
					    <input type="text" class="form-control input-sm" id="G1007_C14030" name="G1007_C14030"  placeholder="EMAIL">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 	
			</form>
		</div>
	</div>
	<div class="col-md-1">&nbsp;</div>
</div>
<div class="row" id="botones">
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-danger" id="btnCancelar" type="button"><?php echo $texto; ?></button>
	</div>
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-success" id="btnBuscar" type="button"><i class="fa fa-search"></i> Busqueda</button>
	</div>
</div>
<br/>
<div class="row" id="resulados">
	<div class="col-md-12 col-xs-12" id="resultadosBusqueda">

	</div>
</div>
<div class="row">
	<div class="col-md-12" id="gestiones">
		<iframe id="frameContenedor" src="" style="width: 100%; height: 2500px;"  marginheight="0" marginwidth="0" noresize  frameborder="0" onload="autofitIframe(this);">
              
        </iframe>
	</div>
</div>

<script type="text/javascript">

    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }


    // Listen to message from child window
    bindEvent(window, 'message', function (e) {
        if(Array.isArray(e.data)){
            console.log(e.data);
            var keys=Object.keys(e.data[0].camposivr);
            var key=0;
            $.each(e.data[0].camposivr, function(i, valor){
                if($("#"+keys[key]).length > 0){
                    $("#"+keys[key]).val(valor); 
                }
                key++;
            });
            buscarRegistros(e.data);
//          $("#btnBuscar").click();
        }
        
        if(typeof(e.data)== 'object'){
            switch (e.data["accion"]){
                case "llamadaDesdeG" :
                    parent.postMessage(e.data, '*');
                    break;
                case "cerrargestion":
                    var origen="formulario"
                    finalizarGestion(e.data[datos],origen);
                    break;
            }
        }
    });

	$(function(){
		$("#btnBuscar").click(function(){
			var datos = $("#formId").serialize();
			$.ajax({
				url     	: 'formularios/G1007/G1007_Funciones_Busqueda_Manual.php?action=GET_DATOS',
				type		: 'post',
				dataType	: 'json',
				data		: datos,
				success 	: function(datosq){
					if(datosq[0].cantidad_registros > 1){
						var valores = null;
						var tabla_a_mostrar = '<div class="box box-default">'+
			            '<div class="box-header">'+
			                '<h3 class="box-title">RESULTADOS DE LA BUSQUEDA</h3><button type="button" style="margin-left:10px;" onclick="adicionarRegistro(1);" class="btn btn-info btn-sm"> <span class="glyphicon glyphicon-plus"></span></button><button type="button" style="margin-left:5px;" onclick="limpiar();" class="btn btn-success btn-sm"> <span class="glyphicon glyphicon-search"></span></button>'+
			            '</div>'+
			            '<div class="box-body">'+
			        		'<table class="table table-hover table-bordered" style="width:100%;">';
						tabla_a_mostrar += '<thead>';
						tabla_a_mostrar += '<tr>';
						tabla_a_mostrar += ' <th>CLIENTE</th><th>CODIGO CLIENTE</th><th>TELEFONO 1</th><th>EMAIL</th> ';
						tabla_a_mostrar += '</tr>';
						tabla_a_mostrar += '</thead>';
						tabla_a_mostrar += '<tbody>';
						$.each(datosq[0].registros, function(i, item) {
							tabla_a_mostrar += '<tr ConsInte="'+ item.G1007_ConsInte__b +'" class="EditRegistro">';
							tabla_a_mostrar += '<td>'+ item.G1007_C14027 +'</td><td>'+ item.G1007_C14026 +'</td><td>'+ item.G1007_C14028 +'</td><td>'+ item.G1007_C14030 +'</td>';
							tabla_a_mostrar += '</tr>';
						});
						tabla_a_mostrar += '</tbody>';
						tabla_a_mostrar += '</table></div></div>';
						
						$("#resultadosBusqueda").html(tabla_a_mostrar);
						
						$(".EditRegistro").dblclick(function(){
							var id = $(this).attr("ConsInte");
							swal({
	                            html : true,
	                            title: "Información - Dyalogo CRM",
	                            text: 'Esta seguro de editar este registro?',
	                            type: "warning",
	                            confirmButtonText: "Editar registro",
	                            cancelButtonText : "No Editar registro",
	                            showCancelButton : true,
	                            closeOnConfirm : true
	                        },
		                        function(isconfirm){
		                        	if(isconfirm){
		                        		$("#buscador").hide();
		                        		$("#botones").hide();
		                        		$("#resulados").hide();
		                        		<?php if(isset($_GET['token'])){ ?>
	                        			$("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?consinte='+id+'&campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
		                        		<?php } ?>
		                        	}else{
		                        		$("#buscador").show();
		                    			$("#botones").show();
		                    			$("#resulados").show();
		                        	}
		                        });
							});
					}else if(datosq[0].cantidad_registros == 1){
						$("#buscador").hide();
                		$("#botones").hide();
                		$("#resulados").hide();
						var id = datosq[0].registros[0].G1007_ConsInte__b;
						<?php if(isset($_GET['token'])){ ?>
            			$("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
	            		<?php } ?>
					}else{
					 	adicionarRegistro(2);
					}
				}
			});
		});

		$("#btnCancelar").click(function(){
            $.ajax({
                url     	: 'formularios/generados/PHP_Cerrar_Cancelar.php?canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal"; } ?>&token=<?php echo $_GET['token'];?>&id_gestion_cbx=<?php echo $_GET['id_gestion_cbx'];?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['origen'])){echo '&origen='.$_GET['origen'];}else{echo '&origen=BusquedaManual';} ?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?><?php echo "&idMonoef=".$idMonoef;?><?php echo "&tiempoInicio=".date("Y-m-d H:i:s");?><?php if(isset($_GET["usuario"])) { echo "&usuario=".$_GET["usuario"]; }?>',
                type		: 'post',
                dataType	: 'json',
                success 	: function(data){
                    var origen="bqmanual";
                    finalizarGestion(data,origen);    
                }
            });  
		});
	});
	function adicionarRegistro(tipo){
		if(tipo == "1"){
			mensaje = "Desea adicionar un registro";
		}
		if(tipo == "2"){
			mensaje = "No se encontraron datos, desea adicionar un registro";
		}
		swal({
            html : true,
            title: "Información - Dyalogo CRM",
            text: mensaje,
            type: "warning",
            confirmButtonText: "Adicionar registro",
            cancelButtonText : "Hacer otra busqueda",
            showCancelButton : true,
            closeOnConfirm : true
        },
        function(isconfirm){
        	$("#buscador").hide();
    		$("#botones").hide();
    		$("#resulados").hide();
        	if(isconfirm){
        		//JDBD - obtenemos los valores de la busqueda manual para enviarselos al formulario
        		var G1089_C16360 = $("#G1007_C14027").val();
			var G1008_C14042 = $("#G1007_C14027").val();
			var G1088_C16324 = $("#G1007_C14027").val();
			var G1089_C16359 = $("#G1007_C14026").val();
			var G1008_C14041 = $("#G1007_C14026").val();
			var G1088_C16323 = $("#G1007_C14026").val();
			var G1089_C16361 = $("#G1007_C14028").val();
			var G1008_C14043 = $("#G1007_C14028").val();
			var G1088_C16325 = $("#G1007_C14028").val();
			var G1089_C16363 = $("#G1007_C14030").val();
			var G1008_C14045 = $("#G1007_C14030").val();
			var G1088_C16327 = $("#G1007_C14030").val();
				$.ajax({
				url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
				type		: 'post',
				dataType	: 'json',
				success 	: function(numeroIdnuevo){
					<?php if(isset($_GET['token'])){ ?>
        			$("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ numeroIdnuevo +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>&nuevoregistro=true&G1089_C16360='+G1089_C16360+'&G1008_C14042='+G1008_C14042+'&G1088_C16324='+G1088_C16324+'&G1089_C16359='+G1089_C16359+'&G1008_C14041='+G1008_C14041+'&G1088_C16323='+G1088_C16323+'&G1089_C16361='+G1089_C16361+'&G1008_C14043='+G1008_C14043+'&G1088_C16325='+G1088_C16325+'&G1089_C16363='+G1089_C16363+'&G1008_C14045='+G1008_C14045+'&G1088_C16327='+G1088_C16327);
            		<?php } ?>
				}
			});
        	}else{
        		limpiar();
        	}
        });
	}
	function limpiar(){
		$("#buscador").show();
		$("#buscador :input").each(function(){
			$(this).val("");
		});
		$("#resultadosBusqueda").html("");
		$("#botones").show();
		$("#resulados").show();
	}
</script>
