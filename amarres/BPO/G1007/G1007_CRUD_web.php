<?php
    session_start();
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
    
    if(isset($_POST['getListaHija'])){

        $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
        //echo $Lsql;
        $res = $mysqli->query($Lsql);
        echo "<option value='0'>Seleccione</option>";
        while($key = $res->fetch_object()){
            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
        }
    }   

    //Inserciones o actualizaciones
    if(isset($_POST["oper"])){
        $str_Lsql  = '';

        $validar = 0;
        $str_LsqlU = "UPDATE ".$BaseDatos.".G1007 SET "; 
        $str_LsqlI = "INSERT INTO ".$BaseDatos.".G1007( G1007_FechaInsercion ,";
        $str_LsqlV = " VALUES ('".date('Y-m-d H:s:i')."',"; 
 
        $G1007_C14020 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1007_C14020"])){    
            if($_POST["G1007_C14020"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1007_C14020 = "'".str_replace(' ', '',$_POST["G1007_C14020"])." 00:00:00'";
                $str_LsqlU .= $separador." G1007_C14020 = ".$G1007_C14020;
                $str_LsqlI .= $separador." G1007_C14020";
                $str_LsqlV .= $separador.$G1007_C14020;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1007_C14021"]) && $_POST["G1007_C14021"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C14021 = '".$_POST["G1007_C14021"]."'";
            $str_LsqlI .= $separador."G1007_C14021";
            $str_LsqlV .= $separador."'".$_POST["G1007_C14021"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C14027"]) && $_POST["G1007_C14027"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C14027 = '".$_POST["G1007_C14027"]."'";
            $str_LsqlI .= $separador."G1007_C14027";
            $str_LsqlV .= $separador."'".$_POST["G1007_C14027"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C14026"]) && $_POST["G1007_C14026"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C14026 = '".$_POST["G1007_C14026"]."'";
            $str_LsqlI .= $separador."G1007_C14026";
            $str_LsqlV .= $separador."'".$_POST["G1007_C14026"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C27096"]) && $_POST["G1007_C27096"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C27096 = '".$_POST["G1007_C27096"]."'";
            $str_LsqlI .= $separador."G1007_C27096";
            $str_LsqlV .= $separador."'".$_POST["G1007_C27096"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C27097"]) && $_POST["G1007_C27097"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C27097 = '".$_POST["G1007_C27097"]."'";
            $str_LsqlI .= $separador."G1007_C27097";
            $str_LsqlV .= $separador."'".$_POST["G1007_C27097"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C27098"]) && $_POST["G1007_C27098"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C27098 = '".$_POST["G1007_C27098"]."'";
            $str_LsqlI .= $separador."G1007_C27098";
            $str_LsqlV .= $separador."'".$_POST["G1007_C27098"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C14028"]) && $_POST["G1007_C14028"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C14028 = '".$_POST["G1007_C14028"]."'";
            $str_LsqlI .= $separador."G1007_C14028";
            $str_LsqlV .= $separador."'".$_POST["G1007_C14028"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C14029"]) && $_POST["G1007_C14029"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C14029 = '".$_POST["G1007_C14029"]."'";
            $str_LsqlI .= $separador."G1007_C14029";
            $str_LsqlV .= $separador."'".$_POST["G1007_C14029"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C14024"]) && $_POST["G1007_C14024"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C14024 = '".$_POST["G1007_C14024"]."'";
            $str_LsqlI .= $separador."G1007_C14024";
            $str_LsqlV .= $separador."'".$_POST["G1007_C14024"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C14023"]) && $_POST["G1007_C14023"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C14023 = '".$_POST["G1007_C14023"]."'";
            $str_LsqlI .= $separador."G1007_C14023";
            $str_LsqlV .= $separador."'".$_POST["G1007_C14023"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C14025"]) && $_POST["G1007_C14025"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C14025 = '".$_POST["G1007_C14025"]."'";
            $str_LsqlI .= $separador."G1007_C14025";
            $str_LsqlV .= $separador."'".$_POST["G1007_C14025"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C14030"]) && $_POST["G1007_C14030"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C14030 = '".$_POST["G1007_C14030"]."'";
            $str_LsqlI .= $separador."G1007_C14030";
            $str_LsqlV .= $separador."'".$_POST["G1007_C14030"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C14031"]) && $_POST["G1007_C14031"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C14031 = '".$_POST["G1007_C14031"]."'";
            $str_LsqlI .= $separador."G1007_C14031";
            $str_LsqlV .= $separador."'".$_POST["G1007_C14031"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C27099"]) && $_POST["G1007_C27099"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C27099 = '".$_POST["G1007_C27099"]."'";
            $str_LsqlI .= $separador."G1007_C27099";
            $str_LsqlV .= $separador."'".$_POST["G1007_C27099"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C14022"]) && $_POST["G1007_C14022"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C14022 = '".$_POST["G1007_C14022"]."'";
            $str_LsqlI .= $separador."G1007_C14022";
            $str_LsqlV .= $separador."'".$_POST["G1007_C14022"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C14032"]) && $_POST["G1007_C14032"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C14032 = '".$_POST["G1007_C14032"]."'";
            $str_LsqlI .= $separador."G1007_C14032";
            $str_LsqlV .= $separador."'".$_POST["G1007_C14032"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C15540"]) && $_POST["G1007_C15540"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C15540 = '".$_POST["G1007_C15540"]."'";
            $str_LsqlI .= $separador."G1007_C15540";
            $str_LsqlV .= $separador."'".$_POST["G1007_C15540"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C15541"]) && $_POST["G1007_C15541"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C15541 = '".$_POST["G1007_C15541"]."'";
            $str_LsqlI .= $separador."G1007_C15541";
            $str_LsqlV .= $separador."'".$_POST["G1007_C15541"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1007_C15542"]) && $_POST["G1007_C15542"] != ''){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_C15542 = '".$_POST["G1007_C15542"]."'";
            $str_LsqlI .= $separador."G1007_C15542";
            $str_LsqlV .= $separador."'".$_POST["G1007_C15542"]."'";
            $validar = 1;
        }
         
 
        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $str_Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($str_Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G1007_C";
                $valorH = $valorG.$campo;
                $str_LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $str_LsqlI .= $separador." ".$valorH;
                $str_LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1007_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $str_LsqlI .= $separador."G1007_IdLlamada";
            $str_LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        if(isset($_POST['ORIGEN_DY_WF'])){
            if($_POST['ORIGEN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $Origen = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 1007 AND PREGUN_Texto_____b = 'ORIGEN_DY_WF'";
                $res_Origen = $mysqli->query($Origen);
                if($res_Origen->num_rows > 0){
                    $dataOrigen = $res_Origen->fetch_array();

                    $str_LsqlU .= $separador."G1007_C".$dataOrigen['PREGUN_ConsInte__b']." = '".$_POST['ORIGEN_DY_WF']."'";
                    $str_LsqlI .= $separador."G1007_C".$dataOrigen['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador."'".$_POST['ORIGEN_DY_WF']."'";
                    $validar = 1;
                }
                

            }
        }

        if(isset($_POST['OPTIN_DY_WF'])){
            if($_POST['OPTIN_DY_WF'] != '0'){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $confirmado = null;
                if($_POST['OPTIN_DY_WF'] == 'SIMPLE'){
                    $confirmado  = "'CONFIRMADO'";
                }

                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 1007 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                if($res_OPTIN_DY_WF->num_rows > 0){
                    $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();

                    $str_LsqlU .= $separador."G1007_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = ".$confirmado;
                    $str_LsqlI .= $separador."G1007_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b'];
                    $str_LsqlV .= $separador." ".$confirmado;
                    $validar = 1;
                }
            }
        }


        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' ){
                
                $str_Lsql = $str_LsqlI.")" . $str_LsqlV.")";
            }
        }

        //si trae algo que insertar inserta

        //echo $str_Lsql;
        if($validar == 1){
            if ($mysqli->query($str_Lsql) === TRUE) {
                $ultimoResgistroInsertado = $mysqli->insert_id;

                


                if(isset($_POST['v'])){
                    
                
                }else{
                    header('Location:http://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=MTAwNw==&result=1');
                }
            } else {
                echo "Error Hacieno el proceso los registros : " . $mysqli->error;
            }
        }
    }
    


?>
