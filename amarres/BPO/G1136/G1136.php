
<?php date_default_timezone_set('America/Bogota'); ?>
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edición</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1136/G1136_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

	$PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

	if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1136_ConsInte__b as id, G1136_C17513 as camp1 , G1136_C17515 as camp2, LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1136 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1136_C17526  WHERE G1136_Usuario = ".$idUsuario." ORDER BY FIELD(G1136_C17526,14280,14281,14282,14283,14284), G1136_FechaInsercion DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1136_ConsInte__b as id, G1136_C17513 as camp1 , G1136_C17515 as camp2, LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1136 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1136_C17526  ORDER BY FIELD(G1136_C17526,14280,14281,14282,14283,14284), G1136_FechaInsercion DESC LIMIT 0, 50";
        }
    }else{
        $Zsql = "SELECT G1136_ConsInte__b as id, G1136_C17513 as camp1 , G1136_C17515 as camp2,  LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1136 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1136_C17526  ORDER BY FIELD(G1136_C17526,14280,14281,14282,14283,14284), G1136_FechaInsercion DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>

<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="box box-primary box-solid">
			<div class="box-header">
                <h3 class="box-title">Historico de gestiones</h3>
            </div>
			<div class="box-body">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Gesti&oacute;n</th>
							<th>Comentarios</th>
							<th>Fecha - hora</th>
							<th>Agente</th>
						</tr>
					</thead>
					<tbody id="tablaGestiones">
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="2187" >


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C20664" id="LblG1136_C20664">Código Cliente </label>
			            <input type="text" class="form-control input-sm" id="G1136_C20664" value="" readonly name="G1136_C20664"  placeholder="Código Cliente ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_CodigoMiembro" id="LblG1136_CodigoMiembro">CODIGO MIEMBRO</label>
			            <input type="text" class="form-control input-sm" id="G1136_CodigoMiembro" value="" readonly name="G1136_CodigoMiembro"  placeholder="CODIGO MIEMBRO">
			        </div>
  
            </div>


            <div class="col-md-6 col-xs-6">
            		<!-- CAMPO TIPO TEXTO -->
 					<div class="form-group">
			            <label for="G1136_Usuario" id="LblG1136_Usuario">AGENTE-CALL</label>
			            <input type="text" style="color:#0897B0" class="form-control input-sm" id="G1136_Usuario" value="" readonly name="G1136_Usuario"  placeholder="NOMBRE AGENTE">
			            

			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17513" id="LblG1136_C17513">RAZON SOCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17513" value="" readonly name="G1136_C17513"  placeholder="RAZON SOCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17514" id="LblG1136_C17514">NOMBRE COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17514" value="" readonly name="G1136_C17514"  placeholder="NOMBRE COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C20334" id="LblG1136_C20334">CONTACTO COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1136_C20334" value="" readonly name="G1136_C20334"  placeholder="CONTACTO COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C20665" id="LblG1136_C20665">ID Contacto comercial</label>
			            <input type="text" class="form-control input-sm" id="G1136_C20665" value="" readonly name="G1136_C20665"  placeholder="ID Contacto comercial">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C20666" id="LblG1136_C20666">Cuidad de expedición </label>
			            <input type="text" class="form-control input-sm" id="G1136_C20666" value="" readonly name="G1136_C20666"  placeholder="Cuidad de expedición ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17515" id="LblG1136_C17515">CEDULA NIT</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17515" value="" readonly name="G1136_C17515"  placeholder="CEDULA NIT">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17516" id="LblG1136_C17516">TEL 1</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17516" value="" readonly name="G1136_C17516"  placeholder="TEL 1">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17517" id="LblG1136_C17517">TEL 2</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17517" value="" readonly name="G1136_C17517"  placeholder="TEL 2">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17518" id="LblG1136_C17518">TEL 3</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17518" value="" readonly name="G1136_C17518"  placeholder="TEL 3">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17519" id="LblG1136_C17519">CIUDAD</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17519" value="" readonly name="G1136_C17519"  placeholder="CIUDAD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17520" id="LblG1136_C17520">DEPARTAMENTO</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17520" value="" readonly name="G1136_C17520"  placeholder="DEPARTAMENTO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17521" id="LblG1136_C17521">MAIL</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17521" value="" readonly name="G1136_C17521"  placeholder="MAIL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17522" id="LblG1136_C17522">DIRECCION</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17522" value="" readonly name="G1136_C17522"  placeholder="DIRECCION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17523" id="LblG1136_C17523">FECHA DE CARGUE</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17523" value="" readonly name="G1136_C17523"  placeholder="FECHA DE CARGUE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17524" id="LblG1136_C17524">CLASIFICACION</label>
			            <input type="text" class="form-controfl input-sm" id="G1136_C17524" value="" readonly name="G1136_C17524"  placeholder="CLASIFICACION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1136_C18891" id="LblG1136_C18891">CUPO CLIENTE</label>
			            <input type="text" class="form-control input-sm Numerico" value="" readonly name="G1136_C18891" id="G1136_C18891" placeholder="CUPO CLIENTE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1136_C18892" id="LblG1136_C18892">ESTADO CLIENTE</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1136_C18892" readonly id="G1136_C18892">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 865 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1136_C18893" id="LblG1136_C18893">FECHA ACTIVACION</label>
			            <input type="text" class="form-control input-sm Fecha" value="" readonly name="G1136_C18893" id="G1136_C18893" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C20953" id="LblG1136_C20953">Hora de activación </label>
			            <input type="text" class="form-control input-sm" id="G1136_C20953" value="" readonly name="G1136_C20953"  placeholder="Hora de activación ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1136_C18894" id="LblG1136_C18894">FECHA VENCIMIENTO</label>
			            <input type="text" class="form-control input-sm Fecha" value="" readonly name="G1136_C18894" id="G1136_C18894" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group"hidden >
			            <label for="G1136_C22655" id="LblG1136_C22655">Agente-Call</label>
			            <input type="text" class="form-control input-sm" id="G1136_C22655" value="" readonly name="G1136_C22655"  placeholder="Agente-Call">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


</div>
  

<div id="2189" style='display:none;'>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17532" id="LblG1136_C17532">Agente</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17532" value="<?php echo getNombreUser($token);?>" readonly name="G1136_C17532"  placeholder="Agente">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17533" id="LblG1136_C17533">Fecha</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17533" value="<?php echo date('Y-m-d');?>" readonly name="G1136_C17533"  placeholder="Fecha">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17534" id="LblG1136_C17534">Hora</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17534" value="<?php echo date('H:i:s');?>" readonly name="G1136_C17534"  placeholder="Hora">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1136_C17535" id="LblG1136_C17535">Campaña</label>
			            <input type="text" class="form-control input-sm" id="G1136_C17535" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"];}else{
                	echo "SIN CAMPAÑA";}?>" readonly name="G1136_C17535"  placeholder="Campaña">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2402">
                DATOS DE PERFIL
            </a>
        </h4>
    </div>
    <div id="s_2402" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1136_C18895" id="LblG1136_C18895">¿Es ó ha sido cliente de Fincaraiz?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1136_C18895" readonly id="G1136_C18895">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1136_C18896" id="LblG1136_C18896">¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1136_C18896" readonly id="G1136_C18896">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			       
			        <!-- CAMPO DE TIPO GUION -->
			        <div class="form-group">
			            <label for="G1136_C18897" id="LblG1136_C18897">¿Desde que ciudad realiza la administración de los inmuebles?</label>
			            <select class="form-control input-sm select2" style="width: 100%;" readonly  name="G1136_C18897" id="G1136_C18897">
			                <option value="0" id="opcionVacia">Seleccione</option>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1136_C18898" id="LblG1136_C18898">¿Cuántos inmuebles USADOS tiene en su inventario?</label>
			            <input type="text" class="form-control input-sm Numerico" value="" readonly name="G1136_C18898" id="G1136_C18898" placeholder="¿Cuántos inmuebles USADOS tiene en su inventario?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1136_C18899" id="LblG1136_C18899">Cupo autorizado</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1136_C18899" readonly id="G1136_C18899">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 977 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1136_C18900" id="LblG1136_C18900">¿Cuántos inmuebles NUEVOS tiene en su inventario?</label>
			            <input type="text" class="form-control input-sm Numerico" value="" readonly name="G1136_C18900" id="G1136_C18900" placeholder="¿Cuántos inmuebles NUEVOS tiene en su inventario?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>

  
        </div>


        </div>
    </div>
</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2191">
                Datos tarea
            </a>
        </h4>
    </div>
    <div id="s_2191" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO DECIMAL -->
			        <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1136_C17537" id="LblG1136_C17537">Cupo usados</label>
			            <input type="text" class="form-control input-sm Decimal" value="" disabled="disabled"  name="G1136_C17537" id="G1136_C17537" placeholder="Cupo usados">
			        </div>
			        <!-- FIN DEL CAMPO TIPO DECIMAL -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
			        <div class="form-group">
			            <label for="G1136_C17536" id="LblG1136_C17536">Usados</label>
			            <div class="checkbox">
			                <label>
			                    <input type="checkbox" value="1" disabled="disabled" name="G1136_C17536" id="G1136_C17536"  data-error="Before you wreck yourself"  > 
			                </label>
			            </div>
			        </div>
			        <!-- FIN DEL CAMPO SI/NO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO DECIMAL -->
			        <!-- Estos campos siempre deben llevar Decimal en la clase asi class="form-control input-sm Decimal" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1136_C17539" id="LblG1136_C17539">Cupo nuevos</label>
			            <input type="text" class="form-control input-sm Decimal" value="" disabled="disabled" name="G1136_C17539" id="G1136_C17539" placeholder="Cupo nuevos">
			        </div>
			        <!-- FIN DEL CAMPO TIPO DECIMAL -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO DE TIPO CHECKBOX O SI/NO -->
			        <div class="form-group">
			            <label for="G1136_C17538" id="LblG1136_C17538">Nuevos</label>
			            <div class="checkbox">
			                <label>
			                    <input type="checkbox" value="1" disabled="disabled" name="G1136_C17538" id="G1136_C17538"  data-error="Before you wreck yourself"  > 
			                </label>
			            </div>
			        </div>
			        <!-- FIN DEL CAMPO SI/NO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO MEMO -->
			        <div class="form-group">
			            <label for="G1136_C17540" id="LblG1136_C17540">Observación</label>
			            <textarea class="form-control input-sm" name="G1136_C17540" id="G1136_C17540"  value="" disabled="disabled" placeholder="Observación"></textarea>
			        </div>
			        <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1136_C18644" id="LblG1136_C18644">Categoría</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" disabled="disabled" name="G1136_C18644"  id="G1136_C18644">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 911 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1136_C20948" id="LblG1136_C20948">Pack adquirido</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" disabled="disabled" name="G1136_C20948"  id="G1136_C20948">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 978 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1136_C20949" id="LblG1136_C20949">Tiempo</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" disabled="disabled" name="G1136_C20949"  id="G1136_C20949">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 979 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1136_C20950" id="LblG1136_C20950">Descuento </label>
			            <select class="form-control input-sm select2"  style="width: 100%;" disabled="disabled" name="G1136_C20950"  id="G1136_C20950">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 980 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1136_C20951" id="LblG1136_C20951">Fecha de lectura del contrato</label>
			            <input type="text" class="form-control input-sm Fecha" value="" disabled="disabled" name="G1136_C20951" id="G1136_C20951" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIMEPICKER -->
			        <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
			        <div class="bootstrap-timepicker">
			            <div class="form-group">
			                <label for="G1136_C20952" id="LblG1136_C20952">Hora de lectura del contrato </label>
			                <div class="input-group">
			                    <input type="text" class="form-control input-sm Hora"  name="G1136_C20952" disabled="disabled" id="G1136_C20952" placeholder="HH:MM:SS" >
			                    <div class="input-group-addon" id="TMP_G1136_C20952">
			                        <i class="fa fa-clock-o"></i>
			                    </div>
			                </div>
			                <!-- /.input group -->
			            </div>
			            <!-- /.form group -->
			        </div>
			        <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div>


        </div>


        </div>
    </div>
</div>


<input type="hidden" name="campana_crm" id="campana_crm" value="<?php if(isset($_GET["campana_crm"])){echo $_GET["campana_crm"];}else{ echo "568";}?>">
<div class="row" style="background-color: #FAFAFA; ">
	<br/>
    <div class="col-md-5 col-xs-5">
        <div class="form-group">
       		<label for="G1136_C17525">Tipificaci&oacute;n</label>
            <select class="form-control input-sm tipificacionBackOffice" name="tipificacion" id="G1136_C17525">
                <option value="0">Seleccione</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 853;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-5 col-xs-5">
        <div class="form-group">
        	<label for="G1136_C17526">Estado de la tarea</label>
            <select class="form-control input-sm reintento" name="reintento" id="G1136_C17526">
                <option value="0">Seleccione</option>
                <?php
                	$Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC 
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 854;";
	                $obj = $mysqli->query($Lsql);
	                while($obje = $obj->fetch_object()){
	                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
	                }      
                ?>
            </select>     
        </div>
    </div>
    <div class="col-md-2 col-xs-2" style="text-align: center;">
    	<label for="G1136_C17526" style="visibility:hidden;">Estado de la tarea</label>
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Guardar Gesti&oacute;n
        </button>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G1136_C17531" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/pies.php");?>
<script type="text/javascript" src="formularios/G1136/G1136_eventos.js"></script> 
<script type="text/javascript">
    $(function(){

    	var meses = new Array(12);
    	meses[0] = "01";
    	meses[1] = "02";
    	meses[2] = "03";
    	meses[3] = "04";
    	meses[4] = "05";
    	meses[5] = "06";
    	meses[6] = "07";
    	meses[7] = "08";
    	meses[8] = "09";
    	meses[9] = "10";
    	meses[10] = "11";
    	meses[11] = "12";

    	var d = new Date();
        var h = d.getHours();
        var horas = (h < 10) ? '0' + h : h;
        var dia = d.getDate();
        var dias = (dia < 10) ? '0' + dia : dia;
        var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
        $("#FechaInicio").val(fechaInicial);
            

        //Esta es por si lo llaman en modo formulario de edicion LigthBox
        <?php if(isset($_GET['registroId'])){ ?>
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
            dataType : 'json',
            success  : function(data){
                //recorrer datos y enviarlos al formulario
                $.each(data, function(i, item) {
                    
 
                    $("#G1136_C20664").val(item.G1136_C20664);

                    $("#G1136_CodigoMiembro").val(item.G1136_CodigoMiembro);

                    $("#G1136_Usuario").val(item.G1136_Usuario);
 
                    $("#G1136_C17513").val(item.G1136_C17513);
 
                    $("#G1136_C17514").val(item.G1136_C17514);
 
                    $("#G1136_C20334").val(item.G1136_C20334);
 
                    $("#G1136_C20665").val(item.G1136_C20665);
 
                    $("#G1136_C20666").val(item.G1136_C20666);
 
                    $("#G1136_C17515").val(item.G1136_C17515);
 
                    $("#G1136_C17516").val(item.G1136_C17516);
 
                    $("#G1136_C17517").val(item.G1136_C17517);
 
                    $("#G1136_C17518").val(item.G1136_C17518);
 
                    $("#G1136_C17519").val(item.G1136_C17519);
 
                    $("#G1136_C17520").val(item.G1136_C17520);
 
                    $("#G1136_C17521").val(item.G1136_C17521);
 
                    $("#G1136_C17522").val(item.G1136_C17522);
 
                    $("#G1136_C17523").val(item.G1136_C17523);
 
                    $("#G1136_C17524").val(item.G1136_C17524);
 
                    $("#G1136_C18891").val(item.G1136_C18891);
 
                    $("#G1136_C18892").val(item.G1136_C18892);
 
                    $("#G1136_C18893").val(item.G1136_C18893);
 
                    $("#G1136_C20953").val(item.G1136_C20953);
 
                    $("#G1136_C18894").val(item.G1136_C18894);
 
                    // $("#G1136_C22654").val(item.G1136_C22654);
 
                    $("#G1136_C22655").val(item.G1136_C22655);
 
                    $("#G1136_C17525").val(item.G1136_C17525);
 
                    $("#G1136_C17526").val(item.G1136_C17526);
 
                    $("#G1136_C17527").val(item.G1136_C17527);
 
                    $("#G1136_C17528").val(item.G1136_C17528);
 
                    $("#G1136_C17529").val(item.G1136_C17529);
 
                    $("#G1136_C17530").val(item.G1136_C17530);
 
                    $("#G1136_C17531").val(item.G1136_C17531);
 
                    $("#G1136_C17532").val(item.G1136_C17532);
 
                    $("#G1136_C17533").val(item.G1136_C17533);
 
                    $("#G1136_C17534").val(item.G1136_C17534);
 
                    $("#G1136_C17535").val(item.G1136_C17535);
 
                    $("#G1136_C17537").val(item.G1136_C17537);
   
                    if(item.G1136_C17536 == 1){
                        $("#G1136_C17536").attr('checked', true);
                    } 
 
                    $("#G1136_C17539").val(item.G1136_C17539);
   
                    if(item.G1136_C17538 == 1){
                        $("#G1136_C17538").attr('checked', true);
                    } 
 
                    $("#G1136_C17540").val(item.G1136_C17540);
 
                    $("#G1136_C18644").val(item.G1136_C18644);
 
                    $("#G1136_C20948").val(item.G1136_C20948);
 
                    $("#G1136_C20949").val(item.G1136_C20949);
 
                    $("#G1136_C20950").val(item.G1136_C20950);
 
                    $("#G1136_C20951").val(item.G1136_C20951);
 
                    $("#G1136_C20952").val(item.G1136_C20952);
 
                    $("#G1136_C18895").val(item.G1136_C18895);
 
                    $("#G1136_C18896").val(item.G1136_C18896);
 
                    $("#G1136_C18897").val(item.G1136_C18897);
 
                    $("#G1136_C18897").val(item.G1136_C18897).trigger("change"); 
 
                    $("#G1136_C18898").val(item.G1136_C18898);
 
                    $("#G1136_C18899").val(item.G1136_C18899);
 
                    $("#G1136_C18900").val(item.G1136_C18900);

					$("#h3mio").html(item.principal);

                });
                //Deshabilitar los campos

                //Habilitar todos los campos para edicion
                $('#FormularioDatos :input').each(function(){
                    $(this).attr('disabled', true);
                });

                              

                //Habilidar los botones de operacion, add, editar, eliminar
                $("#add").attr('disabled', false);
                $("#edit").attr('disabled', false);
                $("#delete").attr('disabled', false);

                //Desahabiliatra los botones de salvar y seleccionar_registro
                $("#cancel").attr('disabled', true);
                $("#Save").attr('disabled', true);
            } 
        });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

     	$("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
        	/*
        	vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
        	idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G1136_C18644").select2();

    $("#G1136_C20948").select2();

    $("#G1136_C20949").select2();

    $("#G1136_C20950").select2();
        //datepickers
        

        $("#G1136_C18893").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1136_C18894").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1136_C17529").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1136_C20951").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1136_C17530").wickedpicker(options);

        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora de lectura del contrato ', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1136_C20952").wickedpicker(options);

        //Validaciones numeros Enteros
        

    	$("#G1136_C18891").numeric();
		        
    	$("#G1136_C17527").numeric();
		        
    	$("#G1136_C17528").numeric();
		        
    	$("#G1136_C18898").numeric();
		        
    	$("#G1136_C18900").numeric();
		        

        //Validaciones numeros Decimales
        

        $("#G1136_C17537").numeric({ decimal : ".",  negative : false, scale: 4 });
		        
        $("#G1136_C17539").numeric({ decimal : ".",  negative : false, scale: 4 });
		        

        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO CLIENTE 

    $("#G1136_C18892").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ¿Es ó ha sido cliente de Fincaraiz? 

    $("#G1136_C18895").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz? 

    $("#G1136_C18896").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Cupo autorizado 

    $("#G1136_C18899").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Categoría 

    $("#G1136_C18644").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Pack adquirido 

    $("#G1136_C20948").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Tiempo 

    $("#G1136_C20949").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Descuento  

    $("#G1136_C20950").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });


        $("#G1136_C17525").change(function(){
        	var id = $(this).attr('id');
            var valor = $("#"+ id +" option:selected").attr('efecividad');
            var monoef = $("#"+ id +" option:selected").attr('monoef');
            var TipNoEF = $("#"+ id +" option:selected").attr('TipNoEF');
            var cambio = $("#"+ id +" option:selected").attr('cambio');
            var importancia = $("#"+ id + " option:selected").attr('importancia');
            var contacto = $("#"+id+" option:selected").attr('contacto');
            $(".reintento").val(TipNoEF).change();
            $("#Efectividad").val(valor);
            $("#MonoEf").val(monoef);
            $("#TipNoEF").val(TipNoEF);
            $("#MonoEfPeso").val(importancia);
            $("#ContactoMonoEf").val(contacto);
    	});
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
        	$("#Save").attr('disabled', true);
        	var bol_respuesta = before_save();
        	var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
            	alertify.error("Es necesaria la tipificación!");
            	valido = 1;
            }

            if($(".reintento").val() == '2'){
            	if($(".TxtFechaReintento").val().length < 1){
            		alertify.error("Es necesario llenar la fecha de reintento!");
            		$(".TxtFechaReintento").focus();
            		valido = 1;
            	}

            	if($(".TxtHoraReintento").val().length < 1){
            		alertify.error("Es necesario llenar la hora de reintento!");
            		$(".TxtHoraReintento").focus();
            		valido = 1;
            	}
            }

            if(valido == '0'){
	        	if(bol_respuesta){            
		            var form = $("#FormularioDatos");
		            //Se crean un array con los datos a enviar, apartir del formulario 
		            var formData = new FormData($("#FormularioDatos")[0]);
		            if($("#G1136_C17525 option:selected").text() == 'Devuelta'){
	                	formData.append("tareaDevuelta","SI");
	                }
		            $.ajax({
		               url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "568"; } ?>',  
		                type: 'POST',
		                data: formData,
		                cache: false,
		                contentType: false,
		                processData: false,
		                //una vez finalizado correctamente
		                success: function(data){
		                    if(data != '0'){
		                    	$("#Save").attr('disabled', false);
			                    alertify.success('Información guardada con exito');   
		                    }else{
		                        //Algo paso, hay un error
		                        $("#Save").attr('disabled', false);
		                        alertify.error('Un error ha ocurrido y no pudimos guardar la información');
		                    }                
		                },
		                //si ha ocurrido un error
		                error: function(){
		                    after_save_error();
		                    $("#Save").attr('disabled', false);
		                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
		                }
		            });
          		}
          	}
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto comercial','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','Hora de activación ','FECHA VENCIMIENTO','AGENTE-CALL','Agente-Call','PASO_ID','REGISTRO_ID','Agente','Fecha','Hora','Campaña','Cupo usados','Usados','Cupo nuevos','Nuevos','Observación','Categoría','Pack adquirido','Tiempo','Descuento ','Fecha de lectura del contrato','Hora de lectura del contrato ','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

	                ,
	                { 
	                    name:'G1136_C20664', 
	                    index: 'G1136_C20664', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_CodigoMiembro', 
	                    index: 'G1136_CodigoMiembro', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_Usuario', 
	                    index: 'G1136_Usuario', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,

	                { 
	                    name:'G1136_C17513', 
	                    index: 'G1136_C17513', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17514', 
	                    index: 'G1136_C17514', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C20334', 
	                    index: 'G1136_C20334', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C20665', 
	                    index: 'G1136_C20665', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C20666', 
	                    index: 'G1136_C20666', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17515', 
	                    index: 'G1136_C17515', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17516', 
	                    index: 'G1136_C17516', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17517', 
	                    index: 'G1136_C17517', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17518', 
	                    index: 'G1136_C17518', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17519', 
	                    index: 'G1136_C17519', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17520', 
	                    index: 'G1136_C17520', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17521', 
	                    index: 'G1136_C17521', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17522', 
	                    index: 'G1136_C17522', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17523', 
	                    index: 'G1136_C17523', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17524', 
	                    index: 'G1136_C17524', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1136_C18891', 
	                    index:'G1136_C18891', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1136_C18892', 
	                    index:'G1136_C18892', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1136_C18892'
	                    }
	                }

	                ,
	                {  
	                    name:'G1136_C18893', 
	                    index:'G1136_C18893', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1136_C20953', 
	                    index: 'G1136_C20953', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                {  
	                    name:'G1136_C18894', 
	                    index:'G1136_C18894', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                

	                ,
	                { 
	                    name:'G1136_C22655', 
	                    index: 'G1136_C22655', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1136_C17527', 
	                    index:'G1136_C17527', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }
 
	                ,
	                {  
	                    name:'G1136_C17528', 
	                    index:'G1136_C17528', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1136_C17532', 
	                    index: 'G1136_C17532', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17533', 
	                    index: 'G1136_C17533', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17534', 
	                    index: 'G1136_C17534', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C17535', 
	                    index: 'G1136_C17535', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                {  
	                    name:'G1136_C17537', 
	                    index:'G1136_C17537', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1136_C17536', 
	                    index:'G1136_C17536', 
	                    width:70 ,
	                    editable: true, 
	                    edittype:"checkbox",
	                    editoptions: {
	                        value:"1:0"
	                    } 
	                }

	                ,
	                {  
	                    name:'G1136_C17539', 
	                    index:'G1136_C17539', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1136_C17538', 
	                    index:'G1136_C17538', 
	                    width:70 ,
	                    editable: true, 
	                    edittype:"checkbox",
	                    editoptions: {
	                        value:"1:0"
	                    } 
	                }

	                ,
	                { 
	                    name:'G1136_C17540', 
	                    index:'G1136_C17540', 
	                    width:150, 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1136_C18644', 
	                    index:'G1136_C18644', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=911&campo=G1136_C18644'
	                    }
	                }

	                ,
	                { 
	                    name:'G1136_C20948', 
	                    index:'G1136_C20948', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=978&campo=G1136_C20948'
	                    }
	                }

	                ,
	                { 
	                    name:'G1136_C20949', 
	                    index:'G1136_C20949', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=979&campo=G1136_C20949'
	                    }
	                }

	                ,
	                { 
	                    name:'G1136_C20950', 
	                    index:'G1136_C20950', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=980&campo=G1136_C20950'
	                    }
	                }

	                ,
	                {  
	                    name:'G1136_C20951', 
	                    index:'G1136_C20951', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1136_C20952', 
	                    index:'G1136_C20952', 
	                    width:70 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            //Timepicker
	                            var options = { 
	                                now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
	                                twentyFour: true, //Display 24 hour format, defaults to false
	                                title: 'Hora de lectura del contrato ', //The Wickedpicker's title,
	                                showSeconds: true, //Whether or not to show seconds,
	                                secondsInterval: 1, //Change interval for seconds, defaults to 1
	                                minutesInterval: 1, //Change interval for minutes, defaults to 1
	                                beforeShow: null, //A function to be called before the Wickedpicker is shown
	                                show: null, //A function to be called when the Wickedpicker is shown
	                                clearable: false, //Make the picker's input clearable (has clickable "x")
	                            }; 
	                            $(el).wickedpicker(options);
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1136_C18895', 
	                    index:'G1136_C18895', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1136_C18895'
	                    }
	                }

	                ,
	                { 
	                    name:'G1136_C18896', 
	                    index:'G1136_C18896', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1136_C18896'
	                    }
	                }

	                ,
	                { 
	                    name:'G1136_C18897', 
	                    index:'G1136_C18897', 
	                    width:300 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1136_C18897=si',
	                        dataInit:function(el){
	                        	$(el).select2();
	                            /*$(el).select2({ 
	                                templateResult: function(data) {
	                                    var r = data.text.split('|');
	                                    var row = '<div class="row">';
	                                    var totalRows = 12 / r.length;
	                                    for(i= 0; i < r.length; i++){
	                                        row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
	                                    }
	                                    row += '</div>';
	                                    var $result = $(row);
	                                    return $result;
	                                },
	                                templateSelection : function(data){
	                                    var r = data.text.split('|');
	                                    return r[0];
	                                }
	                            });*/
	                            $(el).change(function(){
	                                var valores = $(el + " option:selected").attr("llenadores");
	                                var campos =  $(el + " option:selected").attr("dinammicos");
	                                var r = valores.split('|');
	                                if(r.length > 1){

	                                    var c = campos.split('|');
	                                    for(i = 1; i < r.length; i++){
	                                        $("#"+ rowid +"_"+c[i]).val(r[i]);
	                                    }
	                                }
	                            });
	                        }
	                    }
	                }
 
	                ,
	                {  
	                    name:'G1136_C18898', 
	                    index:'G1136_C18898', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1136_C18899', 
	                    index:'G1136_C18899', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=977&campo=G1136_C18899'
	                    }
	                }
 
	                ,
	                {  
	                    name:'G1136_C18900', 
	                    index:'G1136_C18900', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    

                    $("#"+ rowid +"_G1136_C18897").change(function(){
                        var valores = $("#"+ rowid +"_G1136_C18897 option:selected").attr("llenadores");
                        var campos = $("#"+ rowid +"_G1136_C18897 option:selected").attr("dinammicos");
                        var r = valores.split('|');

                        if(r.length > 1){

                            var c = campos.split('|');
                            for(i = 1; i < r.length; i++){
                                if(!$("#"+c[i]).is("select")) {
                                // the input field is not a select
                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                }else{
                                    var change = r[i].replace(' ', ''); 
                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                }
                                
                            }
                        }
                    });
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1136_C17513',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(x){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', Busqueda : x  , G1136_C20664 : $("#busq_G1136_C20664").val() , G1136_C17513 : $("#busq_G1136_C17513").val() , G1136_C17514 : $("#busq_G1136_C17514").val() , G1136_C17515 : $("#busq_G1136_C17515").val(), CodMiembro : $("#busq_G1136_CodigoMiembro").val() },
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    var strIconoBackOffice = '';
                    if(data[i].estado == '1'){
                        strIconoBackOffice = 'En gestión';
                    }else if(data[i].estado == '2'){
                        strIconoBackOffice = 'Cerrada';
                    }else if(data[i].estado == '3'){
                        strIconoBackOffice = 'Devuelta';
                    }else if(data[i].estado == '4'){
                        strIconoBackOffice = 'Sin gestión';
                    }
                    

                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"<span style='position: relative;right: 2px;float: right;font-size:10px;"+ data[i].color+ "'>"+strIconoBackOffice+"</span></p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');
            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G1136_C20664").val(item.G1136_C20664);

                        $("#G1136_CodigoMiembro").val(item.G1136_CodigoMiembro);

                    	$("#G1136_Usuario").val(item.G1136_Usuario);

                        $("#G1136_C17513").val(item.G1136_C17513);

                        $("#G1136_C17514").val(item.G1136_C17514);

                        $("#G1136_C20334").val(item.G1136_C20334);

                        $("#G1136_C20665").val(item.G1136_C20665);

                        $("#G1136_C20666").val(item.G1136_C20666);

                        $("#G1136_C17515").val(item.G1136_C17515);

                        $("#G1136_C17516").val(item.G1136_C17516);

                        $("#G1136_C17517").val(item.G1136_C17517);

                        $("#G1136_C17518").val(item.G1136_C17518);

                        $("#G1136_C17519").val(item.G1136_C17519);

                        $("#G1136_C17520").val(item.G1136_C17520);

                        $("#G1136_C17521").val(item.G1136_C17521);

                        $("#G1136_C17522").val(item.G1136_C17522);

                        $("#G1136_C17523").val(item.G1136_C17523);

                        $("#G1136_C17524").val(item.G1136_C17524);

                        $("#G1136_C18891").val(item.G1136_C18891);

                        $("#G1136_C18892").val(item.G1136_C18892);
 
        	            $("#G1136_C18892").val(item.G1136_C18892).trigger("change"); 

                        $("#G1136_C18893").val(item.G1136_C18893);

                        $("#G1136_C20953").val(item.G1136_C20953);

                        $("#G1136_C18894").val(item.G1136_C18894);

                        // $("#G1136_C22654").val(item.G1136_C22654);

                        $("#G1136_C22655").val(item.G1136_C22655);

                        $("#G1136_C17525").val(item.G1136_C17525);
 
        	            $("#G1136_C17525").val(item.G1136_C17525).trigger("change"); 

                        $("#G1136_C17526").val(item.G1136_C17526);
 
        	            $("#G1136_C17526").val(item.G1136_C17526).trigger("change"); 

                        $("#G1136_C17527").val(item.G1136_C17527);

                        $("#G1136_C17528").val(item.G1136_C17528);

                        $("#G1136_C17529").val(item.G1136_C17529);

                        $("#G1136_C17530").val(item.G1136_C17530);

                        $("#G1136_C17531").val(item.G1136_C17531);

                        $("#G1136_C17532").val(item.G1136_C17532);

                        $("#G1136_C17533").val(item.G1136_C17533);

                        $("#G1136_C17534").val(item.G1136_C17534);

                        $("#G1136_C17535").val(item.G1136_C17535);

                        $("#G1136_C17537").val(item.G1136_C17537);
    
                        if(item.G1136_C17536 == 1){
                           $("#G1136_C17536").attr('checked', true);
                        } 

                        $("#G1136_C17539").val(item.G1136_C17539);
    
                        if(item.G1136_C17538 == 1){
                           $("#G1136_C17538").attr('checked', true);
                        } 

                        $("#G1136_C17540").val(item.G1136_C17540);

                        $("#G1136_C18644").val(item.G1136_C18644);
 
        	            $("#G1136_C18644").val(item.G1136_C18644).trigger("change"); 

                        $("#G1136_C20948").val(item.G1136_C20948);
 
        	            $("#G1136_C20948").val(item.G1136_C20948).trigger("change"); 

                        $("#G1136_C20949").val(item.G1136_C20949);
 
        	            $("#G1136_C20949").val(item.G1136_C20949).trigger("change"); 

                        $("#G1136_C20950").val(item.G1136_C20950);
 
        	            $("#G1136_C20950").val(item.G1136_C20950).trigger("change"); 

                        $("#G1136_C20951").val(item.G1136_C20951);

                        $("#G1136_C20952").val(item.G1136_C20952);

                        $("#G1136_C18895").val(item.G1136_C18895);
 
        	            $("#G1136_C18895").val(item.G1136_C18895).trigger("change"); 

                        $("#G1136_C18896").val(item.G1136_C18896);
 
        	            $("#G1136_C18896").val(item.G1136_C18896).trigger("change"); 

                        $("#G1136_C18897").val(item.G1136_C18897);
 
        	            $("#G1136_C18897").val(item.G1136_C18897).trigger("change"); 

                        $("#G1136_C18898").val(item.G1136_C18898);

                        $("#G1136_C18899").val(item.G1136_C18899);
 
        	            $("#G1136_C18899").val(item.G1136_C18899).trigger("change"); 

                        $("#G1136_C18900").val(item.G1136_C18900);
        				$("#h3mio").html(item.principal);
        				
                    });

               
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();

            $.ajax({
	        	url   : '<?php echo $url_crud; ?>',
	        	type  : 'post',
	        	data  : { DameHistorico : 'si', user_codigo_mien : idTotal, campana_crm : '<?php if(isset($_GET['campana_crm'])) { echo $_GET['campana_crm']; } else { echo "568"; } ?>' },
	        	dataType : 'html',
	        	success : function(data){
	        		$("#tablaGestiones").html('');
	        		$("#tablaGestiones").html(data);
	        	}

	    	});
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
	$(document).ready(function() {
		<?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                		while($objDatos = $resultDatosSql->fetch_object()){ ?>
                        	document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
             		<?php  
             			}	
                    }
                    
                } */  
            }
        ?>
    	<?php if(isset($_GET['user'])){ ?>
        	
        	idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
		
	});
</script>
