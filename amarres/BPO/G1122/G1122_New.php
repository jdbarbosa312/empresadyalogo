
<?php date_default_timezone_set('America/Bogota'); ?>
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1122/G1122_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

	$PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

	if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1122_ConsInte__b as id, G1122_C17121 as camp1 , G1122_C17122 as camp2 FROM ".$BaseDatos.".G1122  WHERE G1122_Usuario = ".$idUsuario." ORDER BY G1122_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1122_ConsInte__b as id, G1122_C17121 as camp1 , G1122_C17122 as camp2 FROM ".$BaseDatos.".G1122  ORDER BY G1122_ConsInte__b DESC LIMIT 0, 50";
        }
    }else{
        $Zsql = "SELECT G1122_ConsInte__b as id, G1122_C17121 as camp1 , G1122_C17122 as camp2 FROM ".$BaseDatos.".G1122  ORDER BY G1122_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<?php
if(isset($_GET['user'])){


	$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

	$XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
	$nombre = $mysqli->query($XLsql);
	$nombreUsuario = NULL;
	//echo $XLsql;
	while ($key = $nombre->fetch_object()) {
	 	echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
	 	$nombreUsuario = $key->nombre;
	 	break;
	} 


	if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


					
		$data = array(	"strToken_t" => $_GET['token'], 
						"strIdGestion_t" => $_GET['id_gestion_cbx'],
						"strDatoPrincipal_t" => $nombreUsuario,
						"strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
		$data_string = json_encode($data);    

		$ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
		//especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                      
		); 
		//recogemos la respuesta
		$respuesta = curl_exec ($ch);
		//o el error, por si falla
		$error = curl_error($ch);
		//y finalmente cerramos curl
		//echo "Respuesta =>  ". $respuesta;
		//echo "<br/>Error => ".$error;
		//include "Log.class.php";
		//$log = new Log("log", "./Log/");
		//$log->insert($error, $respuesta, false, true, false);
		//echo "nada";
		curl_close ($ch);
	}
}else{
	echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";	
}
?>

<?php if(isset($_GET['user'])){ ?>
<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="box">
			<div class="box-body">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
						<tr>
							<th>Gesti&oacute;n</th>
							<th>Comentarios</th>
							<th>Fecha - hora</th>
							<th>Agente</th>
						</tr>
					</thead>
					<tbody>
						<?php

							$Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


							$res = $mysqli->query($Lsql);
							while($key = $res->fetch_object()){
								echo "<tr>";
								echo "<td>".($key->MONOEF_Texto_____b)."</td>";
								echo "<td>".$key->CONDIA_Observacio_b."</td>";
								echo "<td>".$key->CONDIA_Fecha_____b."</td>";
								echo "<td>".$key->USUARI_Nombre____b."</td>";
								echo "</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2140">
                DATOS DE LA BASE DE DATOS
            </a>
        </h4>
    </div>
    <div id="s_2140" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1122_C20661" id="LblG1122_C20661">Código Cliente </label>
			            <input type="text" class="form-control input-sm Numerico" value="<?php $Lsql = "SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 20661"; $res = $mysqli->query($Lsql); $dato = $res->fetch_array(); echo ($dato["CONTADORES_Valor_b"] + 1); $XLsql = "UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = CONTADORES_Valor_b + 1 WHERE CONTADORES_ConsInte__PREGUN_b = 20661"; $mysqli->query($XLsql);?>"  name="G1122_C20661" id="G1122_C20661" placeholder="Código Cliente ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17121" id="LblG1122_C17121">RAZON SOCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17121" value=""  name="G1122_C17121"  placeholder="RAZON SOCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17122" id="LblG1122_C17122">NOMBRE COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17122" value=""  name="G1122_C17122"  placeholder="NOMBRE COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C18857" id="LblG1122_C18857">CONTACTO COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1122_C18857" value=""  name="G1122_C18857"  placeholder="CONTACTO COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C20662" id="LblG1122_C20662">ID Contacto Comercial </label>
			            <input type="text" class="form-control input-sm" id="G1122_C20662" value=""  name="G1122_C20662"  placeholder="ID Contacto Comercial ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1122_C20663" id="LblG1122_C20663">Cuidad de expedición </label>
			            <input type="text" class="form-control input-sm Fecha" value=""  name="G1122_C20663" id="G1122_C20663" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17123" id="LblG1122_C17123">CEDULA NIT</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17123" value=""  name="G1122_C17123"  placeholder="CEDULA NIT">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17124" id="LblG1122_C17124">TEL 1</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17124" value=""  name="G1122_C17124"  placeholder="TEL 1">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17125" id="LblG1122_C17125">TEL 2</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17125" value=""  name="G1122_C17125"  placeholder="TEL 2">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17126" id="LblG1122_C17126">TEL 3</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17126" value=""  name="G1122_C17126"  placeholder="TEL 3">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17127" id="LblG1122_C17127">CIUDAD</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17127" value=""  name="G1122_C17127"  placeholder="CIUDAD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17128" id="LblG1122_C17128">DEPARTAMENTO</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17128" value=""  name="G1122_C17128"  placeholder="DEPARTAMENTO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17129" id="LblG1122_C17129">MAIL</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17129" value=""  name="G1122_C17129"  placeholder="MAIL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17130" id="LblG1122_C17130">DIRECCION</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17130" value=""  name="G1122_C17130"  placeholder="DIRECCION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17131" id="LblG1122_C17131">FECHA DE CARGUE</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17131" value=""  name="G1122_C17131"  placeholder="FECHA DE CARGUE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17132" id="LblG1122_C17132">CLASIFICACION</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17132" value=""  name="G1122_C17132"  placeholder="CLASIFICACION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1122_C18858" id="LblG1122_C18858">CUPO CLIENTE</label>
			            <input type="text" class="form-control input-sm Numerico" value="" readonly name="G1122_C18858" id="G1122_C18858" placeholder="CUPO CLIENTE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1122_C18351" id="LblG1122_C18351">ESTADO CLIENTE</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1122_C18351" id="G1122_C18351">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 895 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1122_C18859" id="LblG1122_C18859">FECHA ACTIVACION</label>
			            <input type="text" class="form-control input-sm Fecha" value=""  name="G1122_C18859" id="G1122_C18859" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1122_C18860" id="LblG1122_C18860">FECHA VENCIMIENTO</label>
			            <input type="text" class="form-control input-sm Fecha" value=""  name="G1122_C18860" id="G1122_C18860" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>

  
        </div>


        </div>
    </div>
</div>

<div id="2142" style='display:none;'>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17138" id="LblG1122_C17138">Agente</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17138" value="<?php echo getNombreUser($token);?>" readonly name="G1122_C17138"  placeholder="Agente">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17139" id="LblG1122_C17139">Fecha</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17139" value="<?php echo date('Y-m-d');?>" readonly name="G1122_C17139"  placeholder="Fecha">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17140" id="LblG1122_C17140">Hora</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17140" value="<?php echo date('H:i:s');?>" readonly name="G1122_C17140"  placeholder="Hora">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C17141" id="LblG1122_C17141">Campaña</label>
			            <input type="text" class="form-control input-sm" id="G1122_C17141" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÑA";}?>" readonly name="G1122_C17141"  placeholder="Campaña">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2144">
                DATOS DE PERFIL
            </a>
        </h4>
    </div>
    <div id="s_2144" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1122_C17144" id="LblG1122_C17144">¿Es ó ha sido cliente de Fincaraiz?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1122_C17144" id="G1122_C17144">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1122_C17145" id="LblG1122_C17145">¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1122_C17145" id="G1122_C17145">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			       
			        <!-- CAMPO DE TIPO GUION -->
			        <div class="form-group">
			            <label for="G1122_C17146" id="LblG1122_C17146">¿Desde que departamento realiza la administración de los inmuebles?</label>
			            <select class="form-control input-sm select2" style="width: 100%;"  name="G1122_C17146" id="G1122_C17146">
			                <option value="0">Seleccione</option>

			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1122_C18721" id="LblG1122_C18721">Cupo autorizado</label>
			            <input type="text" class="form-control input-sm" id="G1122_C18721" value=""  name="G1122_C18721"  placeholder="Cupo autorizado">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1122_C18720" id="LblG1122_C18720">¿Cuántos inmuebles USADOS tiene en su inventario?</label>
			            <input type="text" class="form-control input-sm Numerico" value=""  name="G1122_C18720" id="G1122_C18720" placeholder="¿Cuántos inmuebles USADOS tiene en su inventario?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1122_C17147" id="LblG1122_C17147">¿Cuántos inmuebles NUEVOS tiene en su inventario?</label>
			            <input type="text" class="form-control input-sm Numerico" value=""  name="G1122_C17147" id="G1122_C17147" placeholder="¿Cuántos inmuebles NUEVOS tiene en su inventario?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>

  
        </div>


        </div>
    </div>
</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2468">
                HISTORICO
            </a>
        </h4>
    </div>
    <div id="s_2468" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
            </div>


        </div>


        </div>
    </div>
</div>

<div id="2141" >


</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2145">
                CLASIFICACION DE LA GESTION
            </a>
        </h4>
    </div>
    <div id="s_2145" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1122_C17149" id="LblG1122_C17149">ESTADO</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1122_C17149" id="G1122_C17149">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 828 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1122_C17150" id="LblG1122_C17150">TIPIFICACION 1</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1122_C17150" id="G1122_C17150">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 829 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1122_C17151" id="LblG1122_C17151">TIPIFICACION 2</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1122_C17151" id="G1122_C17151">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 830 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1122_C17152" id="LblG1122_C17152">TIPIFICACION 3</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1122_C17152" id="G1122_C17152">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 831 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO MEMO -->
			        <div class="form-group">
			            <label for="G1122_C20691" id="LblG1122_C20691">COMENTARIOS BACKOFFICE </label>
			            <textarea class="form-control input-sm" name="G1122_C20691" id="G1122_C20691"  value="" placeholder="COMENTARIOS BACKOFFICE "></textarea>
			        </div>
			        <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div>


        </div>


        </div>
    </div>
</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2493">
                BACKOFFICE
            </a>
        </h4>
    </div>
    <div id="s_2493" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
            </div>


            <div class="col-md-6 col-xs-6">

  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
            </div>


            <div class="col-md-6 col-xs-6">

  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
            </div>


            <div class="col-md-6 col-xs-6">

  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
            </div>


            <div class="col-md-6 col-xs-6">

  
            </div>

  
        </div>


        </div>
    </div>
</div>

<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<hr/>
<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">

        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">HISTÓRICO </a>
        </li>

        <li class="">
            <a href="#tab_1" data-toggle="tab" id="tabs_click_1">Enviar tutoriales</a>
        </li>

        <li class="">
            <a href="#tab_2" data-toggle="tab" id="tabs_click_2">Enviar Propuesta</a>
        </li>

        <li class="">
            <a href="#tab_3" data-toggle="tab" id="tabs_click_3">Enviar Link</a>
        </li>

        <li class="">
            <a href="#tab_4" data-toggle="tab" id="tabs_click_4">Crear orden</a>
        </li>

        <li class="">
            <a href="#tab_5" data-toggle="tab" id="tabs_click_5">Cliente a escalar</a>
        </li>

        <li class="">
            <a href="#tab_6" data-toggle="tab" id="tabs_click_6">Enviar presentación</a>
        </li>

        <li class="">
            <a href="#tab_7" data-toggle="tab" id="tabs_click_7">Sacar de lista negra</a>
        </li>

        <li class="">
            <a href="#tab_8" data-toggle="tab" id="tabs_click_8">Solicitud especial</a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
            
        </div>

        <div class="tab-pane " id="tab_1"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless1" width="100%">
            </table>
            <div id="pagerDetalles1">
            </div> 
            <button title="Crear Enviar tutoriales" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_1"><i class="fa fa-plus"></i></button>
        </div>

        <div class="tab-pane " id="tab_2"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless2" width="100%">
            </table>
            <div id="pagerDetalles2">
            </div> 
            <button title="Crear Enviar Propuesta" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_2"><i class="fa fa-plus"></i></button>
        </div>

        <div class="tab-pane " id="tab_3"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless3" width="100%">
            </table>
            <div id="pagerDetalles3">
            </div> 
            <button title="Crear Enviar Link" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_3"><i class="fa fa-plus"></i></button>
        </div>

        <div class="tab-pane " id="tab_4"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless4" width="100%">
            </table>
            <div id="pagerDetalles4">
            </div> 
            <button title="Crear Crear orden" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_4"><i class="fa fa-plus"></i></button>
        </div>

        <div class="tab-pane " id="tab_5"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless5" width="100%">
            </table>
            <div id="pagerDetalles5">
            </div> 
            <button title="Crear Cliente a escalar" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_5"><i class="fa fa-plus"></i></button>
        </div>

        <div class="tab-pane " id="tab_6"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless6" width="100%">
            </table>
            <div id="pagerDetalles6">
            </div> 
            <button title="Crear Enviar presentación" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_6"><i class="fa fa-plus"></i></button>
        </div>

        <div class="tab-pane " id="tab_7"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless7" width="100%">
            </table>
            <div id="pagerDetalles7">
            </div> 
            <button title="Crear Sacar de lista negra" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_7"><i class="fa fa-plus"></i></button>
        </div>

        <div class="tab-pane " id="tab_8"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless8" width="100%">
            </table>
            <div id="pagerDetalles8">
            </div> 
            <button title="Crear Solicitud especial" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_8"><i class="fa fa-plus"></i></button>
        </div>

    </div>

</div>

<div class="row" style="background-color: #FAFAFA; ">
	<br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1122_C17133">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 826;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
   	<div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1122_C17133">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 826;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G1122_C17134">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G1122_C17135" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G1122_C17136" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G1122_C17137" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G1122/G1122_eventos.js"></script> 
<script type="text/javascript">
    $(function(){

    	var meses = new Array(12);
    	meses[0] = "01";
    	meses[1] = "02";
    	meses[2] = "03";
    	meses[3] = "04";
    	meses[4] = "05";
    	meses[5] = "06";
    	meses[6] = "07";
    	meses[7] = "08";
    	meses[8] = "09";
    	meses[9] = "10";
    	meses[10] = "11";
    	meses[11] = "12";

    	var d = new Date();
        var h = d.getHours();
        var horas = (h < 10) ? '0' + h : h;
        var dia = d.getDate();
        var dias = (dia < 10) ? '0' + dia : dia;
        var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
        $("#FechaInicio").val(fechaInicial);
            

        //Esta es por si lo llaman en modo formulario de edicion LigthBox
        <?php if(isset($_GET['registroId'])){ ?>
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
            dataType : 'json',
            success  : function(data){
                //recorrer datos y enviarlos al formulario
                $.each(data, function(i, item) {
                    

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id);

            $.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
            cargarHijos_1(id);

            $.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
            cargarHijos_2(id);

            $.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion Recargar 
            cargarHijos_3(id);

            $.jgrid.gridUnload('#tablaDatosDetalless4'); //funcion Recargar 
            cargarHijos_4(id);

            $.jgrid.gridUnload('#tablaDatosDetalless5'); //funcion Recargar 
            cargarHijos_5(id);

            $.jgrid.gridUnload('#tablaDatosDetalless6'); //funcion Recargar 
            cargarHijos_6(id);

            $.jgrid.gridUnload('#tablaDatosDetalless7'); //funcion Recargar 
            cargarHijos_7(id);

            $.jgrid.gridUnload('#tablaDatosDetalless8'); //funcion Recargar 
            cargarHijos_8(id);
 
                    $("#G1122_C20661").val(item.G1122_C20661);
 
                    $("#G1122_C17121").val(item.G1122_C17121);
 
                    $("#G1122_C17122").val(item.G1122_C17122);
 
                    $("#G1122_C18857").val(item.G1122_C18857);
 
                    $("#G1122_C20662").val(item.G1122_C20662);
 
                    $("#G1122_C20663").val(item.G1122_C20663);
 
                    $("#G1122_C17123").val(item.G1122_C17123);
 
                    $("#G1122_C17124").val(item.G1122_C17124);
 
                    $("#G1122_C17125").val(item.G1122_C17125);
 
                    $("#G1122_C17126").val(item.G1122_C17126);
 
                    $("#G1122_C17127").val(item.G1122_C17127);
 
                    $("#G1122_C17128").val(item.G1122_C17128);
 
                    $("#G1122_C17129").val(item.G1122_C17129);
 
                    $("#G1122_C17130").val(item.G1122_C17130);
 
                    $("#G1122_C17131").val(item.G1122_C17131);
 
                    $("#G1122_C17132").val(item.G1122_C17132);
 
                    $("#G1122_C18858").val(item.G1122_C18858);
 
                    $("#G1122_C18351").val(item.G1122_C18351);
 
                    $("#G1122_C18859").val(item.G1122_C18859);
 
                    $("#G1122_C18860").val(item.G1122_C18860);
 
                    $("#G1122_C17133").val(item.G1122_C17133);
 
                    $("#G1122_C17134").val(item.G1122_C17134);
 
                    $("#G1122_C17135").val(item.G1122_C17135);
 
                    $("#G1122_C17136").val(item.G1122_C17136);
 
                    $("#G1122_C17137").val(item.G1122_C17137);
 
                    $("#G1122_C17138").val(item.G1122_C17138);
 
                    $("#G1122_C17139").val(item.G1122_C17139);
 
                    $("#G1122_C17140").val(item.G1122_C17140);
 
                    $("#G1122_C17141").val(item.G1122_C17141);
   
                    if(item.G1122_C17142 == 1){
                        $("#G1122_C17142").attr('checked', true);
                    } 
   
                    if(item.G1122_C17143 == 1){
                        $("#G1122_C17143").attr('checked', true);
                    } 
 
                    $("#G1122_C17144").val(item.G1122_C17144);
 
                    $("#G1122_C17145").val(item.G1122_C17145);
 
                    $("#G1122_C17146").val(item.G1122_C17146);
 
                    $("#G1122_C17146").val(item.G1122_C17146).trigger("change"); 
 
                    $("#G1122_C18721").val(item.G1122_C18721);
 
                    $("#G1122_C18720").val(item.G1122_C18720);
 
                    $("#G1122_C17147").val(item.G1122_C17147);
 
                    $("#G1122_C17149").val(item.G1122_C17149);
 
                    $("#G1122_C17150").val(item.G1122_C17150);
 
                    $("#G1122_C17151").val(item.G1122_C17151);
 
                    $("#G1122_C17152").val(item.G1122_C17152);
 
                    $("#G1122_C20691").val(item.G1122_C20691);

					$("#h3mio").html(item.principal);

                });
                //Deshabilitar los campos

                //Habilitar todos los campos para edicion
                $('#FormularioDatos :input').each(function(){
                    $(this).attr('disabled', true);
                });

                              

                //Habilidar los botones de operacion, add, editar, eliminar
                $("#add").attr('disabled', false);
                $("#edit").attr('disabled', false);
                $("#delete").attr('disabled', false);

                //Desahabiliatra los botones de salvar y seleccionar_registro
                $("#cancel").attr('disabled', true);
                $("#Save").attr('disabled', true);
            } 
        });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

     	$("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        $("#btnLlamar_1").attr('padre', <?php echo $_GET['registroId'];?>);$("#btnLlamar_2").attr('padre', <?php echo $_GET['registroId'];?>);$("#btnLlamar_3").attr('padre', <?php echo $_GET['registroId'];?>);$("#btnLlamar_4").attr('padre', <?php echo $_GET['registroId'];?>);$("#btnLlamar_5").attr('padre', <?php echo $_GET['registroId'];?>);$("#btnLlamar_6").attr('padre', <?php echo $_GET['registroId'];?>);$("#btnLlamar_7").attr('padre', <?php echo $_GET['registroId'];?>);$("#btnLlamar_8").attr('padre', <?php echo $_GET['registroId'];?>);

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
        	/*$("#btnLlamar_1").attr('padre', <?php echo $_GET['user'];?>);$("#btnLlamar_2").attr('padre', <?php echo $_GET['user'];?>);$("#btnLlamar_3").attr('padre', <?php echo $_GET['user'];?>);$("#btnLlamar_4").attr('padre', <?php echo $_GET['user'];?>);$("#btnLlamar_5").attr('padre', <?php echo $_GET['user'];?>);$("#btnLlamar_6").attr('padre', <?php echo $_GET['user'];?>);$("#btnLlamar_7").attr('padre', <?php echo $_GET['user'];?>);$("#btnLlamar_8").attr('padre', <?php echo $_GET['user'];?>);
        	vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
        	idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');

            $.jgrid.gridUnload('#tablaDatosDetalless1');

            $.jgrid.gridUnload('#tablaDatosDetalless2');

            $.jgrid.gridUnload('#tablaDatosDetalless3');

            $.jgrid.gridUnload('#tablaDatosDetalless4');

            $.jgrid.gridUnload('#tablaDatosDetalless5');

            $.jgrid.gridUnload('#tablaDatosDetalless6');

            $.jgrid.gridUnload('#tablaDatosDetalless7');

            $.jgrid.gridUnload('#tablaDatosDetalless8');
            
    	$("#btnLlamar_0").attr('padre', $("#G1122_C17129").val());
    		var id_0 = $("#G1122_C17129").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
    	$("#btnLlamar_1").attr('padre', $("#G1122_C0").val());
    		var id_1 = $("#G1122_C0").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
    		cargarHijos_1(id_1);
    	$("#btnLlamar_2").attr('padre', $("#G1122_C20661").val());
    		var id_2 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
    		cargarHijos_2(id_2);
    	$("#btnLlamar_3").attr('padre', $("#G1122_C20661").val());
    		var id_3 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion Recargar 
    		cargarHijos_3(id_3);
    	$("#btnLlamar_4").attr('padre', $("#G1122_C20661").val());
    		var id_4 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless4'); //funcion Recargar 
    		cargarHijos_4(id_4);
    	$("#btnLlamar_5").attr('padre', $("#G1122_C20661").val());
    		var id_5 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless5'); //funcion Recargar 
    		cargarHijos_5(id_5);
    	$("#btnLlamar_6").attr('padre', $("#G1122_C20661").val());
    		var id_6 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless6'); //funcion Recargar 
    		cargarHijos_6(id_6);
    	$("#btnLlamar_7").attr('padre', $("#G1122_C20661").val());
    		var id_7 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless7'); //funcion Recargar 
    		cargarHijos_7(id_7);
    	$("#btnLlamar_8").attr('padre', $("#G1122_C20661").val());
    		var id_8 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless8'); //funcion Recargar 
    		cargarHijos_8(id_8);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
            cargarHijos_0(idTotal);
        });

        $("#btnLlamar_0").click(function( event ) {
            event.preventDefault(); 
            var padre = $(this).attr('padre');
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1226&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20657<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                    $("#oper").val('edit');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data){
                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                if($("#oper").val() == 'add'){
                                    idTotal = data;
                                }else{
                                    idTotal= $("#hidId").val();
                                }
                                $("#hidId").val(idTotal);
                                $("#oper").val('edit');

                                int_guardo = 1;
                                $(".llamadores").attr('padre', idTotal);
                                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1226&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20657&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                                $("#editarDatos").modal('show');
                                $("#oper").val('edit');

                            }else{
                                //Algo paso, hay un error
                                alertify.error('Un error ha ocurrido');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }else{

                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1226&view=si&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1122&pincheCampo=20657&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });

        $("#tabs_click_1").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless1'); 
            cargarHijos_1(idTotal);
        });

        $("#btnLlamar_1").click(function( event ) {
            event.preventDefault(); 
            var padre = $(this).attr('padre');
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1137&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=0<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                    $("#oper").val('edit');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data){
                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                if($("#oper").val() == 'add'){
                                    idTotal = data;
                                }else{
                                    idTotal= $("#hidId").val();
                                }
                                $("#hidId").val(idTotal);
                                $("#oper").val('edit');

                                int_guardo = 1;
                                $(".llamadores").attr('padre', idTotal);
                                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1137&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=0&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                                $("#editarDatos").modal('show');
                                $("#oper").val('edit');

                            }else{
                                //Algo paso, hay un error
                                alertify.error('Un error ha ocurrido');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }else{

                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1137&view=si&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1122&pincheCampo=0&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });

        $("#tabs_click_2").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless2'); 
            cargarHijos_2(idTotal);
        });

        $("#btnLlamar_2").click(function( event ) {
            event.preventDefault(); 
            var padre = $(this).attr('padre');
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1133&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20676<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                    $("#oper").val('edit');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data){
                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                if($("#oper").val() == 'add'){
                                    idTotal = data;
                                }else{
                                    idTotal= $("#hidId").val();
                                }
                                $("#hidId").val(idTotal);
                                $("#oper").val('edit');

                                int_guardo = 1;
                                $(".llamadores").attr('padre', idTotal);
                                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1133&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20676&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                                $("#editarDatos").modal('show');
                                $("#oper").val('edit');

                            }else{
                                //Algo paso, hay un error
                                alertify.error('Un error ha ocurrido');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }else{

                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1133&view=si&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1122&pincheCampo=20676&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });

        $("#tabs_click_3").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless3'); 
            cargarHijos_3(idTotal);
        });

        $("#btnLlamar_3").click(function( event ) {
            event.preventDefault(); 
            var padre = $(this).attr('padre');
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1134&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20670<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                    $("#oper").val('edit');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data){
                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                if($("#oper").val() == 'add'){
                                    idTotal = data;
                                }else{
                                    idTotal= $("#hidId").val();
                                }
                                $("#hidId").val(idTotal);
                                $("#oper").val('edit');

                                int_guardo = 1;
                                $(".llamadores").attr('padre', idTotal);
                                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1134&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20670&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                                $("#editarDatos").modal('show');
                                $("#oper").val('edit');

                            }else{
                                //Algo paso, hay un error
                                alertify.error('Un error ha ocurrido');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }else{

                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1134&view=si&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1122&pincheCampo=20670&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });

        $("#tabs_click_4").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless4'); 
            cargarHijos_4(idTotal);
        });

        $("#btnLlamar_4").click(function( event ) {
            event.preventDefault(); 
            var padre = $(this).attr('padre');
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1135&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20667<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                    $("#oper").val('edit');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data){
                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                if($("#oper").val() == 'add'){
                                    idTotal = data;
                                }else{
                                    idTotal= $("#hidId").val();
                                }
                                $("#hidId").val(idTotal);
                                $("#oper").val('edit');

                                int_guardo = 1;
                                $(".llamadores").attr('padre', idTotal);
                                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1135&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20667&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                                $("#editarDatos").modal('show');
                                $("#oper").val('edit');

                            }else{
                                //Algo paso, hay un error
                                alertify.error('Un error ha ocurrido');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }else{

                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1135&view=si&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1122&pincheCampo=20667&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });

        $("#tabs_click_5").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless5'); 
            cargarHijos_5(idTotal);
        });

        $("#btnLlamar_5").click(function( event ) {
            event.preventDefault(); 
            var padre = $(this).attr('padre');
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1136&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20664<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                    $("#oper").val('edit');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data){
                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                if($("#oper").val() == 'add'){
                                    idTotal = data;
                                }else{
                                    idTotal= $("#hidId").val();
                                }
                                $("#hidId").val(idTotal);
                                $("#oper").val('edit');

                                int_guardo = 1;
                                $(".llamadores").attr('padre', idTotal);
                                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1136&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20664&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                                $("#editarDatos").modal('show');
                                $("#oper").val('edit');

                            }else{
                                //Algo paso, hay un error
                                alertify.error('Un error ha ocurrido');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }else{

                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1136&view=si&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1122&pincheCampo=20664&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });

        $("#tabs_click_6").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless6'); 
            cargarHijos_6(idTotal);
        });

        $("#btnLlamar_6").click(function( event ) {
            event.preventDefault(); 
            var padre = $(this).attr('padre');
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1138&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20673<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                    $("#oper").val('edit');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data){
                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                if($("#oper").val() == 'add'){
                                    idTotal = data;
                                }else{
                                    idTotal= $("#hidId").val();
                                }
                                $("#hidId").val(idTotal);
                                $("#oper").val('edit');

                                int_guardo = 1;
                                $(".llamadores").attr('padre', idTotal);
                                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1138&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20673&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                                $("#editarDatos").modal('show');
                                $("#oper").val('edit');

                            }else{
                                //Algo paso, hay un error
                                alertify.error('Un error ha ocurrido');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }else{

                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1138&view=si&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1122&pincheCampo=20673&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });

        $("#tabs_click_7").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless7'); 
            cargarHijos_7(idTotal);
        });

        $("#btnLlamar_7").click(function( event ) {
            event.preventDefault(); 
            var padre = $(this).attr('padre');
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1139&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20682<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                    $("#oper").val('edit');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data){
                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                if($("#oper").val() == 'add'){
                                    idTotal = data;
                                }else{
                                    idTotal= $("#hidId").val();
                                }
                                $("#hidId").val(idTotal);
                                $("#oper").val('edit');

                                int_guardo = 1;
                                $(".llamadores").attr('padre', idTotal);
                                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1139&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20682&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                                $("#editarDatos").modal('show');
                                $("#oper").val('edit');

                            }else{
                                //Algo paso, hay un error
                                alertify.error('Un error ha ocurrido');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }else{

                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1139&view=si&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1122&pincheCampo=20682&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });

        $("#tabs_click_8").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless8'); 
            cargarHijos_8(idTotal);
        });

        $("#btnLlamar_8").click(function( event ) {
            event.preventDefault(); 
            var padre = $(this).attr('padre');
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1140&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20685<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                    $("#oper").val('edit');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data){
                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                if($("#oper").val() == 'add'){
                                    idTotal = data;
                                }else{
                                    idTotal= $("#hidId").val();
                                }
                                $("#hidId").val(idTotal);
                                $("#oper").val('edit');

                                int_guardo = 1;
                                $(".llamadores").attr('padre', idTotal);
                                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1140&view=si&formaDetalle=si&formularioPadre=1122&yourfather='+ idTotal +'&pincheCampo=20685&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                                $("#editarDatos").modal('show');
                                $("#oper").val('edit');

                            }else{
                                //Algo paso, hay un error
                                alertify.error('Un error ha ocurrido');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }else{

                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1140&view=si&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1122&pincheCampo=20685&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });
        //Select2 estos son los guiones
        


    $("#G1122_C18351").select2();

    $("#G1122_C17144").select2();

    $("#G1122_C17145").select2();

        $("#G1122_C17146").select2({
        	placeholder: "Buscar",
	        allowClear: false,
	        minimumInputLength: 3,
	        ajax: {
	            url: '<?=$url_crud;?>?CallDatosCombo_Guion_G1122_C17146=si',
	            dataType: 'json',
	            type : 'post',
	            delay: 250,
	            data: function (params) {
	                return {
	                    q: params.term
	                };
	            },
              	processResults: function(data) {
				  	return {
				    	results: $.map(data, function(obj) {
				      		return {
				        		id: obj.id,
				        		text: obj.text
				      		};
				    	})
				  	};
				},
	            cache: true
	        }
    	});            
        
        $("#G1122_C17146").change(function(){
            $.ajax({
        		url   : '<?php echo $url_crud;?>',
        		data  : { dameValoresCamposDinamicos_Guion_G1122_C17146 : $(this).val() },
        		type  : 'post',
        		dataType : 'json',
        		success  : function(data){
        				
        		}
        	});
        });
                                      

    $("#G1122_C17149").select2();

    $("#G1122_C17150").select2();

    $("#G1122_C17151").select2();

    $("#G1122_C17152").select2();
        //datepickers
        

        $("#G1122_C20663").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1122_C18859").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1122_C18860").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1122_C17135").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1122_C17136").wickedpicker(options);

        //Validaciones numeros Enteros
        

    	$("#G1122_C20661").numeric();
		        
    	$("#G1122_C18858").numeric();
		        
    	$("#G1122_C18720").numeric();
		        
    	$("#G1122_C17147").numeric();
		        

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO CLIENTE 

    $("#G1122_C18351").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ¿Es ó ha sido cliente de Fincaraiz? 

    $("#G1122_C17144").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz? 

    $("#G1122_C17145").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ESTADO 

    $("#G1122_C17149").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

		$.ajax({
			url    : '<?php echo $url_crud; ?>',
			type   : 'post',
			data   : { getListaHija : true , opcionID : '829' , idPadre : $(this).val() },
			success : function(data){
				$("#G1122_C17150").html(data);
			}
		});
		
    });

    //function para TIPIFICACION 1 

    $("#G1122_C17150").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

		$.ajax({
			url    : '<?php echo $url_crud; ?>',
			type   : 'post',
			data   : { getListaHija : true , opcionID : '830' , idPadre : $(this).val() },
			success : function(data){
				$("#G1122_C17151").html(data);
			}
		});
		
    });

    //function para TIPIFICACION 2 

    $("#G1122_C17151").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

		$.ajax({
			url    : '<?php echo $url_crud; ?>',
			type   : 'post',
			data   : { getListaHija : true , opcionID : '831' , idPadre : $(this).val() },
			success : function(data){
				$("#G1122_C17152").html(data);
			}
		});
		
    });

    //function para TIPIFICACION 3 

    $("#G1122_C17152").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
        	$("#Save").attr('disabled', true);
        	var bol_respuesta = before_save();
        	var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
            	alertify.error("Es necesaria la tipificación!");
            	valido = 1;
            }

            if($(".reintento").val() == '2'){
            	if($(".TxtFechaReintento").val().length < 1){
            		alertify.error("Es necesario llenar la fecha de reintento!");
            		$(".TxtFechaReintento").focus();
            		valido = 1;
            	}

            	if($(".TxtHoraReintento").val().length < 1){
            		alertify.error("Es necesario llenar la hora de reintento!");
            		$(".TxtHoraReintento").focus();
            		valido = 1;
            	}
            }

            if(valido == '0'){
	        	if(bol_respuesta){            
		            var form = $("#FormularioDatos");
		            //Se crean un array con los datos a enviar, apartir del formulario 
		            var formData = new FormData($("#FormularioDatos")[0]);
		            $.ajax({
		               url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
		                type: 'POST',
		                data: formData,
		                cache: false,
		                contentType: false,
		                processData: false,
		                //una vez finalizado correctamente
		                success: function(data){
		                    if(data != '0'){
		                    	<?php if(!isset($_GET['campan'])){ ?>
			                        //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
			                        if($("#oper").val() == 'add'){
			                            idTotal = data;
			                        }else{
			                            idTotal= $("#hidId").val();
			                        }
			                       
			                        //Limpiar formulario
			                        form[0].reset();
			                        after_save();
			                        <?php if(isset($_GET['registroId'])){ ?>
			                        $.ajax({
			                            url      : '<?=$url_crud;?>',
			                            type     : 'POST',
			                            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
			                            dataType : 'json',
			                            success  : function(data){
			                                //recorrer datos y enviarlos al formulario
			                                $.each(data, function(i, item) {
		                                    
 
		                                    	$("#G1122_C20661").val(item.G1122_C20661);
 
		                                    	$("#G1122_C17121").val(item.G1122_C17121);
 
		                                    	$("#G1122_C17122").val(item.G1122_C17122);
 
		                                    	$("#G1122_C18857").val(item.G1122_C18857);
 
		                                    	$("#G1122_C20662").val(item.G1122_C20662);
 
		                                    	$("#G1122_C20663").val(item.G1122_C20663);
 
		                                    	$("#G1122_C17123").val(item.G1122_C17123);
 
		                                    	$("#G1122_C17124").val(item.G1122_C17124);
 
		                                    	$("#G1122_C17125").val(item.G1122_C17125);
 
		                                    	$("#G1122_C17126").val(item.G1122_C17126);
 
		                                    	$("#G1122_C17127").val(item.G1122_C17127);
 
		                                    	$("#G1122_C17128").val(item.G1122_C17128);
 
		                                    	$("#G1122_C17129").val(item.G1122_C17129);
 
		                                    	$("#G1122_C17130").val(item.G1122_C17130);
 
		                                    	$("#G1122_C17131").val(item.G1122_C17131);
 
		                                    	$("#G1122_C17132").val(item.G1122_C17132);
 
		                                    	$("#G1122_C18858").val(item.G1122_C18858);
 
		                                    	$("#G1122_C18351").val(item.G1122_C18351);
 
		                                    	$("#G1122_C18859").val(item.G1122_C18859);
 
		                                    	$("#G1122_C18860").val(item.G1122_C18860);
 
		                                    	$("#G1122_C17133").val(item.G1122_C17133);
 
		                                    	$("#G1122_C17134").val(item.G1122_C17134);
 
		                                    	$("#G1122_C17135").val(item.G1122_C17135);
 
		                                    	$("#G1122_C17136").val(item.G1122_C17136);
 
		                                    	$("#G1122_C17137").val(item.G1122_C17137);
 
		                                    	$("#G1122_C17138").val(item.G1122_C17138);
 
		                                    	$("#G1122_C17139").val(item.G1122_C17139);
 
		                                    	$("#G1122_C17140").val(item.G1122_C17140);
 
		                                    	$("#G1122_C17141").val(item.G1122_C17141);
      
			                                    if(item.G1122_C17142 == 1){
			                                       $("#G1122_C17142").attr('checked', true);
			                                    } 
      
			                                    if(item.G1122_C17143 == 1){
			                                       $("#G1122_C17143").attr('checked', true);
			                                    } 
 
		                                    	$("#G1122_C17144").val(item.G1122_C17144);
 
		                                    	$("#G1122_C17145").val(item.G1122_C17145);
 
		                                    	$("#G1122_C17146").val(item.G1122_C17146);

		                                    	$("#G1122_C17146").val(item.G1122_C17146).trigger("change"); 
 
		                                    	$("#G1122_C18721").val(item.G1122_C18721);
 
		                                    	$("#G1122_C18720").val(item.G1122_C18720);
 
		                                    	$("#G1122_C17147").val(item.G1122_C17147);
 
		                                    	$("#G1122_C17149").val(item.G1122_C17149);
 
		                                    	$("#G1122_C17150").val(item.G1122_C17150);
 
		                                    	$("#G1122_C17151").val(item.G1122_C17151);
 
		                                    	$("#G1122_C17152").val(item.G1122_C17152);
 
		                                    	$("#G1122_C20691").val(item.G1122_C20691);
		              							$("#h3mio").html(item.principal);
			                                });

			                                //Deshabilitar los campos

			                                //Habilitar todos los campos para edicion
			                                $('#FormularioDatos :input').each(function(){
			                                    $(this).attr('disabled', true);
			                                });

			                                //Habilidar los botones de operacion, add, editar, eliminar
			                                $("#add").attr('disabled', false);
			                                $("#edit").attr('disabled', false);
			                                $("#delete").attr('disabled', false);

			                                //Desahabiliatra los botones de salvar y seleccionar_registro
			                                $("#cancel").attr('disabled', true);
			                                $("#Save").attr('disabled', true);
			                            } 
			                        })
			                        $("#hidId").val(<?php echo $_GET['registroId'];?>);
			                        <?php } else { ?>
			                            llenar_lista_navegacion('');
			                        <?php } ?>   

		                        <?php }else{ 
		                        	if(!isset($_GET['formulario'])){
		                        ?>

		                        	$.ajax({
		                        		url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
		                        		type  : "post",
		                        		data  : formData,
		                    		 	cache: false,
					                    contentType: false,
					                    processData: false,
		                        		success : function(xt){
		                        			console.log(xt);
		                        			window.location.href = "quitar.php";
		                        		}
		                        	});
		                        	
				
		                        <?php } 
		                        	}
		                        ?>            
		                    }else{
		                        //Algo paso, hay un error
		                        $("#Save").attr('disabled', false);
		                        alertify.error('Un error ha ocurrido y no pudimos guardar la información');
		                    }                
		                },
		                //si ha ocurrido un error
		                error: function(){
		                    after_save_error();
		                    $("#Save").attr('disabled', false);
		                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
		                }
		            });
          		}
          	}
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto Comercial ','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que departamento realiza la administración de los inmuebles?','Cupo autorizado','¿Cuántos inmuebles USADOS tiene en su inventario?','¿Cuántos inmuebles NUEVOS tiene en su inventario?','ESTADO','TIPIFICACION 1','TIPIFICACION 2','TIPIFICACION 3','COMENTARIOS BACKOFFICE '],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
	                ,
	                {  
	                    name:'G1122_C20661', 
	                    index:'G1122_C20661', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1122_C17121', 
	                    index: 'G1122_C17121', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17122', 
	                    index: 'G1122_C17122', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C18857', 
	                    index: 'G1122_C18857', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C20662', 
	                    index: 'G1122_C20662', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                {  
	                    name:'G1122_C20663', 
	                    index:'G1122_C20663', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17123', 
	                    index: 'G1122_C17123', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17124', 
	                    index: 'G1122_C17124', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17125', 
	                    index: 'G1122_C17125', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17126', 
	                    index: 'G1122_C17126', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17127', 
	                    index: 'G1122_C17127', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17128', 
	                    index: 'G1122_C17128', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17129', 
	                    index: 'G1122_C17129', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17130', 
	                    index: 'G1122_C17130', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17131', 
	                    index: 'G1122_C17131', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17132', 
	                    index: 'G1122_C17132', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1122_C18858', 
	                    index:'G1122_C18858', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1122_C18351', 
	                    index:'G1122_C18351', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=895&campo=G1122_C18351'
	                    }
	                }

	                ,
	                {  
	                    name:'G1122_C18859', 
	                    index:'G1122_C18859', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1122_C18860', 
	                    index:'G1122_C18860', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17138', 
	                    index: 'G1122_C17138', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17139', 
	                    index: 'G1122_C17139', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17140', 
	                    index: 'G1122_C17140', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17141', 
	                    index: 'G1122_C17141', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1122_C17144', 
	                    index:'G1122_C17144', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1122_C17144'
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17145', 
	                    index:'G1122_C17145', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1122_C17145'
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17146', 
	                    index:'G1122_C17146', 
	                    width:300 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1122_C17146=si',
	                        dataInit:function(el){
	                        	$(el).select2();
	                            /*$(el).select2({ 
	                                templateResult: function(data) {
	                                    var r = data.text.split('|');
	                                    var row = '<div class="row">';
	                                    var totalRows = 12 / r.length;
	                                    for(i= 0; i < r.length; i++){
	                                        row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
	                                    }
	                                    row += '</div>';
	                                    var $result = $(row);
	                                    return $result;
	                                },
	                                templateSelection : function(data){
	                                    var r = data.text.split('|');
	                                    return r[0];
	                                }
	                            });*/
	                            $(el).change(function(){
	                                var valores = $(el + " option:selected").attr("llenadores");
	                                var campos =  $(el + " option:selected").attr("dinammicos");
	                                var r = valores.split('|');
	                                if(r.length > 1){

	                                    var c = campos.split('|');
	                                    for(i = 1; i < r.length; i++){
	                                        $("#"+ rowid +"_"+c[i]).val(r[i]);
	                                    }
	                                }
	                            });
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C18721', 
	                    index: 'G1122_C18721', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1122_C18720', 
	                    index:'G1122_C18720', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }
 
	                ,
	                {  
	                    name:'G1122_C17147', 
	                    index:'G1122_C17147', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1122_C17149', 
	                    index:'G1122_C17149', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=828&campo=G1122_C17149'
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17150', 
	                    index:'G1122_C17150', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=829&campo=G1122_C17150'
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17151', 
	                    index:'G1122_C17151', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=830&campo=G1122_C17151'
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C17152', 
	                    index:'G1122_C17152', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=831&campo=G1122_C17152'
	                    }
	                }

	                ,
	                { 
	                    name:'G1122_C20691', 
	                    index:'G1122_C20691', 
	                    width:150, 
	                    editable: true 
	                }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    

                    $("#"+ rowid +"_G1122_C17146").change(function(){
                        var valores = $("#"+ rowid +"_G1122_C17146 option:selected").attr("llenadores");
                        var campos = $("#"+ rowid +"_G1122_C17146 option:selected").attr("dinammicos");
                        var r = valores.split('|');

                        if(r.length > 1){

                            var c = campos.split('|');
                            for(i = 1; i < r.length; i++){
                                if(!$("#"+c[i]).is("select")) {
                                // the input field is not a select
                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                }else{
                                    var change = r[i].replace(' ', ''); 
                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                }
                                
                            }
                        }
                    });
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1122_C17121',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
	            ,subGrid: true,
	            subGridRowExpanded: function(subgrid_id, row_id) { 
	                // we pass two parameters 
	                // subgrid_id is a id of the div tag created whitin a table data 
	                // the id of this elemenet is a combination of the "sg_" + id of the row 
	                // the row_id is the id of the row 
	                // If we wan to pass additinal parameters to the url we can use 
	                // a method getRowData(row_id) - which returns associative array in type name-value 
	                // here we can easy construct the flowing 
	                $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Campo1','Campo2', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                        ,
                        { 
                            name:'G1226_C20657', 
                            index: 'G1226_C20657', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1226_C20658', 
                            index: 'G1226_C20658', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

                var subgrid_table_id_1, pager_id_1; 

                subgrid_table_id_1 = subgrid_id+"_t_1"; 

                pager_id_ = "p_"+subgrid_table_id_1; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_1+"' class='scroll'></table><div id='"+pager_id_1+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_1).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_1=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto comercial','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Observación','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                        ,
                        { 
                            name:'G1137_C20679', 
                            index: 'G1137_C20679', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17541', 
                            index: 'G1137_C17541', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17542', 
                            index: 'G1137_C17542', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17543', 
                            index: 'G1137_C17543', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C20680', 
                            index: 'G1137_C20680', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C20681', 
                            index: 'G1137_C20681', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17544', 
                            index: 'G1137_C17544', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17545', 
                            index: 'G1137_C17545', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17546', 
                            index: 'G1137_C17546', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17547', 
                            index: 'G1137_C17547', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C19153', 
                            index: 'G1137_C19153', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17548', 
                            index: 'G1137_C17548', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17549', 
                            index: 'G1137_C17549', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17550', 
                            index: 'G1137_C17550', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17551', 
                            index: 'G1137_C17551', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17552', 
                            index: 'G1137_C17552', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1137_C19154', 
                            index:'G1137_C19154', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1137_C19155', 
                            index:'G1137_C19155', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1137_C19155'
                            }
                        }

                        ,
                        {  
                            name:'G1137_C19156', 
                            index:'G1137_C19156', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1137_C19157', 
                            index:'G1137_C19157', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        { 
                            name:'G1137_C17560', 
                            index: 'G1137_C17560', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17561', 
                            index: 'G1137_C17561', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17562', 
                            index: 'G1137_C17562', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1137_C17563', 
                            index: 'G1137_C17563', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1137_C19158', 
                            index:'G1137_C19158', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1137_C19158'
                            }
                        }

                        ,
                        {  
                            name:'G1137_C19159', 
                            index:'G1137_C19159', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1137_C19159'
                            }
                        }

                        ,
                        {  
                            name:'G1137_C19160', 
                            index:'G1137_C19160', 
                            width:300 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1137_C19160=si',
                                dataInit:function(el){
                                    $(el).select2({ 
                                        templateResult: function(data) {
                                            var r = data.text.split('|');
                                            var row = '<div class="row">';
                                            var totalRows = 12 / r.length;
                                            for(i= 0; i < r.length; i++){
                                                row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                            }
                                            row += '</div>';
                                            var $result = $(row);
                                            return $result;
                                        },
                                        templateSelection : function(data){
                                            var r = data.text.split('|');
                                            return r[0];
                                        }
                                    });
                                    $(el).change(function(){
                                        var valores = $(el + " option:selected").text();
                                        var campos =  $(el + " option:selected").attr("dinammicos");
                                        var r = valores.split('|');
                                        if(r.length > 1){

                                            var c = campos.split('|');
                                            for(i = 1; i < r.length; i++){
                                               if(!$("#"+ rowid +"_"+c[i]).is("select")) {
                                                // the input field is not a select
                                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                                }else{
                                                    var change = r[i].replace(' ', ''); 
                                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                                }
                                            }
                                        }
                                    });
                                    //campos sub grilla
                                }
                            }
                        }
 
                        ,
                        {  
                            name:'G1137_C19161', 
                            index:'G1137_C19161', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1137_C19162', 
                            index: 'G1137_C19162', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1137_C19163', 
                            index:'G1137_C19163', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1137_C17564', 
                            index:'G1137_C17564', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1137_C17553', 
                            index:'G1137_C17553', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=855&campo=G1137_C17553'
                            }
                        }

                        ,
                        {  
                            name:'G1137_C17554', 
                            index:'G1137_C17554', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=856&campo=G1137_C17554'
                            }
                        }
 
                        ,
                        {  
                            name:'G1137_C17555', 
                            index:'G1137_C17555', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }
 
                        ,
                        {  
                            name:'G1137_C17556', 
                            index:'G1137_C17556', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1137_C17557', 
                            index:'G1137_C17557', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1137_C17558', 
                            index:'G1137_C17558', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                     var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                    

                                }
                            }
                        }

                        ,
                        { 
                            name:'G1137_C17559', 
                            index:'G1137_C17559', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_1, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_1).jqGrid('navGrid',"#"+pager_id_1,{edit:false,add:false,del:false}) 

                var subgrid_table_id_2, pager_id_2; 

                subgrid_table_id_2 = subgrid_id+"_t_2"; 

                pager_id_ = "p_"+subgrid_table_id_2; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_2+"' class='scroll'></table><div id='"+pager_id_2+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_2).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_2=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','PASO_ID','REGISTRO_ID','Codigo Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID conecto comercial ','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Pack','Cupo','Tiempo','Precio full sin IVA','Descuento','Valor con dto sin IVA','Vigencia','Obsequio','Observación','Tipificación','ESTADO_TAREA','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }
 
                        ,
                        {  
                            name:'G1133_C20529', 
                            index:'G1133_C20529', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }
 
                        ,
                        {  
                            name:'G1133_C20530', 
                            index:'G1133_C20530', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1133_C20676', 
                            index: 'G1133_C20676', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17434', 
                            index: 'G1133_C17434', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17435', 
                            index: 'G1133_C17435', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17436', 
                            index: 'G1133_C17436', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C20677', 
                            index: 'G1133_C20677', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C20678', 
                            index: 'G1133_C20678', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17437', 
                            index: 'G1133_C17437', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17438', 
                            index: 'G1133_C17438', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17439', 
                            index: 'G1133_C17439', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17440', 
                            index: 'G1133_C17440', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C18933', 
                            index: 'G1133_C18933', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17441', 
                            index: 'G1133_C17441', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17442', 
                            index: 'G1133_C17442', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17443', 
                            index: 'G1133_C17443', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17444', 
                            index: 'G1133_C17444', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17445', 
                            index: 'G1133_C17445', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1133_C18934', 
                            index:'G1133_C18934', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1133_C18935', 
                            index:'G1133_C18935', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1133_C18935'
                            }
                        }

                        ,
                        {  
                            name:'G1133_C18936', 
                            index:'G1133_C18936', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1133_C18937', 
                            index:'G1133_C18937', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        { 
                            name:'G1133_C17451', 
                            index: 'G1133_C17451', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17452', 
                            index: 'G1133_C17452', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17453', 
                            index: 'G1133_C17453', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17454', 
                            index: 'G1133_C17454', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1133_C18938', 
                            index:'G1133_C18938', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1133_C18938'
                            }
                        }

                        ,
                        {  
                            name:'G1133_C18939', 
                            index:'G1133_C18939', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1133_C18939'
                            }
                        }

                        ,
                        {  
                            name:'G1133_C18940', 
                            index:'G1133_C18940', 
                            width:300 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1133_C18940=si',
                                dataInit:function(el){
                                    $(el).select2({ 
                                        templateResult: function(data) {
                                            var r = data.text.split('|');
                                            var row = '<div class="row">';
                                            var totalRows = 12 / r.length;
                                            for(i= 0; i < r.length; i++){
                                                row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                            }
                                            row += '</div>';
                                            var $result = $(row);
                                            return $result;
                                        },
                                        templateSelection : function(data){
                                            var r = data.text.split('|');
                                            return r[0];
                                        }
                                    });
                                    $(el).change(function(){
                                        var valores = $(el + " option:selected").text();
                                        var campos =  $(el + " option:selected").attr("dinammicos");
                                        var r = valores.split('|');
                                        if(r.length > 1){

                                            var c = campos.split('|');
                                            for(i = 1; i < r.length; i++){
                                               if(!$("#"+ rowid +"_"+c[i]).is("select")) {
                                                // the input field is not a select
                                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                                }else{
                                                    var change = r[i].replace(' ', ''); 
                                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                                }
                                            }
                                        }
                                    });
                                    //campos sub grilla
                                }
                            }
                        }
 
                        ,
                        {  
                            name:'G1133_C18941', 
                            index:'G1133_C18941', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1133_C18942', 
                            index: 'G1133_C18942', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1133_C18943', 
                            index:'G1133_C18943', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1133_C18554', 
                            index: 'G1133_C18554', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C17456', 
                            index:'G1133_C17456', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                                }
                            } 
                        }

                        ,
                        { 
                            name:'G1133_C18555', 
                            index: 'G1133_C18555', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C18556', 
                            index:'G1133_C18556', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                                }
                            } 
                        }

                        ,
                        { 
                            name:'G1133_C18557', 
                            index:'G1133_C18557', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                                }
                            } 
                        }

                        ,
                        { 
                            name:'G1133_C18558', 
                            index:'G1133_C18558', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                                }
                            } 
                        }

                        ,
                        {  
                            name:'G1133_C18559', 
                            index:'G1133_C18559', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        { 
                            name:'G1133_C18560', 
                            index: 'G1133_C18560', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1133_C18561', 
                            index:'G1133_C18561', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1133_C17446', 
                            index:'G1133_C17446', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=847&campo=G1133_C17446'
                            }
                        }

                        ,
                        {  
                            name:'G1133_C17447', 
                            index:'G1133_C17447', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=848&campo=G1133_C17447'
                            }
                        }

                        ,
                        {  
                            name:'G1133_C17448', 
                            index:'G1133_C17448', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1133_C17449', 
                            index:'G1133_C17449', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                     var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                    

                                }
                            }
                        }

                        ,
                        { 
                            name:'G1133_C17450', 
                            index:'G1133_C17450', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_2, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_2).jqGrid('navGrid',"#"+pager_id_2,{edit:false,add:false,del:false}) 

                var subgrid_table_id_3, pager_id_3; 

                subgrid_table_id_3 = subgrid_id+"_t_3"; 

                pager_id_ = "p_"+subgrid_table_id_3; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_3+"' class='scroll'></table><div id='"+pager_id_3+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_3).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_3=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto comercial ','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','CUPO CLIENTE','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Pack','Cupo','Tiempo','Precio full sin IVA','Descuento','Valor con dto sin IVA','Vigencia','Obsequio','Observación','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                        ,
                        { 
                            name:'G1134_C20670', 
                            index: 'G1134_C20670', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17457', 
                            index: 'G1134_C17457', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17458', 
                            index: 'G1134_C17458', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C20333', 
                            index: 'G1134_C20333', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C20671', 
                            index: 'G1134_C20671', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C20672', 
                            index: 'G1134_C20672', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17459', 
                            index: 'G1134_C17459', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17460', 
                            index: 'G1134_C17460', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17461', 
                            index: 'G1134_C17461', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17462', 
                            index: 'G1134_C17462', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17463', 
                            index: 'G1134_C17463', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1134_C18912', 
                            index:'G1134_C18912', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1134_C17464', 
                            index: 'G1134_C17464', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17465', 
                            index: 'G1134_C17465', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17466', 
                            index: 'G1134_C17466', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17467', 
                            index: 'G1134_C17467', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17468', 
                            index: 'G1134_C17468', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1134_C18913', 
                            index:'G1134_C18913', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1134_C18913'
                            }
                        }

                        ,
                        {  
                            name:'G1134_C18914', 
                            index:'G1134_C18914', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1134_C18915', 
                            index:'G1134_C18915', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        { 
                            name:'G1134_C17476', 
                            index: 'G1134_C17476', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17477', 
                            index: 'G1134_C17477', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17478', 
                            index: 'G1134_C17478', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17479', 
                            index: 'G1134_C17479', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1134_C18916', 
                            index:'G1134_C18916', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1134_C18916'
                            }
                        }

                        ,
                        {  
                            name:'G1134_C18917', 
                            index:'G1134_C18917', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1134_C18917'
                            }
                        }

                        ,
                        {  
                            name:'G1134_C18918', 
                            index:'G1134_C18918', 
                            width:300 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1134_C18918=si',
                                dataInit:function(el){
                                    $(el).select2({ 
                                        templateResult: function(data) {
                                            var r = data.text.split('|');
                                            var row = '<div class="row">';
                                            var totalRows = 12 / r.length;
                                            for(i= 0; i < r.length; i++){
                                                row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                            }
                                            row += '</div>';
                                            var $result = $(row);
                                            return $result;
                                        },
                                        templateSelection : function(data){
                                            var r = data.text.split('|');
                                            return r[0];
                                        }
                                    });
                                    $(el).change(function(){
                                        var valores = $(el + " option:selected").text();
                                        var campos =  $(el + " option:selected").attr("dinammicos");
                                        var r = valores.split('|');
                                        if(r.length > 1){

                                            var c = campos.split('|');
                                            for(i = 1; i < r.length; i++){
                                               if(!$("#"+ rowid +"_"+c[i]).is("select")) {
                                                // the input field is not a select
                                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                                }else{
                                                    var change = r[i].replace(' ', ''); 
                                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                                }
                                            }
                                        }
                                    });
                                    //campos sub grilla
                                }
                            }
                        }
 
                        ,
                        {  
                            name:'G1134_C18919', 
                            index:'G1134_C18919', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1134_C18920', 
                            index: 'G1134_C18920', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1134_C18921', 
                            index:'G1134_C18921', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1134_C18344', 
                            index: 'G1134_C18344', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17481', 
                            index:'G1134_C17481', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                                }
                            } 
                        }

                        ,
                        { 
                            name:'G1134_C18345', 
                            index: 'G1134_C18345', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C18346', 
                            index:'G1134_C18346', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                                }
                            } 
                        }

                        ,
                        { 
                            name:'G1134_C18347', 
                            index:'G1134_C18347', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                                }
                            } 
                        }

                        ,
                        { 
                            name:'G1134_C18348', 
                            index:'G1134_C18348', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                                }
                            } 
                        }

                        ,
                        {  
                            name:'G1134_C18349', 
                            index:'G1134_C18349', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        { 
                            name:'G1134_C18350', 
                            index: 'G1134_C18350', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1134_C17482', 
                            index:'G1134_C17482', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1134_C17469', 
                            index:'G1134_C17469', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=849&campo=G1134_C17469'
                            }
                        }

                        ,
                        {  
                            name:'G1134_C17470', 
                            index:'G1134_C17470', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=850&campo=G1134_C17470'
                            }
                        }
 
                        ,
                        {  
                            name:'G1134_C17471', 
                            index:'G1134_C17471', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }
 
                        ,
                        {  
                            name:'G1134_C17472', 
                            index:'G1134_C17472', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1134_C17473', 
                            index:'G1134_C17473', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1134_C17474', 
                            index:'G1134_C17474', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                     var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                    

                                }
                            }
                        }

                        ,
                        { 
                            name:'G1134_C17475', 
                            index:'G1134_C17475', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_3, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_3).jqGrid('navGrid',"#"+pager_id_3,{edit:false,add:false,del:false}) 

                var subgrid_table_id_4, pager_id_4; 

                subgrid_table_id_4 = subgrid_id+"_t_4"; 

                pager_id_ = "p_"+subgrid_table_id_4; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_4+"' class='scroll'></table><div id='"+pager_id_4+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_4).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_4=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto Comercial ','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACIÓN','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Tipo de cliente','Fecha lectura de contrato','Hora','Extensión','Pack adquirido','Cupo','Tiempo','Descuento aplicado','Fecha de activación','Fecha fin','Observación','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                        ,
                        { 
                            name:'G1135_C20667', 
                            index: 'G1135_C20667', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17483', 
                            index: 'G1135_C17483', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17484', 
                            index: 'G1135_C17484', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17485', 
                            index: 'G1135_C17485', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C20668', 
                            index: 'G1135_C20668', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C20669', 
                            index: 'G1135_C20669', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17486', 
                            index: 'G1135_C17486', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17487', 
                            index: 'G1135_C17487', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17488', 
                            index: 'G1135_C17488', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17489', 
                            index: 'G1135_C17489', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C18901', 
                            index: 'G1135_C18901', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17490', 
                            index: 'G1135_C17490', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17491', 
                            index: 'G1135_C17491', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17492', 
                            index: 'G1135_C17492', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17493', 
                            index: 'G1135_C17493', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17494', 
                            index: 'G1135_C17494', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C18902', 
                            index: 'G1135_C18902', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1135_C18903', 
                            index:'G1135_C18903', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1135_C18903'
                            }
                        }

                        ,
                        {  
                            name:'G1135_C18904', 
                            index:'G1135_C18904', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1135_C18905', 
                            index:'G1135_C18905', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        { 
                            name:'G1135_C17502', 
                            index: 'G1135_C17502', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17503', 
                            index: 'G1135_C17503', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17504', 
                            index: 'G1135_C17504', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C17505', 
                            index: 'G1135_C17505', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1135_C18906', 
                            index:'G1135_C18906', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1135_C18906'
                            }
                        }

                        ,
                        {  
                            name:'G1135_C18907', 
                            index:'G1135_C18907', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1135_C18907'
                            }
                        }

                        ,
                        {  
                            name:'G1135_C18908', 
                            index:'G1135_C18908', 
                            width:300 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1135_C18908=si',
                                dataInit:function(el){
                                    $(el).select2({ 
                                        templateResult: function(data) {
                                            var r = data.text.split('|');
                                            var row = '<div class="row">';
                                            var totalRows = 12 / r.length;
                                            for(i= 0; i < r.length; i++){
                                                row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                            }
                                            row += '</div>';
                                            var $result = $(row);
                                            return $result;
                                        },
                                        templateSelection : function(data){
                                            var r = data.text.split('|');
                                            return r[0];
                                        }
                                    });
                                    $(el).change(function(){
                                        var valores = $(el + " option:selected").text();
                                        var campos =  $(el + " option:selected").attr("dinammicos");
                                        var r = valores.split('|');
                                        if(r.length > 1){

                                            var c = campos.split('|');
                                            for(i = 1; i < r.length; i++){
                                               if(!$("#"+ rowid +"_"+c[i]).is("select")) {
                                                // the input field is not a select
                                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                                }else{
                                                    var change = r[i].replace(' ', ''); 
                                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                                }
                                            }
                                        }
                                    });
                                    //campos sub grilla
                                }
                            }
                        }
 
                        ,
                        {  
                            name:'G1135_C18909', 
                            index:'G1135_C18909', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1135_C18910', 
                            index: 'G1135_C18910', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1135_C18911', 
                            index:'G1135_C18911', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1135_C18645', 
                            index:'G1135_C18645', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=912&campo=G1135_C18645'
                            }
                        }

                        ,
                        {  
                            name:'G1135_C17507', 
                            index:'G1135_C17507', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1135_C17508', 
                            index:'G1135_C17508', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                     var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                    

                                }
                            }
                        }

                        ,
                        { 
                            name:'G1135_C17509', 
                            index: 'G1135_C17509', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C18646', 
                            index: 'G1135_C18646', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C18647', 
                            index:'G1135_C18647', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                                }
                            } 
                        }

                        ,
                        { 
                            name:'G1135_C18648', 
                            index: 'G1135_C18648', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1135_C18649', 
                            index: 'G1135_C18649', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1135_C18650', 
                            index:'G1135_C18650', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1135_C18651', 
                            index:'G1135_C18651', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        { 
                            name:'G1135_C17512', 
                            index:'G1135_C17512', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1135_C17495', 
                            index:'G1135_C17495', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=851&campo=G1135_C17495'
                            }
                        }

                        ,
                        {  
                            name:'G1135_C17496', 
                            index:'G1135_C17496', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=852&campo=G1135_C17496'
                            }
                        }
 
                        ,
                        {  
                            name:'G1135_C17497', 
                            index:'G1135_C17497', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }
 
                        ,
                        {  
                            name:'G1135_C17498', 
                            index:'G1135_C17498', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1135_C17499', 
                            index:'G1135_C17499', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1135_C17500', 
                            index:'G1135_C17500', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                     var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                    

                                }
                            }
                        }

                        ,
                        { 
                            name:'G1135_C17501', 
                            index:'G1135_C17501', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_4, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_4).jqGrid('navGrid',"#"+pager_id_4,{edit:false,add:false,del:false}) 

                var subgrid_table_id_5, pager_id_5; 

                subgrid_table_id_5 = subgrid_id+"_t_5"; 

                pager_id_ = "p_"+subgrid_table_id_5; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_5+"' class='scroll'></table><div id='"+pager_id_5+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_5).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_5=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto comercial','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Cupo usados','Usados','Cupo nuevos','Nuevos','Observación','Categoría','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                        ,
                        { 
                            name:'G1136_C20664', 
                            index: 'G1136_C20664', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17513', 
                            index: 'G1136_C17513', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17514', 
                            index: 'G1136_C17514', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C20334', 
                            index: 'G1136_C20334', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C20665', 
                            index: 'G1136_C20665', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C20666', 
                            index: 'G1136_C20666', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17515', 
                            index: 'G1136_C17515', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17516', 
                            index: 'G1136_C17516', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17517', 
                            index: 'G1136_C17517', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17518', 
                            index: 'G1136_C17518', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17519', 
                            index: 'G1136_C17519', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17520', 
                            index: 'G1136_C17520', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17521', 
                            index: 'G1136_C17521', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17522', 
                            index: 'G1136_C17522', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17523', 
                            index: 'G1136_C17523', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17524', 
                            index: 'G1136_C17524', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1136_C18891', 
                            index:'G1136_C18891', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1136_C18892', 
                            index:'G1136_C18892', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1136_C18892'
                            }
                        }

                        ,
                        {  
                            name:'G1136_C18893', 
                            index:'G1136_C18893', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1136_C18894', 
                            index:'G1136_C18894', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        { 
                            name:'G1136_C17532', 
                            index: 'G1136_C17532', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17533', 
                            index: 'G1136_C17533', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17534', 
                            index: 'G1136_C17534', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1136_C17535', 
                            index: 'G1136_C17535', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1136_C18895', 
                            index:'G1136_C18895', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1136_C18895'
                            }
                        }

                        ,
                        {  
                            name:'G1136_C18896', 
                            index:'G1136_C18896', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1136_C18896'
                            }
                        }

                        ,
                        {  
                            name:'G1136_C18897', 
                            index:'G1136_C18897', 
                            width:300 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1136_C18897=si',
                                dataInit:function(el){
                                    $(el).select2({ 
                                        templateResult: function(data) {
                                            var r = data.text.split('|');
                                            var row = '<div class="row">';
                                            var totalRows = 12 / r.length;
                                            for(i= 0; i < r.length; i++){
                                                row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                            }
                                            row += '</div>';
                                            var $result = $(row);
                                            return $result;
                                        },
                                        templateSelection : function(data){
                                            var r = data.text.split('|');
                                            return r[0];
                                        }
                                    });
                                    $(el).change(function(){
                                        var valores = $(el + " option:selected").text();
                                        var campos =  $(el + " option:selected").attr("dinammicos");
                                        var r = valores.split('|');
                                        if(r.length > 1){

                                            var c = campos.split('|');
                                            for(i = 1; i < r.length; i++){
                                               if(!$("#"+ rowid +"_"+c[i]).is("select")) {
                                                // the input field is not a select
                                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                                }else{
                                                    var change = r[i].replace(' ', ''); 
                                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                                }
                                            }
                                        }
                                    });
                                    //campos sub grilla
                                }
                            }
                        }
 
                        ,
                        {  
                            name:'G1136_C18898', 
                            index:'G1136_C18898', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1136_C18899', 
                            index: 'G1136_C18899', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1136_C18900', 
                            index:'G1136_C18900', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1136_C17537', 
                            index:'G1136_C17537', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                                }
                            } 
                        }

                        ,
                        { 
                            name:'G1136_C17536', 
                            index:'G1136_C17536', 
                            width:70 ,
                            editable: true, 
                            edittype:"checkbox",
                            editoptions: {
                                value:"1:0"
                            } 
                        }

                        ,
                        { 
                            name:'G1136_C17539', 
                            index:'G1136_C17539', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric({ decimal : ".",  negative : false, scale: 4 });
                                }
                            } 
                        }

                        ,
                        { 
                            name:'G1136_C17538', 
                            index:'G1136_C17538', 
                            width:70 ,
                            editable: true, 
                            edittype:"checkbox",
                            editoptions: {
                                value:"1:0"
                            } 
                        }

                        ,
                        { 
                            name:'G1136_C17540', 
                            index:'G1136_C17540', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1136_C18644', 
                            index:'G1136_C18644', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=911&campo=G1136_C18644'
                            }
                        }

                        ,
                        {  
                            name:'G1136_C17525', 
                            index:'G1136_C17525', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=853&campo=G1136_C17525'
                            }
                        }

                        ,
                        {  
                            name:'G1136_C17526', 
                            index:'G1136_C17526', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=854&campo=G1136_C17526'
                            }
                        }
 
                        ,
                        {  
                            name:'G1136_C17527', 
                            index:'G1136_C17527', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }
 
                        ,
                        {  
                            name:'G1136_C17528', 
                            index:'G1136_C17528', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1136_C17529', 
                            index:'G1136_C17529', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1136_C17530', 
                            index:'G1136_C17530', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                     var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                    

                                }
                            }
                        }

                        ,
                        { 
                            name:'G1136_C17531', 
                            index:'G1136_C17531', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_5, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_5).jqGrid('navGrid',"#"+pager_id_5,{edit:false,add:false,del:false}) 

                var subgrid_table_id_6, pager_id_6; 

                subgrid_table_id_6 = subgrid_id+"_t_6"; 

                pager_id_ = "p_"+subgrid_table_id_6; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_6+"' class='scroll'></table><div id='"+pager_id_6+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_6).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_6=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto comercial ','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','DEPARTAMENTO','CIUDAD','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Observacion','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                        ,
                        { 
                            name:'G1138_C20673', 
                            index: 'G1138_C20673', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17565', 
                            index: 'G1138_C17565', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17566', 
                            index: 'G1138_C17566', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17567', 
                            index: 'G1138_C17567', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C20674', 
                            index: 'G1138_C20674', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C20675', 
                            index: 'G1138_C20675', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17568', 
                            index: 'G1138_C17568', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17569', 
                            index: 'G1138_C17569', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17570', 
                            index: 'G1138_C17570', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17571', 
                            index: 'G1138_C17571', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17572', 
                            index: 'G1138_C17572', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C18922', 
                            index: 'G1138_C18922', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17573', 
                            index: 'G1138_C17573', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17574', 
                            index: 'G1138_C17574', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17575', 
                            index: 'G1138_C17575', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17576', 
                            index: 'G1138_C17576', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1138_C18923', 
                            index:'G1138_C18923', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1138_C18924', 
                            index:'G1138_C18924', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1138_C18924'
                            }
                        }

                        ,
                        {  
                            name:'G1138_C18925', 
                            index:'G1138_C18925', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1138_C18926', 
                            index:'G1138_C18926', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        { 
                            name:'G1138_C17584', 
                            index: 'G1138_C17584', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17585', 
                            index: 'G1138_C17585', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17586', 
                            index: 'G1138_C17586', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1138_C17587', 
                            index: 'G1138_C17587', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1138_C18927', 
                            index:'G1138_C18927', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1138_C18927'
                            }
                        }

                        ,
                        {  
                            name:'G1138_C18928', 
                            index:'G1138_C18928', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1138_C18928'
                            }
                        }

                        ,
                        {  
                            name:'G1138_C18929', 
                            index:'G1138_C18929', 
                            width:300 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1138_C18929=si',
                                dataInit:function(el){
                                    $(el).select2({ 
                                        templateResult: function(data) {
                                            var r = data.text.split('|');
                                            var row = '<div class="row">';
                                            var totalRows = 12 / r.length;
                                            for(i= 0; i < r.length; i++){
                                                row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                            }
                                            row += '</div>';
                                            var $result = $(row);
                                            return $result;
                                        },
                                        templateSelection : function(data){
                                            var r = data.text.split('|');
                                            return r[0];
                                        }
                                    });
                                    $(el).change(function(){
                                        var valores = $(el + " option:selected").text();
                                        var campos =  $(el + " option:selected").attr("dinammicos");
                                        var r = valores.split('|');
                                        if(r.length > 1){

                                            var c = campos.split('|');
                                            for(i = 1; i < r.length; i++){
                                               if(!$("#"+ rowid +"_"+c[i]).is("select")) {
                                                // the input field is not a select
                                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                                }else{
                                                    var change = r[i].replace(' ', ''); 
                                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                                }
                                            }
                                        }
                                    });
                                    //campos sub grilla
                                }
                            }
                        }
 
                        ,
                        {  
                            name:'G1138_C18930', 
                            index:'G1138_C18930', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1138_C18931', 
                            index: 'G1138_C18931', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1138_C18932', 
                            index:'G1138_C18932', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1138_C17588', 
                            index:'G1138_C17588', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1138_C17577', 
                            index:'G1138_C17577', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=857&campo=G1138_C17577'
                            }
                        }

                        ,
                        {  
                            name:'G1138_C17578', 
                            index:'G1138_C17578', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=858&campo=G1138_C17578'
                            }
                        }
 
                        ,
                        {  
                            name:'G1138_C17579', 
                            index:'G1138_C17579', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }
 
                        ,
                        {  
                            name:'G1138_C17580', 
                            index:'G1138_C17580', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1138_C17581', 
                            index:'G1138_C17581', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1138_C17582', 
                            index:'G1138_C17582', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                     var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                    

                                }
                            }
                        }

                        ,
                        { 
                            name:'G1138_C17583', 
                            index:'G1138_C17583', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_6, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_6).jqGrid('navGrid',"#"+pager_id_6,{edit:false,add:false,del:false}) 

                var subgrid_table_id_7, pager_id_7; 

                subgrid_table_id_7 = subgrid_id+"_t_7"; 

                pager_id_ = "p_"+subgrid_table_id_7; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_7+"' class='scroll'></table><div id='"+pager_id_7+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_7).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_7=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto comercial','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Causa','Observación','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                        ,
                        { 
                            name:'G1139_C20682', 
                            index: 'G1139_C20682', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17589', 
                            index: 'G1139_C17589', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17590', 
                            index: 'G1139_C17590', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C19164', 
                            index: 'G1139_C19164', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C20683', 
                            index: 'G1139_C20683', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C20684', 
                            index: 'G1139_C20684', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17591', 
                            index: 'G1139_C17591', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17592', 
                            index: 'G1139_C17592', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17593', 
                            index: 'G1139_C17593', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17594', 
                            index: 'G1139_C17594', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17595', 
                            index: 'G1139_C17595', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17596', 
                            index: 'G1139_C17596', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17597', 
                            index: 'G1139_C17597', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17598', 
                            index: 'G1139_C17598', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17599', 
                            index: 'G1139_C17599', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17600', 
                            index: 'G1139_C17600', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1139_C19165', 
                            index:'G1139_C19165', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1139_C19166', 
                            index:'G1139_C19166', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1139_C19166'
                            }
                        }

                        ,
                        {  
                            name:'G1139_C19167', 
                            index:'G1139_C19167', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1139_C19168', 
                            index:'G1139_C19168', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        { 
                            name:'G1139_C17608', 
                            index: 'G1139_C17608', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17609', 
                            index: 'G1139_C17609', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17610', 
                            index: 'G1139_C17610', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1139_C17611', 
                            index: 'G1139_C17611', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1139_C19169', 
                            index:'G1139_C19169', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1139_C19169'
                            }
                        }

                        ,
                        {  
                            name:'G1139_C19170', 
                            index:'G1139_C19170', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1139_C19170'
                            }
                        }

                        ,
                        {  
                            name:'G1139_C19171', 
                            index:'G1139_C19171', 
                            width:300 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1139_C19171=si',
                                dataInit:function(el){
                                    $(el).select2({ 
                                        templateResult: function(data) {
                                            var r = data.text.split('|');
                                            var row = '<div class="row">';
                                            var totalRows = 12 / r.length;
                                            for(i= 0; i < r.length; i++){
                                                row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                            }
                                            row += '</div>';
                                            var $result = $(row);
                                            return $result;
                                        },
                                        templateSelection : function(data){
                                            var r = data.text.split('|');
                                            return r[0];
                                        }
                                    });
                                    $(el).change(function(){
                                        var valores = $(el + " option:selected").text();
                                        var campos =  $(el + " option:selected").attr("dinammicos");
                                        var r = valores.split('|');
                                        if(r.length > 1){

                                            var c = campos.split('|');
                                            for(i = 1; i < r.length; i++){
                                               if(!$("#"+ rowid +"_"+c[i]).is("select")) {
                                                // the input field is not a select
                                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                                }else{
                                                    var change = r[i].replace(' ', ''); 
                                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                                }
                                            }
                                        }
                                    });
                                    //campos sub grilla
                                }
                            }
                        }
 
                        ,
                        {  
                            name:'G1139_C19172', 
                            index:'G1139_C19172', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1139_C19173', 
                            index: 'G1139_C19173', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1139_C19174', 
                            index:'G1139_C19174', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1139_C17612', 
                            index:'G1139_C17612', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=861&campo=G1139_C17612'
                            }
                        }

                        ,
                        { 
                            name:'G1139_C17613', 
                            index:'G1139_C17613', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1139_C17601', 
                            index:'G1139_C17601', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=859&campo=G1139_C17601'
                            }
                        }

                        ,
                        {  
                            name:'G1139_C17602', 
                            index:'G1139_C17602', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=860&campo=G1139_C17602'
                            }
                        }
 
                        ,
                        {  
                            name:'G1139_C17603', 
                            index:'G1139_C17603', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }
 
                        ,
                        {  
                            name:'G1139_C17604', 
                            index:'G1139_C17604', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1139_C17605', 
                            index:'G1139_C17605', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1139_C17606', 
                            index:'G1139_C17606', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                     var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                    

                                }
                            }
                        }

                        ,
                        { 
                            name:'G1139_C17607', 
                            index:'G1139_C17607', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_7, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_7).jqGrid('navGrid',"#"+pager_id_7,{edit:false,add:false,del:false}) 

                var subgrid_table_id_8, pager_id_8; 

                subgrid_table_id_8 = subgrid_id+"_t_8"; 

                pager_id_ = "p_"+subgrid_table_id_8; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_8+"' class='scroll'></table><div id='"+pager_id_8+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_8).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_8=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','CÓDIGO CLIENTE ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID CONTACTO COMERCIAL ','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Tipo de solicitud','Observación','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                        ,
                        { 
                            name:'G1140_C20685', 
                            index: 'G1140_C20685', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17614', 
                            index: 'G1140_C17614', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17615', 
                            index: 'G1140_C17615', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17616', 
                            index: 'G1140_C17616', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C20686', 
                            index: 'G1140_C20686', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C20687', 
                            index: 'G1140_C20687', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17617', 
                            index: 'G1140_C17617', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17618', 
                            index: 'G1140_C17618', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17619', 
                            index: 'G1140_C17619', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17620', 
                            index: 'G1140_C17620', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C20202', 
                            index: 'G1140_C20202', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17621', 
                            index: 'G1140_C17621', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17622', 
                            index: 'G1140_C17622', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17623', 
                            index: 'G1140_C17623', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17624', 
                            index: 'G1140_C17624', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17625', 
                            index: 'G1140_C17625', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1140_C20203', 
                            index:'G1140_C20203', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1140_C20204', 
                            index:'G1140_C20204', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1140_C20204'
                            }
                        }

                        ,
                        {  
                            name:'G1140_C20205', 
                            index:'G1140_C20205', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1140_C20206', 
                            index:'G1140_C20206', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        { 
                            name:'G1140_C17633', 
                            index: 'G1140_C17633', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17634', 
                            index: 'G1140_C17634', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17635', 
                            index: 'G1140_C17635', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1140_C17636', 
                            index: 'G1140_C17636', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1140_C20231', 
                            index:'G1140_C20231', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1140_C20231'
                            }
                        }

                        ,
                        {  
                            name:'G1140_C20232', 
                            index:'G1140_C20232', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1140_C20232'
                            }
                        }

                        ,
                        {  
                            name:'G1140_C20233', 
                            index:'G1140_C20233', 
                            width:300 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1140_C20233=si',
                                dataInit:function(el){
                                    $(el).select2({ 
                                        templateResult: function(data) {
                                            var r = data.text.split('|');
                                            var row = '<div class="row">';
                                            var totalRows = 12 / r.length;
                                            for(i= 0; i < r.length; i++){
                                                row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
                                            }
                                            row += '</div>';
                                            var $result = $(row);
                                            return $result;
                                        },
                                        templateSelection : function(data){
                                            var r = data.text.split('|');
                                            return r[0];
                                        }
                                    });
                                    $(el).change(function(){
                                        var valores = $(el + " option:selected").text();
                                        var campos =  $(el + " option:selected").attr("dinammicos");
                                        var r = valores.split('|');
                                        if(r.length > 1){

                                            var c = campos.split('|');
                                            for(i = 1; i < r.length; i++){
                                               if(!$("#"+ rowid +"_"+c[i]).is("select")) {
                                                // the input field is not a select
                                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                                }else{
                                                    var change = r[i].replace(' ', ''); 
                                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                                }
                                            }
                                        }
                                    });
                                    //campos sub grilla
                                }
                            }
                        }
 
                        ,
                        {  
                            name:'G1140_C20234', 
                            index:'G1140_C20234', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        { 
                            name:'G1140_C20235', 
                            index: 'G1140_C20235', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }
 
                        ,
                        {  
                            name:'G1140_C20236', 
                            index:'G1140_C20236', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1140_C17637', 
                            index:'G1140_C17637', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=864&campo=G1140_C17637'
                            }
                        }

                        ,
                        { 
                            name:'G1140_C17638', 
                            index:'G1140_C17638', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1140_C17626', 
                            index:'G1140_C17626', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=862&campo=G1140_C17626'
                            }
                        }

                        ,
                        {  
                            name:'G1140_C17627', 
                            index:'G1140_C17627', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=863&campo=G1140_C17627'
                            }
                        }
 
                        ,
                        {  
                            name:'G1140_C17628', 
                            index:'G1140_C17628', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }
 
                        ,
                        {  
                            name:'G1140_C17629', 
                            index:'G1140_C17629', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1140_C17630', 
                            index:'G1140_C17630', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1140_C17631', 
                            index:'G1140_C17631', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                     var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                    

                                }
                            }
                        }

                        ,
                        { 
                            name:'G1140_C17632', 
                            index:'G1140_C17632', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_8, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_8).jqGrid('navGrid',"#"+pager_id_8,{edit:false,add:false,del:false}) 

            }, 
            subGridRowColapsed: function(subgrid_id, row_id) { 
                // this function is called before removing the data 
                //var subgrid_table_id; 
                //subgrid_table_id = subgrid_id+"_t"; 
                //jQuery("#"+subgrid_table_id).remove(); 
            }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(x){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', Busqueda : x},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');
            $("#btnLlamar_1").attr('padre', id);$("#btnLlamar_2").attr('padre', id);$("#btnLlamar_3").attr('padre', id);$("#btnLlamar_4").attr('padre', id);$("#btnLlamar_5").attr('padre', id);$("#btnLlamar_6").attr('padre', id);$("#btnLlamar_7").attr('padre', id);$("#btnLlamar_8").attr('padre', id);
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            cargarHijos_0(id);

            $.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
            cargarHijos_1(id);

            $.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
            cargarHijos_2(id);

            $.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion Recargar 
            cargarHijos_3(id);

            $.jgrid.gridUnload('#tablaDatosDetalless4'); //funcion Recargar 
            cargarHijos_4(id);

            $.jgrid.gridUnload('#tablaDatosDetalless5'); //funcion Recargar 
            cargarHijos_5(id);

            $.jgrid.gridUnload('#tablaDatosDetalless6'); //funcion Recargar 
            cargarHijos_6(id);

            $.jgrid.gridUnload('#tablaDatosDetalless7'); //funcion Recargar 
            cargarHijos_7(id);

            $.jgrid.gridUnload('#tablaDatosDetalless8'); //funcion Recargar 
            cargarHijos_8(id);

                        $("#G1122_C20661").val(item.G1122_C20661);

                        $("#G1122_C17121").val(item.G1122_C17121);

                        $("#G1122_C17122").val(item.G1122_C17122);

                        $("#G1122_C18857").val(item.G1122_C18857);

                        $("#G1122_C20662").val(item.G1122_C20662);

                        $("#G1122_C20663").val(item.G1122_C20663);

                        $("#G1122_C17123").val(item.G1122_C17123);

                        $("#G1122_C17124").val(item.G1122_C17124);

                        $("#G1122_C17125").val(item.G1122_C17125);

                        $("#G1122_C17126").val(item.G1122_C17126);

                        $("#G1122_C17127").val(item.G1122_C17127);

                        $("#G1122_C17128").val(item.G1122_C17128);

                        $("#G1122_C17129").val(item.G1122_C17129);

                        $("#G1122_C17130").val(item.G1122_C17130);

                        $("#G1122_C17131").val(item.G1122_C17131);

                        $("#G1122_C17132").val(item.G1122_C17132);

                        $("#G1122_C18858").val(item.G1122_C18858);

                        $("#G1122_C18351").val(item.G1122_C18351);
 
        	            $("#G1122_C18351").val(item.G1122_C18351).trigger("change"); 

                        $("#G1122_C18859").val(item.G1122_C18859);

                        $("#G1122_C18860").val(item.G1122_C18860);

                        $("#G1122_C17133").val(item.G1122_C17133);
 
        	            $("#G1122_C17133").val(item.G1122_C17133).trigger("change"); 

                        $("#G1122_C17134").val(item.G1122_C17134);
 
        	            $("#G1122_C17134").val(item.G1122_C17134).trigger("change"); 

                        $("#G1122_C17135").val(item.G1122_C17135);

                        $("#G1122_C17136").val(item.G1122_C17136);

                        $("#G1122_C17137").val(item.G1122_C17137);

                        $("#G1122_C17138").val(item.G1122_C17138);

                        $("#G1122_C17139").val(item.G1122_C17139);

                        $("#G1122_C17140").val(item.G1122_C17140);

                        $("#G1122_C17141").val(item.G1122_C17141);
    
                        if(item.G1122_C17142 == 1){
                           $("#G1122_C17142").attr('checked', true);
                        } 
    
                        if(item.G1122_C17143 == 1){
                           $("#G1122_C17143").attr('checked', true);
                        } 

                        $("#G1122_C17144").val(item.G1122_C17144);
 
        	            $("#G1122_C17144").val(item.G1122_C17144).trigger("change"); 

                        $("#G1122_C17145").val(item.G1122_C17145);
 
        	            $("#G1122_C17145").val(item.G1122_C17145).trigger("change"); 

                        $("#G1122_C17146").val(item.G1122_C17146);
 
        	            $("#G1122_C17146").val(item.G1122_C17146).trigger("change"); 

                        $("#G1122_C18721").val(item.G1122_C18721);

                        $("#G1122_C18720").val(item.G1122_C18720);

                        $("#G1122_C17147").val(item.G1122_C17147);

                        $("#G1122_C17149").val(item.G1122_C17149);
 
        	            $("#G1122_C17149").val(item.G1122_C17149).trigger("change"); 

                        $("#G1122_C17150").val(item.G1122_C17150);
 
        	            $("#G1122_C17150").val(item.G1122_C17150).trigger("change"); 

                        $("#G1122_C17151").val(item.G1122_C17151);
 
        	            $("#G1122_C17151").val(item.G1122_C17151).trigger("change"); 

                        $("#G1122_C17152").val(item.G1122_C17152);
 
        	            $("#G1122_C17152").val(item.G1122_C17152).trigger("change"); 

                        $("#G1122_C20691").val(item.G1122_C20691);
        				$("#h3mio").html(item.principal);
        				
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 

            $.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion descargar 

            $.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion descargar 

            $.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion descargar 

            $.jgrid.gridUnload('#tablaDatosDetalless4'); //funcion descargar 

            $.jgrid.gridUnload('#tablaDatosDetalless5'); //funcion descargar 

            $.jgrid.gridUnload('#tablaDatosDetalless6'); //funcion descargar 

            $.jgrid.gridUnload('#tablaDatosDetalless7'); //funcion descargar 

            $.jgrid.gridUnload('#tablaDatosDetalless8'); //funcion descargar 
    } 

    <?php } ?>


    

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','Campo1','Campo2', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                { 
                    name:'G1226_C20657', 
                    index: 'G1226_C20657', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1226_C20658', 
                    index: 'G1226_C20658', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles0",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1226_C20657',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'HISTÓRICO ',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            
        });
        $('#tablaDatosDetalless0').navGrid("#pagerDetalles0", { add:false, del: true , edit: false });


        $('#tablaDatosDetalless0').inlineNav('#pagerDetalles0',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            del: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_1(id_1){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless1").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_1=si&id='+id_1,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto comercial','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Observación','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                { 
                    name:'G1137_C20679', 
                    index: 'G1137_C20679', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17541', 
                    index: 'G1137_C17541', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17542', 
                    index: 'G1137_C17542', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17543', 
                    index: 'G1137_C17543', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C20680', 
                    index: 'G1137_C20680', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C20681', 
                    index: 'G1137_C20681', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17544', 
                    index: 'G1137_C17544', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17545', 
                    index: 'G1137_C17545', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17546', 
                    index: 'G1137_C17546', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17547', 
                    index: 'G1137_C17547', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C19153', 
                    index: 'G1137_C19153', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17548', 
                    index: 'G1137_C17548', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17549', 
                    index: 'G1137_C17549', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17550', 
                    index: 'G1137_C17550', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17551', 
                    index: 'G1137_C17551', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17552', 
                    index: 'G1137_C17552', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1137_C19154', 
                    index:'G1137_C19154', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1137_C19155', 
                    index:'G1137_C19155', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1137_C19155'
                    }
                }

                ,
                {  
                    name:'G1137_C19156', 
                    index:'G1137_C19156', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1137_C19157', 
                    index:'G1137_C19157', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                { 
                    name:'G1137_C17560', 
                    index: 'G1137_C17560', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17561', 
                    index: 'G1137_C17561', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17562', 
                    index: 'G1137_C17562', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1137_C17563', 
                    index: 'G1137_C17563', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1137_C19158', 
                    index:'G1137_C19158', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1137_C19158'
                    }
                }

                ,
                {  
                    name:'G1137_C19159', 
                    index:'G1137_C19159', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1137_C19159'
                    }
                }

                ,
                {  
                    name:'G1137_C19160', 
                    index:'G1137_C19160', 
                    width:300 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?MostrarCombo_Guion_1137_C19160=si',
                        dataInit:function(el){
                            $(el).select2({ 
					        	placeholder: "Buscar",
						        allowClear: false,
						        minimumInputLength: 3,
						        ajax: {
						            url: '<?=$url_crud;?>?CallDatosCombo_Guion_1137_C19160=si',
						            dataType: 'json',
						            type : 'post',
						            delay: 250,
						            data: function (params) {
						                return {
						                    q: params.term
						                };
						            },
					              	processResults: function(data) {
									  	return {
									    	results: $.map(data, function(obj) {
									      		return {
									        		id: obj.id,
									        		text: obj.text
									      		};
									    	})
									  	};
									},
						            cache: true
						        }
					    	});  
                        },
                        dataEvents: [
                            {  
                                type: 'change',
                                fn: function(e) {
                                    var r = this.id;
                                    var rId = r.split('_');

                                    $.ajax({
									    url   : '<?php echo $url_crud;?>',
									    data  : { dameValoresCamposDinamicos_Guion_1137_C19160 : this.value },
									    type  : 'post',
									    dataType : 'json',
									    success  : function(data){
									    	$.each(data, function(i, item){
									        
									    	});
									    }
									});
                                }
                            }
                        ]
                    }
                }
 
                ,
                {  
                    name:'G1137_C19161', 
                    index:'G1137_C19161', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1137_C19162', 
                    index: 'G1137_C19162', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1137_C19163', 
                    index:'G1137_C19163', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {
                    name:'G1137_C17564', 
                    index:'G1137_C17564', 
                    width:150, 
                    editable: true 
                }

                ,
                {  
                    name:'G1137_C17553', 
                    index:'G1137_C17553', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=855&campo=G1137_C17553'
                    }
                }

                ,
                {  
                    name:'G1137_C17554', 
                    index:'G1137_C17554', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=856&campo=G1137_C17554'
                    }
                }
 
                ,
                {  
                    name:'G1137_C17555', 
                    index:'G1137_C17555', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }
 
                ,
                {  
                    name:'G1137_C17556', 
                    index:'G1137_C17556', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1137_C17557', 
                    index:'G1137_C17557', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1137_C17558', 
                    index:'G1137_C17558', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }

                ,
                {
                    name:'G1137_C17559', 
                    index:'G1137_C17559', 
                    width:150, 
                    editable: true 
                }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_1); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles1",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1137_C20679',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Enviar tutoriales',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_1=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1137&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=0&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless1").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_2(id_2){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless2").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_2=si&id='+id_2,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','PASO_ID','REGISTRO_ID','Codigo Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID conecto comercial ','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Pack','Cupo','Tiempo','Precio full sin IVA','Descuento','Valor con dto sin IVA','Vigencia','Obsequio','Observación','Tipificación','ESTADO_TAREA','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
                ,
                {  
                    name:'G1133_C20529', 
                    index:'G1133_C20529', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }
 
                ,
                {  
                    name:'G1133_C20530', 
                    index:'G1133_C20530', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1133_C20676', 
                    index: 'G1133_C20676', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17434', 
                    index: 'G1133_C17434', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17435', 
                    index: 'G1133_C17435', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17436', 
                    index: 'G1133_C17436', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C20677', 
                    index: 'G1133_C20677', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C20678', 
                    index: 'G1133_C20678', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17437', 
                    index: 'G1133_C17437', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17438', 
                    index: 'G1133_C17438', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17439', 
                    index: 'G1133_C17439', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17440', 
                    index: 'G1133_C17440', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C18933', 
                    index: 'G1133_C18933', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17441', 
                    index: 'G1133_C17441', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17442', 
                    index: 'G1133_C17442', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17443', 
                    index: 'G1133_C17443', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17444', 
                    index: 'G1133_C17444', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17445', 
                    index: 'G1133_C17445', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1133_C18934', 
                    index:'G1133_C18934', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1133_C18935', 
                    index:'G1133_C18935', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1133_C18935'
                    }
                }

                ,
                {  
                    name:'G1133_C18936', 
                    index:'G1133_C18936', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1133_C18937', 
                    index:'G1133_C18937', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                { 
                    name:'G1133_C17451', 
                    index: 'G1133_C17451', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17452', 
                    index: 'G1133_C17452', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17453', 
                    index: 'G1133_C17453', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1133_C17454', 
                    index: 'G1133_C17454', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1133_C18938', 
                    index:'G1133_C18938', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1133_C18938'
                    }
                }

                ,
                {  
                    name:'G1133_C18939', 
                    index:'G1133_C18939', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1133_C18939'
                    }
                }

                ,
                {  
                    name:'G1133_C18940', 
                    index:'G1133_C18940', 
                    width:300 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?MostrarCombo_Guion_1133_C18940=si',
                        dataInit:function(el){
                            $(el).select2({ 
					        	placeholder: "Buscar",
						        allowClear: false,
						        minimumInputLength: 3,
						        ajax: {
						            url: '<?=$url_crud;?>?CallDatosCombo_Guion_1133_C18940=si',
						            dataType: 'json',
						            type : 'post',
						            delay: 250,
						            data: function (params) {
						                return {
						                    q: params.term
						                };
						            },
					              	processResults: function(data) {
									  	return {
									    	results: $.map(data, function(obj) {
									      		return {
									        		id: obj.id,
									        		text: obj.text
									      		};
									    	})
									  	};
									},
						            cache: true
						        }
					    	});  
                        },
                        dataEvents: [
                            {  
                                type: 'change',
                                fn: function(e) {
                                    var r = this.id;
                                    var rId = r.split('_');

                                    $.ajax({
									    url   : '<?php echo $url_crud;?>',
									    data  : { dameValoresCamposDinamicos_Guion_1133_C18940 : this.value },
									    type  : 'post',
									    dataType : 'json',
									    success  : function(data){
									    	$.each(data, function(i, item){
									        
									    	});
									    }
									});
                                }
                            }
                        ]
                    }
                }
 
                ,
                {  
                    name:'G1133_C18941', 
                    index:'G1133_C18941', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1133_C18942', 
                    index: 'G1133_C18942', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1133_C18943', 
                    index:'G1133_C18943', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1133_C18554', 
                    index: 'G1133_C18554', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1133_C17456', 
                    index:'G1133_C17456', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1133_C18555', 
                    index: 'G1133_C18555', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1133_C18556', 
                    index:'G1133_C18556', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1133_C18557', 
                    index:'G1133_C18557', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1133_C18558', 
                    index:'G1133_C18558', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1133_C18559', 
                    index:'G1133_C18559', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                { 
                    name:'G1133_C18560', 
                    index: 'G1133_C18560', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {
                    name:'G1133_C18561', 
                    index:'G1133_C18561', 
                    width:150, 
                    editable: true 
                }

                ,
                {  
                    name:'G1133_C17446', 
                    index:'G1133_C17446', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=847&campo=G1133_C17446'
                    }
                }

                ,
                {  
                    name:'G1133_C17447', 
                    index:'G1133_C17447', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=848&campo=G1133_C17447'
                    }
                }

                ,
                {  
                    name:'G1133_C17448', 
                    index:'G1133_C17448', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1133_C17449', 
                    index:'G1133_C17449', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }

                ,
                {
                    name:'G1133_C17450', 
                    index:'G1133_C17450', 
                    width:150, 
                    editable: true 
                }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_2); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles2",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1133_C20529',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Enviar Propuesta',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_2=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1133&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=20676&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless2").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_3(id_3){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless3").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_3=si&id='+id_3,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto comercial ','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','CUPO CLIENTE','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Pack','Cupo','Tiempo','Precio full sin IVA','Descuento','Valor con dto sin IVA','Vigencia','Obsequio','Observación','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                { 
                    name:'G1134_C20670', 
                    index: 'G1134_C20670', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17457', 
                    index: 'G1134_C17457', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17458', 
                    index: 'G1134_C17458', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C20333', 
                    index: 'G1134_C20333', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C20671', 
                    index: 'G1134_C20671', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C20672', 
                    index: 'G1134_C20672', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17459', 
                    index: 'G1134_C17459', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17460', 
                    index: 'G1134_C17460', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17461', 
                    index: 'G1134_C17461', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17462', 
                    index: 'G1134_C17462', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17463', 
                    index: 'G1134_C17463', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1134_C18912', 
                    index:'G1134_C18912', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1134_C17464', 
                    index: 'G1134_C17464', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17465', 
                    index: 'G1134_C17465', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17466', 
                    index: 'G1134_C17466', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17467', 
                    index: 'G1134_C17467', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17468', 
                    index: 'G1134_C17468', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1134_C18913', 
                    index:'G1134_C18913', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1134_C18913'
                    }
                }

                ,
                {  
                    name:'G1134_C18914', 
                    index:'G1134_C18914', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1134_C18915', 
                    index:'G1134_C18915', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                { 
                    name:'G1134_C17476', 
                    index: 'G1134_C17476', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17477', 
                    index: 'G1134_C17477', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17478', 
                    index: 'G1134_C17478', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1134_C17479', 
                    index: 'G1134_C17479', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1134_C18916', 
                    index:'G1134_C18916', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1134_C18916'
                    }
                }

                ,
                {  
                    name:'G1134_C18917', 
                    index:'G1134_C18917', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1134_C18917'
                    }
                }

                ,
                {  
                    name:'G1134_C18918', 
                    index:'G1134_C18918', 
                    width:300 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?MostrarCombo_Guion_1134_C18918=si',
                        dataInit:function(el){
                            $(el).select2({ 
					        	placeholder: "Buscar",
						        allowClear: false,
						        minimumInputLength: 3,
						        ajax: {
						            url: '<?=$url_crud;?>?CallDatosCombo_Guion_1134_C18918=si',
						            dataType: 'json',
						            type : 'post',
						            delay: 250,
						            data: function (params) {
						                return {
						                    q: params.term
						                };
						            },
					              	processResults: function(data) {
									  	return {
									    	results: $.map(data, function(obj) {
									      		return {
									        		id: obj.id,
									        		text: obj.text
									      		};
									    	})
									  	};
									},
						            cache: true
						        }
					    	});  
                        },
                        dataEvents: [
                            {  
                                type: 'change',
                                fn: function(e) {
                                    var r = this.id;
                                    var rId = r.split('_');

                                    $.ajax({
									    url   : '<?php echo $url_crud;?>',
									    data  : { dameValoresCamposDinamicos_Guion_1134_C18918 : this.value },
									    type  : 'post',
									    dataType : 'json',
									    success  : function(data){
									    	$.each(data, function(i, item){
									        
									    	});
									    }
									});
                                }
                            }
                        ]
                    }
                }
 
                ,
                {  
                    name:'G1134_C18919', 
                    index:'G1134_C18919', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1134_C18920', 
                    index: 'G1134_C18920', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1134_C18921', 
                    index:'G1134_C18921', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1134_C18344', 
                    index: 'G1134_C18344', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1134_C17481', 
                    index:'G1134_C17481', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1134_C18345', 
                    index: 'G1134_C18345', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1134_C18346', 
                    index:'G1134_C18346', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1134_C18347', 
                    index:'G1134_C18347', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1134_C18348', 
                    index:'G1134_C18348', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1134_C18349', 
                    index:'G1134_C18349', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                { 
                    name:'G1134_C18350', 
                    index: 'G1134_C18350', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {
                    name:'G1134_C17482', 
                    index:'G1134_C17482', 
                    width:150, 
                    editable: true 
                }

                ,
                {  
                    name:'G1134_C17469', 
                    index:'G1134_C17469', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=849&campo=G1134_C17469'
                    }
                }

                ,
                {  
                    name:'G1134_C17470', 
                    index:'G1134_C17470', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=850&campo=G1134_C17470'
                    }
                }
 
                ,
                {  
                    name:'G1134_C17471', 
                    index:'G1134_C17471', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }
 
                ,
                {  
                    name:'G1134_C17472', 
                    index:'G1134_C17472', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1134_C17473', 
                    index:'G1134_C17473', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1134_C17474', 
                    index:'G1134_C17474', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }

                ,
                {
                    name:'G1134_C17475', 
                    index:'G1134_C17475', 
                    width:150, 
                    editable: true 
                }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_3); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles3",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1134_C20670',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Enviar Link',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_3=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1134&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=20670&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless3").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_4(id_4){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless4").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_4=si&id='+id_4,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto Comercial ','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACIÓN','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Tipo de cliente','Fecha lectura de contrato','Hora','Extensión','Pack adquirido','Cupo','Tiempo','Descuento aplicado','Fecha de activación','Fecha fin','Observación','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                { 
                    name:'G1135_C20667', 
                    index: 'G1135_C20667', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17483', 
                    index: 'G1135_C17483', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17484', 
                    index: 'G1135_C17484', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17485', 
                    index: 'G1135_C17485', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C20668', 
                    index: 'G1135_C20668', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C20669', 
                    index: 'G1135_C20669', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17486', 
                    index: 'G1135_C17486', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17487', 
                    index: 'G1135_C17487', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17488', 
                    index: 'G1135_C17488', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17489', 
                    index: 'G1135_C17489', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C18901', 
                    index: 'G1135_C18901', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17490', 
                    index: 'G1135_C17490', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17491', 
                    index: 'G1135_C17491', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17492', 
                    index: 'G1135_C17492', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17493', 
                    index: 'G1135_C17493', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17494', 
                    index: 'G1135_C17494', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C18902', 
                    index: 'G1135_C18902', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1135_C18903', 
                    index:'G1135_C18903', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1135_C18903'
                    }
                }

                ,
                {  
                    name:'G1135_C18904', 
                    index:'G1135_C18904', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1135_C18905', 
                    index:'G1135_C18905', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                { 
                    name:'G1135_C17502', 
                    index: 'G1135_C17502', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17503', 
                    index: 'G1135_C17503', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17504', 
                    index: 'G1135_C17504', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C17505', 
                    index: 'G1135_C17505', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1135_C18906', 
                    index:'G1135_C18906', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1135_C18906'
                    }
                }

                ,
                {  
                    name:'G1135_C18907', 
                    index:'G1135_C18907', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1135_C18907'
                    }
                }

                ,
                {  
                    name:'G1135_C18908', 
                    index:'G1135_C18908', 
                    width:300 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?MostrarCombo_Guion_1135_C18908=si',
                        dataInit:function(el){
                            $(el).select2({ 
					        	placeholder: "Buscar",
						        allowClear: false,
						        minimumInputLength: 3,
						        ajax: {
						            url: '<?=$url_crud;?>?CallDatosCombo_Guion_1135_C18908=si',
						            dataType: 'json',
						            type : 'post',
						            delay: 250,
						            data: function (params) {
						                return {
						                    q: params.term
						                };
						            },
					              	processResults: function(data) {
									  	return {
									    	results: $.map(data, function(obj) {
									      		return {
									        		id: obj.id,
									        		text: obj.text
									      		};
									    	})
									  	};
									},
						            cache: true
						        }
					    	});  
                        },
                        dataEvents: [
                            {  
                                type: 'change',
                                fn: function(e) {
                                    var r = this.id;
                                    var rId = r.split('_');

                                    $.ajax({
									    url   : '<?php echo $url_crud;?>',
									    data  : { dameValoresCamposDinamicos_Guion_1135_C18908 : this.value },
									    type  : 'post',
									    dataType : 'json',
									    success  : function(data){
									    	$.each(data, function(i, item){
									        
									    	});
									    }
									});
                                }
                            }
                        ]
                    }
                }
 
                ,
                {  
                    name:'G1135_C18909', 
                    index:'G1135_C18909', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1135_C18910', 
                    index: 'G1135_C18910', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1135_C18911', 
                    index:'G1135_C18911', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1135_C18645', 
                    index:'G1135_C18645', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=912&campo=G1135_C18645'
                    }
                }

                ,
                {  
                    name:'G1135_C17507', 
                    index:'G1135_C17507', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1135_C17508', 
                    index:'G1135_C17508', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }

                ,
                { 
                    name:'G1135_C17509', 
                    index: 'G1135_C17509', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C18646', 
                    index: 'G1135_C18646', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1135_C18647', 
                    index:'G1135_C18647', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1135_C18648', 
                    index: 'G1135_C18648', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1135_C18649', 
                    index: 'G1135_C18649', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1135_C18650', 
                    index:'G1135_C18650', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1135_C18651', 
                    index:'G1135_C18651', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {
                    name:'G1135_C17512', 
                    index:'G1135_C17512', 
                    width:150, 
                    editable: true 
                }

                ,
                {  
                    name:'G1135_C17495', 
                    index:'G1135_C17495', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=851&campo=G1135_C17495'
                    }
                }

                ,
                {  
                    name:'G1135_C17496', 
                    index:'G1135_C17496', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=852&campo=G1135_C17496'
                    }
                }
 
                ,
                {  
                    name:'G1135_C17497', 
                    index:'G1135_C17497', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }
 
                ,
                {  
                    name:'G1135_C17498', 
                    index:'G1135_C17498', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1135_C17499', 
                    index:'G1135_C17499', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1135_C17500', 
                    index:'G1135_C17500', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }

                ,
                {
                    name:'G1135_C17501', 
                    index:'G1135_C17501', 
                    width:150, 
                    editable: true 
                }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_4); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles4",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1135_C20667',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Crear orden',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_4=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1135&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=20667&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless4").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_5(id_5){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless5").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_5=si&id='+id_5,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto comercial','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Cupo usados','Usados','Cupo nuevos','Nuevos','Observación','Categoría','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                { 
                    name:'G1136_C20664', 
                    index: 'G1136_C20664', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17513', 
                    index: 'G1136_C17513', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17514', 
                    index: 'G1136_C17514', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C20334', 
                    index: 'G1136_C20334', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C20665', 
                    index: 'G1136_C20665', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C20666', 
                    index: 'G1136_C20666', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17515', 
                    index: 'G1136_C17515', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17516', 
                    index: 'G1136_C17516', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17517', 
                    index: 'G1136_C17517', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17518', 
                    index: 'G1136_C17518', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17519', 
                    index: 'G1136_C17519', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17520', 
                    index: 'G1136_C17520', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17521', 
                    index: 'G1136_C17521', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17522', 
                    index: 'G1136_C17522', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17523', 
                    index: 'G1136_C17523', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17524', 
                    index: 'G1136_C17524', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1136_C18891', 
                    index:'G1136_C18891', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1136_C18892', 
                    index:'G1136_C18892', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1136_C18892'
                    }
                }

                ,
                {  
                    name:'G1136_C18893', 
                    index:'G1136_C18893', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1136_C18894', 
                    index:'G1136_C18894', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                { 
                    name:'G1136_C17532', 
                    index: 'G1136_C17532', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17533', 
                    index: 'G1136_C17533', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17534', 
                    index: 'G1136_C17534', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1136_C17535', 
                    index: 'G1136_C17535', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1136_C18895', 
                    index:'G1136_C18895', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1136_C18895'
                    }
                }

                ,
                {  
                    name:'G1136_C18896', 
                    index:'G1136_C18896', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1136_C18896'
                    }
                }

                ,
                {  
                    name:'G1136_C18897', 
                    index:'G1136_C18897', 
                    width:300 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?MostrarCombo_Guion_1136_C18897=si',
                        dataInit:function(el){
                            $(el).select2({ 
					        	placeholder: "Buscar",
						        allowClear: false,
						        minimumInputLength: 3,
						        ajax: {
						            url: '<?=$url_crud;?>?CallDatosCombo_Guion_1136_C18897=si',
						            dataType: 'json',
						            type : 'post',
						            delay: 250,
						            data: function (params) {
						                return {
						                    q: params.term
						                };
						            },
					              	processResults: function(data) {
									  	return {
									    	results: $.map(data, function(obj) {
									      		return {
									        		id: obj.id,
									        		text: obj.text
									      		};
									    	})
									  	};
									},
						            cache: true
						        }
					    	});  
                        },
                        dataEvents: [
                            {  
                                type: 'change',
                                fn: function(e) {
                                    var r = this.id;
                                    var rId = r.split('_');

                                    $.ajax({
									    url   : '<?php echo $url_crud;?>',
									    data  : { dameValoresCamposDinamicos_Guion_1136_C18897 : this.value },
									    type  : 'post',
									    dataType : 'json',
									    success  : function(data){
									    	$.each(data, function(i, item){
									        
									    	});
									    }
									});
                                }
                            }
                        ]
                    }
                }
 
                ,
                {  
                    name:'G1136_C18898', 
                    index:'G1136_C18898', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1136_C18899', 
                    index: 'G1136_C18899', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1136_C18900', 
                    index:'G1136_C18900', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1136_C17537', 
                    index:'G1136_C17537', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                , 
                { 
                    name:'G1136_C17536', 
                    index:'G1136_C17536', 
                    width:70 ,
                    editable: true, 
                    edittype:"checkbox",
                    editoptions: {
                        value:"1:0"
                    } 
                }

                ,
                {  
                    name:'G1136_C17539', 
                    index:'G1136_C17539', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                , 
                { 
                    name:'G1136_C17538', 
                    index:'G1136_C17538', 
                    width:70 ,
                    editable: true, 
                    edittype:"checkbox",
                    editoptions: {
                        value:"1:0"
                    } 
                }

                ,
                {
                    name:'G1136_C17540', 
                    index:'G1136_C17540', 
                    width:150, 
                    editable: true 
                }

                ,
                {  
                    name:'G1136_C18644', 
                    index:'G1136_C18644', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=911&campo=G1136_C18644'
                    }
                }

                ,
                {  
                    name:'G1136_C17525', 
                    index:'G1136_C17525', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=853&campo=G1136_C17525'
                    }
                }

                ,
                {  
                    name:'G1136_C17526', 
                    index:'G1136_C17526', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=854&campo=G1136_C17526'
                    }
                }
 
                ,
                {  
                    name:'G1136_C17527', 
                    index:'G1136_C17527', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }
 
                ,
                {  
                    name:'G1136_C17528', 
                    index:'G1136_C17528', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1136_C17529', 
                    index:'G1136_C17529', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1136_C17530', 
                    index:'G1136_C17530', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }

                ,
                {
                    name:'G1136_C17531', 
                    index:'G1136_C17531', 
                    width:150, 
                    editable: true 
                }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_5); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles5",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1136_C20664',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Cliente a escalar',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_5=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1136&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=20664&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless5").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_6(id_6){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless6").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_6=si&id='+id_6,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto comercial ','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','DEPARTAMENTO','CIUDAD','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Observacion','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                { 
                    name:'G1138_C20673', 
                    index: 'G1138_C20673', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17565', 
                    index: 'G1138_C17565', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17566', 
                    index: 'G1138_C17566', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17567', 
                    index: 'G1138_C17567', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C20674', 
                    index: 'G1138_C20674', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C20675', 
                    index: 'G1138_C20675', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17568', 
                    index: 'G1138_C17568', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17569', 
                    index: 'G1138_C17569', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17570', 
                    index: 'G1138_C17570', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17571', 
                    index: 'G1138_C17571', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17572', 
                    index: 'G1138_C17572', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C18922', 
                    index: 'G1138_C18922', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17573', 
                    index: 'G1138_C17573', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17574', 
                    index: 'G1138_C17574', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17575', 
                    index: 'G1138_C17575', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17576', 
                    index: 'G1138_C17576', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1138_C18923', 
                    index:'G1138_C18923', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1138_C18924', 
                    index:'G1138_C18924', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1138_C18924'
                    }
                }

                ,
                {  
                    name:'G1138_C18925', 
                    index:'G1138_C18925', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1138_C18926', 
                    index:'G1138_C18926', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                { 
                    name:'G1138_C17584', 
                    index: 'G1138_C17584', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17585', 
                    index: 'G1138_C17585', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17586', 
                    index: 'G1138_C17586', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1138_C17587', 
                    index: 'G1138_C17587', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1138_C18927', 
                    index:'G1138_C18927', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1138_C18927'
                    }
                }

                ,
                {  
                    name:'G1138_C18928', 
                    index:'G1138_C18928', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1138_C18928'
                    }
                }

                ,
                {  
                    name:'G1138_C18929', 
                    index:'G1138_C18929', 
                    width:300 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?MostrarCombo_Guion_1138_C18929=si',
                        dataInit:function(el){
                            $(el).select2({ 
					        	placeholder: "Buscar",
						        allowClear: false,
						        minimumInputLength: 3,
						        ajax: {
						            url: '<?=$url_crud;?>?CallDatosCombo_Guion_1138_C18929=si',
						            dataType: 'json',
						            type : 'post',
						            delay: 250,
						            data: function (params) {
						                return {
						                    q: params.term
						                };
						            },
					              	processResults: function(data) {
									  	return {
									    	results: $.map(data, function(obj) {
									      		return {
									        		id: obj.id,
									        		text: obj.text
									      		};
									    	})
									  	};
									},
						            cache: true
						        }
					    	});  
                        },
                        dataEvents: [
                            {  
                                type: 'change',
                                fn: function(e) {
                                    var r = this.id;
                                    var rId = r.split('_');

                                    $.ajax({
									    url   : '<?php echo $url_crud;?>',
									    data  : { dameValoresCamposDinamicos_Guion_1138_C18929 : this.value },
									    type  : 'post',
									    dataType : 'json',
									    success  : function(data){
									    	$.each(data, function(i, item){
									        
									    	});
									    }
									});
                                }
                            }
                        ]
                    }
                }
 
                ,
                {  
                    name:'G1138_C18930', 
                    index:'G1138_C18930', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1138_C18931', 
                    index: 'G1138_C18931', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1138_C18932', 
                    index:'G1138_C18932', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {
                    name:'G1138_C17588', 
                    index:'G1138_C17588', 
                    width:150, 
                    editable: true 
                }

                ,
                {  
                    name:'G1138_C17577', 
                    index:'G1138_C17577', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=857&campo=G1138_C17577'
                    }
                }

                ,
                {  
                    name:'G1138_C17578', 
                    index:'G1138_C17578', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=858&campo=G1138_C17578'
                    }
                }
 
                ,
                {  
                    name:'G1138_C17579', 
                    index:'G1138_C17579', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }
 
                ,
                {  
                    name:'G1138_C17580', 
                    index:'G1138_C17580', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1138_C17581', 
                    index:'G1138_C17581', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1138_C17582', 
                    index:'G1138_C17582', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }

                ,
                {
                    name:'G1138_C17583', 
                    index:'G1138_C17583', 
                    width:150, 
                    editable: true 
                }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_6); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles6",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1138_C20673',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Enviar presentación',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_6=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1138&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=20673&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless6").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_7(id_7){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless7").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_7=si&id='+id_7,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto comercial','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Causa','Observación','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                { 
                    name:'G1139_C20682', 
                    index: 'G1139_C20682', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17589', 
                    index: 'G1139_C17589', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17590', 
                    index: 'G1139_C17590', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C19164', 
                    index: 'G1139_C19164', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C20683', 
                    index: 'G1139_C20683', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C20684', 
                    index: 'G1139_C20684', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17591', 
                    index: 'G1139_C17591', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17592', 
                    index: 'G1139_C17592', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17593', 
                    index: 'G1139_C17593', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17594', 
                    index: 'G1139_C17594', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17595', 
                    index: 'G1139_C17595', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17596', 
                    index: 'G1139_C17596', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17597', 
                    index: 'G1139_C17597', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17598', 
                    index: 'G1139_C17598', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17599', 
                    index: 'G1139_C17599', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17600', 
                    index: 'G1139_C17600', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1139_C19165', 
                    index:'G1139_C19165', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1139_C19166', 
                    index:'G1139_C19166', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1139_C19166'
                    }
                }

                ,
                {  
                    name:'G1139_C19167', 
                    index:'G1139_C19167', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1139_C19168', 
                    index:'G1139_C19168', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                { 
                    name:'G1139_C17608', 
                    index: 'G1139_C17608', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17609', 
                    index: 'G1139_C17609', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17610', 
                    index: 'G1139_C17610', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1139_C17611', 
                    index: 'G1139_C17611', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1139_C19169', 
                    index:'G1139_C19169', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1139_C19169'
                    }
                }

                ,
                {  
                    name:'G1139_C19170', 
                    index:'G1139_C19170', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1139_C19170'
                    }
                }

                ,
                {  
                    name:'G1139_C19171', 
                    index:'G1139_C19171', 
                    width:300 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?MostrarCombo_Guion_1139_C19171=si',
                        dataInit:function(el){
                            $(el).select2({ 
					        	placeholder: "Buscar",
						        allowClear: false,
						        minimumInputLength: 3,
						        ajax: {
						            url: '<?=$url_crud;?>?CallDatosCombo_Guion_1139_C19171=si',
						            dataType: 'json',
						            type : 'post',
						            delay: 250,
						            data: function (params) {
						                return {
						                    q: params.term
						                };
						            },
					              	processResults: function(data) {
									  	return {
									    	results: $.map(data, function(obj) {
									      		return {
									        		id: obj.id,
									        		text: obj.text
									      		};
									    	})
									  	};
									},
						            cache: true
						        }
					    	});  
                        },
                        dataEvents: [
                            {  
                                type: 'change',
                                fn: function(e) {
                                    var r = this.id;
                                    var rId = r.split('_');

                                    $.ajax({
									    url   : '<?php echo $url_crud;?>',
									    data  : { dameValoresCamposDinamicos_Guion_1139_C19171 : this.value },
									    type  : 'post',
									    dataType : 'json',
									    success  : function(data){
									    	$.each(data, function(i, item){
									        
									    	});
									    }
									});
                                }
                            }
                        ]
                    }
                }
 
                ,
                {  
                    name:'G1139_C19172', 
                    index:'G1139_C19172', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1139_C19173', 
                    index: 'G1139_C19173', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1139_C19174', 
                    index:'G1139_C19174', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1139_C17612', 
                    index:'G1139_C17612', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=861&campo=G1139_C17612'
                    }
                }

                ,
                {
                    name:'G1139_C17613', 
                    index:'G1139_C17613', 
                    width:150, 
                    editable: true 
                }

                ,
                {  
                    name:'G1139_C17601', 
                    index:'G1139_C17601', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=859&campo=G1139_C17601'
                    }
                }

                ,
                {  
                    name:'G1139_C17602', 
                    index:'G1139_C17602', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=860&campo=G1139_C17602'
                    }
                }
 
                ,
                {  
                    name:'G1139_C17603', 
                    index:'G1139_C17603', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }
 
                ,
                {  
                    name:'G1139_C17604', 
                    index:'G1139_C17604', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1139_C17605', 
                    index:'G1139_C17605', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1139_C17606', 
                    index:'G1139_C17606', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }

                ,
                {
                    name:'G1139_C17607', 
                    index:'G1139_C17607', 
                    width:150, 
                    editable: true 
                }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_7); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles7",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1139_C20682',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Sacar de lista negra',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_7=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1139&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=20682&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless7").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_8(id_8){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless8").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_8=si&id='+id_8,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','CÓDIGO CLIENTE ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID CONTACTO COMERCIAL ','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACION','FECHA VENCIMIENTO','Agente','Fecha','Hora','Campaña','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?','Tipo de solicitud','Observación','Tipificación','ESTADO_TAREA','PASO_ID','REGISTRO_ID','Fecha Agenda','Hora Agenda','Observacion', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                { 
                    name:'G1140_C20685', 
                    index: 'G1140_C20685', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17614', 
                    index: 'G1140_C17614', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17615', 
                    index: 'G1140_C17615', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17616', 
                    index: 'G1140_C17616', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C20686', 
                    index: 'G1140_C20686', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C20687', 
                    index: 'G1140_C20687', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17617', 
                    index: 'G1140_C17617', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17618', 
                    index: 'G1140_C17618', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17619', 
                    index: 'G1140_C17619', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17620', 
                    index: 'G1140_C17620', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C20202', 
                    index: 'G1140_C20202', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17621', 
                    index: 'G1140_C17621', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17622', 
                    index: 'G1140_C17622', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17623', 
                    index: 'G1140_C17623', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17624', 
                    index: 'G1140_C17624', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17625', 
                    index: 'G1140_C17625', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1140_C20203', 
                    index:'G1140_C20203', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1140_C20204', 
                    index:'G1140_C20204', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1140_C20204'
                    }
                }

                ,
                {  
                    name:'G1140_C20205', 
                    index:'G1140_C20205', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1140_C20206', 
                    index:'G1140_C20206', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                { 
                    name:'G1140_C17633', 
                    index: 'G1140_C17633', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17634', 
                    index: 'G1140_C17634', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17635', 
                    index: 'G1140_C17635', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1140_C17636', 
                    index: 'G1140_C17636', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1140_C20231', 
                    index:'G1140_C20231', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1140_C20231'
                    }
                }

                ,
                {  
                    name:'G1140_C20232', 
                    index:'G1140_C20232', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1140_C20232'
                    }
                }

                ,
                {  
                    name:'G1140_C20233', 
                    index:'G1140_C20233', 
                    width:300 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?MostrarCombo_Guion_1140_C20233=si',
                        dataInit:function(el){
                            $(el).select2({ 
					        	placeholder: "Buscar",
						        allowClear: false,
						        minimumInputLength: 3,
						        ajax: {
						            url: '<?=$url_crud;?>?CallDatosCombo_Guion_1140_C20233=si',
						            dataType: 'json',
						            type : 'post',
						            delay: 250,
						            data: function (params) {
						                return {
						                    q: params.term
						                };
						            },
					              	processResults: function(data) {
									  	return {
									    	results: $.map(data, function(obj) {
									      		return {
									        		id: obj.id,
									        		text: obj.text
									      		};
									    	})
									  	};
									},
						            cache: true
						        }
					    	});  
                        },
                        dataEvents: [
                            {  
                                type: 'change',
                                fn: function(e) {
                                    var r = this.id;
                                    var rId = r.split('_');

                                    $.ajax({
									    url   : '<?php echo $url_crud;?>',
									    data  : { dameValoresCamposDinamicos_Guion_1140_C20233 : this.value },
									    type  : 'post',
									    dataType : 'json',
									    success  : function(data){
									    	$.each(data, function(i, item){
									        
									    	});
									    }
									});
                                }
                            }
                        ]
                    }
                }
 
                ,
                {  
                    name:'G1140_C20234', 
                    index:'G1140_C20234', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                { 
                    name:'G1140_C20235', 
                    index: 'G1140_C20235', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
 
                ,
                {  
                    name:'G1140_C20236', 
                    index:'G1140_C20236', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1140_C17637', 
                    index:'G1140_C17637', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=864&campo=G1140_C17637'
                    }
                }

                ,
                {
                    name:'G1140_C17638', 
                    index:'G1140_C17638', 
                    width:150, 
                    editable: true 
                }

                ,
                {  
                    name:'G1140_C17626', 
                    index:'G1140_C17626', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=862&campo=G1140_C17626'
                    }
                }

                ,
                {  
                    name:'G1140_C17627', 
                    index:'G1140_C17627', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=863&campo=G1140_C17627'
                    }
                }
 
                ,
                {  
                    name:'G1140_C17628', 
                    index:'G1140_C17628', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }
 
                ,
                {  
                    name:'G1140_C17629', 
                    index:'G1140_C17629', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1140_C17630', 
                    index:'G1140_C17630', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1140_C17631', 
                    index:'G1140_C17631', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }

                ,
                {
                    name:'G1140_C17632', 
                    index:'G1140_C17632', 
                    width:150, 
                    editable: true 
                }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_8); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles8",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1140_C20685',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'Solicitud especial',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_8=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'http://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1140&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=20685&formularioPadre=1122<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless8").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){
        
    	$("#btnLlamar_0").attr('padre', $("#G1122_C17129").val());
    		var id_0 = $("#G1122_C17129").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
    	$("#btnLlamar_1").attr('padre', $("#G1122_C0").val());
    		var id_1 = $("#G1122_C0").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
    		cargarHijos_1(id_1);
    	$("#btnLlamar_2").attr('padre', $("#G1122_C20661").val());
    		var id_2 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
    		cargarHijos_2(id_2);
    	$("#btnLlamar_3").attr('padre', $("#G1122_C20661").val());
    		var id_3 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion Recargar 
    		cargarHijos_3(id_3);
    	$("#btnLlamar_4").attr('padre', $("#G1122_C20661").val());
    		var id_4 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless4'); //funcion Recargar 
    		cargarHijos_4(id_4);
    	$("#btnLlamar_5").attr('padre', $("#G1122_C20661").val());
    		var id_5 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless5'); //funcion Recargar 
    		cargarHijos_5(id_5);
    	$("#btnLlamar_6").attr('padre', $("#G1122_C20661").val());
    		var id_6 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless6'); //funcion Recargar 
    		cargarHijos_6(id_6);
    	$("#btnLlamar_7").attr('padre', $("#G1122_C20661").val());
    		var id_7 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless7'); //funcion Recargar 
    		cargarHijos_7(id_7);
    	$("#btnLlamar_8").attr('padre', $("#G1122_C20661").val());
    		var id_8 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless8'); //funcion Recargar 
    		cargarHijos_8(id_8);
    }
</script>
<script type="text/javascript">
	$(document).ready(function() {
		<?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                		while($objDatos = $resultDatosSql->fetch_object()){ ?>
                        	document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
             		<?php  
             			}	
                    }
                    
                } */  
            }
        ?>
    	<?php if(isset($_GET['user'])){ ?>
        	
    	$("#btnLlamar_0").attr('padre', $("#G1122_C17129").val());
    		var id_0 = $("#G1122_C17129").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
    	$("#btnLlamar_1").attr('padre', $("#G1122_C0").val());
    		var id_1 = $("#G1122_C0").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
    		cargarHijos_1(id_1);
    	$("#btnLlamar_2").attr('padre', $("#G1122_C20661").val());
    		var id_2 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
    		cargarHijos_2(id_2);
    	$("#btnLlamar_3").attr('padre', $("#G1122_C20661").val());
    		var id_3 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion Recargar 
    		cargarHijos_3(id_3);
    	$("#btnLlamar_4").attr('padre', $("#G1122_C20661").val());
    		var id_4 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless4'); //funcion Recargar 
    		cargarHijos_4(id_4);
    	$("#btnLlamar_5").attr('padre', $("#G1122_C20661").val());
    		var id_5 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless5'); //funcion Recargar 
    		cargarHijos_5(id_5);
    	$("#btnLlamar_6").attr('padre', $("#G1122_C20661").val());
    		var id_6 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless6'); //funcion Recargar 
    		cargarHijos_6(id_6);
    	$("#btnLlamar_7").attr('padre', $("#G1122_C20661").val());
    		var id_7 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless7'); //funcion Recargar 
    		cargarHijos_7(id_7);
    	$("#btnLlamar_8").attr('padre', $("#G1122_C20661").val());
    		var id_8 = $("#G1122_C20661").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless8'); //funcion Recargar 
    		cargarHijos_8(id_8);
        	idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
		
	});
</script>
