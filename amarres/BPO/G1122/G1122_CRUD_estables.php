<?php
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');

    function vetarFincaRaiz($id_usuario){
        include(__DIR__."/../../conexion.php");
        $Lsql = "UPDATE ".$BaseDatos.".G1122 SET G1122_C18351 = 14544 WHERE G1122_ConsInte__b = ".$id_usuario;
        if($mysqli->query($Lsql) === true){
            /* Vetado */
        }else{
            echo "No se pudo Vetar";
        }
    }

    function llenar_tareas_finca_raiz($campana_crm, $id_usuario, $respuesta, $ultimoId, $estadofinal = null){
        include(__DIR__."/../../conexion.php");
        $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConfDinam_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana_crm;
       
        $res_Lsql_Campan = $mysqli->query($Lsql_Campan);

        $PasoLsql = "SELECT ESTPAS_ConsInte__b FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__CAMPAN_b = ".$campana_crm;
        $resPaso = $mysqli->query($PasoLsql);
        $pasoInicial = 0;
        if($resPaso){
            $estosPasos = $resPaso->fetch_array();
            $pasoInicial = $estosPasos['ESTPAS_ConsInte__b'];
        }

        if($res_Lsql_Campan){
            $datoCampan = $res_Lsql_Campan->fetch_array();
            $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
            $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
            $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
            $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
            $int_Guion_Camp_2 = "G".$datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
            $rea_ConfD_Campan = $datoCampan['CAMPAN_ConfDinam_b'];



            /* ya tenemos la muestra ahora toca insertar esa jugada */
            $LsqPregun  = "SELECT PREGUN_ConsInte__b , PREGUN_ConsInte__OPCION_B FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$int_Guion_Campan." AND PREGUN_Texto_____b = 'ESTADO_TAREA'";
            $resPregun  = $mysqli->query($LsqPregun);
            $idEstado = 0;
            $preguntaEstado = '';
            if($resPregun->num_rows > 0){
                $datoPregun = $resPregun->fetch_array();
                $preguntaEstado = $datoPregun['PREGUN_ConsInte__b'];
                /* Vamos a bscar el Id de sin gestion */
                $LisopcLsql = "SELECT LISOPC_ConsInte__b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$datoPregun['PREGUN_ConsInte__OPCION_B']." AND LISOPC_Nombre____b = 'Sin gestión'" ;


                $resLisop = $mysqli->query($LisopcLsql);
                if($resLisop){
                    $datosLisop = $resLisop->fetch_array();
                    $idEstado = $datosLisop['LISOPC_ConsInte__b'];
                    
                }
            }else{
                $array = array(
                    array('Sin gestión'),
                    array('En gestión'),
                    array('En gestión por devolución'),
                    array('Cerrada'),
                    array('Devuelta')
                );
                $tamanho = 5;
                /*Insertamos el OPCION */
                $insertLsql = "INSERT INTO ".$BaseDatos_systema.".OPCION (OPCION_ConsInte__GUION__b, OPCION_Nombre____b, OPCION_ConsInte__PROYEC_b, OPCION_FechCrea__b, OPCION_UsuaCrea__b) VALUES (".$int_Guion_Campan.", 'ESTADO_TAREA - ".$int_Guion_Campan."', ".$_SESSION['HUESPED'].", '".date('Y-m-d H:s:i')."', ".$_SESSION['IDENTIFICACION'].");";
                if($mysqli->query($insertLsql) === true){
                    /* Se inserto la lista perfectamente */
                    $ultimoLista = $mysqli->insert_id;
                    for ($i=0; $i < $tamanho ; $i++) { 
                        $insertLisopc = "INSERT INTO ".$BaseDatos_systema.".LISOPC (LISOPC_Nombre____b, LISOPC_ConsInte__OPCION_b, LISOPC_Posicion__b) VALUES ('".$array[$i][0]."', ".$ultimoLista.", ".$i.");";
                        if($mysqli->query($insertLisopc) === true){
                     
                        }else{
                            echo $mysqli->error;
                        }
                    }

                    $Lsql_Reintento_campo = "INSERT INTO ".$BaseDatos_systema.".G6(G6_C39, G6_C40, G6_C51, G6_C207, G6_C209, G6_C44) VALUES ('ESTADO_TAREA', 6, 1, ".$int_Guion_Campan.", 0, ".$ultimoLista.");";
                    if($mysqli->query($Lsql_Reintento_campo) === true){
                        $int_Reintento_campo = $mysqli->insert_id;
                        $Lsql_Editar_Guion = "UPDATE ".$BaseDatos_systema.".G5 SET G5_C312 = ".$int_Reintento_campo." WHERE G5_ConsInte__b = ".$int_Guion_Campan;
                        if($mysqli->query($Lsql_Editar_Guion) !== true){
                            echo "error => ".$mysqli->error;
                        }

                        $edit_Lsql = "ALTER TABLE ".$BaseDatos.".G".$int_Guion_Campan." ADD G".$int_Guion_Campan."_C".$int_Reintento_campo." int(10) DEFAULT NULL";
                        $mysqli->query($edit_Lsql);
                    }
                }
            }

            $numeroPaso = 0;
            $LsqPregun  = "SELECT PREGUN_ConsInte__b , PREGUN_ConsInte__GUION__PRE_B FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$int_Guion_Campan." AND PREGUN_Texto_____b = 'PASO_ID'";
            $resPregun  = $mysqli->query($LsqPregun);
            if($resPregun->num_rows === 0){
                $Lsql_Paso_campo = "INSERT INTO ".$BaseDatos_systema.".G6(G6_C39, G6_C40, G6_C51, G6_C207, G6_C209) VALUES ('PASO_ID', 3, 1, ".$int_Guion_Campan.",0);";
                if($mysqli->query($Lsql_Paso_campo) === true){
                    $int_Reintento_campo = $mysqli->insert_id;
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".G".$int_Guion_Campan." ADD G".$int_Guion_Campan."_C".$int_Reintento_campo." int(10) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }
            }else{
                $paso = $resPregun->fetch_array();
                $numeroPaso = $paso['PREGUN_ConsInte__b'];
            }


            $LsqPregun  = "SELECT PREGUN_ConsInte__b , PREGUN_ConsInte__GUION__PRE_B FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$int_Guion_Campan." AND PREGUN_Texto_____b = 'REGISTRO_ID'";
            $resPregun  = $mysqli->query($LsqPregun);
            $REGISTRO_ID = 0;
            if($resPregun->num_rows === 0){
                $Lsql_Paso_campo = "INSERT INTO ".$BaseDatos_systema.".G6(G6_C39, G6_C40, G6_C51, G6_C207, G6_C209) VALUES ('REGISTRO_ID', 3, 1, ".$int_Guion_Campan.", 0);";
                if($mysqli->query($Lsql_Paso_campo) === true){
                    $int_Reintento_campo = $mysqli->insert_id;
                    $edit_Lsql = "ALTER TABLE ".$BaseDatos.".G".$int_Guion_Campan." ADD G".$int_Guion_Campan."_C".$int_Reintento_campo." int(10) DEFAULT NULL";
                    $mysqli->query($edit_Lsql);
                }
            }else{
                $resgitrp = $resPregun->fetch_array();
                $REGISTRO_ID = $resgitrp['PREGUN_ConsInte__b'];
            }

            /**
             * Obtener el estado del cliente
             */
            
            $LsqlPregunEstado = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_Texto_____b = 'ESTADO CLIENTE' AND PREGUN_ConsInte__GUION__b = ".$int_Guion_Campan;
            $estadoCliente = null;
            $resPregun  = $mysqli->query($LsqlPregunEstado);
            if($resPregun->num_rows > 0){
                $arrayEstado = $resPregun->fetch_array();
                if($estadofinal != null){
                    $estadoCliente = ','.$int_Guion_Camp_2.'_C'.$arrayEstado['PREGUN_ConsInte__b']."=".$estadofinal;    
                }
                
            }

            $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana_crm;
            $resultcampSql = $mysqli->query($campSql);
            //$Lsql = 'UPDATE '.$BaseDatos.'.'.$int_Guion_Camp_2.' , '.$BaseDatos.'.'.$str_Pobla_Campan.' SET ';
            $i=0;
            $select = 'SELECT '.$str_Pobla_Campan.'_ConsInte__b';
            $insert = 'INSERT INTO '.$BaseDatos.'.'.$int_Guion_Camp_2.'(G'.$int_Guion_Campan.'_CodigoMiembro';
            while($key = $resultcampSql->fetch_object()){
                $validoparaedicion = false;
                $valorScript = $key->CAMINC_NomCamGui_b;

                $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$key->CAMINC_NomCamPob_b."'";

                //echo $LsqlShow;
                $resultShow = $mysqli->query($LsqlShow);
                if($resultShow->num_rows === 0){
                    //comentario el campo no existe
                    $validoparaedicion = false;
                }else{
                    $validoparaedicion = true;
                } 

                $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$int_Guion_Camp_2." WHERE Field = '".$key->CAMINC_NomCamGui_b."'";
                //echo $LsqlShow;
                $resultShow = $mysqli->query($LsqlShow);
                if($resultShow->num_rows === 0 ){
                    //comentario el campo no existe
                    $validoparaedicion = false;
                }else{
                    $validoparaedicion = true;
                } 

            
                if($validoparaedicion){
                    $select .= ', '.$key->CAMINC_NomCamPob_b;
                    $insert .= ', '.$valorScript;
                }
                
            } 
            $select .= ' FROM '.$BaseDatos.'.'.$str_Pobla_Campan.' WHERE '.$str_Pobla_Campan.'_ConsInte__b = '.$id_usuario; 
            $insert .= ') ';
            $Lsql = $insert.$select; 
            //echo "Esta select => ".$select;;
            if($mysqli->query($Lsql) === TRUE ){

                $ultimoIdQueseguadro = $mysqli->insert_id;
                /* Actualizamos el estado */
                if($preguntaEstado != ''){

                    $LsqlUpdate = "UPDATE ".$BaseDatos.".G".$int_Guion_Campan." SET ".$int_Guion_Camp_2."_C".$preguntaEstado." = ".$idEstado.", ".$int_Guion_Camp_2."_PoblacionOrigen = ".$int_Pobla_Camp_2." , ".$int_Guion_Camp_2."_FechaInsercion = '".date('Y-m-d H:i:s')."', ".$int_Guion_Camp_2."_C".$numeroPaso." = 442, ".$int_Guion_Camp_2."_C".$REGISTRO_ID." = ".$ultimoId." ".$estadoCliente;
                    
                   

                    $LsqlUpdate .= " WHERE  G".$int_Guion_Campan.'_ConsInte__b = '.$ultimoIdQueseguadro;
                    
                    //echo $LsqlUpdate;
                    if($mysqli->query($LsqlUpdate) === true){
                        $datos = array();
                        $datos['code'] = $respuesta;
                        $datos['ultimoId'] = $ultimoIdQueseguadro;
                        $datos['idFinalizacion'] = $ultimoId;
                        echo json_encode($datos);
                    }else{
                        $datos = array();
                        $datos['code'] = '-1';
                        $datos['error'] = "Error Actualizando el estado_dy".$mysqli->error;
                    }
                }
            
            }else{
                $datos = array();
                $datos['code'] = '-1';
                $datos['error'] = "NO SE ACTALIZO LA BASE DE DATOS => ".$mysqli->error;
                echo json_encode($datos);
            }

                
            
        }
    }
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
        if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1122_ConsInte__b, G1122_FechaInsercion , G1122_Usuario ,  G1122_CodigoMiembro  , G1122_PoblacionOrigen , G1122_EstadoDiligenciamiento ,  G1122_IdLlamada , G1122_C17121 as principal ,G1122_C20661,G1122_C17121,G1122_C17122,G1122_C18857,G1122_C20662,G1122_C20663,G1122_C17123,G1122_C17124,G1122_C17125,G1122_C17126,G1122_C17127,G1122_C17128,G1122_C17129,G1122_C17130,G1122_C17131,G1122_C17132,G1122_C18858,G1122_C18351,G1122_C18859,G1122_C18860,G1122_C17133,G1122_C17134,G1122_C17135,G1122_C17136,G1122_C17137,G1122_C17138,G1122_C17139,G1122_C17140,G1122_C17141,G1122_C17144,G1122_C17145,G1122_C17146,G1122_C18721,G1122_C18720,G1122_C17147,G1122_C17149,G1122_C17150,G1122_C17151,G1122_C17152 FROM '.$BaseDatos.'.G1122 WHERE G1122_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1122_C20661'] = $key->G1122_C20661;

                $datos[$i]['G1122_C17121'] = $key->G1122_C17121;

                $datos[$i]['G1122_C17122'] = $key->G1122_C17122;

                $datos[$i]['G1122_C18857'] = $key->G1122_C18857;

                $datos[$i]['G1122_C20662'] = $key->G1122_C20662;

                $datos[$i]['G1122_C20663'] = explode(' ', $key->G1122_C20663)[0];

                $datos[$i]['G1122_C17123'] = $key->G1122_C17123;

                $datos[$i]['G1122_C17124'] = $key->G1122_C17124;

                $datos[$i]['G1122_C17125'] = $key->G1122_C17125;

                $datos[$i]['G1122_C17126'] = $key->G1122_C17126;

                $datos[$i]['G1122_C17127'] = $key->G1122_C17127;

                $datos[$i]['G1122_C17128'] = $key->G1122_C17128;

                $datos[$i]['G1122_C17129'] = $key->G1122_C17129;

                $datos[$i]['G1122_C17130'] = $key->G1122_C17130;

                $datos[$i]['G1122_C17131'] = $key->G1122_C17131;

                $datos[$i]['G1122_C17132'] = $key->G1122_C17132;

                $datos[$i]['G1122_C18858'] = $key->G1122_C18858;

                $datos[$i]['G1122_C18351'] = $key->G1122_C18351;

                $datos[$i]['G1122_C18859'] = explode(' ', $key->G1122_C18859)[0];

                $datos[$i]['G1122_C18860'] = explode(' ', $key->G1122_C18860)[0];

                $datos[$i]['G1122_C17133'] = $key->G1122_C17133;

                $datos[$i]['G1122_C17134'] = $key->G1122_C17134;

                $datos[$i]['G1122_C17135'] = explode(' ', $key->G1122_C17135)[0];
  
                $hora = '';
                if(!is_null($key->G1122_C17136)){
                    $hora = explode(' ', $key->G1122_C17136)[1];
                }

                $datos[$i]['G1122_C17137'] = $key->G1122_C17137;

                $datos[$i]['G1122_C17138'] = $key->G1122_C17138;

                $datos[$i]['G1122_C17139'] = $key->G1122_C17139;

                $datos[$i]['G1122_C17140'] = $key->G1122_C17140;

                $datos[$i]['G1122_C17141'] = $key->G1122_C17141;

                $datos[$i]['G1122_C17144'] = $key->G1122_C17144;

                $datos[$i]['G1122_C17145'] = $key->G1122_C17145;

                $datos[$i]['G1122_C17146'] = $key->G1122_C17146;

                $datos[$i]['G1122_C18721'] = $key->G1122_C18721;

                $datos[$i]['G1122_C18720'] = $key->G1122_C18720;

                $datos[$i]['G1122_C17147'] = $key->G1122_C17147;

                $datos[$i]['G1122_C17149'] = $key->G1122_C17149;

                $datos[$i]['G1122_C17150'] = $key->G1122_C17150;

                $datos[$i]['G1122_C17151'] = $key->G1122_C17151;

                $datos[$i]['G1122_C17152'] = $key->G1122_C17152;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1122_ConsInte__b as id,  G1122_C17121 as camp1 , G1122_C17122 as camp2 ";
            $Lsql .= " FROM ".$BaseDatos.".G1122 ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " WHERE G1122_C17121 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1122_C17122 like '%".$_POST['Busqueda']."%' ";
            }
            $Lsql .= " ORDER BY G1122_ConsInte__b DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID']." ORDER BY LISOPC_Nombre____b ASC ";
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

        if(isset($_GET['MostrarCombo_Guion_G1122_C17146'])){
            echo '<select class="form-control input-sm"  name="G1122_C17146" id="G1122_C17146">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G1122_C17146'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_G1122_C17146'])){
             
            $Lsql = "SELECT  G1188_ConsInte__b as id , G1188_C18653 FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_G1122_C17146'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	            $data[$i]['id'] = $key->id;
                $data[$i]['cupo'] = $key->G1188_C18653;
                $i++;
            }
            
            echo json_encode($data);
        }
        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1122");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1122_ConsInte__b, G1122_FechaInsercion , G1122_Usuario ,  G1122_CodigoMiembro  , G1122_PoblacionOrigen , G1122_EstadoDiligenciamiento ,  G1122_IdLlamada , G1122_C17121 as principal ,G1122_C20661,G1122_C17121,G1122_C17122,G1122_C18857,G1122_C20662,G1122_C20663,G1122_C17123,G1122_C17124,G1122_C17125,G1122_C17126,G1122_C17127,G1122_C17128,G1122_C17129,G1122_C17130,G1122_C17131,G1122_C17132,G1122_C18858, a.LISOPC_Nombre____b as G1122_C18351,G1122_C18859,G1122_C18860, b.LISOPC_Nombre____b as G1122_C17133, c.LISOPC_Nombre____b as G1122_C17134,G1122_C17135,G1122_C17136,G1122_C17137,G1122_C17138,G1122_C17139,G1122_C17140,G1122_C17141, d.LISOPC_Nombre____b as G1122_C17144, e.LISOPC_Nombre____b as G1122_C17145, G1188_C18652,G1122_C18721,G1122_C18720,G1122_C17147, f.LISOPC_Nombre____b as G1122_C17149, g.LISOPC_Nombre____b as G1122_C17150, h.LISOPC_Nombre____b as G1122_C17151, i.LISOPC_Nombre____b as G1122_C17152 FROM '.$BaseDatos.'.G1122 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1122_C18351 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1122_C17133 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1122_C17134 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1122_C17144 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1122_C17145 LEFT JOIN '.$BaseDatos.'.G1188 ON G1188_ConsInte__b  =  G1122_C17146 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1122_C17149 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1122_C17150 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1122_C17151 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G1122_C17152';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1122_C17136)){
                    $hora_a = explode(' ', $fila->G1122_C17136)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1122_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1122_ConsInte__b , ($fila->G1122_C20661) , ($fila->G1122_C17121) , ($fila->G1122_C17122) , ($fila->G1122_C18857) , ($fila->G1122_C20662) , explode(' ', $fila->G1122_C20663)[0] , ($fila->G1122_C17123) , ($fila->G1122_C17124) , ($fila->G1122_C17125) , ($fila->G1122_C17126) , ($fila->G1122_C17127) , ($fila->G1122_C17128) , ($fila->G1122_C17129) , ($fila->G1122_C17130) , ($fila->G1122_C17131) , ($fila->G1122_C17132) , ($fila->G1122_C18858) , ($fila->G1122_C18351) , explode(' ', $fila->G1122_C18859)[0] , explode(' ', $fila->G1122_C18860)[0] , ($fila->G1122_C17133) , ($fila->G1122_C17134) , explode(' ', $fila->G1122_C17135)[0] , $hora_a , ($fila->G1122_C17137) , ($fila->G1122_C17138) , ($fila->G1122_C17139) , ($fila->G1122_C17140) , ($fila->G1122_C17141) , ($fila->G1122_C17144) , ($fila->G1122_C17145) , ($fila->G1188_C18652) , ($fila->G1122_C18721) , ($fila->G1122_C18720) , ($fila->G1122_C17147) , ($fila->G1122_C17149) , ($fila->G1122_C17150) , ($fila->G1122_C17151) , ($fila->G1122_C17152) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1122 WHERE G1122_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1122_ConsInte__b as id,  G1122_C17121 as camp1 , G1122_C17122 as camp2  FROM '.$BaseDatos.'.G1122 ORDER BY G1122_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1122 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1122(";
            $LsqlV = " VALUES ("; 
  
            $G1122_C20661 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1122_C20661"])){
                if($_POST["G1122_C20661"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1122_C20661 = $_POST["G1122_C20661"];
                    $LsqlU .= $separador." G1122_C20661 = ".$G1122_C20661."";
                    $LsqlI .= $separador." G1122_C20661";
                    $LsqlV .= $separador.$G1122_C20661;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1122_C17121"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17121 = '".$_POST["G1122_C17121"]."'";
                $LsqlI .= $separador."G1122_C17121";
                $LsqlV .= $separador."'".$_POST["G1122_C17121"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17122"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17122 = '".$_POST["G1122_C17122"]."'";
                $LsqlI .= $separador."G1122_C17122";
                $LsqlV .= $separador."'".$_POST["G1122_C17122"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C18857"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C18857 = '".$_POST["G1122_C18857"]."'";
                $LsqlI .= $separador."G1122_C18857";
                $LsqlV .= $separador."'".$_POST["G1122_C18857"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C20662"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C20662 = '".$_POST["G1122_C20662"]."'";
                $LsqlI .= $separador."G1122_C20662";
                $LsqlV .= $separador."'".$_POST["G1122_C20662"]."'";
                $validar = 1;
            }
             
 
            $G1122_C20663 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1122_C20663"])){    
                if($_POST["G1122_C20663"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1122_C20663"]);
                    if(count($tieneHora) > 1){
                        $G1122_C20663 = "'".$_POST["G1122_C20663"]."'";
                    }else{
                        $G1122_C20663 = "'".str_replace(' ', '',$_POST["G1122_C20663"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1122_C20663 = ".$G1122_C20663;
                    $LsqlI .= $separador." G1122_C20663";
                    $LsqlV .= $separador.$G1122_C20663;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1122_C17123"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17123 = '".$_POST["G1122_C17123"]."'";
                $LsqlI .= $separador."G1122_C17123";
                $LsqlV .= $separador."'".$_POST["G1122_C17123"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17124"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17124 = '".$_POST["G1122_C17124"]."'";
                $LsqlI .= $separador."G1122_C17124";
                $LsqlV .= $separador."'".$_POST["G1122_C17124"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17125"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17125 = '".$_POST["G1122_C17125"]."'";
                $LsqlI .= $separador."G1122_C17125";
                $LsqlV .= $separador."'".$_POST["G1122_C17125"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17126"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17126 = '".$_POST["G1122_C17126"]."'";
                $LsqlI .= $separador."G1122_C17126";
                $LsqlV .= $separador."'".$_POST["G1122_C17126"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17127"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17127 = '".$_POST["G1122_C17127"]."'";
                $LsqlI .= $separador."G1122_C17127";
                $LsqlV .= $separador."'".$_POST["G1122_C17127"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17128"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17128 = '".$_POST["G1122_C17128"]."'";
                $LsqlI .= $separador."G1122_C17128";
                $LsqlV .= $separador."'".$_POST["G1122_C17128"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17129"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17129 = '".$_POST["G1122_C17129"]."'";
                $LsqlI .= $separador."G1122_C17129";
                $LsqlV .= $separador."'".$_POST["G1122_C17129"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17130"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17130 = '".$_POST["G1122_C17130"]."'";
                $LsqlI .= $separador."G1122_C17130";
                $LsqlV .= $separador."'".$_POST["G1122_C17130"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17131"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17131 = '".$_POST["G1122_C17131"]."'";
                $LsqlI .= $separador."G1122_C17131";
                $LsqlV .= $separador."'".$_POST["G1122_C17131"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17132"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17132 = '".$_POST["G1122_C17132"]."'";
                $LsqlI .= $separador."G1122_C17132";
                $LsqlV .= $separador."'".$_POST["G1122_C17132"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C18351"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C18351 = '".$_POST["G1122_C18351"]."'";
                $LsqlI .= $separador."G1122_C18351";
                $LsqlV .= $separador."'".$_POST["G1122_C18351"]."'";
                $validar = 1;
            }
             
 
            $G1122_C18859 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1122_C18859"])){    
                if($_POST["G1122_C18859"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1122_C18859"]);
                    if(count($tieneHora) > 1){
                        $G1122_C18859 = "'".$_POST["G1122_C18859"]."'";
                    }else{
                        $G1122_C18859 = "'".str_replace(' ', '',$_POST["G1122_C18859"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1122_C18859 = ".$G1122_C18859;
                    $LsqlI .= $separador." G1122_C18859";
                    $LsqlV .= $separador.$G1122_C18859;
                    $validar = 1;
                }
            }
 
            $G1122_C18860 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1122_C18860"])){    
                if($_POST["G1122_C18860"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1122_C18860"]);
                    if(count($tieneHora) > 1){
                        $G1122_C18860 = "'".$_POST["G1122_C18860"]."'";
                    }else{
                        $G1122_C18860 = "'".str_replace(' ', '',$_POST["G1122_C18860"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1122_C18860 = ".$G1122_C18860;
                    $LsqlI .= $separador." G1122_C18860";
                    $LsqlV .= $separador.$G1122_C18860;
                    $validar = 1;
                }
            }
 
            $G1122_C17133 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1122_C17133 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1122_C17133 = ".$G1122_C17133;
                    $LsqlI .= $separador." G1122_C17133";
                    $LsqlV .= $separador.$G1122_C17133;
                    $validar = 1;

                    
                }
            }
 
            $G1122_C17134 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1122_C17134 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1122_C17134 = ".$G1122_C17134;
                    $LsqlI .= $separador." G1122_C17134";
                    $LsqlV .= $separador.$G1122_C17134;
                    $validar = 1;
                }
            }
 
            $G1122_C17135 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1122_C17135 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1122_C17135 = ".$G1122_C17135;
                    $LsqlI .= $separador." G1122_C17135";
                    $LsqlV .= $separador.$G1122_C17135;
                    $validar = 1;
                }
            }
 
            $G1122_C17136 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1122_C17136 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1122_C17136 = ".$G1122_C17136;
                    $LsqlI .= $separador." G1122_C17136";
                    $LsqlV .= $separador.$G1122_C17136;
                    $validar = 1;
                }
            }
 
            $G1122_C17137 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1122_C17137 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1122_C17137 = ".$G1122_C17137;
                    $LsqlI .= $separador." G1122_C17137";
                    $LsqlV .= $separador.$G1122_C17137;
                    $validar = 1;
                }
            }
  
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17138 = '".getNombreUser($_GET['token'])."'";
                $LsqlI .= $separador."G1122_C17138";
                $LsqlV .= $separador."'".getNombreUser($_GET['token'])."'";
                $validar = 1;
            
             
 
            $G1122_C17139 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }
            $G1122_C17139 = "'".date('Y-m-d H:i:s')."'";
            $LsqlU .= $separador." G1122_C17139 = ".$G1122_C17139;
            $LsqlI .= $separador." G1122_C17139";
            $LsqlV .= $separador.$G1122_C17139;
            $validar = 1;
           
 
            $G1122_C17140 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }
            $G1122_C17140 = "'".date('Y-m-d H:i:s')."'";
            $LsqlU .= $separador." G1122_C17140 = ".$G1122_C17140;
            $LsqlI .= $separador." G1122_C17140";
            $LsqlV .= $separador.$G1122_C17140;
            $validar = 1;
           
  
               /* $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array();
                $LsqlU .= $separador."G1122_C17141 = '".$dataCampa["CAMPAN_Nombre____b"]."'";
                $LsqlI .= $separador."G1122_C17141";
                $LsqlV .= $separador."'".$dataCampa["CAMPAN_Nombre____b"]."'";
                $validar = 1;*/
            
             
  
            if(isset($_POST["G1122_C17144"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17144 = '".$_POST["G1122_C17144"]."'";
                $LsqlI .= $separador."G1122_C17144";
                $LsqlV .= $separador."'".$_POST["G1122_C17144"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17145"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17145 = '".$_POST["G1122_C17145"]."'";
                $LsqlI .= $separador."G1122_C17145";
                $LsqlV .= $separador."'".$_POST["G1122_C17145"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17146"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17146 = '".$_POST["G1122_C17146"]."'";
                $LsqlI .= $separador."G1122_C17146";
                $LsqlV .= $separador."'".$_POST["G1122_C17146"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C18721"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C18721 = '".$_POST["G1122_C18721"]."'";
                $LsqlI .= $separador."G1122_C18721";
                $LsqlV .= $separador."'".$_POST["G1122_C18721"]."'";
                $validar = 1;
            }
             
  
            $G1122_C18720 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1122_C18720"])){
                if($_POST["G1122_C18720"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1122_C18720 = $_POST["G1122_C18720"];
                    $LsqlU .= $separador." G1122_C18720 = ".$G1122_C18720."";
                    $LsqlI .= $separador." G1122_C18720";
                    $LsqlV .= $separador.$G1122_C18720;
                    $validar = 1;
                }
            }
  
            $G1122_C17147 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1122_C17147"])){
                if($_POST["G1122_C17147"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1122_C17147 = $_POST["G1122_C17147"];
                    $LsqlU .= $separador." G1122_C17147 = ".$G1122_C17147."";
                    $LsqlI .= $separador." G1122_C17147";
                    $LsqlV .= $separador.$G1122_C17147;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1122_C17149"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17149 = '".$_POST["G1122_C17149"]."'";
                $LsqlI .= $separador."G1122_C17149";
                $LsqlV .= $separador."'".$_POST["G1122_C17149"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17150"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17150 = '".$_POST["G1122_C17150"]."'";
                $LsqlI .= $separador."G1122_C17150";
                $LsqlV .= $separador."'".$_POST["G1122_C17150"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17151"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17151 = '".$_POST["G1122_C17151"]."'";
                $LsqlI .= $separador."G1122_C17151";
                $LsqlV .= $separador."'".$_POST["G1122_C17151"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1122_C17152"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_C17152 = '".$_POST["G1122_C17152"]."'";
                $LsqlI .= $separador."G1122_C17152";
                $LsqlV .= $separador."'".$_POST["G1122_C17152"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1122_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1122_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1122_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1122_Usuario , G1122_FechaInsercion, G1122_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1122_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1122 WHERE G1122_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        
                        $ultimoId =  $mysqli->insert_id;
                        $respuesta = 1;
                       // echo "empezar => ". $_POST['G1122_C17151'];
                        if(isset($_POST['creamos']) && $_POST['creamos'] == 1){
                            $tipificacion = $_POST['G1122_C17150'];
                            if($tipificacion  == '14044'){
                                llenar_tareas_finca_raiz(572 , $_GET['CodigoMiembro'], 7, $ultimoId);
                            }else if($tipificacion  == '14039'){
                                llenar_tareas_finca_raiz(570 , $_GET['CodigoMiembro'], 8, $ultimoId);
                            }else{
                                $tipificacion2 = $_POST['G1122_C17151'];
                                switch ($tipificacion2) {
                                    case '14047':
                                        //Enviar Propuesta
                                        llenar_tareas_finca_raiz(565 , $_GET['CodigoMiembro'], 2, $ultimoId);
                                        break;

                                    case '14082':
                                        //Enviar Link de pago
                                        llenar_tareas_finca_raiz(566, $_GET['CodigoMiembro'], 3, $ultimoId);
                                        break;

                                    case '14059':
                                        //Tiene un cupo superior
                                        if($_POST['G1122_C17152'] == '14112'){
                                            //Cliente a escalar
                                            vetarFincaRaiz($ultimoId);
                                            llenar_tareas_finca_raiz(568, $_GET['CodigoMiembro'], 4, $ultimoId, '15415');
                                        }else{
                                            $datos = array();
                                            $datos['code'] = $respuesta;
                                            $datos['ultimoId'] = $ultimoId;
                                            echo json_encode($datos);
                                        }
                                        
                                        break;

                                    case '14063':
                                        //Solo tiene immuebles nuevos
                                        if($_POST['G1122_C17152'] == '14120'){
                                            //Cliente a escalar
                                            vetarFincaRaiz($ultimoId);
                                            llenar_tareas_finca_raiz(568, $_GET['CodigoMiembro'], 4, $ultimoIdm , '15415');
                                        }else{
                                            $datos = array();
                                            $datos['code'] = $respuesta;
                                            $datos['ultimoId'] = $ultimoId;
                                            echo json_encode($datos);
                                        }

                                        break;

                                    case '14064':
                                        //Otros municipios
                                        if($_POST['G1122_C17152'] == '14122'){
                                            //Cliente a escalar
                                            vetarFincaRaiz($ultimoId);
                                            llenar_tareas_finca_raiz(568, $_GET['CodigoMiembro'], 4, $ultimoId, '15415');
                                        }else{
                                            $datos = array();
                                            $datos['code'] = $respuesta;
                                            $datos['ultimoId'] = $ultimoId;
                                            echo json_encode($datos);
                                        }
                                        break;

                                    case '14048':
                                        //se envia Link de pago -- enviar link
                                        llenar_tareas_finca_raiz(566, $_GET['CodigoMiembro'], 3, $ultimoId);

                                        break;
                                        
                                    case '14083':
                                        //se le vencio el link
                                        if($_POST['G1122_C17152'] == '14157'){
                                            //se envia el mismo link -- enviar link
                                            llenar_tareas_finca_raiz(566, $_GET['CodigoMiembro'], 3, $ultimoId);

                                        }else if($_POST['G1122_C17152'] == '14158'){
                                            // se renueva con nueva oferta -- enviar link
                                            llenar_tareas_finca_raiz(566, $_GET['CodigoMiembro'], 3, $ultimoId);
                                        }else{
                                            $datos = array();
                                            $datos['code'] = $respuesta;
                                            $datos['ultimoId'] = $ultimoId;
                                            echo json_encode($datos);
                                        }
                                        
                                        break;

                                    case '14089':
                                        //cliente en lista negra -- sacar de lista negra
                                        if($_POST['G1122_C17152'] == '14164'){
                                            //EL cliente agrega eñ contacto - dacar de ista negra
                                            llenar_tareas_finca_raiz(571, $_GET['CodigoMiembro'], 5, $ultimoId);
                                        }else if($_POST['G1122_C17152'] == '14165'){
                                            // cambo de correo - savar de lista negra
                                            llenar_tareas_finca_raiz(571, $_GET['CodigoMiembro'], 5, $ultimoId);
                                        }else if($_POST['G1122_C17152'] == '14166'){
                                            //confirma que ya se agrego y no le llegan los correos - sacar de lista negra
                                            llenar_tareas_finca_raiz(571, $_GET['CodigoMiembro'], 5, $ultimoId);
                                        }else if($_POST['G1122_C17152'] == '14167'){
                                            //bandeja de entrada llena - sacar de lista negra
                                            llenar_tareas_finca_raiz(571, $_GET['CodigoMiembro'], 5, $ultimoId);
                                        }else if($_POST['G1122_C17152'] == '14168'){
                                            // sproblemas con el servidor - sacar de lista negra
                                            llenar_tareas_finca_raiz(571, $_GET['CodigoMiembro'], 5, $ultimoId);
                                        }else{
                                            $datos = array();
                                            $datos['code'] = $respuesta;
                                            $datos['ultimoId'] = $ultimoId;
                                            echo json_encode($datos);
                                        }
                                        break;

                                    case '14086':
                                        //activacion de la Ovi . crear orden
                                        llenar_tareas_finca_raiz(567, $_GET['CodigoMiembro'], 6, $ultimoId);
                                        break;

                                    default:
                                        $datos = array();
                                        $datos['code'] = $respuesta;
                                        $datos['ultimoId'] = $ultimoId;
                                        echo json_encode($datos);
                                        break;
                                }
                            }

                            


                        }else{
                            $datos = array();
                            $datos['code'] = $respuesta;
                            $datos['ultimoId'] = $ultimoId;
                            echo json_encode($datos);
                        }

                    }else{
                        echo "1";           
                    }

                } else {
        
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }
        }
    }


    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        $page = $_POST['page'];  // Almacena el numero de pagina actual
        $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
        $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
        $sord = $_POST['sord'];  // Almacena el modo de ordenación
        if(!$sidx) $sidx =1;

        $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1122 WHERE G1122_CodigoMiembro = '".$numero."'");
        // Se obtiene el resultado de la consulta
        $fila = $result->fetch_array();
        $count = $fila['count'];
        //En base al numero de registros se obtiene el numero de paginas
        if( $count >0 ) {
            $total_pages = ceil($count/$limit);
        } else {
            $total_pages = 0;
        }
        if ($page > $total_pages)
            $page=$total_pages;

        //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
        $start = $limit*$page - $limit; 

        $SQL = "SELECT G1122_ConsInte__b, a.LISOPC_Nombre____b as G1122_C17149 ,  b.LISOPC_Nombre____b as G1122_C17150,  c.LISOPC_Nombre____b as G1122_C17151,  d.LISOPC_Nombre____b as G1122_C17152, G1122_C17138, G1122_C17139, G1122_C17137, G1122_C17136 FROM ".$BaseDatos.".G1122  ";
        $SQL .= "LEFT JOIN ".$BaseDatos_systema.".LISOPC a ON a.LISOPC_ConsInte__b = G1122_C17149 ";
        $SQL .= "LEFT JOIN ".$BaseDatos_systema.".LISOPC b ON b.LISOPC_ConsInte__b = G1122_C17150 ";
        $SQL .= "LEFT JOIN ".$BaseDatos_systema.".LISOPC c ON c.LISOPC_ConsInte__b = G1122_C17151 ";
        $SQL .= "LEFT JOIN ".$BaseDatos_systema.".LISOPC d ON d.LISOPC_ConsInte__b = G1122_C17152 ";
        $SQL .= " WHERE G1122_CodigoMiembro = '".$numero."'"; 

        if ($_REQUEST["_search"] == "false") {

        } else {
            $operations = array(
                'eq' => "= '%s'",            // Equal
                'ne' => "<> '%s'",           // Not equal
                'lt' => "< '%s'",            // Less than
                'le' => "<= '%s'",           // Less than or equal
                'gt' => "> '%s'",            // Greater than
                'ge' => ">= '%s'",           // Greater or equal
                'bw' => "like '%s%%'",       // Begins With
                'bn' => "not like '%s%%'",   // Does not begin with
                'in' => "in ('%s')",         // In
                'ni' => "not in ('%s')",     // Not in
                'ew' => "like '%%%s'",       // Ends with
                'en' => "not like '%%%s'",   // Does not end with
                'cn' => "like '%%%s%%'",     // Contains
                'nc' => "not like '%%%s%%'", // Does not contain
                'nu' => "is null",           // Is null
                'nn' => "is not null"        // Is not null
            ); 
            $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
            $SQL .= sprintf(" AND %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
        }
        $SQL .= ' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
        $result = $mysqli->query($SQL);

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1122_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1122_ConsInte__b)."</cell>"; 
            echo "<cell>". ($fila->G1122_C17149)."</cell>";
            echo "<cell>". ($fila->G1122_C17150)."</cell>";
            echo "<cell>". ($fila->G1122_C17151)."</cell>";
            echo "<cell>". ($fila->G1122_C17152)."</cell>";
            echo "<cell>". ($fila->G1122_C17138)."</cell>";
            echo "<cell>". ($fila->G1122_C17139)."</cell>";
            echo "<cell>". ($fila->G1122_C17137)."</cell>";
            echo "<cell>". ($fila->G1122_C17136)."</cell>";
            
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_1"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1137_ConsInte__b, G1137_C17564 FROM ".$BaseDatos.".G1137";

        $SQL .= " WHERE G1137_CodigoMiembro = '".$numero."'"; 

        $SQL .= " ORDER BY G1137_C17564 DESC";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1137_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1137_ConsInte__b)."</cell>"; 
            echo "<cell><![CDATA[". ($fila->G1137_C17564)."]]></cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_2"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1133_ConsInte__b, G1133_C18554, G1133_C17456, G1133_C18555, G1133_C18556, G1133_C18557, G1133_C18558, G1133_C18559, G1133_C18560, G1133_C18561 FROM ".$BaseDatos.".G1133 ";

        $SQL .= " WHERE G1133_CodigoMiembro = '".$numero."'"; 

        $SQL .= " ORDER BY G1133_C18554";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1133_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1133_ConsInte__b)."</cell>"; 
            
            echo "<cell>". ($fila->G1133_C18554)."</cell>";

            echo "<cell>". $fila->G1133_C17456."</cell>"; 

            echo "<cell>". ($fila->G1133_C18555)."</cell>";

            echo "<cell>". $fila->G1133_C18556."</cell>"; 

            echo "<cell>". $fila->G1133_C18557."</cell>"; 

            echo "<cell>". $fila->G1133_C18558."</cell>"; 

            if($fila->G1133_C18559 != ''){
                echo "<cell>". explode(' ', $fila->G1133_C18559)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1133_C18560)."</cell>";

            echo "<cell><![CDATA[". ($fila->G1133_C18561)."]]></cell>";

            echo "</row>"; 
        } 

        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_3"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1134_ConsInte__b,G1134_C18344, G1134_C17481, G1134_C18345, G1134_C18346, G1134_C18347, G1134_C18348, G1134_C18349, G1134_C18350, G1134_C17482 FROM ".$BaseDatos.".G1134 ";

        $SQL .= " WHERE G1134_CodigoMiembro = '".$numero."'"; 

        $SQL .= " ORDER BY G1134_C20670";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1134_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1134_ConsInte__b)."</cell>"; 

            echo "<cell>". ($fila->G1134_C18344)."</cell>";

            echo "<cell>". $fila->G1134_C17481."</cell>"; 

            echo "<cell>". ($fila->G1134_C18345)."</cell>";

            echo "<cell>". $fila->G1134_C18346."</cell>"; 

            echo "<cell>". $fila->G1134_C18347."</cell>"; 

            echo "<cell>". $fila->G1134_C18348."</cell>"; 

            if($fila->G1134_C18349 != ''){
                echo "<cell>". explode(' ', $fila->G1134_C18349)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1134_C18350)."</cell>";

            echo "<cell><![CDATA[". ($fila->G1134_C17482)."]]></cell>";

            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_4"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1135_ConsInte__b, af.LISOPC_Nombre____b as  G1135_C18645, G1135_C17507, G1135_C17508, G1135_C17509, G1135_C18646, G1135_C18647, G1135_C18648, G1135_C18649, G1135_C18650, G1135_C18651, G1135_C17512 FROM ".$BaseDatos.".G1135 LEFT JOIN ".$BaseDatos_systema.".LISOPC as af ON af.LISOPC_ConsInte__b =  G1135_C18645 ";

        $SQL .= " WHERE G1135_CodigoMiembro = '".$numero."'"; 

        $SQL .= " ORDER BY G1135_ConsInte__b";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1135_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1135_ConsInte__b)."</cell>"; 
            
            echo "<cell>". ($fila->G1135_C18645)."</cell>";

            if($fila->G1135_C17507 != ''){
                echo "<cell>". explode(' ', $fila->G1135_C17507)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1135_C17508 != ''){
                echo "<cell>". explode(' ', $fila->G1135_C17508)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1135_C17509)."</cell>";

            echo "<cell>". ($fila->G1135_C18646)."</cell>";

            echo "<cell>". $fila->G1135_C18647."</cell>"; 

            echo "<cell>". ($fila->G1135_C18648)."</cell>";

            echo "<cell>". ($fila->G1135_C18649)."</cell>";

            if($fila->G1135_C18650 != ''){
                echo "<cell>". explode(' ', $fila->G1135_C18650)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1135_C18651 != ''){
                echo "<cell>". explode(' ', $fila->G1135_C18651)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell><![CDATA[". ($fila->G1135_C17512)."]]></cell>";

            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_5"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1136_ConsInte__b, G1136_C17537, G1136_C17536, G1136_C17539, G1136_C17538, G1136_C17540, ak.LISOPC_Nombre____b as  G1136_C18644 FROM ".$BaseDatos.".G1136  LEFT JOIN ".$BaseDatos_systema.".LISOPC as ak ON ak.LISOPC_ConsInte__b =  G1136_C18644  ";

        $SQL .= " WHERE G1136_CodigoMiembro  = '".$numero."'"; 

        $SQL .= " ORDER BY G1136_C20664";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1136_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1136_ConsInte__b)."</cell>"; 
            
            echo "<cell>". $fila->G1136_C17537."</cell>"; 

            echo "<cell>". $fila->G1136_C17536."</cell>"; 

            echo "<cell>". $fila->G1136_C17539."</cell>"; 

            echo "<cell>". $fila->G1136_C17538."</cell>"; 

            echo "<cell><![CDATA[". ($fila->G1136_C17540)."]]></cell>";

            echo "<cell>". ($fila->G1136_C18644)."</cell>";

           
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_6"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1138_ConsInte__b,  G1138_C17588 FROM ".$BaseDatos.".G1138   ";

        $SQL .= " WHERE G1138_CodigoMiembro = '".$numero."'"; 

        $SQL .= " ORDER BY G1138_C20673";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1138_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1138_ConsInte__b)."</cell>"; 
        
            echo "<cell><![CDATA[". ($fila->G1138_C17588)."]]></cell>";

            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_7"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1139_ConsInte__b, G1139_C17612, G1139_C17613 FROM ".$BaseDatos.".G1139 ";

        $SQL .= " WHERE G1139_CodigoMiembro = '".$numero."'"; 

        $SQL .= " ORDER BY G1139_C20682";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1139_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1139_ConsInte__b)."</cell>"; 

            echo "<cell>". ($fila->G1139_C17612)."</cell>";

            echo "<cell><![CDATA[". ($fila->G1139_C17613)."]]></cell>";

            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_8"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1140_ConsInte__b, af.LISOPC_Nombre____b as  G1140_C17637, G1140_C17638 FROM ".$BaseDatos.".G1140  LEFT JOIN ".$BaseDatos_systema.".LISOPC as r ON r.LISOPC_ConsInte__b =  G1140_C20204 LEFT JOIN ".$BaseDatos_systema.".LISOPC as af ON af.LISOPC_ConsInte__b =  G1140_C17637 ";

        $SQL .= " WHERE G1140_CodigoMiembro = '".$numero."'"; 

        $SQL .= " ORDER BY G1140_C20685";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1140_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1140_ConsInte__b)."</cell>";  

            echo "<cell>". ($fila->G1140_C17637)."</cell>";

            echo "<cell><![CDATA[". ($fila->G1140_C17638)."]]></cell>";
            
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }
  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1226 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1226(";
            $LsqlV = " VALUES ("; 
 
                                                                         
            if(isset($_POST["G1226_C20658"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1226_C20658 = '".$_POST["G1226_C20658"]."'";
                $LsqlI .= $separador."G1226_C20658";
                $LsqlV .= $separador."'".$_POST["G1226_C20658"]."'";
                $validar = 1;
            }
                                                                          
                                                                           

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1226_C20657 = $numero;
                    $LsqlU .= ", G1226_C20657 = ".$G1226_C20657."";
                    $LsqlI .= ", G1226_C20657";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1226_Usuario ,  G1226_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1226_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1226 WHERE  G1226_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }

?>
