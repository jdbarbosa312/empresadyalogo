<?php
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1059_ConsInte__b, G1059_FechaInsercion , G1059_Usuario ,  G1059_CodigoMiembro  , G1059_PoblacionOrigen , G1059_EstadoDiligenciamiento ,  G1059_IdLlamada , G1059_C15217 as principal ,G1059_C15258,G1059_C15259,G1059_C15217,G1059_C15216,G1059_C15226,G1059_C15228,G1059_C15227,G1059_C15218,G1059_C15219,G1059_C15221,G1059_C15222,G1059_C15223,G1059_C15220,G1059_C15224,G1059_C15225,G1059_C15230,G1059_C15231,G1059_C15232,G1059_C15374,G1059_C15375,G1059_C15376 FROM '.$BaseDatos.'.G1059 WHERE G1059_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1059_C15258'] = explode(' ', $key->G1059_C15258)[0];

                $datos[$i]['G1059_C15259'] = $key->G1059_C15259;

                $datos[$i]['G1059_C15217'] = $key->G1059_C15217;

                $datos[$i]['G1059_C15216'] = $key->G1059_C15216;

                $datos[$i]['G1059_C15226'] = $key->G1059_C15226;

                $datos[$i]['G1059_C15228'] = $key->G1059_C15228;

                $datos[$i]['G1059_C15227'] = $key->G1059_C15227;

                $datos[$i]['G1059_C15218'] = $key->G1059_C15218;

                $datos[$i]['G1059_C15219'] = $key->G1059_C15219;

                $datos[$i]['G1059_C15221'] = $key->G1059_C15221;

                $datos[$i]['G1059_C15222'] = $key->G1059_C15222;

                $datos[$i]['G1059_C15223'] = $key->G1059_C15223;

                $datos[$i]['G1059_C15220'] = $key->G1059_C15220;

                $datos[$i]['G1059_C15224'] = $key->G1059_C15224;

                $datos[$i]['G1059_C15225'] = $key->G1059_C15225;

                $datos[$i]['G1059_C15230'] = $key->G1059_C15230;

                $datos[$i]['G1059_C15231'] = $key->G1059_C15231;

                $datos[$i]['G1059_C15232'] = $key->G1059_C15232;

                $datos[$i]['G1059_C15374'] = $key->G1059_C15374;

                $datos[$i]['G1059_C15375'] = $key->G1059_C15375;

                $datos[$i]['G1059_C15376'] = $key->G1059_C15376;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1059_ConsInte__b as id,  G1059_C15217 as camp1 , G1059_C15216 as camp2 ";
            $Lsql .= " FROM ".$BaseDatos.".G1059 ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " WHERE G1059_C15217 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1059_C15216 like '%".$_POST['Busqueda']."%' ";
            }
            $Lsql .= " ORDER BY G1059_ConsInte__b DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1059");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1059_ConsInte__b, G1059_FechaInsercion , G1059_Usuario ,  G1059_CodigoMiembro  , G1059_PoblacionOrigen , G1059_EstadoDiligenciamiento ,  G1059_IdLlamada , G1059_C15217 as principal ,G1059_C15258,G1059_C15259,G1059_C15217,G1059_C15216,G1059_C15226,G1059_C15228,G1059_C15227,G1059_C15218,G1059_C15219,G1059_C15221,G1059_C15222,G1059_C15223,G1059_C15220,G1059_C15224,G1059_C15225,G1059_C15230,G1059_C15231, a.LISOPC_Nombre____b as G1059_C15232, b.LISOPC_Nombre____b as G1059_C15374, c.LISOPC_Nombre____b as G1059_C15375, d.LISOPC_Nombre____b as G1059_C15376 FROM '.$BaseDatos.'.G1059 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1059_C15232 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1059_C15374 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1059_C15375 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1059_C15376';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G1059_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1059_ConsInte__b , explode(' ', $fila->G1059_C15258)[0] , ($fila->G1059_C15259) , ($fila->G1059_C15217) , ($fila->G1059_C15216) , ($fila->G1059_C15226) , ($fila->G1059_C15228) , ($fila->G1059_C15227) , ($fila->G1059_C15218) , ($fila->G1059_C15219) , ($fila->G1059_C15221) , ($fila->G1059_C15222) , ($fila->G1059_C15223) , ($fila->G1059_C15220) , ($fila->G1059_C15224) , ($fila->G1059_C15225) , ($fila->G1059_C15230) , ($fila->G1059_C15231) , ($fila->G1059_C15232) , ($fila->G1059_C15374) , ($fila->G1059_C15375) , ($fila->G1059_C15376) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1059 WHERE G1059_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1059_ConsInte__b as id,  G1059_C15217 as camp1 , G1059_C15216 as camp2  FROM '.$BaseDatos.'.G1059 ORDER BY G1059_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1059 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1059(";
            $LsqlV = " VALUES ("; 
 
            $G1059_C15258 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1059_C15258"])){    
                if($_POST["G1059_C15258"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1059_C15258"]);
                    if(count($tieneHora) > 1){
                    	$G1059_C15258 = "'".$_POST["G1059_C15258"]."'";
                    }else{
                    	$G1059_C15258 = "'".str_replace(' ', '',$_POST["G1059_C15258"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1059_C15258 = ".$G1059_C15258;
                    $LsqlI .= $separador." G1059_C15258";
                    $LsqlV .= $separador.$G1059_C15258;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1059_C15259"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15259 = '".$_POST["G1059_C15259"]."'";
                $LsqlI .= $separador."G1059_C15259";
                $LsqlV .= $separador."'".$_POST["G1059_C15259"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15217"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15217 = '".$_POST["G1059_C15217"]."'";
                $LsqlI .= $separador."G1059_C15217";
                $LsqlV .= $separador."'".$_POST["G1059_C15217"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15216"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15216 = '".$_POST["G1059_C15216"]."'";
                $LsqlI .= $separador."G1059_C15216";
                $LsqlV .= $separador."'".$_POST["G1059_C15216"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15226"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15226 = '".$_POST["G1059_C15226"]."'";
                $LsqlI .= $separador."G1059_C15226";
                $LsqlV .= $separador."'".$_POST["G1059_C15226"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15228"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15228 = '".$_POST["G1059_C15228"]."'";
                $LsqlI .= $separador."G1059_C15228";
                $LsqlV .= $separador."'".$_POST["G1059_C15228"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15227"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15227 = '".$_POST["G1059_C15227"]."'";
                $LsqlI .= $separador."G1059_C15227";
                $LsqlV .= $separador."'".$_POST["G1059_C15227"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15218"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15218 = '".$_POST["G1059_C15218"]."'";
                $LsqlI .= $separador."G1059_C15218";
                $LsqlV .= $separador."'".$_POST["G1059_C15218"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15219"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15219 = '".$_POST["G1059_C15219"]."'";
                $LsqlI .= $separador."G1059_C15219";
                $LsqlV .= $separador."'".$_POST["G1059_C15219"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15221"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15221 = '".$_POST["G1059_C15221"]."'";
                $LsqlI .= $separador."G1059_C15221";
                $LsqlV .= $separador."'".$_POST["G1059_C15221"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15222"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15222 = '".$_POST["G1059_C15222"]."'";
                $LsqlI .= $separador."G1059_C15222";
                $LsqlV .= $separador."'".$_POST["G1059_C15222"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15223"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15223 = '".$_POST["G1059_C15223"]."'";
                $LsqlI .= $separador."G1059_C15223";
                $LsqlV .= $separador."'".$_POST["G1059_C15223"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15220"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15220 = '".$_POST["G1059_C15220"]."'";
                $LsqlI .= $separador."G1059_C15220";
                $LsqlV .= $separador."'".$_POST["G1059_C15220"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15224"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15224 = '".$_POST["G1059_C15224"]."'";
                $LsqlI .= $separador."G1059_C15224";
                $LsqlV .= $separador."'".$_POST["G1059_C15224"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15225"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15225 = '".$_POST["G1059_C15225"]."'";
                $LsqlI .= $separador."G1059_C15225";
                $LsqlV .= $separador."'".$_POST["G1059_C15225"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15230"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15230 = '".$_POST["G1059_C15230"]."'";
                $LsqlI .= $separador."G1059_C15230";
                $LsqlV .= $separador."'".$_POST["G1059_C15230"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15231"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15231 = '".$_POST["G1059_C15231"]."'";
                $LsqlI .= $separador."G1059_C15231";
                $LsqlV .= $separador."'".$_POST["G1059_C15231"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15232"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15232 = '".$_POST["G1059_C15232"]."'";
                $LsqlI .= $separador."G1059_C15232";
                $LsqlV .= $separador."'".$_POST["G1059_C15232"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15374"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15374 = '".$_POST["G1059_C15374"]."'";
                $LsqlI .= $separador."G1059_C15374";
                $LsqlV .= $separador."'".$_POST["G1059_C15374"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15375"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15375 = '".$_POST["G1059_C15375"]."'";
                $LsqlI .= $separador."G1059_C15375";
                $LsqlV .= $separador."'".$_POST["G1059_C15375"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1059_C15376"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_C15376 = '".$_POST["G1059_C15376"]."'";
                $LsqlI .= $separador."G1059_C15376";
                $LsqlV .= $separador."'".$_POST["G1059_C15376"]."'";
                $validar = 1;
            }
             

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1059_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1059_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}


			$padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1059_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
			if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1059_Usuario , G1059_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1059_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1059 WHERE G1059_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                    	echo $mysqli->insert_id;
                	}else{
                		echo "1";    		
                	}
                } else {
                	echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }
        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1101_ConsInte__b, G1101_C16723, b.LISOPC_Nombre____b as  G1101_C16715, c.LISOPC_Nombre____b as  G1101_C16719, d.LISOPC_Nombre____b as  G1101_C16729, G1101_C16717, G1101_C16718, G1101_C16716, G1101_C16720, G1101_C16721, G1101_C16722, G1101_C16730 FROM ".$BaseDatos.".G1101  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b =  G1101_C16715 LEFT JOIN ".$BaseDatos_systema.".LISOPC as c ON c.LISOPC_ConsInte__b =  G1101_C16719 LEFT JOIN ".$BaseDatos_systema.".LISOPC as d ON d.LISOPC_ConsInte__b =  G1101_C16729 ";

        $SQL .= " WHERE G1101_C16723 = '".$numero."'"; 

        $SQL .= " ORDER BY G1101_C16723";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1101_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1101_ConsInte__b)."</cell>"; 
            

            echo "<cell>". ($fila->G1101_C16723)."</cell>";

            echo "<cell>". ($fila->G1101_C16715)."</cell>";

            echo "<cell>". ($fila->G1101_C16719)."</cell>";

            echo "<cell>". ($fila->G1101_C16729)."</cell>";

            if($fila->G1101_C16717 != ''){
                echo "<cell>". explode(' ', $fila->G1101_C16717)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1101_C16718)."</cell>";

            echo "<cell><![CDATA[". ($fila->G1101_C16716)."]]></cell>";

            echo "<cell>". ($fila->G1101_C16720)."</cell>";

            if($fila->G1101_C16721 != ''){
                echo "<cell>". explode(' ', $fila->G1101_C16721)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1101_C16722 != ''){
                echo "<cell>". explode(' ', $fila->G1101_C16722)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". $fila->G1101_C16730."</cell>"; 
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1101 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1101(";
            $LsqlV = " VALUES ("; 
 
            if(isset($_POST["G1101_C16715"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16715 = '".$_POST["G1101_C16715"]."'";
                $LsqlI .= $separador."G1101_C16715";
                $LsqlV .= $separador."'".$_POST["G1101_C16715"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1101_C16719"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16719 = '".$_POST["G1101_C16719"]."'";
                $LsqlI .= $separador."G1101_C16719";
                $LsqlV .= $separador."'".$_POST["G1101_C16719"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1101_C16729"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16729 = '".$_POST["G1101_C16729"]."'";
                $LsqlI .= $separador."G1101_C16729";
                $LsqlV .= $separador."'".$_POST["G1101_C16729"]."'";
                $validar = 1;
            }

            $G1101_C16717 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1101_C16717"])){    
                if($_POST["G1101_C16717"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16717 = "'".str_replace(' ', '',$_POST["G1101_C16717"])." 00:00:00'";
                    $LsqlU .= $separador." G1101_C16717 = ".$G1101_C16717;
                    $LsqlI .= $separador." G1101_C16717";
                    $LsqlV .= $separador.$G1101_C16717;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1101_C16718"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16718 = '".$_POST["G1101_C16718"]."'";
                $LsqlI .= $separador."G1101_C16718";
                $LsqlV .= $separador."'".$_POST["G1101_C16718"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
  

            if(isset($_POST["G1101_C16716"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16716 = '".$_POST["G1101_C16716"]."'";
                $LsqlI .= $separador."G1101_C16716";
                $LsqlV .= $separador."'".$_POST["G1101_C16716"]."'";
                $validar = 1;
            }
                                                                           
 
                                                                         
            if(isset($_POST["G1101_C16720"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16720 = '".$_POST["G1101_C16720"]."'";
                $LsqlI .= $separador."G1101_C16720";
                $LsqlV .= $separador."'".$_POST["G1101_C16720"]."'";
                $validar = 1;
            }
                                                                          
                                                                           

            $G1101_C16721 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1101_C16721"])){    
                if($_POST["G1101_C16721"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16721 = "'".str_replace(' ', '',$_POST["G1101_C16721"])." 00:00:00'";
                    $LsqlU .= $separador." G1101_C16721 = ".$G1101_C16721;
                    $LsqlI .= $separador." G1101_C16721";
                    $LsqlV .= $separador.$G1101_C16721;
                    $validar = 1;
                }
            }
 
            $G1101_C16722 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1101_C16722"])){    
                if($_POST["G1101_C16722"] != '' && $_POST["G1101_C16722"] != 'undefined' && $_POST["G1101_C16722"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16722 = "'".$fecha." ".str_replace(' ', '',$_POST["G1101_C16722"])."'";
                    $LsqlU .= $separador."  G1101_C16722 = ".$G1101_C16722."";
                    $LsqlI .= $separador."  G1101_C16722";
                    $LsqlV .= $separador.$G1101_C16722;
                    $validar = 1;
                }
            }
 
            $G1101_C16730= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1101_C16730"])){    
                if($_POST["G1101_C16730"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16730 = $_POST["G1101_C16730"];
                    $LsqlU .= $separador." G1101_C16730 = '".$G1101_C16730."'";
                    $LsqlI .= $separador." G1101_C16730";
                    $LsqlV .= $separador."'".$G1101_C16730."'";
                    $validar = 1;
                }
            }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1101_C16723 = $numero;
                    $LsqlU .= ", G1101_C16723 = ".$G1101_C16723."";
                    $LsqlI .= ", G1101_C16723";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1101_Usuario ,  G1101_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1101_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1101 WHERE  G1101_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
