<?php
    session_start();
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario

        if(isset($_GET['EliminarTareaCancelada'])){

            $eliminar = "DELETE FROM ".$BaseDatos.".G1133 WHERE G1133_ConsInte__b = ".$_POST["id"];

            $eliminando = $mysqli->query($eliminar);

        }
 
      //Datos del formulario
      	if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1133_ConsInte__b, G1133_FechaInsercion ,USUARI_Nombre____b, G1133_Usuario ,  G1133_CodigoMiembro  , G1133_PoblacionOrigen , G1133_EstadoDiligenciamiento ,  G1133_IdLlamada , G1133_C17434 as principal ,G1133_C20676,G1133_C17434,G1133_CodigoMiembro,G1133_C17435,G1133_C17436,G1133_C20677,G1133_C20678,G1133_C17437,G1133_C17438,G1133_C17439,G1133_C17440,G1133_C18933,G1133_C17441,G1133_C17442,G1133_C17443,G1133_C17444,G1133_C17445,G1133_C18934,G1133_C18935,G1133_C18936,G1133_C18937,G1133_C22659,G1133_C17446,G1133_C17447,G1133_C17448,G1133_C17449,G1133_C17450,G1133_C17451,G1133_C17452,G1133_C17453,G1133_C17454,G1133_C21451,G1133_C21452,G1133_C18554,G1133_C27095,G1133_C27093,G1133_C27094,G1133_C20962,G1133_C18555,G1133_C18556,G1133_C18557,G1133_C18558,G1133_C18559,G1133_C18560,G1133_C18561,G1133_C20960,G1133_C20961,G1133_C20963,G1133_C18938,G1133_C18939,G1133_C18940,G1133_C18941,G1133_C18942,G1133_C18943 FROM '.$BaseDatos.'.G1133 LEFT JOIN DYALOGOCRM_SISTEMA.USUARI ON USUARI_ConsInte__b =G1133_Usuario  WHERE G1133_ConsInte__b ='.$_POST['id'];

            // echo $Lsql; die();
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1133_C20676'] = $key->G1133_C20676;

                $datos[$i]['G1133_CodigoMiembro'] = $key->G1133_CodigoMiembro;

                $datos[$i]['G1133_Usuario'] = $key->USUARI_Nombre____b;

                $datos[$i]['G1133_C17434'] = $key->G1133_C17434;

                $datos[$i]['G1133_C17435'] = $key->G1133_C17435;

                $datos[$i]['G1133_C17436'] = $key->G1133_C17436;

                $datos[$i]['G1133_C20677'] = $key->G1133_C20677;

                $datos[$i]['G1133_C20678'] = $key->G1133_C20678;

                $datos[$i]['G1133_C17437'] = $key->G1133_C17437;

                $datos[$i]['G1133_C17438'] = $key->G1133_C17438;

                $datos[$i]['G1133_C17439'] = $key->G1133_C17439;

                $datos[$i]['G1133_C17440'] = $key->G1133_C17440;

                $datos[$i]['G1133_C18933'] = $key->G1133_C18933;

                $datos[$i]['G1133_C17441'] = $key->G1133_C17441;

                $datos[$i]['G1133_C17442'] = $key->G1133_C17442;

                $datos[$i]['G1133_C17443'] = $key->G1133_C17443;

                $datos[$i]['G1133_C17444'] = $key->G1133_C17444;

                $datos[$i]['G1133_C17445'] = $key->G1133_C17445;

                $datos[$i]['G1133_C18934'] = $key->G1133_C18934;

                $datos[$i]['G1133_C18935'] = $key->G1133_C18935;

                $datos[$i]['G1133_C18936'] = explode(' ', $key->G1133_C18936)[0];

                $datos[$i]['G1133_C18937'] = explode(' ', $key->G1133_C18937)[0];

                $datos[$i]['G1133_C22659'] = $key->G1133_C22659;

                $datos[$i]['G1133_C17446'] = $key->G1133_C17446;

                $datos[$i]['G1133_C17447'] = $key->G1133_C17447;

                $datos[$i]['G1133_C17448'] = explode(' ', $key->G1133_C17448)[0];
  
                $hora = '';
                if(!is_null($key->G1133_C17449)){
                    $hora = explode(' ', $key->G1133_C17449)[1];
                }

                $datos[$i]['G1133_C17450'] = $key->G1133_C17450;

                $datos[$i]['G1133_C17451'] = $key->G1133_C17451;

                $datos[$i]['G1133_C17452'] = $key->G1133_C17452;

                $datos[$i]['G1133_C17453'] = $key->G1133_C17453;

                $datos[$i]['G1133_C17454'] = $key->G1133_C17454;

                $datos[$i]['G1133_C21451'] = $key->G1133_C21451;

                $datos[$i]['G1133_C21452'] = $key->G1133_C21452;

                $datos[$i]['G1133_C18554'] = $key->G1133_C18554;

                $datos[$i]['G1133_C27095'] = $key->G1133_C27095;

                $datos[$i]['G1133_C27093'] = $key->G1133_C27093;

                $datos[$i]['G1133_C27094'] = $key->G1133_C27094;

                $datos[$i]['G1133_C20962'] = explode(' ', $key->G1133_C20962)[0];

                $datos[$i]['G1133_C18555'] = $key->G1133_C18555;

                $datos[$i]['G1133_C18556'] = $key->G1133_C18556;

                $datos[$i]['G1133_C18557'] = $key->G1133_C18557;

                $datos[$i]['G1133_C18558'] = $key->G1133_C18558;

                $datos[$i]['G1133_C18559'] = explode(' ', $key->G1133_C18559)[0];

                $datos[$i]['G1133_C18560'] = $key->G1133_C18560;

                $datos[$i]['G1133_C18561'] = $key->G1133_C18561;

                $datos[$i]['G1133_C20960'] = explode(' ', $key->G1133_C20960)[0];
  
                $hora = '';
                if(!is_null($key->G1133_C20961)){
                    $hora = explode(' ', $key->G1133_C20961)[1];
                }
  
                $hora = '';
                if(!is_null($key->G1133_C20963)){
                    $hora = explode(' ', $key->G1133_C20963)[1];
                }

                $datos[$i]['G1133_C18938'] = $key->G1133_C18938;

                $datos[$i]['G1133_C18939'] = $key->G1133_C18939;

                $datos[$i]['G1133_C18940'] = $key->G1133_C18940;

                $datos[$i]['G1133_C18941'] = $key->G1133_C18941;

                $datos[$i]['G1133_C18942'] = $key->G1133_C18942;

                $datos[$i]['G1133_C18943'] = $key->G1133_C18943;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        /*Esto es para traer el historico del BackOffice*/
        if(isset($_POST['DameHistorico'])){

        	if(isset($_POST['campana_crm']) && $_POST['campana_crm'] != 0){
        		$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConfDinam_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST['campana_crm'];
	            $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	            $datoCampan = $res_Lsql_Campan->fetch_array();
	            $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	            $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	            $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];


	            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA_BACKOFFICE JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_POST['campana_crm']." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_POST['user_codigo_mien']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";
	            $res = $mysqli->query($Lsql);
	            while($key = $res->fetch_object()){
	                echo "<tr>";
	                echo "<td>".$key->MONOEF_Texto_____b."</td>";
	                echo "<td>".$key->CONDIA_Observacio_b."</td>";
	                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
	                echo "<td>".$key->USUARI_Nombre____b."</td>";
	                echo "</tr>";
	            }
        	}
            
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1133_ConsInte__b as id,  G1133_C17434 as camp1 , G1133_C17436 as camp2 , LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1133 LEFT JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1133_C17447  WHERE TRUE ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " AND ( G1133_C17434 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1133_C17436 like '%".$_POST['Busqueda']."%' ) ";
            }
            if(!is_null($_POST['CodMiembro']) && $_POST['CodMiembro'] != ''){
                $Lsql .= " AND G1133_CodigoMiembro = ". $_POST['CodMiembro'] ." ";
            }
            
			if(!is_null($_POST['G1133_C20676']) && $_POST['G1133_C20676'] != ''){
				$Lsql .= " AND G1133_C20676 LIKE '%". $_POST['G1133_C20676'] ."%' ";
			}
			if(!is_null($_POST['G1133_C17434']) && $_POST['G1133_C17434'] != ''){
				$Lsql .= " AND G1133_C17434 LIKE '%". $_POST['G1133_C17434'] ."%' ";
			}
			if(!is_null($_POST['G1133_C17435']) && $_POST['G1133_C17435'] != ''){
				$Lsql .= " AND G1133_C17435 LIKE '%". $_POST['G1133_C17435'] ."%' ";
			}
			if(!is_null($_POST['G1133_C17437']) && $_POST['G1133_C17437'] != ''){
				$Lsql .= " AND G1133_C17437 LIKE '%". $_POST['G1133_C17437'] ."%' ";
			}

            $Lsql .= " ORDER BY FIELD(G1133_C17447,14256,14257,14258,14259,14260), G1133_FechaInsercion DESC LIMIT 0, 50"; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){

            	$color = '';
                $strIconoBackOffice = '';
                if($key->estado == 'En gestión' || $key->estado == 'En gestión por devolución'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = '1';
                }else if($key->estado == 'Cerrada'){
                    $color = 'color:green;';
                    $strIconoBackOffice = '2';
                }else if($key->estado == 'Devuelta'){
                    $color = 'color:red;';
                    $strIconoBackOffice = '3';
                }else if($key->estado == 'Sin gestión'){
                    $color = 'color:orange;';
                    $strIconoBackOffice = '4';
                }

                $datos[$i]['camp1'] = mb_strtoupper(($key->camp1));
                $datos[$i]['camp2'] = mb_strtolower(($key->camp2));
                $datos[$i]['estado'] = $strIconoBackOffice;
                $datos[$i]['color'] = $color;
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID']." ORDER BY LISOPC_Nombre____b ASC";
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

        if(isset($_GET['MostrarCombo_Guion_G1133_C18940'])){
            echo '<select class="form-control input-sm"  name="G1133_C18940" id="G1133_C18940">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G1133_C18940'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_G1133_C18940'])){
             $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_G1133_C18940'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	            
                $i++;
            }
            
            echo json_encode($data);
        }
        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1133");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1133_ConsInte__b, G1133_FechaInsercion , G1133_Usuario ,  G1133_CodigoMiembro  , G1133_PoblacionOrigen , G1133_EstadoDiligenciamiento ,  G1133_IdLlamada , G1133_C17434 as principal ,G1133_C20676,G1133_C17434,G1133_C17435,G1133_C17436,G1133_C20677,G1133_C20678,G1133_C17437,G1133_C17438,G1133_C17439,G1133_C17440,G1133_C18933,G1133_C17441,G1133_C17442,G1133_C17443,G1133_C17444,G1133_C17445,G1133_C18934, a.LISOPC_Nombre____b as G1133_C18935,G1133_C18936,G1133_C18937,G1133_C22659, b.LISOPC_Nombre____b as G1133_C17446, c.LISOPC_Nombre____b as G1133_C17447,G1133_C17448,G1133_C17449,G1133_C17450,G1133_C17451,G1133_C17452,G1133_C17453,G1133_C17454,G1133_C21451,G1133_C21452, d.LISOPC_Nombre____b as G1133_C18554, e.LISOPC_Nombre____b as G1133_C27095, f.LISOPC_Nombre____b as G1133_C27093, g.LISOPC_Nombre____b as G1133_C27094,G1133_C20962, h.LISOPC_Nombre____b as G1133_C18555,G1133_C18556, i.LISOPC_Nombre____b as G1133_C18557,G1133_C18558,G1133_C18559,G1133_C18560,G1133_C18561,G1133_C20960,G1133_C20961,G1133_C20963, j.LISOPC_Nombre____b as G1133_C18938, k.LISOPC_Nombre____b as G1133_C18939,G1133_C18941,G1133_C18942,G1133_C18943 FROM '.$BaseDatos.'.G1133 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1133_C18935 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1133_C17446 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1133_C17447 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1133_C18554 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1133_C27095 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1133_C27093 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1133_C27094 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1133_C18555 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G1133_C18557 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G1133_C18938 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as k ON k.LISOPC_ConsInte__b =  G1133_C18939 LEFT JOIN '.$BaseDatos.'.G1188 ON G1188_ConsInte__b  =  G1133_C18940';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1133_C17449)){
                    $hora_a = explode(' ', $fila->G1133_C17449)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1133_C20961)){
                    $hora_b = explode(' ', $fila->G1133_C20961)[1];
                }

                $hora_c = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1133_C20963)){
                    $hora_c = explode(' ', $fila->G1133_C20963)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1133_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1133_ConsInte__b , ($fila->G1133_C20676) , ($fila->G1133_C17434) , ($fila->G1133_C17435) , ($fila->G1133_C17436) , ($fila->G1133_C20677) , ($fila->G1133_C20678) , ($fila->G1133_C17437) , ($fila->G1133_C17438) , ($fila->G1133_C17439) , ($fila->G1133_C17440) , ($fila->G1133_C18933) , ($fila->G1133_C17441) , ($fila->G1133_C17442) , ($fila->G1133_C17443) , ($fila->G1133_C17444) , ($fila->G1133_C17445) , ($fila->G1133_C18934) , ($fila->G1133_C18935) , explode(' ', $fila->G1133_C18936)[0] , explode(' ', $fila->G1133_C18937)[0] , ($fila->G1133_C22659) , ($fila->G1133_C17446) , ($fila->G1133_C17447) , explode(' ', $fila->G1133_C17448)[0] , $hora_a , ($fila->G1133_C17450) , ($fila->G1133_C17451) , ($fila->G1133_C17452) , ($fila->G1133_C17453) , ($fila->G1133_C17454) , ($fila->G1133_C21451) , ($fila->G1133_C21452) , ($fila->G1133_C18554) , ($fila->G1133_C27095) , ($fila->G1133_C27093) , ($fila->G1133_C27094) , explode(' ', $fila->G1133_C20962)[0] , ($fila->G1133_C18555) , ($fila->G1133_C18556) , ($fila->G1133_C18557) , ($fila->G1133_C18558) , explode(' ', $fila->G1133_C18559)[0] , ($fila->G1133_C18560) , ($fila->G1133_C18561) , explode(' ', $fila->G1133_C20960)[0] , $hora_b , $hora_c , ($fila->G1133_C18938) , ($fila->G1133_C18939) , ($fila->G1133_C18941) , ($fila->G1133_C18942) , ($fila->G1133_C18943) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1133 WHERE G1133_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $Zsql = 'SELECT  G1133_ConsInte__b as id,  G1133_C17434 as camp1 , G1133_C17436 as camp2, G1133_C17447 as estado FROM '.$BaseDatos.'.G1133 ORDER BY FIELD(G1133_C17447,14256,14257,14258,14259,14260), G1133_FechaInsercion DESC LIMIT '.$inicio.' , '.$fin;

            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                $color = '';
                $strIconoBackOffice = '';
                if($obj->estado == '14257' || $obj->estado == '14258'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = 'En gestión';
                }else if($obj->estado == '14259'){
                    $color = 'color:green;';
                    $strIconoBackOffice = 'Cerrada';
                }else if($obj->estado == '14260'){
                    $color = 'color:red;';
                    $strIconoBackOffice = 'Devuelta';
                }else if($obj->estado == '14256'){
                    $color = 'color:orange;';
                    $strIconoBackOffice = 'Sin gestión';
                }
                                                

                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."<span style='position: relative;right: 2px;float: right;font-size:10px;".$color."'>".$strIconoBackOffice."</i>
                        </p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1133 SET "; 
  
            if(isset($_POST["G1133_C20676"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C20676 = '".$_POST["G1133_C20676"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17434"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17434 = '".$_POST["G1133_C17434"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17435"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17435 = '".$_POST["G1133_C17435"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17436"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17436 = '".$_POST["G1133_C17436"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C20677"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C20677 = '".$_POST["G1133_C20677"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C20678"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C20678 = '".$_POST["G1133_C20678"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17437"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17437 = '".$_POST["G1133_C17437"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17438"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17438 = '".$_POST["G1133_C17438"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17439"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17439 = '".$_POST["G1133_C17439"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17440"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17440 = '".$_POST["G1133_C17440"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C18933"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18933 = '".$_POST["G1133_C18933"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17441"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17441 = '".$_POST["G1133_C17441"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17442"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17442 = '".$_POST["G1133_C17442"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17443"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17443 = '".$_POST["G1133_C17443"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17444"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17444 = '".$_POST["G1133_C17444"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17445"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17445 = '".$_POST["G1133_C17445"]."'";
                $validar = 1;
            }
             
  
            $G1133_C18934 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C18934"])){
                if($_POST["G1133_C18934"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18934 = $_POST["G1133_C18934"];
                    $LsqlU .= $separador." G1133_C18934 = ".$G1133_C18934."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1133_C18935"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18935 = '".$_POST["G1133_C18935"]."'";
                $validar = 1;
            }
             
 
            $G1133_C18936 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1133_C18936"])){    
                if($_POST["G1133_C18936"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1133_C18936"]);
                    if(count($tieneHora) > 1){
                    	$G1133_C18936 = "'".$_POST["G1133_C18936"]."'";
                    }else{
                    	$G1133_C18936 = "'".str_replace(' ', '',$_POST["G1133_C18936"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1133_C18936 = ".$G1133_C18936;
                    $validar = 1;
                }
            }
 
            $G1133_C18937 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1133_C18937"])){    
                if($_POST["G1133_C18937"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1133_C18937"]);
                    if(count($tieneHora) > 1){
                    	$G1133_C18937 = "'".$_POST["G1133_C18937"]."'";
                    }else{
                    	$G1133_C18937 = "'".str_replace(' ', '',$_POST["G1133_C18937"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1133_C18937 = ".$G1133_C18937;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1133_C22659"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C22659 = '".$_POST["G1133_C22659"]."'";
                $validar = 1;
            }
             
 
            $G1133_C17446 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1133_C17446 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1133_C17446 = ".$G1133_C17446;
                    $validar = 1;

                    
                }
            }
 
            $G1133_C17447 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1133_C17447 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1133_C17447 = ".$G1133_C17447;
                    $validar = 1;
                }
            }
 
            $G1133_C17450 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1133_C17450 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1133_C17450 = ".$G1133_C17450;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1133_C17451"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17451 = '".$_POST["G1133_C17451"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17452"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17452 = '".$_POST["G1133_C17452"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17453"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17453 = '".$_POST["G1133_C17453"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C17454"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C17454 = '".$_POST["G1133_C17454"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C21451"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C21451 = '".$_POST["G1133_C21451"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C21452"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C21452 = '".$_POST["G1133_C21452"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C18554"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18554 = '".$_POST["G1133_C18554"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C27095"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C27095 = '".$_POST["G1133_C27095"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C27093"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C27093 = '".$_POST["G1133_C27093"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C27094"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C27094 = '".$_POST["G1133_C27094"]."'";
                $validar = 1;
            }
             
 
            $G1133_C20962 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1133_C20962"])){    
                if($_POST["G1133_C20962"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1133_C20962"]);
                    if(count($tieneHora) > 1){
                    	$G1133_C20962 = "'".$_POST["G1133_C20962"]."'";
                    }else{
                    	$G1133_C20962 = "'".str_replace(' ', '',$_POST["G1133_C20962"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1133_C20962 = ".$G1133_C20962;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1133_C18555"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18555 = '".$_POST["G1133_C18555"]."'";
                $validar = 1;
            }
             
  
            $G1133_C18556 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C18556"])){
                if($_POST["G1133_C18556"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18556 = $_POST["G1133_C18556"];
                    $LsqlU .= $separador." G1133_C18556 = ".$G1133_C18556."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1133_C18557"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18557 = '".$_POST["G1133_C18557"]."'";
                $validar = 1;
            }
             
  
            $G1133_C18558 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C18558"])){
                if($_POST["G1133_C18558"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18558 = $_POST["G1133_C18558"];
                    $LsqlU .= $separador." G1133_C18558 = ".$G1133_C18558."";
                    $validar = 1;
                }
            }
 
            $G1133_C18559 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1133_C18559"])){    
                if($_POST["G1133_C18559"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1133_C18559"]);
                    if(count($tieneHora) > 1){
                    	$G1133_C18559 = "'".$_POST["G1133_C18559"]."'";
                    }else{
                    	$G1133_C18559 = "'".str_replace(' ', '',$_POST["G1133_C18559"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1133_C18559 = ".$G1133_C18559;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1133_C18560"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18560 = '".$_POST["G1133_C18560"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C18561"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18561 = '".$_POST["G1133_C18561"]."'";
                $validar = 1;
            }
             
 
            $G1133_C20960 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1133_C20960"])){    
                if($_POST["G1133_C20960"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1133_C20960"]);
                    if(count($tieneHora) > 1){
                    	$G1133_C20960 = "'".$_POST["G1133_C20960"]."'";
                    }else{
                    	$G1133_C20960 = "'".str_replace(' ', '',$_POST["G1133_C20960"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1133_C20960 = ".$G1133_C20960;
                    $validar = 1;
                }
            }
  
            // $G1133_C20961 = NULL;
            // //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            // if(isset($_POST["G1133_C20961"])){   
            //     if($_POST["G1133_C20961"] != '' && $_POST["G1133_C20961"] != 'undefined' && $_POST["G1133_C20961"] != 'null'){
            //         $separador = "";
            //         $fecha = date('Y-m-d');
            //         if($validar == 1){
            //             $separador = ",";
            //         }

            //         $G1133_C20961 = "'".$fecha." ".str_replace(' ', '',$_POST["G1133_C20961"])."'";
            //         $LsqlU .= $separador." G1133_C20961 = ".$G1133_C20961."";
            //         $validar = 1;
            //     }
            // }
  
            // $G1133_C20963 = NULL;
            // este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            // if(isset($_POST["G1133_C20963"])){   
            //     if($_POST["G1133_C20963"] != '' && $_POST["G1133_C20963"] != 'undefined' && $_POST["G1133_C20963"] != 'null'){
            //         $separador = "";
            //         $fecha = date('Y-m-d');
            //         if($validar == 1){
            //             $separador = ",";
            //         }

            //         $G1133_C20963 = "'".$fecha." ".str_replace(' ', '',$_POST["G1133_C20963"])."'";
            //         $LsqlU .= $separador." G1133_C20963 = ".$G1133_C20963."";
            //         $validar = 1;
            //     }
            // }
  
            if(isset($_POST["G1133_C18938"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18938 = '".$_POST["G1133_C18938"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C18939"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18939 = '".$_POST["G1133_C18939"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1133_C18940"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18940 = '".$_POST["G1133_C18940"]."'";
                $validar = 1;
            }
             
  
            $G1133_C18941 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C18941"])){
                if($_POST["G1133_C18941"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18941 = $_POST["G1133_C18941"];
                    $LsqlU .= $separador." G1133_C18941 = ".$G1133_C18941."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1133_C18942"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_C18942 = '".$_POST["G1133_C18942"]."'";
                $validar = 1;
            }
             
  
            $G1133_C18943 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1133_C18943"])){
                if($_POST["G1133_C18943"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1133_C18943 = $_POST["G1133_C18943"];
                    $LsqlU .= $separador." G1133_C18943 = ".$G1133_C18943."";
                    $validar = 1;
                }
            }

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1133_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}
           
             if(isset($_GET['ValAgen'])){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }                
                        $LsqlU .= $separador."G1133_Usuario = '".$_GET['ValAgen']."'";
                        $validar = 1;
                    }
			if(isset($_POST['oper'])){
                $Lsql = $LsqlU." WHERE G1133_ConsInte__b =".$_POST["id"]; 
            }
            //si trae algo que insertar inserta

            echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {

                	if(isset($_POST["campana_crm"]) && $_POST["campana_crm"] != 1){
                		/*Ahor ainsertamos en CONDIA BACKOFICE*/
	                    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana_crm"];
	                    //echo $Lsql_Campan;
	                    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	                    $datoCampan = $res_Lsql_Campan->fetch_array();
	                    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	                    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
	                    $intActualiza_oNo = $datoCampan['CAMPAN_ActPobGui_b']; 

	                    /* PARA SABER SI ACTUALIZAMOS O NO*/
	                    if($intActualiza_oNo == '-1'){
	                        /* toca hacer actualizacion desde Script */
	                        
	                        $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana_crm"];
	                        $resultcampSql = $mysqli->query($campSql);
	                        $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
	                        $i=0;
	                        while($key = $resultcampSql->fetch_object()){
	                            $validoparaedicion = false;
	                            $valorScript = $key->CAMINC_NomCamGui_b;

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$key->CAMINC_NomCamPob_b."'";

	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = '".$key->CAMINC_NomCamGui_b."'";
	                            //echo $LsqlShow;
	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0 ){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlPAsaNull = "SELECT ".$key->CAMINC_NomCamGui_b." as Campo_valido FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE  G".$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"];
	                            $LsqlRes = $mysqli->query($LsqlPAsaNull);
	                            if($LsqlRes){
	                                $sata = $LsqlRes->fetch_array();
	                                if($sata['Campo_valido'] != '' && $sata['Campo_valido'] != null){

	                                }else{
	                                    $valorScript = 'NULL';
	                                }
	                            }

	                            if($validoparaedicion){
	                                if($i == 0){
	                                    $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }else{
	                                    $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }
	                                $i++;    
	                            }
	                            
	                        } 
	                        $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"].' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
	                        //echo "Esta ".$Lsql;
	                        if($mysqli->query($Lsql) === TRUE ){

	                        }else{
	                            echo "NO SE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
	                        }
	                    }

	                    /* AHora toca insertar en Condia */
	                    $fecha_gestion = date('Y-m-d H:i:s');
	                    $CondiaSql = "INSERT INTO ".$BaseDatos_systema.".CONDIA_BACKOFFICE (
	                        CONDIA_IndiEfec__b, 
	                        CONDIA_TipNo_Efe_b, 
	                        CONDIA_ConsInte__MONOEF_b, 
	                        CONDIA_TiemDura__b, 
	                        CONDIA_Fecha_____b, 
	                        CONDIA_ConsInte__CAMPAN_b, 
	                        CONDIA_ConsInte__USUARI_b, 
	                        CONDIA_ConsInte__GUION__Gui_b, 
	                        CONDIA_ConsInte__GUION__Pob_b, 
	                        CONDIA_ConsInte__MUESTR_b, 
	                        CONDIA_CodiMiem__b, 
	                        CONDIA_Observacio_b) 
	                        VALUES (
	                        '".$_POST['tipificacion']."', 
	                        '".$_POST['reintento']."',
	                        '".$_POST['MonoEf']."',
	                        '".$fecha_gestion."',
	                        '".$fecha_gestion."',
	                        '".$_POST["campana_crm"]."',
	                        '".$_SESSION['IDENTIFICACION']."',
	                        '".$int_Guion_Campan."',
	                        '".$int_Pobla_Camp_2."',
	                        '".$int_Muest_Campan."',
	                        '".$_POST["id"]."',
	                        '".$_POST['textAreaComentarios']."'
	                    )";
                        

	                    echo $CondiaSql;
	                    if($mysqli->query($CondiaSql) === true){

	                    }else{
	                        echo "Error insertando Condia => ".$mysqli->error;
	                    }

	                    include '../funcion_Devolver_tarea.php';
                    	devolverTarea($int_Guion_Campan, $_POST['tipificacion'], $_POST['reintento'], $_POST['id']);
                	}

                	
                	
                    echo $_POST["id"];
                } else {
                	echo '0';
                    //echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }
        }
    }
  

  
?>
