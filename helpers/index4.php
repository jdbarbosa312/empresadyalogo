<?php
    $mysqli = new  mysqli("cloud2.dyalogodev.com", "dyrep", "DyR3p0rt3s");

    /* comprobar la conexión */
    if ($mysqli->connect_errno) {
        printf("Falló la conexión: %s\n", $mysqli->connect_error);
        exit();
    }

    if (!$mysqli->set_charset("utf8")) {
	    printf("Error cargando el conjunto de caracteres utf8: %s\n", $mysqli->error);
	    exit();
	} else {

	}

    $S = "DYALOGOCRM_SISTEMA";
    $W = "DYALOGOCRM_WEB";

    $campaCon = "SELECT CAMPAN_ConsInte__b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$S.".CAMPAN WHERE CAMPAN_TipoCamp__b = 6 AND CAMPAN_ConsInte__b <> 604 ORDER BY CAMPAN_ConsInte__b DESC;";

    $resultCam = $mysqli->query($campaCon);

    while ($campana = $resultCam->fetch_object()) {

        $guion = $campana->CAMPAN_ConsInte__GUION__Gui_b;
        $base = $campana->CAMPAN_ConsInte__GUION__Pob_b;

        $tablaBase = "SHOW COLUMNS FROM ".$W.".G".$base.";";
        $tablaGuion = "SHOW COLUMNS FROM ".$W.".G".$guion.";";

        if ($mysqli->query($tablaBase) && $mysqli->query($tablaGuion)) {
            $camincCon = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$S.".CAMINC WHERE CAMINC_ConsInte__GUION__Gui_b = ".$guion." AND CAMINC_ConsInte__GUION__Pob_b = ".$base.";";

            $resultCol = $mysqli->query($camincCon);

            $colSet = "";
            while ($columna = $resultCol->fetch_object()) {

                $nomColBase = $columna->CAMINC_NomCamPob_b;
                $nomColGuion = $columna->CAMINC_NomCamGui_b;

                $colBase = "SHOW COLUMNS FROM ".$W.".G".$base." LIKE '".$nomColBase."';";
                $colGuion = "SHOW COLUMNS FROM ".$W.".G".$guion." LIKE '".$nomColGuion."';";

                if ($mysqli->query($colBase) && $mysqli->query($colGuion)) {
                     $colSet .= $nomColGuion." = ".$nomColBase.",";
                }
            }
            $colSet = substr($colSet, 0, -1); 
             
            $conUpdate = "UPDATE ".$W.".G".$guion." as A 
                          LEFT JOIN ".$W.".G".$base." as B 
                          ON A.G".$guion."_CodigoMiembro = B.G".$base."_ConsInte__b
                          SET ".$colSet." WHERE G".$guion."_Usuario = -10";
                          
            if ($mysqli->query($conUpdate)) {
                echo "SE ACTUALIZAO ".$guion."<br/>";
            }else{
                echo "FALLO ".$guion."<br/>";
                echo $mysqli->error."<br/>";
                echo $conUpdate."<br/>";
            }
        }

    }
?>
