<?php

class ClearData{

    private $mysqli;

    public function __construct($mysqli){
        $this->mysqli = $mysqli;
    }

    public function sanearInput($input){
        //global $mysqli;
        if (is_array($input)) {
            foreach($input as $var=>$val) {
                $output[$var] = $this->sanearInput($val);
            }
        }
        else {
            if (get_magic_quotes_gpc()) {
                $input = stripslashes($input);
            }
            $input  = $this->cleanInput($input);
            $output = $this->mysqli->real_escape_string($input);
        }
        return $output;
    }

    private function cleanInput($input){
        $search = array(
            '@<script[^>]*?>.*?</script>@si',   // Elimina javascript
           /* '@<[\/\!]*?[^<>]*?>@si',            // Elimina las etiquetas HTML*/
            '@<style[^>]*?>.*?</style>@siU',    // Elimina las etiquetas de estilo
            '@<![\s\S]*?--[ \t\n\r]*>@'         // Elimina los comentarios multi-línea
          );
         
        $output = preg_replace($search, '', $input);
        return $output;
    }
}
