<?php

class MySQL{

	private $result;
	private $db;
	
	public function __construct(){
		//INTERNO
        // $this->db = new  mysqli("34.74.195.125","dyremadm","dy4dmiN4pp*bd");
        //BPO
        // $this->db = new  mysqli("34.73.56.78","dyjdb","dyjdb-DrqRc8rn0E");
        //CUS
        // $this->db = new  mysqli("customers.dyalogo.cloud","dyjdb","dyjdb-DrqRc8rn0E");
        //CLI
        // $this->db = new  mysqli("clientes.dyalogo.cloud","dyjdb","dyjdb-DrqRc8rn0E");
        //MERCURIO
        // $this->db = new  mysqli("34.73.64.114","eyrs","eyrs-ykfviNOJvY");
        //HERMES
        $this->db = new  mysqli("35.227.60.232","eyrs","eyrs-ykfviNOJvY");
        $this->result = array();
	}

	public function eliminando($id,$placa){


		$strSQLLogYorman = "delete from DYALOGOCRM_WEB.G2838 where G2838_C56824 = '".$placa."' and G2838_ConsInte__b != ".$id.";";

		$this->result = array();

		foreach ($this->db->query($strSQLLogYorman) as $res) {
			$this->result[] = $res;
		}

		return $this->result;

		$this->db = NULL;

	}

	public function finalfinal($id,$placa){


		$strSQLLogYorman = "update DYALOGOCRM_WEB.G2851 left join DYALOGOCRM_WEB.G2838 ON G2851_CodigoMiembro = G2838_ConsInte__b 
set G2851_CodigoMiembro = ".$id." where G2838_C56824 = '".$placa."';";

		$this->result = array();

		foreach ($this->db->query($strSQLLogYorman) as $res) {
			$this->result[] = $res;
		}

		return $this->result;

		$this->db = NULL;

	}

	public function correccion($placa){


		$strSQLLogYorman = "select G2838_ConsInte__b from DYALOGOCRM_WEB.G2838 where G2838_C56824 = '".$placa."' order by G2838_FecUltGes_b desc;";

		$this->result = array();

		foreach ($this->db->query($strSQLLogYorman) as $res) {
			$this->result[] = $res;
		}

		return $this->result;

		$this->db = NULL;

	}

	public function placas(){


		$strSQLLogYorman = "select G2838_C56824,count(1) as cantidad from DYALOGOCRM_WEB.G2838 where G2838_C56824 != '' group by G2838_C56824 having count(1) > 1 ORDER BY count(1) desc";

		$this->result = array();

		foreach ($this->db->query($strSQLLogYorman) as $res) {
			$this->result[] = $res;
		}

		return $this->result;

		$this->db = NULL;

	}

	// public function generalGMI($intId_p){


	// 	$strSQLLogYorman = "SELECT G2131_ConsInte__b AS id FROM DYALOGOCRM_WEB.G2131 JOIN DYALOGOCRM_SISTEMA.LISOPC L ON G2131_C39227 = L.LISOPC_ConsInte__b WHERE G2131_CodigoMiembro = ".$intId_p." order by G2131_Clasificacion desc, G2131_ConsInte__b desc limit 1";

	// 	$this->result = array();

	// 	foreach ($this->db->query($strSQLLogYorman) as $res) {
	// 		$this->result[] = $res;
	// 	}

	// 	return $this->result;

	// 	$this->db = NULL;

	// }

	// public function generalUG($intId_p){


	// 	$strSQLLogYorman = "SELECT G2131_ConsInte__b AS id FROM DYALOGOCRM_WEB.G2131 JOIN DYALOGOCRM_SISTEMA.LISOPC L ON G2131_C39227 = L.LISOPC_ConsInte__b WHERE G2131_CodigoMiembro = ".$intId_p." order by G2131_ConsInte__b desc limit 1";

	// 	$this->result = array();

	// 	foreach ($this->db->query($strSQLLogYorman) as $res) {
	// 		$this->result[] = $res;
	// 	}

	// 	return $this->result;

	// 	$this->db = NULL;

	// }

	// public function UG($intId_p){


	// 	$strSQLLogYorman = "SELECT G2131_ConsInte__b AS id FROM DYALOGOCRM_WEB.G2131 JOIN DYALOGOCRM_SISTEMA.LISOPC L ON G2131_C39227 = L.LISOPC_ConsInte__b WHERE G2131_CodigoMiembro = ".$intId_p." and G2131_Paso = 1549 order by G2131_ConsInte__b desc limit 1";

	// 	$this->result = array();

	// 	foreach ($this->db->query($strSQLLogYorman) as $res) {
	// 		$this->result[] = $res;
	// 	}

	// 	return $this->result;

	// 	$this->db = NULL;

	// }

	public function camposGuion($intIdGuion_p){


		$strSQLLogYorman = "SELECT PREGUN_ConsInte__b AS idCampo, PREGUN_Texto_____b AS nombreCampo FROM DYALOGOCRM_SISTEMA.PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$intIdGuion_p." AND ((PREGUN_Texto_____b = 'Tipificación' AND PREGUN_Tipo______b = 6) OR (PREGUN_Texto_____b = 'Fecha' AND PREGUN_Tipo______b = 1) OR (PREGUN_Texto_____b = 'Hora' AND PREGUN_Tipo______b = 1) OR (PREGUN_Texto_____b = 'Reintento' AND PREGUN_Tipo______b = 6) OR (PREGUN_Texto_____b = 'Fecha Agenda' AND PREGUN_Tipo______b = 5) OR (PREGUN_Texto_____b = 'Hora Agenda' AND PREGUN_Tipo______b = 10) OR (PREGUN_Texto_____b = 'Observacion' AND PREGUN_Tipo______b = 2)) ORDER BY PREGUN_Tipo______b ASC, PREGUN_Texto_____b ASC;";

		$this->result = array();

		foreach ($this->db->query($strSQLLogYorman) as $res) {
			$this->result[] = $res;
		}

		return $this->result;

		$this->db = NULL;

	}

	public function datosCampaña($intIdPaso_p){


		$strSQLLogYorman = "SELECT ESTPAS_ConsInte__b AS idPaso,ESTPAS_Tipo______b AS idTipo, ESTPAS_Nombre__b AS nombreTipo,ESTPAS_Comentari_b AS nombrePaso, (CASE WHEN ESTPAS_Tipo______b > 6 THEN ESTPAS_ConsInte__MUESTR_b ELSE CAMPAN_ConsInte__MUESTR_b END) AS muestra, ESTRAT_ConsInte_GUION_Pob AS base, CAMPAN_ConsInte__GUION__Gui_b AS guion from DYALOGOCRM_SISTEMA.ESTPAS LEFT JOIN DYALOGOCRM_SISTEMA.CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b LEFT JOIN DYALOGOCRM_SISTEMA.ESTRAT ON ESTPAS_ConsInte__ESTRAT_b = ESTRAT_ConsInte__b where ESTPAS_Tipo______b IN (1,6) AND ESTPAS_ConsInte__b = ".$intIdPaso_p." ORDER BY ESTPAS_Tipo______b ASC, ESTPAS_ConsInte__b ASC";

		$this->result = array();

		foreach ($this->db->query($strSQLLogYorman) as $res) {
			$this->result[] = $res;
		}

		return $this->result;

		$this->db = NULL;

	}

	public function UG($intId_p,$intIdPaso_p,$intIdGuion_p){


		$strSQLLogYorman = "SELECT G".$intIdGuion_p."_ConsInte__b AS id FROM DYALOGOCRM_WEB.G".$intIdGuion_p." WHERE G".$intIdGuion_p."_CodigoMiembro = ".$intId_p." AND G".$intIdGuion_p."_Paso = ".$intIdPaso_p." ORDER BY G".$intIdGuion_p."_ConsInte__b DESC LIMIT 1";

		$this->result = array();

		foreach ($this->db->query($strSQLLogYorman) as $res) {
			$this->result[] = $res;
		}

		return $this->result;

		$this->db = NULL;

	}

	public function GMI($intId_p,$intIdPaso_p,$intIdGuion_p){


		$strSQLLogYorman = "SELECT G".$intIdGuion_p."_ConsInte__b AS id FROM DYALOGOCRM_WEB.G".$intIdGuion_p." WHERE G".$intIdGuion_p."_CodigoMiembro = ".$intId_p." AND G".$intIdGuion_p."_Paso = ".$intIdPaso_p." ORDER BY G".$intIdGuion_p."_Clasificacion DESC, G".$intIdGuion_p."_ConsInte__b DESC LIMIT 1";

		$this->result = array();

		foreach ($this->db->query($strSQLLogYorman) as $res) {
			$this->result[] = $res;
		}

		return $this->result;

		$this->db = NULL;

	}

	public function registrosFontur($intIdGuion_t,$intIdPaso_t){


		$strSQLLogYorman = "SELECT G".$intIdGuion_t."_CodigoMiembro AS id FROM DYALOGOCRM_WEB.G".$intIdGuion_t." WHERE G".$intIdGuion_t."_Paso = ".$intIdPaso_t." GROUP BY G".$intIdGuion_t."_CodigoMiembro ORDER BY G".$intIdGuion_t."_CodigoMiembro DESC";

		// $strSQLLogYorman = "select G2139_ConsInte__b as id from DYALOGOCRM_WEB.G2139 order by G2139_ConsInte__b desc";

		$this->result = array();

		foreach ($this->db->query($strSQLLogYorman) as $res) {
			$this->result[] = $res;
		}

		return $this->result;

		$this->db = NULL;

	}
	
	// public function finca(){


	// 	$strSQLLogYorman = "select G1121_C17109, G1121_C17110, G1121_C17111 from DYALOGOCRM_WEB.G1121 WHERE G1121_C17109 IS NOT NULL AND G1121_C17110 IS NOT NULL AND G1121_C17111 IS NOT NULL limit 3";

	// 	$this->result = array();

	// 	$asdasd = $this->db->query($strSQLLogYorman);

	// 	$row = $asdasd->fetch_array(MYSQLI_NUM);

	// 	return $row;

	// 	$this->db = NULL;

	// }

	// public function collation2($tabla) {

	// 	$strSQL = "ALTER TABLE DYALOGOCRM_WEB.".$tabla." CHARACTER SET utf8 COLLATE utf8_general_ci;";

	// 	$resSQL = $this->db->query($strSQL);

	// 	$strSQL = "ALTER TABLE DYALOGOCRM_WEB.".$tabla." CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci";

	// 	$resSQL = $this->db->query($strSQL);

	// 	return $resSQL;

	// }
	// public function collation() {

	// 	$strSQL = "SELECT table_name
 //    FROM INFORMATION_SCHEMA.TABLES
 //    WHERE TABLE_SCHEMA = 'DYALOGOCRM_WEB'";

	// 	$resSQL = $this->db->query($strSQL);

	// 	return $resSQL;

	// }

	// public function registro() {

	// 	$strSQL = "select G1905_C34989 from DYALOGOCRM_WEB.G1905 where G1905_ConsInte__b = 8330";

	// 	$resSQL = $this->db->query($strSQL);

	// 	$objSQL = $resSQL->fetch_object();

	// 	return $objSQL->G1905_C34989;

	// }

	// public function insertarLogconsulta(){


	// 	$strSQLInsert_t = "UPDATE DYALOGOCRM_WEB.G1852_M1302 A JOIN (SELECT DISTINCT a.G1852_ConsInte__b, b.G1852_ConsInte__b FROM dy_cargue_datos.G185220200603064128 a JOIN DYALOGOCRM_WEB.G1852 b ON a.G1852_ConsInte__b = b.G1852_ConsInte__b ORDER BY b.G1852_ConsInte__b ASC LIMIT 0,0) B ON A.G1852_M1302_CoInMiPo__b = B.G1852_ConsInte__b SET A.G1852_M1302_ConIntUsu_b = 1677";

	// 	$this->db->query($strSQLInsert_t);

	// 	if ($this->db->query($strSQLInsert_t) === false) {
	// 		return $this->db;
	// 	}else{
	// 		return $this->db;
	// 	}
	// 	$this->db = NULL;

	// }

	// public function ejecucionUpdate($strSQLUpdate_p){


	// 	$strSQLUpdate_t = $strSQLUpdate_p;

	// 	$this->result = array();

	// 	if ($this->db->query($strSQLUpdate_t)) {
	// 		return true;
	// 	}else{
	// 		return false;
	// 	}

	// 	$this->db = NULL;

	// }
	// public function traerConsultasFallidas(){


	// 	$strSQLLogYorman = "SELECT sqlGenerado FROM DYALOGOCRM_WEB.log_yorman_temporal WHERE sqlGenerado like '%sin canal%'";

	// 	$this->result = array();

	// 	foreach ($this->db->query($strSQLLogYorman) as $res) {
	// 		$this->result[] = $res;
	// 	}

	// 	return $this->result;

	// 	$this->db = NULL;

	// }


	// public function eliminarTodasLasMetricas(){


	// 	$strSQLeliminarTodasLasMetricas_t = "DELETE FROM DYALOGOCRM_SISTEMA.METMED";

	// 	if ($this->db->query($strSQLeliminarTodasLasMetricas_t)) {
	// 		return "Se han eliminado todas las metricas";
	// 	}else{
	// 		return "No se pudo eliminar las metricas";
	// 	}


	// 	$this->db = NULL;

	// }
	// public function eliminarTodasLasMetas(){


	// 	$strSQLeliminarTodasLasMetas_t = "DELETE FROM DYALOGOCRM_SISTEMA.METDEF";

	// 	if ($this->db->query($strSQLeliminarTodasLasMetas_t)) {
	// 		return "Se han eliminado todas las metas";
	// 	}else{
	// 		return "No se pudo eliminar las metas";
	// 	}


	// 	$this->db = NULL;

	// }
	// public function insertarNuevaMeta($strNombreMeta_p,$intIdEstrat_p,$intIdPaso_p,$strConfiguraion_p){


	// 	$strSQLinsertarNuevaMeta = "INSERT INTO DYALOGOCRM_SISTEMA.METDEF (METDEF_Nombre___b,METDEF_Consinte__ESTRAT_b,METDEF_Consinte__ESTPAS_b,METDEF_Nivel_____b,METDEF_Tipo______b,METDEF_SubTipo___b,METDEF_Rango____b) VALUES ('".$strNombreMeta_p."',".$intIdEstrat_p.",".$intIdPaso_p.",".$strConfiguraion_p.");";

	// 		if ($this->db->query($strSQLinsertarNuevaMeta)) {
	// 			return "
	// 	Exito= Meta : ".$strNombreMeta_p.", IdEstrat : ".$intIdEstrat_p.", IdPaso : ".$intIdPaso_p;
	// 		}else{
	// 			return "
	// 	Fallo = Meta : ".$strNombreMeta_p.", IdEstrat : ".$intIdEstrat_p.", IdPaso : ".$intIdPaso_p;
	// 		}


	// 		$this->db = NULL;

	// }
	// public function MetasExistentes($intIdPaso_p,$intNivel_p,$intTipo_p,$intSubTipo_p,$intRango_p){


	// 	$strSQLMetasExistentes = "SELECT METDEF_Consinte__b 
 //                      FROM DYALOGOCRM_SISTEMA.METDEF 
 //                      WHERE METDEF_Consinte__ESTPAS_b = ".$intIdPaso_p." AND 
 //                      METDEF_Nivel_____b = ".$intNivel_p." AND 
 //                      METDEF_Tipo______b = ".$intTipo_p." AND 
 //                      METDEF_SubTipo___b = ".$intSubTipo_p." AND 
 //                      METDEF_Rango____b = ".$intRango_p.";";

	// 	$this->result = array();

	// 	foreach ($this->db->query($strSQLMetasExistentes) as $res) {
	// 		$this->result[] = $res;
	// 	}

	// 	return $this->result;

	// 	$this->db = NULL;

	// }
	// public function IdTiposDePaso($intIdEstrat_p){


	// 	$strSQLIdTiposDePaso = "SELECT ESTPAS_ConsInte__b as id, ESTPAS_Tipo______b as tipo FROM DYALOGOCRM_SISTEMA.ESTPAS WHERE ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_p." AND (ESTPAS_Tipo______b = 1 OR ESTPAS_Tipo______b = 6)";

	// 	$this->result = array();

	// 	foreach ($this->db->query($strSQLIdTiposDePaso) as $res) {
	// 		$this->result[] = $res;
	// 	}

	// 	return $this->result;

	// 	$this->db = NULL;

	// }
	// public function IdEstrat(){


	// 	$strSQLIdEstrat = "SELECT ESTRAT_ConsInte__b AS id FROM DYALOGOCRM_SISTEMA.ESTRAT";

	// 	$this->result = array();

	// 	foreach ($this->db->query($strSQLIdEstrat) as $res) {
	// 		$this->result[] = $res;
	// 	}

	// 	return $this->result;

	// 	$this->db = NULL;

	// }
	public function traerIdBases($intTipoBase_p){


		$strSQLtraerIdBases_t = "SELECT GUION__ConsInte__b AS id FROM DYALOGOCRM_SISTEMA.GUION_ WHERE GUION__Tipo______b = ".$intTipoBase_p;

		$this->result = array();
		
		foreach ($this->db->query($strSQLtraerIdBases_t) as $res) {
			$this->result[] = $res;
		}

		return $this->result;

		$this->db = NULL;

	}

}

?>