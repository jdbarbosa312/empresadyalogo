<?php

require_once("conexionModelo.php");


$mysql = new MySQL();

//JDBD - Eliminamos todas las metas.
$resEliminarMetas_t = $mysql->eliminarTodasLasMetas();
//JDBD - Eliminamos todas las metricas.
$resEliminarMetricas_t = $mysql->eliminarTodasLasMetricas();


$arrIdEstrat_t = $mysql->IdEstrat();


foreach ($arrIdEstrat_t as $idEstrat) {
	
	$arrPasos_t = $mysql->IdTiposDePaso($idEstrat["id"]);

	foreach ($arrPasos_t as $paso) {
        echo insertarMetas($idEstrat["id"],$paso["id"],$paso["tipo"]);
	}

}

function insertarMetas($intIdEstrat_p,$intIdPaso_p,$intTipoPaso_p){

	global $mysql;
    
    $strInsercion_t = "";
    //JDBD - validamos si las metas son para paso tipo entrante o saliente.
    if ($intTipoPaso_p == 1) {
        //JDBD - Escogemos las metas pertenecientes en este caso a las entrantes.
        $arrMetas_t = ['221','237','238','233','263','261','241','211'];

        for ($i=0; $i < count($arrMetas_t); $i++) { 
            //JDBD - obtenemos el nombre y la configuracion de la meta. 
            $resConfMetas_t = configuracionDeMetas($arrMetas_t[$i]);
            $resMeta_t = metasQueYaExisten($resConfMetas_t[1],$intIdPaso_p);
            //JDBD - validamos si existe la meta por insertar.
            if ($resMeta_t == false) {
                //JDBD - insertamos la meta
                $strInsercion_t .= $mysql->insertarNuevaMeta($resConfMetas_t[0],$intIdEstrat_p,$intIdPaso_p,$resConfMetas_t[1]);
                
            }
        }

    }else{

        $arrMetas_t = ['236','241','232','234','262','233','263','264'];

        for ($i=0; $i < count($arrMetas_t); $i++) { 

            $resConfMetas_t = configuracionDeMetas($arrMetas_t[$i]);
            $resMeta_t = metasQueYaExisten($resConfMetas_t[1],$intIdPaso_p);

            if ($resMeta_t == false) {
                $strInsercion_t .= $mysql->insertarNuevaMeta($resConfMetas_t[0],$intIdEstrat_p,$intIdPaso_p,$resConfMetas_t[1]);
            }

        }

    }

    return $strInsercion_t;

}
function configuracionDeMetas($strTipoMeta_p){
    $arrMetaDefinida_t = [];
    //JDBD - se valida el tipo de meta.
    switch ($strTipoMeta_p) {

            case '232':
                $arrMetaDefinida_t=['#Sin gestion','2,3,2,1'];
                break;
            case '264':
                $arrMetaDefinida_t=['%Sin gestion','2,6,4,1'];
                break;
            case '236':
                $arrMetaDefinida_t=['#Gestiones','2,3,6,1'];
                break;
            case '241':
                $arrMetaDefinida_t=['TMO','2,4,1,1'];
                break;
            case '234':
                $arrMetaDefinida_t=['#Contactados','2,3,4,1'];
                break;
            case '262':
                $arrMetaDefinida_t=['%Contactados','2,6,2,1'];
                break;
            case '238':
                $arrMetaDefinida_t=['#Contestadas','2,3,8,1'];
                break;
            case '261':
                $arrMetaDefinida_t=['%Contestadas','2,6,1,1'];
                break;
            case '233':
                $arrMetaDefinida_t=['#Efectivos','2,3,3,1'];
                break;
            case '263':
                $arrMetaDefinida_t=['%Efectivos','2,6,3,1'];
                break;
            case '221':
                $arrMetaDefinida_t=['#En cola','2,2,1,1'];
                break;
            case '211':
                $arrMetaDefinida_t=['%TSF','2,1,1,1'];
                break;
            case '237':
                $arrMetaDefinida_t=['#Recibidas','2,3,7,1'];
                break;

    }

    return $arrMetaDefinida_t;
}
function metasQueYaExisten($strTipoMeta_p,$intIdPaso_p){
    
    global $mysql;

    $arrMediciones = explode(",", $strTipoMeta_p);

    $arrMetas_t=[];
    $arrMetas_t = $mysql->MetasExistentes($intIdPaso_p,$arrMediciones[0],$arrMediciones[1],$arrMediciones[2],$arrMediciones[3]);

    if (count($arrMetas_t) > 0) {
        $boolMetaExis_t = true;
    }else{
        $boolMetaExis_t = false;
    }
    //JDBD - true = existe, false = no existe para insertar meta. 
    return $boolMetaExis_t;
}
?>