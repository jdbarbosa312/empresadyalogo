<?php

include_once("Json.php");


$arrJsonAgentes_t = json_decode($strJsonAgentes_t);
$arrJsonMetas_t = json_decode($strJsonMetas_t);

$arrTiempoReal_t = [];

// var_dump($arrJsonAgentes_t->objSerializar_t[0]->lstCampanasAsignadas_t);
// echo $arrJsonMetas_t->objSerializar_t[0]->objEstrat_t->ESTRATNombreb;

foreach ($arrJsonMetas_t->objSerializar_t as $keyEstrat => $valueEstrat) {

  $intIdEstrat_t = $valueEstrat->objEstrat_t->ESTRATConsInteb;
  $strNombreEstrat_t = $valueEstrat->objEstrat_t->ESTRATNombreb;
  $strColorEstrat_t = $valueEstrat->objEstrat_t->ESTRATColorb;

  $arrTiempoReal_t[$keyEstrat] = [
    "intIdEstrat_t"     => $intIdEstrat_t,
    "strNombreEstrat_t" => $strNombreEstrat_t,
    "strColorEstrat_t"  => $strColorEstrat_t,
    "arrEstpas_t"       => null
  ];

  $arrPasos_t = [];

  foreach ($valueEstrat->lstPasos_t as $keyPaso => $valuePaso) {

    $intIdEstpas_t = $valuePaso->objEstpas_t->ESTPASConsInteb;
    $intIdCampan_t = $valuePaso->objEstpas_t->ESTPASConsInteCAMPANb;
    $strNombreCampan_t = $valuePaso->objEstpas_t->ESTPASComentarib;
    $intTipoEstpas_t = $valuePaso->objEstpas_t->ESTPASTipob;

    $arrPasos_t[$keyPaso] = [
      "intIdEstpas_t"     => $intIdEstpas_t,
      "intIdCampan_t"     => $intIdCampan_t,
      "strNombreCampan_t" => $strNombreCampan_t,
      "intTipoEstpas_t"   => $intTipoEstpas_t,
      "arrMetas_t"        => null,
      "arrAgentes_t"      => null
    ];


    $arrMetas_t = [];

    foreach ($valuePaso->lstDefinicionMetricas_t as $keyMeta => $valueMeta) {

      $strNombreMeta_t = $valueMeta->METDEFNombreb;
      $intValorMeta = $valueMeta->objMedicion_t->METMEDValorb;

      $arrMetas_t[$keyMeta] = [
        "strNombreMeta_t" => $strNombreMeta_t,
        "intValorMeta"    => $intValorMeta
      ];

    }

    $arrAgentes_t = [];

    foreach ($arrJsonAgentes_t->objSerializar_t as $keyAgente => $valueAgente) {

      if (in_array($intIdCampan_t, $valueAgente->lstCampanasAsignadas_t)) {
        
        $strNombreEstpasActaual_t = $valueAgente->campanaActual;
        $strDuracionEstadoTiempo_t = $valueAgente->duracionEstadoTiempo;
        $strEstado_t = $valueAgente->estado;
        $strFechaHoraCambioEstado = $valueAgente->strFechaHoraCambioEstado_t;
        $strFoto_t = $valueAgente->fotoActual;
        $intIdAgente_t = $valueAgente->idUsuario;
        $strIdentificacionAgente_t = $valueAgente->identificacionUsuario;
        $intIdCampanActual_t = $valueAgente->intIdCampanaActual_t;
        $strNombreAgente_t = $valueAgente->nombreUsuario;
        $strPausa_t = $valueAgente->pausa;

        $arrAgentes_t[$keyAgente] = [
          "strNombreEstpasActaual_t"  => $strNombreEstpasActaual_t,
          "strDuracionEstadoTiempo_t" => $strDuracionEstadoTiempo_t,
          "strEstado_t"               => $strEstado_t,
          "strFechaHoraCambioEstado"  => $strFechaHoraCambioEstado,
          "strFoto_t"                 => $strFoto_t,
          "intIdAgente_t"             => $intIdAgente_t,
          "strIdentificacionAgente_t" => $strIdentificacionAgente_t,
          "intIdCampanActual_t"       => $intIdCampanActual_t,
          "strNombreAgente_t"         => $strNombreAgente_t,
          "strPausa_t"                => $strPausa_t
        ];

      }

    }

    // array_push($arrPasos_t[$keyPaso]["arrMetas_t"] = $arrMetas_t);
    $arrPasos_t[$keyPaso]["arrMetas_t"] = $arrMetas_t;
    // array_push($arrPasos_t[$keyPaso]["arrAgentes_t"] = $arrAgentes_t);
    $arrPasos_t[$keyPaso]["arrAgentes_t"] = $arrAgentes_t;

  }

  // array_push($arrTiempoReal_t[$keyEstrat]["arrEstpas_t"] = $arrPasos_t);
  $arrTiempoReal_t[$keyEstrat]["arrEstpas_t"] = $arrPasos_t;

}

echo json_encode($arrTiempoReal_t);

?>