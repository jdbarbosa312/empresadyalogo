<?php 

$strJsonAgentes_t = '{
  "objSerializar_t": [
    {
      "campanaActual": "SalientePruebaUnoLetrasLargasMasDeVeinte",
      "canalActual": "correo",
      "duracionEstadoTiempo": "6818:10:48",
      "estado": "Ocupado",
      "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
      "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
      "fotoActual": "",
      "id": 10,
      "idComunicacion": "1568055788.0",
      "idEstrategia": 0,
      "idHuesped": 100,
      "idUsuario": 422,
      "identificacionUsuario": "1",
      "intIdCampanaActual_t": 1000,
      "lstCampanasAsignadas_t": [
        2000,
        1000
      ],
      "nombreUsuario": "AgenteUno",
      "pausa": "Break",
      "sentido": "",
      "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
      "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
    },
    {
      "campanaActual": "",
      "canalActual": "correo",
      "duracionEstadoTiempo": "6818:10:48",
      "estado": "Disponible",
      "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
      "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
      "fotoActual": "",
      "id": 10,
      "idComunicacion": "1568055788.0",
      "idEstrategia": 0,
      "idHuesped": 100,
      "idUsuario": 422,
      "identificacionUsuario": "1",
      "intIdCampanaActual_t": null,
      "lstCampanasAsignadas_t": [
        1000,
        2000,
        3000,
        4000
      ],
      "nombreUsuario": "AgenteDos",
      "pausa": "Break",
      "sentido": "",
      "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
      "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
    },
    {
      "campanaActual": "EntrantePruebaCuatroLetrasLargasMasDeVeinte",
      "canalActual": "correo",
      "duracionEstadoTiempo": "6818:10:48",
      "estado": "Ocupado",
      "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
      "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
      "fotoActual": "",
      "id": 10,
      "idComunicacion": "1568055788.0",
      "idEstrategia": 0,
      "idHuesped": 100,
      "idUsuario": 422,
      "identificacionUsuario": "1",
      "intIdCampanaActual_t": 4000,
      "lstCampanasAsignadas_t": [
        1000,
        2000,
        3000,
        4000
      ],
      "nombreUsuario": "AgenteTres",
      "pausa": "Break",
      "sentido": "",
      "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
      "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
    },
    {
      "campanaActual": "",
      "canalActual": "correo",
      "duracionEstadoTiempo": "6818:10:48",
      "estado": "Pausado",
      "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
      "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
      "fotoActual": "",
      "id": 10,
      "idComunicacion": "1568055788.0",
      "idEstrategia": 0,
      "idHuesped": 100,
      "idUsuario": 422,
      "identificacionUsuario": "1",
      "intIdCampanaActual_t": null,
      "lstCampanasAsignadas_t": [
        1000,
        2000,
        3000,
        4000
      ],
      "nombreUsuario": "AgenteCuatro",
      "pausa": "Break",
      "sentido": "",
      "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
      "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
    }
  ],
  "strEstado_t": "ok",
  "strMensaje_t": ""
}';

$strJsonMetas_t = '{
  "objSerializar_t": [
    {
      "lstPasos_t": [
        {
          "lstDefinicionMetricas_t": [
            {
              "METDEFNombreb": "Gestiones",
              "objMedicion_t": {
                "METMEDValorb": "1000"
              }
            },
            {
              "METDEFNombreb": "Sin gestion",
              "objMedicion_t": {
                "METMEDValorb": "1000"
              }
            },
            {
              "METDEFNombreb": "%Contacto",
              "objMedicion_t": {
                "METMEDValorb": "1000"
              }
            },
            {
              "METDEFNombreb": "%Efectivo",
              "objMedicion_t": {
                "METMEDValorb": "1000"
              }
            }
          ],
          "objEstpas_t": {
            "ESTPASComentarib": "SalientePruebaUnoLetrasLargasMasDeVeinte",
            "ESTPASConsInteCAMPANb": 1000,
            "ESTPASConsInteb" : 10000, 
            "ESTPASTipob": 6
          }
        },
        {
          "lstDefinicionMetricas_t": [
            {
              "METDEFNombreb": "Gestiones",
              "objMedicion_t": {
                "METMEDValorb": "2000"
              }
            },
            {
              "METDEFNombreb": "Sin gestion",
              "objMedicion_t": {
                "METMEDValorb": "2000"
              }
            },
            {
              "METDEFNombreb": "%Contacto",
              "objMedicion_t": {
                "METMEDValorb": "2000"
              }
            },
            {
              "METDEFNombreb": "%Efectivo",
              "objMedicion_t": {
                "METMEDValorb": "2000"
              }
            }
          ],
          "objEstpas_t": {
            "ESTPASComentarib": "SalientePruebaDosLetrasLargasMasDeVeinte",
            "ESTPASConsInteCAMPANb": 2000,
            "ESTPASConsInteb" : 20000, 
            "ESTPASTipob": 6
          }
        },
        {
          "lstDefinicionMetricas_t": [
            {
              "METDEFNombreb": "TMO",
              "objMedicion_t": {
                "METMEDValorb": "3000"
              }
            },
            {
              "METDEFNombreb": "Llam. cola",
              "objMedicion_t": {
                "METMEDValorb": "3000"
              }
            },
            {
              "METDEFNombreb": "TSF",
              "objMedicion_t": {
                "METMEDValorb": "3000"
              }
            },
            {
              "METDEFNombreb": "%Contestadas",
              "objMedicion_t": {
                "METMEDValorb": "3000"
              }
            }
          ],
          "objEstpas_t": {
            "ESTPASComentarib": "EntrantePruebaTresLetrasLargasMasDeVeinte",
            "ESTPASConsInteCAMPANb": 3000,
            "ESTPASConsInteb" : 30000, 
            "ESTPASTipob": 6
          }
        },
        {
          "lstDefinicionMetricas_t": [
            {
              "METDEFNombreb": "TMO",
              "objMedicion_t": {
                "METMEDValorb": "4000"
              }
            },
            {
              "METDEFNombreb": "Llam. cola",
              "objMedicion_t": {
                "METMEDValorb": "4000"
              }
            },
            {
              "METDEFNombreb": "TSF",
              "objMedicion_t": {
                "METMEDValorb": "4000"
              }
            },
            {
              "METDEFNombreb": "%Contestadas",
              "objMedicion_t": {
                "METMEDValorb": "4000"
              }
            }
          ],
          "objEstpas_t": {
            "ESTPASComentarib": "EntrantePruebaCuatroLetrasLargasMasDeVeinte",
            "ESTPASConsInteCAMPANb": 4000,
            "ESTPASConsInteb" : 40000, 
            "ESTPASTipob": 6
          }
        }
      ],
      "objEstrat_t": {
        "ESTRATColorb": "#AC3EEB",
        "ESTRATConsIntePROYECb": 100,
        "ESTRATConsInteTIPOESTRATb": 1,
        "ESTRATConsInteb": 1,
        "ESTRATNombreb": "PrimetraEstrategiaDePruebasConLetrasLargas"
      }
    }
  ],
  "strEstado_t": "ok",
  "strMensaje_t": ""
}';


 ?>