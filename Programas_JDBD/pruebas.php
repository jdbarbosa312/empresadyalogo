<?php

	$strDatosJSON_t = '{
		"nombres": "Yesika",
		"apellidos": "Gonzalez",
		"cedula": "213123123",
		"ciudad": "Medell&iacute;n",
		"email": "mail@gmail.com",
		"tipo_telefono": 1,
		"telefono": "3324324232",
		"extension": "prueba",
		"producto": "prueba",
		"optin": 1,
		"glid": "prueba",
		"utm_source": "fuente",
		"utm_campaign": "campana",
		"utm_medium": "medio",
		"queu_promo": "James",
		"referencia_producto": "postpago_Inesperado_3,5GB",
		"canal_trafico": "Referido",
		"resumen_plan": "Info Plan"
		}';

    //Codificamos el arreglo en formato JSON
    // $strDatosJSON_t = json_encode($arrayDatos_p);
    
    //Inicializamos la conexion CURL al web service local para ser consumido
    $objCURL_t = curl_init("http://186.31.64.92/ServiciosWeb/ingresaSolicitud.php?origen=WEBCALL_ORGANICO");
    
    //Asignamos todos los parametros del consumo
    curl_setopt($objCURL_t, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
    curl_setopt($objCURL_t, CURLOPT_POSTFIELDS, $strDatosJSON_t); 
    curl_setopt($objCURL_t,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($objCURL_t, CURLOPT_HTTPHEADER, array(
        'Accept: application/json',
        'Content-Type: application/json')                                                                      
    );

    //Obtenemos la respuesta
    $objRespuestaCURL_t = curl_exec($objCURL_t);

    //Obtenemos el error 
    $objRespuestaError_t = curl_error($objCURL_t);

    //Cerramos la conexion
    curl_close ($objCURL_t);

    print_r($objRespuestaCURL_t);