<?php

class MySQL{

	private $result;
	private $db;
	
	public function __construct(){
        $this->db = new  mysqli("34.73.56.78","dyjdb","dyjdb-DrqRc8rn0E");
        $this->result = array();
	}

	public function correccionFechaGestion($intIdGuion_p,$intIdFecha_p,$intIdHora_p){


		$strSQL = "UPDATE DYALOGOCRM_WEB.G".$intIdGuion_p." SET G".$intIdGuion_p."_C".$intIdFecha_p." = CONCAT(DATE(G".$intIdGuion_p."_C".$intIdFecha_p."),' ',G".$intIdGuion_p."_C".$intIdHora_p.") WHERE LENGTH(G".$intIdGuion_p."_C".$intIdHora_p.") = 8;";

		$this->db->query($strSQL);

		$strSQL = "UPDATE DYALOGOCRM_WEB.G".$intIdGuion_p." SET G".$intIdGuion_p."_C".$intIdFecha_p." = CONCAT(DATE(G".$intIdGuion_p."_C".$intIdFecha_p."),' ',DATE_FORMAT(G".$intIdGuion_p."_C".$intIdHora_p.",'%H:%i:%s')) WHERE LENGTH(G".$intIdGuion_p."_C".$intIdHora_p.") = 19;";

		$this->db->query($strSQL);

		$this->db = NULL;

	}

	public function camposGuion($intIdGuion_p){


		$strSQL = "SELECT PREGUN_ConsInte__b AS idCampo, PREGUN_Texto_____b AS nombreCampo FROM DYALOGOCRM_SISTEMA.PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$intIdGuion_p." AND ((PREGUN_Texto_____b = 'Tipificación' AND PREGUN_Tipo______b = 6) OR (PREGUN_Texto_____b = 'Fecha' AND PREGUN_Tipo______b = 1) OR (PREGUN_Texto_____b = 'Hora' AND PREGUN_Tipo______b = 1) OR (PREGUN_Texto_____b = 'Reintento' AND PREGUN_Tipo______b = 6) OR (PREGUN_Texto_____b = 'Fecha Agenda' AND PREGUN_Tipo______b = 5) OR (PREGUN_Texto_____b = 'Hora Agenda' AND PREGUN_Tipo______b = 10) OR (PREGUN_Texto_____b = 'Observacion' AND PREGUN_Tipo______b = 2)) ORDER BY PREGUN_Tipo______b ASC, PREGUN_Texto_____b ASC;";

		$this->result = array();

		foreach ($this->db->query($strSQL) as $res) {
			$this->result[] = $res;
		}
0.
		return $this->result;

		$this->db = NULL;

	}

	public function paso($intIdEstrat_p){


		$strSQL = "SELECT ESTPAS_ConsInte__b AS idPaso,ESTPAS_Tipo______b AS idTipo, ESTPAS_Nombre__b AS nombreTipo,ESTPAS_Comentari_b AS nombrePaso, (CASE WHEN ESTPAS_Tipo______b > 6 THEN ESTPAS_ConsInte__MUESTR_b ELSE CAMPAN_ConsInte__MUESTR_b END) AS muestra, ESTRAT_ConsInte_GUION_Pob AS base, CAMPAN_ConsInte__GUION__Gui_b AS guion from DYALOGOCRM_SISTEMA.ESTPAS LEFT JOIN DYALOGOCRM_SISTEMA.CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b LEFT JOIN DYALOGOCRM_SISTEMA.ESTRAT ON ESTPAS_ConsInte__ESTRAT_b = ESTRAT_ConsInte__b where ESTPAS_Tipo______b IN (1,6,7,8,9) AND ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_p." ORDER BY ESTPAS_Tipo______b ASC, ESTPAS_ConsInte__b ASC";

		$this->result = array();

		foreach ($this->db->query($strSQL) as $res) {
			$this->result[] = $res;
		}

		return $this->result;

		$this->db = NULL;

	}

	public function actualizacionGMI($intIdEstrat_p){

		$this->result = array();

		foreach ($this->db->query("SELECT ESTPAS_ConsInte__b AS idPaso,ESTPAS_Tipo______b AS idTipo, ESTPAS_Nombre__b AS nombreTipo,ESTPAS_Comentari_b AS nombrePaso, (CASE WHEN ESTPAS_Tipo______b > 6 THEN ESTPAS_ConsInte__MUESTR_b ELSE CAMPAN_ConsInte__MUESTR_b END) AS muestra, ESTRAT_ConsInte_GUION_Pob AS base, CAMPAN_ConsInte__GUION__Gui_b AS guion from DYALOGOCRM_SISTEMA.ESTPAS LEFT JOIN DYALOGOCRM_SISTEMA.CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b LEFT JOIN DYALOGOCRM_SISTEMA.ESTRAT ON ESTPAS_ConsInte__ESTRAT_b = ESTRAT_ConsInte__b where ESTPAS_Tipo______b IN (1,6,7,8,9) AND ESTPAS_ConsInte__ESTRAT_b = ".$intIdEstrat_p." ORDER BY ESTPAS_Tipo______b ASC, ESTPAS_ConsInte__b ASC;") as $res) {
			$this->result[] = $res;
		}

		return $this->result;

		// $this->db = NULL;

	}
}

?>