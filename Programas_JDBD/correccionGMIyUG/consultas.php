<?php

function pasos($paso) {
	
	return "SELECT ESTPAS_ConsInte__b AS idPaso,ESTPAS_Tipo______b AS idTipo, ESTPAS_Nombre__b AS nombreTipo,ESTPAS_Comentari_b AS nombrePaso, (CASE WHEN ESTPAS_Tipo______b > 6 THEN ESTPAS_ConsInte__MUESTR_b ELSE CAMPAN_ConsInte__MUESTR_b END) AS muestra, ESTRAT_ConsInte_GUION_Pob AS base, CAMPAN_ConsInte__GUION__Gui_b AS guion from DYALOGOCRM_SISTEMA.ESTPAS LEFT JOIN DYALOGOCRM_SISTEMA.CAMPAN ON ESTPAS_ConsInte__CAMPAN_b = CAMPAN_ConsInte__b LEFT JOIN DYALOGOCRM_SISTEMA.ESTRAT ON ESTPAS_ConsInte__ESTRAT_b = ESTRAT_ConsInte__b where ESTPAS_Tipo______b IN (1,6) AND ESTPAS_ConsInte__ESTRAT_b = ".$paso." ORDER BY ESTPAS_Tipo______b ASC, ESTPAS_ConsInte__b ASC";

}

function camposGuion($campo) {
	
	return "SELECT PREGUN_ConsInte__b AS idCampo, PREGUN_Texto_____b AS nombreCampo FROM DYALOGOCRM_SISTEMA.PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$campo." AND ((PREGUN_Texto_____b = 'Tipificación' AND PREGUN_Tipo______b = 6) OR (PREGUN_Texto_____b = 'Fecha' AND PREGUN_Tipo______b = 1) OR (PREGUN_Texto_____b = 'Hora' AND PREGUN_Tipo______b = 1) OR (PREGUN_Texto_____b = 'Reintento' AND PREGUN_Tipo______b = 6) OR (PREGUN_Texto_____b = 'Fecha Agenda' AND PREGUN_Tipo______b = 5) OR (PREGUN_Texto_____b = 'Hora Agenda' AND PREGUN_Tipo______b = 10) OR (PREGUN_Texto_____b = 'Observacion' AND PREGUN_Tipo______b = 2)) ORDER BY PREGUN_Tipo______b ASC, PREGUN_Texto_____b ASC;";

}

function correccionFechasGestion1($guion,$fecha,$hora) {
	
	return "UPDATE DYALOGOCRM_WEB.G".$guion." SET G".$guion."_C".$fecha." = CONCAT(DATE(G".$guion."_C".$fecha."),' ',G".$guion."_C".$hora.") WHERE LENGTH(G".$guion."_C".$hora.") = 8;";

}

function correccionFechasGestion2($guion,$fecha,$hora) {
	
	return "UPDATE DYALOGOCRM_WEB.G".$guion." SET G".$guion."_C".$fecha." = CONCAT(DATE(G".$guion."_C".$fecha."),' ',DATE_FORMAT(G".$guion."_C".$hora.",'%H:%i:%s')) WHERE LENGTH(G".$guion."_C".$hora.") = 19;";

}

function GMI($guion,$registro,$paso) {
	
	return "SELECT G".$guion."_ConsInte__b AS id FROM DYALOGOCRM_WEB.G".$guion." WHERE G".$guion."_CodigoMiembro = ".$registro." AND G".$guion."_Paso = ".$paso." ORDER BY G".$guion."_Clasificacion DESC, G".$guion."_ConsInte__b DESC LIMIT 1";

}

?>