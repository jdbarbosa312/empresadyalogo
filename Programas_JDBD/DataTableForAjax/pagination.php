<?php

require_once ("conexion.php");

if (isset($_GET["consultar"])) {

	$intFilas_t = $_GET["filas"]; 

	$strSQL_t = "select * from DYALOGOCRM_WEB.DY_V_H18_T_FA_G1122_TELESALES_W_SCRIPT ORDER BY ID DESC LIMIT ".$intFilas_t;

	$arrData_t = [];

	if ($resSQL_t = $mysqli->query($strSQL_t)) {

		if ($resSQL_t->num_rows > 0) {
			
			while ($row = $resSQL_t->fetch_assoc()) {
				$arrData_t[] = $row;
			}

		}

	}

	$arrColumnas_t = [];

	if (count($arrData_t) > 0) {

		$arrColumnas_t = array_keys($arrData_t[0]);

	}


	$strHTML_t = '';


	$strHTML_t .= '<thead>';	
		$strHTML_t .= '<tr>';	

			foreach ($arrColumnas_t as $kCol => $vCol) {
				$strHTML_t .= '<th>'.$vCol.'</th>';
			}

		$strHTML_t .= '</tr>';	
	$strHTML_t .= '</thead>';	

	$strHTML_t .= '<tbody>';

			foreach ($arrData_t as $kData => $vData) {
				$strHTML_t .= '<tr>';	

					foreach ($arrColumnas_t as $kCOL) {

						$strHTML_t .= '<td>'.$vData[$kCOL].'</td>';

					}

				$strHTML_t .= '</tr>';
			}
	$strHTML_t .= '</tbody>';	

	$strHTML_t .= '<tfoot>';
		$strHTML_t .= '<tr>';

			foreach ($arrColumnas_t as $kF => $vF) {
				$strHTML_t .= '<th>'.$vF.'</th>';
			}

		$strHTML_t .= '</tr>';	
	$strHTML_t .= '</tfoot>';	

	echo $strHTML_t;	


}


?>