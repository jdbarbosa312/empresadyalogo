<!DOCTYPE html>
<html>
<head>
	<title>DataTable</title>
	<link rel="stylesheet" href="assets/plugins/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css"/>
	<script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

</head>
<body>
<button class="form-control btn btn-primary" type="button" id="btnEnvioReport">Reporte</button>
<div class="row">
    <!-- <textarea id="json" style="display:none;"><?php echo $strJsonDatos_t;?></textarea> -->
    <input type="hidden" id="desdeRegistro" value="0">
    <input type="hidden" id="hastaRegistro" value="0">
    <input type="hidden" id="totalRegistros" value="0">

    <div class="col-xs-6">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-header">
	            <div class="btn-group pull-left">
					<div class="dataTables_length" id="grid_length">
						<label>Ver <select id="cantidadFilas" name="cantidadFilas" aria-controls="grid" class="form-control input-sm">
										<option selected value="10">10</option>
										<option value="25">25</option>
										<option value="50">50</option>
										<option value="100">100</option>
									</select> Filas</label>
					</div>
	            </div>
                <div class="btn-group pull-right">
                    <button id="export-errores" type="button" class="btn btn-danger" style="display: none; margin-right: 10px;" >Se presentaron errores, presione aquí para verlos</button>
                    <button id="export-menu" type="button" class="btn btn-info" >Exportar</button>

                </div>
            </div>
            <div class="box-body">
                <table id="TablaReporte" class="table table-bordered table-striped">
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-sm-5"><div class="dataTables_info" id="grid_info" role="status" aria-live="polite">Mostrando del 71 al 80 de 189 Registros</div></div>
</div>
<script type="text/javascript">
	
$("#btnEnvioReport").click(function(){

	$.ajax({

		url : "pagination.php?consultar=si&filas="+$("#cantidadFilas").val(),
		type: "POST",
        dataType : "HTML",
		success : function(data){
			$("#TablaReporte").html(data);
		}

	});

});

</script>
</body>
</html>