<div class="modal fade-in" id="modVisorColaMArcador" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 90%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
            </div>
            <div class="modal-body">
                <iframe id="ifrVisorColaMArcador" src="" style="width: 100%; height: 535px;">
                </iframe>
            </div>
        </div>
    </div>
</div>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

        <div class="row" id="divFiltros">
            <div class="panel box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#s_filtros">
                            Filtros                                                    </a>
                    </h3>
                    <div class="box-tools">
                        
                    </div>
                </div>
                <div id="s_filtros" class="panel-collapse collapse">
                    <input type="hidden" name="arrCamSel_t" id="arrCamSel_t">
                    <div class="box-body" id="divFiltrador">
                        
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group" style="margin-top: 10px; text-align: left;">
                                    <input type="checkbox" name="chColas" id="chColas">
                                    <label for="checkAutomatico">Mostrar colas individuales de agentes.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" style="margin-top: 10px; text-align: right;">
                                    <button type="button" class="btn btn-sm btn-success" id="guardarFiltro">
                                        <i class="fa fa-save"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">

                                <div class="form-group row" id="dragAndDrop">

                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <input type="text" name="buscadorDisponible" id="buscadorDisponible" class="form-control">
                                            <span class="input-group-addon">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                        <p class="text-center titulo-dragdrop">Mostrar Campañas</p>
                                        <ul id="disponible" class="nav nav-pills nav-stacked lista ui-sortable" style="height: 50%;max-height: 400px;
                                                    margin-bottom: 10px;
                                                    overflow: auto;   
                                                    -webkit-overflow-scrolling: touch;border: 1px solid #eaeaea;"></ul>
                                    </div>
                                    <div class="col-md-2 text-center" style="padding-top:100px">
                                        <button type="button" id="derecha" style="width:90px; height:30px; margin-bottom:5px" class="btn btn-info btn-sm">&gt;</button> <br>
                                        <button type="button" id="todoDerecha" style="width:90px; height:30px; margin-bottom:5px" class="btn btn-info btn-sm">&gt;&gt;</button> <br>
                                        
                                        <button type="button" id="izquierda" style="width:90px; height:30px; margin-bottom:5px" class="btn btn-info btn-sm">&lt;</button> <br>
                                        <button type="button" id="todoIzquierda" style="width:90px; height:30px; margin-bottom:5px" class="btn btn-info btn-sm">&lt;&lt;</button>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="input-group">
                                            <input type="text" name="buscadorSeleccionado" id="buscadorSeleccionado" class="form-control">
                                            <span class="input-group-addon">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                        <p class="text-center titulo-dragdrop">Ocultar Campañas</p>
                                        <ul id="seleccionado" class="nav nav-pills nav-stacked lista ui-sortable" style="height: 50%;max-height: 400px;
                                                    margin-bottom: 10px;
                                                    overflow: auto;   
                                                    -webkit-overflow-scrolling: touch;border: 1px solid #eaeaea;"></ul>
                                    </div>
                                    
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="divTiempoReal">



        </div>

    </section>

</div>
<div id="secJava">

</div>
<script type="text/javascript">

$("#guardarFiltro").click(function(){

    var obj = $("#seleccionado li");

    var arrCamSel_t = new Array();

    obj.each(function (key, value){

        arrCamSel_t[key]=$(value).data("id");

    });    

    $.ajax({

        url : 'pages/Dashboard/TiempoRealServicios.php?TIREFI=true',
        type : 'post',
        data : {arrCampTerefi_t : arrCamSel_t},
        success : function(data){

            $("#arrCamSel_t").val(arrCamSel_t.join(','));

            ordenEjecucion(0);

        }


    });

});

ordenEjecucion(0);

function ordenEjecucion(x){

    //JDBD Esta es la funcion que muestra el tiempo real. 

    function tiempoReal(arrCamSel_p){
        
        //JDBD Consumimos los servicios del tiempo real.    
        $.ajax({
            async:  true,
            type: "POST",
            url: "pages/Dashboard/TiempoRealServicios.php?TiempoReal=si",
            data : {strCamSel_t : $("#arrCamSel_t").val()},
            dataType : "JSON",
            success: function(data){

                console.log(data);
                
                //JDBD - Si el API No nos trae informacion lo volvemos a llamar inmediatemente hasta que la traiga.
                if (data.length == 0) {

                    tiempoReal($("#arrCamSel_t").val());
                    
                }


                //JDBD Recorremos las estrategias del JSON.
                    $.each(data,function(kEstrat,vEstrat){

                        $.each(vEstrat.arrEstpas_t,function(kEstpas,vEstpas){

                            var strHTML_t = '';
                            var arrTiempoDuracion_t = new Array();

                            var strHTMLAgen_t = '';

                            var arrScriptAgente_t = new Array();
                            
                            //JDBD Validamos si la campaña ya esta pintada.
                                if ($("#divEstpas_"+vEstpas.intIdEstpas_t).length) {

                                    // console.log("Existe : #divEstpas_"+vEstpas.intIdEstpas_t);

                                    $("#h5Nombre_"+vEstpas.intIdEstpas_t).html('<strong>'+vEstrat.strNombreEstrat_t.substr(0,20)+' | '+vEstpas.strNombreCampan_t.substr(0,20)+' | '+vEstpas.strSentido_t+'</strong>');

                                    $.each(vEstpas.arrEstados_t, function(kEstados,vEstados){

                                        $("#spanEstado_"+kEstados+"_"+vEstpas.intIdEstpas_t).html(vEstados.strEstado_t+"  "+vEstados.intEstado_t);

                                    });  

                                    $.each(vEstpas.arrMetas_t,function(kMetas,vMetas){

                                        if (vMetas.arrMetas_t[1].strMeta_t.indexOf("%") == 0) {
                                            var strOperador_t = '%';
                                        }else{
                                            var strOperador_t = 'seg.';
                                        }

                                        $("#spanMeta1_"+kMetas+"_"+vEstpas.intIdEstpas_t).html('<i class="fa fa-phone"></i>&nbsp;&nbsp;'+vMetas.arrMetas_t[0].strValor_t);

                                        $("#divProgres_"+kMetas+"_"+vEstpas.intIdEstpas_t).width(vMetas.arrMetas_t[1].strValor_t+'%');

                                        $("#spanMeta2_"+kMetas+"_"+vEstpas.intIdEstpas_t).html('&nbsp;&nbsp;'+vMetas.arrMetas_t[1].strMeta_t.replace('%', '')+'&nbsp;&nbsp;<strong>'+vMetas.arrMetas_t[1].strValor_t+'&nbsp;'+strOperador_t+'</strong>');

                                        if (vEstpas.strSentido_t == 'Entrante') {

                                            if (kMetas == 0) {

                                                $("#spanMeta3_"+kMetas+"_"+vEstpas.intIdEstpas_t).html('&nbsp;&nbsp;'+vMetas.arrMetas_t[2].strMeta_t.replace('%', '')+'&nbsp;&nbsp;<strong>'+vMetas.arrMetas_t[2].strValor_t+'&nbsp;%</strong>');
                                                
                                            }else if (kMetas == 1) {

                                                $("#spanMeta3_"+kMetas+"_"+vEstpas.intIdEstpas_t).html('&nbsp;&nbsp;'+vMetas.arrMetas_t[2].strMeta_t.replace('%', '')+'&nbsp;&nbsp;<strong>'+vMetas.arrMetas_t[2].strValor_t+'&nbsp;seg.</strong>');
                                                
                                            }else if (kMetas == 2) {

                                                $("#spanMeta3_"+kMetas+"_"+vEstpas.intIdEstpas_t).html('&nbsp;&nbsp;'+vMetas.arrMetas_t[2].strMeta_t.replace('%', '')+'&nbsp;&nbsp;<strong>'+vMetas.arrMetas_t[2].strValor_t+'&nbsp;seg.</strong>');
                                                
                                            }


                                            
                                        }

                                    });


                                    $.each(vEstpas.arrAgentes_t,function(kAgentes,vAgentes){

                                        strHTMLAgen_t += agente(vEstpas.intIdCampan_t,vAgentes.strNombreEstpasActaual_t,vAgentes.strEstado_t,vAgentes.strFechaHoraCambioEstado_t,vAgentes.intIdAgente_t,vAgentes.intIdCampanActual_t,vAgentes.strNombreAgente_t,vAgentes.strPausa_t,vAgentes.strColorEstado_t,vAgentes.strFechaHoraCambioEstadoFormat_t,vEstpas.strSentido_t,vAgentes.strCanalActual_t);

                                        arrScriptAgente_t.push({"intIdAgenteFoto_t":""+vAgentes.intIdAgente_t+vEstpas.intIdCampan_t,"strFechaHoraCambioEstado_t":vAgentes.strFechaHoraCambioEstado_t});


                                    });

                                    $("#divAgente_"+vEstpas.intIdEstpas_t).html(strHTMLAgen_t);

                                    $.each(arrScriptAgente_t,function(kTiempos,vTiempos){

                                        var strScript_t = ''; 

                                        strScript_t += '<script>';
                                            strScript_t += 'function reloj_'+vTiempos.intIdAgenteFoto_t+'(){';
                                                strScript_t += 'var date1 = new Date();';
                                                strScript_t += 'var date2 = new Date("'+vTiempos.strFechaHoraCambioEstado_t+'");';
                                                strScript_t += 'var diff = (date2 - date1)/1000;';
                                                strScript_t += 'var diff = Math.abs(Math.floor(diff));';
                                                strScript_t += 'var days = Math.floor(diff/(24*60*60));';
                                                strScript_t += 'var leftSec = diff - days * 24*60*60;';
                                                strScript_t += 'var hrs = Math.floor(leftSec/(60*60));';
                                                strScript_t += 'var leftSec = leftSec - hrs * 60*60;';
                                                strScript_t += 'var min = Math.floor(leftSec/(60));';
                                                strScript_t += 'var leftSec = leftSec - min * 60;';
                                                strScript_t += 'hrs = colocarCero_'+vTiempos.intIdAgenteFoto_t+'(hrs);';
                                                strScript_t += 'min = colocarCero_'+vTiempos.intIdAgenteFoto_t+'(min);';
                                                strScript_t += 'leftSec = colocarCero_'+vTiempos.intIdAgenteFoto_t+'(leftSec);';
                                                strScript_t += '$("#reloj_'+vTiempos.intIdAgenteFoto_t+'").html(hrs+":"+min+":"+leftSec);';
                                                strScript_t += '$("#relojTD_'+vTiempos.intIdAgenteFoto_t+'").html(hrs+":"+min+":"+leftSec);';
                                                strScript_t += 'var t = setTimeout(function(){reloj_'+vTiempos.intIdAgenteFoto_t+'();},1000);';
                                                strScript_t += 'function colocarCero_'+vTiempos.intIdAgenteFoto_t+'(i){if(i<10){i="0"+i}return i;}';
                                                strScript_t += '}';
                                                strScript_t += 'reloj_'+vTiempos.intIdAgenteFoto_t+'();';
                                        strScript_t += '<\/script>';    

                                        // console.log(strScript_t);                


                                        $("#secJava").html(strScript_t);


                                    });

                                    var strFunctionVisor_t ='';

                                    strFunctionVisor_t +='<script>';
                                        strFunctionVisor_t +='$(".visorLlamadas").click(function(){';
                                            strFunctionVisor_t +='var strb64IdPaso_t = btoa($(this).attr("idPaso")+"|0");';
                                            strFunctionVisor_t +='$("#ifrVisorColaMArcador").attr("src","ColaMarcador.php?encrypt="+strb64IdPaso_t);';
                                            strFunctionVisor_t +='$("#modVisorColaMArcador").modal();';
                                        strFunctionVisor_t +='});';
                                    strFunctionVisor_t +='<\/script>';

                                    $("#secJava").append(strFunctionVisor_t);

                                }else{
                                    // console.log("NO Existe : #divEstpas_"+vEstpas.intIdEstpas_t);

                                    strHTML_t += '<div id="divEstpas_'+vEstpas.intIdEstpas_t+'" class="col-md-12 col-lg-12 col-xs-12" style="padding: 0px;">';
                                        strHTML_t += '<div class="box box" id="ColorAc'+vEstpas.intIdEstpas_t+'" style="border-top-color: '+vEstrat.strColorEstrat_t+';">';
                                            strHTML_t += '<div class="box-header with-border">';
                                                //JDBD : Aca añadimos el titulo del acordeon.
                                                strHTML_t += '<div class="box-tool pull-left col-md-4" style="text-align:left;">';
                                                    strHTML_t += '<h5 id="h5Nombre_'+vEstpas.intIdEstpas_t+'" class="box-title" style = "font-size:10pt"><strong>'+vEstrat.strNombreEstrat_t.substr(0,20)+' | '+vEstpas.strNombreCampan_t.substr(0,20)+' | '+vEstpas.strSentido_t+'</strong></h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                                strHTML_t += '</div>';

                                                //JDBD : Aca añadimos el boton "Detalle y actividad del marcador".    
                                                if (vEstpas.strSentido_t == "Saliente") {

                                                    strHTML_t += '<div class="box-tool pull-left col-md-3">';
                                                        strHTML_t += '<span id="spanVisor_'+vEstpas.intIdEstpas_t+'" idpaso="'+vEstpas.intIdEstpas_t+'" class="badge visorLlamadas" style="background-color : #009FE3; cursor: pointer; border-radius: 2px; box-shadow: -2pt 2pt 0 rgba(0,0,0,.4);"><i class="fa fa-bar-chart-o"></i> <u>Detalle y actividad del marcador</u></span>';
                                                    strHTML_t += '</div>';
                                                    
                                                }

                                                //JDBD : Aca añadimos los labels de los conteos por estado.
                                                strHTML_t += '<div class="box-tool pull-right col-md-5" style="text-align:right;">';  

                                                    $.each(vEstpas.arrEstados_t, function(kEstados,vEstados){

                                                        strHTML_t += '<span id="spanEstado_'+kEstados+'_'+vEstpas.intIdEstpas_t+'" class="badge" style="background-color : '+vEstados.strColor_t+';">'+vEstados.strEstado_t+'  '+vEstados.intEstado_t+'</span>&nbsp;';


                                                    });  

                                                    strHTML_t += '<button type="button" class="btn btn-box-tool minimizar" acordeon="Ac'+vEstpas.intIdEstpas_t+'" data-widget="collapse"><i class="fa fa-minus" id="Ac'+vEstpas.intIdEstpas_t+'"></i></button>';

                                                strHTML_t += '</div>';
                                                
                                            strHTML_t += '</div>';
                                            strHTML_t += '<div class="box-body no-padding" style="display: block;" id="divAc'+vEstpas.intIdEstpas_t+'">';

                                            //JDBD - Seccion metas.
                                            strHTML_t += '<div class="row">';

                                            var arrColores_t = ["yellow","green","red","aqua"];

                                            var arrGrupo_t = new Array();

                                            if (vEstpas.strSentido_t == "Entrante") {
                                                var arrGrupo_t = ["EN COLA","RECIBIDAS","CONTESTADAS DEL DIA","ABANDONADAS DEL DIA"];
                                            }
                                            if (vEstpas.strSentido_t == "Saliente") {
                                                var arrGrupo_t = ["TOTAL REGISTROS SIN GESTION","GESTIONES DEL DIA","CONTACTADOS DEL DIA","EFECTIVOS DEL DIA"];
                                            }

                                            $.each(vEstpas.arrMetas_t,function(kMetas,vMetas){

                                                if (vMetas.arrMetas_t[1].strMeta_t.indexOf("%") == 0) {
                                                    var strOperador_t = '%';
                                                }else{
                                                    var strOperador_t = 'seg.';
                                                }

                                                strHTML_t += '<div class="col-md-3">';
                                                    strHTML_t += '<div class="info-box" style="background-color:'+vMetas.strColor_t+'; color: #FFFFFF;">';
                                                      strHTML_t += '<span class="info-box-icon"><i class="'+vMetas.strIcono_t+'"></i></span>';
                                                      strHTML_t += '<div class="info-box-content">';
                                                        strHTML_t += '<table width="100%">';
                                                            strHTML_t += '<tbody>';
                                                                strHTML_t += '<tr>';
                                                                    strHTML_t += '<td style="text-align:center;" colspan="4">';
                                                                        strHTML_t += '<span class="info-box-text">'+arrGrupo_t[kMetas]+'</span>';
                                                                    strHTML_t += '</td>';
                                                                strHTML_t += '</tr>';
                                                                strHTML_t += '<tr>';
                                                                    strHTML_t += '<td style="text-align:left;" width="25%">';
                                                                        strHTML_t += '<span id="spanMeta1_'+kMetas+'_'+vEstpas.intIdEstpas_t+'" class="info-box-number"><i class="fa fa-phone"></i>&nbsp;&nbsp;'+vMetas.arrMetas_t[0].strValor_t+'</span>';
                                                                    strHTML_t += '</td>';
                                                                    strHTML_t += '<td style="text-align:center;">';
                                                                    strHTML_t += '</td>';
                                                                    strHTML_t += '<td style="text-align:center;">';
                                                                    strHTML_t += '</td>';
                                                                    strHTML_t += '<td style="text-align:center;">';
                                                                    strHTML_t += '</td>';
                                                                strHTML_t += '</tr>';
                                                            strHTML_t += '</tbody>';
                                                        strHTML_t += '</table>';
                                                        strHTML_t += '<div class="progress">';
                                                          strHTML_t += '<div id="divProgres_'+kMetas+'_'+vEstpas.intIdEstpas_t+'" class="progress-bar" style="width: '+vMetas.arrMetas_t[1].strValor_t+'%">';
                                                          strHTML_t += '</div>';
                                                        strHTML_t += '</div>';


                                                        strHTML_t += '<table width="100%">';
                                                            strHTML_t += '<tbody>';
                                                                strHTML_t += '<tr>';

                                                                    strHTML_t += '<td>';

                                                                        strHTML_t += '<span id="spanMeta2_'+kMetas+'_'+vEstpas.intIdEstpas_t+'" class="progress-description">&nbsp;&nbsp;'+vMetas.arrMetas_t[1].strMeta_t.replace('%', '')+'&nbsp;&nbsp;<strong>'+vMetas.arrMetas_t[1].strValor_t+'&nbsp;'+strOperador_t+'</strong></span>';

                                                                    strHTML_t += '</td>';

                                                                    if (vEstpas.strSentido_t == 'Entrante') {

                                                                        if (kMetas == 0) {

                                                                            strHTML_t += '<td>';

                                                                                strHTML_t += '<span id="spanMeta3_'+kMetas+'_'+vEstpas.intIdEstpas_t+'" class="progress-description">&nbsp;&nbsp;'+vMetas.arrMetas_t[2].strMeta_t.replace('%', '')+'&nbsp;&nbsp;<strong>'+vMetas.arrMetas_t[2].strValor_t+'&nbsp;%</strong></span>';

                                                                            strHTML_t += '</td>';

                                                                        }else if (kMetas == 1) {

                                                                            strHTML_t += '<td>';

                                                                                strHTML_t += '<span id="spanMeta3_'+kMetas+'_'+vEstpas.intIdEstpas_t+'" class="progress-description">&nbsp;&nbsp;'+vMetas.arrMetas_t[2].strMeta_t.replace('%', '')+'&nbsp;&nbsp;<strong>'+vMetas.arrMetas_t[2].strValor_t+'&nbsp;seg.</strong></span>';

                                                                            strHTML_t += '</td>';
                                                                            
                                                                        }else if (kMetas == 2) {

                                                                            strHTML_t += '<td>';

                                                                                strHTML_t += '<span id="spanMeta3_'+kMetas+'_'+vEstpas.intIdEstpas_t+'" class="progress-description">&nbsp;&nbsp;'+vMetas.arrMetas_t[2].strMeta_t.replace('%', '')+'&nbsp;&nbsp;<strong>'+vMetas.arrMetas_t[2].strValor_t+'&nbsp;seg.</strong></span>';

                                                                            strHTML_t += '</td>';
                                                                            
                                                                        }

                                                                        
                                                                    }


                                                                strHTML_t += '</tr>';
                                                            strHTML_t += '</tbody>';
                                                        strHTML_t += '</table>';



                                                      strHTML_t += '</div>';
                                                    strHTML_t += '</div>';
                                                strHTML_t += '</div>';

                                            });

                                            strHTML_t += '</div>';

                                            strHTML_t += '<div class="row" id="divAgente_'+vEstpas.intIdEstpas_t+'">';

                                                $.each(vEstpas.arrAgentes_t,function(kAgentes,vAgentes){

                                                    strHTML_t += agente(vEstpas.intIdCampan_t,vAgentes.strNombreEstpasActaual_t,vAgentes.strEstado_t,vAgentes.strFechaHoraCambioEstado_t,vAgentes.intIdAgente_t,vAgentes.intIdCampanActual_t,vAgentes.strNombreAgente_t,vAgentes.strPausa_t,vAgentes.strColorEstado_t,vAgentes.strFechaHoraCambioEstadoFormat_t,vEstpas.strSentido_t,vAgentes.strCanalActual_t);

                                                    arrTiempoDuracion_t.push({"intIdAgenteFoto_t":""+vAgentes.intIdAgente_t+vEstpas.intIdCampan_t,"strFechaHoraCambioEstado_t":vAgentes.strFechaHoraCambioEstado_t});

                                                });

                                            strHTML_t += '</div>';
                                            
                                        strHTML_t += '</div>';
                                    strHTML_t += '</div>';
                                    
                                    $("#divTiempoReal").append(strHTML_t);

                                    $.each(arrTiempoDuracion_t,function(kTiempos,vTiempos){

                                        var strScript_t = ''; 

                                        strScript_t += '<script>';
                                            strScript_t += 'function reloj_'+vTiempos.intIdAgenteFoto_t+'(){';
                                                strScript_t += 'var date1 = new Date();';
                                                strScript_t += 'var date2 = new Date("'+vTiempos.strFechaHoraCambioEstado_t+'");';
                                                strScript_t += 'var diff = (date2 - date1)/1000;';
                                                strScript_t += 'var diff = Math.abs(Math.floor(diff));';
                                                strScript_t += 'var days = Math.floor(diff/(24*60*60));';
                                                strScript_t += 'var leftSec = diff - days * 24*60*60;';
                                                strScript_t += 'var hrs = Math.floor(leftSec/(60*60));';
                                                strScript_t += 'var leftSec = leftSec - hrs * 60*60;';
                                                strScript_t += 'var min = Math.floor(leftSec/(60));';
                                                strScript_t += 'var leftSec = leftSec - min * 60;';
                                                strScript_t += 'hrs = colocarCero_'+vTiempos.intIdAgenteFoto_t+'(hrs);';
                                                strScript_t += 'min = colocarCero_'+vTiempos.intIdAgenteFoto_t+'(min);';
                                                strScript_t += 'leftSec = colocarCero_'+vTiempos.intIdAgenteFoto_t+'(leftSec);';
                                                strScript_t += '$("#reloj_'+vTiempos.intIdAgenteFoto_t+'").html(hrs+":"+min+":"+leftSec);';
                                                strScript_t += '$("#relojTD_'+vTiempos.intIdAgenteFoto_t+'").html(hrs+":"+min+":"+leftSec);';
                                                strScript_t += 'var t = setTimeout(function(){reloj_'+vTiempos.intIdAgenteFoto_t+'();},1000);';
                                                strScript_t += 'function colocarCero_'+vTiempos.intIdAgenteFoto_t+'(i){if(i<10){i="0"+i}return i;}';
                                                strScript_t += '}';
                                                strScript_t += 'reloj_'+vTiempos.intIdAgenteFoto_t+'();';
                                        strScript_t += '<\/script>';    

                                        // console.log(strScript_t);                


                                        $("#secJava").html(strScript_t);


                                    });

                                    var strFunctionVisor_t = '';

                                    strFunctionVisor_t +='<script>';
                                        strFunctionVisor_t +='$(".visorLlamadas").click(function(){';
                                            strFunctionVisor_t +='var strb64IdPaso_t = btoa($(this).attr("idPaso")+"|0");';
                                            strFunctionVisor_t +='$("#ifrVisorColaMArcador").attr("src","ColaMarcador.php?encrypt="+strb64IdPaso_t);';
                                            strFunctionVisor_t +='$("#modVisorColaMArcador").modal();';
                                        strFunctionVisor_t +='});';
                                    strFunctionVisor_t +='<\/script>';

                                    $("#secJava").append(strFunctionVisor_t);

                                }
                            
                        });


                    });

                $('[data-toggle="popover"]').popover({
                    html : true,
                    placement: 'right'
                });

                // divAcordeon();
            }
        });

    }

    if (x < 1) {

        $.ajax({
            url : 'pages/Dashboard/TiempoRealServicios.php?campanasFiltro=true',
            dataType : 'json',
            success : function(data){
                $('#disponible').html(data.dis);
                $('#seleccionado').html(data.sel);

                var obj = $("#seleccionado li");

                var arrCamSel_t = new Array();

                obj.each(function (key, value){

                    arrCamSel_t[key]=$(value).data("id");

                });   

                $("#arrCamSel_t").val(arrCamSel_t.join(','));

                console.log($("#arrCamSel_t").val());

                $("#disponible").sortable({connectWith:"#seleccionado"});

                $("#disponible, #seleccionado").on('click', 'li', function(){
                    $(this).find(".mi-check").iCheck('toggle');

                    if($(this).find(".mi-check").is(':checked') ){
                        $(this).addClass('seleccionado');
                    }else{
                        $(this).removeClass('seleccionado');
                    }
                    
                });

                $("#disponible, #seleccionado").on('ifToggled', 'input', function(event){
                    if($(this).is(':checked') ){
                        $(this).closest('li').addClass('seleccionado');
                    }else{
                        $(this).closest('li').removeClass('seleccionado');
                    }
                });

                $('#derecha').click(function(){
                    var obj = $("#disponible .seleccionado");
                    $('#seleccionado').append(obj);

                    let arrSeleccionado = [];
                    let arrSeleccionado2 = [];
                    obj.each(function (key, value){
                        arrSeleccionado[key] = $(value).data("id");
                        arrSeleccionado2[key] = $(value).data("camp3");
                    });

                    obj.removeClass('seleccionado');
                    obj.find(".mi-check").iCheck('toggle');
                    
                });

                $('#izquierda').click(function(){
                    var obj = $("#seleccionado .seleccionado");
                    $('#disponible').append(obj);

                    let arrDisponible = [];
                    let arrDisponible2 = [];
                    obj.each(function (key, value){
                        arrDisponible[key] = $(value).data("id");
                        arrDisponible2[key] = $(value).data("camp3");
                    });

                    obj.removeClass('seleccionado');
                    obj.find(".mi-check").iCheck('toggle');
                });
                $('#todoDerecha').click(function(){
                    var obj = $("#disponible li");
                    $('#seleccionado').append(obj);

                    let arrSeleccionado = [];
                    let arrSeleccionado2 = [];
                    obj.each(function (key, value){
                        arrSeleccionado[key] = $(value).data("id");
                        arrSeleccionado2[key] = $(value).data("camp3");
                    });
                    
                    // moverUsuarios(arrSeleccionado, arrSeleccionado2, "derecha");
                });
                $('#todoIzquierda').click(function(){
                    var obj = $("#seleccionado li");
                    $('#disponible').append(obj);

                    let arrDisponible = [];
                    let arrDisponible2 = [];
                    obj.each(function (key, value){
                        arrDisponible[key] = $(value).data("id");
                        arrDisponible2[key] = $(value).data("camp3");
                    });
                    
                    // moverUsuarios(arrDisponible, arrDisponible2, "izquierda");
                });

                $('#buscadorDisponible, #buscadorSeleccionado').keyup(function(){
                    var tipoBuscador = $(this).attr('id');
                    var nombres = '';

                    if(tipoBuscador == 'buscadorDisponible'){
                        nombres = $('ul#disponible .nombre');
                    }else if(tipoBuscador == 'buscadorSeleccionado'){
                        nombres = $('ul#seleccionado .nombre');
                    }

                    var buscando = $(this).val();

                    var item='';

                    for (let i = 0; i < nombres.length; i++) {
                        item = $(nombres[i]).html().toLowerCase();
                        
                        for (let x = 0; x < item.length; x++) {
                            if(item.indexOf(buscando.toLowerCase()) > -1 ){
                                $(nombres[i]).closest('li').show();
                            }else{
                                $(nombres[i]).closest('li').hide();
                                
                            }
                        }
                        
                    }
                }); 


            }
        });

        setTimeout(() => ordenEjecucion(x+1), 5000);

    }else{

        console.log($("#arrCamSel_t").val()+" queee");

        //JDBD - Llamamos la funcion que muestra el tiempo real.
        tiempoReal($("#arrCamSel_t").val());
        //JDBD - Creamos un intervalo para que llame la funcion cada 5 segundos.
        var tiempoReal = setInterval(tiempoReal, 5000);

    }

}

function agente(intIdEstpas_p,strNombreEstpasActaual_p,strEstado_p,strFechaHoraCambioEstado_p,intIdAgente_p,intIdCampanActual_p,strNombreAgente_p,strPausa_p,strColorEstado_p,strFechaHoraCambioEstadoFormat_p,strSentido_p,strCanalActual_p){

    var strHTMLAgente_t = '';

    strHTMLAgente_t += '<div class="col-md-1 col-xs-12" style="text-align:center;padding-bottom:2px;padding:15px">';
      strHTMLAgente_t += '<img src="/DyalogoImagenes/usr'+intIdAgente_p+'.jpg" data-toggle="popover" data-trigger="hover" alt="Dyalogo" data-content="'+modalAgente('/DyalogoImagenes/usr'+intIdAgente_p+'.jpg',intIdAgente_p,intIdEstpas_p,strEstado_p,strColorEstado_p,strFechaHoraCambioEstadoFormat_p,strSentido_p,strNombreEstpasActaual_p,strPausa_p)+'" style="border:solid '+strColorEstado_p+' 4px; width: 60px; height: 60px;" title="" class="imagenuUsuarios img-circle" idusuario="'+intIdAgente_p+'" data-original-title="'+strNombreAgente_p+'">';
      strHTMLAgente_t += '<a class="users-list-name" style="color:'+strColorEstado_p+';" href="#">'+strNombreAgente_p+'</a>';
      strHTMLAgente_t += '<span class="users-list-date" style="color:'+strColorEstado_p+';">'+canalIcono(strSentido_p,strCanalActual_p,strEstado_p);
        strHTMLAgente_t += '&nbsp;&nbsp;<span style="color:'+strColorEstado_p+';" id="reloj_'+intIdAgente_p+intIdEstpas_p+'" style = "font-size:10px">'+strFechaHoraCambioEstado_p+'</span>';
      strHTMLAgente_t += '</span>';
    strHTMLAgente_t += '</div>';

    return strHTMLAgente_t;

}

function modalAgente(strFotoAgente_p,intIdAgente_p,intIdEstpas_p,strEstado_p,strColorEstado_p,strFecha_p,strSentido_p,strNombreEstpasActaual_p,strPausa_p){

    var strHTMLModalAgente_t = "";

    strHTMLModalAgente_t += "<table class='table' style='font-size:12px;' width='300px'>";
        strHTMLModalAgente_t += "<tr>";
            strHTMLModalAgente_t += "<td><img src='"+strFotoAgente_p+"' class='img-circle' style='width:100px; height:100px; border:solid "+strColorEstado_p+" 4px;'></td>";
            strHTMLModalAgente_t += "<td width='50%'>";
                strHTMLModalAgente_t += "<table class='table table-bordered' width='100%'>";
                    strHTMLModalAgente_t += "<tr>";
                        strHTMLModalAgente_t += "<td style='color:"+strColorEstado_p+";'>"+strEstado_p+"</td>";
                    strHTMLModalAgente_t += "</tr>";
                    strHTMLModalAgente_t += "<tr>";
                        strHTMLModalAgente_t += "<td style='color:"+strColorEstado_p+";' id='relojTD_"+intIdAgente_p+intIdEstpas_p+"'>"+strFecha_p+"</td>";
                    strHTMLModalAgente_t += "</tr>";
                    strHTMLModalAgente_t += "<tr>";
                        strHTMLModalAgente_t += "<td style='color:"+strColorEstado_p+";'>"+strSentido_p+"</td>";
                    strHTMLModalAgente_t += "</tr>";
                strHTMLModalAgente_t += "</table>";
            strHTMLModalAgente_t += "</td>";
        strHTMLModalAgente_t += "</tr>";
        strHTMLModalAgente_t += "<tr>";

            if (strEstado_p == "Pausado") {

                strHTMLModalAgente_t += "<th>Tipo de Pausa</th>";
                strHTMLModalAgente_t += "<td style='color:"+strColorEstado_p+"' width='50%'>"+strPausa_p.toUpperCase()+"</td>";

            }else if(strEstado_p.toUpperCase().includes('OCUPADO')){

                strHTMLModalAgente_t += "<th>Campaña Actual</th>";
                strHTMLModalAgente_t += "<td style='color:"+strColorEstado_p+"' width='50%'>"+strNombreEstpasActaual_p.toUpperCase()+"</td>";

            }

        strHTMLModalAgente_t += "</tr>";
    strHTMLModalAgente_t += "</table>";

    return strHTMLModalAgente_t; 

}

function canalIcono(strSentido_p,strCanalActual_p,strEstado_p){

    $canal = '<i class="fa fa-phone"></i>&nbsp;<i class="fa fa-arrow-right"></i>';

    if (strEstado_p.toUpperCase().includes('OCUPADO')) {

        switch (strCanalActual_p) {
            case 'voip':

                if(strSentido_p == 'Entrante'){
                    $canal = '<i class="fa fa-arrow-right" style = "font-size:10px"></i>&nbsp;<i class="fa fa-phone" style = "font-size:10px"></i>';
                }else{
                    $canal = '<i class="fa fa-phone" style = "font-size:10px"></i>&nbsp;<i class="fa fa-arrow-right" style = "font-size:10px"></i>';
                    
                }

                
                break;
            case 'voz':

                if(strSentido_p == 'Entrante'){
                    $canal = '<i class="fa fa-arrow-right" style = "font-size:10px"></i>&nbsp;<i class="fa fa-phone" style = "font-size:10px"></i>';
                }else{
                    $canal = '<i class="fa fa-phone" style = "font-size:10px"></i>&nbsp;<i class="fa fa-arrow-right" style = "font-size:10px"></i>';
                    
                }

                
                break;

            case 'correo':

                if(strSentido_p == 'Entrante'){
                    $canal = '<i class="fa fa-arrow-right"></i>&nbsp;<i class="fa fa-envelope-o"></i>';
                }else{
                    $canal = '<i class="fa fa-envelope-o"></i>&nbsp;<i class="fa fa-arrow-right"></i>';
                }

                
                break;

            case 'chat':

                if(strSentido_p == 'Entrante'){
                    $canal = '<i class="fa fa-arrow-right"></i>&nbsp;<i class="fa fa-comment-o"></i>';
                }else{
                    $canal = '<i class="fa fa-comment-o"></i>&nbsp;<i class="fa fa-arrow-right"></i>';
                }

                
                break;
                
        }
    }else{

        $canal = '';

    }


    return $canal;

}

</script>