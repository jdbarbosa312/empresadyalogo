<?php
  session_start();
  require_once('../conexion.php');

  $intIdUsuario_t = $_SESSION['IDENTIFICACION'];

  if (isset($_GET["TIREFI"])) {

    $arrCampTerefi_t = $_POST["arrCampTerefi_t"];

    $strSQLDeleteTerefi_t = "DELETE FROM ".$BaseDatos_systema.".TIREFI WHERE TIREFI_ConsInte_USUARI_b = ".$intIdUsuario_t;

    $mysqli->query($strSQLDeleteTerefi_t);

    $strSQLInsertTerefi_t = "INSERT INTO ".$BaseDatos_systema.".TIREFI (TIREFI_ConsInte_CAMPAN_b,TIREFI_ConsInte_USUARI_b) VALUES ";

    foreach ($arrCampTerefi_t as $key => $value) {

      $strSQLInsertTerefi_t .= "(".$value.",".$intIdUsuario_t."),";

    }

    $strSQLInsertTerefi_t = substr($strSQLInsertTerefi_t, 0, -1);

    $mysqli->query($strSQLInsertTerefi_t);

  }

  if (isset($_GET["campanasFiltro"])) {

    $strSQLCampanasFiltroDis_t = "SELECT CAMPAN_Nombre____b,CAMPAN_ConsInte__b FROM ".$BaseDatos_systema.".USUARI JOIN ".$BaseDatos_general.".huespedes_usuarios ON USUARI_UsuaCBX___b = id_usuario JOIN ".$BaseDatos_systema.".CAMPAN ON id_huesped = CAMPAN_ConsInte__PROYEC_b JOIN ".$BaseDatos_systema.".ESTPAS ON CAMPAN_ConsInte__b = ESTPAS_ConsInte__CAMPAN_b LEFT JOIN ".$BaseDatos_systema.".TIREFI ON CAMPAN_ConsInte__b = TIREFI_ConsInte_CAMPAN_b WHERE USUARI_ConsInte__b = ".$intIdUsuario_t." AND TIREFI_ConsInte_CAMPAN_b IS NULL GROUP BY CAMPAN_ConsInte__b";

    $resSQLCampanasFiltroDis_t = $mysqli->query($strSQLCampanasFiltroDis_t);

    $strHTMLCampanasDis_t = '';

    while ($item = $resSQLCampanasFiltroDis_t->fetch_object()) {

        $strHTMLCampanasDis_t .= '<li data-id="'.$item->CAMPAN_ConsInte__b.'"><table class="table table-hover"><tr><td width="40px"><input type="checkbox" class="flat-red mi-check"></td><td class="nombre">'.$item->CAMPAN_Nombre____b.'</td></tr></table></li>';

    }

    $strSQLCampanasFiltroSel_t = "SELECT CAMPAN_Nombre____b,CAMPAN_ConsInte__b FROM ".$BaseDatos_systema.".USUARI JOIN ".$BaseDatos_general.".huespedes_usuarios ON USUARI_UsuaCBX___b = id_usuario JOIN ".$BaseDatos_systema.".CAMPAN ON id_huesped = CAMPAN_ConsInte__PROYEC_b JOIN ".$BaseDatos_systema.".ESTPAS ON CAMPAN_ConsInte__b = ESTPAS_ConsInte__CAMPAN_b LEFT JOIN ".$BaseDatos_systema.".TIREFI ON CAMPAN_ConsInte__b = TIREFI_ConsInte_CAMPAN_b WHERE USUARI_ConsInte__b = ".$intIdUsuario_t." AND TIREFI_ConsInte_CAMPAN_b IS NOT NULL GROUP BY CAMPAN_ConsInte__b";

    $resSQLCampanasFiltroSel_t = $mysqli->query($strSQLCampanasFiltroSel_t);

    $strHTMLCampanasSel_t = '';

    while ($item = $resSQLCampanasFiltroSel_t->fetch_object()) {

        $strHTMLCampanasSel_t .= '<li data-id="'.$item->CAMPAN_ConsInte__b.'"><table class="table table-hover"><tr><td width="40px"><input type="checkbox" class="flat-red mi-check"></td><td class="nombre">'.$item->CAMPAN_Nombre____b.'</td></tr></table></li>';

    }

    echo json_encode(["dis"=>$strHTMLCampanasDis_t,"sel"=>$strHTMLCampanasSel_t]);

  }

  if (isset($_GET["TiempoReal"])) {
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    require_once('../conexion.php');
    include("../../global/WSCoreClient.php");
    include("../../global/funcionesGenerales.php");
    date_default_timezone_set('America/Bogota');

    $arrJsonAgentes_t = '{
        "objSerializar_t": [
          {
            "campanaActual": "Saliente Dos",
            "canalActual": "correo",
            "duracionEstadoTiempo": "6818:10:48",
            "estado": "Ocupado",
            "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
            "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
            "fotoActual": "",
            "id": 96,
            "idComunicacion": "1568055788.0",
            "idEstrategia": 2,
            "idHuesped": 100,
            "idUsuario": 503,
            "identificacionUsuario": "96",
            "intIdCampanaActual_t": 1000,
            "lstCampanasAsignadas_t": [
              1000,
              4001
            ],
            "nombreUsuario":"Agente98Agente98Agente98",
            "pausa": "Break",
            "sentido": "",
            "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
            "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
          },
          {
            "campanaActual": "Saliente Dos",
            "canalActual": "chat",
            "duracionEstadoTiempo": "6818:10:48",
            "estado": "Ocupado",
            "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
            "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
            "fotoActual": "",
            "id": 97,
            "idComunicacion": "1568055788.0",
            "idEstrategia": 2,
            "idHuesped": 100,
            "idUsuario": 502,
            "identificacionUsuario": "97",
            "intIdCampanaActual_t": 4001,
            "lstCampanasAsignadas_t": [
              1000,
              4001
            ],
            "nombreUsuario": "Agente99Agente99Agente99",
            "pausa": "Break",
            "sentido": "",
            "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
            "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
          },
          {
            "campanaActual": "Saliente Dos",
            "canalActual": "correo",
            "duracionEstadoTiempo": "6818:10:48",
            "estado": "",
            "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
            "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
            "fotoActual": "",
            "id": 98,
            "idComunicacion": "1568055788.0",
            "idEstrategia": 2,
            "idHuesped": 100,
            "idUsuario": 501,
            "identificacionUsuario": "98",
            "lstCampanasAsignadas_t": [
              4001
            ],
            "nombreUsuario": "Agente100Agente100Agente100",
            "pausa": "Break",
            "sentido": "",
            "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
            "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
          },
          {
            "campanaActual": "Saliente Dos",
            "canalActual": "correo",
            "duracionEstadoTiempo": "6818:10:48",
            "estado": "Ocupado",
            "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
            "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
            "fotoActual": "",
            "id": 99,
            "idComunicacion": "1568055788.0",
            "idEstrategia": 2,
            "idHuesped": 100,
            "idUsuario": 500,
            "identificacionUsuario": "101",
            "intIdCampanaActual_t": 4001,
            "lstCampanasAsignadas_t": [
              4000,
              4001
            ],
            "nombreUsuario": "Agente102Agente102Agente102",
            "pausa": "Break",
            "sentido": "",
            "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
            "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
          },
          {
            "campanaActual": "Saliente Prueba Uno Letras LargasMas DeVeinte",
            "canalActual": "voip",
            "duracionEstadoTiempo": "6818:10:48",
            "estado": "Ocupado",
            "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
            "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
            "fotoActual": "",
            "id": 10,
            "idComunicacion": "1568055788.0",
            "idEstrategia": 0,
            "idHuesped": 100,
            "idUsuario": 418,
            "identificacionUsuario": "1",
            "intIdCampanaActual_t": 1000,
            "lstCampanasAsignadas_t": [
              2000,
              1000
            ],
            "nombreUsuario": "AgenteUnoAgenteUnoAgenteUno",
            "pausa": "Break",
            "sentido": "",
            "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
            "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
          },
          {
            "campanaActual": "",
            "canalActual": "voip",
            "duracionEstadoTiempo": "6818:10:48",
            "estado": "Disponible",
            "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
            "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
            "fotoActual": "",
            "id": 10,
            "idComunicacion": "1568055788.0",
            "idEstrategia": 0,
            "idHuesped": 100,
            "idUsuario": 419,
            "identificacionUsuario": "1",
            "lstCampanasAsignadas_t": [
              1000,
              2000,
              3000,
              4000
            ],
            "nombreUsuario": "AgenteDosAgenteDosAgenteDos",
            "pausa": "Break",
            "sentido": "",
            "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
            "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
          },
          {
            "campanaActual": "Entrante Prueba Cuatro Letras Largas Mas De Veinte",
            "canalActual": "voip",
            "duracionEstadoTiempo": "6818:10:48",
            "estado": "Ocupado",
            "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
            "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
            "fotoActual": "",
            "id": 10,
            "idComunicacion": "1568055788.0",
            "idEstrategia": 0,
            "idHuesped": 100,
            "idUsuario": 420,
            "identificacionUsuario": "1",
            "intIdCampanaActual_t": 4000,
            "lstCampanasAsignadas_t": [
              1000,
              2000,
              3000,
              4000
            ],
            "nombreUsuario": "AgenteTresAgenteTresAgenteTres",
            "pausa": "Break",
            "sentido": "",
            "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
            "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
          },
          {
            "campanaActual": "",
            "canalActual": "voip",
            "duracionEstadoTiempo": "6818:10:48",
            "estado": "Pausado",
            "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
            "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
            "fotoActual": "",
            "id": 10,
            "idComunicacion": "1568055788.0",
            "idEstrategia": 0,
            "idHuesped": 100,
            "idUsuario": 426,
            "identificacionUsuario": "1",
            "lstCampanasAsignadas_t": [
              1000,
              2000,
              3000,
              4000
            ],
            "nombreUsuario": "AgenteCuatroAgenteCuatroAgenteCuatro",
            "pausa": "Break",
            "sentido": "",
            "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
            "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
          },
          {
            "campanaActual": "Saliente 2 Prueba Uno Letras LargasMas DeVeinte",
            "canalActual": "voip",
            "duracionEstadoTiempo": "6818:10:48",
            "estado": "Ocupado",
            "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
            "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
            "fotoActual": "",
            "id": 10,
            "idComunicacion": "1568055788.0",
            "idEstrategia": 0,
            "idHuesped": 100,
            "idUsuario": 427,
            "identificacionUsuario": "1",
            "intIdCampanaActual_t": 1111,
            "lstCampanasAsignadas_t": [
              2222,
              1111
            ],
            "nombreUsuario": "AgenteUnoAgenteUnoAgenteUno",
            "pausa": "Break",
            "sentido": "",
            "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
            "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
          },
          {
            "campanaActual": "",
            "canalActual": "voip",
            "duracionEstadoTiempo": "6818:10:48",
            "estado": "Disponible",
            "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
            "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
            "fotoActual": "",
            "id": 10,
            "idComunicacion": "1568055788.0",
            "idEstrategia": 0,
            "idHuesped": 100,
            "idUsuario": 433,
            "identificacionUsuario": "1",
            "lstCampanasAsignadas_t": [
              1111,
              2222,
              3333,
              4444
            ],
            "nombreUsuario": "AgenteDosAgenteDosAgenteDos",
            "pausa": "Break",
            "sentido": "",
            "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
            "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
          },
          {
            "campanaActual": "Entrante Prueba Cuatro Letras Largas Mas De Veinte",
            "canalActual": "voip",
            "duracionEstadoTiempo": "6818:10:48",
            "estado": "Ocupado",
            "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
            "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
            "fotoActual": "",
            "id": 10,
            "idComunicacion": "1568055788.0",
            "idEstrategia": 0,
            "idHuesped": 100,
            "idUsuario": 429,
            "identificacionUsuario": "1",
            "intIdCampanaActual_t": 4444,
            "lstCampanasAsignadas_t": [
              1111,
              2222,
              3333,
              4444
            ],
            "nombreUsuario": "AgenteNueveAgenteNueveAgenteNueve",
            "pausa": "Break",
            "sentido": "",
            "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
            "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
          },
          {
            "campanaActual": "",
            "canalActual": "voip",
            "duracionEstadoTiempo": "6818:10:48",
            "estado": "Pausado",
            "fechaHoraCambioEstado": "2019-09-09T19:03:20Z[UTC]",
            "fechaHoraInicioGestion": "2019-09-09T19:03:20Z[UTC]",
            "fotoActual": "",
            "id": 10,
            "idComunicacion": "1568055788.0",
            "idEstrategia": 0,
            "idHuesped": 100,
            "idUsuario": 431,
            "identificacionUsuario": "1",
            "lstCampanasAsignadas_t": [
              1111,
              2222,
              3333,
              4444
            ],
            "nombreUsuario": "AgenteDiezAgenteDiezAgenteDiez",
            "pausa": "Break",
            "sentido": "",
            "strFechaHoraCambioEstado_t": "2019-09-09 14:03:20",
            "strFechaHoraInicioGestion_t": "2019-09-09 14:03:20"
          }
        ],
        "strEstado_t": "ok",
        "strMensaje_t": ""
      }';
      $arrJsonMetas_t   = '{
          "objSerializar_t": [
            {
              "lstPasos_t": [
                {
                  "lstDefinicionMetricas_t": [
                    {
                      "METDEFNombreb": "#Gestiones",
                      "objMedicion_t": {
                        "METMEDValorb": "65"
                      }
                    },
                    {
                      "METDEFNombreb": "TMO",
                      "objMedicion_t": {
                        "METMEDValorb": "350"
                      }
                    },
                    {
                      "METDEFNombreb": "#Sin gestion",
                      "objMedicion_t": {
                        "METMEDValorb": "751"
                      }
                    },
                    {
                      "METDEFNombreb": "#Contactados",
                      "objMedicion_t": {
                        "METMEDValorb": "51"
                      }
                    },
                    {
                      "METDEFNombreb": "%Contactados",
                      "objMedicion_t": {
                        "METMEDValorb": "75"
                      }
                    },
                    {
                      "METDEFNombreb": "#Efectivos",
                      "objMedicion_t": {
                        "METMEDValorb": "20"
                      }
                    },
                    {
                      "METDEFNombreb": "%Efectivos",
                      "objMedicion_t": {
                        "METMEDValorb": "27"
                      }
                    },
                    {
                      "METDEFNombreb": "%Sin gestion",
                      "objMedicion_t": {
                        "METMEDValorb": "75"
                      }
                    }
                  ],
                  "objEstpas_t": {
                    "ESTPASComentarib": "Saliente Prueba Uno Letras LargasMas DeVeinte",
                    "ESTPASConsInteCAMPANb": 1000,
                    "ESTPASConsInteb" : 224, 
                    "ESTPASTipob": 6
                  }
                },
                {
                  "lstDefinicionMetricas_t": [
                    {
                      "METDEFNombreb": "#En cola",
                      "objMedicion_t": {
                        "METMEDValorb": "1"
                      }
                    },
                    {
                      "METDEFNombreb": "#Recibidas",
                      "objMedicion_t": {
                        "METMEDValorb": "31"
                      }
                    },
                    {
                      "METDEFNombreb": "#Contestadas",
                      "objMedicion_t": {
                        "METMEDValorb": "25"
                      }
                    },
                    {
                      "METDEFNombreb": "#Abandonadas",
                      "objMedicion_t": {
                        "METMEDValorb": "10"
                      }
                    },
                    {
                      "METDEFNombreb": "%Abandonadas",
                      "objMedicion_t": {
                        "METMEDValorb": "24"
                      }
                    },
                    {
                      "METDEFNombreb": "AWT",
                      "objMedicion_t": {
                        "METMEDValorb": "151"
                      }
                    },
                    {
                      "METDEFNombreb": "ASA",
                      "objMedicion_t": {
                        "METMEDValorb": "131"
                      }
                    },
                    {
                      "METDEFNombreb": "OCUP",
                      "objMedicion_t": {
                        "METMEDValorb": "161"
                      }
                    },
                    {
                      "METDEFNombreb": "%Contestadas",
                      "objMedicion_t": {
                        "METMEDValorb": "40"
                      }
                    },
                    {
                      "METDEFNombreb": "TMO",
                      "objMedicion_t": {
                        "METMEDValorb": "367"
                      }
                    },
                    {
                      "METDEFNombreb": "%TSF",
                      "objMedicion_t": {
                        "METMEDValorb": "85"
                      }
                    }
                  ],
                  "objEstpas_t": {
                    "ESTPASComentarib": "Entrante Prueba",
                    "ESTPASConsInteCAMPANb": 409,
                    "ESTPASConsInteb" : 40000, 
                    "ESTPASTipob": 1
                  }
                }
              ],
              "objEstrat_t": {
                "ESTRATColorb": "#299E00",
                "ESTRATConsIntePROYECb": 100,
                "ESTRATConsInteTIPOESTRATb": 1,
                "ESTRATConsInteb": 1,
                "ESTRATNombreb": "Primera Estrategia De Pruebas Con Letras Largas"
              }
            },
            {
              "lstPasos_t": [
                {
                  "lstDefinicionMetricas_t": [
                    {
                      "METDEFNombreb": "#En cola",
                      "objMedicion_t": {
                        "METMEDValorb": "3"
                      }
                    },
                    {
                      "METDEFNombreb": "#Recibidas",
                      "objMedicion_t": {
                        "METMEDValorb": "51"
                      }
                    },
                    {
                      "METDEFNombreb": "#Contestadas",
                      "objMedicion_t": {
                        "METMEDValorb": "24"
                      }
                    },
                    {
                      "METDEFNombreb": "#Efectivos",
                      "objMedicion_t": {
                        "METMEDValorb": "15"
                      }
                    },
                    {
                      "METDEFNombreb": "%Efectivos",
                      "objMedicion_t": {
                        "METMEDValorb": "10"
                      }
                    },
                    {
                      "METDEFNombreb": "%Contestadas",
                      "objMedicion_t": {
                        "METMEDValorb": "20"
                      }
                    },
                    {
                      "METDEFNombreb": "TMO",
                      "objMedicion_t": {
                        "METMEDValorb": "350.49845648654"
                      }
                    },
                    {
                      "METDEFNombreb": "%TSF",
                      "objMedicion_t": {
                        "METMEDValorb": "35"
                      }
                    }
                  ],
                  "objEstpas_t": {
                    "ESTPASComentarib": "Saliente Dos",
                    "ESTPASConsInteCAMPANb": 220,
                    "ESTPASConsInteb" : 40001, 
                    "ESTPASTipob": 1
                  }
                }
              ],
              "objEstrat_t": {
                "ESTRATColorb": "#9E1300",
                "ESTRATConsIntePROYECb": 100,
                "ESTRATConsInteTIPOESTRATb": 1,
                "ESTRATConsInteb": 2,
                "ESTRATNombreb": "Estrategia 2 De Pruebas Con Letras Largas"
              }
            }
          ],
          "strEstado_t": "ok",
          "strMensaje_t": ""
        }';

    // $arrJsonAgentes_t = json_decode(listaAgentesTiempoReal($intIdUsuario_t));
    // $arrJsonMetas_t   = json_decode(metricasTiempoReal($intIdUsuario_t));

    $arrCamSel_t = explode(",", $_POST["strCamSel_t"]);

    $arrJsonAgentes_t = json_decode($arrJsonAgentes_t);
    $arrJsonMetas_t   = json_decode($arrJsonMetas_t);

    $arrTiempoReal_t = [];

    $intAvtividad_t = count($arrJsonMetas_t->objSerializar_t);

    if ($intAvtividad_t > 0) {

      foreach ($arrJsonMetas_t->objSerializar_t as $keyEstrat => $valueEstrat) {

        $intIdEstrat_t = $valueEstrat->objEstrat_t->ESTRATConsInteb;
        $strNombreEstrat_t = $valueEstrat->objEstrat_t->ESTRATNombreb;
        $strColorEstrat_t = $valueEstrat->objEstrat_t->ESTRATColorb;

        $arrTiempoReal_t[$keyEstrat] = [
          "intIdEstrat_t"     => $intIdEstrat_t,
          "strNombreEstrat_t" => $strNombreEstrat_t,
          "strColorEstrat_t"  => $strColorEstrat_t,
          "arrEstpas_t"       => null
        ];

        $arrPasos_t = [];

        foreach ($valueEstrat->lstPasos_t as $keyPaso => $valuePaso) {

          if (!in_array($valuePaso->objEstpas_t->ESTPASConsInteCAMPANb, $arrCamSel_t)) {

            $intNoDisponibles_t = 0;
            $intPausados_t = 0;
            $intOcupados_t = 0;
            $intOcupados2_t = 0;
            $intDisponibles_t = 0;

            $intIdEstpas_t = $valuePaso->objEstpas_t->ESTPASConsInteb;
            $intIdCampan_t = $valuePaso->objEstpas_t->ESTPASConsInteCAMPANb;
            $strNombreCampan_t = $valuePaso->objEstpas_t->ESTPASComentarib;
            $intTipoEstpas_t = $valuePaso->objEstpas_t->ESTPASTipob;

            $arrPasos_t[$keyPaso] = [
              "intIdEstpas_t"     => $intIdEstpas_t,
              "intIdCampan_t"     => $intIdCampan_t,
              "strNombreCampan_t" => $strNombreCampan_t,
              "strSentido_t"      => sentido($intTipoEstpas_t),
              "arrMetas_t"        => null,
              "arrAgentes_t"      => null
            ];

            $arrOrdenMetas_t = [];
            $arrMetas_t = [];

            if ($intTipoEstpas_t == 6) {

              $arrOrdenMetas_t = [0=>["metas"=>["#Sin gestion","%Sin gestion"],
                                      "color"=>"#F37D00","icono"=>"fa fa-users"],
                                  1=>["metas"=>["#Gestiones","TMO"],
                                      "color"=>"#F3BC00","icono"=>"fa fa-battery-three-quarters"],
                                  2=>["metas"=>["#Contactados","%Contactados"],
                                      "color"=>"#299E00","icono"=>"fa fa-hourglass-2"],
                                  3=>["metas"=>["#Efectivos","%Efectivos"],
                                      "color"=>"#009FE3","icono"=>"fa fa-thumbs-o-up"]];

              foreach ($arrOrdenMetas_t as $keyGrup0 => $valueGrup0) {

                foreach ($valueGrup0["metas"] as $keyM0 => $valueM0) {

                  $intIter0_t = 0;

                  foreach ($valuePaso->lstDefinicionMetricas_t as $keyMeta0 => $valueMeta0) {

                    if ($valueM0 == $valueMeta0->METDEFNombreb) {
                      $intIter0_t ++;
                    }


                  }

                  if ($intIter0_t == 0) {

                    array_push($valuePaso->lstDefinicionMetricas_t, (object)["METDEFNombreb"=>$valueM0,"objMedicion_t"=>(object)["METMEDValorb"=>"N/A"]]);

                  }

                }

              }

              foreach ($arrOrdenMetas_t as $keyGrup => $valueGrup) {
                
                  $arrMetas_t[$keyGrup]["strColor_t"] = $valueGrup["color"];
                  $arrMetas_t[$keyGrup]["strIcono_t"] = $valueGrup["icono"];

                foreach ($valueGrup["metas"] as $keyM => $valueM) {

                  foreach ($valuePaso->lstDefinicionMetricas_t as $keyMeta => $valueMeta) {

                    if ($valueM == $valueMeta->METDEFNombreb) {

                      $intValor_t = "N/A";

                      if ($valueMeta->objMedicion_t->METMEDValorb != "N/A") {
                        $intValor_t = ceil($valueMeta->objMedicion_t->METMEDValorb);
                      }

                      $arrMetas_t[$keyGrup]["arrMetas_t"][$keyM] = ["strMeta_t"=>$valueM,"strValor_t"=>$intValor_t];

                    }


                  }
                  
                  
                }


              }


            }elseif ($intTipoEstpas_t == 1) {

              $arrOrdenMetas_t = [0=>["metas"=>["#En cola","%TSF","OCUP"],
                                      "color"=>"#F37D00","icono"=>"fa fa-users"],
                                  1=>["metas"=>["#Recibidas","TMO","AWT"],
                                      "color"=>"#F3BC00","icono"=>"fa fa-battery-three-quarters"],
                                  2=>["metas"=>["#Contestadas","%Contestadas","ASA"],
                                      "color"=>"#299E00","icono"=>"fa fa-hourglass-2"],
                                  3=>["metas"=>["#Abandonadas","%Abandonadas"],
                                      "color"=>"#E55D40","icono"=>"fa fa-thumbs-o-down"]];

              foreach ($arrOrdenMetas_t as $keyGrup0 => $valueGrup0) {

                foreach ($valueGrup0["metas"] as $keyM0 => $valueM0) {

                  $intIter0_t = 0;

                  foreach ($valuePaso->lstDefinicionMetricas_t as $keyMeta0 => $valueMeta0) {

                    if ($valueM0 == $valueMeta0->METDEFNombreb) {
                      $intIter0_t ++;
                    }


                  }

                  if ($intIter0_t == 0) {

                    array_push($valuePaso->lstDefinicionMetricas_t, (object)["METDEFNombreb"=>$valueM0,"objMedicion_t"=>(object)["METMEDValorb"=>"N/A"]]);

                  }

                }

              }

              foreach ($arrOrdenMetas_t as $keyGrup => $valueGrup) {
                
                  $arrMetas_t[$keyGrup]["strColor_t"] = $valueGrup["color"];
                  $arrMetas_t[$keyGrup]["strIcono_t"] = $valueGrup["icono"];

                foreach ($valueGrup["metas"] as $keyM => $valueM) {

                  foreach ($valuePaso->lstDefinicionMetricas_t as $keyMeta => $valueMeta) {

                    if ($valueM == $valueMeta->METDEFNombreb) {

                      $intValor_t = "N/A";

                      if ($valueMeta->objMedicion_t->METMEDValorb != "N/A") {
                        $intValor_t = ceil($valueMeta->objMedicion_t->METMEDValorb);
                      }

                      $arrMetas_t[$keyGrup]["arrMetas_t"][$keyM] = ["strMeta_t"=>$valueM,"strValor_t"=>$intValor_t];

                    }


                  }
                  
                  
                }


              }

            }

            $arrAgentes_t = [];

            foreach ($arrJsonAgentes_t->objSerializar_t as $keyAgente => $valueAgente) {

              if (property_exists($valueAgente,"intIdCampanaActual_t") == false) {

                $valueAgente->intIdCampanaActual_t = null;

              }

              if (in_array($intIdCampan_t, $valueAgente->lstCampanasAsignadas_t) || $intIdCampan_t == $valueAgente->intIdCampanaActual_t) {
                
                $strNombreEstpasActaual_t = $valueAgente->campanaActual;
                $strCanalActual_t = $valueAgente->canalActual;
                $strDuracionEstadoTiempo_t = $valueAgente->duracionEstadoTiempo;
                $strEstado_t = $valueAgente->estado;

                $strFecha1_t = new DateTime($valueAgente->strFechaHoraCambioEstado_t);
                $strFecha2_t = new DateTime("now");
                $strDiffFecha_t = $strFecha1_t->diff($strFecha2_t);
                $strFecha_t = $strDiffFecha_t->format("%H:%I:%S");
                $strFechaHoraCambioEstadoFormat_t = $strFecha_t;
                $strFechaHoraCambioEstado_t = $valueAgente->strFechaHoraCambioEstado_t;

                $strFoto_t = $valueAgente->fotoActual;
                $intIdAgente_t = $valueAgente->idUsuario;
                $strIdentificacionAgente_t = $valueAgente->identificacionUsuario;

                $intIdCampanActual_t = null;
                if (isset($valueAgente->intIdCampanaActual_t)) {
                  $intIdCampanActual_t = $valueAgente->intIdCampanaActual_t;
                }

                $strNombreAgente_t = $valueAgente->nombreUsuario;
                $strPausa_t = $valueAgente->pausa;

                if ($strEstado_t == "Disponible") {
                  $strColorEstado_t = "#009603";
                  $intDisponibles_t ++;
                }elseif ($strEstado_t == "Pausado") {
                  $strColorEstado_t = "#f39d00";
                  $intPausados_t ++;
                }elseif (stristr($strEstado_t, 'Ocupado')) {
                  if ($intIdCampanActual_t == $intIdCampan_t) {
                    $strColorEstado_t = "#ff1c00";
                    $intOcupados_t ++;
                  }else{
                    $strColorEstado_t = "#941200";
                    $intOcupados2_t ++;
                  }
                }else{
                  $strColorEstado_t = "#9c9c9c";
                  $intNoDisponibles_t ++;
                }

                $strFoto_t = "assets/img/Kakashi.fw.png";
                if(file_exists("/var/../Dyalogo/img_usuarios/usr".$intIdAgente_t.".jpg")){
                    $strFoto_t = "/DyalogoImagenes/usr".$intIdAgente_t.".jpg";
                }
                
                $arrAgentes_t[$keyAgente] = [
                  "strNombreEstpasActaual_t"  => $strNombreEstpasActaual_t,
                  "strCanalActual_t"  => $strCanalActual_t,
                  "strDuracionEstadoTiempo_t" => $strDuracionEstadoTiempo_t,
                  "strEstado_t"               => $strEstado_t,
                  "strColorEstado_t"          => $strColorEstado_t,
                  "strFechaHoraCambioEstadoFormat_t"  => $strFechaHoraCambioEstadoFormat_t,
                  "strFechaHoraCambioEstado_t"  => $strFechaHoraCambioEstado_t,
                  "strFoto_t"                 => $strFoto_t,
                  "intIdAgente_t"             => $intIdAgente_t,
                  "strIdentificacionAgente_t" => $strIdentificacionAgente_t,
                  "intIdCampanActual_t"       => $intIdCampanActual_t,
                  "strNombreAgente_t"         => $strNombreAgente_t,
                  "strPausa_t"                => $strPausa_t,
                  "strFoto_t"                 => $strFoto_t
                ];

              }

            }

            $arrOrdenAgentes_t = ["#9c9c9c","#009603","#f39d00","#941200","#ff1c00"];

            $arrAgentesOrdenados_t = [];

            $intIter_t = 0;


            //JDBD - Organizamos los agentes por estado.
            foreach ($arrOrdenAgentes_t as $color) {

              foreach ($arrAgentes_t as $array) {

                if ($color == $array["strColorEstado_t"]) {

                  $arrAgentesOrdenados_t[$intIter_t] = $array;

                  $intIter_t ++;

                }

              }

            }

            $arrPasos_t[$keyPaso]["arrMetas_t"] = $arrMetas_t;
            $arrPasos_t[$keyPaso]["arrAgentes_t"] = $arrAgentesOrdenados_t;
            $arrPasos_t[$keyPaso]["arrEstados_t"] = [0=>["strEstado_t"=>"<i class=\"fa fa-warning\"></i> Sin conexion",
                                                         "strColor_t" => "#9c9c9c",
                                                         "intEstado_t"=>$intNoDisponibles_t],
                                                     1=>["strEstado_t"=>"Pausados",
                                                         "strColor_t" => "#f39d00",
                                                         "intEstado_t"=>$intPausados_t],
                                                     2=>["strEstado_t"=>"Ocupados",
                                                         "strColor_t" => "#ff1c00",
                                                         "intEstado_t"=>$intOcupados_t],
                                                     3=>["strEstado_t"=>"No disponible",
                                                         "strColor_t" => "#941200",
                                                         "intEstado_t"=>$intOcupados2_t],
                                                     4=>["strEstado_t"=>"Disponibles",
                                                         "strColor_t" => "#009603",
                                                         "intEstado_t"=>$intDisponibles_t]];

          }

        }

        $arrTiempoReal_t[$keyEstrat]["arrEstpas_t"] = $arrPasos_t;

      }

    }

  
    echo json_encode($arrTiempoReal_t);
  
  }

  if (isset($_GET["ConsAcordeon"])) {

    if (isset($_SESSION["ACORDEONES"])) {

      $arrAcordeones_t = json_decode($_SESSION["ACORDEONES"]);

      $strIdAcordeon_t = $_POST["strIdAcordeon_t"];

      $itEA = 0;

      foreach ($arrAcordeones_t as $EAkey => $EAvalue) {

        if ($EAvalue->strIdAcordeon_t == $strIdAcordeon_t) {

          $itEA ++;

          if ($EAvalue->strEstado_t == "none") {

            echo json_encode(["box box collapsed-box","plus","none"]);

          }else{

            echo json_encode(["box box","minus","block"]);

          }

        }

      }

      if ($itEA == 0) {

          echo json_encode(["box box","minus","block"]);
          
      }



    }else{

      echo json_encode(["box box","minus","block"]);

    }

  }

  if (isset($_GET["session2"])) {

    echo $_SESSION["ACORDEONES"];

  }

  function sentido($intTipoCampan_p){

    if ($intTipoCampan_p == 1) {
      return "Entrante";
    }elseif($intTipoCampan_p == 6){
      return "Saliente";
    }

  }

?>